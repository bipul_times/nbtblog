<!--[if lt IE 9]>
<style>
  .container{
    width:1002px;
  }
</style>
<![endif]-->
<?php
$main_menus = array();         
foreach(wp_get_nav_menu_items('main-navigation') as $val){
   if($val->menu_item_parent != 0){
       if(is_array($main_menus[$val->menu_item_parent]->children)){
		   $main_menus[$val->menu_item_parent]->children[$val->ID] = $val;
	   } else {
		$main_menus[$val->menu_item_parent]->children = array($val->ID => $val);
	   }
   }
   else{
       $main_menus[$val->ID] = $val;
   }
}
?>
<div class="container">
  <div class="row">
    <div style='width: 100%;margin: 0 auto;' id="main-header">
      <div class="navlft">
        <div class="tpgry1">
		  <div class="tpgrynw1">
			<div class="topbrnw1">
			  <div style="width:470px;float:left;" class="globalnav">
				<div style="margin: 0px 0pt 0pt 5px; float: left;">
				  <a target="_blank" href="http://tamil.samayam.com/">தமிழ்</a><span>|</span>
				  <a target="_blank" href="http://telugu.samayam.com/">తెలుగు</a><span>|</span>
				  <a target="_blank" href="http://malayalam.samayam.com/">മലയാളം</a><span>|</span>
				  <a target="_blank" href="http://eisamay.indiatimes.com/">বাংলা</a><span>|</span>
				  <a target="_blank" href="http://navgujaratsamay.indiatimes.com/">ગુજરાતી</a><span>|</span>
				  <a target="_blank" href="http://navbharattimes.indiatimes.com/">हिन्दी</a><span>|</span>
				  <a target="_blank" href="http://vijaykarnataka.indiatimes.com/">ಕನ್ನಡ</a><span>|</span>
				  <a target="_blank" href="http://maharashtratimes.indiatimes.com/">मराठी</a><span>|</span>
				  <a target="_blank" href="http://timesofindia.indiatimes.com/">News</a>
				</div>
				<div id="first" style="float: left;">
				  <div style="padding: 0px 2px 0px 7px; width: 50px; color: rgb(2, 78, 151); position:relative; cursor: pointer;" id="srchsel3">
					<div style="position: absolute; top: -3px; left: 0px; z-index: 1000; display: none; background-color: rgb(255, 255, 255); text-align: left; width: 100px;" id="srchcmb3">
					  <div style="height:22px;" class="moreim" id="secdiv"><?php echo $my_settings['more_txt']; ?><img src="https://timesofindia.indiatimes.com/photo/5848326.cms" style="margin-top: 1px;" align="absmiddle"></div>
					  <div style="width:740px;background-color:#ffffff;position:absolute;left:-282px;top:28px;z-index:1000;border:1px solid #ABABAB;" class="glblnav1">
						<div style="clear:both;height:10px;"></div>
					  </div>
					</div>
				  </div>
				</div>
			  </div>
			  <div class="toprtnw1">
				<div style="float:right;padding-top:3px;"><a style="margin:0px;padding:0px;" target="_blank" href="<?php echo $my_settings['rss_url'];?>"><img align="absmiddle" src="https://timesofindia.indiatimes.com/photo/5552682.cms" border="0" hspace="0"></a></div>
				<div style="float:right;padding-top:3px;"><a target="_blank" style="text-decoration: none;margin-left:1px;" href="<?php echo $my_settings['google_url'];?>"><img style="border: 0;" height="16" width="16" src="https://ssl.gstatic.com/images/icons/gplus-16.png"></a></div>
				<div style="float:right;padding-top:3px;"><a target="_blank" style="text-decoration: none;margin-left:1px;" href="<?php echo $my_settings['twitter_url'];?>"><img style="border: 0;" height="16" width="16" src="https://twitter.com/favicon.ico"></a></div>
				<div style="float:right;padding-top:3px;"><a target="_blank" style="text-decoration: none;margin-left:1px;" href="<?php echo $my_settings['fb_url'];?>"><img style="border: 0;" height="16" width="16" src="https://www.facebook.com/favicon.ico"></a></div>
				<div class="top" style="float:right;font-family: Arial, Helvetica, sans-serif; padding:5px 5px 0 0;">
				  <div class="wrapper">
					<span data-plugin="user-isloggedin" class="hide">  <!-- span visible when user is not logged in -->
					  <img data-plugin="user-icon" />
					  <span data-plugin="user-name"></span>
					  <span data-plugin="user-points-wrapper" class="hide">
						(<span data-plugin="user-level"></span>
						<span data-plugin="user-points"></span>)
					  </span> |
					  <span data-plugin="user-logout" style="cursor:pointer"><?php echo $my_settings['logout_txt']; ?></span>
					</span>
					<span data-plugin="user-notloggedin" class="hide pointer"> <!-- span visible when user is not logged in -->
					  <span data-plugin="user-login" style="cursor:pointer"><?php echo $my_settings['login_txt']; ?></span>
					</span>  
				  </div>
				</div>
			  </div>
			</div>
		  </div>  
<div class="padlftrgt">
  <div>
    <div style="display:table;width:100%;height: 105px;">
      <div style="padding-top:8px;">
        <div class="fltpdg">
          <div class="fltleft">
            <div id="">
              <a href="<?php echo $my_settings['main_site_url'];?>">
                <img class="logoImg" style="float:left" title="<?php echo $my_settings['logo_title'];?>" alt="<?php echo $my_settings['logo_title'];?>" src="<?php echo $my_settings['logo_url'];?>" border="0">
              </a>
              <br>
				<?php /*<h1 style="color: #7a7a7a;font-family: Arial;font-size: 11px;margin-top: 10px;padding-top: 11px;text-align:center;">
				    <?php  $date_format = get_option( 'date_format' );
					$time_format = get_option( 'time_format' );
					echo date( "{$date_format}, {$time_format}", current_time( 'timestamp' ) );?>
				</h1>*/ ?>
            </div>
          </div>
          <div style="padding-top:5px;" class="fltleft">
          </div>
        </div>
        <div style="margin-right:8px;" class="topsrh ta_pad">
          <div class="tsh"><?php echo $my_settings['google_ad_top']; ?></div>
        </div>
      </div>
      <div class="clrtop5">
      </div>
    </div>
  </div>
  <?php    include 'nav.php'; ?>
</div>
</div>
</div>
</div>
</div>
</div>
  <?php    include 'nav-mob.php'; ?>
