<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd" >
<html>

<head>
    <META http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Tamil Samayam Newsletter</title>
    <style>
        body,
        div,
        img,
        p {
            margin: 0;
            padding: 0;
            font-family: ARIAL UNICODE MS, mangal, raghu8;
            font-size: 17px;
        }
        
        body {
            background: #f2f2f2;
        }
        
        .newsltr_cont {
            width: 700px;
            margin: 0 auto;
        }
        
        .newsltr_message {
            text-align: center;
            width: 650px;
            font-size: 11px;
            margin: 0 auto;
            padding: 10px 0;
        }
        
        .logo {
            width: 700px;
            height: 48px;
        }
        
        #topbar_outer {
            background: #fff url(http://tamil.samayam.com/photo/18792467.cms) repeat-x center 30px;
            position: fixed;
            top: 0;
            z-index: 10000;
        }
        
        #topnav_outer {
            width: 700px;
            float: left;
            background-image: url('http://tamil.samayam.com/photo/19526379.cms');
            background-repeat: repeat-x;
            height: 33px
        }
        
        #topNav {
            width: 700px;
            margin: 0 auto;
        }
        
        .tabs {
            margin: 0;
            padding: 0;
            list-style: none;
            float: left;
            width: 700px;
        }
        
        .tabs ul {
            padding: 0;
            margin: 0;
        }
        
        .tabs li {
            float: left;
            display: block;
            padding: 0;
            margin: 0;
        }
        
        .tabs a {
            float: left;
            padding: 0 14px;
            text-decoration: none !important;
            color: #fff;
            height: 33px;
            line-height: 33px;
            font-size: 17px;
        }
        
        .clear {
            clear: both;
        }
        
        .newsltr_body {
            background: #fff;
            float: left;
            width: 668px;
            padding: 20px 15px 15px 15px;
            border-left: 1px solid #dadada;
            border-right: 1px solid #dadada
        }
        
        .lftpart {
            float: left;
            width: 655px;
            margin-right: 13px;
        }
        
        .title-wrap {
            height: 27px;
            border-bottom: solid #9b3202;
            border-width: 1px;
            color: #a4511f;
        }
        
        .title-wrap a {
            text-decoration: none
        }
        
        .txt_style {
            color: #a1a1a1;
        }
        
        .txt_deco {
            color: #4e80ba;
            font-size: 15px;
        }
        
        .txt_deco a {
            padding-bottom: 5px;
            color: #4e80ba;
            font-size: 14px;
            text-decoration: none;
            line-height: 23px;
        }
        
        .clr {
            clear: both;
            height: 20px;
            background: #ffffff
        }
        
        .footer {
            font-size: 11px;
            height: 50px;
            padding-bottom: 10px;
            clear: both;
        }
        
        .footer span.copyright {
            font-size: 11px;
            font-family: ARIAL UNICODE MS, mangal, raghu8
        }
        
        .footer a {
            text-decoration: none;
        }
        
        img {
            border: 0px;
        }
    </style>
    <script type="text/javascript">
        var domainname = document.location.host;
        var domainname = document.location.host;
        var hrefpath = document.location.href;
        var domainpath = window.location.origin;
        var __activity = [];
    </script>
    <link rel="publisher" href="https://plus.google.com/116030764148407709961/about">
    <script type="text/javascript">
        var _sf_startpt = (new Date()).getTime()
    </script>
    <link href="/icons/tamil.ico" rel="shortcut icon">
    <script language="javascript">
        var qstr = '';
        var qmsid = '';
        var cmsur_type = 'viewed';

        function blockError() {
            return true;
        }
        window.onerror = blockError;
        var dtTT_startofpage = new Date();
        var rndtno = Math.random();
    </script>
    <script>
        var hdomain = 'samayam.com';
        if (document.domain != hdomain) {
            if ((document.domain.indexOf(hdomain)) != -1) {
                document.domain = hdomain
            } else if (window.location.href.indexOf("toidev") >= 0 || window.location.href.indexOf("langdev") >= 0) {
                document.domain = 'indiatimes.com';
            }
        }
    </script>
    <script>
        function Get_Ckie(name) {
            var value = "; " + document.cookie;
            var parts = value.split("; " + name + "=");
            if (parts.length == 2) return parts.pop().split(";").shift();
        }

        (function(i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function() {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-29031733-11', 'auto');
        if (typeof Get_Ckie == "function" && Get_Ckie('ssoid')) {
            ga('set', 'userId', Get_Ckie('ssoid'));
        }
        ga('require', 'displayfeatures');
        ga('send', 'pageview');
    </script>
    <script>
        var _comscore = _comscore || [];
        _comscore.push({
            c1: "2",
            c2: "6036484"
        });
        (function() {
            var s = document.createElement("script"),
                el = document.getElementsByTagName("script")[0];
            s.async = true;
            s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";
            el.parentNode.insertBefore(s, el);
        })();
    </script>
    <noscript><img src="https://sb.scorecardresearch.com/p?c1=2&amp;c2=6036484&amp;cv=2.0&amp;cj=1"></noscript>
    <div id="fb-root"></div>
    <script>
        window.fbAsyncInit = function() {
            FB.init({
                appId: '980029552049573',
                xfbml: true,
                version: 'v2.0'
            });
        };
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {
                return;
            }
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
        FB.init({
            appId: '980029552049573',
            oauth: true,
            status: true,
            cookie: true,
            xfbml: true
        });
    </script>
</head>

<body>
    <table width="700" cellspacing="0" cellpadding="0" align="center">
        <tr>
            <td>
                <div class="newsltr_cont">
                    <div class="newsltr_body">
                        <div class="lftpart">
                            <p><font style="font-weight:bold">அன்பு வாசகர்,</font>
                                <br>
                                <br>'தமிழ் சமயத்துடன் இணைப்பில் இருந்ததற்கு நன்றி!
                                <br>
                                <br>உங்களது கருத்தை தமிழ்.சமயம்.காமில் லைவ் ஆக பார்க்கலாம் இதைப் பார்க்க <a style="text-decoration:none;" href="http://blogs.tamil.samayam.com/?p=[#article_id]&comments=show&amp;commentid=[#usrcommentid]&amp;type=[#commenttype]">இங்கே</a> கிளிக் செய்யவும்
                                <br>
                                <br>'தொடர்ந்து தமிழ்.சமயம்.காமில் இருக்கவும். உங்களது நண்பர்களுடன் இணைப்பை பகிரவும்
                                <br>
                                <br><font style="font-weight:bold">நன்றி,</font>
                                <br>ஆசிரியர்
                                <br>
                            </p>
                        </div>
                    </div>
                    <div class="footer" align="center"><a href="http://www.indiatimes.com/aboutus" target="_blank">About Us</a>| <a href="http://advertise.indiatimes.com/" target="_blank">Advertise with Us</a> | <a href="http://www.indiatimes.com/termsandcondition" target="_blank">Terms of Use</a> | <a href="http://www.indiatimes.com/privacypolicy" target="_blank">Privacy 
								    Policy</a> | <a href="http://tamil.samayam.com/sitemap.cms" target="_blank">Sitemap</a>
                        <br><span class="copyright">Copyright &copy; <?php echo date('Y');?> Bennett Coleman &amp; Co. Ltd. All rights reserved. For reprint rights: <a href="http://timescontent.com/" target="_blank">Times Syndication Service</a></span>
                        <br><span style="padding-top:5px;">If you want to unsubscribe this service, please <a href="#" target="_blank">click here</a></span></div>
                </div>
            </td>
        </tr>
    </table>
    <script>
        function blockError() {
            return true;
        }
        window.onerror = blockError;
        var timeslog_channel_url = 'tamil.samayam.com';
        var ttrendlogmsid = '';
    </script>
    <script>
        (function() {
            var _fbq = window._fbq || (window._fbq = []);
            if (!_fbq.loaded) {
                var fbds = document.createElement('script');
                fbds.async = true;
                fbds.src = '//connect.facebook.net/en_US/fbds.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(fbds, s);
                _fbq.loaded = true;
            }
            _fbq.push(['addPixelId', '530684973736330']);
        })();
        window._fbq = window._fbq || [];
        window._fbq.push(['track', 'PixelInitialized', {}]);
    </script>
    <noscript><img src="https://www.facebook.com/tr?id=530684973736330&amp;ev=PixelInitialized" style="display:none" alt="" width="1" height="1"></noscript>
</body>

</html>