<?php 

define('Z_IMAGE_PLACEHOLDER', get_template_directory_uri()."/images/placeholder.png");
define('FB_PLACEHOLDER', get_template_directory_uri()."/images/blog-tl-facebook.png");
define('Z_AUTHOR_PLACEHOLDER', get_template_directory_uri()."/images/50.jpg");
define('RECO_URL','http://reco.indiatimes.com/Reco/RecoIndexer?eventType=update&hostid=1b&contentType=blog&msid=');
define('COMMENTS_URL','http://www.telugublogcmtapi.indiatimes.com/');
define('RCHID', '2147478029');
define('MYTIMES_URL','http://myt.indiatimes.com/');
define('MYT_COMMENTS_API_KEY', 'TELB');

global $my_settings;
$my_settings['site_lang'] = 'te';
$my_settings['og_locale'] = 'te_IN';
$my_settings['google_site_verification'] = 'zU5qA2HPdN2PFPyZd70q25_5NHXI8AE8AbqWNwXsoFA';
$my_settings['fblikeurl'] = 'https://www.facebook.com/pages/Telugu-Samayam/515040411993267';
$my_settings['favicon'] = 'https://telugu.samayam.com/icons/telugu.ico';
$my_settings['ga'] = "<script>

TimesGDPR.common.consentModule.gdprCallback(function(data) {
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-29031733-9', 'auto');
  var userCookie = document.cookie.match(/(?:\s)?ssoid=(\w+);?/);
  if(!!(userCookie)) {
	ga('set', 'userId', userCookie[1]);
  }
  if( TimesGDPR.common.consentModule.isEUuser() ){
  ga('set', 'anonymizeIp', true); 
  }
  ga('require', 'displayfeatures');
  ga('send', 'pageview');
  });
  </script>";
$my_settings['fbappid'] = '417098625162615';
$my_settings['channel_url_part'] = 'channel=tl';
$my_settings['main_site_url'] = 'https://telugu.samayam.com';
$my_settings['main_site_txt'] = 'Telugu Samayam Blog';
$my_settings['fb_url'] = 'https://www.facebook.com/pages/Telugu-Samayam/515040411993267?fref=ts';
$my_settings['twitter_url'] = 'https://twitter.com/TeluguSamayam';
$my_settings['twitter_handle'] = 'TeluguSamayam';
$my_settings['google_url'] = 'https://plus.google.com/b/101444379247252748743/dashboard/overview/getstarted?hl=en&service=PLUS';
$my_settings['rss_url'] = 'http://telugu.samayam.com/rssfeedsdefault.cms';
$my_settings['logo_title'] = 'Telugu Samayam';
$my_settings['logo_url'] = 'https://telugu.samayam.com/photo/48054996.cms';
$my_settings['footer_logo_txt'] = 'సమయం';
$my_settings['comscore_tag'] = '<!-- Begin comScore Tag -->
<script>
  var _comscore = _comscore || [];
  var objComScore = { c1: "2", c2: "6036484" }; 
  TimesGDPR.common.consentModule.gdprCallback(function(data) {
    if( TimesGDPR.common.consentModule.isEUuser() ){
      objComScore["cs_ucfr"] = 0;
    }
   _comscore.push(objComScore); 
  (function() {
	var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
	s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";
	el.parentNode.insertBefore(s, el);
  })();
  });
   
</script>
<noscript><img src="http://b.scorecardresearch.com/p?c1=2&amp;c2=6036484&amp;cv=2.0&amp;cj=1"></noscript>
<!-- End comScore Tag -->';
$my_settings['ibeat_channel'] = 'TeluguBlog';
$my_settings['ibeat_host'] = 'blogs.telugu.samayam.com';
$my_settings['ibeat_key'] = 'c4e164ec3142f749bcb10ba2c84fe';
$my_settings['ibeat_domain'] = 'telugu.samayam.com';
$my_settings['footer_google_tag'] = "<!-- /7176/Telgu/TLG_Home/TLG_Home_Home/TLG_HP_OP_Innov1 -->
<div id='div-gpt-ad-4354024247868-4'>
<script type='text/javascript'>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-4354024247868-4'); });
</script>
</div>
<!-- /7176/Telgu/TLG_Home/TLG_Home_Home/TLG_HP_OP_Inter -->
<div id='div-gpt-ad-4354024247868-5'>
<script type='text/javascript'>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-4354024247868-5'); });
</script>
</div>
<!-- /7176/Telgu/TLG_Home/TLG_Home_Home/TLG_HP_OP_Pop -->
<div id='div-gpt-ad-4354024247868-6'>
<script type='text/javascript'>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-4354024247868-6'); });
</script>
</div>
<!-- /7176/Telgu/TLG_Home/TLG_Home_Home/TLG_HP_OP_Shosh -->
<div id='div-gpt-ad-4354024247868-7'>
<script type='text/javascript'>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-4354024247868-7'); });
</script>
</div>";
$my_settings['google_tag_js_head'] = "<script type='text/javascript'>
	var googletag = googletag || {};
	googletag.cmd = googletag.cmd || [];
	(function() {
	var gads = document.createElement('script');
	gads.async = true;
	gads.type = 'text/javascript';
	var useSSL = 'https:' == document.location.protocol;
	gads.src = (useSSL ? 'https:' : 'http:') + 
	'//www.googletagservices.com/tag/js/gpt.js';
	var node = document.getElementsByTagName('script')[0];
	node.parentNode.insertBefore(gads, node);
	})();
	</script>
	
	<script type='text/javascript'>
	googletag.cmd.push(function() {

	<!-- Audience Segment Targeting -->
	var _auds = new Array();
	if(typeof(_ccaud)!='undefined') {
	for(var i=0;i<_ccaud.Profile.Audiences.Audience.length;i++)
	if(i<200)
	_auds.push(_ccaud.Profile.Audiences.Audience[i].abbr);
	}
	<!-- End Audience Segment Targeting -->
	
	<!-- Contextual Targeting -->
	var _HDL = '';
	var _ARC1 = '';
	var _Hyp1 = '';
	var _article = '';
	var _tval = function(v) {
	if(typeof(v)=='undefined') return '';
	if(v.length>100) return v.substr(0,100);
	return v;
	}
	<!-- End Contextual Targeting -->


googletag.defineSlot('/7176/Telgu/TLG_Home/TLG_Home_Home/TLG_HP_ATF_300', [300,250], 'div-gpt-ad-4354024247868-0').addService(googletag.pubads());
googletag.defineSlot('/7176/Telgu/TLG_Home/TLG_Home_Home/TLG_HP_BTF_300', [300,250], 'div-gpt-ad-4354024247868-1').addService(googletag.pubads());
googletag.defineSlot('/7176/Telgu/TLG_Home/TLG_Home_Home/TLG_HP_BTF_728', [728,90], 'div-gpt-ad-4354024247868-2').addService(googletag.pubads());
googletag.defineSlot('/7176/Telgu/TLG_Home/TLG_Home_Home/TLG_HP_MTF_300', [300,250], 'div-gpt-ad-4354024247868-3').addService(googletag.pubads());
googletag.defineSlot('/7176/Telgu/TLG_Home/TLG_Home_Home/TLG_HP_OP_Innov1', [1,1], 'div-gpt-ad-4354024247868-4').addService(googletag.pubads());
googletag.defineSlot('/7176/Telgu/TLG_Home/TLG_Home_Home/TLG_HP_OP_Inter', [1,1], 'div-gpt-ad-4354024247868-5').addService(googletag.pubads());
googletag.defineSlot('/7176/Telgu/TLG_Home/TLG_Home_Home/TLG_HP_OP_Pop', [1,1], 'div-gpt-ad-4354024247868-6').addService(googletag.pubads());
googletag.defineSlot('/7176/Telgu/TLG_Home/TLG_Home_Home/TLG_HP_OP_Shosh', [1,1], 'div-gpt-ad-4354024247868-7').addService(googletag.pubads());

googletag.pubads().setTargeting('sg', _auds).setTargeting('HDL', _tval(_HDL)).setTargeting('ARC1', _tval(_ARC1)).setTargeting('Hyp1', _tval(_Hyp1)).setTargeting('article', _tval(_article));
googletag.pubads().enableSingleRequest();
googletag.pubads().collapseEmptyDivs();
TimesGDPR.common.consentModule.gdprCallback(function(data) {
	if( TimesGDPR.common.consentModule.isEUuser() ) {
		googletag.pubads().setRequestNonPersonalizedAds(1);
	}
	googletag.enableServices();
} );
});
</script>";
$my_settings['google_ad_top'] = "<!-- /7176/Telgu/TLG_Home/TLG_Home_Home/TLG_HP_BTF_728 -->
<div id='div-gpt-ad-4354024247868-2'>
<script type='text/javascript'>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-4354024247868-2'); });
</script>
</div>";
$my_settings['google_ad_right_1'] = "<!-- /7176/Telgu/TLG_Home/TLG_Home_Home/TLG_HP_ATF_300 -->
<div id='div-gpt-ad-4354024247868-0'>
<script type='text/javascript'>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-4354024247868-0'); });
</script>
</div>";
$my_settings['google_ad_right_2'] = "<!-- /7176/Telgu/TLG_Home/TLG_Home_Home/TLG_HP_BTF_300 -->
<div id='div-gpt-ad-4354024247868-1'>
<script type='text/javascript'>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-4354024247868-1'); });
</script>
</div>";
$my_settings['visual_revenue_reader_response_tracking_script'] = "<!--Visual Revenue Reader Response Tracking Script (v6) -->
        <script type='text/javascript'>
            var _vrq = _vrq || [];
            _vrq.push(['id', 624]);
            _vrq.push(['automate', false]);
            _vrq.push(['track', function(){}]);
            (function(d, a){
                var s = d.createElement(a),
                x = d.getElementsByTagName(a)[0];
                s.async = true;
            s.src = 'http://a.visualrevenue.com/vrs.js';
            x.parentNode.insertBefore(s, x);
            })(document, 'script');
        </script>
        <!-- End of VR RR Tracking Script - All rights reserved -->";
$my_settings['visual_revenue_reader_response_tracking_script_for_not_singular'] = "<!--Visual Revenue Reader Response Tracking Script (v6) -->
        <script type='text/javascript'>
            var _vrq = _vrq || [];
            _vrq.push(['id', 624]);
            _vrq.push(['automate', true]);
            _vrq.push(['track', function(){}]);
            (function(d, a){
                var s = d.createElement(a),
                x = d.getElementsByTagName(a)[0];
                s.async = true;
            s.src = 'http://a.visualrevenue.com/vrs.js';
            x.parentNode.insertBefore(s, x);
            })(document, 'script');
        </script>
        <!-- End of VR RR Tracking Script - All rights reserved -->";
$my_settings['not_found_heading'] = 'దొరకలేదు';
$my_settings['not_found_txt'] = 'మీ సెర్చ్ విఫలమైంది. మళ్లీ ప్రయత్నించండి';
$my_settings['search_placeholder_txt'] = '3 అక్షరాలు టైప్ చేయండి ...';
$my_settings['go_to_the_profile_of_txt'] = '';
$my_settings['go_to_txt'] = '';
$my_settings['share_link_fb_txt'] = 'ఫేస్ బుక్ లో షేర్ చేయండి';
$my_settings['share_link_twitter_txt'] = 'ట్విట్టర్ లో షేర్ చేయండి';
$my_settings['share_link_linkedin_txt'] = 'లింక్డన్ లో షేర్ చేయండి';
$my_settings['mail_link_txt'] = 'మెయిల్ లింక్';
$my_settings['read_complete_article_here_txt'] = 'ఇక్కడ పూర్తి వార్త చదవండి';
$my_settings['featured_txt'] = 'ఫీచర్డ్';
$my_settings['disclaimer_txt'] = 'తిరస్కరించబడింది';
$my_settings['disclaimer_on_txt'] = 'ఇది టైమ్స్ ఆఫ్ ఇండియా పేపర్ లోని అభిప్రాయం';
$my_settings['disclaimer_off_txt'] = "పైన వ్యక్తపరిచిన అభిప్రాయాలు రచయితవి మాత్రమే";
$my_settings['most discussed_txt'] = 'ఎక్కువ చర్చించినవి';
$my_settings['more_txt'] = 'మరిన్ని';
$my_settings['less_txt'] = 'తక్కువ';
//$my_settings['login_txt'] = 'లాగిన్';
//$my_settings['logout_txt'] = 'లాగౌట్';
$my_settings['login_txt'] = 'Log In';
$my_settings['logout_txt'] = 'Log Out';
$my_settings['view_all_posts_in_txt'] = '';
$my_settings['home_txt'] = 'హోం';
$my_settings['blogs_txt'] = 'బ్లాగులు';
$my_settings['search_results_for_txt'] = 'కోసం శోధన ఫలితాలు';
$my_settings['most_read_txt'] = 'ఎక్కువమంది చదివినవి';
$my_settings['popular_tags_txt'] = 'ఆదరణపొందిన ట్యాగులు';
$my_settings['recently_joined_authors_txt'] = 'ఇటీవలే చేరిన రచయితలు';
$my_settings['like_us_txt'] = 'లైక్ చేయండి';
$my_settings['author_txt'] = 'రచయిత';
$my_settings['popular_from_author_txt'] = 'ప్రముఖ పోస్ట్లు రచయిత';
$my_settings['search_authors_by_name_txt'] = 'రచయిత పేరుతో ప్రయత్నించండి';
$my_settings['search_txt'] = 'శోధన';
$my_settings['back_to_authors_page_txt'] = 'తిరిగి రచయితల పేజీకి';
$my_settings['no_authors_found_txt'] = 'అలాంటి  రచయితలు లేరు';
$my_settings['further_commenting_is_disabled_txt'] = 'మరిన్ని వ్యాఖ్యలు నిషేధించబడినవి';
$my_settings['comments_on_this_post_are_closed_now_txt'] = 'ఈ పోస్ట్ పై వ్యాఖ్యలు ఇక అనుమతించబడవు';
$my_settings['add_your_comment_here_txt'] = 'మీ వ్యాఖ్యను ఇక్కడ జోడించండి';
$my_settings['characters_remaining_txt'] = 'మిగిలిన అక్షరాలు';
$my_settings['share_on_fb_txt'] = 'ఫేస్ బుక్ తో పంచుకోండి';
$my_settings['share_on_twitter_txt'] = 'ట్విట్టర్ తో పంచుకోండి';
$my_settings['sort_by_txt'] = 'క్రోడీకరించు';
$my_settings['newest_txt'] = 'సరికొత్తది';
$my_settings['oldest_txt'] = 'పురాతనమైనది';
$my_settings['discussed_txt'] = 'చర్చించినవి';
$my_settings['up_voted_txt'] = 'వేసిన ఓటు';
$my_settings['down_voted_txt'] = 'తిరస్కరించిన ఓట్లు';
$my_settings['be_the_first_one_to_review_txt'] = 'మొట్టమొదట మీరే సమీక్షించండి';
$my_settings['more_points_needed_to_reach_next_level_txt'] = 'తదుపరి స్థాయికి వెళ్లేందుకు మరిన్ని పాయింట్లు అవసరం';
$my_settings['know_more_about_times_points_txt'] = 'టైమ్స్ పాయింట్స్ గురించి మరిన్ని వివరాలు';
$my_settings['know_more_about_times_points_link'] = 'http://www.timespoints.com/about/8606871469114261504';
$my_settings['badges_earned_txt'] = 'మీరు సంపాదించిన బ్యాడ్జిలు';
$my_settings['just_now_txt'] = 'ఇప్పుడే';
$my_settings['follow_txt'] = 'అనుసరించండి';
$my_settings['reply_txt'] = 'ప్రత్యుత్తరం';
$my_settings['flag_txt'] = 'జెండా';
$my_settings['up_vote_txt'] = 'ఓటు';
$my_settings['down_vote_txt'] = 'తిరస్కరించిన ఓటు';
$my_settings['mark_as_offensive_txt'] = 'అభ్యంతరకరమైన వ్యాఖ్యలు';
$my_settings['find_this_comment_offensive_txt'] = 'ఈ వ్యాఖ్యను అభ్యంతరకరమైనదిగా భావిస్తున్నారా?';
$my_settings['reason_submitted_to_admin_txt'] = 'మీ అభ్యంతరాన్ని అడ్మిన్ కు పంపటమైనది';
$my_settings['choose_reason_txt'] = 'కింద పేర్కొన్న కారణాలలో ఏదైనా ఒకటి ఎంచుకుని ‘సబ్మిట్’ బటన్ ను ప్రెస్ చేయండి. ఇది తదుపరి చర్యకు ఉపయోగపడుతుంది.';
$my_settings['reason_for_reporting_txt'] = 'ఫిర్యాదుకు కారణం';
$my_settings['foul_language_txt'] = 'తప్పుడు భాష';
$my_settings['defamatory_txt'] = 'పరువునష్టం';
$my_settings['inciting_hatred_against_certain_community_txt'] = 'కొన్ని వర్గాలకు వ్యతిరేకంగా ద్వేషాన్ని ప్రోత్సహించటం';
$my_settings['out_of_context_spam_txt'] = 'అసందర్భం\అనవసరం';
$my_settings['others_txt'] = 'ఇతర కారణాలు';
$my_settings['report_this_txt'] = 'దీన్ని రిపోర్ట్ చేయండి';
$my_settings['close_txt'] = 'మూసివేయబడినది';
$my_settings['already_marked_as_offensive'] = 'అభ్యతరకరమైనదిగా ఇప్పటికే గుర్తించటమైనది';
$my_settings['flagged_txt'] = 'పతాకస్థాయి';
$my_settings['blogauthor_link'] = 'http://blogauthors.telugu.samayam.com/';
$my_settings['old_blogauthor_link'] = 'http://blogauthors.telugu.samayam.com/';
$my_settings['blog_link'] = 'http://blogs.telugu.samayam.com/';
$my_settings['common_cookie_domain'] = 'samayam.com';
$my_settings['quill_lang'] = 'telugu';
$my_settings['quill_link_1'] = 'తెలుగులో టైప్ చేయండి(ఇన్‌స్క్రిప్ట్)';
$my_settings['quill_link_2'] = 'తెలుగులో టైప్ చేయండి(ఇంగ్లీష్ అక్షరాలతో)';
$my_settings['quill_link_3'] = 'Write in English';
$my_settings['quill_link_4'] = 'వర్చువల్ కీబోర్డు';
$my_settings['footer'] = '<div class="container footercontainer"><div class="insideLinks"><h2>உங்களது பார்வைக்கு </h2><ul><li><a href="http://telugu.samayam.com/cinema/articlelist/48237837.cms" pg="fotkjlnk1" target="_blank">சினிமா </a></li><li><a href="http://telugu.samayam.com/social/articlelist/45939352.cms" target="_blank" pg="fotkjlnk2">சமூகம் </a></li><li><a href="http://telugu.samayam.com/news/articlelist/48237907.cms" target="_blank" pg="fotkjlnk10">செய்திகள்  </a></li><li><a href="http://telugu.samayam.com/lifestyle/articlelist/45939394.cms" pg="fotkjlnk3" target="_blank">லைஃப் ஸ்டைல்  </a></li><li><a href="http://telugu.samayam.com/sports/articlelist/47766652.cms" target="_blank" pg="fotkjlnk4">விளையாட்டு </a></li><li><a href="http://telugu.samayam.com/spiritual/articlelist/45939389.cms" pg="fotkjlnk5" target="_blank">ஆன்மிகம் </a></li><li><a href="http://telugu.samayam.com/jokes/articlelist/48213869.cms" target="_blank" pg="fotkjlnk6">ஜோக்ஸ் </a></li><li><a href="http://telugu.samayam.com/education/articlelist/48213890.cms" pg="fotkjlnk7" target="_blank">கல்வி </a></li><li><a href="http://telugu.samayam.com/video/videolist/47344128.cms" target="_blank" pg="fotkjlnk8">வீடியோ </a></li><li><a href="http://telugu.samayam.com/photogallery/photolist/47344662.cms" pg="fotkjlnk9" target="_blank">புகைப்படம் </a></li></ul></div><div class="newsletter"><div class="fottershareLinks"><h4>
						எப்பொழுதும் தமிழ் சமயம் App இணைப்பில் இருக்க 
					</h4><div class="fotterApplinks"><a href="https://play.google.com/store" target="_blank" class="andriod"></a></div></div></div><div class="timesotherLinks"><h2>எங்களது பிற இணையதளங்கள்</h2><a href="http://timesofindia.indiatimes.com/" target="_blank" pg="fottgwslnk1">Times of India</a>| <a target="_blank" href="http://economictimes.indiatimes.com/" pg="fottgwslnk2">Economic Times</a> | <a href="http://itimes.com/" target="_blank" pg="fottgwslnk16">iTimes</a>|<br><a href="http://vijaykarnataka.indiatimes.com/" target="_blank" pg="fottgwslnk4">Vijay Karnataka</a>| <a href="http://eisamay.indiatimes.com/" target="_blank" pg="fottgwslnk5">Ei Samay</a> | <a href="http://navgujaratsamay.indiatimes.com/" target="_blank" pg="fottngslnk5">Navgujarat Samay</a> | <a href="http://maharashtratimes.com/" target="_blank" pg="fottgwslnk6">महाराष्ट्र टाइम्स</a> |<a href="http://www.businessinsider.in/" target="_blank" pg="fottgwslnk7">Business Insider</a>| <a href="http://zoomtv.indiatimes.com/" target="_blank" pg="fottgwslnk8">ZoomTv</a> | <a href="http://boxtv.com/" target="_blank" pg="fottgwslnk11">BoxTV</a>| <a href="http://www.gaana.com/" target="_blank" pg="fottgwslnk12">Gaana</a> | <a href="http://shopping.indiatimes.com/" target="_blank" pg="fottgwslnk13">Shopping</a> | <a href="http://www.idiva.com/" target="_blank" pg="fottgwslnk14">IDiva</a> | <a href="http://www.astrospeak.com/" target="_blank" pg="fottgwslnk15">Astrology</a> |
				<a target="_blank" href="http://www.simplymarry.com/" pg="fottgwslnk17">Matrimonial</a><span class="footfbLike">பேஸ்புக்கில் தமிழ் சமயம்</span><iframe width="260px" height="35" frameborder="0" src="http://www.facebook.com/plugins/like.php?href=https%3a%2f%2fwww.facebook.com%2fpages%2fTelugu-Samayam%2f515040411993267&amp;width&amp;layout=standard&amp;action=like&amp;show_faces=false&amp;share=false&amp;height=35&amp;colorscheme=dark" scrolling="no" allowtransparency="true"></iframe></div><div class="timesLinks"><a target="_blank" href="http://www.timesinternet.in/" pg="fotlnk1">About Us</a>&nbsp;|&nbsp;
				<a target="_blank" href="http://advertise.indiatimes.com/" pg="fotlnk2">Advertise with Us</a>&nbsp;|&nbsp;
				<a target="_blank" href="http://www.indiatimes.com/termsandcondition" pg="fotlnk3">Terms of Use and Grievance Redressal Policy</a>&nbsp;|&nbsp;
				<a target="_blank" href="http://www.indiatimes.com/privacypolicy" pg="fotlnk4">Privacy Policy</a>&nbsp;|&nbsp;
						<a target="_blank" href="http://telugu.samayam.com/sitemap.cms" pg="fotlnk6">Sitemap</a></div><div class="copyText">Copyright &copy;&nbsp; '.date('Y').' &nbsp;Bennett Coleman &amp; Co. Ltd. All rights reserved. For reprint rights: <a target="_blank" href="http://timescontent.com/" pg="fotlnk7">Times Syndication Service</a></div></div>';
$my_settings['footer_css'] = '<style type="text/css">
.footercontainer{background:#333333; clear:both; overflow:hidden; padding:10px 0;}
.footercontainer a{color:#FDFDFD;}
.footercontainer h2{ color:#fff; font-size:20px; line-height:30px; padding:0 0 10px 2px;font-family: Arial, Helvetica, sans-serif;}

.insideLinks{ width:36%; padding-left:2%; float:left;}
.insideLinks ul{ list-style:none; display: inline-block; padding: 0; margin: 0}
.insideLinks ul li{ float:left; margin:0 10px 0 0; width:90px; }
.insideLinks ul li a{ font-family:Arial, Helvetica, sans-serif;color:#B5B3B4;font-size:14px;font-weight:bold;line-height:25px;}

.newsletter{width:32%; padding:0 2%; float:left;}
.newsletterpost {background:#CCCCCC; padding:14px; margin:10px 0 10px 0;float:left; width:300px;}

.newsletterpost input[type="text"]{background:#fff; border:1px #B7B7B7 solid; color:#333;height:21px; width:205px; font-size:14px;font-weight:bold;color:#999;float:left; padding:2px 10px;}
.newsletterpost input[type="submit"]{ background:#f5cc10; color:#000; padding:0 5px; *padding:0 4px; height:26px; cursor:pointer; border:none;font-weight:bold;float:left;}
.fottershareLinks span{ font-size:14px; color:#fff; width:100%; float:left; margin-bottom:10px;}
.fottershareLinks img{ vertical-align:middle;margin-top:5px;}
.fottershareLinks a{margin: 0 2px;}

.fotterApplinks{padding: 0 0 15px 0;}
.fotterApplinks a{padding: 0 10px 0 0; background-image:url(http://navbharattimes.indiatimes.com/photo/42711833.cms); float:left; width:47px; height:65px;background-repeat: no-repeat;}
.fotterApplinks a.andriod{background-position:0 4px}
.fotterApplinks a.andriod:hover{background-position:0 -68px}

.fotterApplinks a.ios{background-position: 0 -144px;}
.fotterApplinks a.ios:hover{background-position:0 -219px}

.fotterApplinks a.java{background-position:0 -296px;}
.fotterApplinks a.java:hover{background-position:0 -378px;}

.fotterApplinks a.win{background-position:0 -447px}
.fotterApplinks a.win:hover{background-position:0 -526px}

.fottershareLinks{clear: both;}
.fottershareLinks h4{color:#fff; font-size:16px;line-height: 23px;}
.fottershareLinks strong{color:#f5cc10; font-weight:normal}

.footfbLike{display: block;margin: 25px 0 12px 0;font-size: 14px;color: #fff;}

.timesotherLinks{width:32%; float:left; padding-right:2%; color:#CCCCCC;font-size:12px;}
.timesotherLinks a{ font-size:11px; padding:0 1px; color:#CCCCCC; line-height:18px;}

.timesLinks{ font-family:Arial;clear:both;text-align:center; font-size:12px;color:#929292; padding:10px 0; border-top:1px solid #474747;}
.timesLinks a {color:#FDFDFD; font-family:Arial;}

.copyText{ font-family:Arial;clear:both;text-align:center; font-size:12px;color:#929292; padding:0 35px;}
.copyText a {color:#FDFDFD; font-family:Arial;}

.seoLinks{clear:both; margin:10px 0; background:#585757; overflow:hidden;padding-left: 11px;}
.seoLinks ul{list-style:none; padding:0;}
.seoLinks ul li{float:left;}
.seoLinks ul li a{font-family:arial;padding:0 7px;color:#fff;font-size:13px; line-height:32px; display:block; float:left; border-left:#4a4a4a 1px solid;  border-right:#696969 1px solid;}
@media (max-width: 767px){.insideLinks,.newsletter,.timesotherLinks,.seoLinks,.timesLinks{display: none;}}
</style>';
