<?php 

define('Z_IMAGE_PLACEHOLDER', get_template_directory_uri()."/images/placeholder.png");
define('FB_PLACEHOLDER', get_template_directory_uri()."/images/blog-ngs-facebook.png");
define('Z_AUTHOR_PLACEHOLDER', get_template_directory_uri()."/images/50.jpg");
define('RECO_URL','http://reco.indiatimes.com/Reco/RecoIndexer?eventType=update&hostid=1b&contentType=blog&msid=');
define('COMMENTS_URL','');
define('RCHID', '');

global $my_settings;
$my_settings['site_lang'] = 'gu';
$my_settings['og_locale'] = 'gu_IN';
$my_settings['google_site_verification'] = '';
$my_settings['fblikeurl'] = 'http://www.facebook.com/navgujaratsamay';
$my_settings['favicon'] = 'http://navgujaratsamay.indiatimes.com/icons/ngs-fav.ico';
$my_settings['ga'] = "<script>

TimesGDPR.common.consentModule.gdprCallback(function(data) {
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-29031733-7', 'auto', {'allowLinker': true});
  var userCookie = document.cookie.match(/(?:\s)?ssoid=(\w+);?/);
  if(!!(userCookie)) {
	ga('set', 'userId', userCookie[1]);
  }
  if( TimesGDPR.common.consentModule.isEUuser() ){
  ga('set', 'anonymizeIp', true); 
  }
  ga('require', 'displayfeatures'); 
  ga('require', 'linker');
  ga('linker:autoLink', ['superpacer.in', 'navgujaratsamay.com', 'ngsamay.com', 'ngsnavratri.com'] );
  ga('send', 'pageview');
  });
  </script>";
$my_settings['fbappid'] = '117787264903013';
$my_settings['channel_url_part'] = 'channel=ngs';
$my_settings['main_site_url'] = 'http://navgujaratsamay.indiatimes.com';
$my_settings['main_site_txt'] = 'NavGujarat Samay Blog';
$my_settings['fb_url'] = 'http://www.facebook.com/navgujaratsamay';
$my_settings['twitter_url'] = 'http://twitter.com/navgujaratsamay';
$my_settings['twitter_handle'] = 'navgujaratsamay';
$my_settings['google_url'] = 'https://plus.google.com/116950524935037890208';
$my_settings['rss_url'] = 'http://navgujaratsamay.indiatimes.com/rssfeedsdefault.cms';
$my_settings['logo_title'] = 'NGS Blogs';
$my_settings['logo_url'] = 'http://navgujaratsamay.indiatimes.com/photo/28182221.cms';
$my_settings['footer_logo_txt'] = 'નવગુજરાત સમયની';
$my_settings['comscore_tag'] = '<!-- Begin comScore Tag -->
<script>
  var _comscore = _comscore || [];
  var objComScore = { c1: "2", c2: "6036484" }; 
  TimesGDPR.common.consentModule.gdprCallback(function(data) {
    if( TimesGDPR.common.consentModule.isEUuser() ){
      objComScore["cs_ucfr"] = 0;
    }
    _comscore.push(objComScore);   
  (function() {
	var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
	s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";
	el.parentNode.insertBefore(s, el);
  })();
  }); 
</script>
<noscript><img src="http://b.scorecardresearch.com/p?c1=2&amp;c2=6036484&amp;cv=2.0&amp;cj=1"></noscript>
<!-- End comScore Tag -->';
$my_settings['ibeat_channel'] = 'NgsBlog';
$my_settings['ibeat_host'] = 'blogs.navgujaratsamay.indiatimes.com';
$my_settings['ibeat_key'] = 'ac632e236282d9c6c6a7d63fdcae68ee';
$my_settings['ibeat_domain'] = 'navgujaratsamay.indiatimes.com';
$my_settings['footer_google_tag'] = "";
$my_settings['google_tag_js_head'] = "";
$my_settings['google_ad_top'] = "";
$my_settings['google_ad_right_1'] = "";
$my_settings['google_ad_right_2'] = "";
$my_settings['visual_revenue_reader_response_tracking_script'] = "";
$my_settings['visual_revenue_reader_response_tracking_script_for_not_singular'] = "";
$my_settings['not_found_heading'] = 'નથી મળી રહ્યું';
$my_settings['not_found_txt'] = 'આ લોકેશન પર કોઈ માહિતી નથી મળી રહી. સર્ચ ઓપ્શમાં જઈ પ્રયાસ કરો.';
$my_settings['search_placeholder_txt'] = 'ત્રણ શબ્દો ટાઈપ કરો';
$my_settings['go_to_the_profile_of_txt'] = '';
$my_settings['go_to_txt'] = '';
$my_settings['share_link_fb_txt'] = 'ફેસબુક પર લિંક શેર કરો';
$my_settings['share_link_twitter_txt'] = 'ટ્વીટ પર લિંક શેર કરો';
$my_settings['share_link_linkedin_txt'] = 'લિંક્ડઈન પર લિંક શેર કરો';
$my_settings['mail_link_txt'] = 'લિંકને ઈમેલ કરો';
$my_settings['read_complete_article_here_txt'] = 'અહીં વાંચો સંપૂર્ણ આર્ટિકલ';
$my_settings['featured_txt'] = 'ફીચર્ડ';
$my_settings['disclaimer_txt'] = 'ડિસ્ક્લેમર';
$my_settings['disclaimer_on_txt'] = 'આ લેખ નવગુજરાત સમયની પ્રિન્ટ એડિશનના તંત્રી લેખ તરીકે પ્રસિદ્ધ થયેલો છે';
$my_settings['disclaimer_off_txt'] = 'અહીં વ્યક્ત કરાયેલા વિચાર લેખકના પોતાના છે';
$my_settings['most discussed_txt'] = 'સૌથી વધુ ચર્ચિત';
$my_settings['more_txt'] = 'વધુ';
$my_settings['less_txt'] = 'ઓછું';
//$my_settings['login_txt'] = '';
//$my_settings['logout_txt'] = '';
$my_settings['login_txt'] = 'Log In';
$my_settings['logout_txt'] = 'Log Out';
$my_settings['view_all_posts_in_txt'] = '';
$my_settings['home_txt'] = 'હોમ';
$my_settings['blogs_txt'] = 'બ્લોગ્સ';
$my_settings['search_results_for_txt'] = 'નું સર્ચ રિઝલ્ટ';
$my_settings['most_read_txt'] = 'સૌથી વધુ વંચાયેલું';
$my_settings['popular_tags_txt'] = 'પોપ્યુલર ટેગ્સ';
$my_settings['recently_joined_authors_txt'] = 'હાલમાં જોડાયેલા લેખકો';
$my_settings['like_us_txt'] = 'અમને લાઈક કરો';
$my_settings['author_txt'] = 'લેખક';
$my_settings['popular_from_author_txt'] = 'સૌથી વધુ વંચાયેલા લેખક';
$my_settings['search_authors_by_name_txt'] = 'લેખકને નામ દ્વારા શોધો';
$my_settings['search_txt'] = 'શોધો';
$my_settings['back_to_authors_page_txt'] = 'લેખકના પેજ પર પરત જાઓ';
$my_settings['no_authors_found_txt'] = 'કોઈ લેખક નથી મળી રહ્યા';
$my_settings['further_commenting_is_disabled_txt'] = 'વધુ કૉમેન્ટ નહીં કરી શકાય';
$my_settings['comments_on_this_post_are_closed_now_txt'] = 'આ પોસ્ટ પર હવે કોઈ કૉમેન્ટ નહીં કરી શકાય';
$my_settings['add_your_comment_here_txt'] = 'આપની કૉમેન્ટ અહીં એડ કરો';
$my_settings['characters_remaining_txt'] = 'શબ્દો બાકી છે';
$my_settings['share_on_fb_txt'] = 'ફેસબુક પર શેર કરો';
$my_settings['share_on_twitter_txt'] = 'ટ્વીટર પર શેર કરો';
$my_settings['sort_by_txt'] = 'શોર્ટ બાય';
$my_settings['newest_txt'] = 'સૌથી નવું';
$my_settings['oldest_txt'] = 'સૌથી જૂનું';
$my_settings['discussed_txt'] = 'ચર્ચિત';
$my_settings['up_voted_txt'] = 'અપ વોટેડ';
$my_settings['down_voted_txt'] = 'ડાઉન વોટેડ';
$my_settings['be_the_first_one_to_review_txt'] = 'સૌથી પહેલો રિવ્યૂ લખો';
$my_settings['more_points_needed_to_reach_next_level_txt'] = 'આગલા લેવલ સુધી પહોંચવા વધુ પોઈન્ટ્સ જરુરી છે';
$my_settings['know_more_about_times_points_txt'] = 'ટાઈમ્સ પોઈન્ટ્સ વિશે વધુ જાણો';
$my_settings['know_more_about_times_points_link'] = 'http://navgujaratsamay.indiatimes.com/abouttimesrewards.cms';
$my_settings['badges_earned_txt'] = 'મેળવેલા બેજ્ડ';
$my_settings['just_now_txt'] = 'હાલમાં';
$my_settings['follow_txt'] = 'ફૉલો';
$my_settings['reply_txt'] = 'રિપ્લાય';
$my_settings['flag_txt'] = 'ફ્લેગ';
$my_settings['up_vote_txt'] = 'અપ વોટ';
$my_settings['down_vote_txt'] = 'ડાઉન વોટ';
$my_settings['mark_as_offensive_txt'] = 'વાંધાજનક તરીકે માર્ક કરો';
$my_settings['find_this_comment_offensive_txt'] = 'શું આ કૉમેન્ટ વાંધાજનક છે?';
$my_settings['reason_submitted_to_admin_txt'] = 'આપના દ્વારા અપાયેલું કારણ એડમિનને સબમિટ કરી દેવાયું છે';
$my_settings['choose_reason_txt'] = 'અમારા મોડરેટર્સને એલર્ટ કરવા માટે નીચેનામાંથી કોઈ કારણ પસંદ કરો અને સબમિટ બટન પર ક્લિક કરો';
$my_settings['reason_for_reporting_txt'] = 'રિપોર્ટિંગનું કારણ';
$my_settings['foul_language_txt'] = 'ભાષા યોગ્ય નથી';
$my_settings['defamatory_txt'] = 'બદનક્ષીપૂર્ણ';
$my_settings['inciting_hatred_against_certain_community_txt'] = 'કોઈ સમુદાય સામે નફરત ફેલાવનારું';
$my_settings['out_of_context_spam_txt'] = 'સ્પામ';
$my_settings['others_txt'] = 'અન્ય';
$my_settings['report_this_txt'] = 'રિપોર્ટ કરો';
$my_settings['close_txt'] = 'બંધ કરો';
$my_settings['already_marked_as_offensive'] = 'વાંધાજનક તરીકે પહેલાથી જ માર્ક કરી દેવાયું છે';
$my_settings['flagged_txt'] = 'ફ્લેગ્ડ';
$my_settings['blogauthor_link'] = 'http://author.blogs.navgujaratsamay.indiatimes.com/';
$my_settings['old_blogauthor_link'] = 'http://author.blogs.navgujaratsamay.indiatimes.com/';
$my_settings['blog_link'] = 'http://blogs.navgujaratsamay.indiatimes.com/';
$my_settings['common_cookie_domain'] = 'indiatimes.com';
$my_settings['quill_lang'] = 'gujarati';
$my_settings['quill_link_1'] = 'ગુજરાતીમાં લખો (ઈન્સ્ક્રિપ્ટ)';
$my_settings['quill_link_2'] = 'ગુજરાતીમાં લખો (અંગ્રેજી અક્ષરોમાં)';
$my_settings['quill_link_3'] = 'Write in English';
$my_settings['quill_link_4'] = 'વર્ચ્યુઅલ કીબોર્ડ';
$my_settings['footer'] = '<div class="container footercontainer"><div class="insideLinks"><h2>ઊડતી નજરે...</h2><ul><li><a href="http://navgujaratsamay.indiatimes.com/ahmedabad/articlelist/25042315.cms">અમદાવાદ</a></li><li><a href="http://navgujaratsamay.indiatimes.com/gujarat/articlelist/25042236.cms">ગુજરાત</a></li><li><a href="http://navgujaratsamay.indiatimes.com/national/articlelist/25042220.cms">રાષ્ટ્રીય</a></li><li><a href="http://navgujaratsamay.indiatimes.com/international/articlelist/25042195.cms">આંતરરાષ્ટ્રીય</a></li><li><a href="http://navgujaratsamay.indiatimes.com/celebrating-gujarat/articlelist/25042167.cms">સેલિબ્રેટિંગ ગુજરાત</a></li><li><a href="http://navgujaratsamay.indiatimes.com/editorial/articlelist/25042143.cms">એડિટોરિયલ</a></li><li><a href="http://navgujaratsamay.indiatimes.com/sports/articlelist/25042115.cms">સ્પોર્ટ્સ</a></li><li><a href="http://navgujaratsamay.indiatimes.com/business/articlelist/25042076.cms">બિઝનેસ</a></li><li><a href="http://navgujaratsamay.indiatimes.com/entertainment/articlelist/25042050.cms">મનોરંજન</a></li><li><a href="http://navgujaratsamay.indiatimes.com/lifestyle/articlelist/25042019.cms">લાઇફસ્ટાઇલ </a></li><li><a href="http://navgujaratsamay.indiatimes.com/dharma-adhyatma/articlelist/25041998.cms">ધર્મ - આધ્યાત્મ </a></li><li><a href="http://navgujaratsamay.indiatimes.com/jyotish-astrology/articlelist/25041967.cms">જ્યોતિષ</a></li><li><a href="http://navgujaratsamay.indiatimes.com/photo-mazza/photolist/25057165.cms">તસવીરો બોલે છે</a></li><li><a href="http://navgujaratsamay.indiatimes.com/video/videolist/26424892.cms">વીડિયો</a></li></ul></div><div class="newsletter"><div class="fottershareLinks"><h4 style="color:#fff">હંમેશા રહો કનેક્ટેડ<br><strong> નવગુજરાત સમયની </strong> એપ સાથે</h4><div class="fotterApplinks"><a class="ios" target="_blank" href="https://itunes.apple.com/in/app/gujarati-news-navgujarat-samay/id797534457?mt=8"></a><a class="andriod" target="_blank" href="https://play.google.com/store/apps/details?id=com.ngs.reader"></a></div></div></div><div class="timesotherLinks"><h2>ટાઈમ્સ ગ્રુપની અન્ય વેબસાઈટ્સ</h2><a pg="fottgwslnk1" target="_blank" href="http://timesofindia.indiatimes.com/">Times of India</a>| <a pg="fottgwslnk2" href="http://economictimes.indiatimes.com/" target="_blank">Economic Times</a> | <a pg="fottgwslnk3" target="_blank" href="http://itimes.com/">iTimes</a>| <a pg="fottgwslnk5" target="_blank" href="http://maharashtratimes.indiatimes.com/">Marathi News</a> | <a pg="fottgwslnk6" target="_blank" href="http://eisamay.indiatimes.com/">Bangla News</a> | <a pg="fottgwslnk7" target="_blank" href="http://vijaykarnataka.indiatimes.com/">Kannada News</a>| <a pg="fottgwslnk9" target="_blank" href="http://tamil.samayam.com/">Tamil News</a> | <a pg="fottgwslnk10" target="_blank" href="http://telugu.samayam.com/">Telugu News</a> | <a pg="fottgwslnk11" target="_blank" href="http://malayalam.samayam.com/">Malayalam News</a> | <a pg="fottgwslnk12" target="_blank" href="http://www.businessinsider.in/">Business Insider</a>| <a pg="fottgwslnk13" target="_blank" href="http://zoomtv.indiatimes.com/">ZoomTv</a> | <a pg="fottgwslnk16" target="_blank" href="http://boxtv.com/">BoxTV</a>| <a pg="fottgwslnk17" target="_blank" href="http://www.gaana.com/">Gaana</a> | <a pg="fottgwslnk18" target="_blank" href="http://shopping.indiatimes.com/">Shopping</a> | <a pg="fottgwslnk19" target="_blank" href="http://www.idiva.com/">IDiva</a> | <a pg="fottgwslnk20" target="_blank" href="http://www.astrospeak.com/">Astrology</a> | <a pg="fottgwslnk21" href="http://www.simplymarry.com/" target="_blank">Matrimonial</a> | <a pg="fottgwsbs" target="_blank" href="https://play.google.com/store/apps/details?id=com.til.timesnews&amp;hl=en">Breaking News</a><span class="footfbLike">નવગુજરાત સમય ઓન ફેસબુક</span><div data-share="false" data-show-faces="false" data-action="like" data-layout="button_count" data-href="https://www.facebook.com/NavGujaratSamay/" class="fb-like" style="height: 20px;overflow: hidden;"></div></div><div class="seoLinks"><ul><li><a href="http://navgujaratsamay.indiatimes.com/" target="_blank" title="Gujarati News" style="border-left:none !important"> Gujarati News </a></li><li><a href="http://navgujaratsamay.indiatimes.com/national/articlelist/25042220.cms" target="_blank"> National News </a></li><li><a href="http://navgujaratsamay.indiatimes.com/international/articlelist/25042195.cms" target="_blank">World News</a></li><li><a href="http://navgujaratsamay.indiatimes.com/entertainment/dhollywood-news/articlelist/38993187.cms" target="_blank">Bollywood News</a></li><li><a href="http://navgujaratsamay.indiatimes.com/lifestyle/youth-of-gujarat/articlelist/44781867.cms" target="_blank">Gujarat Youth</a> </li><li> <a href="http://navgujaratsamay.indiatimes.com/sports/articlelist/25043115.cms" target="_blank"> Cricket News </a> </li><li> <a href="http://navgujaratsamay.indiatimes.com/entertainment/movie-review/moviearticlelist/25598815.cms" target="_blank"> Movie Review </a> </li><li> <a href="http://navgujaratsamay.indiatimes.com/business/share-bazaar/articlelist/25042976.cms" target="_blank"> Share Market </a> </li><li> <a href="http://navbharattimes.indiatimes.com/" target="_blank"> Hindi News </a> </li></ul><!--/footerseolinks.cmspotime:7--></div><div class="timesLinks"><a pg="fotlnk1" href="http://www.timesinternet.in/" target="_blank">About Us</a>&nbsp;|&nbsp;<a href="https://www.ads.timesinternet.in/expresso/selfservice/loginSelfService.htm" target="_blank">Create Your Own Ad</a>&nbsp;|&nbsp;<a pg="fotlnk2" href="http://advertise.indiatimes.com/" target="_blank">Advertise with Us</a>&nbsp;|&nbsp;<a pg="fotlnk3" href="http://navgujaratsamay.indiatimes.com/termsandcondition.cms" target="_blank">Terms of Use and Grievance Redressal Policy</a>&nbsp;|&nbsp;<a pg="fotlnk4" href="http://navgujaratsamay.indiatimes.com/privacypolicy.cms" target="_blank">Privacy Policy</a>&nbsp;|&nbsp;<span class="feedbackeu"><a pg="feedback" href="mailto:grievance.ngs@timesinternet.in">Feedback</a>&nbsp;|&nbsp;</span><a pg="fotlnk6" href="http://navgujaratsamay.indiatimes.com/sitemap.cms" target="_blank">Sitemap</a></div><div class="copyText">Copyright &copy;&nbsp; '.date('Y').' &nbsp;Bennett Coleman &amp; Co. Ltd. All rights reserved. For reprint rights: <a pg="fotlnk7" href="http://timescontent.com/" target="_blank">Times Syndication Service</a></div></div>';
$my_settings['footer_css'] = '<style type="text/css">
.footercontainer{background:#003d76; clear:both; overflow:hidden; padding:10px 0;}
.footercontainer a{color:#b5b3b4;}
.footercontainer h2{ color:#fff; font-size:20px; line-height:30px; padding:0 0 10px 2px;font-family: Arial, Helvetica, sans-serif;}

.insideLinks{ width:40%; padding-left:2%; float:left;}
.insideLinks ul{ list-style:none;}
.insideLinks ul li{ float:left; margin:0 10px 0 0; width:140px; }
.insideLinks ul li a{ font-family:Arial, Helvetica, sans-serif;color:#B5B3B4;font-size:14px;font-weight:bold;line-height:25px;}

.newsletter{width:30%; padding:0 2%; float:left;}
.newsletterpost {background:#CCCCCC; padding:14px; margin:10px 0 10px 0;float:left; width:300px;}

.newsletterpost input[type="text"]{background:#fff; border:1px #B7B7B7 solid; color:#333;height:21px; width:205px; font-size:14px;font-weight:bold;color:#999;float:left; padding:2px 10px;}
.newsletterpost input[type="submit"]{ background:#f5cc10; color:#000; padding:0 5px; *padding:0 4px; height:26px; cursor:pointer; border:none;font-weight:bold;float:left;}
.fottershareLinks span{ font-size:14px; color:#fff; width:100%; float:left; margin-bottom:10px;}
.fottershareLinks img{ vertical-align:middle;margin-top:5px;}
.fottershareLinks a{margin: 0 2px;}

.fotterApplinks{padding: 0 0 15px 0;}
.fotterApplinks a{padding: 0 10px 0 0; background-image:url(http://navgujaratsamay.indiatimes.com/photo/47012892.cms); float:left; width:47px; height:65px;background-repeat: no-repeat;}
.fotterApplinks a.ios{background-position:0 0}
.fotterApplinks a.ios:hover{background-position:0 0px}

.fotterApplinks a.andriod{background-position: top right;}
.fotterApplinks a.andriod:hover{background-position:top right}

.fotterApplinks a.java{background-position:0 -296px;}
.fotterApplinks a.java:hover{background-position:0 -378px;}

.fotterApplinks a.win{background-position:0 -447px}
.fotterApplinks a.win:hover{background-position:0 -526px}

.fottershareLinks{clear: both;}
.fottershareLinks h4{color:#fff; font-size:16px;line-height: 23px;}
.fottershareLinks strong{color:#f5cc10; font-weight:normal}

.footfbLike{display: block;margin: 25px 0 12px 0;font-size: 14px;color: #fff;}

.timesotherLinks{width:30%; float:left; padding-right:2%; color:#CCCCCC;font-size:12px;}
.timesotherLinks a{ font-size:11px; padding:0 1px; color:#CCCCCC; line-height:18px;}

.timesLinks{ font-family:Arial;clear:both;text-align:center; font-size:12px;color:#929292; padding:10px 0;}
.timesLinks a {color:#FDFDFD; font-family:Arial;}

.copyText{ font-family:Arial;clear:both;text-align:center; font-size:12px;color:#929292; padding:0 35px;}
.copyText a {color:#FDFDFD; font-family:Arial;}

.seoLinks{clear:both; margin:10px 0; background:#274c92; overflow:hidden;padding-left: 11px;}
.seoLinks ul{list-style:none; padding:0;}
.seoLinks ul li{float:left;}
.seoLinks ul li a{font-family:arial;padding:0 14px;color:#fff;font-size:13px; line-height:32px; display:block; float:left; border-left:#385ea6 1px solid;  border-right:#003d76 1px solid;}
@media (max-width: 767px){.insideLinks,.newsletter,.timesotherLinks,.seoLinks,.timesLinks{display: none;}}
</style>';
$my_settings['ctn_homepage'] = '';
$my_settings['ctn_homepage_rhs'] = '';
$my_settings['ctn_article_list'] = '';
$my_settings['ctn_article_list_rhs'] = '';
$my_settings['ctn_article_show_rhs'] = '';
$my_settings['ctn_article_show_end_article_1'] = '';
$my_settings['ctn_article_show_end_article_2'] = '';
