var googletag = googletag || {};
googletag.cmd = googletag.cmd || [];
(function() {
	var gads = document.createElement('script');
	gads.async = true;
	gads.type = 'text/javascript';
	var useSSL = 'https:' == document.location.protocol;
	gads.src = (useSSL ? 'https:' : 'http:') + 
	'//www.googletagservices.com/tag/js/gpt.js';
	var node = document.getElementsByTagName('script')[0];
	node.parentNode.insertBefore(gads, node);
})();
googletag.cmd.push(function() {
	var _auds = new Array();
	if(typeof(_ccaud)!='undefined') {
		for(var i=0;i<_ccaud.Profile.Audiences.Audience.length;i++)
			if(i<200)
				_auds.push(_ccaud.Profile.Audiences.Audience[i].abbr);
		}

		var _HDL = '';
		var _ARC1 = '';
		var _Hyp1 = '';
		var _article = '';
		var _tval = function(v) {
			if(typeof(v)=='undefined') return '';
			if(v.length>100) return v.substr(0,100);
			return v;
		}

		<!-- End Contextual Targeting -->
		googletag.defineSlot('/7176/Malyalam/MLM_Home/MLM_Home_Home/MLM_HP_ATF_300', [300,250], 'div-gpt-ad-8337392800902-0').addService(googletag.pubads());
		googletag.defineSlot('/7176/Malyalam/MLM_Home/MLM_Home_Home/MLM_HP_BTF_300', [300,250], 'div-gpt-ad-8337392800902-1').addService(googletag.pubads());
		googletag.defineSlot('/7176/Malyalam/MLM_Home/MLM_Home_Home/MLM_HP_BTF_728', [728,90], 'div-gpt-ad-8337392800902-2').addService(googletag.pubads());
		googletag.defineSlot('/7176/Malyalam/MLM_Home/MLM_Home_Home/MLM_HP_MTF_300', [300,250], 'div-gpt-ad-8337392800902-3').addService(googletag.pubads());
		googletag.defineSlot('/7176/Malyalam/MLM_Home/MLM_Home_Home/MLM_HP_OP_Innov1', [1,1], 'div-gpt-ad-8337392800902-4').addService(googletag.pubads());
		googletag.defineSlot('/7176/Malyalam/MLM_Home/MLM_Home_Home/MLM_HP_OP_Inter', [1,1], 'div-gpt-ad-8337392800902-5').addService(googletag.pubads());
		googletag.defineSlot('/7176/Malyalam/MLM_Home/MLM_Home_Home/MLM_HP_OP_Pop', [1,1], 'div-gpt-ad-8337392800902-6').addService(googletag.pubads());
		googletag.defineSlot('/7176/Malyalam/MLM_Home/MLM_Home_Home/MLM_HP_OP_Shosh', [1,1], 'div-gpt-ad-8337392800902-7').addService(googletag.pubads());
		googletag.defineSlot('/7176/Malyalam/MLM_Home/MLM_Home_Home/MLM_HP_EP_ATF_RHS_210', [[210, 70], [240, 85]], 'div-gpt-ad-1437483681362-0').addService(googletag.pubads());
		googletag.pubads().setTargeting('sg', _auds).setTargeting('HDL', _tval(_HDL)).setTargeting('ARC1', _tval(_ARC1)).setTargeting('Hyp1', _tval(_Hyp1)).setTargeting('article', _tval(_article));
		googletag.pubads().enableSingleRequest();
		googletag.pubads().collapseEmptyDivs();
		TimesGDPR.common.consentModule.gdprCallback(function(data) {
			if( TimesGDPR.common.consentModule.isEUuser() ) {
				googletag.pubads().setRequestNonPersonalizedAds(1);
			}
			googletag.enableServices();
		} );
	});



function showDCAdsRight1(Right1_msid,Right1_subid){
	document.write("<!-- /Malyalam/MLM_Home/MLM_Home_Home/MLM_HP_ATF_300 --><div id='div-gpt-ad-8337392800902-0'><script type='text/javascript'>googletag.cmd.push(function() { googletag.display('div-gpt-ad-8337392800902-0'); });</script></div>")
}
function showDCAdsRight2(Right2_msid,Right2_subid){
	document.write("<!-- /Malyalam/MLM_Home/MLM_Home_Home/MLM_HP_BTF_300 --><div id='div-gpt-ad-8337392800902-1' style=' height:250px;'><script type='text/javascript'>googletag.cmd.push(function() { googletag.display('div-gpt-ad-8337392800902-1'); });</script></div>")
}
function showDCAdTop(Top_msid,Top_subid){				
	document.write("<!-- /Malyalam/MLM_Home/MLM_Home_Home/MLM_HP_BTF_728 --><div id='div-gpt-ad-8337392800902-2'><script type='text/javascript'>googletag.cmd.push(function() { googletag.display('div-gpt-ad-8337392800902-2'); });</script></div>")
}
