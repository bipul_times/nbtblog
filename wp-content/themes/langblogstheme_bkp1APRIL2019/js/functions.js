  /**
   * Theme functions file
   *
   * Contains handlers for navigation, accessibility, header sizing
   * footer widgets and Featured Content slider
   *
   */
if("undefined"==typeof jQuery)throw new Error("Bootstrap's JavaScript requires jQuery");+function(t){"use strict";function n(n){return this.each(function(){var i=t(this),a=i.data("bs.tab");a||i.data("bs.tab",a=new e(this)),"string"==typeof n&&a[n]()})}var e=function(n){this.element=t(n)};e.VERSION="3.2.0",e.prototype.show=function(){var n=this.element,e=n.closest("ul:not(.dropdown-menu)"),i=n.data("target");if(i||(i=n.attr("href"),i=i&&i.replace(/.*(?=#[^\s]*$)/,"")),!n.parent("li").hasClass("active")){var a=e.find(".active:last a")[0],r=t.Event("show.bs.tab",{relatedTarget:a});if(n.trigger(r),!r.isDefaultPrevented()){var s=t(i);this.activate(n.closest("li"),e),this.activate(s,s.parent(),function(){n.trigger({type:"shown.bs.tab",relatedTarget:a})})}}},e.prototype.activate=function(n,e,i){function a(){r.removeClass("active").find("> .dropdown-menu > .active").removeClass("active"),n.addClass("active"),s?(n[0].offsetWidth,n.addClass("in")):n.removeClass("fade"),n.parent(".dropdown-menu")&&n.closest("li.dropdown").addClass("active"),i&&i()}var r=e.find("> .active"),s=i&&t.support.transition&&r.hasClass("fade");s?r.one("bsTransitionEnd",a).emulateTransitionEnd(150):a(),r.removeClass("in")};var i=t.fn.tab;t.fn.tab=n,t.fn.tab.Constructor=e,t.fn.tab.noConflict=function(){return t.fn.tab=i,this},t(document).on("click.bs.tab.data-api",'[data-toggle="tab"], [data-toggle="pill"]',function(e){e.preventDefault(),n.call(t(this),"show")})}(jQuery),+function(t){"use strict";function n(){var t=document.createElement("bootstrap"),n={WebkitTransition:"webkitTransitionEnd",MozTransition:"transitionend",OTransition:"oTransitionEnd otransitionend",transition:"transitionend"};for(var e in n)if(void 0!==t.style[e])return{end:n[e]};return!1}t.fn.emulateTransitionEnd=function(n){var e=!1,i=this;t(this).one("bsTransitionEnd",function(){e=!0});var a=function(){e||t(i).trigger(t.support.transition.end)};return setTimeout(a,n),this},t(function(){t.support.transition=n(),t.support.transition&&(t.event.special.bsTransitionEnd={bindType:t.support.transition.end,delegateType:t.support.transition.end,handle:function(n){return t(n.target).is(this)?n.handleObj.handler.apply(this,arguments):void 0}})})}(jQuery);

   ( function( $ ) {

     $( document ).ready(function() {

      $('.showdrop').hover(function() {
        var hoverClass = $(this).attr('id') + 'menu';
        $('.' + hoverClass).show();
      }, function() {
        var hoverClass = $(this).attr('id') + 'menu';
        $('.' + hoverClass).hide();
      });
      
      $('#tview').hover(function() {
        $('.tviewmenu').show();
      }, function() {
        $('.tviewmenu').hide();
      });

      $('#city').hover(function() {
        $('.citymenu').show();
      }, function() {
        $('.citymenu').hide();
      });

      $('#edit').hover(function() {
        $('.editmenu').show();
      }, function() {
        $('.editmenu').hide();
      });

      $('#home').hover(function() {
        $('.homemenu').show();
      }, function() {
        $('.homemenu').hide();
      });

      $('#bhome').hover(function() {
        $('.bhomemenu').show();
      }, function() {
        $('.bhomemenu').hide();
      });

      $('#business').hover(function() {
        $('.businessmenu').show();
      }, function() {
        $('.businessmenu').hide();
      });

      $('#roots-wings').hover(function() {
        $('.roots-wingsmenu').show();
      }, function() {
        $('.roots-wingsmenu').hide();
      });

      var isMobile = window.matchMedia("only screen and (max-width: 760px)");
      if (!isMobile.matches) {
		  var activeShareBar = '';
		  $('.feeds>.article').hover(function(e) {
			e.preventDefault(); 
			if(activeShareBar!=''){
			  activeShareBar.css('visibility','hidden');
			}
			activeShareBar = $(this).find('div.commentwrapper');
			activeShareBar.socialLikes();
			activeShareBar.css('visibility','visible');
		  });
	  }

    $('.scomments').click(function(e) { 
      // Prevent a page reload when a link is pressed
      e.preventDefault(); 
      $(window).scrollTop($('.comment-section').offset().top-20);
      $('textarea.placeholder:first').focus();
    });
  });

     function v_kalturafix(kalturaid){
      var tmpjs = document.createElement("script");
      tmpjs.type="text/javascript";
      tmpjs.src="http://timesofindia.indiatimes.com/js_kalturaapi.cms?version=1";
      document.getElementsByTagName("head")[0].appendChild(tmpjs);
      tmpjs.onreadystatechange = function () {
        getklSource('303932',kalturaid);
  //alert('me1');
  }
  tmpjs.onload = function () {
    getklSource('303932',kalturaid);
  }
  }

  $( "ul.trending li" ).each(function( index ) {
    $(this).find('[pg]').each(function( i) {
      if(pgname!=null && pgname!='')
        this.setAttribute('pg','Blog_'+curPage+'_'+pgname+'_'+this.getAttribute('pg')+'#'+parseInt(index+1));
      else 
        this.setAttribute('pg','Blog_'+curPage+'_'+this.getAttribute('pg')+'#'+parseInt(index+1));
    });
  });

  $( ".feeds>.article" ).each(function( index ) {
    $(this).find('[pg]').each(function( i) {
      if(pgname!=null && pgname!='')
        this.setAttribute('pg','Blog_'+curPage+'_'+pgname+'_'+this.getAttribute('pg')+'#'+parseInt(index+1));
      else 
        this.setAttribute('pg','Blog_'+curPage+'_'+this.getAttribute('pg')+'#'+parseInt(index+1));
    });
    //console.log( index + ": " + $( this ).text() );
  });

  $( ".featured .article" ).each(function( index ) {
    $(this).find('[pg]').each(function( i) {
      this.setAttribute('pg',this.getAttribute('pg')+'#'+parseInt(index+1));
    });
    //console.log( index + ": " + $( this ).text() );
  });

  function evtCap(b) {
    try {
      if (window.event && window.event.srcElement) {
        if (window.event.srcElement.getAttribute("pg") != null) {
          pgtrack(window.event.srcElement.getAttribute("pg"))
        }else{
          //trcode="_gaq.push(['_trackEvent','dummy','select','dummy'])";
          //eval(trcode);
        }
      } else {
        if (b && b.stopPropagation && !window.opera) {
          if (b.target.getAttribute("pg") != null) {
            pgtrack(b.target.getAttribute("pg"))
          }else{
            //trcode="_gaq.push(['_trackEvent','dummy','select','dummy'])";
            //eval(trcode);
          }
        }
      }
      chkDiv(b);
      winCloseNotiFrame()
    } catch (c) {
    }
  }

  try {
    document.onclick = evtCap
  } catch (ee) {
  }

  function pgtrack(pgatt)
  {if(pgatt!=null)
    {var pgat=pgatt.split('#');var trcode=null;
  //alert (pgat[0]);
  if(pgat.length==1)
  {trcode="_gaq.push(['_trackEvent','"+pgat[0]+"','select','"+pgat[0]+"'])";}
  else
  {
    var evarray = pgat[0].split('_');
    var ev = evarray[0]+evarray[1];
    trcode="_gaq.push(['_trackEvent','"+ev+"','select','"+pgat[0]+pgat[1]+"'])";
  }
  eval(trcode);
  }}

  $('.social-likes').on('popup_closed.social-likes', function(event, service) {
      $(event.currentTarget).socialLikes({forceUpdate: true});  // Update counters
  });




  /*    var rmid='24';
            
      if (!hasReqestedVersion && rmid==24)  /*if on ipad for kaltura videos 
      {   v_kalturafix('1_1pg7jyca');

      }
      */
  	   //  window.onload=function(){
      //       document.getElementsByClassName('media article').onMouseOver = function() { 
      //         debugger;
      //         alert(this); 
      //     }
      // }
      if ($(window).width() < 768) { $('meta[name=viewport]').attr('content','initial-scale=1.0, user-scalable=no'); }


      function gup( name )
      {
        name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
        var regexS = "[\\?&]"+name+"=([^&#]*)";
        var regex = new RegExp( regexS );
        var results = regex.exec( window.location.href );
        if( results == null )
          return "";
        else
          return results[1];
      }

      if(gup( 'source' ) == 'app'){
        $("a").attr("href", function(i, href) {
          if(href.indexOf("source=app") == -1){
            return href + '?source=app';
          }
        });
        $(".inComment").hide();
      }

	
    //var readTodayUrl = 'http://ibeatserv.indiatimes.com/iBeat/toparticleCatid.html?host='+location.hostname+'&count=5&interval=3&days=1';
    //var readWeekUrl = 'http://ibeatserv.indiatimes.com/iBeat/toparticleCatid.html?host='+location.hostname+'&count=5&interval=4&days=7';
    //var readMonthUrl = 'http://ibeatserv.indiatimes.com/iBeat/toparticleCatid.html?host='+location.hostname+'&count=5&interval=5&days=30';
   
	var readTodayUrl = 'http://ibeatserv.indiatimes.com/iBeat/toparticleCatid.html?host=IBEAT_HOST&count=5&interval=3&contentType=IBEAT_CONTENT_TYPE&days=1&k=IBEAT_KEY';
    var readWeekUrl = 'http://ibeatserv.indiatimes.com/iBeat/toparticleCatid.html?host=IBEAT_HOST&count=5&interval=4&contentType=IBEAT_CONTENT_TYPE&days=7&k=IBEAT_KEY';
    var readMonthUrl = 'http://ibeatserv.indiatimes.com/iBeat/toparticleCatid.html?host=IBEAT_HOST&count=5&interval=5&contentType=IBEAT_CONTENT_TYPE&days=30&k=IBEAT_KEY';
     
    function getDataFromIbeat (url,divId){
        if($(divId).find('li').length > 0){
          return;
        }
        jQuery.ajax({
              type: "GET",
              url: site_url+'/wp-admin/admin-ajax.php',
              dataType: "json",
              data: {
                action: "getIbeatData",
                url: url,
            },
            success: function(data, textStatus, XMLHttpRequest){
              $(divId).html(data);
            },
          error: function(MLHttpRequest, textStatus, errorThrown){
            if (window.console) {
              console.log(errorThrown)
            }
          }
      });
    }

    $('#readtabs a').click(function (e) {
      e.preventDefault();
      var href = e.target.getAttribute('href');
      switch (href){
        case '#read-today':
          getDataFromIbeat(readTodayUrl,href);
          break;
        case '#read-week':
          getDataFromIbeat(readWeekUrl,href);
          break;
        case '#read-month':
          getDataFromIbeat(readMonthUrl,href);            
          break;
      }
      $(this).tab('show');
    });

    $("ul.primary-tabs li a[href='#read']").click(function (e) {
		getDataFromIbeat(readTodayUrl,'#read-today');
    });
   
     $("#search-field").keyup(function(){
        var inputValue = $("#search-field").val().trim();
        if(inputValue.length>2){
          $(".searchdd").empty();
          $(".searchdd").append('<li class="loading"><a>Fetching Results ... <a></li>');
          $(".searchdd").show();
          jQuery.ajax({
              type: "GET",
              url: site_url+ "/wp-admin/admin-ajax.php",
              dataType: "json",
              data: {
                action: "getSearchData",
                input: inputValue,
              },
            success: function(data, textStatus, XMLHttpRequest){
                $(".searchdd").empty();
                if(data.length>0){
                  for (var i in data ){
                   var name = (data[i].name).replace(new RegExp(inputValue, 'gi'), '<u>$&</u>');
                   $(".searchdd").append('<li><a href="' + data[i].link + '">' + name + '</a></li>');
                  }
                }else{
                   $(".searchdd").append('<li class="loading"><a>No results found <a></li>');        
                }
            },
            error: function(MLHttpRequest, textStatus, errorThrown){
              if (window.console) {
                console.log(errorThrown)
              }
              $(".searchdd.loading").html('<a>Something went wrong, try again <a>'); 
            }
        });
      }else{
        $(".searchdd").empty();
        $(".searchdd").hide();
      }
    });
    
})( jQuery );




