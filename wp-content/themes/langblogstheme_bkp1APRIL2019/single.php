<?php
/**
 * The Template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<div class="container">
	<div class="row">
		<div class="col-md-8">
			<?php
			$authorName = '';
			$categories;
				// Start the Loop.
			while ( have_posts() ) : the_post();

					/*
					 * Include the post format-specific template for the content. If you want to
					 * use this in a child theme, then include a file called called content-___.php
					 * (where ___ is the post format) and that will be used instead.
					 
					$format = get_post_format();
					if ( false === $format )
						$format = 'post'; 
					*/
						//twentyfourteen_post_thumbnail();
						get_template_part( 'content','post');

					// Previous/next post navigation.
				 	// twentyfourteen_post_nav();
					// If comments are open or we have at least one comment, load up the comment template.
						comments_template();
					/*if ( comments_open()) {
						comments_template();
					}else{
						echo 'Further Commenting is Disabled';
					}*/
					endwhile;
                    $name = get_the_author_meta( 'display_name' );

				?>					
				</div>
				<div class="col-md-4 sidebar">
					<div class="panel authors web-authors">
						<div class="panel-heading">
							<h3 class="panel-title adj-font-tabs"><?php echo $my_settings['author_txt']; ?></h3>
						</div>
						<div class="panel-body">
							<a class="pull-left" pg="Blog_Article_AuthorProfile" title="<?php echo $my_settings['go_to_the_profile_of_txt']; ?> <?php echo $name; ?>" href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' )) ?>">
								<img class="media-object" src="<?php  echo get_user_avatar(get_the_author_meta('ID')); ?>" alt="<?php echo $name; ?>">
							</a>
							<a class="media-heading" pg="Blog_Article_AuthorProfile" title="<?php echo $my_settings['go_to_the_profile_of_txt']; ?> <?php echo $name; ?>" href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' )) ?>" rel="bookmark"><b><?php echo $name; ?></b></a>
						<?php 
						$desc = get_the_author_meta( 'description' );
						if(!empty($desc)){
							?>
							<div class="bio">
								<div id="showmore" style="display:none">
									<?php echo $desc.'<br><br>';?>    				
								</div>	
								<div id="showless">
									<?php 
									if(strlen ($desc) > 156){
										echo substr($desc, 0, strpos($desc, ' ', 156));
										echo '. . .';
									}else{
										echo $desc;
									}
									echo '<br><br>';
									?>    				
								</div>    		
								<?php if(strlen($desc) > 161){ ?>
								<a href="javascript:void(0)" pg="Blog_Article_MoreButton" id="more-button"><?php echo $my_settings['more_txt']; ?></a>
								<a href="javascript:void(0)" pg="Blog_Article_LessButton" id="less-button" style="display:none"><?php echo $my_settings['less_txt']; ?></a>
								<script>
								var a1 = document.getElementById("more-button");
								a1.onclick = function() {
									$("#showmore").show();
									$("#showless").hide();	
									$("#less-button").show();
									$("#more-button").hide();	
									return false;
								}
								var a2 = document.getElementById("less-button");
								a2.onclick = function() {
									$("#showmore").hide();
									$("#showless").show();
									$("#less-button").hide();
									$("#more-button").show();	
									return false;
								}
								</script>
								<?php } ?>
							</div>
							<hr style="margin:0px">
							<?php 
						}
						?>
						</div>
					</div>
					<div>
						<div class="panel">
							<div class="addwrapper">
								<?php echo $my_settings['google_ad_right_1']; ?>
							</div>
						</div>
					</div>
		    <?php include 'sidebar.php' ?>
		</div>
	</div>
</div> <!--container -->
	<?php
	get_footer();
