<?php get_header(); ?>
<?php global $my_settings; ?>
<style>
#navigation {
background: #ebeedd;
position: relative;
display: none;
}

.navbar-btn {
display: block;
background: #3d9ae8 !important;
height: 35px;
}
.skip-link{position: absolute;right: 4px;top:6px;z-index: 100;}
.style_nbt .navbar-btn{background: #f5cc10 !important;}
.navbar-btn a {
background-color: #3d9ae8 !important;
width: 40px;
display: block;
text-indent: -2000px;
overflow: hidden;
padding: 5px 0px 0px;
margin: 0;
float: left;
}
.style_nbt .navbar-btn a{background: #f5cc10 !important;}
.navbar-btn hr{
  height: 3px;
  background: white;
  border: white;
  margin: 4px 6px;
}
.mobile_blog_head{color: white;padding-top: 3px;display: block;font-size: 20px;font-weight: bold;font-family: arial sans-serif;margin-left: 40px;width: auto;}
.style_nbt .mobile_blog_head{color: #000;}

.style_eisamay .navbar-btn{background: #f9a11b !important;}
.style_eisamay .navbar-btn a{background: #f9a11b !important;}
.style_eisamay .mobile_blog_head{color: #000;}

.style_vk .navbar-btn{background: #000 !important;}
.style_vk .navbar-btn a{background: #000 !important;}
.style_vk .mobile_blog_head{color: #fff;}
.style_vk .skip-link{color: #fff;}

.style_mt .skip-link{color: #fff;}

.style_ml .navbar-btn{background: #00695c !important;}
.style_ml .navbar-btn a{background: #00695c !important;}
.style_ml .mobile_blog_head{color: #fff;}
.style_ml .skip-link{color: #fff;}

.style_tm .navbar-btn{background: #bf4707 !important;}
.style_tm .navbar-btn a{background: #bf4707 !important;}
.style_tm .mobile_blog_head{color: #fff;}
.style_tm .skip-link{color: #fff;}

.style_tl .navbar-btn{background: #c00200 !important;}
.style_tl .navbar-btn a{background: #c00200 !important;}
.style_tl .mobile_blog_head{color: #fff;}
.style_tl .skip-link{color: #fff;}
</style>

<div class="container" id="navigation">
  <div class="row">
    <div class="navbar-btn">
      <span class="mobile_blog_head"><?php echo $my_settings['footer_logo_txt'].' '.$my_settings['blogs_txt']; ?></span>
    </div>
	<a class="skip-link" onClick="window.location.replace(prevURL);"><strong>SKIP</strong></a>
  </div>
</div>
<script type="text/javascript">var prevURL = _getCookie('prevurl');</script>
<div class="container featured">
	<div class="row">
		<div class="col-md-4" data-vr-contentbox="">
			<div class="article media">
				<span id="intCount" style="color:#2b2b2b;">Site will load in few seconds.</span>
				<div style="display:block;width:305px; margin:auto;">
					<?php echo get_option('wap_ad_content'); ?>			
				</div>
			</div>
		</div>
	</div>
</div> <!--container -->
<script type="text/javascript">                
	if(_getCookie('intAd') === '1'){
		window.location.replace(prevURL);
	}
	else{
		_setCookie('intAd','1',1);
		// set interval
		var countdownElement = document.getElementById('intCount'),
			seconds = 7,
			second = 0,
			interval;
	
		interval = setInterval(function() {
			countdownElement.firstChild.data = 'Site will load in ' + (seconds - second) + ' seconds.';
			if (second >= seconds) {
				clearInterval(interval);
				window.location.replace(prevURL);
			}
			second++;
		}, 700);
	}
	
</script>
<?php
	get_footer();
