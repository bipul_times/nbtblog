<?php
  /**
   * The main template file
   *
   * This is the most generic template file in a WordPress theme and one
   * of the two required files for a theme (the other being style.css).
   * It is used to display a page when nothing more specific matches a query,
   * e.g., it puts together the home page when no home.php file exists.
   *
   * @link http://codex.wordpress.org/Template_Hierarchy
   *
   * @package WordPress
   * @subpackage BlogsTheme
   * @since Blogs Theme 1.0
   */
  
  get_header();
  $currentPage = (get_query_var('paged')) ? get_query_var('paged') : 1;
  if (is_front_page() && $currentPage == 1) {
      // Include the featured content template.
      get_template_part( 'featured-content' );
  }
  ?>
<script type='text/javascript'>
curPage = 'Home';
</script>
<div class="container">
<div class="row">
  <div class="col-md-8 feeds" data-vr-zone="feeds">
    <?php
    //echo 'here';
    if (is_front_page() && $currentPage == 1) {
		if(array_key_exists('esi_status',$my_settings) && $my_settings['esi_status'] == 'TEST' && isset($_GET['isEsi']) && $_GET['isEsi'] == 'YES'){
			echo $my_settings['esi_homepage'];
		}else if(array_key_exists('esi_homepage',$my_settings) && array_key_exists('esi_status',$my_settings) && $my_settings['esi_status'] == 'LIVE'){
			echo $my_settings['esi_homepage'];
		}else{
			echo $my_settings['ctn_homepage'];
		}
		
      // Include the featured content template.
      $featured_posts = twentyfourteen_get_featured_posts();
     // print_r($featured_posts);
      $i = -1;
      foreach ( (array) $featured_posts as $order => $post ) :
        $i++;
        if($i<3){
          continue;
        }
        if($i == 13){
          break;
        }
        setup_postdata( $post );
        // Include the featured content template.
        get_template_part( 'content',get_post_format());
      endforeach;

    /**
     * Fires after the Twenty Fourteen featured content.
     *
     * @since Twenty Fourteen 1.0
     */
    //do_action( 'twentyfourteen_featured_posts_after' );

    wp_reset_postdata();

    } else {
		if(array_key_exists('esi_status',$my_settings) && $my_settings['esi_status'] == 'TEST' && isset($_GET['isEsi']) && $_GET['isEsi'] == 'YES'){
			echo $my_settings['esi_article_list'];
		}else if(array_key_exists('esi_article_list',$my_settings) && array_key_exists('esi_status',$my_settings) && $my_settings['esi_status'] == 'LIVE'){
			echo $my_settings['esi_article_list'];
		}else{
			echo $my_settings['ctn_article_list'];
		}
	}

      if (have_posts()):
      // Start the Loop.
          while (have_posts()):
              the_post();
              /*
               * Include the post format-specific template for the content. If you want to
               * use this in a child theme, then include a file called called content-___.php
               * (where ___ is the post format) and that will be used instead.
               */
              get_template_part('content', get_post_format());
          endwhile;
          // Previous/next post navigation.
          twentyfourteen_paging_nav();
      else:
          // If no content, include the "No posts found" template.
          get_template_part('content', 'none');
      endif;
      ?>
  </div>
  <div class="col-md-4 sidebar">
    <div class="panel">
      <div class="addwrapper">
        <?php echo $my_settings['google_ad_right_1']; ?>
      </div>
    </div>
    <?php include 'sidebar.php' ?>
  </div>
</div>
</div>
<!-- .container -->
<?php
get_footer();
