<?php
  /**
   * The main template file
   *
   * This is the most generic template file in a WordPress theme and one
   * of the two required files for a theme (the other being style.css).
   * It is used to display a page when nothing more specific matches a query,
   * e.g., it puts together the home page when no home.php file exists.
   *
   * @link http://codex.wordpress.org/Template_Hierarchy
   *
   * @package WordPress
   * @subpackage BlogsTheme
   * @since Blogs Theme 1.0
   */
  
  get_header();
  ?>
<div class="container">
<div class="row">
  <div class="col-md-8">
		<?php if ( have_posts() ) : ?>
		<header class="page-header">
			<h1 class="page-title"><?php echo single_tag_title( '', false ); ?><?php //printf( __( 'Tag Archives: %s', 'twentyfourteen' ), single_tag_title( '', false ) ); ?> <a href="<?php echo $_SERVER["REQUEST_URI"].'feed/rss2' ?>" target="_blank" style="outline:none;"> <img src="<?php echo get_template_directory_uri().'/images/rss.png' ?>" width="15px" alt="RSS" title="Subscribe to RSS"> </a></h1>
		</header><!-- .archive-header -->
		<div class="feeds">
    <?php
          while (have_posts()):
              the_post(); 
              /*
               * Include the post format-specific template for the content. If you want to
               * use this in a child theme, then include a file called called content-___.php
               * (where ___ is the post format) and that will be used instead.
               */
              get_template_part('content', get_post_format());
          endwhile;
          // Previous/next post navigation.
          twentyfourteen_paging_nav();
      else:
          // If no content, include the "No posts found" template.
          get_template_part('content', 'none');
      endif;
      ?>
    </div>
  </div>
  <div class="col-md-4 sidebar">
    <div class="panel">
      <div class="addwrapper">
        <?php echo $my_settings['google_ad_right_1']; ?>
      </div>
    </div>
    <?php include 'sidebar.php' ?>
  </div>
</div>
</div>
<!-- .container -->
<?php
get_footer();
