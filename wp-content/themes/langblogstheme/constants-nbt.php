<?php 
$randomNumber = rand();
define('Z_IMAGE_PLACEHOLDER', get_template_directory_uri()."/images/placeholder.png");
define('FB_PLACEHOLDER', get_template_directory_uri()."/images/blog-nbt-facebook.png");
define('Z_AUTHOR_PLACEHOLDER', get_template_directory_uri()."/images/50.jpg");
define('RECO_URL','http://reco.indiatimes.com/Reco/RecoIndexer?eventType=update&hostid=1b&contentType=blog&msid=');
define('COMMENTS_URL','http://www.nbtblogcmtapi.indiatimes.com/');
define('RCHID', '2147478026');
if(getenv("environment")=='live'){
    define('MYTIMES_URL','https://mytpvt.indiatimes.com/');
}else{
    define('MYTIMES_URL','https://mytest.indiatimes.com/');
}

define('MYT_COMMENTS_API_KEY', 'NBTB');

global $my_settings;
$my_settings['site_lang'] = 'hi';
$my_settings['og_locale'] = 'hi_IN';
$my_settings['google_site_verification'] = 'ZgFICIedNvVZl5pV9EfAUeenwta9vBY0Za_GgmV4zuw';
$my_settings['fblikeurl'] = 'https://www.facebook.com/navbharattimes';
$my_settings['favicon'] = 'https://navbharattimes.indiatimes.com/icons/nbtfavicon.ico';
$my_settings['ga'] = ""; // Moved to Hook wp_head
//$my_settings['fbappid'] = '127963980560368';
$my_settings['fbappid'] = '117787264903013';
$my_settings['channel_url_part'] = 'channel=nbt';
$my_settings['main_site_url'] = 'https://navbharattimes.indiatimes.com';
$my_settings['main_site_txt'] = 'NavBharat Times Blog';
$my_settings['fb_url'] = 'https://www.facebook.com/navbharattimes';
$my_settings['twitter_url'] = 'https://twitter.com/navbharattimes';
$my_settings['twitter_handle'] = 'navbharattimes';
$my_settings['google_url'] = 'https://plus.google.com/+navbharattimes/';
$my_settings['rss_url'] = 'https://navbharattimes.indiatimes.com/rssfeedsdefault.cms';
$my_settings['logo_title'] = 'NBT Blogs';
$my_settings['logo_url'] = 'https://navbharattimes.indiatimes.com/photo/40174796.cms';
$my_settings['footer_logo_txt'] = 'नवभारत टाइम्स';
$my_settings['comscore_tag'] = '<!-- Begin comScore Tag -->
<script>
  var _comscore = _comscore || [];
  var objComScore = { c1: "2", c2: "6036484" };
  TimesGDPR.common.consentModule.gdprCallback(function(data) {
		if( TimesGDPR.common.consentModule.isEUuser() ){
			objComScore["cs_ucfr"] = 0;
		}
		_comscore.push(objComScore);
  
	(function() {
		var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
		s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";
		el.parentNode.insertBefore(s, el);
	})();
	});
  
   
</script>
<noscript><img src="http://b.scorecardresearch.com/p?c1=2&amp;c2=6036484&amp;cv=2.0&amp;cj=1"></noscript>
<!-- End comScore Tag -->';
$my_settings['ibeat_channel'] = 'NbtBlog';
$my_settings['ibeat_host'] = 'blogs.navbharattimes.indiatimes.com';
$my_settings['ibeat_key'] = '34b3c4d8455dc5bdd5c8c644d326c945';
$my_settings['ibeat_domain'] = 'navbharattimes.indiatimes.com';
$my_settings['mrec_adsPosition'] = [ '4'];
foreach($my_settings['mrec_adsPosition'] as $adsPos){
    if(isMobile()){
        $my_settings['mrec_adsId'][$adsPos] = 'div-gpt-ad-1574909362719-'.$adsPos;
    }else{
        $my_settings['mrec_adsId'][$adsPos] = 'div-gpt-ad-1343039310577-'.$adsPos;
    }
}

$catName =  getCatFromURL($_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
//div-gpt-ad-1343039310577-4
//$my_settings['footer_dmp_tag'] = 'https://static.clmbtech.com/ase/2310/16/aa.js';
if(isMobile()){
    $my_settings['footer_dmp_tag'] = 'https://static.clmbtech.com/ase/2310/49/aa.js';
}else{
    $my_settings['footer_dmp_tag'] = 'https://static.clmbtech.com/ase/2310/16/aa.js';
}
$adsSlot = "";
if(isMobile()){    
    $adsSlot .= "googletag.defineSlot('/7176/NBT_MWeb/NBT_MWeb_Blog/NBT_MWeb_Blog_ATF', [320, 50], 'div-gpt-ad-1574909264418-0').addService(googletag.pubads());";
    $adsSlot .= "googletag.defineSlot('/7176/NBT_MWeb/NBT_MWeb_Blog/NBT_MWeb_Blog_FBN', [320, 50], 'div-gpt-ad-1574909318428-0').addService(googletag.pubads());";
    $adsSlot .= "googletag.defineSlot('/7176/NBT_MWeb/NBT_MWeb_Blog/NBT_MWeb_Blog_Mrec', [300, 250], 'div-gpt-ad-1574909362719-4').addService(googletag.pubads());";
    $adsSlot .= "googletag.defineSlot('/7176/NBT_MWeb/NBT_MWeb_Blog/NBT_MWeb_Blog_Mrec', [300, 250], 'div-gpt-ad-1343039310577-0').addService(googletag.pubads());";
    $adsSlot .= "googletag.defineSlot('/7176/NBT_MWeb/NBT_MWeb_Blog/NBT_MWeb_Blog_Mrec', [300, 250], 'div-gpt-ad-1343039310577-2').addService(googletag.pubads());";    
}else{
    $adsSlot .= "googletag.defineOutOfPageSlot('/7176/Navbharattimes/NBT_Blog/NBT_Blog_ROS_OOP_INNOV1', 'div-gpt-ad-1574943016563-0').addService(googletag.pubads());";
    $adsSlot .= "googletag.defineSlot('/7176/Navbharattimes/NBT_Blog/NBT_ROS_ATF_BLG_NW_AS_728', [[970, 250], [728, 90], [980, 200]], 'div-gpt-ad-1343039310577-1').addService(googletag.pubads());";
    $adsSlot .= "googletag.defineSlot('/7176/Navbharattimes/NBT_Blog/NBT_ROS_ATF_BLG_NW_AS_300', [[300, 250], [300, 600]], 'div-gpt-ad-1343039310577-0').addService(googletag.pubads());";
    $adsSlot .= "googletag.defineSlot('/7176/Navbharattimes/NBT_Blog/NBT_ROS_ATF_BLG_NW_AS_300', [[300, 250], [300, 600]], 'div-gpt-ad-1343039310577-2').addService(googletag.pubads());";
    $adsSlot .= "googletag.defineSlot('/7176/Navbharattimes/NBT_Blog/NBT_ROS_ATF_BLG_NW_AS_300', [[300, 250], [300, 600]], 'div-gpt-ad-1343039310577-4').addService(googletag.pubads());"; 
    
}
$my_settings['footer_google_tags_mobile'] = "<!-- /7176/NBT_MWeb/NBT_MWeb_Blog/NBT_MWeb_Blog_FBN -->
<div class='sticky_ads'><div id='div-gpt-ad-1574909318428-0' style='width: 320px; height: 50px;'>
  <script>
    googletag.cmd.push(function() { googletag.display('div-gpt-ad-1574909318428-0'); });
  </script>
</div>
</div>";
$my_settings['footer_google_tag'] = "
<!-- /7176/Navbharattimes/NBT_Blog/NBT_Blog_ROS_OOP_INNOV1  -->
<div id='div-gpt-ad-1574943016563-0'>
  <script>
    googletag.cmd.push(function() { googletag.display('div-gpt-ad-1574943016563-0'); });
  </script>
</div> 
";

 


global $wp;

$my_settings['google_tag_js_head'] = "
<script type='text/javascript' src='https://ade.clmbtech.com/cde/aef/var=colaud?cid=2310:49&fpc=".$_COOKIE['_col_uuid']."&optout=1&dsmi=1&_u=".home_url( $wp->request )."'></script>
<script type='text/javascript'>
	var googletag = googletag || {};
	googletag.cmd = googletag.cmd || [];
	(function() {
	var gads = document.createElement('script');
	gads.async = true;
	gads.type = 'text/javascript';
	var useSSL = 'https:' == document.location.protocol;
	gads.src = (useSSL ? 'https:' : 'http:') + 
	'//www.googletagservices.com/tag/js/gpt.js';
	var node = document.getElementsByTagName('script')[0];
	node.parentNode.insertBefore(gads, node);
	})();

	function setCookie(name,value,days) {
		var expires = '';
		if (days) {
			var date = new Date();
			date.setTime(date.getTime() + (days*24*60*60*1000));
			expires = '; expires=' + date.toUTCString();
		}
		document.cookie = name + '=' + (value || '')  + expires + '; path=/';
	}
	function getCookie(name) {
		var nameEQ = name + '=';
		var ca = document.cookie.split(';');
		for(var i=0;i < ca.length;i++) {
			var c = ca[i];
			while (c.charAt(0)==' ') c = c.substring(1,c.length);
			if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
		}
		return null;
	}

	</script>
	
	<script type='text/javascript'>
	googletag.cmd.push(function() {
	    

	<!-- Audience Segment Targeting -->
	var _auds = '';
	if(typeof(colaud)!='undefined') {
		_auds = colaud.aud;
		setCookie('colaud', _auds, 365)		
	}else{
		if(getCookie('colaud')!='undefined'){
			_auds = getCookie('colaud');
		}
	}
	<!-- End Audience Segment Targeting -->
	
	<!-- Contextual Targeting -->
	var _HDL = '';
	var _ARC1 = '';
	var _Hyp1 = '';
	var _article = '';
	var _tval = function(v) {
	if(typeof(v)=='undefined') return '';
	if(v.length>100) return v.substr(0,100);
	return v;
	}
	<!-- End Contextual Targeting -->

".$adsSlot."
googletag.pubads().setTargeting('sg', _auds).setTargeting('HDL', _tval(_HDL)).setTargeting('ARC1', _tval(_ARC1)).setTargeting('Hyp1', _tval(_Hyp1)).setTargeting('article', _tval(_article));
googletag.pubads().setTargeting( 'section', '".$catName."' )
googletag.pubads().enableSingleRequest();
googletag.pubads().collapseEmptyDivs();

	TimesGDPR.common.consentModule.gdprCallback(function(data) {
		if( TimesGDPR.common.consentModule.isEUuser() ) {
			googletag.pubads().setRequestNonPersonalizedAds(1);
		}
		googletag.enableServices();
	} );
});
</script>";
$my_settings['google_masthead_ad_mweb'] = "<!-- /7176/NBT_MWeb/NBT_MWeb_Others/NBT_Mweb_OTH_Fixed -->
<div id='div-gpt-ad-1556260975448-0'>
<script>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1556260975448-0'); });
</script>
</div>";

$my_settings['google_masthead_ad_web'] = "<!-- /7176/Navbharattimes/NBT_BUDGET_2018/NBT_HP_ATF_BAND_ELE18_1000x70 -->
<div id='div-gpt-ad-1556260793019-0'>
<script>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1556260793019-0'); });
</script>
</div>";
$my_settings['google_mobile_atf_ads'] = "<!-- /7176/NBT_MWeb/NBT_MWeb_Blog/NBT_MWeb_Blog_ATF -->
<div id='div-gpt-ad-1574909264418-0' style='width: 320px; height: 50px;'>
  <script>
    googletag.cmd.push(function() { googletag.display('div-gpt-ad-1574909264418-0'); });
  </script>
</div>";
$my_settings['google_ad_top'] = "<!-- /7176/Navbharattimes/NBT_Home/NBT_Home_Blogs_AL/NBT_ROS_ATF_HOM_BLG_AL_728 -->
<div id='div-gpt-ad-1343039310577-1'>
<script type='text/javascript'>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1343039310577-1'); });
</script>
</div>";
$my_settings['google_ad_right_1'] = "<!-- /7176/Navbharattimes/NBT_Home/NBT_Home_Blogs_AL/NBT_ROS_ATF_HOM_BLG_AL_300 -->
<div id='div-gpt-ad-1343039310577-0'>
<script type='text/javascript'>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1343039310577-0'); });
</script>
</div>";
$my_settings['google_ad_right_2'] = "<!-- /7176/Navbharattimes/NBT_Home/NBT_Home_Blogs_AL/NBT_ROS_BTF_HOM_BLG_AL_300 -->
<div id='div-gpt-ad-1343039310577-2'>
<script type='text/javascript'>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1343039310577-2'); });
</script>
</div>";
$my_settings['visual_revenue_reader_response_tracking_script'] = "";
$my_settings['visual_revenue_reader_response_tracking_script_for_not_singular'] = "";
$my_settings['not_found_heading'] = 'कोई रिज़ल्ट नहीं मिला';
$my_settings['not_found_txt'] = 'क्षमा करें, लेकिन आपके खोज शब्दों से मिलता हुआ कुछ भी नहीं मिला। कृपया कुछ अलग कीवर्ड के साथ फ़िर से कोशिश करें';
$my_settings['search_placeholder_txt'] = '3 अक्षर टाइप करे';
$my_settings['go_to_the_profile_of_txt'] = '';
$my_settings['go_to_txt'] = '';
$my_settings['share_link_fb_txt'] = 'फेसबुक पर लिंक शेयर करे';
$my_settings['share_link_twitter_txt'] = 'ट्विटर पर लिंक शेयर करे';
$my_settings['share_link_linkedin_txt'] = 'लिंक्डइन पर लिंक शेयर करे';
$my_settings['mail_link_txt'] = 'मेल पर लिंक शेयर करे';
$my_settings['read_complete_article_here_txt'] = 'यहाँ पूरा लेख पढ़ें';
$my_settings['featured_txt'] = 'विशेष';
$my_settings['disclaimer_txt'] = 'डिसक्लेमर';
$my_settings['disclaimer_on_txt'] = 'नवभारत टाइम्स में छपने वाले लेख, कॉलम, लेखकों के दृष्टिकोण, विचार आपको इस ब्लॉग में पढ़ने के लिए मिलेंगे।';
$my_settings['disclaimer_off_txt'] = 'ऊपर व्यक्त विचार लेखक के अपने हैं';
$my_settings['most_discussed_txt'] = 'सबसे चर्चित पोस्ट';
$my_settings['more_txt'] = 'और';
$my_settings['less_txt'] = 'कम';
//$my_settings['login_txt'] = '';
//$my_settings['logout_txt'] = '';
$my_settings['login_txt'] = 'Log In';
$my_settings['logout_txt'] = 'Log Out';
$my_settings['view_all_posts_in_txt'] = '';
$my_settings['home_txt'] = 'होम';
$my_settings['blogs_txt'] = 'ब्लॉग्स';
$my_settings['search_results_for_txt'] = 'सर्च रिज़ल्ट';
$my_settings['most_read_txt'] = 'सुपरहिट पोस्ट';
$my_settings['popular_tags_txt'] = 'टॉपिक से खोजें';
$my_settings['recently_joined_authors_txt'] = 'नए लेखक';
$my_settings['like_us_txt'] = 'हमें Like करें';
$my_settings['author_txt'] = ' लेखक';
$my_settings['popular_from_author_txt'] = 'लेखक की लोकप्रिय पोस्ट';
$my_settings['search_authors_by_name_txt'] = 'नाम से लेखक खोजें';
$my_settings['search_txt'] = 'खोजे';
$my_settings['back_to_authors_page_txt'] = 'वापस लेखक के पेज पर';
$my_settings['no_authors_found_txt'] = 'कोई लेखक नही मिला';
$my_settings['further_commenting_is_disabled_txt'] = 'अब कॉमेंट्स नही किये जा सकते';
$my_settings['comments_on_this_post_are_closed_now_txt'] = 'इस पोस्ट पर कॉमेंट बंद कर दिये गये है';
$my_settings['add_your_comment_here_txt'] = 'अपना कॉमेंट लिखे';
$my_settings['characters_remaining_txt'] = 'अक्षर बचे है';
$my_settings['share_on_fb_txt'] = 'फेसबुक पर शेयर करे';
$my_settings['share_on_twitter_txt'] = 'ट्विटर पर शेयर करे';
$my_settings['sort_by_txt'] = 'सॉर्ट बाइ';
$my_settings['newest_txt'] = 'सबसे नया';
$my_settings['oldest_txt'] = 'सबसे पुराने';
$my_settings['discussed_txt'] = 'सबसे ज्यादा चर्चित';
$my_settings['up_voted_txt'] = 'इस कॉमेंट से सहमत';
$my_settings['down_voted_txt'] = 'इस कॉमेंट से असहमत';
$my_settings['be_the_first_one_to_review_txt'] = 'सबसे पहले कॉमेंट करे';
$my_settings['more_points_needed_to_reach_next_level_txt'] = 'अगले स्तर तक पहुँचने के लिये अधिक अंक की जरूरत';
$my_settings['know_more_about_times_points_txt'] = 'जानें, क्या है Times Points';
$my_settings['know_more_about_times_points_link'] = 'http://www.timespoints.com/about/7341341299842220032';
$my_settings['badges_earned_txt'] = 'अब तक जीते मेडल';
$my_settings['just_now_txt'] = 'अभी';
$my_settings['follow_txt'] = 'फॉलो करें';
$my_settings['reply_txt'] = 'जवाब दें';
$my_settings['flag_txt'] = 'शिकायत करें';
$my_settings['up_vote_txt'] = 'सहमत';
$my_settings['down_vote_txt'] = 'असहमत';
$my_settings['mark_as_offensive_txt'] = 'यह कॉमेंट आपत्तिजनक है';
$my_settings['find_this_comment_offensive_txt'] = 'क्या आपको इस कॉमेंट पर एतराज़ है?';
$my_settings['reason_submitted_to_admin_txt'] = 'आपका बताया हुआ कारण ऐडमिन को भेज दिया गया है।';
$my_settings['choose_reason_txt'] = 'नीचे के टेक्स्ट बॉक्स में कारण लिखिए और फिर भेज दें पर क्लिक करे। इससे हमारे मॉडरेटर के पास सूचना पहुंच जाएगी और वह उचित कार्रवाई कर सकेंगे।';
$my_settings['reason_for_reporting_txt'] = 'आपत्ति का कारण';
$my_settings['foul_language_txt'] = 'अभद्र भाषा';
$my_settings['defamatory_txt'] = 'अपमान करनेवाला';
$my_settings['inciting_hatred_against_certain_community_txt'] = 'किसी कम्युनिटी के खिलाफ घृणा फैलाना';
$my_settings['out_of_context_spam_txt'] = 'संदर्भ से बाहर / स्पैम';
$my_settings['others_txt'] = ' अन्य';
$my_settings['report_this_txt'] = 'भेज दें';
$my_settings['close_txt'] = 'बंद करें';
$my_settings['already_marked_as_offensive'] = 'आप इस कॉमेंट को आपत्तिजनक बता चुके हैं!';
$my_settings['flagged_txt'] = 'Flagged';
$my_settings['blogauthor_link'] = 'http://author-blogs-navbharattimes.indiatimes.com/';
$my_settings['old_blogauthor_link'] = 'http://author.blogs.navbharattimes.indiatimes.com/';
$my_settings['blog_link'] = 'https://blogs.navbharattimes.indiatimes.com/';
$my_settings['common_cookie_domain'] = 'indiatimes.com';
$my_settings['view_comments'] ='कॉमेंट्स देखें';
$my_settings['all_comments'] ='कॉमेंट्स';
$my_settings['quill_lang'] = 'hindi';
$my_settings['quill_link_1'] = 'हिंदी में लिखें (इन्स्क्रिप्ट)';
$my_settings['quill_link_2'] = 'हिंदी में लिखें (अंग्रेज़ी अक्षरों में)';
$my_settings['quill_link_3'] = 'Write in English';
$my_settings['quill_link_4'] = 'वर्चुअल कीबोर्ड';
$my_settings['offensive_comment_warning'] = 'कॉमेंट में किसी भी तरह की आपत्तिजनक भाषा का प्रयोग न करें। अभद्र भाषा, किसी की मानहानि, निजी हमला, भड़काऊ कॉमेंट या किसी संप्रदाय के खिलाफ द्वेष फैलाने वाले कॉमेंट पोस्ट न करें। इन निर्देशों को न मानने वाले कॉमेंट्स को साइट से हटाने में हमारी मदद करें और ऐसे कॉमेंट्स को आपत्तिजनक मार्क करें। साइट पर साफ-सुथरे वार्तालाप को बढ़ाने में हमारा सहयोग करें।';
$my_settings['footer'] = '<div class="container footercontainer"><div class="insideLinks"><h2>खबरें एक झलक में</h2><ul><li><a href="http://navbharattimes.indiatimes.com/india/articlelist/1564454.cms" pg="fotkjlnk1" target="_blank">भारत</a></li><li><a href="http://navbharattimes.indiatimes.com/sports/cricket.cms" target="_blank" pg="fotkjlnk9">खेल </a></li><li><a href="http://blogs.navbharattimes.indiatimes.com/" pg="fotkjlnk22" target="_blank">NBT ब्लॉग</a></li><li><a href="http://navbharattimes.indiatimes.com/metro/delhidefault.cms" pg="fotkjlnk3" target="_blank">दिल्ली</a></li><li><a href="http://navbharattimes.indiatimes.com/movie-masti/movies/2279793.cms" target="_blank" pg="fotkjlnk10">मूवी-मस्ती</a></li><li><a href="http://readerblogs.navbharattimes.indiatimes.com/" target="_blank" pg="fotkjlnk23">अपना ब्लॉग</a></li><li><a href="http://navbharattimes.indiatimes.com/metro/mumbaihome.cms" pg="fotkjlnk4" target="_blank">मुंबई</a></li><li><a href="http://navbharattimes.indiatimes.com/jokes.cms" target="_blank" pg="fotkjlnk11">जोक्स </a></li><li><a href="http://navbharattimes.indiatimes.com/other/home-and-relations/articlelist/2354729.cms" pg="fotkjlnk14" target="_blank">घर-परिवार</a></li><li><a href="http://navbharattimes.indiatimes.com/metro/lucknowdefault.cms" pg="fotkjlnk5" target="_blank">लखनऊ</a></li><li><a href="http://navbharattimes.indiatimes.com/tech.cms" target="_blank" pg="fotkjlnk15">टेक</a></li><li><a href="http://photogallery.navbharattimes.indiatimes.com/" target="_blank" pg="fotkjlnk17">फोटो धमाल</a></li><li><a href="http://navbharattimes.indiatimes.com/state/articlelist/2279808.cms" pg="fotkjlnk2" target="_blank">अन्य शहर</a></li><li><a href="http://navbharattimes.indiatimes.com/auto/automobile.cms" pg="fotkjlnk16" target="_blank">ऑटो</a></li><li><a href="http://navbharattimes.indiatimes.com/other/thoughts-platform/sunday-nbt/articlelist/6968985.cms" pg="fotkjlnk19" target="_blank">संडे NBT </a></li><li><a href="http://navbharattimes.indiatimes.com/world/articlelist/2279801.cms" pg="fotkjlnk6" target="_blank">दुनिया</a></li><li><a href="http://navbharattimes.indiatimes.com/business.cms" target="_blank" pg="fotkjlnk13">ET हिंदी</a></li><li><a href="http://m.nbt.in/" pg="fotkjlnk20" target="_blank">NBT मोबाइल</a></li><li><a href="http://navbharattimes.indiatimes.com/astro.cms" target="_blank" pg="fotkjlnk8">राशिफल </a></li><li><a href="http://navbharattimes.indiatimes.com/opinion/articlelist/2279782.cms" pg="fotkjlnk12" target="_blank">विचार</a></li><li><a href="http://navbharattimes.indiatimes.com/appslist.cms" pg="fotkjlnk21" target="_blank">NBT ऐप</a></li></ul></div><div class="newsletter"><div class="fottershareLinks"><h4 style="color:#fff">हमेशा कनेक्टेड रहें<br><strong>नवभारत टाइम्स  </strong> की ऐप के साथ</h4><div class="fotterApplinks"><a href="https://itunes.apple.com/us/app/navbharat-times/id656093141?ls=1&amp;mt=8" target="_blank" class="ios"></a><a href="https://play.google.com/store/apps/details?id=com.nbt.reader" target="_blank" class="andriod"></a><a href="http://navbharattimes.indiatimes.com/j2me/NBT.jad" target="_blank" class="java"></a><a href="http://windowsphone.com/s?appid=33151dbb-7443-4efa-ab5d-3846ac889a3a" target="_blank" class="win"></a></div></div></div><div class="timesotherLinks"><h2>हमारी दूसरी साइट्स </h2><a href="http://timesofindia.indiatimes.com/" target="_blank" pg="fottgwslnk1">Times of India</a>| <a target="_blank" href="http://economictimes.indiatimes.com/" pg="fottgwslnk2">Economic Times</a> | <a href="http://vijaykarnataka.indiatimes.com/" target="_blank" pg="fottgwslnk4">Vijay Karnataka</a>| <a href="http://eisamay.indiatimes.com/" target="_blank" pg="fottgwslnk5">Ei Samay</a> | <a href="http://navgujaratsamay.indiatimes.com/" target="_blank" pg="fottngslnk5">Navgujarat Samay</a> | <a href="http://maharashtratimes.com/" target="_blank" pg="fottgwslnk6">महाराष्ट्र टाइम्स</a> |<a href="http://www.businessinsider.in/" target="_blank" pg="fottgwslnk7">Business Insider</a>| <a href="http://zoomtv.indiatimes.com/" target="_blank" pg="fottgwslnk8">ZoomTv</a> | <a href="http://boxtv.com/" target="_blank" pg="fottgwslnk11">BoxTV</a>| <a href="http://www.gaana.com/" target="_blank" pg="fottgwslnk12">Gaana</a> | <a href="http://shopping.indiatimes.com/" target="_blank" pg="fottgwslnk13">Shopping</a> | <a href="http://www.idiva.com/" target="_blank" pg="fottgwslnk14">IDiva</a> | <a target="_blank" href="http://www.simplymarry.com/" pg="fottgwslnk17">Matrimonial</a><span class="footfbLike">नवभारत टाइम्स ऑन फेसबुक</span><iframe width="260px" height="35" frameborder="0" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fnavbharattimes&amp;width&amp;layout=standard&amp;action=like&amp;show_faces=false&amp;share=false&amp;height=35&amp;colorscheme=dark" scrolling="no" allowtransparency="true"></iframe></div><div class="seoLinks"><ul>
<li><a style="border-left:none !important" title="Hindi News" target="_blank" href="http://navbharattimes.indiatimes.com/">
Hindi News
</a></li>

<li><a target="_blank" href="http://navbharattimes.indiatimes.com/jokes.cms">
Funny jokes in Hindi
</a></li>

<li><a target="_blank" href="http://navbharattimes.indiatimes.com/astro/kundli-horoscope/astrokundali.cms#1">Kundali matching</a></li>

<li><a target="_blank" href="http://navbharattimes.indiatimes.com/metro/delhi/articlelist/4836708.cms ">Delhi news in Hindi</a></li>

<li><a target="_blank" href="http://navbharattimes.indiatimes.com/astro.cms ">Rashifal</a>
</li>
					
						<li>
						<a target="_blank" href="http://navbharattimes.indiatimes.com/state/maharashtra/pune/articlelist/21239264.cms">
							Pune News
						</a>
					</li>
					<li>
						<a target="_blank" href="http://navbharattimes.indiatimes.com/metro/mumbai/articlelist/5722181.cms">
							Mumbai News
						</a>
					</li>
                    
                    	<li>
						<a target="_blank" href="http://navbharattimes.indiatimes.com/jokes/funny-photo/photoarticlelist/12545581.cms">
							Funny Image
						</a>
					</li>
                    	<li>
						<a target="_blank" href="http://navbharattimes.indiatimes.com/state/bihar/articlelist/21236753.cms">
							Bihar News
						</a>
					</li>
                    
                    <li>
						<a pg="fotkjedu" target="_blank" href="http://navbharattimes.indiatimes.com/metro/lucknow/articlelist/21248218.cms" style="border-right:none !important">
							Lucknow News
						</a>
					</li>
			    </ul><!--/footerseolinks.cmspotime:21--></div><div class="timesLinks"><a target="_blank" href="http://www.timesinternet.in/" pg="fotlnk1">About Us</a>&nbsp;|&nbsp;
				<a target="_blank" href="https://www.ads.timesinternet.in/expresso/selfservice/loginSelfService.htm">Create Your Own Ad</a>|
				<a target="_blank" href="http://advertise.indiatimes.com/" pg="fotlnk2">Advertise with Us</a>|
				<a target="_blank" href="http://www.indiatimes.com/termsandcondition" pg="fotlnk3">Terms of Use and Grievance Redressal Policy</a>&nbsp;|&nbsp;
				<a target="_blank" href="https://navbharattimes.indiatimes.com/privacypolicyeu.cms" pg="fotlnk4">Privacy Policy</a>&nbsp;|&nbsp;
				<span class="feedbackeu">
				<a target="_blank" href="http://navbharattimes.indiatimes.com/feedback.cms" pg="fotlnk5">Feedback</a>&nbsp;|&nbsp;</span>
				<a target="_blank" href="http://navbharattimes.indiatimes.com/sitemap.cms" pg="fotlnk6">Sitemap</a></div><div class="copyText">Copyright &copy;&nbsp; '.date('Y').'
        &nbsp;Bennett Coleman &amp; Co. Ltd. All rights reserved. For reprint rights: <a target="_blank" href="http://timescontent.com/" pg="fotlnk7">Times Syndication Service</a></div></div>';
$my_settings['footer_css'] = '<style type="text/css">
.footercontainer{background:#333333; clear:both; overflow:hidden; padding:10px 0;}
.footercontainer a{color:#FDFDFD;}
.footercontainer h2{ color:#fff; font-size:20px; line-height:30px; padding:0 0 10px 2px;font-family: Arial, Helvetica, sans-serif;}

.insideLinks{ width:36%; padding-left:2%; float:left;}
.insideLinks ul{ list-style:none; display: inline-block; padding: 0; margin: 0}
.insideLinks ul li{ float:left; margin:0 10px 0 0; width:90px; }
.insideLinks ul li a{ font-family:Arial, Helvetica, sans-serif;color:#B5B3B4;font-size:14px;font-weight:bold;line-height:25px;}

.newsletter{width:32%; padding:0 2%; float:left;}
.newsletterpost {background:#CCCCCC; padding:14px; margin:10px 0 10px 0;float:left; width:300px;}

.newsletterpost input[type="text"]{background:#fff; border:1px #B7B7B7 solid; color:#333;height:21px; width:205px; font-size:14px;font-weight:bold;color:#999;float:left; padding:2px 10px;}
.newsletterpost input[type="submit"]{ background:#f5cc10; color:#000; padding:0 5px; *padding:0 4px; height:26px; cursor:pointer; border:none;font-weight:bold;float:left;}
.fottershareLinks span{ font-size:14px; color:#fff; width:100%; float:left; margin-bottom:10px;}
.fottershareLinks img{ vertical-align:middle;margin-top:5px;}
.fottershareLinks a{margin: 0 2px;}

.fotterApplinks{padding: 0 0 15px 0;}
.fotterApplinks a{padding: 0 10px 0 0; background-image:url(https://navbharattimes.indiatimes.com/photo/42711833.cms); float:left; width:47px; height:65px;background-repeat: no-repeat;}
.fotterApplinks a.andriod{background-position:0 4px}
.fotterApplinks a.andriod:hover{background-position:0 -68px}

.fotterApplinks a.ios{background-position: 0 -144px;}
.fotterApplinks a.ios:hover{background-position:0 -219px}

.fotterApplinks a.java{background-position:0 -296px;}
.fotterApplinks a.java:hover{background-position:0 -378px;}

.fotterApplinks a.win{background-position:0 -447px}
.fotterApplinks a.win:hover{background-position:0 -526px}

.fottershareLinks{clear: both;}
.fottershareLinks h4{color:#fff; font-size:16px;line-height: 23px;}
.fottershareLinks strong{color:#f5cc10; font-weight:normal}

.footfbLike{display: block;margin: 25px 0 12px 0;font-size: 14px;color: #fff;}

.timesotherLinks{width:32%; float:left; padding-right:2%; color:#CCCCCC;font-size:12px;}
.timesotherLinks a{ font-size:11px; padding:0 1px; color:#CCCCCC; line-height:18px;}

.timesLinks{ font-family:Arial;clear:both;text-align:center; font-size:12px;color:#929292; padding:10px 0;}
.timesLinks a {color:#FDFDFD; font-family:Arial;}

.copyText{ font-family:Arial;clear:both;text-align:center; font-size:12px;color:#929292; padding:0 35px;}
.copyText a {color:#FDFDFD; font-family:Arial;}

.seoLinks{clear:both; margin:10px 0; background:#585757; overflow:hidden;padding-left: 11px;}
.seoLinks ul{list-style:none; padding:0;}
.seoLinks ul li{float:left;}
.seoLinks ul li a{font-family:arial;padding:0 7px;color:#fff;font-size:13px; line-height:32px; display:block; float:left; border-left:#4a4a4a 1px solid;  border-right:#696969 1px solid;}
@media (max-width: 767px){.insideLinks,.newsletter,.timesotherLinks,.seoLinks,.timesLinks{display: none;}}
.mob_atf div[id^="div-gpt-ad"] {margin:auto}
.sticky_ads{position: fixed; bottom: 0; left: 0; right: 0; text-align: center; background: #fff;}
.sticky_ads div[id^="div-gpt-ad"] {margin:auto}
.rhs_600ads{position: fixed; right: 0; top: 0;height:600px;width:125px}
</style>';
$random = rand();
$my_settings['ctn_homepage'] = '<!-- NBT_Blog_HP_CTN_NAT,position=1--><div id="div-clmb-ctn-208423-1" data-slot="208423" data-position="1" data-section="0" data-cb="adwidget" class="colombia media article ctn_list"></div>';
$my_settings['ctn_homepage_rhs'] = '<!-- NBT_Blog_HP_RHS_RCMW_CTN_NAT,position=1--><div id="div-clmb-ctn-208424-1" data-slot="208424" data-position="1" data-section="0" data-cb="adwidget" class="colombia panel ctn_rhs" data-paidad-head="FROM WEB"></div>';
$my_settings['ctn_article_list'] = '<!-- NBT_Blog_ROS_AL_CTN_NAT,position=1--><div id="div-clmb-ctn-208419-1" data-slot="208419" data-position="1" data-section="0" data-cb="adwidget" class="colombia media article ctn_list"></div>';
$my_settings['ctn_article_list_rhs'] = '<!-- NBT_Blog_ROS_AL_RHS_RCMW_CTN_NAT,position=1--><div id="div-clmb-ctn-208420-1" data-slot="208420" data-position="1" data-section="0" data-cb="adwidget" class="colombia panel ctn_rhs" data-paidad-head="FROM WEB"></div>';
$my_settings['ctn_article_show_rhs'] = '<!-- NBT_Blog_ROS_AS_RHS_RCMW_CTN_NAT,position=1--><div id="div-clmb-ctn-208421-1-'.rand().'" data-slot="208421" data-position="'.rand().'" data-section="0" data-cb="adwidget" class="colombia panel ctn_rhs" data-paidad-head="FROM WEB"></div>';
$my_settings['ctn_article_show_end_article_1'] = '<!-- NBT_Blog_ROS_AS_EOA_RCMW_CTN_NAT,position=1--><div id="div-clmb-ctn-208415-1-'.rand().'" data-slot="208415" data-position="'.rand().'" data-section="0" class="colombia ctn_article_show_bot_1" data-paidad-head="FROM WEB" data-organicad-head="FROM NAVBHARAT TIMES" data-clear-float="1"></div>';
$my_settings['ctn_article_show_end_mobile_article_1'] = '<!-- NBT_Blog_Mweb_ROS_AS_EOA_RCMW_CTN_NAT,position=1--><div id="div-clmb-ctn-208422-1-'.rand().'" data-slot="208422" data-position="'.rand().'" data-section="0" class="colombia ctn_article_show_bot_1" data-paidad-head="FROM WEB" data-organicad-head="FROM NAVBHARAT TIMES" data-clear-float="1"></div>';
$my_settings['ctn_article_show_end_article_2'] = '<!-- NBT_Blog_ROS_AS_EOA1_RCMW_CTN_NAT,position=1--><div id="div-clmb-ctn-208416-1-'.rand().'" data-slot="208416" data-position="'.rand().'" data-section="0" class="colombia ctn_article_show_bot_2" data-paidad-head="FROM WEB" data-organicad-head="FROM NAVBHARAT TIMES" data-clear-float="1" data-separate-container="1"></div>';

// esi ads setting

$my_settings['esi_status'] = 'NONE';// TEST, LIVE, NONE

if(isMobile() && $my_settings['esi_status'] != 'NONE'){
	
	$esiHeaderStr = '<!--esi 
           <esi:eval src="/fetch_native_content/?fpc=$url_encode($(HTTP_COOKIE{\'_col_uuid\'}))&ab=$(HTTP_COOKIE{\'ce_nbapm\'})&id=$url_encode(\'##ESIIDS##\')&_t=4&_u=$url_encode($(HTTP_HOST)+$(REQUEST_PATH))&ua=$url_encode($(HTTP_USER_AGENT))&ip=$(REMOTE_ADDR)&_v=0&dpv=1&r=$rand()" dca="esi"/> 
           -->';

        $esiHeaderStr .= <<<'ESISTR'
<!--esi           
                        <esi:choose>
                            <esi:when test="$exists($(native_content{'_col_uuid'}))">
                		        $add_header('Set-Cookie', $(native_content{'_col_uuid'}))
                            </esi:when>
                        </esi:choose>
                    -->
                
                    <!--esi
                        <esi:choose>
                            <esi:when test="$exists($(native_content{'_col_script'}))">
                		        $(native_content{'_col_script'})
                            </esi:when>
                        </esi:choose>
                    -->
                
                    <!--esi
                        <esi:choose>
                            <esi:when test="$exists($(native_content{'_col_ab_call'}))">
                		        $(native_content{'_col_ab_call'})
                            </esi:when>
                        </esi:choose>
                    -->
ESISTR;
$my_settings['esi_header'] = $esiHeaderStr;

	$my_settings['esi_homepage_ids'] = '208417~1~0';
	
	/*HP_CTN_NAT*/
	$my_settings['esi_homepage'] = '<!--esi
						<esi:choose>
							<esi:when test="$exists($(native_content{\'208417~1~0\'}))">
								$(native_content{\'208417~1~0\'})
							</esi:when>
						</esi:choose>
					-->  ';
					
	/*HP_RHS_RCMW_CTN_NAT*/
	$my_settings['esi_homepage_rhs'] = "";
	
	$my_settings['esi_article_list_ids'] = '208418~1~0';
	
	/*ROS_AL_CTN_NAT*/
	$my_settings['esi_article_list'] = '<!--esi
						<esi:choose>
							<esi:when test="$exists($(native_content{\'208418~1~0\'}))">
								$(native_content{\'208418~1~0\'})
							</esi:when>
						</esi:choose>
					-->  ';
	
	/*ROS_AL_RHS_RCMW_CTN_NAT*/
	$my_settings['esi_article_list_rhs'] = "";
	
	$my_settings['esi_article_show_ids'] = '208422~1~0';
	
	/*ROS_AS_RHS_RCMW_CTN_NAT*/
	$my_settings['esi_article_show_rhs'] = "";
	
	/*ROS_AS_EOA_RCMW_CTN_NAT*/
	$my_settings['esi_article_show_end_article_1'] = '<!--esi
						<esi:choose>
							<esi:when test="$exists($(native_content{\'208422~1~0\'}))">
								$(native_content{\'208422~1~0\'})
							</esi:when>
						</esi:choose>
					-->  ';
	
	/*ROS_AS_EOA1_RCMW_CTN_NAT*/
	$my_settings['esi_article_show_end_article_2'] = ""; 
	
	/*Mweb_AS_BLY_VDO_CTN_NAT*/                               
	$my_settings['ctn_article_show_mid_article_video'] = '<!-- Mweb_AS_BLY_VDO_CTN_NAT,position=1--><div id="div-clmb-ctn-297347-1" data-slot="297347" data-position="1" data-section="0" class="colombia media article ctn_list"></div>';                               

}elseif($my_settings['esi_status'] != 'NONE'){

	$esiHeaderStr = '<!--esi 
           <esi:eval src="/fetch_native_content/?fpc=$url_encode($(HTTP_COOKIE{\'_col_uuid\'}))&ab=$(HTTP_COOKIE{\'ce_nbapd\'})&id=$url_encode(\'##ESIIDS##\')&_t=4&_u=$url_encode($(HTTP_HOST)+$(REQUEST_PATH))&ua=$url_encode($(HTTP_USER_AGENT))&ip=$(REMOTE_ADDR)&_v=0&dpv=1&r=$rand()" dca="esi"/> 
           -->';

        $esiHeaderStr .= <<<'ESISTR'
<!--esi           
                        <esi:choose>
                            <esi:when test="$exists($(native_content{'_col_uuid'}))">
                		        $add_header('Set-Cookie', $(native_content{'_col_uuid'}))
                            </esi:when>
                        </esi:choose>
                    -->
                
                    <!--esi
                        <esi:choose>
                            <esi:when test="$exists($(native_content{'_col_script'}))">
                		        $(native_content{'_col_script'})
                            </esi:when>
                        </esi:choose>
                    -->
                
                    <!--esi
                        <esi:choose>
                            <esi:when test="$exists($(native_content{'_col_ab_call'}))">
                		        $(native_content{'_col_ab_call'})
                            </esi:when>
                        </esi:choose>
                    -->
ESISTR;
$my_settings['esi_header'] = $esiHeaderStr;


	$my_settings['esi_homepage_ids'] = '208423~1~0,208424~1~0';
	
	/*HP_CTN_NAT*/
	$my_settings['esi_homepage'] = '<!--esi
						<esi:choose>
							<esi:when test="$exists($(native_content{\'208423~1~0\'}))">
								$(native_content{\'208423~1~0\'})
							</esi:when>
						</esi:choose>
					-->  ';
	
	/*HP_RHS_RCMW_CTN_NAT*/
	$my_settings['esi_homepage_rhs'] = '<!--esi
						<esi:choose>
							<esi:when test="$exists($(native_content{\'208424~1~0\'}))">
								$(native_content{\'208424~1~0\'})
							</esi:when>
						</esi:choose>
					-->  ';
					
	$my_settings['esi_article_list_ids'] = '208419~1~0,208420~1~0';
	
	/*ROS_AL_CTN_NAT*/
	$my_settings['esi_article_list'] = '<!--esi
						<esi:choose>
							<esi:when test="$exists($(native_content{\'208419~1~0\'}))">
								$(native_content{\'208419~1~0\'})
							</esi:when>
						</esi:choose>
					-->  ';
	
	/*ROS_AL_RHS_RCMW_CTN_NAT*/
	$my_settings['esi_article_list_rhs'] = '<!--esi
						<esi:choose>
							<esi:when test="$exists($(native_content{\'208420~1~0\'}))">
								$(native_content{\'208420~1~0\'})
							</esi:when>
						</esi:choose>
					-->  ';
					
	$my_settings['esi_article_show_ids'] = '208421~1~0,208415~1~0,208416~1~0';
	
	/*ROS_AS_RHS_RCMW_CTN_NAT*/
	$my_settings['esi_article_show_rhs'] = '<!--esi
						<esi:choose>
							<esi:when test="$exists($(native_content{\'208421~1~0\'}))">
								$(native_content{\'208421~1~0\'})
							</esi:when>
						</esi:choose>
					-->  ';
					
	/*ROS_AS_EOA_RCMW_CTN_NAT*/
	$my_settings['esi_article_show_end_article_1'] = '<!--esi
						<esi:choose>
							<esi:when test="$exists($(native_content{\'208415~1~0\'}))">
								$(native_content{\'208415~1~0\'})
							</esi:when>
						</esi:choose>
					-->  ';
					
	/*ROS_AS_EOA1_RCMW_CTN_NAT*/
	$my_settings['esi_article_show_end_article_2'] = '<!--esi
						<esi:choose>
							<esi:when test="$exists($(native_content{\'208416~1~0\'}))">
								$(native_content{\'208416~1~0\'})
							</esi:when>
						</esi:choose>
					-->  ';          
	
	/*AS_BLY_VDO_CTN_NAT*/  
	$my_settings['ctn_article_show_mid_article_video'] = '<!-- Mweb_AS_BLY_VDO_CTN_NAT,position=1--><div id="div-clmb-ctn-297346-1" data-slot="297346" data-position="1" data-section="0" class="colombia media article ctn_list"></div>';                                                
}

//$my_settings['ATF_300_DIV_ID'] ='div-gpt-ad-1343039310577-0';
//$my_settings['ATF_728_DIV_ID'] ='div-gpt-ad-1343039310577-1';
//$my_settings['BTF_300_DIV_ID'] ='div-gpt-ad-1343039310577-2';
//$my_settings['ATF_300_AD_CODE'] ='/7176/Navbharattimes/NBT_Blog/NBT_ROS_ATF_BLG_NW_AS_300';
//$my_settings['ATF_728_AD_CODE'] ='/7176/Navbharattimes/NBT_Blog/NBT_ROS_ATF_BLG_NW_AS_728';
//$my_settings['BTF_300_AD_CODE'] ='/7176/Navbharattimes/NBT_Blog/NBT_ROS_ATF_BLG_NW_AS_300';
$my_settings['channel'] ='nbt';
$my_settings['site_id'] ='9830e1f81f623b33106acc186b93374e';
$my_settings['domain'] ='indiatimes.com';
$my_settings['ofcommenthostid'] ='294';
$my_settings['ofcommentchannelid'] ='2147478026';
$my_settings['appid'] ='117787264903013';
$my_settings['gPlusClient'] ='265054015577-n4ep9siuh3vjn02oe9vmgcjoi0p6mk4b.apps.googleusercontent.com';
$commnetText = ["name_required" =>"Please enter your name.",
			"location_required" =>"Please enter your location.",
			"captcha_required" =>"Please enter captcha value.",
			"name_toolong" =>"Name cannot be longer than 30 chars.",
			"name_not_string" =>"Name can only contain alphabets.",
			"location_toolong" =>"Location cannot be longer than 30 chars.",
			"location_not_string" =>"Location can only contain alphabets.",
			"captcha_toolong" =>"Captcha cannot be longer than 4 chars.",
			"captcha_number_only" =>"Captcha value can only be a number.",
			"email_required" =>"Please enter your email address.",
			"email_invalid" =>"Please enter a valid email address.",
			"captcha_invalid" =>"Please enter a valid captcha value.",
			"minlength"=> "You can t post this comment as the length it is too short. ",
			"blank"=> "You can t post this comment as it is blank.",
			"maxlength"=> "You have entered more than 3000 characters.",
			"popup_blocked"=> "Popup is blocked.",
			"has_url"=> "You can t post this comment as it contains URL.",
			"duplicate"=> "You can t post this comment as it is identical to the previous one.",
			"abusive"=> "You can't post this comment as it contains inappropriate content.",
			"self_agree"=> "You can't Agree with your own comment",
			"self_disagree"=> "You can't Disagree with your own comment",
			"self_recommend"=> "You can't Recommend your own comment",
			"self_offensive"=> "You can't mark your own comment as Offensive",
			"already_agree"=> "You have already Agreed with this comment",
			"already_disagree"=> "You have already Disagreed with this comment",
			"already_recommended"=> "You have already Recommended this comment",
			"already_offensive"=> "You have already marked this comment Offensive",
			"cant_agree_disagree"=> "You can't Agree and Disagree with the same comment",
			"cant_agree_offensive"=> "You can't Agree and mark the same comment Offensive",
			"cant_disagree_recommend"=> "You can't Disagree and Recommend the same comment",
			"cant_recommend_offensive"=> "You can't Recommend and mark the same comment Offensive",
			"permission_facebook"=> "You can't post to facebook. Post permission is required.",
			"offensive_reason"=> "Please select a reason.",
			"offensive_reason_text"=> "Please enter a reason." ,
			"offensive_reason_text_limit"=> "Please enter less than 200 chars.",
			"be_the_first_text"=> "Be the first one to review.",
			"no_comments_discussed_text"=> "None of the comments have been discussed.",
			"no_comments_up_voted_text"=> "None of the comments have been up voted.",
			"no_comments_down_voted_text"=> "None of the comments have been down voted."
		 ];
$my_settings['comment_text'] = (json_encode($commnetText));

if(getenv("environment")=='live'){
    $my_settings['ssoUrl']      = 'https://jsso.indiatimes.com';
    $my_settings['myTimesUrl']  = 'https://myt.indiatimes.com';
    $my_settings['jssocdn']  = 'https://jssocdn.indiatimes.com';
    $my_settings['socialappsintegrator']  = 'https://socialappsintegrator.indiatimes.com';
     
}else{
    $my_settings['ssoUrl']      = 'https://jssostg.indiatimes.com';
    $my_settings['myTimesUrl']  = 'https://mytest.indiatimes.com';
    $my_settings['jssocdn']  = 'https://jssocdnstg.indiatimes.com';
    $my_settings['socialappsintegrator']  = 'https://testsocialappsintegrator.indiatimes.com';
}
