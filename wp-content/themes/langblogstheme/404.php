<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
<div class="container">
	<div class="row">
		<div class="col-md-8 feeds" data-vr-zone="feeds">
			<div class="article">
				<header class="page-header">
					<h2 class="media-heading"><?php _e( $my_settings['not_found_heading'] ); ?></h2>
				</header>
				<div class="content">
					<p><?php _e( $my_settings['not_found_txt'] ); ?></p>
					<?php get_search_form(); ?>
				</div>
			</div>
		</div>
		<div class="col-md-4 sidebar">
			<div class="panel">
				<div class="addwrapper">
					<?php echo $my_settings['google_ad_right_1']; ?>
				</div>
			</div>
			<?php include 'sidebar.php' ?>
		</div>
	</div>
</div>
<?php
get_footer();
