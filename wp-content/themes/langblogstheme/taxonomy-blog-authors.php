<?php 
  get_header();
?>

<script type='text/javascript'>
curPage = 'Authors';
</script>
<div class="container">
<div class="row">
  <div class="col-md-8 author-list">

<?php

  $pagenum_link =	html_entity_decode( get_pagenum_link() );
  $url_parts    = explode( '?', $pagenum_link );
  if ( isset( $url_parts[1] ) ) {
    wp_parse_str( $url_parts[1], $query_args );
  }
  //echo 'page - '.$query_args['page'];
  $number = 40;
  $role = 'author';//sanitize_text_field($role);
  //$number = sanitize_text_field($number);
  //echo 'here';

  // We're outputting a lot of HTML, and the easiest way 
  // to do it is with output buffering from PHP.
  // ob_start();
  
  // Get the Search Term
  $search = ( isset($_GET["as"]) ) ? sanitize_text_field($_GET["as"]) : false ;
  
  // Get Query Var for pagination. This already exists in WordPress
  if(isset($query_args['page'])){
      $page = $query_args['page'];
  }else{
      $page = 1; 
  }
  //$page = (get_query_var('paged')) ? get_query_var('paged') : 1;
  //echo 'pg'.$page;
  // Calculate the offset (i.e. how many users we should skip)
  $offset = ($page - 1) * $number;
  //echo $offset;
  
  if ($search){
    // Generate the query based on search field
    $my_users = new WP_User_Query( 
      array( 
        /*'role' => $role,*/
        'meta_query'   => array(
			'relation' => 'OR',
			array(
			  'compare' => '!=',
			  'key' => 'hide_on_front',
			  'value' => 'yes',
			),
			array(
			  'compare' => 'NOT EXISTS',
			  'key' => 'hide_on_front'
			)
		  ),
		'query_id' => 'authors_with_posts',
		'fields' => 'ids',
		'orderby' => 'user_registered',
		'order' => 'DESC',
        'exclude' => array('1'),
        'search' => '*' . $search . '*' 
      ));
  } else {
    // Generate the query 
    $my_users = new WP_User_Query( 
      array( 
        /*'role' => $role,*/
        'meta_query'   => array(
			'relation' => 'OR',
			array(
			  'compare' => '!=',
			  'key' => 'hide_on_front',
			  'value' => 'yes',
			),
			array(
			  'compare' => 'NOT EXISTS',
			  'key' => 'hide_on_front'
			)
		  ),
		'query_id' => 'authors_with_posts',
        'offset' => $offset ,
        'number' => $number,
        'fields' => 'ids',
        'orderby' => 'user_registered',
        'order' => 'DESC',
        'exclude' => array('1')
      ));
  }
  
  // Get the total number of authors. Based on this, offset and number 
  // per page, we'll generate our pagination. 
  $total_authors = $my_users->total_users;

  // Calculate the total number of pages for the pagination
  $total_pages = intval($total_authors / $number) + 1;
  // The authors object. 
  $authors = $my_users->get_results();
?>
  
  <div class="author-search">
    <form method="get" id="sul-searchform" action="<?php the_permalink() ?>">
      <input type="text" class="form-field" name="as" id="sul-s" placeholder="<?php echo $my_settings['search_authors_by_name_txt']; ?>" />
      <input type="submit" class="submit" name="submit" id="sul-searchsubmit" value="<?php echo $my_settings['search_txt']; ?>" />
    </form>
  <?php 
  if($search){ ?>
    <a href="<?php echo site_url(); ?>/authors" style="font-size:13px;color:#428bca"><?php echo $my_settings['back_to_authors_page_txt'];?></a>
    <h3 ><?php echo $my_settings['search_results_for_txt']; ?>: <em><?php echo $search; ?></em></h3>
  <?php } ?>
  
  </div><!-- .author-search -->
  
<?php if (!empty($authors))   { ?>
<?php
  // loop through each author
  foreach($authors as $author){
    $author_info = get_userdata($author);
    $user_post_count = count_user_posts( $author );
    ?>
    <div class="col-md-6">	
      <div class="media vcard">
        <a class="pull-left" pg="Author_Pos" href="<?php echo get_author_posts_url($author) ?>" title="<?php echo $my_settings['go_to_the_profile_of_txt']; ?> <?php echo $author_info->display_name; ?>">
    	   <img class="media-object" src="<?php  echo get_user_avatar($author); ?>" alt="<?php echo $my_settings['go_to_the_profile_of_txt']; ?> <?php echo $author_info->display_name ?>" >
        </a>
        <div class="media-body">
          <?php echo '<h3 class="media-heading"><a pg="Pos" href="' . get_author_posts_url($author) . '" rel="bookmark" title="'.$my_settings['go_to_txt'].' '.$author_info->display_name.'">'.$author_info->display_name.'</a></h3>';?>
          <div class="media-meta">
            <span class="cat"><b>Total Posts : </b><?php echo $user_post_count; ?></span>
          </div>
        </div>
	     </div>
	 </div>
    <?php
  }
?>
<?php } else { ?>
  <h2><?php echo $my_settings['no_authors_found_txt']; ?></h2>
<?php } //endif ?>
<div style="clear:both"></div>
<nav class="navigation paging-navigation" role="navigation">
    <div class="pagination loop-pagination">
      <?php 
      for ($x=1; $x<=$total_pages; $x++) {
      ?>
        <a class="<?php if($x==$page)echo 'page-numbers current'; else echo 'page-numbers'; ?>" href="<?php the_permalink() ?>?page=<?php echo $x; ?>"><?php echo $x; ?></a>
      <?php
      } 
      ?>  
    </div><!-- .pagination -->
  </nav>
  <?php 
  // Output the content.
  //$output = ob_get_contents();
  //ob_end_clean();
  //echo $output;
?>
  </div>
  <div class="col-md-4 sidebar">
    <div class="panel">
      <div class="addwrapper">
        <?php echo $my_settings['google_ad_right_1']; ?>
      </div>
    </div>
    <?php include 'sidebar.php' ?>
  </div>
</div>
</div>

<?php
  get_footer();
