<?php
/**
 * The template for displaying Comments
 *
 * The area of the page that contains comments and the comment form.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

/*
 * If the current post is protected by a password and the visitor has not yet
 * entered the password we will return early without loading the comments.
 */
/*if ( post_password_required() ) {
	return;
}
*/
global $my_settings;
$show = true;
$today = date("r");
$articledate = get_the_time('r');
$difference = round((strtotime($today) - strtotime($articledate))/(24*60*60),0);
$text = $my_settings['further_commenting_is_disabled_txt'];
//echo $difference;
if ($difference >= 70000000) {
    $show = false;
    $text = $my_settings['comments_on_this_post_are_closed_now_txt'];
}

?>
<!-- Comment section -->
<div class="comment-section <?php if($_GET['comments'] == 'show') { ?><?php } else { ?>web-comment<?php } ?>" id="comment-section-<?php the_ID() ?>">
    <span class="hide" data-plugin="facebook-loadjs"></span>
    <?php if($_GET['comments'] == 'show') { ?>
	<span data-plugin="comment-count" id="mainCommentCount" style="display:none;"></span>
	<?php } else { ?>
    <h2><span data-plugin="comment-count"></span> Comments</h2>
    <?php } ?>
<?php 
if ( comments_open() && $show==true) {
?>
            <div data-plugin="comment" data-index="-1" data-id="0" data-level="0" id="write">
                <div class="comment-form2" data-plugin="comment-form">
                        <div class="chooseLang" style="display:none;">
							<a href="#" data-c="inscript" data-d="" data-l=""><?php echo $my_settings['quill_link_1']; ?></a>
							&nbsp;|&nbsp;<a class="selected" href="#" data-c="<?php echo $my_settings['quill_lang']; ?>" data-d="" data-l=""><?php echo $my_settings['quill_link_2']; ?></a>
							&nbsp;|&nbsp;<a href="#" data-c="english" data-d="" data-l=""><?php echo $my_settings['quill_link_3']; ?></a>
							<?php if ( !wp_is_mobile() ) { ?>&nbsp;|&nbsp;<a href="#" data-c="vk" data-d="" data-l=""><?php echo $my_settings['quill_link_4']; ?></a><?php } ?>
						</div>
                        <div class="user-thumbnail">
                            <img src="https://timesofindia.indiatimes.com/photo/29251859.cms" data-plugin="user-thumb"/>
                        </div>
                        <textarea rows="5" cols="40" data-plugin="comment-input" placeholder="<?php echo $my_settings['add_your_comment_here_txt']; ?>"></textarea><br />
                        <?php if(isset($my_settings['offensive_comment_warning']) && !empty($my_settings['offensive_comment_warning'])) { ?>
                        <div style="line-height: 15px;color: #999;font-size: 11px;"><?php echo $my_settings['offensive_comment_warning']; ?></div>
                        <?php } ?>
                        <div class="error" data-plugin="comment-error"></div>
                        <div class="comment-footer clearfix">
                            <span class="char-count"><?php echo $my_settings['characters_remaining_txt']; ?>: <span data-plugin="comment-input-remaining" data-maxchar="3000">3000</span></span>
                            <div class="social-post hide" data-plugin="user-isloggedin">
                                <?php /*<label class="fb">
                                    <input type="checkbox" data-plugin="comment-facebook"/>
                                    <i class="icon-fb"></i>

                                    <span><?php echo $my_settings['share_on_fb_txt']; ?></span>
                                </label>*/ ?>
                                <label class="twitter">
                                    <input type="checkbox" data-plugin="comment-twitter"/>
                                    <i class="icon-twitter"></i>

                                    <span><?php echo $my_settings['share_on_twitter_txt']; ?></span>
                                </label>
                            </div>
                            <div class="social-login clearfix">
                                
                                <input type="submit" value="POST" class="submit" data-plugin="comment-post"/>
                                <div class="hide social-login-inner" data-plugin="user-notloggedin">
                                    <span class="fb" data-plugin="comment-facebook-post"></span>
                                    <span class="google" data-plugin="comment-google-post"></span>
                                </div>
                                
                            </div>
                        </div>
                        <a href="#" class="close" data-action="comment-close">X</a>
                    </div>
            </div>

<?php 
}else {
   ?>
   <div style="display:none;">
	   <div class="comment-form2" data-plugin="comment-form">
			<div class="chooseLang" style=display:none;>
				<a href="#" data-c="inscript" data-d="" data-l=""><?php echo $my_settings['quill_link_1']; ?></a>
				&nbsp;|&nbsp;<a class="selected" href="#" data-c="<?php echo $my_settings['quill_lang']; ?>" data-d="" data-l=""><?php echo $my_settings['quill_link_2']; ?></a>
				&nbsp;|&nbsp;<a href="#" data-c="english" data-d="" data-l=""><?php echo $my_settings['quill_link_3']; ?></a>
				<?php if ( !wp_is_mobile() ) { ?>&nbsp;|&nbsp;<a href="#" data-c="vk" data-d="" data-l=""><?php echo $my_settings['quill_link_4']; ?></a><?php } ?>
			</div>
			<div class="user-thumbnail">
				<img src="https://timesofindia.indiatimes.com/photo/29251859.cms" data-plugin="user-thumb"/>
			</div>
			<textarea rows="5" cols="40" data-plugin="comment-input" placeholder="<?php echo $my_settings['add_your_comment_here_txt']; ?>"></textarea><br />
			<div class="error" data-plugin="comment-error"></div>
			<div class="comment-footer clearfix">
				<span class="char-count"><?php echo $my_settings['characters_remaining_txt']; ?>: <span data-plugin="comment-input-remaining" data-maxchar="3000">3000</span></span>
				<div class="social-post hide" data-plugin="user-isloggedin">
					<?php /*<label class="fb">
						<input type="checkbox" data-plugin="comment-facebook"/>
						<i class="icon-fb"></i>

						<span><?php echo $my_settings['share_on_fb_txt']; ?></span>
					</label>*/ ?>
					<label class="twitter">
						<input type="checkbox" data-plugin="comment-twitter"/>
						<i class="icon-twitter"></i>

						<span><?php echo $my_settings['share_on_twitter_txt']; ?></span>
					</label>
				</div>
				<div class="social-login clearfix">
					
					<input type="submit" value="POST" class="submit" data-plugin="comment-post"/>
					<div class="hide social-login-inner" data-plugin="user-notloggedin">
						<span class="fb" data-plugin="comment-facebook-post"></span>
						<span class="google" data-plugin="comment-google-post"></span>
					</div>
					
				</div>
			</div>
			<a href="#" class="close" data-action="comment-close">X</a>
		</div>
   </div>
   <div class="notice">
          <?php echo $text; ?>
   </div>
   <style>
      .reply{
         display: none;
      }
   </style>
<?php
}
?>
            <!--/Comment-form -->
            <!--div class="sortby clearfix hide">
                <span><?php echo $my_settings['sort_by_txt']; ?>:</span>
                <select id="comment_sort">
                        <option value=""><?php echo $my_settings['newest_txt']; ?></option>
                        <option value="oldest"><?php echo $my_settings['oldest_txt']; ?></option>
                        <option value="discussed"><?php echo $my_settings['discussed_txt']; ?></option>
                        <option value="agree"><?php echo $my_settings['up_voted_txt']; ?></option>
                        <option value="disagree"><?php echo $my_settings['down_voted_txt']; ?></option>
                </select>
            </div-->
            <?php if ( comments_open() && $show==true) { ?>
            <div class="hide noComment"><?php echo $my_settings['be_the_first_one_to_review_txt']; ?></div>
            <?php } ?>
            <div class="comments-list" id="comments" data-plugin="comments"></div>
            <div class="comments-list hide" id="comments_oldest"></div>
            <div class="comments-list hide" id="comments_recommended"></div>
            <div class="comments-list hide" id="comments_discussed"></div>
            <div class="comments-list hide" id="comments_agree"></div>
            <div class="comments-list hide" id="comments_disagree"></div>
            <div data-plugin="comment-loading"><img src="https://timesofindia.indiatimes.com/photo/29439462.cms"/></div>
        </div>
        <!-- /Comment section -->

    <script type="text/javascript">
		function configureQuillOptions(textAreaId){
			$('.chooseLang a').click(function(e) { 
				// Prevent a page reload when a link is pressed
				e.preventDefault();
				if($(this).hasClass('selected')){
					return false;
				}
				var c = $(this).attr('data-c');
				var d = $(this).attr('data-d');
				var l = $(this).attr('data-l');
				if (c == 'inscript') {
					Quill.setLanguage(d, l);
					Quill.setInscriptMode(d, true);
					Quill.hideKeyBoard(d);
				} else if(c == 'vk'){
					Quill.setLanguage(d, l);
					Quill.showkeyBoard(d);
				} else {
					Quill.setLanguage(d, c);
					Quill.setInscriptMode(d, false);
					Quill.hideKeyBoard(d);
				}
				$(this).parent('.chooseLang').find('a').removeClass('selected');
				$(this).addClass('selected');
				return false;
			});
			$("#Quill" + textAreaId + "_div").live("DOMSubtreeModified", function() { 
				$(this).parents("div.comment-form2").find("[data-plugin='comment-input']").trigger('change');
				$(this).parents("div.comment-form2").find("[data-plugin='comment-input-remaining']").text(Quill.getCharsLeft(textAreaId));
			});
		}
		function applyingComments(){
			require(["comments"],function(comments){
				comments.run()
			});
			require(["tiljs/event"],function(event){
				event.subscribe("comment.post.end",function(){
					$(".comment-section .noComment").hide();
				});
				event.subscribe("user.logout",function(){
					$( "[data-plugin='user-thumb']" ).attr( "src", "https://timesofindia.indiatimes.com/photo/29251859.cms" );
				});
			});
		}
		function applyingQuillComments() {
			if(Quill!=undefined && Quill!=null) {
				require(["comments"],function(comments){
					comments.run()
				});
				require(["tiljs/event"],function(event){
					event.subscribe("comment.post.end",function(){
						$(".comment-section .noComment").hide();
					});
					event.subscribe("comment.form.addclass.full",function(e){
						var textAreaId = 'textarea1';
						$(e).attr('id', textAreaId);
						Quill.init(textAreaId, true, null, 3000, '<?php echo $my_settings['quill_lang']; ?>');
						Quill.setPrefillText(textAreaId,'<?php echo $my_settings['quill_lang']; ?>','<?php echo $my_settings['add_your_comment_here_txt']; ?>');
						$(e).parents("[data-plugin='comment-form']").find('.chooseLang').find('a').attr('data-d', textAreaId);
						$(e).parents("[data-plugin='comment-form']").find('a').attr('data-l', '<?php echo $my_settings['quill_lang']; ?>');
						$(e).parents("[data-plugin='comment-form']").find('.chooseLang').show();
						var marginTop = $(e).parents("[data-plugin='comment-form']").find('.chooseLang').outerHeight();
						$(e).parents("[data-plugin='comment-form']").find('.user-thumbnail').css('margin-top', marginTop + 'px');
						configureQuillOptions(textAreaId);
					});
					event.subscribe("comment.reply.form.addclass.full",function(e){
						var textAreaId = $(e).parents("[data-plugin='comment-reply']").parents('.comment-box').attr('id').replace('cmt', '');
						textAreaId = 'textarea' + textAreaId;
						$(e).attr('id', textAreaId);
						Quill.init(textAreaId, true, null, 3000, '<?php echo $my_settings['quill_lang']; ?>');
						Quill.setPrefillText(textAreaId,'<?php echo $my_settings['quill_lang']; ?>','<?php echo $my_settings['add_your_comment_here_txt']; ?>');
						$(e).parents("[data-plugin='comment-reply']").find('.chooseLang').find('a').attr('data-d', textAreaId);
						$(e).parents("[data-plugin='comment-reply']").find('.chooseLang').find('a').attr('data-l', '<?php echo $my_settings['quill_lang']; ?>');
						$(e).parents("[data-plugin='comment-reply']").find('.chooseLang').show();
						var marginTop = $(e).parents("[data-plugin='comment-reply']").find('.chooseLang').outerHeight();
						$(e).parents("[data-plugin='comment-reply']").find('.user-thumbnail').css('margin-top', marginTop + 'px');
						configureQuillOptions(textAreaId);
					});
					event.subscribe("comment.post.rendering",function(e){
						var textAreaId = $(e).attr('id');
						Quill.clearText(textAreaId);
					});
					event.subscribe("user.logout",function(){
						$( "[data-plugin='user-thumb']" ).attr( "src", "https://timesofindia.indiatimes.com/photo/29251859.cms" );
					});
				})
			}
		}
        window.msid = '<?php the_ID() ?>';
        window.toicommonjs = true;
        window.cmsid = '';
    </script>
<script id="comment_tmpl" type="application/template">
    <div class="comment-box level{{:level}} {{if id==-1}}highlight{{/if}}" id="cmt{{:id}}" data-plugin="comment" data-index="{{:index}}" data-id="{{:id}}" data-level="{{:level}}" data-user="{{:user.name}}" data-email="{{:user.email}}" data-userid="{{:user.id}}">
            <div class="user-thumbnail">
                {{if user.image}}
                <img class="userimg flL" data-src="{{:user.image}}" src="https://timesofindia.indiatimes.com/photo/11350517.cms"/>
                {{else}}
                <img class="userimg flL" data-src="https://timesofindia.indiatimes.com/photo/29251859.cms" src="https://timesofindia.indiatimes.com/photo/11350517.cms"/>
                {{/if}}
            </div>
            <div class="info-bar clearfix">
                <a href="https://mytimes.indiatimes.com/profile/{{:user.username || user.id}}" class="name" target="_blank">{{:user.name}}</a>
                
                {{if user.points}}
                <span class="divider">•</span>
                <span class="psR flL tooltip_wrap">
                <span class="point {{:user.pointslevel.toLowerCase()}}" title="{{:user.points}} Points">{{:user.points}}</span>
                <span class="tootipbox">
                    <strong class="rem_point">{{:user.pointsNeeded}}</strong> <?php echo $my_settings['more_points_needed_to_reach_next_level_txt']; ?>
                   <a href="<?php echo $my_settings['know_more_about_times_points_link']; ?>" class="more" target="_blank"><?php echo $my_settings['know_more_about_times_points_txt']; ?></a>
                </span>
                </span>
                {{/if}}
                {{if user.badge}}
                    
                    <div class="badges clearfix">
                    <span class="divider ">•</span>
                        {{for user.badge}}
                           {{if #index < 3}}
                                <div class="badge" data-plugin="comment-user-badge" data-name="{{:name}}">
                                    <img data-src="{{:image}}" title="{{:name}}"  src="https://timesofindia.indiatimes.com/photo/11350517.cms">
                                    <div class="popup_badge">
                                       <div class="heading-block"><span class="level">Level {{:count}}</span><h4>{{:name}}</h4></div>
                                       <div class="content1 clearfix">
                                                <div class="cont_left" >
                                                    <img src="{{:image}}" title="{{:name}}"  src="https://timesofindia.indiatimes.com/photo/11350517.cms">
                                                </div>
                                                <div class="cont_right" >
                                                    <p><b>{{:#parent.parent.parent.data.user.name}}</b> {{:levelDesc}}</p>
                                                </div>
                                           <div class="more-info">
                                           <a href="<?php echo $my_settings['know_more_about_times_points_link']; ?>" class="more" target="_blank"><?php echo $my_settings['know_more_about_times_points_txt']; ?></a>
                                           </div>
                                       </div>
                                    </div>
                                </div>
                           {{/if}}
                        {{/for}}
                        <div class="more-btn">
                            <div class="popup_badge">
                               <div class="heading-block"><h4><?php echo $my_settings['badges_earned_txt']; ?></h4></div>
                               <div class="content1 clearfix">
                                    <!-- These are badges -->
                                   {{for user.badge}}
                                        <div class="section" data-plugin="comment-user-badge" data-name="{{:name}}">
                                            <img src="{{:image}}" title="{{:name}}"  src="https://timesofindia.indiatimes.com/photo/11350517.cms">
                                            <span class="name">{{:name}}</span>
                                            <span class="badgepoint">{{:count}}</span>
                                        </div>
                                    {{/for}}
                                   <!-- / badges End here -->
                                   <div class="more-info">
                                   <a href="<?php echo $my_settings['know_more_about_times_points_link']; ?>" class="more" target="_blank"><?php echo $my_settings['know_more_about_times_points_txt']; ?></a>
                                   </div>
                               </div>
                            </div>
                        </div>
                    </div>
                {{/if}}
                {{if parentusername}}
                <span class="divider">•</span>
                <span class="metadata"><i class="icon-reply"></i> {{:parentusername}}</span>
                {{/if}}
                {{if user.location}}
                <span class="divider">•</span>
                <span class="metadata">{{:user.location}}</span>
                {{/if}}
                <span class="divider">•</span>
                <span title="{{:abstime}}" class="metadata" data-plugin="dynamic-uptime" data-time="{{:time}}"><?php echo $my_settings['just_now_txt']; ?></span>
                {{if user.name}}
                <span data-plugin="comment-user-follow_wrapper" class="dont_show" >
                

                <span class="metadata follow" title="<?php echo $my_settings['follow_txt']; ?> {{:user.name}} {{:user.follower_text}}" data-plugin="comment-user-follow" ><span class="divider">•</span><?php echo $my_settings['follow_txt']; ?></span>
                </span>

                {{/if}}
            </div>
            <!--/Info bar -->
            <p>{{:comment}}</p>
            <!-- Footbar -->
            {{if id!=-1}}
            <div class="footbar clearfix">
                {{if opinion && opinion[0] && opinion[1]}}
                    <span class="up cpointer" data-action="comment-agree" title="<?php echo $my_settings['up_vote_txt']; ?>"><span data-plugin="comment-agree-count">{{:opinion[0].count}}</span> <i class="icon-uparrow"></i></span>
                    <span class="down cpointer" data-action="comment-disagree" title="<?php echo $my_settings['down_vote_txt']; ?>"><span data-plugin="comment-disagree-count">{{:opinion[1].count}}</span> <i class="icon-downarrow"></i></span>
                    <span class="divider">•</span>
                {{/if}}
                <span data-action="comment-reply" class="cpointer"><?php echo $my_settings['reply_txt']; ?></span>
                <span class="divider">•</span>

                <span class="cpointer flag_comment">
                    <span data-action="comment-offensive" title="<?php echo $my_settings['mark_as_offensive_txt']; ?>"><?php echo $my_settings['flag_txt']; ?></span>
                    <div class="popup_badge" data-plugin="offensive_popup">
                        <div class="heading-block"><h4><?php echo $my_settings['find_this_comment_offensive_txt']; ?></h4></div>
                        <div class="content1 clearfix">
                            <span class="success-message"><?php echo $my_settings['reason_submitted_to_admin_txt']; ?></span>
                            <span class="info-txt"><?php echo $my_settings['choose_reason_txt']; ?></span>
                            <h6><?php echo $my_settings['reason_for_reporting_txt']; ?>:</h6>
                            <ul class="reasons" style="padding-left:1px;">
							   <li><label for="reason_foul"><input type="radio" value="Foul language" name="radio1" id="reason_foul"><?php echo $my_settings['foul_language_txt']; ?></label></li>
                               <li><label for="reson_defamatory"><input type="radio" value="Defamatory" name="radio1" id="reson_defamatory"><?php echo $my_settings['defamatory_txt']; ?></label></li>
                               <li><label for="reason_hatred"><input type="radio" value="Inciting hatred against a certain community" name="radio1" id="reason_hatred"><?php echo $my_settings['inciting_hatred_against_certain_community_txt']; ?></label></li>
                               <li><label for="reason_spam"><input type="radio" value="Out of context/Spam" name="radio1" id="reason_spam"><?php echo $my_settings['out_of_context_spam_txt']; ?></label></li>
                               <li><label for="reason_others"><input type="radio" value="Others" name="radio1" id="reason_others"><?php echo $my_settings['others_txt']; ?></label></li>
                               <li><textarea style="display:none" data-plugin="offensive_popup_reason" class="offensive_popup_reason"></textarea></li>
                            </ul>
                            <div data-plugin="comment-offensive-error" class="error"></div>
                            <div class="buttons"><span class="button submit" data-action="offensive_popup_submit"><?php echo $my_settings['report_this_txt']; ?></span> <span class="button close_popup" data-action="offensive_popup_close"><?php echo $my_settings['close_txt']; ?></span></div>
                        </div>
                    </div>   
                </span>
                <span data-action="comment-offensive-already" title="<?php echo $my_settings['already_marked_as_offensive']; ?>" class="hide"><?php echo $my_settings['flagged_txt']; ?></span>
            </div>
            <div class="error clearfix" data-plugin="comment-error-outer"></div>

            <!--/Footbar -->
            <!--<div class="comment-form reply hide" data-plugin="comment-reply-old">-->
            <!--<div class="user-thumbnail">-->
            <!--<img data-src="https://timesofindia.indiatimes.com/photo/29251859.cms" alt="thumb" data-plugin="user-icon"  />-->
            <!--</div>-->
            <!--<textarea rows="5" cols="40" data-plugin="comment-input"></textarea>-->
            <!--<div class="comment-footer clearfix">-->
            <!--<span class="char-count">Characters Remaining: <span data-plugin="comment-input-remaining" data-maxchar="3000">3000</span></span>-->
            <!--<input type="submit" value="SEND" class="submit" data-action="comment-post"/>-->
            <!--</div>-->
            <!--<a href="#" class="close" data-action="comment-close">X</a>-->
            <!--</div>-->
            {{/if}}
        </div>        
        </script>

        <script id="badge_tmpl" type="application/template">
        <div class="badge_new" style="position:absolute;left:-148px;background-color:#FFF;z-index:2;">
            <div style="background:url(https://timesofindia.indiatimes.com/photo/12353738.cms) top center no-repeat; width:320px; height:54px;">
                <div class="full_cont"><div class="left_badge">
                    <div class="textone_badge">Wordsmith</div>
                </div>
                    <div class="right_badge">
                        <div class="texttwo_badge">Level 1</div>
                    </div>
                </div>
            </div>
            <div class="hoverblock_new">
                <div class="cont_pad">
                    <div class="full_cont">
                        <div class="cont_left">
                            <img style="cursor:auto" border="0" height="50" width="65" src="http://static.rewards.indiatimes.com/images/wordsmith.png" title="Wordsmith" ></div>
                        <div class="cont_right"><div class="badgesyn_two">
                            <p><b>monil shah</b>&nbsp;has posted 10 comments on Timesofindia.com to earn the <b>Wordsmith Level 1</b> badge.</p></div>
                            <div class="badgesyn"></div></div><div></div></div><div class="divclear"></div>
                    <div class="full_cont_one cont_mar"><div class="bordertop"><div class="leftthree_logged">
                        <div onclick="moretimespoint();" class="linkone_badge"><?php echo $my_settings['know_more_about_times_points_txt']; ?></div>
                    </div>
                    </div>
                    </div>
                </div>
            </div>
            <div style="background:url(https://timesofindia.indiatimes.com/photo/12353749.cms) no-repeat; width:320px; height:14px;"></div>
        </div>
        
        </script>
