<?php
  /**
   * The main template file
   *
   * This is the most generic template file in a WordPress theme and one
   * of the two required files for a theme (the other being style.css).
   * It is used to display a page when nothing more specific matches a query,
   * e.g., it puts together the home page when no home.php file exists.
   *
   * @link http://codex.wordpress.org/Template_Hierarchy
   *
   * @package WordPress
   * @subpackage BlogsTheme
   * @since Blogs Theme 1.0
   */
  
  get_header();
  $catTitle = single_cat_title( '', false );
  ?>
<script type='text/javascript'>
curPage = 'Cat';
pgname = '<?php echo $catTitle; ?>';
</script>
<div class="container">
<div class="row">
  <div class="col-md-8">
    <?php 
    //echo get_the_category()[0]->slug;
    $term = get_term_by( 'id', $cat, 'category' ); // get current term
    //print_r($term);
    $parent = get_term($term->parent, 'category' ); // get parent term
    //print_r($parent);
    $children = get_term_children($term->term_id, 'category'); // get children
    if(sizeof($children)>0 || $parent->term_id!="" ){
      // this has heirarchy
      if(sizeof($children)>0){
        /// this is the parent
        $isParent = true;
        $parentTerm = $term;
      }else{
        $isParent = false;
        $parentTerm = $parent;
      }

      echo '<ul class="nav nav-pills">';
      /*if($isParent)
        echo '<li class="active">';
      else
        echo '<li>';
      echo '<a href="' . get_term_link( $parentTerm, 'category' ) .'">'.$parentTerm->name.'</a></li>';
      echo "<li> >> </li>";*/
      $termchildren = get_term_children( $parentTerm->term_id, 'category' );
        foreach ( $termchildren as $child ) {
          $cat1 = get_term_by( 'id', $child, 'category' );
          if($cat1->name == $term->name)
            echo '<li class="active">';//. $cat1->name . '</a></li>';
          else
            echo '<li>';
          echo '<a href="' . get_term_link( $child, 'category' ) . '">' . $cat1->name . '</a></li>';
      }
      echo '</ul>';
    }
    printf( __( '<h1 class="blog-heading"> %s </h1>', 'twentyfourteen' ), $catTitle); 
    $term_description = term_description();
      /*if ( ! empty( $term_description ) ){
        echo '<div style="color:white">'.$term_description.'</div>';
      }*/
    ?>
    <div class="feeds">
    <?php  
      if (have_posts()):
      // Start the Loop.
          while (have_posts()):
              the_post();  
              /*
               * Include the post format-specific template for the content. If you want to
               * use this in a child theme, then include a file called called content-___.php
               * (where ___ is the post format) and that will be used instead.
               */
              get_template_part('content', get_post_format());
          endwhile;
          // Previous/next post navigation.
          twentyfourteen_paging_nav();
      else:
          // If no content, include the "No posts found" template.
          get_template_part('content', 'none');
      endif;
      ?>
    </div>
  </div>
  <div class="col-md-4 sidebar">
    <div class="panel">
      <div class="addwrapper">
        <?php echo $my_settings['google_ad_right_1']; ?>
      </div>
    </div>
    
    <!--<div class="panel">
      <h3 class="panel-title">Most Discussed</h3>
      <ul class="panel-body list-unstyled trending">
        <?php
      //getTrendingByCat(get_the_category()[0]->slug)  
          ?>               
      </ul>
    </div> -->
    
    <?php include 'sidebar.php' ?>
  </div>
</div>
</div><!-- .container -->
<?php
get_footer();
