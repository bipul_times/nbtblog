<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage BlogsTheme
 * @since Blogs Theme 1.0
 */
global $my_settings;
$blog  = array_values(get_the_terms(get_the_ID(), 'blog' ))[0]; //[0];	
$isInterview = false;
$categories = get_the_category();
$catIds = array();
if($categories){
    foreach($categories as $category) {
        $catIds[] = $category->cat_ID;
        if($category->cat_name == 'Interviews')
            $isInterview = true;
    }
}
//include_once('defads.php');
if(!empty($blog)){     
    $authorName = get_the_author_meta('display_name');
    $hidedisclaimer = get_post_meta(get_the_ID(),'hidedisclaimer', true);
?>
<?php if ( isset($_GET['comments']) && $_GET['comments'] == 'show' ) { ?>
<div class="article" data-vr-contentbox="">
  <div class="cmntheading btnsticky">
	<div class="back-to-story">
	  <a href="<?php the_permalink(); ?>"><img alt="" src="https://m.timesofindia.com/photo/47010955.cms"><br>Back to story</a>
	</div>
	<div class="headofcmnt">
	  <h3><b><span data-plugin="comment-count-<?php the_ID(); ?>"></span></b> comments for </h3>
	  <h2><?php the_title() ?></h2>
	</div>
  </div>
</div>
<script>
/*var btnoffset = $('.cmntheading').offset().top;
$(window).scroll(function(event){
	var getscroll = $(this).scrollTop();
	if (btnoffset < getscroll) {
		$('.btnsticky').addClass('stickhead');
	}
	else{
		$('.btnsticky').removeClass('stickhead');
	}
})*/
</script>
<?php } else {  ?>
<div class="article" data-vr-contentbox="">
    <h1 class="media-heading"><?php the_title() ?></h1>
    <div class="media-meta">
        <span class="date"><?php echo esc_attr( get_the_date() ).', <span class="time">'.esc_attr( get_the_time() ).'</span>  IST' ?></span> 
        <a class="author" pg="Blog_Article_Author" href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' )) ?>" rel="author" title="<?php echo $my_settings['go_to_the_profile_of_txt']; ?> <?php echo $authorName ?>"><?php echo $authorName ?></a> in 
        <a class="blog" pg="Blog_Article_Blog" href="<?php  echo get_term_link($blog->term_id,'blog') ?>" sl-processed="1" title="<?php echo $my_settings['go_to_txt']; ?> <?php echo $blog->name ?>"><?php echo $blog->name ?></a> <b>|</b>  
        <span class="cat"><?php the_category(', ')?></span> 
	</div>
    <div class="commentwrapper">
        <div id="sharer" class="social-likes social-likes_notext" data-zeroes="yes" data-url="<?php the_permalink(); ?>" >
            <?php if(isMobile()){ ?>
            <div class="whatsapp" title="<?php the_title() ?>"  data-title="<?php echo $blog->name.' : '.get_the_title() ?>"></div>
            <?php } ?>
            <div class="facebook" title="<?php echo $my_settings['share_link_fb_txt']; ?>"></div>
            <div class="twitter" title="<?php echo $my_settings['share_link_twitter_txt']; ?>" data-title="<?php echo $blog->name.' : '.get_the_title() ?>" data-via="<?php echo $my_settings['twitter_handle']; ?>"></div>
            <div class="linkedin" title="<?php echo $my_settings['share_link_linkedin_txt']; ?>" data-title="<?php echo $blog->name.' : '.get_the_title() ?>"></div>
            <div class="plusone" title="Share link on Google+" data-title="<?php echo $blog->name.' : '.get_the_title() ?>"></div>
        </div>
        <div data-vertical="H" class="smailto"><a title="<?php echo $my_settings['mail_link_txt']; ?>" href="mailto:%20?subject=<?php echo rawurlencode ($blog->name.' : '.get_the_title()) ?>&body=<?php echo rawurlencode($my_settings['read_complete_article_here_txt'].' - ');the_permalink(); echo '%0D%0AExcerpt : '.rawurlencode (html_entity_decode(get_the_excerpt()));   ?>" target="_top"></a></div>
        <!--<div class="scomments"><span class="commentcount"></span><span class="scounter" data-plugin="comment-count"></span></div>-->
        <!--<div class="g-plusone" data-href="<?php //the_permalink(); ?>"></div>-->
    </div>
    <div class="commentwrapperv" style="display:none">
        <div id="sharer" class="social-likes social-likes_notext" data-zeroes="yes" data-url="<?php the_permalink(); ?>" >
            <?php if(isMobile()){ ?>
            <div class="whatsapp" title="<?php the_title() ?>"></div>
            <?php } ?>
            <div class="facebook" title="<?php echo $my_settings['share_link_fb_txt']; ?>"></div>
            <div class="twitter" title="<?php echo $my_settings['share_link_twitter_txt']; ?>" data-title="<?php echo $blog->name.' : '.get_the_title() ?>" data-via="<?php echo $my_settings['twitter_handle']; ?>"></div>
            <div class="linkedin" title="<?php echo $my_settings['share_link_linkedin_txt']; ?>" data-title="<?php echo $blog->name.' : '.get_the_title() ?>"></div>
            <div class="plusone" title="Share link on Google+" data-title="<?php echo $blog->name.' : '.get_the_title() ?>"></div>
        </div>
        <!--<div class="scomments"><span class="commentcount"></span><span class="scounter" data-plugin="comment-count"></span></div>-->
        <div data-vertical="V" class="smailto"><a title="<?php echo $my_settings['mail_link_txt']; ?>" href="mailto:%20?subject=<?php echo rawurlencode ($blog->name.' : '.get_the_title()) ?>&body=<?php echo rawurlencode($my_settings['read_complete_article_here_txt'].' - ');the_permalink(); echo '%0D%0AExcerpt : '.rawurlencode (html_entity_decode(get_the_excerpt()));   ?>" target="_top"></a></div>
        <!--<div class="g-plusone" data-href="<?php //the_permalink(); ?>" data-size="tall"></div>-->
    </div>
    <div class="content"><?php 
        $content = apply_filters( 'the_content', get_the_content() );
        $content = str_replace( ']]>', ']]&gt;', $content );
        $content = str_replace( $my_settings['blogauthor_link'],'../../',$content);
        $content = str_replace( $my_settings['old_blogauthor_link'],'../../',$content);
        
        global $adsPosition;
        $matchFound = 0;        
        $splitPattern = '#(<p[\s>].*?</p>)#s';
        $adsPosition = $my_settings['mrec_adsPosition'];
        $adsArr = [];
        $content = preg_replace_callback(
                $splitPattern,
                function ($matches) {                     
                    global $matchFound, $adsPosition, $my_settings;
                    
                    $matchFound = $matchFound+1;
                    if(in_array($matchFound, $adsPosition)){
                        $matches[0] = "<div id='".$my_settings['mrec_adsId'][$matchFound]."' style='width: 300px; height: 250px;'>
                                <script>
                                  googletag.cmd.push(function() { googletag.display('".$my_settings['mrec_adsId'][$matchFound]."'); });
                                </script>
                              </div>".$matches[0];
                        }    
                    return $matches[0];
                },
                $content
        );
                                
        //esi append after 2nd <p>

        /*if(array_key_exists('ctn_article_show_mid_article_video',$my_settings)){
			global $matchFound, $adsArr, $adsArrIndexs;
			$adsArrIndexs = array();
			$adsArr = array($my_settings['ctn_article_show_mid_article_video']);
			$firstAdPos = 3;
			$adDiffPos = 3;
			for($i = 0; $i < count($adsArr); $i++){
				if($i == 0)
					$adsArrIndexs[] = $firstAdPos;
				else
					$adsArrIndexs[] = end($adsArrIndexs) + $adDiffPos;
			}
			
			$matchFound = 0;
			$tagForSplit = 'p';
			$splitPattern = '/(<'.$tagForSplit.'.*>)/U';

			$content = preg_replace_callback(
				$splitPattern,
				function ($matches) {
					global $matchFound, $adsArr, $adsArrIndexs;
					$matchFound = $matchFound + 1;
					if(in_array($matchFound, $adsArrIndexs)){
						$matches[0] = $adsArr[0].$matches[0];
						unset($adsArr[0]);
						$adsArr = array_values($adsArr);
					}
					return $matches[0];
				},
				$content
			);
		}*/
		
        echo $content; ?>
    </div>
    <div style="clear:both;"></div>
        <?php
        if( $isInterview == true){

        }else{
            if((isset($hidedisclaimer) && $hidedisclaimer=='1')){
                echo '<div class="disclaimer"> '.$my_settings['disclaimer_on_txt'].' </div>';
            } else {         
                echo '<div class="disclaimer"> '.$my_settings['disclaimer_txt'].' : '.$my_settings['disclaimer_off_txt'].' </div>';
            }
        }
        //the_tags( '<footer class="entry-meta"><span class="tag-links">', '', '</span></footer>' );
       
        $posttags = get_the_tags();
        if(!empty($posttags)){
            foreach($posttags as $this_tag){
                $tagdisplay = $tagdisplay . "".$this_tag->name.", ";
            }
        }
        $ptags = substr($tagdisplay,0,-2); // remove the comma after last tag
        $date = esc_attr( get_the_date('M j, Y')) . ', ' .strtoupper(esc_attr( get_the_time() )).' IST';
       ?>
	<div class="panel authors mobile-authors">
	  <div class="panel-heading">
		<h3 class="panel-title"><?php echo $my_settings['author_txt']; ?></h3>
	  </div>
	  <div class="panel-body">
		<a class="pull-left" pg="Blog_Article_AuthorProfile" title="<?php echo $my_settings['go_to_the_profile_of_txt']; ?> <?php the_author_meta( 'display_name' ); ?>" href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' )) ?>">
			<img class="media-object" src="<?php  echo get_user_avatar(get_the_author_meta('ID')); ?>" alt="<?php the_author_meta( 'display_name' ); ?>">
		</a>
		<a class="media-heading" pg="Blog_Article_AuthorProfile" title="<?php echo $my_settings['go_to_the_profile_of_txt']; ?> <?php the_author_meta( 'display_name' ); ?>" href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' )) ?>" rel="bookmark"><b><?php the_author_meta( 'display_name' ); ?></b></a>
	<?php 
	$desc = get_the_author_meta( 'description' );
	if(!empty($desc)){
		?>
		<div class="bio">
			<div id="mb-showmore" style="display:none">
				<?php echo $desc.'<br><br>';?>    				
			</div>	
			<div id="mb-showless">
				<?php
				if(strlen ($desc) > 156){
					echo substr($desc, 0, strpos($desc, ' ', 156));
					echo '. . .';
				} else {
					echo $desc;
				}
				echo '<br><br>';
				?>    				
			</div>    		
			<?php if(strlen($desc) > 161){ ?>
			<a href="javascript:void(0)" pg="Blog_Article_MoreButton" id="mb-more-button"><?php echo $my_settings['more_txt']; ?></a>
			<a href="javascript:void(0)" pg="Blog_Article_LessButton" id="mb-less-button" style="display:none"><?php echo $my_settings['less_txt']; ?></a>
			<script>
			var mb_a1 = document.getElementById("mb-more-button");
			mb_a1.onclick = function() {
				$("#mb-showmore").show();
				$("#mb-showless").hide();	
				$("#mb-less-button").show();
				$("#mb-more-button").hide();	
				return false;
			}
			var mb_a2 = document.getElementById("mb-less-button");
			mb_a2.onclick = function() {
				$("#mb-showmore").hide();
				$("#mb-showless").show();
				$("#mb-less-button").hide();
				$("#mb-more-button").show();	
				return false;
			}
			</script>
			<?php } ?>
		</div>
		<hr style="margin:0px">
		<?php 
	}
	?>
	  </div>
	</div>
<?php /*<style>
.AR_11 {
font-size: 15px !important;
}
.ob_custom_date{
    display: none;
}
.AR_11 .odb_li {
height: auto !important;
}
</style>
<div class="OUTBRAIN" data-widget-id="AR_11" data-ob-template="timesofindia" ></div>
<script type="text/javascript" src="http://widgets.outbrain.com/outbrain.js" ></script>*/ ?>
<?php 
	if(array_key_exists('esi_status',$my_settings) && $my_settings['esi_status'] == 'TEST' && isset($_GET['isEsi']) && $_GET['isEsi'] == 'YES'){
		echo $my_settings['esi_article_show_end_article_1'];
	}else if(array_key_exists('esi_article_show_end_article_1',$my_settings) && array_key_exists('esi_status',$my_settings) && $my_settings['esi_status'] == 'LIVE'){
		echo $my_settings['esi_article_show_end_article_1'];
	}else{
		if(isMobile()){			
			echo $my_settings['ctn_article_show_end_mobile_article_1'];
		}else{
			echo $my_settings['ctn_article_show_end_article_1'];
		}

		
		echo '<div style="clear:both; margin-bottom:20px;"></div>';
	}
	if(array_key_exists('esi_status',$my_settings) && $my_settings['esi_status'] == 'TEST' && isset($_GET['isEsi']) && $_GET['isEsi'] == 'YES'){
		echo $my_settings['esi_article_show_end_article_2'];
	}else if(array_key_exists('esi_article_show_end_article_2',$my_settings) && array_key_exists('esi_status',$my_settings) && $my_settings['esi_status'] == 'LIVE'){
		echo $my_settings['esi_article_show_end_article_2'];
	}else{
		echo $my_settings['ctn_article_show_end_article_2'];
	}
?>
			
<div style="clear:both;"></div>
</div>
<?php } ?>
<script>
setInterval(function() {
    var st = $(this).scrollTop();
    //alert(st);
    /*if(st > parseInt($('.container')[0].offsetHeight) + parseInt($('.commentwrapper').position().top) + 30){
        $('.commentwrapperv').show();
    }else{
        $('.commentwrapperv').hide();
    }*/
}, 250);
</script>
<script>
        var a = '<?php the_ID() ?>';
        var cat = "<?php echo $categories[0]->cat_name; ?>";
        var catIds = "<?php echo implode(',', $catIds); ?>";
        var tags = "<?php echo $ptags; ?>";
        var date = "<?php echo $date; ?>";
        var n = "<?php echo $authorName; ?>";
        var facebookktitle_<?php the_ID() ?> ="<?php echo $blog->name.' : '.get_the_title() ?>";
        var facebooksyn_<?php the_ID() ?> = "<?php echo get_the_excerpt(); ?>";
        var facebooklink_<?php the_ID() ?> ="<?php the_permalink(); ?>";
        var fb_Img_<?php the_ID() ?> = "<?php echo FB_PLACEHOLDER; ?>";
        //_gaq.push(['_trackEvent','categoryevent','view',cat]);
</script>
<?php
}
?>
