<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>
<?php global $my_settings; ?>
<?php //if(isMobile()){?>
<?php //echo $my_settings['footer_css'];
    //echo $my_settings['footer'];?>
<?php //} else{?>
<?php if(isset($_GET['frmapp']) && $_GET['frmapp'] == 'yes'){?>
<?php }else{ ?>
<?php if(getNbtFooterHtml()){?>
<div class="container footer-continer">
    <div class="row">
        <div class="col-md-12">
            <?php echo  getNbtFooterHtml();?>
        </div>
    </div>
</div>
<?php } else{?>
<?php echo $my_settings['footer_css'];
    echo $my_settings['footer'];?>
<?php } ?>
<?php } ?>
<script type="text/javascript">

    TimesGDPR.common.consentModule.gdprCallback(function(){
			if(window._euuser){
			//alert('EU');		
			enablegdpr();
			if(TimesGDPR.common.consentModule.isConsentGiven()){
				//_setCookie();
			}
			//$('.cmtbtn-wrapper').hide();
			}
	});
</script>
<?php wp_footer(); ?>
<?php if(!isInitAd()) { ?>
<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
<script type="text/javascript">
  (function() {
    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    po.src = 'https://apis.google.com/js/platform.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
  })();
</script>
<script src="//platform.linkedin.com/in.js" type="text/javascript">
 lang: en_US
</script>
<?php } ?>
<?php echo $my_settings['comscore_tag'];?>
<?php 
if(is_singular()){
?>
<script type="text/javascript">
date = date.replace(':','.');
//date = date.replace(' AM','AM');
//date = date.replace(' PM','PM');

var _page_config, _ibeat_config ={};
        
function setPageConfig(config){
	_page_config = {
	  host: '<?php echo $my_settings['ibeat_host'];?>', 
	  key: '<?php echo $my_settings['ibeat_key'];?>',
	  domain: '<?php echo $my_settings['ibeat_domain'];?>',
	  channel: config._iBeat_channel,
	  action : config._iBeat_action,
	  articleId: config._iBeat_articleid,
	  contentType: 21,
	  location : 1,
	  url : config._iBeat_url,
	  cat : config._iBeat_cat,
	  subcat: config._iBeat_subcat,
	  contenttag: config._iBeat_tag,
	  catIds   : config._iBeat_catIds,
	  articledt: config._iBeat_articledt,
	  author: config._iBeat_author,
	  position: config._iBeat_position
	};
}
function trim(str){return str.replace(/^\s\s*/, '').replace(/\s\s*$/, '');}


 _ibeat_articleid = a;
 _ibeat_config[_ibeat_articleid] = {};
 _ibeat_config[_ibeat_articleid]['_iBeat_articleid']=_ibeat_articleid;
 _ibeat_config[_ibeat_articleid]['_iBeat_tag']=tags;
 _ibeat_config[_ibeat_articleid]['_iBeat_catIds']=catIds;
 _ibeat_config[_ibeat_articleid]['_iBeat_url'] = window.location.href;
 _ibeat_config[_ibeat_articleid]['_iBeat_type']=1;
 _ibeat_config[_ibeat_articleid]['_iBeat_action']=1;
 _ibeat_config[_ibeat_articleid]['_iBeat_cat']= trim(cat);
 _ibeat_config[_ibeat_articleid]['_iBeat_subcat']='';
 _ibeat_config[_ibeat_articleid]['_iBeat_articledt']=date;
 _ibeat_config[_ibeat_articleid]['_iBeat_author']=n;
 _ibeat_config[_ibeat_articleid]['_iBeat_channel'] ="<?php echo $my_settings['ibeat_channel'];?>";
 _ibeat_config[_ibeat_articleid]['_iBeat_position'] = " ";


if(location.hostname == '<?php echo $my_settings['ibeat_host'];?>' && !TimesGDPR.common.consentModule.isEUuser()){
	setPageConfig(_ibeat_config[_ibeat_articleid]);
	_page_config['position'] = Get_Ckie('PlRef') ? Get_Ckie('PlRef') : '';
	var iBeatTimer = setTimeout(function(){loadIbeatJS();},20000);
	$(window).load(function(){loadIbeatJS();clearTimeout(iBeatTimer);});
        var _ibeat_track = {"id": _ibeat_articleid, "ct": 21}
}

function loadIbeatJS(){
	try{
		if(typeof iBeatPgTrend == 'undefined'){
			var ele = document.createElement('script');
			ele.setAttribute('language', 'javascript');
			ele.setAttribute('type', 'text/javascript');
			ele.setAttribute('src',"https://agi-static.indiatimes.com/cms-common/ibeat.min.js");
			document.body.appendChild(ele);
		}
	}catch(e){}
}
function Get_Ckie(name){
	var value = "; " + document.cookie;
	var parts = value.split("; " + name + "=");
	if (parts.length == 2) return parts.pop().split(";").shift();
}

jQuery(document).ready(function(){
	//var i = 1;
	/*jQuery('.chooseLang a').on('click',function(event){  
			jQuery('.QuillAbstable').remove();
	});
	jQuery('body').on('click',function(){
		jQuery('.QuillAbstable').remove();
	});*/
	
	/*jQuery('body').on('mousemove',function(){
		if(jQuery('[data-action="comment-reply"]').length > 0 && jQuery('.QuillAbstable').length > 0 ){
			if(jQuery('.QuillKeyboardContainer').length >= i){
				i++;
				jQuery('.chooseLang a').on('click',function(event){  
					jQuery('.QuillAbstable').remove();
				});
			}
		}
	});*/
});
//alert('wapads_'+<?php the_ID();?>);
initGoogleAds(wapads_<?php the_ID();?>);

</script>

<?php
}
?>
<script>
if(!TimesGDPR.common.consentModule.isEUuser()){	
	var tilDmpWebSrc = '<?php echo $my_settings['footer_dmp_tag'];?>';
	if(typeof( tilDmpWebSrc ) != 'undefined' ){
            var tilDmpSrc  = tilDmpWebSrc;
            var tilDmpScriptTag = document.createElement('script');
            tilDmpScriptTag.setAttribute('src', tilDmpSrc);
            document.body.appendChild(tilDmpScriptTag);
        }


}
</script>
<?php 

if(!isInitAd() && !isMobile()) { 
    echo $my_settings['footer_google_tag'];     
}else if(!isInitAd() && isMobile()){
    echo $my_settings['footer_google_tags_mobile'];
} 

?>
<!-- <style>
    html, body, div, span, object, iframe, h1, h2, h3, h4, h5, h6, p, blockquote, pre, a, abbr, acronym, address, big, cite, code, del, dfn, em, img, ins, kbd, q, s, samp, small, strike, strong, sub, sup, tt, var, b, u, i, center, dl, dt, dd, ol, ul, li, fieldset, form, label, legend, div, table, caption, tbody, tfoot, thead, tr, th, td, article, aside, canvas, details, embed{font-weight:inherit}
    a{color: #000;}
</style>-->
</body>
</html>
