<?php
/**
 * The template for displaying featured posts on the front page
 *
 * @package WordPress
 * @subpackage BlogsTheme
 * @since Blogs Theme 1.0
 */
global $my_settings;
$blog  = array_values(get_the_terms(get_the_ID(), 'blog' ))[0]; //[0];					
$authorName = get_the_author_meta('display_name');
?>
<div class="col-md-4" data-vr-contentbox="">
    <div class="article media">
        <a class="pull-left" pg="Blog_HomeFeatured_Author_Pos" href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ) ?>" title="<?php echo $my_settings['go_to_the_profile_of_txt']; ?> <?php echo $authorName ?>">
            <img class="media-object" src="<?php  echo get_user_avatar(get_the_author_meta('ID')); ?>" alt="<?php echo $my_settings['go_to_the_profile_of_txt']; ?> <?php echo $authorName ?>">
        </a>
        <div class="featured-ribbon">
            <?php echo $my_settings['featured_txt']; ?>                        
        </div>
        <div class="media-body">
        <?php the_title( '<h3 class="media-heading"><a href="' . esc_url( get_permalink() ) . '" pg="Blog_HomeFeatured_Pos" rel="bookmark" title="'.$my_settings['go_to_txt'].' '.get_the_title().'">', '</a></h3>' );?>
    	<div class="media-meta">
            <span class="date"><?php echo esc_attr( get_the_date() ).', <span class="time">'.esc_attr( get_the_time() ).'</span>  IST' ?></span> <br> 
        	<a pg="Blog_HomeFeatured_Author_Pos" href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' )) ?>" rel="author"  title="<?php echo $my_settings['go_to_the_profile_of_txt']; ?> <?php echo $authorName ?>"><?php echo get_the_author_meta('display_name') ?></a> in 
        	<a pg="Blog_HomeFeatured_Pos" href="<?php echo get_term_link($blog->term_id,'blog') ?>" title="<?php echo $my_settings['go_to_txt']; ?> <?php echo $blog->name; ?>"><?php echo $blog->name; ?></a> 
       	</div>
		</div>
	</div>
</div>
