<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Blogs_Theme
 * @since Blogs Theme 1.0
 */
?>
<?php global $my_settings; ?>
<?php $myTimesUrl = 'https://myt.indiatimes.com';
$ssoUrl = 'https://jsso.indiatimes.com';
$ssochannel = $my_settings['channel'];
        $fbclient = $my_settings['appid'];
        $gPlusclient = $my_settings['gPlusClient'];
        $home_url = home_url();
       // wp_localize_script( 'login_js', 'tLoginObj', array( 'pluginsUrl' => plugins_url(), 'ssochannel' => $ssochannel, 'ssoUrl'=>$ssoUrl, 'home_url' => $home_url, 'fbclient'=>$fbclient, 'gPlusclient' =>$gPlusclient,'myTimesUrl'=>$myTimesUrl ) );
        $tLoginObj =array( 'pluginsUrl' => plugins_url(), 'ssochannel' => $ssochannel, 'ssoUrl'=>$ssoUrl, 'home_url' => $home_url, 'fbclient'=>$fbclient, 'gPlusclient' =>$gPlusclient,'myTimesUrl'=>$myTimesUrl );
   ?>
<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() . '/css/gdpr.css?ver=1.1' ;?>" />	
<script type="text/javascript" src="<?php echo get_template_directory_uri()?>/js/gdpr.js?ver=1.2"></script>
	<?php 
	$seconds_to_cache = 1800;
    if(is_single()){
        $seconds_to_cache = 3600;
    }
    $ts = gmdate("D, d M Y H:i:s", time() + $seconds_to_cache) . " GMT";
    header("Cache-Control: public, max-age=$seconds_to_cache, must-revalidate");
    header("Connection: Keep-Alive");
    header("Keep-Alive: timeout=15, max=98");    
    header("Expires: $ts");
    header("pragma: cache");
	?>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php wp_head(); ?>
	
	

	<meta name="robots" content="NOODP" />
	<?php if($_SERVER['REQUEST_URI'] == '/blocked') { ?><META CONTENT="NOINDEX, NOFOLLOW" NAME="ROBOTS"><?php } ?>
	<?php /*<title><?php wp_title('-', true, 'right'); ?></title>*/ ?>
	<link type="image/x-icon" href="<?php echo $my_settings['favicon'];?>" rel="shortcut icon">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="<?php echo get_template_directory_uri()?>/js/html5shiv.js"></script>
    <script src="<?php echo get_template_directory_uri()?>/js/respond.min.js"></script>
    <![endif]-->
	<?php echo $my_settings['ga'];?>
	<script type="text/javascript">
		var $ = jQuery.noConflict();
		var site_url = '<?php echo site_url(); ?>';
		var curPage = '';
		var pgname = '';
		var _gaq = Object();
		var isMobile = window.matchMedia("only screen and (max-width: 760px)");
		var isQuillEnabled = true;
		if (isMobile.matches) {
			isQuillEnabled = false;
		}
	</script>
	<?php if(!isInitAd()) { echo $my_settings['google_tag_js_head']; } ?>
	<script type="text/javascript">
	function _getCookie(c_name) {
		var i,x,y;
		var cookieStr = document.cookie.replace("UserName=", "UserName&");
		var ARRcookies=cookieStr.split(";");

		for (i=0;i<ARRcookies.length;i++)
		{
			x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
			y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
			x=x.replace(/^\s+|\s+$/g,"");
			if (x==c_name)
			{
				return unescape(y);
			}
		 }
	}

	function _setCookie(cname, cvalue, exdays) {
		var d = new Date();
		d.setTime(d.getTime() + (exdays*24*60*60*1000));
		var expires = "expires="+ d.toUTCString();
		document.cookie = cname + "=" + cvalue + "; " + expires + "; path=/";
	}
	</script>
</head>

<?php 
	$currentPage = (get_query_var('paged')) ? get_query_var('paged') : 1;
	if(array_key_exists('esi_status',$my_settings) && $my_settings['esi_status'] == 'LIVE'){
		if (is_front_page() && $currentPage == 1) {
			//echo $my_settings['ctn_homepage_rhs'];
			if(array_key_exists('esi_header',$my_settings)){
				echo str_replace('##ESIIDS##',$my_settings['esi_homepage_ids'],$my_settings['esi_header']);
			}
		} else if(is_single()){
			//echo $my_settings['ctn_article_show_rhs'];
			if(array_key_exists('esi_header',$my_settings)){
				echo str_replace('##ESIIDS##',$my_settings['esi_article_show_ids'],$my_settings['esi_header']);
			}
		} else {
			//echo $my_settings['ctn_article_list_rhs'];
			if(array_key_exists('esi_header',$my_settings)){
				echo str_replace('##ESIIDS##',$my_settings['esi_article_list_ids'],$my_settings['esi_header']);
			}
		}
	}
?>

<body <?php body_class(); ?>>
    <div style="display:none" class="" id="login-popup">
	<button class="close-btn" type="button">+</button>
	<div id="user-sign-in"></div>
</div>
<?php if ( !is_front_page() && $_SERVER['REQUEST_URI'] != '/blocked') { ?>
	<?php echo get_option('ad_blocker_js_code'); ?>
<?php } ?>
<?php if(!isInitAd()) { ?>
<div id="fb-root"></div>
<script>
  window.fbAsyncInit = function() {
    // init the FB JS SDK
    FB.init({appId: '<?php echo $my_settings['fbappid'];?>',oauth:true, status: true, cookie: true, xfbml: true});
    // Additional initialization code such as adding Event Listeners goes here
  };
  // Load the SDK asynchronously
  (function(){
     // If we've already installed the SDK, we're done
     if (document.getElementById('facebook-jssdk')) {return;}

     // Get the first script element, which we'll use to find the parent node
     var firstScriptElement = document.getElementsByTagName('script')[0];

     // Create a new script element and set its id
     var facebookJS = document.createElement('script'); 
     facebookJS.id = 'facebook-jssdk';

     // Set the new script's source to the source of the Facebook JS SDK
     facebookJS.src = '//connect.facebook.net/en_US/all.js';

     // Insert the Facebook JS SDK into the DOM
     firstScriptElement.parentNode.insertBefore(facebookJS, firstScriptElement);
   }());
   
function checkLogin(){
	TimesGDPR.common.consentModule.gdprCallback(function(){
            if(!window._euuser){
	require(["login"],function(login){
		login.init();
	});
	}});	
}
function callfunctiononload(){
	/*if(isQuillEnabled){
		var scrt=document.createElement('script');
		scrt.type = 'text/javascript';
		scrt.src='<?php echo get_template_directory_uri(); ?>/js/quill-compiled.js?ver=1';
		document.getElementsByTagName("head")[0].appendChild(scrt);
		
		// all cool browsers
		try { scrt.onload = applyingQuill; } catch(ex){}
		// IE 6 & 7
		scrt.onreadystatechange = function() {
			if (this.readyState == 'complete') {
				try { applyingQuill(); } catch(ex){}
			}
		}
	} else {
		try { applyingComments(); } catch(ex){}
	}*/
	if(isQuillEnabled){
		try { applyingQuill(); } catch(ex){}
	} else {
		try { applyingComments(); } catch(ex){}
	}
}
function applyingQuill(){
	try { applyingQuillComments(); } catch(ex){}
	if($('#search-form-field').length > 0){
		Quill.init('search-form-field', false, null, null, '<?php echo $my_settings['quill_lang']; ?>');
		Quill.setLanguage('search-form-field', '<?php echo $my_settings['quill_lang']; ?>');
	}
	if($('#search-form-top-field').length > 0){
		Quill.init('search-form-top-field', false, null, null, '<?php echo $my_settings['quill_lang']; ?>');
		Quill.setLanguage('search-form-top-field', '<?php echo $my_settings['quill_lang']; ?>');
	}
	/*if($('#search-form-topmobile-field').length > 0){
		Quill.init('search-form-topmobile-field', false, null, null, '<?php echo $my_settings['quill_lang']; ?>');
		Quill.setLanguage('search-form-topmobile-field', '<?php echo $my_settings['quill_lang']; ?>');
	}*/
}
$(function(){
	callfunctiononload();
	require(["tiljs/event"],function(event){
		event.subscribe("user.login",function(){
			$( '#timespointframe' ).attr( 'src', function () { return $( this )[0].src; } );
		});
		event.subscribe("user.logout",function(){
			$( '#timespointframe' ).attr( 'src', function () { return $( this )[0].src; } );
		});
	})
	
});
$(window).load(function() {
	var themeClass = '<?php echo implode(' ', get_body_class()); ?>';
	$('body').addClass(themeClass);
});

var showInitAd = '<?php echo get_option('show_ad'); ?>';
var initAdUrl = '';
if(_getCookie('intAd') !== '1'){
	if(isMobile.matches){
		if(showInitAd == 'Both' || showInitAd == 'WAP'){
			initAdUrl = '<?php bloginfo('url'); ?>/defaultinterstitial?v=mob';
		}
	} else {
		if(showInitAd == 'Both' || showInitAd == 'WEB'){
			initAdUrl = '<?php bloginfo('url'); ?>/defaultinterstitial';
		}
	}
}
if(initAdUrl){
	_setCookie('prevurl',window.location.href,1);
	window.location.replace(initAdUrl);
}
var languageChannelName = '<?php echo $my_settings['channel'];?>';
var siteId = '<?php echo $my_settings['site_id'];?>';
var domainName = '<?php echo $my_settings['domain'];?>';
var ofcommenthostid = '<?php echo $my_settings['ofcommenthostid'];?>';
var ofcommentchannelid = '<?php echo $my_settings['ofcommentchannelid'];?>';
var appid = '<?php echo $my_settings['appid'];?>';
var commentText = <?php echo $my_settings['comment_text'];?>;
</script>
<script>
    var tLoginObj = {
  //"pluginsUrl":"http://local.blogs.navbharattimes.indiatimes.com/wp-content/plugins",
  "ssochannel":'<?php echo $my_settings['channel'];?>',
  "ssoUrl":"<?php echo $my_settings['ssoUrl'];?>",
  "home_url":"<?php echo home_url();?>",
  "fbclient":"<?php echo $my_settings['appid'];?>",
  "gPlusclient":"<?php echo $my_settings['gPlusClient'];?>",
  "myTimesUrl":"<?php echo $my_settings['myTimesUrl'];?>",
  "MytCommentApiKey" :"<?php echo MYT_COMMENTS_API_KEY;?>",
  "siteId" :"<?php echo $my_settings['site_id'];?>",
  "domainName" :"<?php echo $my_settings['domain'];?>",
  "isEnvironment":"<?php echo getenv("environment")?>",
  "socialappsintegrator":"<?php echo $my_settings["socialappsintegrator"]?>"
  
};
    </script>
<?php
	if(isset($_GET['source']) && $_GET['source'] == 'app'){
		return;
	}else if(isset($_GET['frmapp']) && $_GET['frmapp'] == 'yes'){
            return;            
        } else {
		include 'custom-header.php';
	}
}
     
?>
<style>
    .user_area span{font-size:13px;color:#000;cursor:pointer}
.user-controls .user-image{vertical-align:middle;padding-right:5px}
.user-controls a span{font-size:13px;color:#000;cursor:pointer}
.user_area .user-controls{display:inline-block;position:relative; margin:-5px 0 0 0}
.user_area .user-controls .user_name{display:inline-block;font-weight:600}
.user_area .dropdown{width:105px;top:10px;right:-12px;background:#f2f2f2;padding:10px 10px 5px;z-index:-1;position:absolute;transition:all .5s;-webkit-transition:all .5s;opacity:0;visibility:hidden}
.user_area .dropdown:before{content:"";border-right:6px solid transparent;border-left:6px solid transparent;border-bottom:8px solid #f2f2f2;position:absolute;top:-7px;right:10px;z-index:1}.user_area .dropdown li{list-style:none;margin:0 0 4px;font-size:12px;line-height:16px;text-align:left;color:#ff0}.user_area .user-controls:hover .dropdown{opacity:1;top:31px;z-index:999;visibility:visible}.user_area .dropdown li a, .user_area .dropdown li span{color:#000; margin:0 5px; font-size:11px;}.user_area .dropdown li span:hover, .user_area .dropdown li a:hover{color: #e44349}.user_area .login{display:block;margin-top:0px}.user_name .icon_down{display:inline-block;width:14px;height:17px;position:relative}
.user_name .icon_down:after{position:absolute;content:"";border:solid #000;width:7px;height:7px;border-width:0 2px 2px 0;top:8px;right:3px;transform:rotate(45deg);-webkit-transform:rotate(45deg);box-sizing:border-box}#login-popup{background:rgba(0,0,0,.6);position:fixed;width:100%;height:100%;left:0;top:0;overflow-y:auto;overflow-x:hidden;cursor:default;opacity:0;z-index:-1;transition:all .5s}#login-popup,#login-popup *{box-sizing:border-box;-webkit-box-sizing:border-box}


    #login-popup { background:rgba(0,0,0,.6); position:fixed; width:100%; height:100%; left:0; top:0; overflow-y:auto; overflow-x:hidden; cursor:default; opacity:0; z-index:-1; transition:all .5s; }
#login-popup, #login-popup * { box-sizing:border-box; -webkit-box-sizing:border-box; }
#login-popup.active { opacity:1; z-index:1000000; }

#user-sign-in { background:#f7f7f7; width:408px; height:100%; min-height:100%; padding:46px 30px; overflow-y:auto; box-shadow:0 1px 12px 0 rgba(0, 0, 0, 0.3); position:absolute; right:-408px; top:0; transition:all .5s; }
#login-popup.active #user-sign-in { right:0; left:initial;}

/*--- Loader ---*/
#user-sign-in.loader { height:inherit; overflow:hidden; }
#user-sign-in.loader:before { position:fixed; content: ''; background: rgba(0,0,0,.5); height: 100%; width: inherit; right: 0; top: 0; z-index: 11; }
#user-sign-in.loader:after { position:absolute; content:''; width:50px; height:50px; left:50%; top:50%; margin-left:-25px; /*margin-top:-25px;*/ border:6px solid rgba(0,0,0,0); -webkit-animation:spin 1s linear infinite; animation:spin 1s linear infinite; border-radius:50%; border-top-color:#ff394a; border-right-color:#ff394a; z-index:12; }

@-webkit-keyframes spin {
  from { -webkit-transform:rotate(0deg); }
  to { -webkit-transform:rotate(360deg); }
}

@keyframes spin {
  from { transform:rotate(0deg); }
  to { transform:rotate(360deg); }
}
#user-sign-in > div { border:1px solid #d9d9d9; padding:20px; height:auto; min-height:100%; }
#user-sign-in a { text-decoration:none; }
#login-popup .close-btn { font-family: 'proxima-regular1', sans serif; background:none; border:0; font-size:34px; line-height:1; color:#7b7b7b; transform:rotate(-45deg); position:absolute; right:-400px; top:5px; z-index:1; transition:all .5s; }
#login-popup.active .close-btn { right:15px; }
/*#login-popup span { cursor:initial; }*/

#user-sign-in .form ul li { list-style:none; margin-bottom:22px; }

#user-sign-in .user-icon { margin:-45px auto 25px; width:68px; background:#f7f7f7; text-align:center; }
#user-sign-in .user-icon img { width:44px; height:44px; border-radius:50%; border:2px solid #ddd; vertical-align:middle; }

#login-popup.mweb #user-sign-in{width:100%;padding:46px 10px;}
#login-popup.mweb #user-sign-in > div{border:0; padding: 30px 0 10px 0;}
#login-popup.mweb #user-sign-in .user-icon{display:none;}

#socialConnectImgDiv { margin-bottom:24px; }
#socialConnectImgDiv button { border:0; border-radius:2px; font-size:14px; height:40px; line-height:40px; padding-left:50px; position:relative; display:block; width:100%; text-align:left; font-weight:500; box-shadow:0 2px 2px 0 rgba(0, 0, 0, 0.24), 0 0 2px 0 rgba(0, 0, 0, 0.12); cursor:pointer; }
#socialConnectImgDiv button::before { position:absolute; content:''; background:url(https://timesofindia.indiatimes.com/photo/64971829.cms) no-repeat; background-size:250px; }
#socialConnectImgDiv button + button, #sso-gplus-login { margin-top:16px; }
#socialConnectImgDiv .fb { background:#2553b4; color:#fff; }
#socialConnectImgDiv .fb::before { background-position:0 0; width:14px; height:29px; bottom:0; left:13px; }
#socialConnectImgDiv .gplus { background:#fff; color:#757575; }
#socialConnectImgDiv .gplus::before { background-position:-19px 0; width:19px; height:19px; left:8px; top:11px; }

#user-sign-in .heading { font-size:16px; line-height:1; color:#000; position:relative; text-align:center; margin-bottom:20px; }
#user-sign-in .heading span { display:inline-block; background:#f7f7f7; padding:0 10px; position:relative; z-index:1; color:#868686;}
#user-sign-in .heading:after { position:absolute; content:''; background:#d6d6d6; width:100%; height:1px; left:0; top:50%; }
#user-sign-in .heading.small { font-size:14px; color:#a2a2a2; font-weight:normal; }
#user-sign-in .heading + p { margin-bottom:20px; }
#user-sign-in .secondary-link { display:inline-block; font-size:12px; line-height:18px; font-weight:500; color:#5b9ae4; text-transform:uppercase; margin-top:5px; }
#user-sign-in .secondary-link.disabled {cursor: default; color:#b0b0b0; text-decoration: none;}
#user-sign-in .input-field { padding-left:23px; position:relative; }
#user-sign-in .input-field::before { position:absolute; content:''; background-image:url(https://timesofindia.indiatimes.com/photo/64971829.cms); background-repeat:no-repeat; background-size:250px; left:0; }
#user-sign-in .input-field.email::before { background-position:-40px 0; width:15px; height:11px; top:10px; }
#user-sign-in .input-field.user-name::before { background-position:-60px 0; width:13px; height:15px; top:8px; }
#user-sign-in .input-field.password::before { background-position:-75px 0; width:13px; height:16px; top:8px; }
#user-sign-in .input-field.password .view-password, #user-sign-in .input-field.password .hide-password { position:absolute; width:30px; height:27px; top:0; right:0; z-index:100; cursor:pointer; }
#user-sign-in .input-field.password .view-password:before, #user-sign-in .input-field.password .hide-password:before { position:absolute; content:''; background:url(https://timesofindia.indiatimes.com/photo/64971829.cms) no-repeat -105px 0; background-size:250px; width:20px; height:12px; left:5px; top:7px; }
#user-sign-in .input-field.password .hide-password:before { opacity:1; }
#user-sign-in .input-field.password .view-password { opacity:.4; }
#user-sign-in .input-field.mobile-no::before { width:12px; height:18px; top:5px; background-position:-90px 0; }
#user-sign-in .input-field.mobile-no p { border-bottom:1px solid #888; }
#user-sign-in .input-field.mobile-no p .country-code { color:#b0b0b0; padding:0 5px; height:27px; line-height:27px; vertical-align:middle; }
#user-sign-in .input-field.mobile-no p input { border-bottom:0; display:inline-block; width:calc(100% - 45px); }
#user-sign-in .form { margin-bottom:20px; }
#user-sign-in .form .submit { margin-bottom:0; }
#user-sign-in input { display:block; width:100%; line-height:normal; border:0; font-size:14px; }
#user-sign-in input[type=text], #user-sign-in input[type=password], #user-sign-in input[type=number] { height:28px; border-bottom:1px solid #888; background:none; color:#222; border-radius:0; }
:-ms-input-placeholder { color:#222 !important; }
#user-sign-in input[type=button], #user-sign-in input[type=submit] { background:#be281a; box-shadow:0 2px 2px 0 rgba(0, 0, 0, 0.24), 0 0 2px 0 rgba(0, 0, 0, 0.12); height:40px; color:#fff; text-transform:uppercase; font-weight:500; -webkit-border-radius:2px; -moz-border-radius:2px; border-radius:2px; -webkit-appearance:none; -moz-appearance:none; cursor:pointer; }
#user-sign-in input:disabled { opacity:.4; }
#user-sign-in .regenerate-otp, #user-sign-in .subtext { display:inline-block; color:#b0b0b0; margin-right:5px; margin-top: 5px; font-size: 12px; }
#user-sign-in .form .error { color:#be281a; }
#user-sign-in .form .error input { border-color:#be281a; color:#be281a; }

input:-webkit-autofill, textarea:-webkit-autofill, select:-webkit-autofill { box-shadow:0 0 0 17px #f7f7f7 inset; }

/* --- Checkbox --- */ 
#user-sign-in .checkbox { display:block; margin:0 0 12px; }
#user-sign-in .checkbox input[type="checkbox"] { display:none; }
#user-sign-in .checkbox label { display:inline-block; padding-left:23px; cursor:pointer; vertical-align:middle; font-weight:normal; line-height:20px; color:#000; position:relative; }
#user-sign-in .checkbox label:before { content:""; position:absolute; left:0px; top:2px; height:14px; width:14px; line-height:15px; background:#ffffff no-repeat; -webkit-transition:all 0.5s; -moz-transition:all 0.5s; -ms-transition:all 0.5s; -o-transition:all 0.5s; transition:all 0.5s; border:1px solid #b1b1b1; border-radius:2px; }
#user-sign-in .checkbox label a { color:#5b9ae4; }
#user-sign-in .checkbox input[type="checkbox"]:checked + label:before { background:#3696de; border-color:#3696de; }
#user-sign-in .checkbox input[type="checkbox"]:checked + label:after { position:absolute; content:''; border:solid #fff; width:5px; height:10px; left:5px; top:2px; border-width:0 2px 2px 0; transform:rotate(45deg); }

.forget-password { display:block; font-size:12px; line-height:18px; color:#000; font-weight:600; text-align:center; text-transform:uppercase; margin-top:20px; text-decoration: underline !important; }
.forget-password.disabled { cursor: default; color: #b0b0b0; text-decoration: none !important; }
#user-sign-in p { font-size:14px; line-height:18px; color:#222; }
#user-sign-in .password-conditions { padding:10px 0 0; display:none; }
#user-sign-in .password-conditions p { margin-bottom:10px; }
#user-sign-in .password-conditions ul li { font-size:14px; line-height:1; color:#666; margin-bottom:5px; position:relative; padding-left:10px; }
#user-sign-in .password-conditions ul li::before { position:absolute; content:''; background:#b2b2b2; width:6px; height:6px; left:0; top:3px; border-radius:50%; border:1px solid transparent; }
#user-sign-in .password-conditions ul li.error { color:#b12e2a }
#user-sign-in .password-conditions ul li.error::before { background-color:transparent; border-color:#b12e2a; }
#user-sign-in .password-conditions ul li.success { color:#43b23f; }
#user-sign-in .password-conditions ul li.success::before { background-color:#43b23f; }

#user-sign-in .errorMsg { font-size:12px; line-height:1; color:#be281a; display:none; margin-top:5px; }
#user-sign-in .successMsg { font-size:12px; line-height:1; color:#43b23f; display:none; margin-top:5px; }
#sso-pwdDiv, #sso-otpLoginDiv, #changeEmailIdDiv { display:none; }

#user-sign-in .mandatory-box { background:#fff; padding:20px; margin:0 -20px -20px; }
#user-sign-in .mandatory-box p { font-size:12px; line-height:20px; color:#404040; }
body.disable-scroll {
    overflow: hidden;
    margin-right: 17px;
}

/*Registration Success CSS*/

#user-sign-in .fp-success{font-size: 20px; position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%); width: 70%; text-align: center; color: #404040;}
#user-sign-in .fp-success i{background: #43b23e; width: 41px; height: 41px; border-radius: 50%; position:relative; margin: 20px auto; display: block;}
#user-sign-in .fp-success i:before{width: 17px; height: 8px; border-bottom: 3px solid #fff; border-left: 3px solid #fff; content: ""; position: absolute; left: 11px; top: 13px; transform: rotate(-45deg);}

#user-sign-in .verified{padding: 0px 0 20px; font-size: 14px; line-height: 20px;}
#user-sign-in .verified div:first-child{color: #404040;}
#user-sign-in .verified div:nth-child(2){color: #222;}

#user-sign-in .verified div:nth-child(2) strong{line-height: 14px; display: inline-block; vertical-align: top;}
#user-sign-in .verified .tick, #user-sign-in .mn-verified .tick{display: inline-block; width: 14px; height: 14px; vertical-align: top; background: #43b23e; margin-left: 10px; border-radius: 50%; position: relative;}
#user-sign-in .verified .tick:before, #user-sign-in .mn-verified .tick:before{width: 7px; height: 3px; border-bottom: 1px solid #fff; border-left: 1px solid #fff; content: ""; position: absolute; left: 3px; top: 4px; transform: rotate(-45deg);}
#user-sign-in .success-wrapper{position: absolute; left: 50%; top: 50%; transform: translate(-50%, -50%); text-align: center; width: 100%;}
#user-sign-in .success-user{display: block; margin: 0 auto 20px; width: 42px; height: 42px; background: url(https://timesofindia.indiatimes.com/photo/64971829.cms) no-repeat -130px 0; background-size: 250px;}
#user-sign-in .fp-success-msg{font-size: 20px; text-align: center; color: #404040; line-height: 1;}
#user-sign-in .signup-section, #user-sign-in .signin-section{margin-top: -29px;}

.powered-by{width: 90%; text-align: center; margin: 45px auto 0;}
.powered-by img{width: 220px;}
.powered-by:before{content:"Powered by"; text-transform: uppercase; color: #868686; font-size: 12px; display: block; margin: 0 0 10px 0; font-weight: 600;}

.teams-logo{padding-top:40px; text-align: center;}
.teams-logo span{ font-weight: bold; color: #000; font-size: 15px; display:block; text-transform: capitalize;}
.teams-logo a{background:url(https://static.toiimg.com/photo/65470945.cms)no-repeat 0 -10000px; width: 32px; height: 32px; display: inline-block; margin:10px 4px 0 0;}
.teams-logo .toi{background-position: -6px -16px;}
.teams-logo .et{background-position: -6px -66px;}
.teams-logo .nbt{background-position: -6px -116px;}
.teams-logo .sm{background-position: -6px -166px;}
.teams-logo .st{background-position: -6px -217px;}
.teams-logo .gaana{background-position: -6px -268px;}
.teams-logo .itimes{background-position: -6px -318px;}
.teams-logo .tp{background-position: -6px -369px;}


@media screen and (max-width:325px){
.teams-logo{padding-top:10px;}
.teams-logo a{margin: 10px 1px 0 0;}
}
  </style>

