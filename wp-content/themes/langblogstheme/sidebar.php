        <?php
          //getTrending();
          global $my_settings;
          ?>              
    <div class="panel" style="min-height:300px;" pg="populartag_readmore">
      <ul class="nav nav-tabs primary-tabs" role="tablist">
         <li class="active"><a pg="mdtab_pos" href="#discussed" role="tab" class="adj-font-tabs" data-toggle="tab" style="text-align:center;"><?php echo $my_settings['most_discussed_txt']; ?></a></li>
         <li><a pg="mrtab_pos" href="#read" role="tab" class="adj-font-tabs" data-toggle="tab" style="text-align:center;"><?php echo $my_settings['most_read_txt']; ?></a></li>
      </ul>
      <div class="tab-content">
      <div class="tab-pane active" id="discussed">
        <ul class="nav nav-tabs" role="tablist" style="display:none;">
         <li class="active"><a href="#discussed-today" role="tab" data-toggle="tab">Today</a></li>
         <li><a href="#discussed-week" role="tab" data-toggle="tab">This Week</a></li>
         <li><a href="#discussed-month" role="tab" data-toggle="tab">This Month</a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
         <ul class="tab-pane active list-unstyled trending" id="discussed-today" data-vr-zone="Discussed Today">
            <?php getMostDiscussedWeek() ?>
         </ul>
         <ul class="tab-pane list-unstyled trending" id="discussed-week">
            <?php getMostDiscussedWeek() ?>
         </ul>
         <ul class="tab-pane list-unstyled trending" id="discussed-month">
            <?php getMostDiscussedMonth() ?>
         </ul>
        </div>
      </div>

      <div class="tab-pane" id="read">
        <ul class="nav nav-tabs" role="tablist" id="readtabs" style="display:none;">
         <li class="active"><a href="#read-today" role="tab" data-toggle="tab">Today</a></li>
         <li><a href="#read-week" role="tab" data-toggle="tab">This Week</a></li>
         <li><a href="#read-month" role="tab" data-toggle="tab">This Month</a></li>
        </ul>
      <!-- Tab panes -->
        <div class="tab-content">
          <ul class="tab-pane active list-unstyled trending" id="read-today" data-vr-zone="Read Today">
			<div style="width: 100%; height: 100%; position: relative;height:200px;"><span style=" position: absolute; top: 75px; left:0px; color: #000; width: 100%; text-align: center; font-size: 14px;"><img src="https://timesofindia.indiatimes.com/photo/20259450.cms" style="width: 16px; height: 16px; display: block; margin: 0 auto 10px;">Loading...</span></div>
          </ul>
          <ul class="tab-pane list-unstyled trending" id="read-week"></ul>
          <ul class="tab-pane list-unstyled trending" id="read-month"></ul>         
        </div>
      </div>
    </div>
  </div>
    
    <div class="panel" pg="populartag_pos">
      <h3 class="panel-title"><?php echo $my_settings['popular_tags_txt']; ?></h3>
      <?php
        wp_tag_cloud('largest=22&smallest=8&number=50&orderby=count&order=RAND&number=25');
        ?>
    </div>
    <div class="panel" pg="recentlyjoined_pos">
      <?php $recentlyauthorslist = getRecentlyJoinedAuthors(); ?>
      <h3 class="panel-title"><?php echo $my_settings['recently_joined_authors_txt']; ?> 
      <?php if($recentlyauthorslist['count'] >= 10) { ?>
      <small style="float: right;"><a href="<?php echo site_url().'/authors' ?>"> <?php echo $my_settings['more_txt']; ?> &raquo; </a></small>
      <?php } ?>
      </h3>
      <?php echo $recentlyauthorslist['html']; ?>
    </div>
    <?php
    $currentPage = (get_query_var('paged')) ? get_query_var('paged') : 1;
    if (is_front_page() && $currentPage == 1) {
		if(array_key_exists('esi_status',$my_settings) && $my_settings['esi_status'] == 'TEST' && isset($_GET['isEsi']) && $_GET['isEsi'] == 'YES'){
			echo $my_settings['esi_homepage_rhs'];
		}else if(array_key_exists('esi_homepage_rhs',$my_settings) && array_key_exists('esi_status',$my_settings) && $my_settings['esi_status'] == 'LIVE'){
			echo $my_settings['esi_homepage_rhs'];
		}else{
			echo $my_settings['ctn_homepage_rhs'];
		}
	} else if(is_single()){
		if(array_key_exists('esi_status',$my_settings) && $my_settings['esi_status'] == 'TEST' && isset($_GET['isEsi']) && $_GET['isEsi'] == 'YES'){
			echo $my_settings['esi_article_show_rhs'];
		}else if(array_key_exists('esi_article_show_rhs',$my_settings) && array_key_exists('esi_status',$my_settings) && $my_settings['esi_status'] == 'LIVE'){
			echo $my_settings['esi_article_show_rhs'];
		}else{
      if(!isMobile()){
			  echo $my_settings['ctn_article_show_rhs'];
      }
		}
	} else {
		if(array_key_exists('esi_status',$my_settings) && $my_settings['esi_status'] == 'TEST' && isset($_GET['isEsi']) && $_GET['isEsi'] == 'YES'){
			echo $my_settings['esi_article_list_rhs'];
		}else if(array_key_exists('esi_article_list_rhs',$my_settings) && array_key_exists('esi_status',$my_settings) && $my_settings['esi_status'] == 'LIVE'){
			echo $my_settings['esi_article_list_rhs'];
		}else{
			echo $my_settings['ctn_article_list_rhs'];
		}
	}
    ?>
    <div class="panel">
      <div class="addwrapper">
        <?php /*<iframe scrolling="no" frameborder="0" vspace="0" hspace="0" marginheight="0" marginwidth="0" height="100" width="300" src="http://ad.doubleclick.net/N7176/adi/TOI/TOI_EkThaVillianSlug_300x100;sz=300x100;ord=3328324891626835?"></iframe>*/ ?>
      </div>
    </div>

    <div class="panel">
      <div class="addwrapper">
        <?php  
        if(is_single()){
            echo $my_settings['google_ad_right_2'];
        }else{
            echo $my_settings['google_ad_right_2'];
        }
        
        ?>
         
      </div>
    </div>
    <?php 
      if(!isset($_GET['source']) || $_GET['source'] != 'app'){ ?>
    <div class="panel" id="fb-likebox" pg="likebox_Pos">
      <h3 class="panel-title"><?php echo $my_settings['like_us_txt']; ?></h3>
      <div class="addwrapper">
          <iframe src="https://www.facebook.com/plugins/page.php?href=<?php echo $my_settings['fblikeurl'];?>&tabs&width=304&height=214&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true" width="304" height="214" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
        <!--<div class="fb-like-box" data-href="<?php echo $my_settings['fblikeurl'];?>" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="false" data-show-border="true"></div>-->
      </div>      
    </div>
    <?php
    }
    ?>
    <div style="text-align: center;">
      <?php
        if(!isset($_GET['source']) || $_GET['source'] != 'app')
          include 'timespoints.php';
        ?> 
    </div>
