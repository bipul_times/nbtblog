<?php

$lang = isLang();
if(!empty($lang)){
	if (strpos($_SERVER['HTTP_HOST'], 'readerblogs.navbharattimes.indiatimes.com') !== false || strpos($_SERVER['HTTP_HOST'], 'vsp1nbtreaderblogs.indiatimes.com') !== false ) {
        require get_template_directory() . '/constants-readernbt.php';
    }else{
        require get_template_directory() . '/constants-'.$lang.'.php';
    }
}
require get_template_directory() . '/bloghooks.php';

/**
 * restrict authors to only being able to view media that they've uploaded
 */
add_filter('parse_query', 'restrict_posts');
function restrict_posts($wp_query)
{
    //are we looking at the Media Library or the Posts list?
    if (strpos($_SERVER['REQUEST_URI'], '/wp-admin/upload.php') !== false || strpos($_SERVER['REQUEST_URI'], '/wp-admin/edit.php') !== false) {
        //user level 5 converts to Editor
        if (!current_user_can('level_5')) {
            //restrict the query to current user
            global $current_user;
            $wp_query->set('author', $current_user->id);
        }
    }
}

/**
 * restrict authors to only being able to view media that they've uploaded
 */
add_action('pre_get_posts', 'restrict_media_library');
function restrict_media_library($wp_query_obj)
{
    global $current_user, $pagenow;
    if (!is_a($current_user, 'WP_User'))
        return;
    if ('admin-ajax.php' != $pagenow || $_REQUEST['action'] != 'query-attachments')
        return;
    if (!current_user_can('manage_media_library'))
        $wp_query_obj->set('author', $current_user->ID);
    return;
}


function z_add_style()
{
    echo '<style type="text/css" media="screen">
    th.column-thumb {width:60px;}
    .form-field img.taxonomy-image {border:1px solid #eee;max-width:300px;max-height:300px;}
    .inline-edit-row fieldset .thumb label span.title {width:48px;height:48px;border:1px solid #eee;display:inline-block;}
    .column-thumb span {width:48px;height:48px;border:1px solid #eee;display:inline-block;}
    .inline-edit-row fieldset .thumb img,.column-thumb img {width:48px;height:48px;}
    </style>';
}

// add image field in add form
function z_add_texonomy_field()
{
    wp_enqueue_media();
    echo '<div class="form-field">
    <label for="taxonomy_image">' . __('Image', 'zci') . '</label>
    <input type="text" name="taxonomy_image" id="taxonomy_image" value="" />
    <br/>
    <button class="z_upload_image_button button">' . __('Upload/Add image', 'zci') . '</button>
    </div>' . z_script();
    
}

// add image field in edit form
function z_edit_texonomy_field($taxonomy)
{
    wp_enqueue_media();
    if (z_taxonomy_image_url($taxonomy->term_id, NULL, TRUE) == Z_IMAGE_PLACEHOLDER)
        $image_text = "";
    else
        $image_text = z_taxonomy_image_url($taxonomy->term_id, NULL, TRUE);
    echo '<tr class="form-field">
    <th scope="row" valign="top"><label for="taxonomy_image">' . __('Image', 'zci') . '</label></th>
    <td><img class="taxonomy-image" src="' . z_taxonomy_image_url($taxonomy->term_id, NULL, TRUE) . '"/><br/><input type="text" name="taxonomy_image" id="taxonomy_image" value="' . $image_text . '" /><br />
    <button class="z_upload_image_button button">' . __('Upload/Add image', 'zci') . '</button>
    <button class="z_remove_image_button button">' . __('Remove image', 'zci') . '</button>
    </td>
    </tr>' . z_script();
    ?>
    <tr class="form-field">
        <th> Add/Remove authors </th>
        <td> 
          <div>
              <p> <input type="text" id="select-user" name="select-user" class="newtag form-input-tip" size="16" autocomplete="off" value="">
                <input type="hidden" id="author-input" name="author-input" />
                <div id="selector-dd" style="display:none;min-width:50px;z-index: 500;float: left;position: absolute;background: white;border: 1px solid;opacity: .8;margin-left: 1px;padding: 2px;">
                </div>
                <p class="howto">Type at least 3 letters and select user from dropdown. Hit update button when done</p>
                <div class="tagchecklist">
                </div>
            </div>
        </td>
    </tr>
    <?php
    echo '<script type="text/javascript">';
    echo 'var taxonomyId ="' . $taxonomy->term_id . '";';
    echo 'var authorNames = []; var authorIds = [];';
    $blogAuthorIds = get_option("blogAuthors_" . $taxonomy->term_id);
    if ($blogAuthorIds && !empty($blogAuthorIds)) {
        foreach ($blogAuthorIds as $authorId) {
            if (!empty($authorId)) {
                $author = get_user_by('id', $authorId);
                echo 'authorIds.push("' . $authorId . '");';                
                echo 'authorNames.push("' . $author->user_nicename . '");';                
            }
        }
    }
    echo addAuthorAjaxScript();
}

// upload using wordpress upload
function addAuthorAjaxScript()
{
    return '
    jQuery(document).ready(function($) {

     $("#author-input").val(authorIds.join("-"))
     fillSpan();

     $("#select-user").keyup(function(){ 
        var inputValue = $("#select-user").val().trim();
        if(inputValue.length>2){
          jQuery.ajax({
              type: "POST",
              url: "' . site_url() . '/wp-admin/admin-ajax.php",
              dataType: "json",
              data: {
                action: "autoSuggestUsers",
                input: $("#select-user").val(),
            },
            success: function(data, textStatus, XMLHttpRequest){
                $("#selector-dd").empty();
                for (key in data.data ){
                  $("#selector-dd").append("<a id=user-"+key+">"+data.data[key]+"</a><br>");
                  $("#selector-dd").show();
              }
          },
          error: function(MLHttpRequest, textStatus, errorThrown){
            alert("hello"+errorThrown);
        }
    });
}
});

$("#selector-dd > a").live("click",function(e){
  var userId = e.target.getAttribute("id").split("-")[1];
  if(authorIds.indexOf(userId)==-1){
        //new author id added
    var userName = e.target.innerHTML;
    authorIds.push(userId);
    authorNames.push(userName);
    $("#author-input").val(authorIds.join("-"))
    fillSpan();
}
$("#selector-dd").hide();
$("#select-user").val("");  
});

function fillSpan(){
  $(".tagchecklist").empty();
  for (var key in authorIds ){
      $(".tagchecklist").append("<span><a id=author-"+authorIds[key]+" class=\'ntdelbutton\'>X</a>"+authorNames[key]+"</span>");      
  }
}

$(".ntdelbutton").live("click", function(e){
  var userId = e.target.getAttribute("id").split("-")[1];
  var index =  authorIds.indexOf(userId);      
  authorIds.splice(index,1);
  authorNames.splice(index,1);
  $("#author-input").val(authorIds.join("-"))
  fillSpan();
  this.remove();
});
});
</script>';
}

// upload using wordpress upload
function z_script()
{
    return '<script type="text/javascript">
    jQuery(document).ready(function($) {
      var wordpress_ver = "' . get_bloginfo("version") . '", upload_button;
      $(".z_upload_image_button").click(function(event) {
        upload_button = $(this);
        var frame;
        if (wordpress_ver >= "3.5") {
          event.preventDefault();
          if (frame) {
            frame.open();
            return;
        }
        frame = wp.media();
        frame.on( "select", function() {
            // Grab the selected attachment.
            var attachment = frame.state().get("selection").first();
            frame.close();
            if (upload_button.parent().prev().children().hasClass("tax_list")) {
              upload_button.parent().prev().children().val(attachment.attributes.url);
              upload_button.parent().prev().prev().children().attr("src", attachment.attributes.url);
          }
          else
              $("#taxonomy_image").val(attachment.attributes.url);
      });
frame.open();
}
else {
  tb_show("", "media-upload.php?type=image&amp;TB_iframe=true");
  return false;
}
});

$(".z_remove_image_button").click(function() {
    $("#taxonomy_image").val("");
    $(this).parent().siblings(".title").children("img").attr("src","' . Z_IMAGE_PLACEHOLDER . '");
    $(".inline-edit-col :input[name=\'taxonomy_image\']").val("");
    return false;
});

if (wordpress_ver < "3.5") {
    window.send_to_editor = function(html) {
      imgurl = $("img",html).attr("src");
      if (upload_button.parent().prev().children().hasClass("tax_list")) {
        upload_button.parent().prev().children().val(imgurl);
        upload_button.parent().prev().prev().children().attr("src", imgurl);
    }
    else
        $("#taxonomy_image").val(imgurl);
    tb_remove();
}
}

$(".editinline").live("click", function(){  
  var tax_id = $(this).parents("tr").attr("id").substr(4);
  var thumb = $("#tag-"+tax_id+" .thumb img").attr("src");
  if (thumb != "' . Z_IMAGE_PLACEHOLDER . '") {
      $(".inline-edit-col :input[name=\'taxonomy_image\']").val(thumb);
  } else {
      $(".inline-edit-col :input[name=\'taxonomy_image\']").val("");
  }
  $(".inline-edit-col .title img").attr("src",thumb);
  return false;  
});  
});
</script>';
}

add_action('blog_add_form_fields', 'z_add_texonomy_field');
add_action('blog_edit_form_fields', 'z_edit_texonomy_field');
add_filter('manage_edit-blog_columns', 'z_taxonomy_columns');
add_filter('manage_blog_custom_column', 'z_taxonomy_column', 10, 3);

// save our taxonomy image while edit or save term
add_action('edit_term', 'z_save_taxonomy_image');
add_action('create_term', 'z_save_taxonomy_image');
function z_save_taxonomy_image($term_id)
{
    if (isset($_POST['taxonomy_image']))
        update_option('z_taxonomy_image' . $term_id, $_POST['taxonomy_image'], false);
    if (isset($_POST['author-input'])) {
        $authorIds = explode('-', $_POST['author-input']);
        $previousAuthorIds = get_option("blogAuthors_".$term_id);
        foreach ($authorIds as $id) {
            if(!in_array($id, $previousAuthorIds)){
                // this id was newly added
                $authoringBlogIds = get_user_meta($id,'authoringBlogIds',true);
                if (!in_array($term_id, $authoringBlogIds)) {
                    array_push($authoringBlogIds, $term_id);
                    update_user_meta($id, 'authoringBlogIds', $authoringBlogIds);
                }
            }
        }
        foreach ($previousAuthorIds as $id) {
            if(!in_array($id, $authorIds)){
                // this id was removed   
                $authoringBlogIds = get_user_meta($id,'authoringBlogIds',true);
                array_splice($authoringBlogIds, array_search($term_id, $authoringBlogIds), 1);
                update_user_meta($id, 'authoringBlogIds', $authoringBlogIds);
            }
        }
        update_option("blogAuthors_".$term_id, $authorIds, false);
    }
}

// get attachment ID by image url
function z_get_attachment_id_by_url($image_src)
{
    global $wpdb;
    $query = "SELECT ID FROM {$wpdb->posts} WHERE guid = '$image_src'";
    $id    = $wpdb->get_var($query);
    return (!empty($id)) ? $id : NULL;
}

// get taxonomy image url for the given term_id (Place holder image by default)
function z_taxonomy_image_url($term_id = NULL, $size = NULL)
{
    if (!$term_id) {
        if (is_category())
            $term_id = get_query_var('cat');
        elseif (is_tax()) {
            $current_term = get_term_by('slug', get_query_var('term'), get_query_var('taxonomy'));
            $term_id      = $current_term->term_id;
        }
    }
    
    $taxonomy_image_url = get_option('z_taxonomy_image' . $term_id);
    $attachment_id      = z_get_attachment_id_by_url($taxonomy_image_url);
    if (!empty($attachment_id)) {
        if (empty($size))
            $size = 'full';
        $taxonomy_image_url = wp_get_attachment_image_src($attachment_id, $size);
        $taxonomy_image_url = $taxonomy_image_url[0];
    }else {
        //check in backup
        $upload_dir = wp_upload_dir();
        $src = $upload_dir['basedir'].'/backup/'.$taxonomy_image_url;
        if (file_exists($src)) {
            $taxonomy_image_url = $upload_dir['baseurl'].'/backup/'.$taxonomy_image_url;
        }
    }
    return ($taxonomy_image_url != '') ? $taxonomy_image_url : Z_IMAGE_PLACEHOLDER;
}

function z_quick_edit_custom_box($column_name, $screen, $name)
{
    if ($column_name == 'thumb')
    echo '<fieldset>
    <div class="thumb inline-edit-col">
    <label>
    <span class="title"><img src="" alt="Thumbnail"/></span>
    <span class="input-text-wrap"><input type="text" name="taxonomy_image" value="" class="tax_list" /></span>
    <span class="input-text-wrap">
    <button class="z_upload_image_button button">' . __('Upload/Add image', 'zci') . '</button>
    <button class="z_remove_image_button button">' . __('Remove image', 'zci') . '</button>
    </span>
    </label>
    </div>
    </fieldset>';
}

/**
 * Thumbnail column added to category admin.
 *
 * @access public
 * @param mixed $columns
 * @return void
 */
function z_taxonomy_columns($columns)
{
    $new_columns            = array();
    $new_columns['cb']      = $columns['cb'];
    $new_columns['thumb']   = __('Image', 'zci');
    $new_columns['authors'] = __('Authors', 'zci');
    
    unset($columns['cb']);
    
    return array_merge($new_columns, $columns);
}

/**
 * Thumbnail column value added to category admin.
 *
 * @access public
 * @param mixed $columns
 * @param mixed $column
 * @param mixed $id
 * @return void
 */
function z_taxonomy_column($columns, $column, $id)
{
    if ($column == 'thumb')
        $columns = '<span><img src="' . z_taxonomy_image_url($id, NULL, TRUE) . '" alt="' . __('Thumbnail', 'zci') . '" class="wp-post-image" /></span>';
    else if ($column == 'authors') {
        $blogAuthorIds = get_option("blogAuthors_" . $id);
        if ($blogAuthorIds) {
            $authors     = get_users(array(
                'include' => $blogAuthorIds,
                'fields' => array(
                    'user_nicename'
                    )
                ));
            $authorNames = array();
            foreach ($authors as $author) {
                array_push($authorNames, $author->user_nicename);
            }
            // print_r($authorNames);
            $columns = implode(',', $authorNames);
        }
    }
    return $columns;
}

// change 'insert into post' to 'use this image'
function z_change_insert_button_text($safe_text, $text)
{
    return str_replace("Insert into Post", "Use this image", $text);
}

// style the image in category list
if (strpos($_SERVER['SCRIPT_NAME'], 'edit-tags.php') > 0) {
    add_action('admin_head', 'z_add_style');
    add_action('quick_edit_custom_box', 'z_quick_edit_custom_box', 10, 3);
    add_filter("attribute_escape", "z_change_insert_button_text", 10, 2);
}

// Validating options
function z_options_validate($input)
{
    return $input;
}

// creating Ajax call for autosuggesting users in the add to Add To Blog field  
add_action('wp_ajax_autoSuggestUsers', 'autoSuggestUsers');
function autoSuggestUsers()
{
    //get the data from ajax() call  
    $input     = $_POST['input'];
    $users     = get_users('search=*' . $input . '*');
    $userArray = array();
    foreach ($users as $user) {
        if($user->ID!='1'){
            $userArray[$user->ID] = $user->user_login;
        }
    }
    $result['status'] = "success";
    $result['data']   = $userArray;
    echo json_encode($result);
    die();
}

function get_user_avatar($id){
    if(!function_exists('get_wp_user_avatar_src')){
        return null;
    }
    //check if avatar is set
    $avatar_new = get_wp_user_avatar_src($id);
    if( strpos($avatar_new,'default') === false){
        //uploaded image exists
        return $avatar_new;
    }else{
        $avatar = get_user_meta($id, 'avatar',true);
        if(empty($avatar)){
         return $avatar_new;
        }
        $upload_dir = wp_upload_dir();
        $src = $upload_dir['basedir'].'/backup/'.$avatar;
        $srcET = $upload_dir['basedir'].'/auth-et/'.$avatar;
        
        if (file_exists($src)) {
            return $upload_dir['baseurl'].'/backup/'.$avatar;
        }elseif (file_exists($srcET)) {
            return $upload_dir['baseurl'].'/auth-et/'.$avatar;
        }else{    
         // check for et source
         return $avatar_new;
        }
    }
}

add_action('admin_head', 'hide_menu');
function hide_menu() {
    if ( wp_get_current_user()->ID != 1 ) {
		// To remove the whole Appearance admin menu you would use;
		remove_menu_page( 'themes.php' );
		remove_menu_page( 'plugins.php' );
		remove_menu_page( 'options-general.php' );
		remove_menu_page( 'wp-user-avatar' );
		remove_menu_page( 'wp-tweets-pro' );
		remove_menu_page( 'wp_stream' );
		remove_menu_page( 'edit.php?post_type=page' );
		remove_menu_page( 'edit-comments.php' );
		remove_menu_page( 'tools.php' );
		remove_submenu_page( 'index.php', 'update-core.php' );
		remove_submenu_page( 'themes.php', 'theme-editor.php' );
		remove_submenu_page( 'themes.php', 'widgets.php' );
		remove_submenu_page( 'themes.php', 'nav-menus.php' );
		remove_submenu_page( 'themes.php', 'customize.php' );
		remove_submenu_page( 'themes.php', 'custom-background' );
	}
}

// remove links/menus from the admin bar
add_action( 'wp_before_admin_bar_render', 'mytheme_admin_bar_render' );
function mytheme_admin_bar_render() {
    if ( wp_get_current_user()->ID != 1 ) {
		global $wp_admin_bar;
		$wp_admin_bar->remove_menu('themes');
		$wp_admin_bar->remove_menu('customize');
		$wp_admin_bar->remove_menu('widgets');
		$wp_admin_bar->remove_menu('menus');
		$wp_admin_bar->remove_menu('view-site');
		$wp_admin_bar->remove_node( 'wp-logo');
		$wp_admin_bar->remove_node( 'new-page');
		$wp_admin_bar->remove_node( 'comments' );
	}
}

add_action( 'admin_menu', 'remove_meta_boxes' );
function remove_meta_boxes() {
	if ( wp_get_current_user()->ID != 1 ) {
		remove_meta_box('tagsdiv-blog', 'post', 'normal');
		remove_meta_box('formatdiv', 'post', 'normal');
		remove_meta_box('commentsdiv','post','normal');
		remove_meta_box('postcustom','post','normal');
		//remove_meta_box('slugdiv','post','normal');
		remove_meta_box('trackbacksdiv','post','normal');
	}
}

add_action('admin_menu', 'remove_the_dashboard');
function remove_the_dashboard () {
    if (current_user_can('level_10')) {
        return;
    }
    else {
        global $menu, $submenu, $user_ID;
        $the_user = new WP_User($user_ID);
        reset($menu); $page = key($menu);
        while ((__('Dashboard') != $menu[$page][0]) && next($menu))
            $page = key($menu);
            if (__('Dashboard') == $menu[$page][0]) unset($menu[$page]);
            reset($menu); $page = key($menu);
            while (!$the_user->has_cap($menu[$page][1]) && next($menu))
                $page = key($menu);
            if (preg_match('#wp-admin/?(index.php)?$#',$_SERVER['REQUEST_URI']) && ('index.php' != $menu[$page][2]))
                wp_redirect(get_option('siteurl') . '/wp-admin/post-new.php');
   }
}

add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );
function custom_excerpt_length( $length ) {
    return 35;
}

add_filter('excerpt_more', 'new_excerpt_more');
function new_excerpt_more( $more ) {
    return '...';
}

function getTrendingByCat($cat){
        $args = array(
            'date_query' => array(
                            array(
                                'column' => 'post_date',
                                'after' => '2 months ago',
                                ),
                            ),
                        'posts_per_page' => 10,
                        'orderby' => 'comment_count',
                        'category_name' =>$cat
                        );
             $popular = new WP_Query($args);
                $i=0;             
                while ($popular->have_posts()) : $popular->the_post();$i++; ?>   
                <li>
                    <span class="pull-left number"><?php echo $i; ?>.</span>
                    <?php the_title( '<h4><a href="' . esc_url( get_permalink() ) . '" rel="bookmark" title="Go to '.get_the_title().'">', '</a></h4>' );?>  
                    <!--<span class="post-date"><?php the_time('F j, Y') ?></span>-->
                </li> 
            <?php endwhile; 
}

function getPopularByCat($cat){
        $args = array(
                        'posts_per_page' => 5,
                        'orderby' => 'comment_count',
                        'category_name' =>$cat
                        );
             $popular = new WP_Query($args);
        while ($popular->have_posts()) : $popular->the_post(); ?>   
        <div class="media">
        <img class="pull-left media-object img-circle" src="<?php echo get_user_avatar(get_the_author_meta('ID')); ?>" >
        <div class="media-body">
          <?php the_title( '<h4 class="media-heading"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark" title="Go to '.get_the_title().'">', '</a></h4>' );?>  
          <span class="post-date"><?php the_time('F j, Y') ?></span>
        </div>
        </div>
    <?php endwhile; 
}


function getTrending(){
    $args = array(
            'date_query' => array(
                            array(
                                'column' => 'post_date',
                                'after' => '2 weeks ago',
                                ),
                            ),
                        'posts_per_page' => 10,
                        'orderby' => 'comment_count'
                        );
            $popular = new WP_Query($args);
            $i=0;
            echo '<div class="panel"><h3 class="panel-title">Most Discussed</h3><ul class="panel-body list-unstyled trending" data-vr-zone="Most Discussed">';
            while ($popular->have_posts()) : $popular->the_post();$i++; ?>   
                <li data-vr-contentbox="">
                    <span class="pull-left number"><?php echo $i; ?>.</span>
                    <?php the_title( '<h4><a pg="Trending_Pos" href="' . esc_url( get_permalink() ) . '" rel="bookmark" title="Go to '.get_the_title().'">', '</a></h4>' );?>  
                    <!--<span class="post-date"><?php the_time('F j, Y') ?></span>-->
                </li> 
            <?php endwhile; 
            echo '</ul></div>';
            ?> 
    <?php
}

function getSideWidget(){
    echo '<div class="panel"><h3 class="panel-title">Most Discussed</h3><ul class="panel-body list-unstyled trending" data-vr-zone="Most Discussed">';            
    $args = array(
            'date_query' => array(
                            array(
                                'column' => 'post_date',
                                'after' => '2 weeks ago',
                                ),
                            ),
                        'posts_per_page' => 10,
                        'orderby' => 'comment_count'
                        );
            $popular = new WP_Query($args);
            $i=0;
            while ($popular->have_posts()) : $popular->the_post();$i++; ?>   
                <li data-vr-contentbox="">
                    <span class="pull-left number"><?php echo $i; ?>.</span>
                    <?php the_title( '<h4><a pg="Trending_Pos" href="' . esc_url( get_permalink() ) . '" rel="bookmark" title="Go to '.get_the_title().'">', '</a></h4>' );?>  
                    <!--<span class="post-date"><?php the_time('F j, Y') ?></span>-->
                </li> 
            <?php endwhile; 
    echo '</ul></div>';      
}

function getPopular($popular){
    $args = array(
            'date_query' => array(
                            array(
                                'column' => 'post_date',
                                'after' => '6 months ago',
                                ),
                            ),
                        'posts_per_page' => 5,
                        'orderby' => 'comment_count'
                        );
    $popular = new WP_Query($args); 
    while ($popular->have_posts()) : $popular->the_post(); ?>   
        <div class="media">
        <img class="pull-left media-object img-circle" src="<?php echo get_user_avatar(get_the_author_meta('ID')); ?>" >
        <div class="media-body">
          <?php the_title( '<h4 class="media-heading"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark" title="Go to '.get_the_title().'">', '</a></h4>' );?>  
          <span class="post-date"><?php the_time('F j, Y') ?></span>
        </div>
        </div>
    <?php endwhile; ?> 
<?php
}


function getMostDiscussedToday(){
    $args = array(
            'date_query' => array(
                            array(
                                'column' => 'post_date',
                                'after' => '1 day ago',
                                ),
                            ),
                        'posts_per_page' => 5,
                        'orderby' => 'comment_count'
                        );
            $popular = new WP_Query($args);
            $i=0;
            while ($popular->have_posts()) : $popular->the_post();$i++; ?>   
                <li data-vr-contentbox="">
                    <span class="pull-left number"><?php echo $i; ?>.</span>
                    <?php the_title( '<h4><a pg="Trending_Pos" href="' . esc_url( get_permalink() ) . '" rel="bookmark" title="Go to '.get_the_title().'">', '</a></h4>' );?>  
                    <!--<span class="post-date"><?php the_time('F j, Y') ?></span>-->
                </li> 
            <?php endwhile; 
            ?> 
    <?php
}

function getMostDiscussedWeek(){
   $args = array(
            'date_query' => array(
                            array(
                                'column' => 'post_date',
                                'after' => '1 week ago',
                                ),
                            ),
                        'posts_per_page' => 5,
                        'orderby' => 'comment_count'
                        );
            $popular = new WP_Query($args);
            $i=0;
            while ($popular->have_posts()) : $popular->the_post();$i++; ?>   
                <li data-vr-contentbox="">
                    <?php the_title( '<a pg="Trending_Pos" href="' . esc_url( get_permalink() ) . '" rel="bookmark" title="Go to '.get_the_title().'">', '</a>' );?>  
                    <!--<span class="post-date"><?php the_time('F j, Y') ?></span>-->
                </li> 
            <?php endwhile; 
            wp_reset_query();
}

function getMostDiscussedMonth(){
    $args = array(
            'date_query' => array(
                            array(
                                'column' => 'post_date',
                                'after' => '1 month ago',
                                ),
                            ),
                        'posts_per_page' => 5,
                        'orderby' => 'comment_count'
                        );
            $popular = new WP_Query($args);
            $i=0;
            while ($popular->have_posts()) : $popular->the_post();$i++; ?>   
                <li data-vr-contentbox="">
                    <span class="pull-left number"><?php echo $i; ?>.</span>
                    <?php the_title( '<h4><a pg="Trending_Pos" href="' . esc_url( get_permalink() ) . '" rel="bookmark" title="Go to '.get_the_title().'">', '</a></h4>' );?>  
                    <!--<span class="post-date"><?php the_time('F j, Y') ?></span>-->
                </li> 
            <?php endwhile;
            wp_reset_query();
}

function getRecentlyBlogged(){
    $args = array(
            'date_query' => array(
                            array(
                                'column' => 'post_date',
                                'after' => '1 month ago',
                                ),
                            ),
                        'posts_per_page' => 5,
                        'orderby' => 'comment_count'
                        );
            $popular = new WP_Query($args);
            $i=0;
            while ($popular->have_posts()) : $popular->the_post();$i++; ?>   
                <li data-vr-contentbox="">
                    <span class="pull-left number"><?php echo $i; ?>.</span>
                    <?php the_title( '<h4><a pg="Trending_Pos" href="' . esc_url( get_permalink() ) . '" rel="bookmark" title="Go to '.get_the_title().'">', '</a></h4>' );?>  
                    <!--<span class="post-date"><?php the_time('F j, Y') ?></span>-->
                </li> 
            <?php endwhile; 
            ?> 
    <?php
}

function getRecentlyJoinedAuthors(){
	$recent_users = new WP_User_Query( 
	  array( 
		'meta_query'   => array(
			'relation' => 'OR',
			array(
			  'compare' => '!=',
			  'key' => 'hide_on_front',
			  'value' => 'yes',
			),
			array(
			  'compare' => 'NOT EXISTS',
			  'key' => 'hide_on_front'
			)
		  ),
		'query_id' => 'authors_with_posts',
		'orderby' => 'user_registered',
		'order' => 'DESC',
		'offset' => 0 ,
		'number' => 10,
		'fields' => 'ids',
		'exclude' => array('1')
	  ));
	$users = $recent_users->get_results();
	return $users;
}

add_action( 'admin_head', 'showhiddencustomfields' );

function showhiddencustomfields() {
    echo "<style type='text/css'>#misc-publishing-actions #visibility {
        display: none;
    }
#wp-admin-bar-view-site{display:none}
    </style>";
}

add_action( 'user_register', 'addMetaFields', 10, 1 );

function addMetaFields( $user_id ) {
    update_user_meta($user_id, 'authoringBlogIds', array());
}


/** 
* Commenting Systems code
**/
add_action('wp_ajax_postComment', 'postComment');
add_action('wp_ajax_nopriv_postComment', 'postComment');

function postComment(){
    //close connection
    $result = array();
    $result['status'] = "success";
    $result['data']   = "dummy";
    $status = json_encode($result);
    die();
}

add_action('wp_ajax_validatecomment', 'validatecomment');
add_action('wp_ajax_nopriv_validatecomment', 'validatecomment');
function html2txt($document){
    $search = array('@<script[^>]*?>.*?</script>@si',  // Strip out javascript
                   '@<[\/\!]*?[^<>]*?>@si',            // Strip out HTML tags
                   '@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly
                   '@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments including CDATA
    );
    $text = preg_replace($search, '', $document);
    return $text;
} 
function vinput($ip, $dt) 
{   
    $newip = $ip;
    switch ($dt) {
        case 'i':
            $newip = (int)$ip;
            break;
        case 'b':
            $newip = (boolean)$ip;
            break;
        case 's':
            //$ip = strip_tags($ip, '<br/>');
            //$ip = html2txt($ip);
            $ip = htmlentities($ip, ENT_QUOTES);
            $newip = (string)$ip;
            break;
    }
    return $newip;
}

function validatecomment(){

    $appKeyForMyTimes = getAppKeyForMyTimes($_POST['msid']);        
    $postVars = $_POST;
    $postVars['fromname'] = vinput($_POST['fromname'], 's');
    $postVars['fromaddress'] = vinput($_POST['fromaddress'], 's');
    $postVars['userid'] = vinput($_POST['userid'], 's');
    $postVars['location'] = vinput($_POST['location'], 's');
    $postVars['imageurl'] = vinput($_POST['imageurl'], 's');
    $postVars['loggedstatus'] = vinput($_POST['loggedstatus'], 'i');
    $postVars['message'] = urlencode($_POST['message']);
    $postVars['roaltdetails'] = vinput($_POST['roaltdetails'], 's');
    $postVars['ArticleID'] = vinput($_POST['ArticleID'], 'i');
    $postVars['msid'] = vinput($_POST['msid'], 'i');
    $postVars['parentid'] = vinput($_POST['parentid'], 'i');
    $postVars['rootid'] = vinput($_POST['rootid'], 'i');
    
    //echo $fields_string;
    $logFileName = WP_CONTENT_DIR . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'postcommentlogs' . DIRECTORY_SEPARATOR . $postVars['ArticleID'] . '.txt';
    $logFileData = 'Request Time : ' . date( 'Y-m-d H:i:s', current_time( 'timestamp', 0 ) ) . PHP_EOL;
    $logFileData = $logFileData . 'params : ' . $fields_string . PHP_EOL;
    //open connection
    if($postVars['rootid'] >0 && $postVars['parentid']>0)
    {
        $objectType  = "A";
        $activityType = "Replied";

    }else{
        $objectType  = "B";
        $activityType = "Commented";
    }

    $queryStringData = 'objectType='.$objectType.'&activityType='.$activityType.'&appKey='.$appKeyForMyTimes.'&uniqueAppID='.vinput($_POST['ArticleID'], 'i').'&exCommentTxt='.($postVars['message']).'&baseEntityType=ARTICLE&ssoId='.vinput($_POST['userid'], 's').'&userName='.urlencode(vinput($_POST['fromname'], 's'));
    
    if($postVars['rootid'] >0 && $postVars['parentid']>0)
    {
        $queryStringData.= '&objectId='.$postVars['parentid'];
    }
    $queryStringData.= '&uuId='.vinput($_POST['userid'], 's');   
    $postXML = 'eroaltdetails=<roaltdetails><fromaddress>'.vinput($_POST['userid'], 's').'</fromaddress><location>'.vinput($_POST['location'], 's').'</location><fromname>'.urlencode(vinput($_POST['fromname'], 's')).'</fromname><loggedstatus>1</loggedstatus><ssoid>'.vinput($_POST['userid'], 's').'</ssoid><url>'.vinput($_POST['url'], 's').'</url></roaltdetails>&isCommentMigrate=true&isCommentMailMigrate=true&uuId='.vinput($_POST['userid'], 's');

    $url = MYTIMES_URL.'mytimes/commentIngestion';
    $curl_headers = array('Content-Type: application/x-www-form-urlencoded; charset=UTF-8', 'Content-Length: ' . (strlen($str)), 'Accept-Encoding: gzip; deflate', 'Accept-Language: en-US');
    
    $url  = $url."/?".$queryStringData;

    //error_log("Mytimes comment post Data::: ".$postXML);
    //echo $postXML;

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url); 
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,$postXML);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,300);
    //curl_setopt($ch, CURLOPT_HTTPHEADER, $curl_headers);
    $ch_result = curl_exec($ch);
    curl_close($ch);
    $logFileData = $logFileData  .'response : ' . $result . PHP_EOL . PHP_EOL;
    //$status2 = substr(json_encode((json_decode($result)), 0, -1));
    $status = json_decode($ch_result);
    //echo $status;
    if( $status == null || !isset($status->Error)){
        global $wpdb;
        $wpdb->query('UPDATE wp_posts SET comment_count= comment_count+1 WHERE ID = '.$postVars['ArticleID'].';');
        echo json_encode($status);
    }elseif(isset($status->Error) && isset($status->Error) && ($status->Error=='Message Duplicate')){
        header('Content-type: text/html');
        echo 'duplicatecontent';
    }elseif( isset($status->Error) && isset($status->Error) && ($status->Error=='Comment contains abusive text')){
        header('Content-type: text/html');
        echo 'abusivecontent';
    }else{
		$fp = fopen($logFileName, 'a');
		fwrite($fp, $logFileData);
		fclose($fp);
		echo json_encode($status);
	}
    die();
}



add_action('wp_ajax_ratecomment', 'ratecomment');
add_action('wp_ajax_nopriv_ratecomment', 'ratecomment');

function ratecomment(){
    //$url =  COMMENTS_URL.'json/ratecomment.cms';
    $params = array();
    $param['callback'] = 'test';
    $param['actorType'] = 'U';
    if($_GET['typeid']=='101')
    {
        $param['activityType'] = 'Disagreed';
    }
    else
    {
        $param['activityType'] = 'Agreed';
    }
    $param['objectId'] = $_GET['opinionid'];
    $param['objectType'] = 'A';
    $param['fromMyTimes'] = true;
    if(isset($_COOKIE['ssoid']))
    {
        $param['uuId']        = $_COOKIE['ssoid'];    
    }
    
    $url = "http://mytpvt.indiatimes.com/mytimes/addActivity";

    foreach( $param as $key => $value )
    {
        $fields_string .= $key.'='.$value.'&';
    }
    $fields_string = rtrim( $fields_string, '&' );
    $url = $url."?".$fields_string; 
    
    //open connection
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($ch);
    curl_close($ch);
    header('Content-type: text/html');
    echo $result;
    die();
}

add_action('wp_ajax_offensive', 'offensive');
add_action('wp_ajax_nopriv_offensive', 'offensive');

function offensive(){
    
    $params = array();
    $param['callback'] = 'test';
    $param['actorType'] = 'U';
    $param['activityType'] = 'Offensive';
    $param['objectId'] = $_GET['ofcommenteroid'];
    $param['offenText']= $_GET['ofreason'];
    $param['objectType'] = 'A';
    $param['isCommentMigrate'] = true;
    $param['fromMyTimes'] = true;
    if(isset($_COOKIE['ssoid']))
    {
        $param['uuId']        = $_COOKIE['ssoid'];    
    }
    
    
    $url = "http://mytpvt.indiatimes.com/mytimes/addActivity";
    foreach( $param as $key => $value )
    {
        $fields_string .= $key.'='.$value.'&';
    }
    $fields_string = rtrim( $fields_string, '&' );
    echo $url = $url."?".$fields_string;    

    //open connection
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($ch);
    curl_close($ch);
    header('Content-type: text/html');
    echo $result;
    die();
}


function getBadges($badges,$uid){
    if(is_array ($badges) ){
        foreach($badges as $badge){
            if ($badge->userId == $uid){
                return $badge;
            }
        }
    }elseif (is_object($badges)) {
        if($badges->userId && $badges->userId==$uid){
            return $badges;
        }
    }
    return null;
}

function getRewards($rewards,$uid){
    if(is_array ($rewards) ){
        foreach($rewards as $reward){
            if ($reward->userid == $uid){
                return $reward;
            }
        }
    }elseif (is_object($rewards)) {
        if($rewards->userid && $rewards->userid==$uid){
            return $rewards;
        }
    }
    return null;
}

add_action('wp_ajax_commentsdatamobile', 'commentsdatamobile');
add_action('wp_ajax_nopriv_commentsdatamobile', 'commentsdatamobile');
function commentsdatamobile(){

    $postId = $_GET['msid'];
    $appKeyForMyTimes = getAppKeyForMyTimes($postId);
    $url = MYTIMES_URL.'mytimes/getFeed/Activity/?msid='.$postId.'&curpg=1&commenttype=agree&appkey='.$appKeyForMyTimes.'&sortcriteria=AgreeCount&order=desc&size=1&pagenum=1';
    $curl_headers = array('Content-Type: application/x-www-form-urlencoded; charset=UTF-8', 'Accept-Encoding: gzip; deflate', 'Accept-Language: en-US');
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url); 
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,30);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $curl_headers);
    $ch_result = curl_exec($ch);
    curl_close($ch);
    echo $ch_result;
    die();
}

add_action('wp_ajax_commentsdata', 'commentsdata');
add_action('wp_ajax_nopriv_commentsdata', 'commentsdata');

function commentsdataOld(){
    $postId = $_GET['msid'];
    $curpg = $_GET['curpg'];
    $url =  COMMENTS_URL.'showcomments.cms?extid='.$postId.'&perpage=15&feedtype=json&curpg='.$curpg;
    if(isset($_GET['ordertype']) && !empty($_GET['ordertype'])){
        $url .= '&ordertype='.$_GET['ordertype'];
    }
    if(isset($_GET['commenttype']) && !empty($_GET['commenttype'])){
        $url .= '&commenttype='.$_GET['commenttype'];
    }
    if(isset($_GET['commenttype']) && !empty($_GET['commenttype'])){
        $url .= '&commenttype='.$_GET['commenttype'];
    }
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Set curl to return the data instead of printing it to the browser.
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,20); # timeout after 10 seconds, you can increase it
    curl_setopt($ch, CURLOPT_URL, $url ); #set the url and get string together    
    $result = curl_exec($ch);
    $comments = json_decode($result);
    //echo "<pre>";print_r($comments);
    //die;
    if(empty($comments->rothrd)){
        curl_close($ch);
    $response = array();
    $response['new_cmtofart2_nit_v1'] = new stdClass();
    $response['new_cmtofart2_nit_v1']->mytuserdata = new stdClass();
    $response['new_cmtofart2_nit_v1']->ActionParams =  $comments->ActionParams;
    $response['new_cmtofart2_nit_v1']->rothrd =  $comments->rothrd;
    $response['new_cmtofart2_nit_v1']->tdate =  new stdClass();
    $response['new_cmtofart2_nit_v1']->tdate->potime =  0;
    //date_default_timezone_set('UTC');
    $response['new_cmtofart2_nit_v1']->tdate->date = date('D M d Y H:i:s', mktime(date("H")+5, date("i")+30, date("s"), date("m")  , date("d"), date("Y")));    
    //print_r($response);
    echo json_encode($response,JSON_HEX_TAG);
        die();
    }
    if($comments->rothrd->opctr){
        $userIds = '';
        if($comments->rothrd->opctr=='1'){
            $singleComment = $comments->rothrd->op;
            $comments->rothrd->op = array();
            array_push($comments->rothrd->op,$singleComment);
        }
        foreach ($comments->rothrd->op as $value){
            $value->optext = html_entity_decode(stripslashes($value->optext), ENT_QUOTES);
            //echo '<pre>';var_dump($value);
            $userIds .= urlencode($value->roaltdetails->fromaddress).',';
            //$value->level = $value->leveldepth;
        }
        $userIds = rtrim($userIds,',');
        $userUrl = 'http://mytpvt.indiatimes.com/mytimes/getUsersInfo?ssoids='.$userIds;
        curl_setopt($ch, CURLOPT_URL, $userUrl); #set the url and get string together    
        $users = json_decode(curl_exec($ch));
        //print_r($users);
        $badgesUrl = 'http://rewards.indiatimes.com/bp/api/urs/mubhtry?format=json&pcode=TOI&uid='.$userIds;       
        curl_setopt($ch, CURLOPT_URL, $badgesUrl); #set the url and get string together    
        $badges = json_decode(curl_exec($ch));
        $badges = $badges->output;
        //print_r($badges);
        $rewardsUrl = 'http://rewards.indiatimes.com/bp/api/urs/ups?format=json&pcode=TOI&uid='.$userIds;
        curl_setopt($ch, CURLOPT_URL, $rewardsUrl); #set the url and get string together    
        $rewards = json_decode(curl_exec($ch));
        //print_r($rewards);
        $rewards = $rewards->output->user;
        //print_r($rewards);
        foreach ($users as $user){
            $userReward = getRewards($rewards,$user->sso);
            $user->reward = new stdClass();
            $user->reward->user = $userReward;
            //$userBadges = new stdClass();
            $userBadges = getBadges($badges->userbadgehistory[0]->userbadges,$user->sso);            
            $user->rewardpoint = new stdClass(); 
            //$user->rewardpoint
            $user->rewardpoint->userbadges = $userBadges;
        }
    }
    curl_close($ch);   
    $response = array();
    $response['new_cmtofart2_nit_v1'] = new stdClass();
    $response['new_cmtofart2_nit_v1']->mytuserdata = new stdClass();
    $response['new_cmtofart2_nit_v1']->mytuserdata->array = $users;
    $response['new_cmtofart2_nit_v1']->ActionParams =  $comments->ActionParams;
    $response['new_cmtofart2_nit_v1']->rothrd =  $comments->rothrd;
    $response['new_cmtofart2_nit_v1']->tdate =  new stdClass();
    $response['new_cmtofart2_nit_v1']->tdate->potime =  0;
    //date_default_timezone_set('UTC');
    $response['new_cmtofart2_nit_v1']->tdate->date = date('D M d Y H:i:s', mktime(date("H")+5, date("i")+30, date("s"), date("m")  , date("d"), date("Y")));    
    //print_r($response);
    echo json_encode($response,JSON_HEX_TAG);
    die();
}

function extraDataFromAPI($value,$isreply=false){

    $indi = new stdClass();
    //$commetDetails[''] = '';
    if($isreply==true){
        $indi->level = "2";
        $indi->rootid = $value->O_ID;
        $indi->leveldepth =  "2";
    }else{
        $indi->istoplevel = "1";
        $indi->level = "1";
        $indi->leveldepth =  "1";
        $indi->rootid = $value->_id;
    }
    
    $indi->displaylocation = "1";
    $indi->mailidpresent =  "1";
    $indi->optext =  $value->C_T;

    $indi->roaltdetails =  array( 'configid' =>$value->_id,
                                'imageurl'=> $value->user_detail->profile,
                                'rotype'=> "0",
                                'url'=>$value->teu,
                                'fromaddress'=> $value->user_detail->_id,
                                'urs'=>"",
                                'loggedstatus'=> "1",
                                'extid'=> "10",
                                'rchid'=>'',//old comment channel id
                                'location'=> $value->user_detail->CITY,
                                'fromname'=> $value->user_detail->FL_N,
                                'ssoid'=> $value->user_detail->uid,
                                'useripaddress'=> "192.168.99.10"
                                );
    
    $indi->msid =  $value->msid;
    //$indi->rodate =  "05 Jul, 2017 11:35 AM";
    $indi->rodate =  date('d M, Y h:i A', (($value->C_D_Minute*60)+(330*60)));
    $indi->messageid =  $value->_id;
    $indi->parentid =  $value->O_ID;
    $indi->agree = $value->AC_A_C;
    $indi->disagree = $value->AC_D_C;
    $indi->recommended = "0";
    $indi->offensive = $value->AC_O_C;;
    $indi->uid = $value->user_detail->uid;
    
    if($isreply==true){
        $indi->roaltdetails['rootid'] = $value->O_ID;
        $indi->parentusername =  $value->PA_U_D_N_U;
        $indi->parentuid =  $value->PA_A_I;
    }
    

    return $indi;
}

function formatChildForReplyShow(&$childIndi,$value)
{
    if(isset($value->CHILD) && count($value->CHILD)>0)
    {
        foreach ($value->CHILD as $key => $childValue) 
        {
           if(isset($childValue->CHILD) && count($childValue->CHILD)>0)
           {
               $childIndi[] = extraDataFromAPI($childValue,true);
               formatChildForReplyShow($childIndi,$childValue);
           }else{
                $childIndi[] = extraDataFromAPI($childValue,true);
           }
        }
    }
    //var_dump($childIndi);
    //return $childIndi;
}

function commentsdata(){
    $postId = $_GET['msid'];
    $curpg = $_GET['curpg'];
    $appKeyForMyTimes = getAppKeyForMyTimes($postId);
   // $url =  COMMENTS_URL.'showcomments.cms?extid='.$postId.'&perpage=15&feedtype=json&curpg='.$curpg;
    $ordertype = 'desc';
    if(isset($_GET['ordertype']) && !empty($_GET['ordertype'])){
        $ordertype=$_GET['ordertype'];
    }

    $commenttype  = 'CreationDate';
    if(isset($_GET['commenttype']) && !empty($_GET['commenttype'])){
        $commenttype =$_GET['commenttype'];
    }
    

    switch($commenttype){
        
        case "disagree":
        $commenttype= 'disagree';
        $sortcriteria= 'DisagreeCount';
        $order = 'desc';
        break;

        case 'discussed':
        $commenttype = 'mostdiscussed'; 
        $sortcriteria = 'discussed';
        $order = 'desc';
        break;

        case 'agree':
        $commenttype='agree';
        $sortcriteria='AgreeCount';
        $order='desc';
        break;

        default:
        $sortcriteria = 'CreationDate';
        $order       = ($ordertype=="asc")?"desc":"asc";
        break;
    }

    $queryStringData = 'msid='.$postId.'&curpg='.$curpg.'&appkey='.$appKeyForMyTimes.'&commenttype='.$commenttype.'&sortcriteria='.$sortcriteria.'&order='.$order.'&size=15&pagenum='.$curpg.'&withReward=true';
    //MYTIMES_URL/mytimes/getFeed/Activity?msid=".$postId."&curpg=1&appkey=FEMINA&sortcriteria=CreationDate&order=asc&size=10&pagenum=1&withReward=true
    $url = MYTIMES_URL.'mytimes/getFeed/Activity';
    $curl_headers = array('Content-Type: application/x-www-form-urlencoded; charset=UTF-8', 'Accept-Encoding: gzip; deflate', 'Accept-Language: en-US');
    
    $url  = $url."/?".$queryStringData;
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url); 
    //curl_setopt($ch, CURLOPT_POST, 1);
    //curl_setopt($ch, CURLOPT_POSTFIELDS,array());
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,300);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $curl_headers);
    $ch_result = curl_exec($ch);
    curl_close($ch);
    $rawComments = json_decode($ch_result);
    //echo "<pre>";
    //var_dump($rawComments);
    $comments = new stdClass();
    $comments->totalcount = $rawComments[0]->totalcount;
    $comments->ActionParams = array();
    $comments->rothrd= new stdClass();
    $comments->rothrd->op = array();
    $comments->rothrd->potime = "96";
    $comments->rothrd->opctr = $rawComments[0]->totalcount;
    $comments->rothrd->opctrtopcnt = $rawComments[0]->cmt_c;
    $comments->rothrd->recommendcount = $rawComments[0]->cmt_c;
    $comments->rothrd->agreecount = $rawComments[0]->cmt_c;
    $comments->rothrd->disagreecount = $rawComments[0]->cmt_c;
    $comments->rothrd->loggedopctr = "0";
    unset($rawComments[0]);
    foreach ($rawComments as $key => $value) 
    {
        
        $indi = extraDataFromAPI($value);
        array_push($comments->rothrd->op,$indi);

        $childIndi = array();
        formatChildForReplyShow($childIndi,$value);
        

        foreach ($childIndi as $key => $childValue) 
        {
            array_push($comments->rothrd->op,$childValue);
        }
        //echo "<pre>";
        //print_r($comments->rothrd->op);

    }
    //echo "<pre>";
    //print_r($comments);
    //echo json_encode($comments);

    if(empty($comments->totalcount)){
        if(is_object($ch)){
            curl_close($ch);
        }
    $response = array();
    $response['new_cmtofart2_nit_v1'] = new stdClass();
    $response['new_cmtofart2_nit_v1']->mytuserdata = new stdClass();
    $response['new_cmtofart2_nit_v1']->ActionParams =  $comments->ActionParams;
    $response['new_cmtofart2_nit_v1']->rothrd =  $comments->rothrd;
    $response['new_cmtofart2_nit_v1']->tdate =  new stdClass();
    $response['new_cmtofart2_nit_v1']->tdate->potime =  0;
    //date_default_timezone_set('UTC');
    $response['new_cmtofart2_nit_v1']->tdate->date = date('D M d Y H:i:s', mktime(date("H")+5, date("i")+30, date("s"), date("m")  , date("d"), date("Y")));    
    //print_r($response);
    echo json_encode($response,JSON_HEX_TAG);
        die();
    }
    //var_dump($comments);
    if($comments->totalcount >0){
        //unset($rawComments[0]);
        //echo "<pre>";
        //print_r($comments);

        $userIds = '';
        /*if($comments->rothrd->opctr=='1'){
            $singleComment = $comments->rothrd->op;
            $comments->rothrd->op = array();
            array_push($comments->rothrd->op,$singleComment);
        }*/
        foreach ($comments->rothrd->op as $value){
            $value->optext = html_entity_decode(stripslashes(strip_tags($value->optext)), ENT_QUOTES);
            $userIds .= urlencode($value->roaltdetails['ssoid']).',';
        }
        $userIds = rtrim($userIds,',');
        //$userIds = urlencode("13pmqrqgfa4mvep66cjwcbh5i");
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,300);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $curl_headers);
        $userUrl = 'http://mytpvt.indiatimes.com/mytimes/getUsersInfo?ssoids='.$userIds;
        curl_setopt($ch, CURLOPT_URL, $userUrl); #set the url and get string together    
        $users = json_decode(curl_exec($ch));
        //var_dump($users);
        //die;
        $badgesUrl = 'http://rewards.indiatimes.com/bp/api/urs/mubhtry?format=json&pcode=TOI&uid='.$userIds;       
        curl_setopt($ch, CURLOPT_URL, $badgesUrl); #set the url and get string together    
        $badges = json_decode(curl_exec($ch));
        $badges = $badges->output;
        //print_r($badges);
        $rewardsUrl = 'http://rewards.indiatimes.com/bp/api/urs/ups?format=json&pcode=TOI&uid='.$userIds;
        curl_setopt($ch, CURLOPT_URL, $rewardsUrl); #set the url and get string together    
        $rewards = json_decode(curl_exec($ch));
        //print_r($rewards);
        $rewards = $rewards->output->user;
        foreach ($users as $user){
            $userReward = getRewards($rewards,$user->_id);
            $user->reward = new stdClass();
            $user->reward->user = $userReward;
            //$userBadges = new stdClass();
            $userBadges = getBadges($badges->userbadgehistory[0]->userbadges,$user->_id);            
            $user->rewardpoint = new stdClass(); 
            //$user->rewardpoint
            $user->rewardpoint->userbadges = $userBadges;
            $user->sso = $user->_id;
        }
        //print_r($users);
    }
    curl_close($ch);   
    $response = array();
    $response['new_cmtofart2_nit_v1'] = new stdClass();
    $response['new_cmtofart2_nit_v1']->mytuserdata = new stdClass();
    $response['new_cmtofart2_nit_v1']->mytuserdata->array = $users;
    $response['new_cmtofart2_nit_v1']->ActionParams =  $comments->ActionParams;
    $response['new_cmtofart2_nit_v1']->rothrd =  $comments->rothrd;
    $response['new_cmtofart2_nit_v1']->tdate =  new stdClass();
    $response['new_cmtofart2_nit_v1']->tdate->potime =  0;
    //date_default_timezone_set('UTC');
    $response['new_cmtofart2_nit_v1']->tdate->date = date('D M d Y H:i:s', mktime(date("H")+5, date("i")+30, date("s"), date("m")  , date("d"), date("Y")));    
    //print_r($response);
    echo json_encode($response,JSON_HEX_TAG);
    die();
}


//add_filter('screen_options_show_screen', '__return_false');    
add_action( 'admin_head-post-new.php','hide_ping_track');
add_action( 'admin_head-post.php', 'hide_ping_track' );
function hide_ping_track() {
    if( get_post_type() === "post" ){
            // only for non-admins
            echo "<style>.meta-options label[for=ping_status], #ping_status{display:none !important;} </style>";
    }
}

add_filter( 'user_has_cap', 'user_has_cap', 10, 3 );
function user_has_cap( $all_caps, $cap, $args ){
if ( isset( $all_caps['contributor'] ) && $all_caps['contributor'] )
        {
            $all_caps['upload_files'] = TRUE;
            $all_caps['edit_published_posts'] = TRUE;
        }
    return $all_caps;
}

//Adding the Open Graph in the Language Attributes
function add_opengraph_doctype( $output ) {
    return $output . ' xmlns:og="http://opengraphprotocol.org/schema/" xmlns:fb="http://www.facebook.com/2008/fbml"';
}
add_filter('language_attributes', 'add_opengraph_doctype');

function catch_first_image($content) {
	$first_img = '';
	ob_start();
	ob_end_clean();
	$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $content, $matches);
	$first_img = $matches[1][0];

	if(empty($first_img)) {
		$first_img = '';
	} else if(substr_count($first_img, '/backup/') >= 1) {
		$first_img = '';
	} else {
		$first_img = preg_replace( '/-\d+x\d+(?=\.(jpg|jpeg|png|gif)$)/i', '', $first_img );
		global $wpdb;
		$attachment = $wpdb->get_col($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE guid='%s';", $first_img ));
		$first_img_arr = wp_get_attachment_image_src($attachment[0], 'full');
		if(isset($first_img_arr[0]) && !empty($first_img_arr[0]) && $first_img_arr[1] > 200 && $first_img_arr[2] > 200) {
			$first_img = $first_img_arr[0];
			$first_img = str_replace( ADMIN_URL.'/','../../',$first_img);
			$first_img = str_replace('../..', WP_HOME, $first_img);
		} else {
			$first_img = '';
		}
	}
	return $first_img;
}

//Lets add Open Graph Meta Info
function insert_fb_in_head() {
    global $post, $paged;
    if(isTOI()==true){
        echo '<meta name="google-site-verification" content="ZgFICIedNvVZl5pV9EfAUeenwta9vBY0Za_GgmV4zuw" />';
        $sitename = 'Times of India Blog';
        $placeholderImage = FB_PLACEHOLDER;
    } else {
		$sitename = 'Economic Times Blog';
		$placeholderImage = ET_FB_PLACEHOLDER;
	}
    if ( get_previous_posts_link() ) { ?>
        <link rel="prev" href="<?php echo get_pagenum_link( $paged - 1 ); ?>" /><?php
    }

    if ( get_next_posts_link() ) { ?>
        <link rel="next" href="<?php echo get_pagenum_link( $paged +1 ); ?>" /><?php
    }
	
	echo '<meta property="og:site_name" content="'.$sitename.'" />';
	$meta_part = '';
	$og_keywords = '';
	$current_url = current_page_url();
    if ( is_singular()){
		setup_postdata( $post );
		$authorName = get_the_author_meta('display_name');
		$canUrl = get_post_meta(get_the_ID(),'canurl', true);
		if(empty($canUrl)){
			$canUrl = get_permalink();
		}
		$og_title = get_the_title();
		$og_desc = get_the_excerpt();
		$categories = get_the_category();
		$og_keywords_arr = array();
		if($categories){
			foreach($categories as $category) {
				$og_keywords_arr[] = $category->cat_name . ' blog';
			}
		}
		$og_keywords_arr[] = $authorName . ' blog';
		$blog = array_values(get_the_terms(get_the_ID(), 'blog' ))[0];
		$og_keywords_arr[] = $blog->name . ' blog';
		$og_keywords = implode(', ', $og_keywords_arr);
		$og_url = get_permalink();
		$og_image = catch_first_image(get_the_content());
		if(empty($og_image)){
			$og_image = get_user_avatar(get_the_author_meta('ID'));
			if( strpos($og_image,'default') !== false){
				$og_image = $placeholderImage;
			}
		}
		$og_type = 'article';
		
		$meta_part = $meta_part . '<meta property="article:published_time" content="'.esc_attr( get_the_date() ).'">';
	} elseif(is_home() || is_front_page()) {
		//$canUrl = WP_HOME;
		$canUrl = $current_url;
		if(isTOI()==true){
			$og_title = 'Times of India Blogs';
			$og_desc = 'Find out the views, opinions, analysis of our experts on entertainment, news, economy, science, spirituality and much more at Times of India';
			$og_keywords = 'Blogs, news blogs, entertainment blogs, economy blogs, funny blogs, blogs on spirituality, science blogs';
		} else {
			$og_title = 'Economic Times Blog: News Blogs, Business Articles & Opinions';
			$og_desc = 'Browse through Business Blogs, News, Articles, Expert Opinions & more on Economic Times Blog. Find news blogs on markets, stocks, wealth, mutual fund & more.';
		}
		$og_url = $current_url;
		$og_image = $placeholderImage;
		$og_type = 'website';
	} elseif( is_tag() ){
		$term_id = get_query_var('tag_id');
		$taxonomy = 'post_tag';
		$args ='include=' . $term_id;
		$terms = get_terms( $taxonomy, $args );
		$tag_name = $terms[0]->name;
		//$tag_link = get_tag_link($terms[0]->term_taxonomy_id);
		$tag_link = $current_url;
		$canUrl = $tag_link;
		$og_title = $tag_name . ' Tag Blog - ' . $sitename;
		$og_desc = get_term_meta( $term_id, 'metades', true );
		if(empty($og_desc)){
			$og_desc = 'Get all the blog post with the '.$tag_name.' tag from our '.$sitename.'.';
		}
		$og_url = $tag_link;
		$og_image = $placeholderImage;
		$og_type = '';
	} elseif( is_category() ){
		$category_name = single_cat_title( '', false );
		$the_cat = get_term_by( 'name', $category_name, 'category' );
		$category_link = $current_url;
		$canUrl = $category_link;
		$og_title = $category_name . ' Blog - ' . $sitename;
		$og_desc = get_term_meta( $the_cat->cat_ID, 'metades', true );
		if(empty($og_desc)){
			$og_desc = 'Browse through '.$category_name.' Blogs, News, Articles, Expert Opinions & more on '.$sitename.'. Find latest news and articles on '.$category_name.'.';
		}
		$og_url = $category_link;
		$og_image = $placeholderImage;
		$og_type = '';
	} elseif( is_tax( 'blog' ) ){
		$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
		$blogAuthorIds = get_option( "blogAuthors_".$term->term_id);
		if($term->slug == 'authors'){
			//$blog_link = get_term_link($term);
			$blog_link = $current_url;
			$canUrl = $blog_link;
			$og_title = ' Blog New Authors, Blog Authors Profile List'.' - ' . $sitename;
			$og_desc = 'Find out the blog new authors list, top blog authors in who have expressed their views, opinions and stories on politics, social issues, sports and other topics at '.$sitename.'.';
			$og_url = $blog_link;
			$og_image = $placeholderImage;
			$og_type = '';
		} else {
			$blog_name = $term->name;
			//$blog_link = get_term_link($term);
			$blog_link = $current_url;
			$canUrl = $blog_link;
			$og_title = $blog_name . ' Blog - ' . $sitename;
			$og_desc = get_term_meta( $term->term_id, 'metades', true );
			if(empty($og_desc)){
				$og_desc = $term->description;
			}
			$og_url = $blog_link;
			$og_image = $placeholderImage;
			$og_type = '';
		}
	} elseif( is_author() ){
		//$author_url = get_author_posts_url(get_the_author_meta( 'ID' ));
		$author_url = $current_url;
		$author_name = get_the_author_meta( 'display_name' );
		$canUrl = $author_url;
		$og_title = $author_name . ' Blog - ' . $sitename;
		$og_desc = get_the_author_meta( 'description' );
		$og_desc = '';
		$og_keywords = ''.$author_name.', '.$author_name.' blog, '.$author_name.' views, '.$author_name.' opinions';
		$og_url = $author_url;
		$og_image = $placeholderImage;
		$og_type = '';
	} elseif( is_search() ){
		$canUrl = $current_url;
		$og_title = $sitename . ' Post Search';
		$og_desc = 'Search result of '.get_search_query().' in '.$sitename.'.';
		$og_url = $current_url;
		$og_image = $placeholderImage;
		$og_type = '';
	}
	
	if ( $paged >= 2 || $page >= 2 ){
		$og_title = sprintf( 'Page %s : %s', max( $paged, $page ),  $og_title);
		$og_desc = sprintf( 'Page %s : %s', max( $paged, $page ),  $og_desc);
	}
	
	$og_desc = str_replace(array("\r", "\n"), array('', ''), strip_tags($og_desc));
	
	if(!empty($canUrl)){
		$meta_part = $meta_part . '<link rel=canonical href="'.$canUrl.'" />';
	}
	$meta_part = $meta_part . '<meta name="description" content="' .$og_desc. '" />';
	if(!empty($og_keywords)){
		$meta_part = $meta_part . '<meta name="keywords" content="' .$og_keywords. '" />';
	}
	$meta_part = $meta_part . '<meta property="og:url" content="' . $og_url . '" />';
	$meta_part = $meta_part . '<meta property="og:title" content="' . $og_title . '"/>';
	$meta_part = $meta_part . '<meta property="og:description" content="' . $og_desc .'"/>';
	if(!empty($og_image)){
		$meta_part = $meta_part . '<meta property="og:image" content="' . $og_image . '"/>';
	}
	if(!empty($og_type)){
		$meta_part = $meta_part . '<meta property="og:type" content="' . $og_type . '"/>';
	}
	
	echo '<title>' . $og_title . '</title>' . $meta_part;
}
add_action( 'wp_head', 'insert_fb_in_head', 5 );

add_action('wp_ajax_getIbeatData', 'getIbeatData');
add_action('wp_ajax_nopriv_getIbeatData', 'getIbeatData');

function getIbeatData()
{
    //echo 'here';
    $url = $_GET['url'];
    $url = str_replace('#TOIRB#', IBEAT_KEY, $url);
    $url = str_replace('#TIMESB#', IBEAT_KEY, $url);
    $url = str_replace('#ETBLOG#', IBEAT_KEY, $url);
    //echo $url;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Set curl to return the data instead of printing it to the browser.
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,10); # timeout after 10 seconds, you can increase it
    curl_setopt($ch, CURLOPT_URL, $url ); #set the url and get string together    
    $result = curl_exec($ch);
    $res = json_decode ($result);
    $html = '';
    $i = 1;
    foreach ($res->DATA as $key => $value){
        if($i==6){
            break;
        }
        $postTitle = get_the_title($key);
        $html .= '<li data-vr-contentbox="">';
        $blogurl = get_permalink($key);
        $html .= '<a pg="MR_Pos#'.$i.'" href="' . $blogurl . '" rel="bookmark" title="Go to '.$postTitle.'">'.$postTitle.' </a>';    
        $html .= '</li>'  ;
        $i++;
    }
    wp_reset_query(); 
    header("Cache-Control: max-age=900"); //30days (60sec * 60min * 24hours * 30days)
    echo json_encode($html);
    die();
}

add_action('wp_ajax_getPlusones', 'getPlusones');
add_action('wp_ajax_nopriv_getPlusones', 'getPlusones');

function getPlusones1()  {
$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, "https://clients6.google.com/rpc");
curl_setopt($curl, CURLOPT_POST, true);
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($curl, CURLOPT_POSTFIELDS, '[{"method":"pos.plusones.get","id":"p","params":{"nolog":true,"id":"'.rawurldecode($_GET['url']).'","source":"widget","userId":"@viewer","groupId":"@self"},"jsonrpc":"2.0","key":"p","apiVersion":"v1"}]');
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
$curl_results = curl_exec ($curl);
curl_close ($curl);
$json = json_decode($curl_results, true);
$res =  isset($json[0]['result']['metadata']['globalCounts']['count'])?intval( $json[0]['result']['metadata']['globalCounts']['count'] ):0;
$response = json_encode($res);
echo $response;
die();
}

function getPlusones()  {
/* get source for custom +1 button */
    $contents = file_get_contents( 'https://plusone.google.com/_/+1/fastbutton?url=' .  rawurldecode($_GET['url']) );
    //echo $contents;
    /* pull out count variable with regex */
    preg_match( '/window\.__SSR = {c: ([\d]+)/', $contents, $matches );
 
    /* if matched, return count, else zed */
    if( isset( $matches[0] ) ) 
        echo (int) str_replace( 'window.__SSR = {c: ', '', $matches[0] );
    else
        echo 0;
    die();
}

add_action('wp_ajax_getSearchData', 'getSearchData');
add_action('wp_ajax_nopriv_getSearchData', 'getSearchData');

function getSearchData (){
    global $wpdb;
    $input = $_GET['input'];
    // fetch authors
    $queryStringUsers = "SELECT display_name as name,CONCAT('".site_url()."/author/',user_nicename) as link FROM {$wpdb->prefix}users u WHERE u.ID != 319 AND u.display_name LIKE '%" . $input . "%' LIMIT 0, 5";
    $tempTerms  = $wpdb->get_results( $queryStringUsers );
    //print_r($tempTerms);
    //echo $wpdb->last_query;
    $queryStringTaxonomies = 'SELECT term.name as name, CONCAT("'.site_url().'/",term.slug) as link FROM ' . $wpdb->term_taxonomy . ' tax ' .
                    'LEFT JOIN ' . $wpdb->terms . ' term ON term.term_id = tax.term_id WHERE 1 = 1 ' .
                    'AND tax.term_id != 9410 ' .
                    'AND term.name LIKE "%' . $input . '%" ' .
                    'AND ( tax.taxonomy = "blog") ' .
                    'ORDER BY tax.count DESC ' .
                    'LIMIT 0, 5';
    //echo $wpdb->last_query;
    $tempTerms2  = $wpdb->get_results( $queryStringTaxonomies );
    $result = array_merge($tempTerms,$tempTerms2);
    echo json_encode($result);
    die();
}


add_action('wp_ajax_getAuthors', 'getAuthors');
add_action('wp_ajax_getAuthors', 'getAuthors');

function getAuthors(){
    global $wpdb;
    $input = $_GET['input'];
    // fetch authors
    $queryStringUsers = "SELECT display_name as name,CONCAT('".site_url()."/author/',user_nicename) as link FROM {$wpdb->prefix}users u WHERE u.display_name LIKE '%" . $input . "%' LIMIT 0, 5";
    $tempTerms  = $wpdb->get_results( $queryStringUsers );
    //print_r($tempTerms);
    //echo $wpdb->last_query;
    $queryStringTaxonomies = 'SELECT term.name as name, CONCAT("'.site_url().'/",term.slug) as link FROM ' . $wpdb->term_taxonomy . ' tax ' .
                    'LEFT JOIN ' . $wpdb->terms . ' term ON term.term_id = tax.term_id WHERE 1 = 1 ' .
                    'AND term.name LIKE "%' . $input . '%" ' .
                    'AND ( tax.taxonomy = "blog") ' .
                    'ORDER BY tax.count DESC ' .
                    'LIMIT 0, 5';
    //echo $wpdb->last_query;
    $tempTerms2  = $wpdb->get_results( $queryStringTaxonomies );
    $result = array_merge($tempTerms,$tempTerms2);
    echo json_encode($result);
    die();
}


add_action('do_meta_boxes', 'customauthor_image_box');

function customauthor_image_box() {
    remove_meta_box('postimagediv', 'post', 'side');
    add_meta_box('postimagediv', __('Custom Author Image'), 'post_thumbnail_meta_box', 'post', 'side', 'low');
}

function new_title( $title, $sep ) {
    $title = '';
    if ( is_singular()){ //if it is not a post or a page
        global $post;
        setup_postdata( $post );
        $title.= get_the_title().' '.$sep.' ';
    }
    if(isTOI()==true){ 
        $title .= 'Times of India Blog';
    }else{
        $title .= 'Economic Times Blog';
    }
    /*global $paged, $page;

    if ( is_feed() )
        return $title;

    // Add the site name.
    $title .= get_bloginfo( 'name' );

    // Add the site description for the home/front page.
    $site_description = get_bloginfo( 'description', 'display' );
    if ( $site_description && ( is_home() || is_front_page() ) )
        $title = "$title $sep $site_description";

    // Add a page number if necessary.
    if ( $paged >= 2 || $page >= 2 )
        $title = "$title $sep " . sprintf( __( 'Page %s', 'twentytwelve' ), max( $paged, $page ) );
    */
    return $title;
}
add_filter( 'wp_title', 'new_title', 10, 2 );

require get_template_directory() . '/apis.php';

add_action('blog_add_form_fields', '_add_taxonomy_metades_field');
add_action('blog_edit_form_fields', '_edit_taxonomy_metades_field');
add_action('category_add_form_fields', '_add_taxonomy_metades_field');
add_action('category_edit_form_fields', '_edit_taxonomy_metades_field');
add_action('post_tag_add_form_fields', '_add_taxonomy_metades_field');
add_action('post_tag_edit_form_fields', '_edit_taxonomy_metades_field');

add_action( 'create_blog', '_save_term_blog' );
add_action( 'edited_blog', '_save_term_blog' );
add_action( 'create_category', '_save_term_category' );
add_action( 'edited_category', '_save_term_category' );
add_action( 'create_post_tag', '_save_term_post_tag' );
add_action( 'edited_post_tag', '_save_term_post_tag' );

function _add_taxonomy_metades_field( $term ) {
	echo '<div class="form-field form-required term-metades-wrap">';
	echo '<label for="metades">Meta Description</label>';
	echo '<textarea name="metades" id="metades" rows="5" cols="40"></textarea>';
	echo '<p class="description">Meta Description for SEO</p>';
	echo '</div>';
}

function _edit_taxonomy_metades_field( $term ){
	$termID = $term->term_id;
	$metades = get_term_meta( $termID, 'metades', true );
	
	echo '<tr class="form-field form-required term-metades-wrap">';
	echo '<th scope="row"><label for="metades">Meta Description</label></th>';
	echo '<td><textarea name="metades" id="metades" rows="5" cols="50" class="large-text">' . esc_attr( $metades ) . '</textarea>';
	echo '<p class="description">Meta Description for SEO</p>';
	echo "</td></tr>";
}

function _save_term_blog( $termID ){
	_save_term( $termID, 'blog' );
}

function _save_term_category( $termID ){
	_save_term( $termID, 'category' );
}

function _save_term_post_tag( $termID ){
	_save_term( $termID, 'post_tag' );
}

function _save_term( $termID, $taxonomyName ){
	if ( isset( $_POST['metades'] ) ) {
		update_term_meta( $termID, 'metades', $_POST['metades'] );
	}
}

function getAppKeyForMyTimes($postId="",$channel="")
{
    $listofWrongAppKeyMSID = array("100096","100094","100083","100081","100082","100077","100073","100042","100046","100038","100049","100048","100051","100043","100070","100063","100055","100053","100035","100031","100030","100018","100027","100026","100024","100022","100029","100028","100021","100013","100011","100006","99948","99949","99943","99951","99947","99950","99964","99954","99935","99933","99930","99929","100001","100003","99905","99911","99909","99907","99903","99899","99912","99901","99894","99889","99888","99879","99999","99995","99848","99841","99843","99846","99836","99845","99874","99853","99850","99852","99820","99992","99985","99988","99981","99978","98007");
    if($postId != '' && in_array($postId, $listofWrongAppKeyMSID))
    {
        $appKeyForMyTimes = "MYT_COMMENTS_API_KEY";
    }
    elseif(!defined("MYT_COMMENTS_API_KEY")|| empty(MYT_COMMENTS_API_KEY))
    {
        $appKeyForMyTimes = ""; 
    }
    else
    {   
        $appKeyForMyTimes = MYT_COMMENTS_API_KEY; 
    }

    if(!empty($channel) && $channel=="et")
    {
        $appKeyForMyTimes = "ETBLOG"; 
    }

    return $appKeyForMyTimes; 
}

function printBottomComments($postId){
    $appKeyForMyTimes = getAppKeyForMyTimes($postId);
    $url = MYTIMES_URL.'mytimes/getFeed/Activity/?msid='.$postId.'&curpg=1&commenttype=agree&appkey='.$appKeyForMyTimes.'&sortcriteria=AgreeCount&order=desc&size=3&pagenum=1';
    $curl_headers = array('Content-Type: application/x-www-form-urlencoded; charset=UTF-8', 'Accept-Encoding: gzip; deflate', 'Accept-Language: en-US');
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url); 
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,300);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $curl_headers);
    $ch_result = curl_exec($ch);
    curl_close($ch);
    $commentsData = json_decode($ch_result, true);
    if(count($commentsData) > 1){
		for($c = 1; $c < count($commentsData); $c++){

            $imgSrc = "https://timesofindia.indiatimes.com/photo/29251859.cms";
            if($commentsData[$c]['PIU']!=''){
               $imgSrc = $commentsData[$c]['PIU']; 
            }

			?>
			<div class="comment-box level1" data-id="<?php echo $commentsData[$c]['_id']; ?>">
				<div class="user-thumbnail"><img class="userimg flL" src="https://timesofindia.indiatimes.com/photo/29251859.cms" data-src="<?php echo str_replace('http://','https://',$imgSrc); ?>"></div>
				<a href="http://mytimes.indiatimes.com/profile/<?php echo $commentsData[$c]['A_ID']; ?>" class="name" target="_blank"><?php echo $commentsData[$c]['A_D_N']; ?></a>
				<p class="short_comment"><?php echo substr(strip_tags($commentsData[$c]['C_T']), 0, 150); ?><?php if(strlen(strip_tags($commentsData[$c]['C_T'])) > 150) { echo '...'; } ?></p>
				<div class="footbar clearfix">
					<span data-action="comment-reply" class="cpointer cp-reply">Reply</span>
					<span class="up cpointer" data-action="comment-agree" title="Up Vote"><i class="icon-uparrow"></i></span>
					<span class="down cpointer" data-action="comment-disagree" title="Down Vote"><i class="icon-downarrow"></i></span>
					<span class="cpointer flag_comment"> <span data-action="comment-offensive" title="Mark as offensive"></span></span>
				</div>
			</div>
			<?php
		}
	}
}

