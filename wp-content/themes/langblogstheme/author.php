<?php
  /**
   * The template for displaying Author archive pages
   *
   * @link http://codex.wordpress.org/Template_Hierarchy
   *
   * @package WordPress
   * @subpackage BlogsTheme
   * @since Blogs Theme 1.0
   */
  
  get_header();
  ?>
<script type='text/javascript'>
  curPage = 'Author';
</script>
<div class="container">
  <div class="row">
    <div class="col-md-8 feeds">
      <?php
        if (have_posts()):
        ?>
      <div class="media authorcard">
        <img class="media-object pull-left img-circle" src="<?php
          echo get_user_avatar(get_the_author_meta('ID'));
          ?>" >
        <div class="media-body">
          <h1 class="media-heading">
            <?php
              /*
               * Queue the first post, that way we know what author
               * we're dealing with (if that is the case).
               *
               * We reset this later so we can run the loop properly
               * with a call to rewind_posts().
               */
              the_post();
              
              printf(__('%s', 'twentyfourteen'), get_the_author());
              ?>
          </h1>
          <?php if ( get_the_author_meta( 'description' ) ) : ?>
          <div class="author-description"><?php the_author_meta( 'description' ); ?></div>
          <?php endif; ?>
        </div>
      </div>
      <?php
        //printf( __( 'Recent posts by %s', 'twentyfourteen' ), get_the_author() );
        
        /*
         * Since we called the_post() above, we need to rewind
         * the loop back to the beginning that way we can run
         * the loop properly, in full.
         */
        rewind_posts();
        
        // Start the Loop.
        while (have_posts()):
            the_post();
            
            /*
             * Include the post format-specific template for the content. If you want to
             * use this in a child theme, then include a file called called content-___.php
             * (where ___ is the post format) and that will be used instead.
             */
            get_template_part('content', get_post_format());
        endwhile;
        // Previous/next page navigation.
        twentyfourteen_paging_nav();
        else:
        // If no content, include the "No posts found" template.
        get_template_part('content', 'none');
        endif;
        ?>
    </div>
    <div class="col-md-4 sidebar">
      <div class="panel">
        <div class="addwrapper">
          <?php echo $my_settings['google_ad_right_1']; ?>
        </div>
      </div>
      <div class="panel">
        <h3 class="panel-title"><?php echo $my_settings['popular_from_author_txt']; ?></h3>
        <?php
          $args = array(
            'posts_per_page' => 5,
            'orderby' => 'comment_count',
            'author' =>get_the_author_ID()
          );
          $popular = new WP_Query($args);
          ?> 
        <ul class="list-unstyled trending">
          <?php
            while ($popular->have_posts()):
                $popular->the_post();
            ?>   
            <li>
              <a href="<?php the_permalink();?>"><?php the_title();?></a>
              <div class="post-date" style="margin-top:3px"><?php
                the_time('F j, Y');
              ?></div>
            </li>
          <?php
            endwhile;
            ?> 
        </ul>
      </div>
          <?php include 'sidebar.php' ?>
    </div>
  </div>
</div>
<?php
get_footer();
