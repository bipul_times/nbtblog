<?php
	global $my_settings;
$lang = isLang();
$fbCode = $_REQUEST['code'];
$state = $_REQUEST['state'];
$stateData = json_decode(stripslashes($state), true);
//print_r($stateData);
$oauthsiteid = $stateData['oauthsiteid'];
?>
<!DOCTYPE HTML><html><head><META http-equiv="Content-Type" content="text/html; charset=UTF-8"><title>SSO</title></head><body>
 Loading.....
 <script type="text/javascript" src="<?php echo $my_settings['jssocdn'];?>/crosswalk/jsso_crosswalk_0.2.5.min.js"></script>
 <script type="text/javascript">
    var CHANNEL = '<?php echo $lang; ?>';
    var SSO_BASE_URL = '<?php echo $my_settings['ssoUrl']?>';
    var SOCIAL_APP_URL = '<?php echo $my_settings['socialappsintegrator'];?>/socialsite';
   // var SOCIAL_APP_URL = 'https://testsocialappsintegrator.indiatimes.com/socialsite';
    var INDEX_PAGE = '<?php echo get_bloginfo( 'url' ); ?>';
    var oauthsiteid = '<?php echo $oauthsiteid;?>';
    
function closeAndRefresh(){
	try {
			if(window.opener && window.opener.location.href){
				window.opener.location.reload(true);
				self.close();
			} else {
				window.location.href=INDEX_PAGE;
			}
		} catch(e){
			window.location.href=INDEX_PAGE;
		}
}

	function QueryStringToJSON() {            
		var pairs = location.search.slice(1).split('&');

		var result = {};
		pairs.forEach(function(pair) {
			pair = pair.split('=');
			result[pair[0]] = decodeURIComponent(pair[1] || '');
		});

		return JSON.parse(JSON.stringify(result));
	}
    
    function checkLogin(){
    	// check self cookie MSCSAuthDetails
    	var logindtls = getCookie("MSCSAuthDetails");
    	if(logindtls == null || logindtls == undefined) {
    		// load script getTicket
    		loadScript(SSO_BASE_URL + '/sso/crossdomain/getTicket?callback=handleTicket&version=v1');
    	} else {
    		this.location.href = INDEX_PAGE;
    	}
    }
    function handleTicket(data) {
    	var ticketid = data.ticketId;
    	if(ticketid != null && ticketid != undefined && ticketid.length > 0) {
    		loadScript(SOCIAL_APP_URL + '/v1validateTicket?ticketId='+ ticketid + '&channel=' + CHANNEL+'&callback=handleTicketValidation');
    	} else {
    		// user is not logged in, do nothing.
    		this.location.href = INDEX_PAGE;
    	}
    }
    function loadScript(url) {
    	var script = document.createElement('script');
    	script.async=true;
    	script.type = 'text/javascript';
    	script.src = url;
    	document.body.appendChild(script);	
    }
    function handleTicketValidation(data){
    	this.location.href = INDEX_PAGE;
    }
    function __loginsuccess(){
        setTimeout(function(){
         if(isPopupActive()){
          setTimeout(function(){document.body.innerHTML = 'Please close this window if it does not close automatically.'}, 5000);
         }else{
            document.body.innerHTML = 'Login Successful!<br/>Please close this window or go to <a href="<?php echo get_bloginfo( 'url' ); ?>" target="_blank" style=" color: blue; text-decoration: underline; "><?php echo get_bloginfo( 'name' ); ?></a>'
         }
        }, 5000);
    }
    
    
    function getCookie(c_name) {
        var i,x,y;
    	var cookieStr = document.cookie.replace("UserName=", "UserName&");
    	var ARRcookies=cookieStr.split(";");
    
        for (i=0;i<ARRcookies.length;i++)
        {
            x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
            y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
            x=x.replace(/^\s+|\s+$/g,"");
            if (x==c_name)
            {
                return unescape(y);
            }
         }
    }
    
    function isPopupActive(){
        return window.parent.location.href != window.location.href;
    }
    
    
    
   (function(){   
       
        var jssoObj = {};
            if (typeof JssoCrosswalk === 'function') {
                jssoCrosswalkObj = new JssoCrosswalk('<?php echo $my_settings['channel'];?>', 'web');
                if(oauthsiteid == 'facebook'){
                    var redirectURL = '<?php echo home_url();?>/t-login-sso';
                    jssoCrosswalkObj.facebookLogin('<?php echo $fbCode ?>', redirectURL, fbLoginCallback);
                }else if(oauthsiteid == 'googleplus'){
                    var redirectURL = '<?php echo home_url();?>/t-login-sso';
                    jssoCrosswalkObj.googleplusLogin('<?php echo $fbCode ?>', redirectURL, fbLoginCallback);
                }
            }
       
       
        function fbLoginCallback(data){
            document.domain = "<?php echo $my_settings['common_cookie_domain']; ?>"; 
            var data = QueryStringToJSON();
            data.sso = true;
            try{
                    if(!data.site){
                        data.site = oauthsiteid;
                    }
                
                    if(navigator.userAgent.match('CriOS')) {  // if ios chrome
                        localStorage.setItem('_ssodata',JSON.stringify(data)); 
                    }

                    if(top && top.__sso){
                      top.__sso(data);
                    }else if(opener && opener.__sso){
                      if(data.status == "mobileverified"){opener._tv_mobver(); this.close();}
                      opener.__sso(data);
                    }
                    __loginsuccess();
                    if (isPopupActive()) {
                        this.close();
                    } else {
                        this.location.href = INDEX_PAGE;
                    }
                       //checkLogin(); // if browser ignore to close this
            } catch (err) {
                checkLogin();
            }


           }
       }())
</script></body></html>
