<?php 

define('Z_IMAGE_PLACEHOLDER', get_template_directory_uri()."/images/placeholder.png");
define('FB_PLACEHOLDER', get_template_directory_uri()."/images/blog-eisamay-facebook.png");
define('Z_AUTHOR_PLACEHOLDER', get_template_directory_uri()."/images/50.jpg");
define('RECO_URL','http://reco.indiatimes.com/Reco/RecoIndexer?eventType=update&hostid=1b&contentType=blog&msid=');
define('COMMENTS_URL','http://www.eisamayblogcmtapi.indiatimes.com/');
define('RCHID', '2147478036');
define('MYT_COMMENTS_API_KEY', 'ESB');
//define('MYTIMES_URL','http://myt.indiatimes.com/');
if(getenv("environment")=='live'){
    define('MYTIMES_URL','https://myt.indiatimes.com/');
}else{
    define('MYTIMES_URL','https://mytest.indiatimes.com/');
}

global $my_settings;
$my_settings['site_lang'] = 'bn';
$my_settings['og_locale'] = 'bn_IN';
$my_settings['google_site_verification'] = 'ZgFICIedNvVZl5pV9EfAUeenwta9vBY0Za_GgmV4zuw';
$my_settings['fblikeurl'] = 'https://www.facebook.com/eisamay.com';
$my_settings['favicon'] = 'https://eisamay.indiatimes.com/icons/ei-samay_favicon.ico';
$my_settings['ga'] = "<script>

TimesGDPR.common.consentModule.gdprCallback(function(data) {
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-29031733-3', 'auto', {'allowLinker': true});
  var userCookie = document.cookie.match(/(?:\s)?ssoid=(\w+);?/);
  if(!!(userCookie)) {
	ga('set', 'userId', userCookie[1]);
  }
  if( TimesGDPR.common.consentModule.isEUuser() ){
  ga('set', 'anonymizeIp', true); 
  }
  ga('require', 'displayfeatures'); 
  ga('require', 'linker');
  ga('linker:autoLink', ['superpacer.in', 'atmadeep.com', 'womenbikerally.com', 'pujoeisamay.com', 'eisamay.com', 'esyoungscholars.com'] );
  ga('send', 'pageview');
});  
  </script>";
$my_settings['fbappid'] = '117787264903013';
$my_settings['channel_url_part'] = 'channel=eisamay';
$my_settings['main_site_url'] = 'https://eisamay.indiatimes.com';
$my_settings['main_site_txt'] = 'Eisamay Blog';
$my_settings['fb_url'] = 'https://www.facebook.com/eisamay.com';
$my_settings['twitter_url'] = 'https://twitter.com/ei_samay';
$my_settings['twitter_handle'] = 'ei_samay';
$my_settings['google_url'] = 'https://plus.google.com/115386879891428361302/posts';
$my_settings['rss_url'] = 'https://eisamay.indiatimes.com/rssfeedsdefault.cms';
$my_settings['logo_title'] = 'Eisamay Blogs';
$my_settings['logo_url'] = 'https://eisamay.indiatimes.com/photo/48422948.cms';
$my_settings['footer_logo_txt'] = 'এই সময়';
$my_settings['comscore_tag'] = '<!-- Begin comScore Tag -->
<script>
  var _comscore = _comscore || [];
  var objComScore = { c1: "2", c2: "6036484" };
  TimesGDPR.common.consentModule.gdprCallback(function(data) {
    if( TimesGDPR.common.consentModule.isEUuser() ){
      objComScore["cs_ucfr"] = 0;
    }
   _comscore.push(objComScore); 
  (function() {
	var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
	s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";
	el.parentNode.insertBefore(s, el);
  })();
  });
   
</script>
<noscript><img src="http://b.scorecardresearch.com/p?c1=2&amp;c2=6036484&amp;cv=2.0&amp;cj=1"></noscript>
<!-- End comScore Tag -->';
$my_settings['ibeat_channel'] = 'EisamayBlog';
$my_settings['ibeat_host'] = 'blogs.eisamay.indiatimes.com';
$my_settings['ibeat_key'] = 'a096b08f339c54c45e2c3c4316877a';
$my_settings['ibeat_domain'] = 'eisamay.indiatimes.com';
$my_settings['footer_dmp_tag'] = 'https://static.clmbtech.com/ase/7270/20/aa.js';
$my_settings['footer_google_tag'] = '';
$my_settings['google_tag_js_head'] = "<script type='text/javascript'>
var googletag = googletag || {};
googletag.cmd = googletag.cmd || [];
(function() {
var gads = document.createElement('script');
gads.async = true;
gads.type = 'text/javascript';
var useSSL = 'https:' == document.location.protocol;
gads.src = (useSSL ? 'https:' : 'http:') + 
'//www.googletagservices.com/tag/js/gpt.js';
var node = document.getElementsByTagName('script')[0];
node.parentNode.insertBefore(gads, node);
})();
</script>

<script type='text/javascript'>
googletag.cmd.push(function() {
 <!-- Audience Segment Targeting -->
 var _auds = new Array();
 if(typeof(_ccaud)!='undefined') {
 for(var i=0;i<_ccaud.Profile.Audiences.Audience.length;i++)
 if(i<200)
  _auds.push(_ccaud.Profile.Audiences.Audience[i].abbr);
  }
  <!-- End Audience Segment Targeting --> 
 <!-- Contextual Targeting -->
  var _HDL = '';
  var _ARC1 = '';
  var _Hyp1 = '';
  var _article = '';
  var _tval = function(v) {
  if(typeof(v)=='undefined') return '';
  if(v.length>100) return v.substr(0,100);
  return v;
  }
  <!-- End Contextual Targeting -->
    googletag.defineSlot('/7176/Eisamay/ES_Home/ES_Home_Home/ES_HP_ATF_300', [[300, 250], [300, 1050]], 'div-gpt-ad-1453364679185-0').addService(googletag.pubads());
    googletag.defineSlot('/7176/Eisamay/ES_Home/ES_Home_Home/ES_HP_ATF_728', [[728, 90], [1003, 90]], 'div-gpt-ad-1453364679185-1').addService(googletag.pubads());
    googletag.defineSlot('/7176/Eisamay/ES_Home/ES_Home_Home/ES_HP_BTF_300', [300, 250], 'div-gpt-ad-1453364679185-2').addService(googletag.pubads());
    googletag.defineSlot('/7176/Eisamay/ES_Home/ES_Home_Home/ES_HP_BTF_300_2', [300, 250], 'div-gpt-ad-1453364679185-3').addService(googletag.pubads());
    googletag.defineSlot('/7176/Eisamay/ES_Home/ES_Home_Home/ES_HP_BTF_728', [[728, 90], [1003, 90]], 'div-gpt-ad-1453364679185-4').addService(googletag.pubads());
     googletag.pubads().enableSingleRequest();
    googletag.pubads().collapseEmptyDivs();
TimesGDPR.common.consentModule.gdprCallback(function(data) {
  if( TimesGDPR.common.consentModule.isEUuser() ) {
    googletag.pubads().setRequestNonPersonalizedAds(1);
  }
  
} );
    
  });
</script>";
$my_settings['google_ad_top'] = "<!-- /7176/Eisamay/ES_Home/ES_Home_Home/ES_HP_ATF_728 -->
<div id='div-gpt-ad-1453364679185-1'>
<script type='text/javascript'>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1453364679185-1'); });
</script>
</div>";
$my_settings['google_ad_right_1'] = "<!-- /7176/Eisamay/ES_Home/ES_Home_Home/ES_HP_BTF_300 -->
<div id='div-gpt-ad-1453364679185-2'>
<script type='text/javascript'>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1453364679185-2'); });
</script>
</div>";
$my_settings['google_ad_right_2'] = "<!-- /7176/Eisamay/ES_Home/ES_Home_Home/ES_HP_BTF_300_2 -->
<div id='div-gpt-ad-1453364679185-3'>
<script type='text/javascript'>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1453364679185-3'); });
</script>
</div>";
$my_settings['visual_revenue_reader_response_tracking_script'] = "";
$my_settings['visual_revenue_reader_response_tracking_script_for_not_singular'] = "";
$my_settings['not_found_heading'] = 'পাওয়া যায়নি';
$my_settings['not_found_txt'] = 'এই জায়গায় কিছু খুঁজে পাওয়া যায়নি, অন্য কোথাও খুঁজে দেখবেন? ';
$my_settings['search_placeholder_txt'] = '৩টি ক্যারেক্টার টাইপ করুন';
$my_settings['go_to_the_profile_of_txt'] = '';
$my_settings['go_to_txt'] = '';
$my_settings['share_link_fb_txt'] = 'ফেসবুকে লিঙ্ক শেয়ার করুন';
$my_settings['share_link_twitter_txt'] = 'টুইটারে লিঙ্ক শেয়ার করুন';
$my_settings['share_link_linkedin_txt'] = 'লিঙ্কডইন-এ শেয়ার করুন';
$my_settings['mail_link_txt'] = 'লিঙ্ক মেল করুন';
$my_settings['read_complete_article_here_txt'] = 'সম্পূর্ণ লেখাটি পড়ুন';
$my_settings['featured_txt'] = 'ফিচারড';
$my_settings['disclaimer_txt'] = 'ডিসক্লেমার';
$my_settings['disclaimer_on_txt'] = " টাইমস অফ ইন্ডিয়ার প্রিন্টে এটি সম্পাদকীয় বিভাগে প্রকাশিত হয়েছে";
$my_settings['disclaimer_off_txt'] = 'সব ভাবনা লেখকের নিজের';
$my_settings['most discussed_txt'] = 'সব থেকে বেশি আলোচিত';
$my_settings['more_txt'] = 'আরও';
$my_settings['less_txt'] = 'কম';
//$my_settings['login_txt'] = '';
//$my_settings['logout_txt'] = '';
$my_settings['login_txt'] = 'Log In';
$my_settings['logout_txt'] = 'Log Out';
$my_settings['view_all_posts_in_txt'] = '';
$my_settings['home_txt'] = 'হোম';
$my_settings['blogs_txt'] = 'ব্লগ';
$my_settings['search_results_for_txt'] = 'সার্চ রেজাল্ট';
$my_settings['most_read_txt'] = 'সব থেকে বেশি পড়া হয়েছে';
$my_settings['popular_tags_txt'] = 'জনপ্রিয় ট্যাগ';
$my_settings['recently_joined_authors_txt'] = 'সম্প্রতি যে সব লেখক এসেছেন';
$my_settings['like_us_txt'] = 'আমাদের লাইক করুন';
$my_settings['author_txt'] = ' লেখক';
$my_settings['popular_from_author_txt'] = 'লেখকের সব থেকে জনপ্রিয় লেখা';
$my_settings['search_authors_by_name_txt'] = 'নাম দিয়ে লেখক খুঁজে নিন';
$my_settings['search_txt'] = 'সার্চ ';
$my_settings['back_to_authors_page_txt'] = 'লেখকের পেজে ফিরে যান';
$my_settings['no_authors_found_txt'] = 'কোনও লেখক পাওয়া যায়নি';
$my_settings['further_commenting_is_disabled_txt'] = 'আর মন্তব্য করা যাবে না';
$my_settings['comments_on_this_post_are_closed_now_txt'] = 'এই পোস্টে মন্তব্য করা যাবে না';
$my_settings['add_your_comment_here_txt'] = 'এখানে আপনার মন্তব্য লিখুন';
$my_settings['characters_remaining_txt'] = 'বাকি থাকা ক্যারেক্টার';
$my_settings['share_on_fb_txt'] = 'ফেসবুকে শেয়ার করুন';
$my_settings['share_on_twitter_txt'] = 'টুইটারে শেয়ার করুন';
$my_settings['sort_by_txt'] = 'বাছুন';
$my_settings['newest_txt'] = 'সব থেকে নতুন';
$my_settings['oldest_txt'] = 'সব থেকে পুরনো';
$my_settings['discussed_txt'] = 'আলোচিত';
$my_settings['up_voted_txt'] = 'আপ ভোটেড';
$my_settings['down_voted_txt'] = 'ডাউন ভোটেড';
$my_settings['be_the_first_one_to_review_txt'] = 'সবার আগে রিভিউ করুন';
$my_settings['more_points_needed_to_reach_next_level_txt'] = 'পরের লেভেলে যেতে আরও পয়েন্ট লাগবে';
$my_settings['know_more_about_times_points_txt'] = 'টাইমস পয়েন্ট সম্পর্কে আরও জানুন';
$my_settings['know_more_about_times_points_link'] = 'http://www.timespoints.com/about/8483497468384772096';
$my_settings['badges_earned_txt'] = 'ব্যাজ জিতেছেন';
$my_settings['just_now_txt'] = 'এই মুহূর্তে';
$my_settings['follow_txt'] = 'ফলো করুন';
$my_settings['reply_txt'] = 'উত্তর দিন';
$my_settings['flag_txt'] = 'ফ্লাগ করুন';
$my_settings['up_vote_txt'] = 'আপ ভোট';
$my_settings['down_vote_txt'] = 'ডাউন ভোট';
$my_settings['mark_as_offensive_txt'] = 'আপত্তিজনক';
$my_settings['find_this_comment_offensive_txt'] = 'এই মন্তব্য আপত্তিজনক মনে হচ্ছে?';
$my_settings['reason_submitted_to_admin_txt'] = 'আপনার যুক্তি অ্যাডমিনের কাছে পৌঁছে গেছে';
$my_settings['choose_reason_txt'] = 'আপনার যুক্তি নিচের থেকে বেছে নিয়ে সাবমিট বাটন ক্লিক করুন। আমাদের মডারেটররা যথাযথ সিদ্ধান্ত নেবেন। ';
$my_settings['reason_for_reporting_txt'] = 'কেন রিপোর্ট করলেন';
$my_settings['foul_language_txt'] = 'অকথ্য ভাষা';
$my_settings['defamatory_txt'] = 'অপমানজনক';
$my_settings['inciting_hatred_against_certain_community_txt'] = 'একটি বিশেষ জাতের প্রতি বিদ্বেষ';
$my_settings['out_of_context_spam_txt'] = 'অপ্রাসঙ্গিক/স্প্যাম';
$my_settings['others_txt'] = 'অন্যান্য';
$my_settings['report_this_txt'] = 'রিপোর্ট করুন!';
$my_settings['close_txt'] = 'বন্ধ';
$my_settings['already_marked_as_offensive'] = 'ইতিমধ্যে আপত্তিজনক হিসেবে চিহ্নিত';
$my_settings['flagged_txt'] = 'ফ্ল্যাগ করা আছে';
$my_settings['blogauthor_link'] = 'http://author.blogs.eisamay.indiatimes.com/';
$my_settings['old_blogauthor_link'] = 'http://author.blogs.eisamay.indiatimes.com/';
$my_settings['blog_link'] = 'http://blogs.eisamay.indiatimes.com/';
$my_settings['common_cookie_domain'] = 'indiatimes.com';
$my_settings['view_comments'] ='कॉमेंट्स देखें';
$my_settings['all_comments'] ='कॉमेंट्स';
$my_settings['quill_lang'] = 'bengali';
$my_settings['quill_link_1'] = 'বাংলায় লিখুন (ইনস্ক্রিপ্ট)';
$my_settings['quill_link_2'] = 'বাংলায় লিখুন (ইংরেজি অক্ষরে)';
$my_settings['quill_link_3'] = 'Write in English';
$my_settings['quill_link_4'] = 'ভার্চুয়াল কি-বোর্ড';
$my_settings['footer'] = '<div class="container footercontainer">
  <div class="insideLinks">
    <h2>এক নজরে খবর</h2>
    <ul>
      <li><a href="http://eisamay.indiatimes.com/city/articlelist/15819618.cms" pg="fotkjlnk1" target="_blank">শহর </a></li>
      <li><a href="http://eisamay.indiatimes.com/state/articlelist/15819609.cms" target="_blank" pg="fotkjlnk9">রাজ্য </a></li>
      <li><a href="http://eisamay.indiatimes.com/nation/articlelist/15819599.cms" pg="fotkjlnk22" target="_blank">দেশ </a></li>
      <li><a href="http://eisamay.indiatimes.com/international/articlelist/15819594.cms" pg="fotkjlnk3" target="_blank">দুনিয়া </a></li>
      <li><a href="http://eisamay.indiatimes.com/sports/articlelist/23000116.cms" target="_blank" pg="fotkjlnk10">খেলার সময় </a></li>
      <li><a href="http://eisamay.indiatimes.com/business/articlelist/15819574.cms" target="_blank" pg="fotkjlnk23">ব্যবসা বাণিজ্য </a></li>
      <li><a href="http://eisamay.indiatimes.com/editorial/articlelist/15819584.cms" pg="fotkjlnk4" target="_blank">সম্পাদকীয় </a></li>
      <li><a href="http://photogallery.eisamay.indiatimes.com" target="_blank" pg="fotkjlnk11">ফটো গ্যালারি </a></li>
      <li><a href="http://eisamay.indiatimes.com/entertainment/entertainmentlist/15819570.cms" pg="fotkjlnk14" target="_blank">বিনোদন </a></li>
      <li><a href="http://eisamay.indiatimes.com/kolkata-collage-kolkattewali/articlelist/20742872.cms" pg="fotkjlnk5" target="_blank">কলকাত্তেওয়ালি </a></li>
      <li><a href="http://eisamay.indiatimes.com/sunday-special-rabibaroari/articlelist/20742861.cms" target="_blank" pg="fotkjlnk15">রবিবারোয়ারি </a></li>
      <li><a href="http://eisamay.indiatimes.com/lifestyle/articlelist/15992436.cms" target="_blank" pg="fotkjlnk17">লাইফস্টাইল </a></li>
      <li><a href="http://eisamay.indiatimes.com/videos/videolist/16683331.cms" pg="fotkjlnk2" target="_blank">ভিডিও </a></li>
      <li><a href="http://eisamay.indiatimes.com/livetv/16682993.cms" pg="fotkjlnk16" target="_blank">লাইভ টিভি </a></li>
    </ul>
  </div>
  <div class="newsletter">
    <div class="fottershareLinks">
      <h4 style="color:#fff">সবসময় যুক্ত থাকুন<br>
        <strong>এই সময়</strong> অ্যাপের সঙ্গে</h4>
      <div class="fotterApplinks"><a class="ios" target="_blank" href="https://itunes.apple.com/in/app/eisamay/id679530233?mt=8"></a><a href="https://play.google.com/store/apps/details?id=com.eisamay.reader&amp;hl=en" target="_blank" class="andriod"></a></div>
    </div>
  </div>
  <div class="timesotherLinks">
    <h2>আমাদের অন্যান্য সাইট</h2>
    <a href="http://timesofindia.indiatimes.com/" target="_blank" pg="fottgwslnk1">Times of India</a>| <a target="_blank" href="http://economictimes.indiatimes.com/" pg="fottgwslnk2">Economic Times</a> | <a href="http://maharashtratimes.indiatimes.com/" target="_blank" pg="fottgwslnk5">Marathi News</a> | <a href="http://navgujaratsamay.indiatimes.com/" target="_blank" pg="fottgwslnk7">Gujarati News</a>| <a href="http://vijaykarnataka.indiatimes.com/" target="_blank" pg="fottgwslnk7">Kannada News</a>|<a href="http://tamil.samayam.com/" target="_blank" pg="fottgwslnk9">Tamil News</a> | <a href="http://telugu.samayam.com/" target="_blank" pg="fottgwslnk10">Telugu News</a> | <a href="http://malayalam.samayam.com/" target="_blank" pg="fottgwslnk11">Malayalam News</a> | <a href="http://www.businessinsider.in/" target="_blank" pg="fottgwslnk12">Business Insider</a>| <a href="http://zoomtv.indiatimes.com/" target="_blank" pg="fottgwslnk13">ZoomTv</a> | <a href="http://boxtv.com/" target="_blank" pg="fottgwslnk16">BoxTV</a>| <a href="http://www.gaana.com/" target="_blank" pg="fottgwslnk17">Gaana</a> | <a href="http://shopping.indiatimes.com/" target="_blank" pg="fottgwslnk18">Shopping</a> | <a href="http://www.idiva.com/" target="_blank" pg="fottgwslnk19">IDiva</a> | <a target="_blank" href="http://www.simplymarry.com/" pg="fottgwslnk21">Matrimonial</a><span class="footfbLike"> ফেসবুকে এই সময়</span>
    <div data-share="false" data-show-faces="false" data-action="like" data-layout="button_count" data-href="http://www.facebook.com/eisamay.com" style="width:310px;height:20px;overflow:hidden;margin-bottom: 10px;" class="fb-like"></div>
  </div>
  <div class="seoLinks">
    <ul>
      <li><a style="border-left:none !important" title="Bangla News" target="_blank" href="http://eisamay.indiatimes.com/"> Bangla News </a></li>
      <li><a target="_blank" href="http://eisamay.indiatimes.com/nation/articlelist/15819599.cms"> India News </a></li>
      <li><a target="_blank" href="http://eisamay.indiatimes.com/Kolkata/articlelist/15991773.cms">Kolkata News</a></li>
      <li><a target="_blank" href="http://eisamay.indiatimes.com/entertainment/entertainmentlist/15819570.cms">Entertainment News</a></li>
      <li><a target="_blank" href="http://eisamay.indiatimes.com/lifestyle/articlelist/15992436.cms">Lifestyle News</a> </li>
      <li> <a target="_blank" href="http://eisamay.indiatimes.com/astro.cms"> Religious News </a> </li>
     <li> <a target="_blank" href="http://eisamay.indiatimes.com/entertainment/entertainment/film-review/articlelist/15900258.cms"> Movie Reviews </a> </li>
      <li> <a target="_blank" href="http://eisamay.indiatimes.com/lifestyle/food/articlelist/15992590.cms"> Food </a> </li>
      <li> <a target="_blank" href="http://navbharattimes.indiatimes.com/"> Hindi News </a> </li>
    </ul>
  </div>
  <div class="timesLinks"><a target="_blank" href="http://www.timesinternet.in/" pg="fotlnk1">About Us</a>&nbsp;|&nbsp; <a target="_blank" href="https://www.ads.timesinternet.in/expresso/selfservice/loginSelfService.htm">Create Your Own Ad</a>&nbsp;|&nbsp; <a target="_blank" href="http://advertise.indiatimes.com/" pg="fotlnk2">Advertise with Us</a>&nbsp;|&nbsp; <a target="_blank" href="http://eisamay.indiatimes.com/termsandcondition.cms" pg="fotlnk3">Terms of Use and Grievance Redressal Policy</a>&nbsp;|&nbsp; <a target="_blank" href="http://eisamay.indiatimes.com/privacypolicy.cms" pg="fotlnk4">Privacy Policy</a>&nbsp;|&nbsp; <span class="feedbackeu"><a target="_blank" href="mailto:grievance.es@timesinternet.in" pg="fotlnk5">Feedback</a>&nbsp;|&nbsp;</span> <a target="_blank" href="http://eisamay.indiatimes.com/sitemap.cms" pg="fotlnk6">Sitemap</a></div><div class="copyText">Copyright &copy;&nbsp; '.date('Y').' &nbsp;Bennett Coleman &amp; Co. Ltd. All rights reserved. For reprint rights: <a target="_blank" href="http://timescontent.com/" pg="fotlnk7">Times Syndication Service</a></div>
</div>';
$my_settings['footer_css'] = '<style type="text/css">
.footercontainer{background:#333333; clear:both; overflow:hidden; padding:10px 0;}
.footercontainer a{color:#FDFDFD;}
.footercontainer h2{ color:#fff; font-size:20px; line-height:30px; padding:0 0 10px 2px;font-family: Arial, Helvetica, sans-serif;}

.insideLinks{ width:36%; padding-left:2%; float:left;}
.insideLinks ul{ list-style:none; display: inline-block; padding: 0; margin: 0}
.insideLinks ul li{ float:left; margin:0 10px 0 0; width:90px; }
.insideLinks ul li a{ font-family:Arial, Helvetica, sans-serif;color:#B5B3B4;font-size:14px;font-weight:bold;line-height:25px;}

.newsletter{width:32%; padding:0 2%; float:left;}
.newsletterpost {background:#CCCCCC; padding:14px; margin:10px 0 10px 0;float:left; width:300px;}

.newsletterpost input[type="text"]{background:#fff; border:1px #B7B7B7 solid; color:#333;height:21px; width:205px; font-size:14px;font-weight:bold;color:#999;float:left; padding:2px 10px;}
.newsletterpost input[type="submit"]{ background:#f5cc10; color:#000; padding:0 5px; *padding:0 4px; height:26px; cursor:pointer; border:none;font-weight:bold;float:left;}
.fottershareLinks span{ font-size:14px; color:#fff; width:100%; float:left; margin-bottom:10px;}
.fottershareLinks img{ vertical-align:middle;margin-top:5px;}
.fottershareLinks a{margin: 0 2px;}

.fotterApplinks{padding: 0 0 15px 0;}
.fotterApplinks a{padding: 0 10px 0 0; background-image:url(https://navbharattimes.indiatimes.com/photo/42711833.cms); float:left; width:47px; height:65px;background-repeat: no-repeat;}
.fotterApplinks a.andriod{background-position:0 4px}
.fotterApplinks a.andriod:hover{background-position:0 -68px}

.fotterApplinks a.ios{background-position: 0 -144px;}
.fotterApplinks a.ios:hover{background-position:0 -219px}

.fotterApplinks a.java{background-position:0 -296px;}
.fotterApplinks a.java:hover{background-position:0 -378px;}

.fotterApplinks a.win{background-position:0 -447px}
.fotterApplinks a.win:hover{background-position:0 -526px}

.fottershareLinks{clear: both;}
.fottershareLinks h4{color:#fff; font-size:16px;line-height: 23px;}
.fottershareLinks strong{color:#53a3e4; font-weight:normal}

.footfbLike{display: block;margin: 25px 0 12px 0;font-size: 14px;color: #fff;}

.timesotherLinks{width:32%; float:left; padding-right:2%; color:#CCCCCC;font-size:12px;}
.timesotherLinks a{ font-size:11px; padding:0 1px; color:#CCCCCC; line-height:18px;}

.timesLinks{ font-family:Arial;clear:both;text-align:center; font-size:12px;color:#929292; padding:10px 0;}
.timesLinks a {color:#FDFDFD; font-family:Arial;}

.copyText{ font-family:Arial;clear:both;text-align:center; font-size:12px;color:#929292; padding:0 35px;}
.copyText a {color:#FDFDFD; font-family:Arial;}

.seoLinks{clear:both; margin:10px 0; background:#585757; overflow:hidden;padding-left: 11px;}
.seoLinks ul{list-style:none; padding:0;}
.seoLinks ul li{float:left;}
.seoLinks ul li a{font-family:arial;padding:0 7px;color:#fff;font-size:12px; line-height:32px; display:block; float:left; border-left:#4a4a4a 1px solid;  border-right:#696969 1px solid;}
@media (max-width: 767px){.insideLinks,.newsletter,.timesotherLinks,.seoLinks,.timesLinks{display: none;}}
</style>';

// esi ads setting

$my_settings['esi_status'] = 'LIVE';// TEST, LIVE, NONE

if(isMobile()){
	                             
	$esiHeaderStr = '<!--esi 
           <esi:eval src="/fetch_native_content/?fpc=$url_encode($(HTTP_COOKIE{\'_col_uuid\'}))&ab=$(HTTP_COOKIE{\'ce_aemsp\'})&id=$url_encode(\'##ESIIDS##\')&_t=4&_u=$url_encode($(HTTP_HOST)+$(REQUEST_PATH))&ua=$url_encode($(HTTP_USER_AGENT))&ip=$(REMOTE_ADDR)&_v=0&dpv=1&r=$rand()" dca="esi"/> 
           -->';

        $esiHeaderStr .= <<<'ESISTR'
<!--esi           
                        <esi:choose>
                            <esi:when test="$exists($(native_content{'_col_uuid'}))">
                		        $add_header('Set-Cookie', $(native_content{'_col_uuid'}))
                            </esi:when>
                        </esi:choose>
                    -->
                
                    <!--esi
                        <esi:choose>
                            <esi:when test="$exists($(native_content{'_col_script'}))">
                		        $(native_content{'_col_script'})
                            </esi:when>
                        </esi:choose>
                    -->
                
                    <!--esi
                        <esi:choose>
                            <esi:when test="$exists($(native_content{'_col_ab_call'}))">
                		        $(native_content{'_col_ab_call'})
                            </esi:when>
                        </esi:choose>
                    -->
ESISTR;
$my_settings['esi_header'] = $esiHeaderStr;

	$my_settings['esi_homepage_ids'] = '311288~1~0';
	
	/*HP_CTN_NAT*/
	$my_settings['esi_homepage'] = '<!--esi
						<esi:choose>
							<esi:when test="$exists($(native_content{\'311288~1~0\'}))">
								$(native_content{\'311288~1~0\'})
							</esi:when>
						</esi:choose>
					-->  ';
					
	/*HP_RHS_RCMW_CTN_NAT*/
	$my_settings['esi_homepage_rhs'] = "";
	
	$my_settings['esi_article_list_ids'] = '311289~1~0';
	
	/*ROS_AL_CTN_NAT*/
	$my_settings['esi_article_list'] = '<!--esi
						<esi:choose>
							<esi:when test="$exists($(native_content{\'311289~1~0\'}))">
								$(native_content{\'311289~1~0\'})
							</esi:when>
						</esi:choose>
					-->  ';
	
	/*ROS_AL_RHS_RCMW_CTN_NAT*/
	$my_settings['esi_article_list_rhs'] = "";
	
	$my_settings['esi_article_show_ids'] = '311290~1~0';
	
	/*ROS_AS_RHS_RCMW_CTN_NAT*/
	$my_settings['esi_article_show_rhs'] = "";
	
	/*ROS_AS_EOA_RCMW_CTN_NAT*/
	$my_settings['esi_article_show_end_article_1'] = '<!--esi
						<esi:choose>
							<esi:when test="$exists($(native_content{\'311290~1~0\'}))">
								$(native_content{\'311290~1~0\'})
							</esi:when>
						</esi:choose>
					-->  ';
	
	/*ROS_AS_EOA1_RCMW_CTN_NAT*/
	$my_settings['esi_article_show_end_article_2'] = ""; 
	
	/*Mweb_AS_BLY_VDO_CTN_NAT*/                               
	$my_settings['ctn_article_show_mid_article_video'] = '';
	
}else{

	$esiHeaderStr = '<!--esi 
           <esi:eval src="/fetch_native_content/?fpc=$url_encode($(HTTP_COOKIE{\'_col_uuid\'}))&ab=$(HTTP_COOKIE{\'ce_esiap\'})&id=$url_encode(\'##ESIIDS##\')&_t=4&_u=$url_encode($(HTTP_HOST)+$(REQUEST_PATH))&ua=$url_encode($(HTTP_USER_AGENT))&ip=$(REMOTE_ADDR)&_v=0&dpv=1&r=$rand()" dca="esi"/> 
           -->';

        $esiHeaderStr .= <<<'ESISTR'
<!--esi           
                        <esi:choose>
                            <esi:when test="$exists($(native_content{'_col_uuid'}))">
                		        $add_header('Set-Cookie', $(native_content{'_col_uuid'}))
                            </esi:when>
                        </esi:choose>
                    -->
                
                    <!--esi
                        <esi:choose>
                            <esi:when test="$exists($(native_content{'_col_script'}))">
                		        $(native_content{'_col_script'})
                            </esi:when>
                        </esi:choose>
                    -->
                
                    <!--esi
                        <esi:choose>
                            <esi:when test="$exists($(native_content{'_col_ab_call'}))">
                		        $(native_content{'_col_ab_call'})
                            </esi:when>
                        </esi:choose>
                    -->
ESISTR;
$my_settings['esi_header'] = $esiHeaderStr;


	$my_settings['esi_homepage_ids'] = '311281~1~0,311282~1~0';
	
	/*HP_CTN_NAT*/
	$my_settings['esi_homepage'] = '<!--esi
						<esi:choose>
							<esi:when test="$exists($(native_content{\'311281~1~0\'}))">
								$(native_content{\'311281~1~0\'})
							</esi:when>
						</esi:choose>
					-->  ';
	
	/*HP_RHS_RCMW_CTN_NAT*/
	$my_settings['esi_homepage_rhs'] = '<!--esi
						<esi:choose>
							<esi:when test="$exists($(native_content{\'311282~1~0\'}))">
								$(native_content{\'311282~1~0\'})
							</esi:when>
						</esi:choose>
					-->  ';
					
	$my_settings['esi_article_list_ids'] = '311284~1~0,311285~1~0';
	
	/*ROS_AL_CTN_NAT*/
	$my_settings['esi_article_list'] = '<!--esi
						<esi:choose>
							<esi:when test="$exists($(native_content{\'311284~1~0\'}))">
								$(native_content{\'311284~1~0\'})
							</esi:when>
						</esi:choose>
					-->  ';
	
	/*ROS_AL_RHS_RCMW_CTN_NAT*/
	$my_settings['esi_article_list_rhs'] = '<!--esi
						<esi:choose>
							<esi:when test="$exists($(native_content{\'311285~1~0\'}))">
								$(native_content{\'311285~1~0\'})
							</esi:when>
						</esi:choose>
					-->  ';
					
	$my_settings['esi_article_show_ids'] = '311286~1~0,311287~1~0';
	
	/*ROS_AS_RHS_RCMW_CTN_NAT*/
	$my_settings['esi_article_show_rhs'] = '<!--esi
						<esi:choose>
							<esi:when test="$exists($(native_content{\'311286~1~0\'}))">
								$(native_content{\'311286~1~0\'})
							</esi:when>
						</esi:choose>
					-->  ';
					
	/*ROS_AS_EOA_RCMW_CTN_NAT*/
	$my_settings['esi_article_show_end_article_1'] = '<!--esi
						<esi:choose>
							<esi:when test="$exists($(native_content{\'311287~1~0\'}))">
								$(native_content{\'311287~1~0\'})
							</esi:when>
						</esi:choose>
					-->  ';
					
	/*ROS_AS_EOA1_RCMW_CTN_NAT*/
	$my_settings['esi_article_show_end_article_2'] = '';          
	
	/*AS_BLY_VDO_CTN_NAT*/  
	$my_settings['ctn_article_show_mid_article_video'] = '';                                                
}

$my_settings['ATF_728_DIV_ID'] ='div-gpt-ad-1453364679185-1';
$my_settings['ATF_300_DIV_ID'] ='div-gpt-ad-1453364679185-2';
$my_settings['BTF_300_DIV_ID'] ='div-gpt-ad-1453364679185-3';
$my_settings['ATF_728_AD_CODE'] ='/7176/Eisamay/ES_Home/ES_Home_Home/ES_HP_ATF_728';
$my_settings['ATF_300_AD_CODE'] ='/7176/Eisamay/ES_Home/ES_Home_Home/ES_HP_BTF_300';
$my_settings['BTF_300_AD_CODE'] ='/7176/Eisamay/ES_Home/ES_Home_Home/ES_HP_BTF_300_2';
$my_settings['channel'] ='eisamay';
$my_settings['site_id'] ='9ac35c7be4b4ae6d525be548c7664f22';
$my_settings['domain'] ='indiatimes.com';
$my_settings['ofcommenthostid'] ='304';
$my_settings['ofcommentchannelid'] ='2147478036';
$my_settings['appid'] ='117787264903013';
$my_settings['gPlusClient'] ='265054015577-n4ep9siuh3vjn02oe9vmgcjoi0p6mk4b.apps.googleusercontent.com';
$commnetText = ["name_required" =>"Please enter your name.",
			"location_required" =>"Please enter your location.",
			"captcha_required" =>"Please enter captcha value.",
			"name_toolong" =>"Name cannot be longer than 30 chars.",
			"name_not_string" =>"Name can only contain alphabets.",
			"location_toolong" =>"Location cannot be longer than 30 chars.",
			"location_not_string" =>"Location can only contain alphabets.",
			"captcha_toolong" =>"Captcha cannot be longer than 4 chars.",
			"captcha_number_only" =>"Captcha value can only be a number.",
			"email_required" =>"Please enter your email address.",
			"email_invalid" =>"Please enter a valid email address.",
			"captcha_invalid" =>"Please enter a valid captcha value.",
			"minlength"=> "You can t post this comment as the length it is too short. ",
			"blank"=> "You can t post this comment as it is blank.",
			"maxlength"=> "You have entered more than 3000 characters.",
			"popup_blocked"=> "Popup is blocked.",
			"has_url"=> "You can t post this comment as it contains URL.",
			"duplicate"=> "You can t post this comment as it is identical to the previous one.",
			"abusive"=> "You can't post this comment as it contains inappropriate content.",
			"self_agree"=> "You can't Agree with your own comment",
			"self_disagree"=> "You can't Disagree with your own comment",
			"self_recommend"=> "You can't Recommend your own comment",
			"self_offensive"=> "You can't mark your own comment as Offensive",
			"already_agree"=> "You have already Agreed with this comment",
			"already_disagree"=> "You have already Disagreed with this comment",
			"already_recommended"=> "You have already Recommended this comment",
			"already_offensive"=> "You have already marked this comment Offensive",
			"cant_agree_disagree"=> "You can't Agree and Disagree with the same comment",
			"cant_agree_offensive"=> "You can't Agree and mark the same comment Offensive",
			"cant_disagree_recommend"=> "You can't Disagree and Recommend the same comment",
			"cant_recommend_offensive"=> "You can't Recommend and mark the same comment Offensive",
			"permission_facebook"=> "You can't post to facebook. Post permission is required.",
			"offensive_reason"=> "Please select a reason.",
			"offensive_reason_text"=> "Please enter a reason." ,
			"offensive_reason_text_limit"=> "Please enter less than 200 chars.",
			"be_the_first_text"=> "Be the first one to review.",
			"no_comments_discussed_text"=> "None of the comments have been discussed.",
			"no_comments_up_voted_text"=> "None of the comments have been up voted.",
			"no_comments_down_voted_text"=> "None of the comments have been down voted."
		 ];
$my_settings['comment_text'] = (json_encode($commnetText));
if(getenv("environment")=='live'){
    $my_settings['ssoUrl']      = 'https://jsso.indiatimes.com';
    $my_settings['myTimesUrl']  = 'https://myt.indiatimes.com';
    $my_settings['jssocdn']  = 'https://jssocdn.indiatimes.com';
    $my_settings['socialappsintegrator']  = 'https://socialappsintegrator.indiatimes.com';
     
}else{
    $my_settings['ssoUrl']      = 'https://jssostg.indiatimes.com';
    $my_settings['myTimesUrl']  = 'https://mytest.indiatimes.com';
    $my_settings['jssocdn']  = 'https://jssocdnstg.indiatimes.com';
    $my_settings['socialappsintegrator']  = 'https://testsocialappsintegrator.indiatimes.com';
}

