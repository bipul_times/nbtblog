<?php
$lang = isLang();
setEnvironment();
if(!empty($lang)){
	if (strpos($_SERVER['HTTP_HOST'], 'readerblogs.navbharattimes.indiatimes.com') !== false || strpos($_SERVER['HTTP_HOST'], 'vsp1nbtreaderblogs.indiatimes.com') !== false ) {
        require get_template_directory() . '/constants-readernbt.php';
    }else{
        require get_template_directory() . '/constants-'.$lang.'.php';
    }
}
require get_template_directory() . '/bloghooks.php';

/*function blogsThemeInit($oldname, $oldtheme=false) {
    remove_role('subscriber');
    remove_role('editor');           
}

add_action("after_switch_theme", "blogsThemeInit", 10 ,  2);

//restrict authors to only being able to view media that they've uploaded

add_filter('parse_query', 'restrict_posts');
function restrict_posts($wp_query)
{
    //are we looking at the Media Library or the Posts list?
    if (strpos($_SERVER['REQUEST_URI'], '/wp-admin/upload.php') !== false || strpos($_SERVER['REQUEST_URI'], '/wp-admin/edit.php') !== false) {
        //user level 5 converts to Editor
        if (!current_user_can('level_5')) {
            //restrict the query to current user
            global $current_user;
            $wp_query->set('author', $current_user->id);
        }
    }
}

//restrict authors to only being able to view media that they've uploaded

add_action('pre_get_posts', 'restrict_media_library');
function restrict_media_library($wp_query_obj)
{
    global $current_user, $pagenow;
    if (!is_a($current_user, 'WP_User'))
        return;
    if ('admin-ajax.php' != $pagenow || $_REQUEST['action'] != 'query-attachments')
        return;
    if (!current_user_can('manage_media_library'))
        $wp_query_obj->set('author', $current_user->ID);
    return;
}*/


function z_add_style()
{
    echo '<style type="text/css" media="screen">
    th.column-thumb {width:60px;}
    .form-field img.taxonomy-image {border:1px solid #eee;max-width:300px;max-height:300px;}
    .inline-edit-row fieldset .thumb label span.title {width:48px;height:48px;border:1px solid #eee;display:inline-block;}
    .column-thumb span {width:48px;height:48px;border:1px solid #eee;display:inline-block;}
    .inline-edit-row fieldset .thumb img,.column-thumb img {width:48px;height:48px;}
    </style>';
}

// add image field in add form
function z_add_texonomy_field()
{
    wp_enqueue_media();
    echo '<div class="form-field">
    <label for="taxonomy_image">' . __('Image', 'zci') . '</label>
    <input type="text" name="taxonomy_image" id="taxonomy_image" value="" />
    <br/>
    <button class="z_upload_image_button button">' . __('Upload/Add image', 'zci') . '</button>
    </div>' . z_script();
    
}

// add image field in edit form
function z_edit_texonomy_field($taxonomy)
{
    wp_enqueue_media();
    if (z_taxonomy_image_url($taxonomy->term_id, NULL, TRUE) == Z_IMAGE_PLACEHOLDER)
        $image_text = "";
    else
        $image_text = z_taxonomy_image_url($taxonomy->term_id, NULL, TRUE);
    echo '<tr class="form-field">
    <th scope="row" valign="top"><label for="taxonomy_image">' . __('Image', 'zci') . '</label></th>
    <td><img class="taxonomy-image" src="' . z_taxonomy_image_url($taxonomy->term_id, NULL, TRUE) . '"/><br/><input type="text" name="taxonomy_image" id="taxonomy_image" value="' . $image_text . '" /><br />
    <button class="z_upload_image_button button">' . __('Upload/Add image', 'zci') . '</button>
    <button class="z_remove_image_button button">' . __('Remove image', 'zci') . '</button>
    </td>
    </tr>' . z_script();
    ?>
    <tr class="form-field">
        <th> Add/Remove authors </th>
        <td> 
          <div>
              <p> <input type="text" id="select-user" name="select-user" class="newtag form-input-tip" size="16" autocomplete="off" value="">
                <input type="hidden" id="author-input" name="author-input" />
                <div id="selector-dd" style="display:none;min-width:50px;z-index: 500;float: left;position: absolute;background: white;border: 1px solid;opacity: .8;margin-left: 1px;padding: 2px;">
                </div>
                <p class="howto">Type at least 3 letters and select user from dropdown. Hit update button when done</p>
                <div class="tagchecklist">
                </div>
            </div>
        </td>
    </tr>
    <?php
    echo '<script type="text/javascript">';
    echo 'var taxonomyId ="' . $taxonomy->term_id . '";';
    echo 'var authorNames = []; var authorIds = [];';
    $blogAuthorIds = get_option("blogAuthors_" . $taxonomy->term_id);
    if ($blogAuthorIds && !empty($blogAuthorIds)) {
        foreach ($blogAuthorIds as $authorId) {
            if (!empty($authorId)) {
                $author = get_user_by('id', $authorId);
                echo 'authorIds.push("' . $authorId . '");';                
                echo 'authorNames.push("' . $author->user_nicename . '");';                
            }
        }
    }
    echo addAuthorAjaxScript();
}

// upload using wordpress upload
function addAuthorAjaxScript()
{
    return '
    jQuery(document).ready(function($) {

     $("#author-input").val(authorIds.join("-"))
     fillSpan();

     $("#select-user").keyup(function(){ 
        var inputValue = $("#select-user").val().trim();
        if(inputValue.length>2){
          jQuery.ajax({
              type: "POST",
              url: "' . site_url() . '/wp-admin/admin-ajax.php",
              dataType: "json",
              data: {
                action: "autoSuggestUsers",
                input: $("#select-user").val(),
            },
            success: function(data, textStatus, XMLHttpRequest){
                $("#selector-dd").empty();
                for (key in data.data ){
                  $("#selector-dd").append("<a id=user-"+key+">"+data.data[key]+"</a><br>");
                  $("#selector-dd").show();
              }
          },
          error: function(MLHttpRequest, textStatus, errorThrown){
            alert("hello"+errorThrown);
        }
    });
}
});

$("#selector-dd > a").live("click",function(e){
  var userId = e.target.getAttribute("id").split("-")[1];
  if(authorIds.indexOf(userId)==-1){
        //new author id added
    var userName = e.target.innerHTML;
    authorIds.push(userId);
    authorNames.push(userName);
    $("#author-input").val(authorIds.join("-"))
    fillSpan();
}
$("#selector-dd").hide();
$("#select-user").val("");  
});

function fillSpan(){
  $(".tagchecklist").empty();
  for (var key in authorIds ){
      $(".tagchecklist").append("<span><a id=author-"+authorIds[key]+" class=\'ntdelbutton\'>X</a>"+authorNames[key]+"</span>");      
  }
}

$(".ntdelbutton").live("click", function(e){
  var userId = e.target.getAttribute("id").split("-")[1];
  var index =  authorIds.indexOf(userId);      
  authorIds.splice(index,1);
  authorNames.splice(index,1);
  $("#author-input").val(authorIds.join("-"))
  fillSpan();
  this.remove();
});
});
</script>';
}

// upload using wordpress upload
function z_script()
{
    return '<script type="text/javascript">
    jQuery(document).ready(function($) {
      var wordpress_ver = "' . get_bloginfo("version") . '", upload_button;
      $(".z_upload_image_button").click(function(event) {
        upload_button = $(this);
        var frame;
        if (wordpress_ver >= "3.5") {
          event.preventDefault();
          if (frame) {
            frame.open();
            return;
        }
        frame = wp.media();
        frame.on( "select", function() {
            // Grab the selected attachment.
            var attachment = frame.state().get("selection").first();
            frame.close();
            if (upload_button.parent().prev().children().hasClass("tax_list")) {
              upload_button.parent().prev().children().val(attachment.attributes.url);
              upload_button.parent().prev().prev().children().attr("src", attachment.attributes.url);
          }
          else
              $("#taxonomy_image").val(attachment.attributes.url);
      });
frame.open();
}
else {
  tb_show("", "media-upload.php?type=image&amp;TB_iframe=true");
  return false;
}
});

$(".z_remove_image_button").click(function() {
    $("#taxonomy_image").val("");
    $(this).parent().siblings(".title").children("img").attr("src","' . Z_IMAGE_PLACEHOLDER . '");
    $(".inline-edit-col :input[name=\'taxonomy_image\']").val("");
    return false;
});

if (wordpress_ver < "3.5") {
    window.send_to_editor = function(html) {
      imgurl = $("img",html).attr("src");
      if (upload_button.parent().prev().children().hasClass("tax_list")) {
        upload_button.parent().prev().children().val(imgurl);
        upload_button.parent().prev().prev().children().attr("src", imgurl);
    }
    else
        $("#taxonomy_image").val(imgurl);
    tb_remove();
}
}

$(".editinline").live("click", function(){  
  var tax_id = $(this).parents("tr").attr("id").substr(4);
  var thumb = $("#tag-"+tax_id+" .thumb img").attr("src");
  if (thumb != "' . Z_IMAGE_PLACEHOLDER . '") {
      $(".inline-edit-col :input[name=\'taxonomy_image\']").val(thumb);
  } else {
      $(".inline-edit-col :input[name=\'taxonomy_image\']").val("");
  }
  $(".inline-edit-col .title img").attr("src",thumb);
  return false;  
});  
});
</script>';
}

add_action('blog_add_form_fields', 'z_add_texonomy_field');
add_action('blog_edit_form_fields', 'z_edit_texonomy_field');
add_filter('manage_edit-blog_columns', 'z_taxonomy_columns');
add_filter('manage_blog_custom_column', 'z_taxonomy_column', 10, 3);

// save our taxonomy image while edit or save term
add_action('edit_term', 'z_save_taxonomy_image');
add_action('create_term', 'z_save_taxonomy_image');
function z_save_taxonomy_image($term_id)
{
    if (isset($_POST['taxonomy_image']))
        update_option('z_taxonomy_image' . $term_id, $_POST['taxonomy_image'], false);
    if (isset($_POST['author-input'])) {
        $authorIds = explode('-', $_POST['author-input']);
        $previousAuthorIds = get_option("blogAuthors_".$term_id);
        foreach ($authorIds as $id) {
            if(!in_array($id, $previousAuthorIds)){
                // this id was newly added
                $authoringBlogIds = get_user_meta($id,'authoringBlogIds',true);
                if (!in_array($term_id, $authoringBlogIds)) {
                    array_push($authoringBlogIds, $term_id);
                    update_user_meta($id, 'authoringBlogIds', $authoringBlogIds);
                }
            }
        }
        foreach ($previousAuthorIds as $id) {
            if(!in_array($id, $authorIds)){
                // this id was removed   
                $authoringBlogIds = get_user_meta($id,'authoringBlogIds',true);
                array_splice($authoringBlogIds, array_search($term_id, $authoringBlogIds), 1);
                update_user_meta($id, 'authoringBlogIds', $authoringBlogIds);
            }
        }
        update_option("blogAuthors_".$term_id, $authorIds, false);
    }
}

// get attachment ID by image url
function z_get_attachment_id_by_url($image_src)
{
    global $wpdb;
    $query = "SELECT ID FROM {$wpdb->posts} WHERE guid = '$image_src'";
    $id    = $wpdb->get_var($query);
    return (!empty($id)) ? $id : NULL;
}

// get taxonomy image url for the given term_id (Place holder image by default)
function z_taxonomy_image_url($term_id = NULL, $size = NULL)
{
    if (!$term_id) {
        if (is_category())
            $term_id = get_query_var('cat');
        elseif (is_tax()) {
            $current_term = get_term_by('slug', get_query_var('term'), get_query_var('taxonomy'));
            $term_id      = $current_term->term_id;
        }
    }
    
    $taxonomy_image_url = get_option('z_taxonomy_image' . $term_id);
    $attachment_id      = z_get_attachment_id_by_url($taxonomy_image_url);
    if (!empty($attachment_id)) {
        if (empty($size))
            $size = 'full';
        $taxonomy_image_url = wp_get_attachment_image_src($attachment_id, $size);
        $taxonomy_image_url = $taxonomy_image_url[0];
    }else {
        //check in backup
        $upload_dir = wp_upload_dir();
        $src = $upload_dir['basedir'].'/backup/'.$taxonomy_image_url;
        if (file_exists($src)) {
            $taxonomy_image_url = $upload_dir['baseurl'].'/backup/'.$taxonomy_image_url;
        }
    }
    return ($taxonomy_image_url != '') ? $taxonomy_image_url : Z_IMAGE_PLACEHOLDER;
}

function z_quick_edit_custom_box($column_name, $screen, $name)
{
    if ($column_name == 'thumb')
    echo '<fieldset>
    <div class="thumb inline-edit-col">
    <label>
    <span class="title"><img src="" alt="Thumbnail"/></span>
    <span class="input-text-wrap"><input type="text" name="taxonomy_image" value="" class="tax_list" /></span>
    <span class="input-text-wrap">
    <button class="z_upload_image_button button">' . __('Upload/Add image', 'zci') . '</button>
    <button class="z_remove_image_button button">' . __('Remove image', 'zci') . '</button>
    </span>
    </label>
    </div>
    </fieldset>';
}

/**
 * Thumbnail column added to category admin.
 *
 * @access public
 * @param mixed $columns
 * @return void
 */
function z_taxonomy_columns($columns)
{
    $new_columns            = array();
    $new_columns['cb']      = $columns['cb'];
    $new_columns['thumb']   = __('Image', 'zci');
    $new_columns['authors'] = __('Authors', 'zci');
    
    unset($columns['cb']);
    
    return array_merge($new_columns, $columns);
}

/**
 * Thumbnail column value added to category admin.
 *
 * @access public
 * @param mixed $columns
 * @param mixed $column
 * @param mixed $id
 * @return void
 */
function z_taxonomy_column($columns, $column, $id)
{
    if ($column == 'thumb')
        $columns = '<span><img src="' . z_taxonomy_image_url($id, NULL, TRUE) . '" alt="' . __('Thumbnail', 'zci') . '" class="wp-post-image" /></span>';
    else if ($column == 'authors') {
        $blogAuthorIds = get_option("blogAuthors_" . $id);
        if ($blogAuthorIds) {
            $authors     = get_users(array(
                'include' => $blogAuthorIds,
                'fields' => array(
                    'user_nicename'
                    )
                ));
            $authorNames = array();
            foreach ($authors as $author) {
                array_push($authorNames, $author->user_nicename);
            }
            // print_r($authorNames);
            $columns = implode(',', $authorNames);
        }
    }
    return $columns;
}

// change 'insert into post' to 'use this image'
function z_change_insert_button_text($safe_text, $text)
{
    return str_replace("Insert into Post", "Use this image", $text);
}

// style the image in category list
if (strpos($_SERVER['SCRIPT_NAME'], 'edit-tags.php') > 0) {
    add_action('admin_head', 'z_add_style');
    add_action('quick_edit_custom_box', 'z_quick_edit_custom_box', 10, 3);
    add_filter("attribute_escape", "z_change_insert_button_text", 10, 2);
}

// Validating options
function z_options_validate($input)
{
    return $input;
}

// creating Ajax call for autosuggesting users in the add to Add To Blog field  
add_action('wp_ajax_autoSuggestUsers', 'autoSuggestUsers');
function autoSuggestUsers()
{
    //get the data from ajax() call  
    $input     = $_POST['input'];
    $users     = get_users('search=*' . $input . '*');
    $userArray = array();
    foreach ($users as $user) {
        if($user->ID!='1'){
            $userArray[$user->ID] = $user->user_login;
        }
    }
    $result['status'] = "success";
    $result['data']   = $userArray;
    echo json_encode($result);
    die();
}

function get_user_avatar($id){
    if(!function_exists('get_wp_user_avatar_src')){
        return null;
    }
    //check if avatar is set
    $avatar_new = get_wp_user_avatar_src($id);
    $avatar_new = str_replace( $my_settings['blogauthor_link'],'../../',$avatar_new);
    $avatar_new = str_replace( $my_settings['old_blogauthor_link'],'../../',$avatar_new);
    if( strpos($avatar_new,'default') === false){
        //uploaded image exists
        return $avatar_new;
    }else{
        $avatar = get_user_meta($id, 'avatar',true);
        if(empty($avatar)){
			return $avatar_new;
        }
        $upload_dir = wp_upload_dir();
        $src = $upload_dir['basedir'].'/backup/'.$avatar;
        
        if (file_exists($src)) {
            return $upload_dir['baseurl'].'/backup/'.$avatar;
        }else{    
			// check for et source
			return $avatar_new;
        }
    }
}

add_action('admin_head', 'hide_menu');
function hide_menu() {
    if ( wp_get_current_user()->ID != 1 ) {
		// To remove the whole Appearance admin menu you would use;
		remove_menu_page( 'themes.php' );
		remove_menu_page( 'plugins.php' );
		remove_menu_page( 'options-general.php' );
		remove_menu_page( 'wp-user-avatar' );
		remove_menu_page( 'w3tc_dashboard' );
		remove_menu_page( 'wp_stream' );
		remove_menu_page( 'edit.php?post_type=page' );
		remove_menu_page( 'edit-comments.php' );
		remove_menu_page( 'tools.php' );
		remove_submenu_page( 'index.php', 'update-core.php' );
		remove_submenu_page( 'themes.php', 'theme-editor.php' );
		remove_submenu_page( 'themes.php', 'widgets.php' );
		remove_submenu_page( 'themes.php', 'nav-menus.php' );
		remove_submenu_page( 'themes.php', 'customize.php' );
		remove_submenu_page( 'themes.php', 'custom-background' );
	}
}

// remove links/menus from the admin bar
add_action( 'wp_before_admin_bar_render', 'mytheme_admin_bar_render' );
function mytheme_admin_bar_render() {
    if ( wp_get_current_user()->ID != 1 ) {
		global $wp_admin_bar;
		$wp_admin_bar->remove_menu('themes');
		$wp_admin_bar->remove_menu('customize');
		$wp_admin_bar->remove_menu('widgets');
		$wp_admin_bar->remove_menu('menus');
		$wp_admin_bar->remove_menu('view-site');
		$wp_admin_bar->remove_node( 'wp-logo');
		$wp_admin_bar->remove_node( 'new-page');
		$wp_admin_bar->remove_node( 'w3tc' );
		$wp_admin_bar->remove_node( 'comments' );
	}
}

add_action( 'admin_menu', 'remove_meta_boxes' );
function remove_meta_boxes() {
	if ( wp_get_current_user()->ID != 1 ) {
		remove_meta_box('tagsdiv-blog', 'post', 'normal');
		remove_meta_box('formatdiv', 'post', 'normal');
		remove_meta_box('commentsdiv','post','normal');
		remove_meta_box('postcustom','post','normal');
		//remove_meta_box('slugdiv','post','normal');
		remove_meta_box('trackbacksdiv','post','normal');
	}
}

add_action('admin_menu', 'remove_the_dashboard');
function remove_the_dashboard () {
    if (current_user_can('level_10')) {
        return;
    }
    else {
        global $menu, $submenu, $user_ID;
        $the_user = new WP_User($user_ID);
        reset($menu); $page = key($menu);
        while ((__('Dashboard') != $menu[$page][0]) && next($menu))
            $page = key($menu);
            if (__('Dashboard') == $menu[$page][0]) unset($menu[$page]);
            reset($menu); $page = key($menu);
            while (!$the_user->has_cap($menu[$page][1]) && next($menu))
                $page = key($menu);
            if (preg_match('#wp-admin/?(index.php)?$#',$_SERVER['REQUEST_URI']) && ('index.php' != $menu[$page][2]))
                wp_redirect(get_option('siteurl') . '/wp-admin/post-new.php');
   }
}

add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );
function custom_excerpt_length( $length ) {
    return 10;
}

add_filter('excerpt_more', 'new_excerpt_more');
function new_excerpt_more( $more ) {
    return '...';
}

function getTrendingByCat($cat){
        global $my_settings;
        $args = array(
            'date_query' => array(
                            array(
                                'column' => 'post_date',
                                'after' => '2 months ago',
                                ),
                            ),
                        'posts_per_page' => 10,
                        'orderby' => 'comment_count',
                        'category_name' =>$cat
                        );
             $popular = new WP_Query($args);
                $i=0;             
                while ($popular->have_posts()) : $popular->the_post();$i++; ?>   
                <li>
                    <span class="pull-left number"><?php echo $i; ?>.</span>
                    <?php the_title( '<h4><a href="' . esc_url( get_permalink() ) . '" rel="bookmark" title="'.$my_settings['go_to_txt'].' '.get_the_title().'">', '</a></h4>' );?>  
                    <!--<span class="post-date"><?php the_time('F j, Y') ?></span>-->
                </li> 
            <?php endwhile; 
}

function getPopularByCat($cat){
        global $my_settings;
        $args = array(
                        'posts_per_page' => 5,
                        'orderby' => 'comment_count',
                        'category_name' =>$cat
                        );
             $popular = new WP_Query($args);
        while ($popular->have_posts()) : $popular->the_post(); ?>   
        <div class="media">
        <img class="pull-left media-object img-circle" src="<?php echo get_user_avatar(get_the_author_meta('ID')); ?>" >
        <div class="media-body">
          <?php the_title( '<h4 class="media-heading"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark" title="'.$my_settings['go_to_txt'].' '.get_the_title().'">', '</a></h4>' );?>  
          <span class="post-date"><?php the_time('F j, Y') ?></span>
        </div>
        </div>
    <?php endwhile; 
}


function getTrending(){
    global $my_settings;
    $args = array(
            'date_query' => array(
                            array(
                                'column' => 'post_date',
                                'after' => '2 weeks ago',
                                ),
                            ),
                        'posts_per_page' => 10,
                        'orderby' => 'comment_count'
                        );
            $popular = new WP_Query($args);
            $i=0;
            echo '<div class="panel"><h3 class="panel-title">'.$my_settings['most_discussed_txt'].'</h3><ul class="panel-body list-unstyled trending" data-vr-zone="Most Discussed">';
            while ($popular->have_posts()) : $popular->the_post();$i++; ?>   
                <li data-vr-contentbox="">
                    <span class="pull-left number"><?php echo $i; ?>.</span>
                    <?php the_title( '<h4><a pg="Trending_Pos" href="' . esc_url( get_permalink() ) . '" rel="bookmark" title="'.$my_settings['go_to_txt'].' '.get_the_title().'">', '</a></h4>' );?>  
                    <!--<span class="post-date"><?php the_time('F j, Y') ?></span>-->
                </li> 
            <?php endwhile; 
            echo '</ul></div>';
            ?> 
    <?php
}

function getSideWidget(){
    global $my_settings;
    echo '<div class="panel"><h3 class="panel-title">'.$my_settings['most_discussed_txt'].'</h3><ul class="panel-body list-unstyled trending" data-vr-zone="Most Discussed">';            
    $args = array(
            'date_query' => array(
                            array(
                                'column' => 'post_date',
                                'after' => '2 weeks ago',
                                ),
                            ),
                        'posts_per_page' => 10,
                        'orderby' => 'comment_count'
                        );
            $popular = new WP_Query($args);
            $i=0;
            while ($popular->have_posts()) : $popular->the_post();$i++; ?>   
                <li data-vr-contentbox="">
                    <span class="pull-left number"><?php echo $i; ?>.</span>
                    <?php the_title( '<h4><a pg="Trending_Pos" href="' . esc_url( get_permalink() ) . '" rel="bookmark" title="'.$my_settings['go_to_txt'].' '.get_the_title().'">', '</a></h4>' );?>  
                    <!--<span class="post-date"><?php the_time('F j, Y') ?></span>-->
                </li> 
            <?php endwhile; 
    echo '</ul></div>';      
}

function getPopular($popular){
    global $my_settings;
    $args = array(
            'date_query' => array(
                            array(
                                'column' => 'post_date',
                                'after' => '6 months ago',
                                ),
                            ),
                        'posts_per_page' => 5,
                        'orderby' => 'comment_count'
                        );
    $popular = new WP_Query($args); 
    while ($popular->have_posts()) : $popular->the_post(); ?>   
        <div class="media">
        <img class="pull-left media-object img-circle" src="<?php echo get_user_avatar(get_the_author_meta('ID')); ?>" >
        <div class="media-body">
          <?php the_title( '<h4 class="media-heading"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark" title="'.$my_settings['go_to_txt'].' '.get_the_title().'">', '</a></h4>' );?>  
          <span class="post-date"><?php the_time('F j, Y') ?></span>
        </div>
        </div>
    <?php endwhile; ?> 
<?php
}


function getMostDiscussedToday(){
    global $my_settings;
    $args = array(
            'date_query' => array(
                            array(
                                'column' => 'post_date',
                                'after' => '1 day ago',
                                ),
                            ),
                        'posts_per_page' => 5,
                        'orderby' => 'comment_count'
                        );
            $popular = new WP_Query($args);
            $i=0;
            while ($popular->have_posts()) : $popular->the_post();$i++; ?>   
                <li data-vr-contentbox="">
                    <span class="pull-left number"><?php echo $i; ?>.</span>
                    <?php the_title( '<h4><a pg="Trending_Pos" href="' . esc_url( get_permalink() ) . '" rel="bookmark" title="'.$my_settings['go_to_txt'].' '.get_the_title().'">', '</a></h4>' );?>  
                    <!--<span class="post-date"><?php the_time('F j, Y') ?></span>-->
                </li> 
            <?php endwhile; 
            ?> 
    <?php
}

function getMostDiscussedWeek(){
    global $my_settings;
    $args = array(
            'date_query' => array(
                            array(
                                'column' => 'post_date',
                                'after' => '1 week ago',
                                ),
                            ),
                        'posts_per_page' => 5,
                        'orderby' => 'comment_count'
                        );
            $popular = new WP_Query($args);
            $i=0;
            while ($popular->have_posts()) : $popular->the_post();$i++; ?>   
                <li data-vr-contentbox="">
                    <span class="pull-left number"><?php echo $i; ?>.</span>
                    <?php the_title( '<h4><a pg="Trending_Pos" href="' . esc_url( get_permalink() ) . '" rel="bookmark" title="'.$my_settings['go_to_txt'].' '.get_the_title().'">', '</a></h4>' );?>  
                    <!--<span class="post-date"><?php the_time('F j, Y') ?></span>-->
                </li> 
            <?php endwhile; 
            ?> 
    <?php
}

function getMostDiscussedMonth(){
    global $my_settings;
    $args = array(
            'date_query' => array(
                            array(
                                'column' => 'post_date',
                                'after' => '1 month ago',
                                ),
                            ),
                        'posts_per_page' => 5,
                        'orderby' => 'comment_count'
                        );
            $popular = new WP_Query($args);
            $i=0;
            while ($popular->have_posts()) : $popular->the_post();$i++; ?>   
                <li data-vr-contentbox="">
                    <span class="pull-left number"><?php echo $i; ?>.</span>
                    <?php the_title( '<h4><a pg="Trending_Pos" href="' . esc_url( get_permalink() ) . '" rel="bookmark" title="'.$my_settings['go_to_txt'].' '.get_the_title().'">', '</a></h4>' );?>  
                    <!--<span class="post-date"><?php the_time('F j, Y') ?></span>-->
                </li> 
            <?php endwhile; 
            ?> 
    <?php
}

function getRecentlyBlogged(){
    global $my_settings;
    $args = array(
            'date_query' => array(
                            array(
                                'column' => 'post_date',
                                'after' => '1 month ago',
                                ),
                            ),
                        'posts_per_page' => 5,
                        'orderby' => 'comment_count'
                        );
            $popular = new WP_Query($args);
            $i=0;
            while ($popular->have_posts()) : $popular->the_post();$i++; ?>   
                <li data-vr-contentbox="">
                    <span class="pull-left number"><?php echo $i; ?>.</span>
                    <?php the_title( '<h4><a pg="Trending_Pos" href="' . esc_url( get_permalink() ) . '" rel="bookmark" title="'.$my_settings['go_to_txt'].' '.get_the_title().'">', '</a></h4>' );?>  
                    <!--<span class="post-date"><?php the_time('F j, Y') ?></span>-->
                </li> 
            <?php endwhile; 
            ?> 
    <?php
}

function getRecentlyJoinedAuthors(){
	$recentlyauthorslist = array();
	$recentlyauthorslist['count'] = 0;
	$recentlyauthorslist['html'] = '';
	$recent_users = new WP_User_Query( 
      array( 
        'meta_query'   => array(
			'relation' => 'OR',
			array(
			  'compare' => '!=',
			  'key' => 'hide_on_front',
			  'value' => 'yes',
			),
			array(
			  'compare' => 'NOT EXISTS',
			  'key' => 'hide_on_front'
			)
		  ),
		'query_id' => 'authors_with_posts',
		'orderby' => 'user_registered',
		'order' => 'DESC',
        'offset' => 0 ,
        'number' => 10,
        'fields' => 'ids',
        'exclude' => array('1')
      ));
    $users = $recent_users->get_results();
	$recentlyauthorslist['html'] = $recentlyauthorslist['html'] . '<ul class="panel-body list-inline recentauthors">';
	foreach($users as $user) { 
		$user_info = get_userdata($user);
		$recentlyauthorslist['html'] = $recentlyauthorslist['html'] . '<li><a href="'.get_author_posts_url( $user ).'" target="_blank"><img data-src="'.get_user_avatar($user).'" width="55" height="55" alt="'.$user_info->display_name.'" title="'.$user_info->display_name.'" class="avatar avatar-50 wp-user-avatar wp-user-avatar-50 alignnone photo"></a></li>';
		$recentlyauthorslist['count'] = $recentlyauthorslist['count'] + 1;
	}
	$recentlyauthorslist['html'] = $recentlyauthorslist['html'] . '</ul>';
	
	return $recentlyauthorslist;
}

add_action( 'admin_head', 'showhiddencustomfields' );

function showhiddencustomfields() {
    echo "<style type='text/css'>#misc-publishing-actions #visibility {
        display: none;
    }
#wp-admin-bar-view-site{display:none}
    </style>";
}

add_action( 'user_register', 'addMetaFields', 10, 1 );

function addMetaFields( $user_id ) {
    update_user_meta($user_id, 'authoringBlogIds', array());
}


/** 
* Commenting Systems code
**/
add_action('wp_ajax_postComment', 'postComment');
add_action('wp_ajax_nopriv_postComment', 'postComment');

function postComment(){
    //close connection
    $result['status'] = "success";
    $result['data']   = "dummy";
    $status = json_encode($result);
    die();
}

add_action('wp_ajax_validatecomment', 'validatecomment');
add_action('wp_ajax_nopriv_validatecomment', 'validatecomment');
function html2txt($document){
    $search = array('@<script[^>]*?>.*?</script>@si',  // Strip out javascript
                   '@<[\/\!]*?[^<>]*?>@si',            // Strip out HTML tags
                   '@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly
                   '@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments including CDATA
    );
    $text = preg_replace($search, '', $document);
    return $text;
} 
function vinput($ip, $dt) 
{   
    $newip = $ip;
    switch ($dt) {
        case 'i':
            $newip = (int)$ip;
            break;
        case 'b':
            $newip = (boolean)$ip;
            break;
        case 's':
            $ip = strip_tags($ip, '<br/>');
            $ip = html2txt($ip);
            //$ip = htmlentities($ip, ENT_QUOTES);
            $newip = (string)$ip;
            break;
    }
    return $newip;
}

function validatecomment(){
    $appKeyForMyTimes = getAppKeyForMyTimes();        
    $postVars = $_POST;
    //echo '<pre>';print_r($postVars); exit;
    $postVars['fromname'] = vinput($_POST['fromname'], 's');
    $postVars['fromaddress'] = vinput($_POST['fromaddress'], 's');
    $postVars['userid'] = vinput($_POST['userid'], 's');
    $postVars['location'] = vinput($_POST['location'], 's');
    $postVars['imageurl'] = vinput($_POST['imageurl'], 's');
    $postVars['loggedstatus'] = vinput($_POST['loggedstatus'], 'i');
    $postVars['message'] = (urlencode($postVars['message']));
    $postVars['roaltdetails'] = vinput($_POST['roaltdetails'], 's');
    $postVars['ArticleID'] = vinput($_POST['ArticleID'], 'i');
    $postVars['msid'] = vinput($_POST['msid'], 'i');
    $postVars['parentid'] = vinput($_POST['parentid'], 'i');
    $postVars['rootid'] = vinput($_POST['rootid'], 'i');
    
    $logFileName = WP_CONTENT_DIR . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'postcommentlogs' . DIRECTORY_SEPARATOR . $postVars['ArticleID'] . '.txt';
    $logFileData = 'Request Time : ' . date( 'Y-m-d H:i:s', current_time( 'timestamp', 0 ) ) . PHP_EOL;
    $logFileData = $logFileData . 'params : ' . $fields_string . PHP_EOL;
    //open connection
    if($postVars['rootid'] >0 && $postVars['parentid']>0)
    {
        $objectType  = "A";
        $activityType = "Replied";

    }else{
        $objectType  = "B";
        $activityType = "Commented";
    }
    $postXML = '<roaltdetails><fromaddress>'.($_POST['fromaddress']).'</fromaddress><location>'.vinput($_POST['location'], 's').'</location><fromname>'.(vinput($_POST['fromname'], 's')).'</fromname><loggedstatus>1</loggedstatus><ssoid>'.vinput($_POST['userid'], 's').'</ssoid><url>'.vinput($_POST['url'], 's').'</url></roaltdetails>';

    $url = MYTIMES_URL.'mytimes/commentIngestion';
    //$curl_headers = array('Content-Type: application/x-www-form-urlencoded;','Accept-Encoding: gzip; deflate');
    
    $url  = $url."/?".$getString;
    $curlPostData = array('eroaltdetails'=>$postXML,
                          'exCommentTxt'=>$postVars['message'],
                          'objectType'  =>$objectType,
                          'activityType'=>$activityType,
                          'appKey'      =>$appKeyForMyTimes,
                          'uniqueAppID' =>vinput($_POST['ArticleID'], 'i'),
                          'baseEntityType'=>'ARTICLE',
                          'ssoId'         =>($_POST['fromaddress']),
                          'userName'      =>(vinput($_POST['fromname'], 's')),
                          'uuId'          =>vinput($_POST['userid'], 's'),
                          'isCommentMigrate'=>true,
                          'isCommentMailMigrate'=>false    
                          );
    if($postVars['rootid'] >0 && $postVars['parentid']>0)
    {
        $curlPostData['objectId'] = $postVars['parentid'];
    }

    $curlPost = http_build_query($curlPostData);

    error_log("Mytimes comment post Data::: ".$curlPost);
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url); 
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS,$curlPost);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,300);
    //curl_setopt($ch, CURLOPT_HTTPHEADER, $curl_headers);
    $ch_result = curl_exec($ch);
    curl_close($ch);
    $logFileData = $logFileData  .'response : ' . $result . PHP_EOL . PHP_EOL;
    $status = json_decode($ch_result);

    if( $status == null || !isset($status->Error)){
        global $wpdb;
        $wpdb->query('UPDATE wp_posts SET comment_count= comment_count+1 WHERE ID = '.$postVars['ArticleID'].';');
        echo json_encode($status);
    }elseif(isset($status->Error) && isset($status->Error) && ($status->Error=='Message Duplicate')){
        header('Content-type: text/html');
        echo 'duplicatecontent';
    }elseif( isset($status->Error) && isset($status->Error) && ($status->Error=='Comment contains abusive text')){
        header('Content-type: text/html');
        echo 'abusivecontent';
    }else{
        $fp = fopen($logFileName, 'a');
        fwrite($fp, $logFileData);
        fclose($fp);
        echo json_encode($status);
    }
    die();
}

add_action('wp_ajax_ratecomment', 'ratecomment');
add_action('wp_ajax_nopriv_ratecomment', 'ratecomment');

function ratecomment(){
    $params = array();
    $param['callback'] = 'test';
    $param['actorType'] = 'U';
    if($_GET['typeid']=='101')
    {
        $param['activityType'] = 'Disagreed';
    }
    else
    {
        $param['activityType'] = 'Agreed';
    }
    $param['objectId'] = $_GET['opinionid'];
    $param['objectType'] = 'A';
    $param['fromMyTimes'] = true;
    if(isset($_COOKIE['ssoid']))
    {
        $param['uuId']        = $_COOKIE['ssoid'];    
    }
    
    $url = "http://mytpvt.indiatimes.com/mytimes/addActivity";

    foreach( $param as $key => $value )
    {
        $fields_string .= $key.'='.$value.'&';
    }
    $fields_string = rtrim( $fields_string, '&' );
    $url = $url."?".$fields_string; 
    
    //open connection
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($ch);
    curl_close($ch);
    header('Content-type: text/html');
    echo $result;
    die();
}

add_action('wp_ajax_offensive', 'offensive');
add_action('wp_ajax_nopriv_offensive', 'offensive');

function offensive(){
    
    $params = array();
    $param['callback'] = 'test';
    $param['actorType'] = 'U';
    $param['activityType'] = 'Offensive';
    $param['objectId'] = $_GET['ofcommenteroid'];
    $param['offenText']= $_GET['ofreason'];
    $param['objectType'] = 'A';
    $param['isCommentMigrate'] = true;
    $param['fromMyTimes'] = true;
    if(isset($_COOKIE['ssoid']))
    {
        $param['uuId']        = $_COOKIE['ssoid'];    
    }
    
    
    $url = "http://mytpvt.indiatimes.com/mytimes/addActivity";
    foreach( $param as $key => $value )
    {
        $fields_string .= $key.'='.$value.'&';
    }
    $fields_string = rtrim( $fields_string, '&' );
    echo $url = $url."?".$fields_string;    

    //open connection
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($ch);
    curl_close($ch);
    header('Content-type: text/html');
    echo $result;
    die();
}


function getBadges($badges,$uid){
    if(is_array ($badges) ){
        foreach($badges as $badge){
            if ($badge->userId == $uid){
                return $badge;
            }
        }
    }elseif (is_object($badges)) {
        if($badges->userId && $badges->userId==$uid){
            return $badges;
        }
    }
    return null;
}

function getRewards($rewards,$uid){
    if(is_array ($rewards) ){
        foreach($rewards as $reward){
            if ($reward->userid == $uid){
                return $reward;
            }
        }
    }elseif (is_object($rewards)) {
        if($rewards->userid && $rewards->userid==$uid){
            return $rewards;
        }
    }
    return null;
}

add_action('wp_ajax_commentsdata', 'commentsdata');
add_action('wp_ajax_nopriv_commentsdata', 'commentsdata');
function extraDataFromAPI($value,$isreply=false){

    $indi = new stdClass();
    //$commetDetails[''] = '';
    if($isreply==true){
        $indi->level = "2";
        $indi->rootid = $value->O_ID;
        $indi->leveldepth =  "2";
    }else{
        $indi->istoplevel = "1";
        $indi->level = "1";
        $indi->leveldepth =  "1";
        $indi->rootid = $value->_id;
    }
    
    $indi->displaylocation = "1";
    $indi->mailidpresent =  "1";
    $indi->optext =  $value->C_T;
    //echo "<pre>";
   //print_r($value);
    $indi->roaltdetails =  array( 'configid' =>$value->_id,
                                'imageurl'=> $value->user_detail->profile,
                                'rotype'=> "0",
                                'url'=>$value->teu,
                                'fromaddress'=> $value->user_detail->uid,
                                'urs'=>'',
                                'loggedstatus'=> "1",
                                'extid'=> $value->msid,
                                'rchid'=>'',
                                'location'=> $value->user_detail->CITY,
                                'fromname'=> $value->user_detail->FL_N,
                                'ssoid'=> $value->user_detail->uid,
                                'useripaddress'=> "192.168.99.10"
                                );


    $indi->msid =  $value->msid;
    //$indi->rodate =  "05 Jul, 2017 11:35 AM";
    $indi->rodate =  date('d M, Y h:i A', (($value->C_D_Minute*60)+(330*60)));
    $indi->messageid =  $value->_id;
    $indi->parentid =  $value->O_ID;
    $indi->agree = $value->AC_A_C;
    $indi->disagree = $value->AC_D_C;
    $indi->recommended = "0";
    $indi->offensive = $value->AC_O_C;;
    $indi->uid = $value->user_detail->uid;
    
    if($isreply==true){
        $indi->roaltdetails['rootid'] = $value->O_ID;
        $indi->parentusername =  $value->PA_U_D_N_U;
        $indi->parentuid =  $value->PA_A_I;
    }
    

    return $indi;
}

function formatChildForReplyShow(&$childIndi,$value)
{
    if(isset($value->CHILD) && count($value->CHILD)>0)
    {
        foreach ($value->CHILD as $key => $childValue) 
        {
           if(isset($childValue->CHILD) && count($childValue->CHILD)>0)
           {
               $childIndi[] = extraDataFromAPI($childValue,true);
               formatChildForReplyShow($childIndi,$childValue);
           }else{
                $childIndi[] = extraDataFromAPI($childValue,true);
           }
        }
    }
    //var_dump($childIndi);
    //return $childIndi;
}
add_action('wp_ajax_commentsdatamobile', 'commentsdatamobile');
add_action('wp_ajax_nopriv_commentsdatamobile', 'commentsdatamobile');
function commentsdatamobile(){

    $postId = $_GET['msid'];
    $appKeyForMyTimes = getAppKeyForMyTimes($postId);
    $url = MYTIMES_URL.'mytimes/getFeed/Activity/?msid='.$postId.'&curpg=1&commenttype=agree&appkey='.$appKeyForMyTimes.'&sortcriteria=AgreeCount&order=desc&size=1&pagenum=1';
    //$curl_headers = array('Content-Type: application/x-www-form-urlencoded; charset=UTF-8', 'Accept-Encoding: gzip; deflate', 'Accept-Language: en-US');
    $curl_headers = array('Content-Type: application/x-www-form-urlencoded; charset=UTF-8', 'Accept-Language: en-US');
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url); 
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,30);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $curl_headers);
    $ch_result = curl_exec($ch);
    curl_close($ch);
    echo $ch_result;
    die();
}
function commentsdata(){
    global $my_settings;
    $postId = $_GET['msid'];
    $curpg = $_GET['curpg'];
    $appKeyForMyTimes = getAppKeyForMyTimes();
   // $url =  COMMENTS_URL.'showcomments.cms?extid='.$postId.'&perpage=15&feedtype=json&curpg='.$curpg;
    $ordertype = 'desc';
    if(isset($_GET['ordertype']) && !empty($_GET['ordertype'])){
        $ordertype=$_GET['ordertype'];
    }

    $commenttype  = 'CreationDate';
    if(isset($_GET['commenttype']) && !empty($_GET['commenttype'])){
        $commenttype =$_GET['commenttype'];
    }
    

    switch($commenttype){
        
        case "disagree":
        $commenttype= 'disagree';
        $sortcriteria= 'DisagreeCount';
        $order = 'desc';
        break;

        case 'discussed':
        $commenttype = 'mostdiscussed'; 
        $sortcriteria = 'discussed';
        $order = 'desc';
        break;

        case 'agree':
        $commenttype='agree';
        $sortcriteria='AgreeCount';
        $order='desc';
        break;

        default:
        $sortcriteria = 'CreationDate';
        $order       = ($ordertype=="asc")?"desc":"asc";
        break;
    }

    $queryStringData = 'msid='.$postId.'&curpg='.$curpg.'&appkey='.$appKeyForMyTimes.'&commenttype='.$commenttype.'&sortcriteria='.$sortcriteria.'&order='.$order.'&size=15&pagenum='.$curpg.'&withReward=true';
    //MYTIMES_URL/mytimes/getFeed/Activity?msid=".$postId."&curpg=1&appkey=FEMINA&sortcriteria=CreationDate&order=asc&size=10&pagenum=1&withReward=true
    $url = MYTIMES_URL.'mytimes/getFeed/Activity';
    //$curl_headers = array('Content-Type: application/x-www-form-urlencoded; charset=UTF-8', 'Accept-Encoding: gzip; deflate', 'Accept-Language: en-US');
    $curl_headers = array('Content-Type: application/x-www-form-urlencoded; charset=UTF-8', 'Accept-Language: en-US');
    $url  = $url."/?".$queryStringData;
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url); 
    //curl_setopt($ch, CURLOPT_POST, 1);
    //curl_setopt($ch, CURLOPT_POSTFIELDS,array());
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,30);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $curl_headers);
    $ch_result = curl_exec($ch);
    curl_close($ch);
    $rawComments = json_decode($ch_result);
    //echo "<pre>";
    //var_dump($rawComments);
    $comments = new stdClass();
    $comments->totalcount = $rawComments[0]->totalcount;
    $comments->ActionParams = array();
    $comments->rothrd= new stdClass();
    $comments->rothrd->op = array();
    $comments->rothrd->potime = "96";
    $comments->rothrd->opctr = $rawComments[0]->totalcount;
    $comments->rothrd->opctrtopcnt = $rawComments[0]->cmt_c;
    $comments->rothrd->recommendcount = $rawComments[0]->cmt_c;
    $comments->rothrd->agreecount = $rawComments[0]->cmt_c;
    $comments->rothrd->disagreecount = $rawComments[0]->cmt_c;
    $comments->rothrd->loggedopctr = "0";
    unset($rawComments[0]);
    foreach ($rawComments as $key => $value) 
    {
        
        $indi = extraDataFromAPI($value);
        array_push($comments->rothrd->op,$indi);

        $childIndi = array();
        formatChildForReplyShow($childIndi,$value);
        

        foreach ($childIndi as $key => $childValue) 
        {
            array_push($comments->rothrd->op,$childValue);
        }
        //echo "<pre>";
        //print_r($comments->rothrd->op);

    }
    //echo "<pre>";
    //print_r($comments);
    //echo json_encode($comments);

    if(empty($comments->totalcount)){
        if(is_object($ch)){
            curl_close($ch);
        }
    $response = array();
    $response['new_cmtofart2_nit_v1'] = new stdClass();
    $response['new_cmtofart2_nit_v1']->mytuserdata = new stdClass();
    $response['new_cmtofart2_nit_v1']->ActionParams =  $comments->ActionParams;
    $response['new_cmtofart2_nit_v1']->rothrd =  $comments->rothrd;
    $response['new_cmtofart2_nit_v1']->tdate =  new stdClass();
    $response['new_cmtofart2_nit_v1']->tdate->potime =  0;
    //date_default_timezone_set('UTC');
    $response['new_cmtofart2_nit_v1']->tdate->date = date('D M d Y H:i:s', mktime(date("H")+5, date("i")+30, date("s"), date("m")  , date("d"), date("Y")));    
    //print_r($response);
    echo json_encode($response,JSON_HEX_TAG);
        die();
    }
    //var_dump($comments);
    if($comments->totalcount >0){
        //unset($rawComments[0]);
        //echo "<pre>";
        //print_r($comments);

        $userIds = '';
        /*if($comments->rothrd->opctr=='1'){
            $singleComment = $comments->rothrd->op;
            $comments->rothrd->op = array();
            array_push($comments->rothrd->op,$singleComment);
        }*/
        foreach ($comments->rothrd->op as $value){
            $value->optext = html_entity_decode(stripslashes(strip_tags($value->optext)), ENT_QUOTES);
            $userIds .= urlencode($value->roaltdetails['ssoid']).',';
        }
        $userIds = rtrim($userIds,',');
        //$userIds = urlencode("13pmqrqgfa4mvep66cjwcbh5i");
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,300);
       // curl_setopt($ch, CURLOPT_HTTPHEADER, $curl_headers);
        
        $userUrl = $my_settings['myTimesUrl'].'/mytimes/getUsersInfo?ssoids='.$userIds;
        //$userUrl = 'http://mytest.indiatimes.com/mytimes/getUsersInfo?ssoids='.$userIds;
        curl_setopt($ch, CURLOPT_URL, $userUrl); #set the url and get string together    
        $users = json_decode(curl_exec($ch));
        //print_r($users);
        //die;
        $badgesUrl = 'http://rewards.indiatimes.com/bp/api/urs/mubhtry?format=json&pcode=TOI&uid='.$userIds;       
        curl_setopt($ch, CURLOPT_URL, $badgesUrl); #set the url and get string together    
        $badges = json_decode(curl_exec($ch));
        $badges = $badges->output;
        //print_r($badges);
        $rewardsUrl = 'http://rewards.indiatimes.com/bp/api/urs/ups?format=json&pcode=TOI&uid='.$userIds;
        curl_setopt($ch, CURLOPT_URL, $rewardsUrl); #set the url and get string together    
        $rewards = json_decode(curl_exec($ch));
        //print_r($rewards);
        $rewards = $rewards->output->user;
        foreach ($users as $user){
            $userReward = getRewards($rewards,$user->uid);
            $user->reward = new stdClass();
            $user->reward->user = $userReward;
            //$userBadges = new stdClass();
            $userBadges = getBadges($badges->userbadgehistory[0]->userbadges,$user->uid);            
            $user->rewardpoint = new stdClass(); 
            //$user->rewardpoint
            $user->rewardpoint->userbadges = $userBadges;
            $user->sso =$user->uid;
            
            
        }
    }
    curl_close($ch);   
    $response = array();
    $response['new_cmtofart2_nit_v1'] = new stdClass();
    $response['new_cmtofart2_nit_v1']->mytuserdata = new stdClass();
    $response['new_cmtofart2_nit_v1']->mytuserdata->array = $users;
    $response['new_cmtofart2_nit_v1']->ActionParams =  $comments->ActionParams;
    $response['new_cmtofart2_nit_v1']->rothrd =  $comments->rothrd;
    $response['new_cmtofart2_nit_v1']->tdate =  new stdClass();
    $response['new_cmtofart2_nit_v1']->tdate->potime =  0;
    //date_default_timezone_set('UTC');
    $response['new_cmtofart2_nit_v1']->tdate->date = date('D M d Y H:i:s', mktime(date("H")+5, date("i")+30, date("s"), date("m")  , date("d"), date("Y")));    
    //print_r($response);
    echo json_encode($response,JSON_HEX_TAG);
    die();
}

//add_filter('screen_options_show_screen', '__return_false');    
add_action( 'admin_head-post-new.php','hide_ping_track');
add_action( 'admin_head-post.php', 'hide_ping_track' );
function hide_ping_track() {
    if( get_post_type() === "post" ){
            // only for non-admins
            echo "<style>.meta-options label[for=ping_status], #ping_status{display:none !important;} </style>";
    }
}

add_filter( 'user_has_cap', 'user_has_cap', 10, 3 );
function user_has_cap( $all_caps, $cap, $args ){
if ( isset( $all_caps['contributor'] ) && $all_caps['contributor'] )
        {
            $all_caps['upload_files'] = TRUE;
            $all_caps['edit_published_posts'] = TRUE;
        }
    return $all_caps;
}

//Adding the Open Graph in the Language Attributes
function add_opengraph_doctype( $output ) {
    global $my_settings;
    return ' lang="'.$my_settings['site_lang'].'" xmlns:og="http://opengraphprotocol.org/schema/" xmlns:fb="http://www.facebook.com/2008/fbml"';
}
add_filter('language_attributes', 'add_opengraph_doctype');

function catch_first_image($content) {
	global $my_settings;
	$first_img = '';
	ob_start();
	ob_end_clean();
	$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $content, $matches);
	$first_img = $matches[1][0];

	if(empty($first_img)) {
		$first_img = FB_PLACEHOLDER;
	} else if(substr_count($first_img, '/backup/') >= 1) {
		$first_img = FB_PLACEHOLDER;
	} else {
		$first_img = preg_replace( '/-\d+x\d+(?=\.(jpg|jpeg|png|gif)$)/i', '', $first_img );
		global $wpdb;
		$attachment = $wpdb->get_col($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE guid='%s';", $first_img ));
		$first_img_arr = wp_get_attachment_image_src($attachment[0], 'full');
		if(isset($first_img_arr[0]) && !empty($first_img_arr[0]) && $first_img_arr[1] > 200 && $first_img_arr[2] > 200) {
			$first_img = $first_img_arr[0];
			$first_img = str_replace( $my_settings['blogauthor_link'],'../../',$first_img);
            $first_img = str_replace( $my_settings['old_blogauthor_link'],'../../',$first_img);
			$first_img = str_replace('../..', WP_HOME, $first_img);
		} else {
			$first_img = FB_PLACEHOLDER;
		}
	}
	return $first_img;
}

//Lets add Open Graph Meta Info
function insert_fb_in_head() {
    global $my_settings, $page, $paged, $post;
    $meta_part = '';
    if(!empty($my_settings['google_site_verification'])) {
		$meta_part = $meta_part . '<meta name="google-site-verification" content="'.$my_settings['google_site_verification'].'" />';
	}
    if ( get_previous_posts_link() ) {
        $meta_part = $meta_part . '<link rel="prev" href="'. get_pagenum_link( $paged - 1 ).'" />';
    }

    if ( get_next_posts_link() ) {
        $meta_part = $meta_part . '<link rel="next" href="'.get_pagenum_link( $paged +1 ).'" />';
    }

    if ( !is_singular()){ //if it is not a post or a page
        //$meta_part = $meta_part . $my_settings['visual_revenue_reader_response_tracking_script_for_not_singular']; 
        //return;
    }
    
    //$meta_part = $meta_part . $my_settings['visual_revenue_reader_response_tracking_script'];
    
    remove_filter( 'the_title', '_show_local_title' );
	remove_filter( 'the_terms', '_show_local_terms_name' );
	remove_filter( 'get_the_terms', '_show_local_terms_name' );
	remove_filter( 'get_terms', '_show_local_terms_name' );
	remove_filter( 'get_term', '_show_local_term_name' );
    
    $meta_part = $meta_part . '<meta property="og:locale" content="'.$my_settings['og_locale'].'" />';
    $meta_part = $meta_part . '<link href="' . $my_settings['google_url'] . '" rel="Publisher" title="' . $my_settings['main_site_txt'] . '">';
    $meta_part = $meta_part . '<meta property="og:site_name" content="' . $my_settings['main_site_txt'] . '"/>';
    $current_url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
    if ( is_singular()){
		setup_postdata( $post );
		$authorName = get_the_author_meta('display_name');
		$canUrl = get_post_meta(get_the_ID(),'canurl', true);
		if(empty($canUrl)){
			$canUrl = get_permalink();
		}
		$og_title = get_the_title() . ' - ' . $my_settings['main_site_txt'];
		$og_desc = get_the_excerpt();
		$og_url = get_permalink();
		$og_image = catch_first_image(get_the_content());
		$og_type = 'article';
		
		$meta_part = $meta_part . '<meta property="article:published_time" content="'.esc_attr( get_the_date() ).'">';
		$meta_part = $meta_part . '<meta property="vr:type" content="text">';
		$category = get_the_category();
		$meta_part = $meta_part . '<meta property="vr:category" content="'.$category[0]->cat_name.'">';
		$meta_part = $meta_part . '<meta property="vr:author" content="'.$authorName.'">';
	} elseif(is_home() || is_front_page()) {
		//$canUrl = WP_HOME;
		$canUrl = $current_url;
		$og_title = 'News Expert Blog, '.ucfirst($my_settings['quill_lang']).' Blog, Top Indian Bloggers - ' . $my_settings['main_site_txt'];
		$og_desc = get_bloginfo('description');
		$og_url = $current_url;
		$og_image = FB_PLACEHOLDER;
		$og_type = 'website';
	} elseif( is_tag() ){
		$term_id = get_query_var('tag_id');
		$taxonomy = 'post_tag';
		$args ='include=' . $term_id;
		$terms = get_terms( $taxonomy, $args );
		$tag_name = $terms[0]->name;
		//$tag_link = get_tag_link($terms[0]->term_taxonomy_id);
		$tag_link = $current_url;
		$canUrl = $tag_link;
		$og_title = $tag_name . ' Tag Blog Post - ' . $my_settings['main_site_txt'];
		$og_desc = 'Get all the blog post with the '.$tag_name.' tag from our '.$my_settings['main_site_txt'].'.';
		$og_url = $tag_link;
		$og_image = FB_PLACEHOLDER;
		$og_type = '';
	} elseif( is_category() ){
		$the_cat = get_the_category();
		$category_name = $the_cat[0]->name;
		//$category_link = get_category_link( $the_cat[0]->cat_ID );
		$category_link = $current_url;
		$canUrl = $category_link;
		$og_title = $category_name . ' Blog in '.ucfirst($my_settings['quill_lang']).', Top '.$category_name.' Blog - ' . $my_settings['main_site_txt'];
		$og_desc = $my_settings['main_site_txt']. ' provides you one of the top '.$category_name.' blog in '.$my_settings['quill_lang'].' with very interesting blog post by the expert bloggers.';
		$og_url = $category_link;
		$og_image = FB_PLACEHOLDER;
		$og_type = '';
	} elseif( is_tax( 'blog' ) ){
		$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
		if($term->slug == 'authors'){
			//$blog_link = get_term_link($term);
			$blog_link = $current_url;
			$canUrl = $blog_link;
			$og_title = ucfirst($my_settings['quill_lang']) . ' Blog New Authors, Blog Authors Profile List'.' - ' . $my_settings['main_site_txt'];
			$og_desc = 'Find out the blog new authors list, top blog authors in '.ucfirst($my_settings['quill_lang']).' who have expressed their views, opinions and stories on politics, social issues, sports and other topics at '.$my_settings['main_site_txt'].'.';
			$og_url = $blog_link;
			$og_image = FB_PLACEHOLDER;
			$og_type = '';
		} else {
			$blog_name = $term->name;
			//$blog_link = get_term_link($term);
			$blog_link = $current_url;
			$canUrl = $blog_link;
			$og_title = $blog_name . ' Post by best bloggers in '.ucfirst($my_settings['quill_lang']).' - ' . $my_settings['main_site_txt'];
			$og_desc = 'Read the bloggers opinion and stories on '.$blog_name.' by best bloggers and news experts in '.ucfirst($my_settings['quill_lang']).'.';
			$og_url = $blog_link;
			$og_image = FB_PLACEHOLDER;
			$og_type = '';
		}
	} elseif( is_author() ){
		//$author_url = get_author_posts_url(get_the_author_meta( 'ID' ));
		$author_url = $current_url;
		$author_name = get_the_author_meta( 'first_name' ) . ' ' . get_the_author_meta( 'last_name' );
		$canUrl = $author_url;
		$og_title = $author_name . ' Blog Post - ' . $my_settings['main_site_txt'];
		$og_desc = get_the_author_meta( 'description' );
		$og_url = $author_url;
		$og_image = FB_PLACEHOLDER;
		$og_type = '';
	} elseif( is_search() ){
		$canUrl = $current_url;
		$og_title = $my_settings['main_site_txt'] . ' Post Search';
		$og_desc = 'Search result of '.get_search_query().' in '.$my_settings['main_site_txt'].'.';
		$og_url = $current_url;
		$og_image = FB_PLACEHOLDER;
		$og_type = '';
	}
	
	if ( $paged >= 2 || $page >= 2 ){
		$og_title = sprintf( 'Page %s : %s', max( $paged, $page ),  $og_title);
		$og_desc = sprintf( 'Page %s : %s', max( $paged, $page ),  $og_desc);
	}
	
	if(!defined('WP_ADMIN')) {
		add_filter( 'the_title', '_show_local_title', 10, 2 );
		add_filter( 'the_terms', '_show_local_terms_name', 10, 2 );
		add_filter( 'get_the_terms', '_show_local_terms_name', 10, 2 );
		add_filter( 'get_terms', '_show_local_terms_name', 10, 2 );
		add_filter( 'get_term', '_show_local_term_name', 10, 2 );
	}
	
	if(!empty($canUrl)){
		$meta_part = $meta_part . '<link rel=canonical href="'.$canUrl.'" />';
	}
	$meta_part = $meta_part . '<meta name="description" content="' .$og_desc. '"/>';
	$meta_part = $meta_part . '<meta property="og:url" content="' . $og_url . '"/>';
	if ( is_singular()){
		$meta_part = $meta_part . '<meta property="og:title" content="' . get_the_title() . '"/>';
	} else {
		$meta_part = $meta_part . '<meta property="og:title" content="' . $og_title . '"/>';
	}
	$meta_part = $meta_part . '<meta property="og:description" content="' . $og_desc .'"/>';
	$meta_part = $meta_part . '<meta property="og:image" content="' . $og_image . '"/>';
	if(!empty($og_type)){
		$meta_part = $meta_part . '<meta property="og:type" content="' . $og_type . '"/>';
	}
	$meta_part = $meta_part . '<meta name="twitter:card" content="summary" />';
	if ( is_singular()){
		$meta_part = $meta_part . '<meta name="twitter:title" content="' . get_the_title() . '" />';
	} else {
		$meta_part = $meta_part . '<meta name="twitter:title" content="' . $og_title . '" />';
	}
	$meta_part = $meta_part . '<meta property="twitter:description" content="' . $og_desc .'">';
	$meta_part = $meta_part . '<meta name="twitter:url" content="' . $og_url . '" >';
	$meta_part = $meta_part . '<meta name="twitter:domain" content="' . WP_HOME . '">';
	
	echo '<title>' . $og_title . '</title>' . $meta_part;
	echo '<script type="text/javascript" src="'.get_template_directory_uri() . '/js/quill-compiled.js?ver=1.2"></script>';
}
add_action( 'wp_head', 'insert_fb_in_head', 5 );

add_action('wp_ajax_getIbeatData', 'getIbeatData');
add_action('wp_ajax_nopriv_getIbeatData', 'getIbeatData');

function getIbeatData()
{
    global $my_settings;
    //echo 'here';
    //$url = $_GET['url'];
//    $url = str_replace('IBEAT_HOST', $my_settings['ibeat_host'], $url);
//    $url = str_replace('IBEAT_KEY', $my_settings['ibeat_key'], $url);
//    $url = str_replace('IBEAT_CONTENT_TYPE', '21', $url);
    
    $oldDate   = date("Y-m-d");
    $startTs = strtotime($oldDate)*1000;    
    $pubEndTs = strtotime($oldDate.'-7 days')*1000;
    
   // die;
    $url = 'https://ibeatserv.indiatimes.com/ibeatread/most-viewed';
    $domain = $my_settings['ibeat_host'];
    $querySrting = '{"domain":"'.$domain.'","startTs":"'.$startTs.'","pubStartTs":"'.$startTs.'","pubEndTs":"'.$pubEndTs.'"}';
    $url = $url.'?q='.urlencode_deep($querySrting);
    //echo $url;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_URL, $url ); #set the url and get string together    
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Set curl to return the data instead of printing it to the browser.
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,10); # timeout after 10 seconds, you can increase it
    
    $result = curl_exec($ch);
    $res = json_decode ($result);
    curl_close($ch);
    
    $html = '';
    $i = 1;
   
    foreach ($res as $key => $rec){
        foreach ($rec as $k => $value){
           
            if(isset($value->TITLE) && isset($value->URL) && $value->CONTENT_TYPE >='2' && $value->CONTENT_TYPE!='20'){
               // print_r($value);
                if($i>=6){
                    break;
                }
                 //print_r($value);
                //$postTitle = substr($value->TITLE, 0, 30).'...';
                $postTitle = substr($value->TITLE, 0, strpos($value->TITLE, ' ', 40)).'...';

                $blogurl = $value->URL;
                $html .= '<li data-vr-contentbox="">';
                $html .= '<span class="pull-left number">'.$i.'.</span>';
                
                $html .= '<h4><a pg="MR_Pos#'.$i.'" target="_blank" href="' . $blogurl . '" rel="bookmark" title="'.$my_settings['go_to_txt'].' '.$postTitle.'">'.$postTitle.' </a></h4>';    
                $html .= '</li>'  ;
                $i++;
            }
        }
    }
    //echo $html;
    //wp_reset_query(); 
   // header("Cache-Control: max-age=900"); //30days (60sec * 60min * 24hours * 30days)
    echo $html;
    die();
}

add_action('wp_ajax_getPlusones', 'getPlusones');
add_action('wp_ajax_nopriv_getPlusones', 'getPlusones');

function getPlusones1()  {
$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, "https://clients6.google.com/rpc");
curl_setopt($curl, CURLOPT_POST, true);
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($curl, CURLOPT_POSTFIELDS, '[{"method":"pos.plusones.get","id":"p","params":{"nolog":true,"id":"'.rawurldecode($_GET['url']).'","source":"widget","userId":"@viewer","groupId":"@self"},"jsonrpc":"2.0","key":"p","apiVersion":"v1"}]');
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
$curl_results = curl_exec ($curl);
curl_close ($curl);
$json = json_decode($curl_results, true);
$res =  isset($json[0]['result']['metadata']['globalCounts']['count'])?intval( $json[0]['result']['metadata']['globalCounts']['count'] ):0;
$response = json_encode($res);
echo $response;
die();
}

function getPlusones()  {
/* get source for custom +1 button */
    $contents = file_get_contents( 'https://plusone.google.com/_/+1/fastbutton?url=' .  rawurldecode($_GET['url']) );
    //echo $contents;
    /* pull out count variable with regex */
    preg_match( '/window\.__SSR = {c: ([\d]+)/', $contents, $matches );
 
    /* if matched, return count, else zed */
    if( isset( $matches[0] ) ) 
        echo (int) str_replace( 'window.__SSR = {c: ', '', $matches[0] );
    else
        echo 0;
    die();
}

add_action('wp_ajax_getSearchData', 'getSearchData');
add_action('wp_ajax_nopriv_getSearchData', 'getSearchData');

function getSearchData (){
    global $wpdb;
    $input = $_GET['input'];
    // fetch authors
    $queryStringUsers = "SELECT display_name as name,CONCAT('".site_url()."/author/',user_nicename) as link FROM {$wpdb->prefix}users u WHERE u.display_name LIKE '%" . $input . "%' LIMIT 0, 5";
    $tempTerms  = $wpdb->get_results( $queryStringUsers );
    //print_r($tempTerms);
    //echo $wpdb->last_query;
    $queryStringTaxonomies = 'SELECT term.name as name, CONCAT("'.site_url().'/",term.slug) as link FROM ' . $wpdb->term_taxonomy . ' tax ' .
                    'LEFT JOIN ' . $wpdb->terms . ' term ON term.term_id = tax.term_id WHERE 1 = 1 ' .
                    'AND term.name LIKE "%' . $input . '%" ' .
                    'AND ( tax.taxonomy = "blog") ' .
                    'ORDER BY tax.count DESC ' .
                    'LIMIT 0, 5';
    //echo $wpdb->last_query;
    $tempTerms2  = $wpdb->get_results( $queryStringTaxonomies );
    $result = array_merge($tempTerms,$tempTerms2);
    echo json_encode($result);
    die();
}


add_action('wp_ajax_getAuthors', 'getAuthors');
add_action('wp_ajax_getAuthors', 'getAuthors');

function getAuthors(){
    global $wpdb;
    $input = $_GET['input'];
    // fetch authors
    $queryStringUsers = "SELECT display_name as name,CONCAT('".site_url()."/author/',user_nicename) as link FROM {$wpdb->prefix}users u WHERE u.display_name LIKE '%" . $input . "%' LIMIT 0, 5";
    $tempTerms  = $wpdb->get_results( $queryStringUsers );
    //print_r($tempTerms);
    //echo $wpdb->last_query;
    $queryStringTaxonomies = 'SELECT term.name as name, CONCAT("'.site_url().'/",term.slug) as link FROM ' . $wpdb->term_taxonomy . ' tax ' .
                    'LEFT JOIN ' . $wpdb->terms . ' term ON term.term_id = tax.term_id WHERE 1 = 1 ' .
                    'AND term.name LIKE "%' . $input . '%" ' .
                    'AND ( tax.taxonomy = "blog") ' .
                    'ORDER BY tax.count DESC ' .
                    'LIMIT 0, 5';
    //echo $wpdb->last_query;
    $tempTerms2  = $wpdb->get_results( $queryStringTaxonomies );
    $result = array_merge($tempTerms,$tempTerms2);
    echo json_encode($result);
    die();
}


add_action('do_meta_boxes', 'customauthor_image_box');

function customauthor_image_box() {
    remove_meta_box('postimagediv', 'post', 'side');
    add_meta_box('postimagediv', __('Custom Author Image'), 'post_thumbnail_meta_box', 'post', 'side', 'low');
}

function new_title( $title, $sep ) {
    $title = '';
    if ( is_singular()){ //if it is not a post or a page
        global $post;
        setup_postdata( $post );
        $title.= get_the_title().' '.$sep.' ';
    }
    $title .= get_bloginfo( 'name' );
    /*global $paged, $page;

    if ( is_feed() )
        return $title;

    // Add the site name.
    $title .= get_bloginfo( 'name' );

    // Add the site description for the home/front page.
    $site_description = get_bloginfo( 'description', 'display' );
    if ( $site_description && ( is_home() || is_front_page() ) )
        $title = "$title $sep $site_description";

    // Add a page number if necessary.
    if ( $paged >= 2 || $page >= 2 )
        $title = "$title $sep " . sprintf( __( 'Page %s', 'twentytwelve' ), max( $paged, $page ) );
    */
    return $title;
}
//add_filter( 'wp_title', 'new_title', 10, 2 );

require get_template_directory() . '/apis.php';

function add_theme_menu_item() {
    add_menu_page('Custom Settings', 'Custom Settings', 'custom-setting', 'custom-cons-theme-options', 'wps_theme_func');
    add_options_page( 'Interstitial Settings', 'Interstitial', 'manage_options', 'interstitial-settings', 'interstitial_settings_page' );
    add_options_page( 'Ad Blocker Settings', 'Ad Blocker', 'manage_options', 'ad-blocker-settings', 'adblocker_settings_page' );
   //add_options_page( 'Constant Settings', 'Constants', 'manage_options', 'constant-settings', 'constant_settings_page' );
    
    add_submenu_page( 'custom-cons-theme-options', 'General Settings', 'General Settings', 'manage_options', 'general-settings', 'wps_theme_func_general_settings');
    add_submenu_page( 'custom-cons-theme-options', 'Ads Settings', 'Ads Settings', 'manage_options', 'ads-settings', 'wps_theme_func_ads_settings');
    add_submenu_page( 'custom-cons-theme-options', 'Social Share Settings', 'Social Share Settings', 'manage_options', 'social-share-settings', 'wps_theme_func_socialshare_settings');
}

function get_group_option_name($groupOptionName = 'custom-socialshare-cons-section') {
    global $new_whitelist_options;
    $option_names = $new_whitelist_options[$groupOptionName];
    if($option_names){
        return $option_names;
    }
}

add_action("admin_init", "get_group_option_name",10,1);
//add_action("admin_init", "update_custom_constant_setting",10,2);

function update_custom_constant_setting($groupOptionNameField,$postData,$customConstantSection){
    $customConstantPostData = array();
    $groupOptionNames = get_group_option_name($customConstantSection);
    if ($groupOptionNames) {
        foreach ($groupOptionNames as $goKey => $groupOptionName) {
            if(isset($postData[$groupOptionName]) && !empty($postData[$groupOptionName])){
                $postData[$groupOptionName] =@stripslashes($postData[$groupOptionName]);
                $postData[$groupOptionName] = @str_replace('script','scriptTags', $postData[$groupOptionName]);
                $customConstantPostData[$groupOptionName] = $postData[$groupOptionName];
            }            
        }
        //echo '<pre>';print_r($customConstantPostData);die;
        if($customConstantPostData){
            update_option($groupOptionNameField, $customConstantPostData, false);
        }
        
    }
}

function get_custom_constant_option_setting($customConstantOption){
    global $customOptionData;
    $customOptionData = get_option($customConstantOption); 
    //$customOptionData = array_merge($customOptionData, $customOptionData1);
    
}


function get_custom_option($customOptionField){
    global $customOptionData;
    if($customOptionData){
        $customOptionData[$customOptionField] =@stripslashes($customOptionData[$customOptionField]);
        return @str_replace('scriptTags', 'script', $customOptionData[$customOptionField]);
    }else{
        return get_option($customOptionField);
    }
    
}

function wps_theme_func_general_settings(){
    get_custom_constant_option_setting('custom_general_constant_option_fields');
    if(isset($_POST['submit'])){  
        //die;
       update_custom_constant_setting('custom_general_constant_option_fields',$_POST,'custom-general-cons-section');
       
    }
    ?>
        <div class="wrap">
	    <h1>Custom General Constant </h1>
	    <form method="post" action="">
	        <?php
	            settings_fields("custom-general-cons-section");
	            do_settings_sections("custom-general-cons-options");      
	            submit_button(); 
	        ?>          
	    </form>
	</div>
<?php }
add_action("admin_menu", "add_theme_menu_item");
function wps_theme_func_ads_settings(){
    get_custom_constant_option_setting('custom_ad_constant_option_fields');
    if(isset($_POST['submit'])){  
        //die;
       update_custom_constant_setting('custom_ad_constant_option_fields',$_POST,'custom-ad-cons-section');
    }
    ?>
	<div class="wrap">
	    <h1>Custom Ads Constant</h1>
	    <form method="post" action="">
	        <?php
	            settings_fields("custom-ad-cons-section");
	            do_settings_sections("custom-ad-cons-options");      
	            submit_button(); 
	        ?>          
	    </form>
	</div>
    <?php
}


function wps_theme_func_socialshare_settings(){
    get_custom_constant_option_setting('custom_ss_cons_option');
    if(isset($_POST['submit'])){  
        //die;
       update_custom_constant_setting('custom_ss_cons_option',$_POST,'custom-socialshare-cons-section');
    }
    ?>
	<div class="wrap">
	    <h1>Custom Social Share Constant</h1>
	    <form method="post" action="">
                
	        <?php
	            settings_fields("custom-socialshare-cons-section");
	            do_settings_sections("custom-socialshare-cons-options");      
	            submit_button(); 
	        ?>          
	    </form>
	</div>
    <?php
}

function adblocker_settings_page(){
    ?>
	<div class="wrap">
	    <h1>Ad Blocker Settings</h1>
	    <form method="post" action="options.php">
	        <?php
	            settings_fields("ad-section");
	            do_settings_sections("ad-blocker-options");      
	            submit_button(); 
	        ?>          
	    </form>
	</div>
    <?php
}

function interstitial_settings_page(){
    ?>
	<div class="wrap">
	    <h1>Interstitial Settings</h1>
	    <form method="post" action="options.php">
	        <?php
	            settings_fields("section");
	            do_settings_sections("theme-options");      
	            submit_button(); 
	        ?>          
	    </form>
	</div>
    <?php
}



function display_webadcontent_element(){
    ?><textarea name="web_ad_content" rows="10" cols="50" id="web_ad_content" class="large-text code"><?php echo get_option('web_ad_content'); ?></textarea><?php
}

function display_wapadcontent_element(){
    ?><textarea name="wap_ad_content" rows="10" cols="50" id="wap_ad_content" class="large-text code"><?php echo get_option('wap_ad_content'); ?></textarea><?php
}

function display_showad_element(){
    ?><fieldset>
    <label><input type="radio" name="show_ad" value="WEB" <?php checked('WEB', get_option('show_ad'), true); ?> />WEB</label><br />
    <label><input type="radio" name="show_ad" value="WAP" <?php checked('WAP', get_option('show_ad'), true); ?> />WAP</label><br />
    <label><input type="radio" name="show_ad" value="Both" <?php checked('Both', get_option('show_ad'), true); ?> />Both</label><br />
    <label><input type="radio" name="show_ad" value="None" <?php checked('None', get_option('show_ad'), true); ?> />None</label>
    </fieldset><?php
}

function display_ad_blocker_js_code_element(){
	 ?><textarea name="ad_blocker_js_code" rows="10" cols="50" id="ad_blocker_js_code" class="large-text code"><?php echo get_option('ad_blocker_js_code'); ?></textarea><?php
}

function display_ad_blocker_content_element(){
	?><textarea name="ad_blocker_content" rows="10" cols="50" id="ad_blocker_content" class="large-text code"><?php echo get_option('ad_blocker_content'); ?></textarea><?php
}

function display_theme_panel_fields(){
    add_settings_section("section", "", null, "theme-options");
    add_settings_field("web_ad_content", "WEB Ad Code", "display_webadcontent_element", "theme-options", "section");
    add_settings_field("wap_ad_content", "WAP Ad Code", "display_wapadcontent_element", "theme-options", "section");
    add_settings_field("show_ad", "Where you want show interstitial?", "display_showad_element", "theme-options", "section");

    register_setting("section", "web_ad_content");
    register_setting("section", "wap_ad_content");
    register_setting("section", "show_ad");
    
    add_settings_section("ad-section", "", null, "ad-blocker-options");
    add_settings_field("ad_blocker_js_code", "JS Code", "display_ad_blocker_js_code_element", "ad-blocker-options", "ad-section");
    add_settings_field("ad_blocker_content", "Page Content", "display_ad_blocker_content_element", "ad-blocker-options", "ad-section");

    register_setting("ad-section", "ad_blocker_js_code");
    register_setting("ad-section", "ad_blocker_content");
    
    add_settings_section("custom-general-cons-section", "", null, "custom-general-cons-options");
    
    
    add_settings_field("site_lang", "Site Lang", "display_site_lang_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("og_locale", "OG Locale", "display_og_locale_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("google_site_verification", "Google Site Verification", "display_google_site_verification_element", "custom-general-cons-options", "custom-general-cons-section");
    
    add_settings_field("fblikeurl", "FB Like Url", "display_fblikeurl_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("favicon", "Favicon", "display_favicon_element", "custom-general-cons-options", "custom-general-cons-section");
    
    add_settings_field("channel_url_part", "Channel Url Part", "display_channel_url_part_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("main_site_url", "Main Site Url", "display_main_site_url_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("main_site_txt", "Main Site txt", "display_main_site_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    
    add_settings_field("logo_title", "Logo Title", "display_logo_title_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("logo_url", "Logo Url", "display_logo_url_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("footer_logo_txt", "Footer Logo txt", "display_footer_logo_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    
    add_settings_field("footer_dmp_tag", "Footer Dmp Tag", "display_footer_dmp_tag_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("visual_revenue_reader_response_tracking_script", "visual revenue reader response tracking script", "display_visual_revenue_reader_response_tracking_script_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("visual_revenue_reader_response_tracking_script_for_not_singular", "visual revenue reader response tracking script for not singular", "display_visual_revenue_reader_response_tracking_script_for_not_singular_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("not_found_heading", "Not Found Heading", "display_not_found_heading_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("not_found_txt", "Not Found txt", "display_not_found_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("search_placeholder_txt", "Search Placeholder txt", "display_search_placeholder_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("go_to_the_profile_of_txt", "Go To The Profile of txt", "display_go_to_the_profile_of_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("go_to_txt", "Go To txt", "display_go_to_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("share_link_fb_txt", "Share Link FB txt", "display_share_link_fb_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("share_link_twitter_txt", "Share Link Twitter txt", "display_share_link_twitter_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("share_link_linkedin_txt", "Share Link Linkedin txt", "display_share_link_linkedin_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("mail_link_txt", "Mail Link txt", "display_mail_link_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("read_complete_article_here_txt", "Read Complete Article Here txt", "display_read_complete_article_here_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("featured_txt", "Featured txt", "display_featured_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("disclaimer_txt", "Disclaimer txt", "display_disclaimer_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("disclaimer_on_txt", "Disclaimer ON txt", "display_disclaimer_on_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("disclaimer_off_txt", "Disclaimer OFF txt", "display_disclaimer_off_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("most_discussed_txt", "Most Discussed txt", "display_most_discussed_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("more_txt", "More txt", "display_more_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("less_txt", "Less txt", "display_less_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("login_txt", "Login txt", "display_login_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("logout_txt", "Logout txt", "display_logout_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("view_all_posts_in_txt", "View All Posts IN txt", "display_view_all_posts_in_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("home_txt", "Home txt", "display_home_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("blogs_txt", "Blogs txt", "display_blogs_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("search_results_for_txt", "Search Results For txt", "display_search_results_for_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("most_read_txt", "Most Read txt", "display_most_read_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("popular_tags_txt", "Popular Tags txt", "display_popular_tags_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("recently_joined_authors_txt", "Recently Joined Authors txt", "display_recently_joined_authors_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("like_us_txt", "Like US txt", "display_like_us_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("author_txt", "Author txt", "display_author_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("popular_from_author_txt", "Popular From Author txt", "display_popular_from_author_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("search_authors_by_name_txt", "Search Authors BY Name txt", "display_search_authors_by_name_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("search_txt", "Search txt", "display_search_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("back_to_authors_page_txt", "Back To Authors Page txt", "display_back_to_authors_page_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("no_authors_found_txt", "NO Authors Found txt", "display_no_authors_found_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("further_commenting_is_disabled_txt", "Further Commenting is Disabled txt", "display_further_commenting_is_disabled_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("comments_on_this_post_are_closed_now_txt", "Comments on this post are closed now txt", "display_comments_on_this_post_are_closed_now_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("add_your_comment_here_txt", "Add your comment here txt", "display_add_your_comment_here_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("characters_remaining_txt", "Characters Remaining txt", "display_characters_remaining_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("share_on_fb_txt", "Share on FB txt", "display_share_on_fb_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("share_on_twitter_txt", "Share on Twitter txt", "display_share_on_twitter_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("sort_by_txt", "Sort By txt", "display_sort_by_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("newest_txt", "Newest txt", "display_newest_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("oldest_txt", "Oldest txt", "display_oldest_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("discussed_txt", "Discussed txt", "display_discussed_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("up_voted_txt", "Up Voted txt", "display_up_voted_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("down_voted_txt", "Down Voted txt", "display_down_voted_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("be_the_first_one_to_review_txt", "Be The First one to Review txt", "display_be_the_first_one_to_review_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("more_points_needed_to_reach_next_level_txt", "More Points Needed to Reach Next Level txt", "display_more_points_needed_to_reach_next_level_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("know_more_about_times_points_txt", "Know More About Times Points txt", "display_know_more_about_times_points_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("know_more_about_times_points_link", "Know More About Times Points link", "display_know_more_about_times_points_link_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("badges_earned_txt", "Badges Earned txt", "display_badges_earned_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("just_now_txt", "Just Now txt", "display_just_now_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("follow_txt", "Follow txt", "display_follow_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("reply_txt", "Reply txt", "display_reply_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("flag_txt", "Flag txt", "display_flag_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("up_vote_txt", "Up Vote txt", "display_up_vote_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("down_vote_txt", "Down Vote txt", "display_down_vote_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("mark_as_offensive_txt", "Mark as Offensive txt", "display_mark_as_offensive_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("find_this_comment_offensive_txt", "Find This Comment Offensive txt", "display_find_this_comment_offensive_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("reason_submitted_to_admin_txt", "Reason Submitted to Admin txt", "display_reason_submitted_to_admin_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("choose_reason_txt", "Choose Reason txt", "display_choose_reason_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("reason_for_reporting_txt", "Reason for Reporting txt", "display_reason_for_reporting_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("foul_language_txt", "Foul Language txt", "display_foul_language_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("defamatory_txt", "Defamatory txt", "display_defamatory_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("inciting_hatred_against_certain_community_txt", "Inciting Hatred Against Certain Community txt", "display_inciting_hatred_against_certain_community_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("out_of_context_spam_txt", "out_of_context_spam_txt txt", "display_out_of_context_spam_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("others_txt", "Others txt", "display_others_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("report_this_txt", "Report This txt", "display_report_this_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("close_txt", "Close txt", "display_close_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("already_marked_as_offensive", "already marked as offensive", "display_already_marked_as_offensive_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("flagged_txt", "Flagged txt", "display_flagged_txt_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("blogauthor_link", "Blogauthor Link", "display_blogauthor_link_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("old_blogauthor_link", "Old Blogauthor Link", "display_old_blogauthor_link_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("blog_link", "Blog Link", "display_blog_link_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("common_cookie_domain", "Common Cookie Domain", "display_common_cookie_domain_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("quill_lang", "Quill Lang", "display_quill_lang_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("quill_link_1", "Quill Lang 1", "display_quill_link_1_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("quill_link_2", "Quill Lang 2", "display_quill_link_2_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("quill_link_3", "Quill Lang 3", "display_quill_link_3_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("quill_link_4", "Quill Lang 4", "display_quill_link_4_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("offensive_comment_warning", "Offensive Comment Warning", "display_offensive_comment_warning_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("footer", "footer", "display_footer_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("footer_css", "Footer css", "display_footer_css_element", "custom-general-cons-options", "custom-general-cons-section");
    
    add_settings_field("channel_name", "channel_name", "display_channel_name_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("site_id", "site_id", "display_site_id_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("domain_name", "domain_name", "display_domain_name_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("ofcommenthostid", "ofcommenthostid", "display_ofcommenthostid_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("ofcommentchannelid", "ofcommentchannelid", "display_ofcommentchannelid_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("appid", "appid", "display_appid_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("comment_text", "comment text", "display_comment_text_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("Z_IMAGE_PLACEHOLDER", "Z_IMAGE_PLACEHOLDER", "display_Z_IMAGE_PLACEHOLDER_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("FB_PLACEHOLDER", "FB_PLACEHOLDER", "display_FB_PLACEHOLDER_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("Z_AUTHOR_PLACEHOLDER", "Z_AUTHOR_PLACEHOLDER", "display_Z_AUTHOR_PLACEHOLDER_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("RECO_URL", "RECO_URL", "display_RECO_URL_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("COMMENTS_URL", "COMMENTS_URL", "display_COMMENTS_URL_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("RCHID", "RCHID", "display_RCHID_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("MYTIMES_URL", "MYTIMES_URL", "display_MYTIMES_URL_element", "custom-general-cons-options", "custom-general-cons-section");
    add_settings_field("MYT_COMMENTS_API_KEY", "MYT_COMMENTS_API_KEY", "display_MYT_COMMENTS_API_KEY_element", "custom-general-cons-options", "custom-general-cons-section");



    

    
    register_setting("custom-general-cons-section", "site_lang");
    register_setting("custom-general-cons-section", "og_locale");
    register_setting("custom-general-cons-section", "google_site_verification");   
    register_setting("custom-general-cons-section", "fblikeurl");
    register_setting("custom-general-cons-section", "favicon");
    
    
    register_setting("custom-general-cons-section", "channel_url_part");
    register_setting("custom-general-cons-section", "main_site_url");
    register_setting("custom-general-cons-section", "main_site_txt");
    
    register_setting("custom-general-cons-section", "logo_title");
    register_setting("custom-general-cons-section", "logo_url");
    register_setting("custom-general-cons-section", "footer_logo_txt");
       
    
    register_setting("custom-general-cons-section", "footer_dmp_tag");
    
    register_setting("custom-general-cons-section", "visual_revenue_reader_response_tracking_script");
    register_setting("custom-general-cons-section", "visual_revenue_reader_response_tracking_script_for_not_singular");
    register_setting("custom-general-cons-section", "not_found_heading");
    register_setting("custom-general-cons-section", "not_found_txt");
    register_setting("custom-general-cons-section", "search_placeholder_txt");
    register_setting("custom-general-cons-section", "go_to_the_profile_of_txt");
    register_setting("custom-general-cons-section", "go_to_txt");
    register_setting("custom-general-cons-section", "share_link_fb_txt");
    register_setting("custom-general-cons-section", "share_link_twitter_txt");
    register_setting("custom-general-cons-section", "share_link_linkedin_txt");
    register_setting("custom-general-cons-section", "mail_link_txt");
    register_setting("custom-general-cons-section", "read_complete_article_here_txt");
    register_setting("custom-general-cons-section", "featured_txt");
    register_setting("custom-general-cons-section", "disclaimer_txt");
    register_setting("custom-general-cons-section", "disclaimer_on_txt");
    register_setting("custom-general-cons-section", "disclaimer_of_txt");
    register_setting("custom-general-cons-section", "most_discussed_txt");
    register_setting("custom-general-cons-section", "more_txt");
    register_setting("custom-general-cons-section", "less_txt");
    register_setting("custom-general-cons-section", "login_txt");
    register_setting("custom-general-cons-section", "logout_txt");
    register_setting("custom-general-cons-section", "view_all_posts_in_txt");
    register_setting("custom-general-cons-section", "home_txt");
    register_setting("custom-general-cons-section", "blogs_txt");
    register_setting("custom-general-cons-section", "search_results_for_txt");
    register_setting("custom-general-cons-section", "most_read_txt");
    register_setting("custom-general-cons-section", "popular_tags_txt");
    register_setting("custom-general-cons-section", "recently_joined_authors_txt");
    register_setting("custom-general-cons-section", "like_us_txt");
    register_setting("custom-general-cons-section", "author_txt");
    register_setting("custom-general-cons-section", "popular_from_author_txt");
    register_setting("custom-general-cons-section", "search_authors_by_name_txt");
    register_setting("custom-general-cons-section", "search_txt");
    register_setting("custom-general-cons-section", "back_to_authors_page_txt");
    register_setting("custom-general-cons-section", "no_authors_found_txt");
    register_setting("custom-general-cons-section", "further_commenting_is_disabled_txt");
    register_setting("custom-general-cons-section", "comments_on_this_post_are_closed_now_txt");
    register_setting("custom-general-cons-section", "add_your_comment_here_txt");
    register_setting("custom-general-cons-section", "characters_remaining_txt");
    register_setting("custom-general-cons-section", "share_on_fb_txt");
    register_setting("custom-general-cons-section", "share_on_twitter_txt");
    register_setting("custom-general-cons-section", "sort_by_txt");
    register_setting("custom-general-cons-section", "newest_txt");
    register_setting("custom-general-cons-section", "oldest_txt");
    register_setting("custom-general-cons-section", "discussed_txt");
    register_setting("custom-general-cons-section", "up_voted_txt");
    register_setting("custom-general-cons-section", "down_voted_txt");
    register_setting("custom-general-cons-section", "be_the_first_one_to_review_txt");
    register_setting("custom-general-cons-section", "more_points_needed_to_reach_next_level_txt");
    register_setting("custom-general-cons-section", "know_more_about_times_points_txt");
    register_setting("custom-general-cons-section", "know_more_about_times_points_link");
    register_setting("custom-general-cons-section", "badges_earned_txt");
    register_setting("custom-general-cons-section", "just_now_txt");
    register_setting("custom-general-cons-section", "follow_txt");
    register_setting("custom-general-cons-section", "reply_txt");
    register_setting("custom-general-cons-section", "flag_txt");
    register_setting("custom-general-cons-section", "up_vote_txt");
    register_setting("custom-general-cons-section", "down_vote_txt");
    register_setting("custom-general-cons-section", "mark_as_offensive_txt");
    register_setting("custom-general-cons-section", "find_this_comment_offensive_txt");
    register_setting("custom-general-cons-section", "reason_submitted_to_admin_txt");
    register_setting("custom-general-cons-section", "choose_reason_txt");
    register_setting("custom-general-cons-section", "reason_for_reporting_txt");
    register_setting("custom-general-cons-section", "foul_language_txt");
    register_setting("custom-general-cons-section", "defamatory_txt");
    register_setting("custom-general-cons-section", "inciting_hatred_against_certain_community_txt");
    register_setting("custom-general-cons-section", "out_of_context_spam_txt");
    register_setting("custom-general-cons-section", "others_txt");
    register_setting("custom-general-cons-section", "report_this_txt");
    register_setting("custom-general-cons-section", "close_txt");
    register_setting("custom-general-cons-section", "already_marked_as_offensive");
    register_setting("custom-general-cons-section", "flagged_txt");
    register_setting("custom-general-cons-section", "blogauthor_link");
    register_setting("custom-general-cons-section", "old_blogauthor_link");
    register_setting("custom-general-cons-section", "blog_link");
    register_setting("custom-general-cons-section", "common_cookie_domain");
    register_setting("custom-general-cons-section", "quill_lang");
    register_setting("custom-general-cons-section", "quill_link_1");
    register_setting("custom-general-cons-section", "quill_link_2");
    register_setting("custom-general-cons-section", "quill_link_3");
    register_setting("custom-general-cons-section", "quill_link_4");
    register_setting("custom-general-cons-section", "offensive_comment_warning");
    register_setting("custom-general-cons-section", "footer");
    register_setting("custom-general-cons-section", "footer_css");
    register_setting("custom-general-cons-section", "footer_css");
    register_setting("custom-general-cons-section", "comment_text");
    register_setting("custom-general-cons-section", "channel_name");
    register_setting("custom-general-cons-section", "site_id");
    register_setting("custom-general-cons-section", "domain_name");
    register_setting("custom-general-cons-section", "ofcommenthostid");
    register_setting("custom-general-cons-section", "ofcommentchannelid");
    register_setting("custom-general-cons-section", "appid");
    register_setting("custom-general-cons-section", "Z_IMAGE_PLACEHOLDER");
    register_setting("custom-general-cons-section", "FB_PLACEHOLDER");
    register_setting("custom-general-cons-section", "Z_AUTHOR_PLACEHOLDER");
    register_setting("custom-general-cons-section", "RECO_URL");
    register_setting("custom-general-cons-section", "COMMENTS_URL");
    register_setting("custom-general-cons-section", "RCHID");
    register_setting("custom-general-cons-section", "MYTIMES_URL");
    register_setting("custom-general-cons-section", "MYT_COMMENTS_API_KEY");
    
    
    
}

add_action("admin_init", "display_theme_panel_fields");
add_action("admin_init", "display_theme_ad_panel_fields");
add_action("admin_init", "display_theme_socialshare_panel_fields");

function display_theme_socialshare_panel_fields(){
    add_settings_section("custom-socialshare-cons-section", "", null, "custom-socialshare-cons-options");
    
    add_settings_field("ga", "GA", "display_ga_element", "custom-socialshare-cons-options", "custom-socialshare-cons-section");
add_settings_field("fbappid", "FB APP ID", "display_fbappid_element", "custom-socialshare-cons-options", "custom-socialshare-cons-section");

add_settings_field("fb_url", "FB URL", "display_fb_url_element", "custom-socialshare-cons-options", "custom-socialshare-cons-section");
add_settings_field("twitter_url", "Twitter Url", "display_twitter_url_element", "custom-socialshare-cons-options", "custom-socialshare-cons-section");
add_settings_field("twitter_handle", "Twitter Handle", "display_twitter_handle_element", "custom-socialshare-cons-options", "custom-socialshare-cons-section");
add_settings_field("google_url", "Google Url", "display_google_url_element", "custom-socialshare-cons-options", "custom-socialshare-cons-section");
add_settings_field("rss_url", "RSS Url", "display_rss_url_element", "custom-socialshare-cons-options", "custom-socialshare-cons-section");
add_settings_field("comscore_tag", "Comscore Tag", "display_comscore_tag_element", "custom-socialshare-cons-options", "custom-socialshare-cons-section");
add_settings_field("ibeat_channel", "ibeat Channel", "display_ibeat_channel_element", "custom-socialshare-cons-options", "custom-socialshare-cons-section");
add_settings_field("ibeat_host", "ibeat Host", "display_ibeat_host_element", "custom-socialshare-cons-options", "custom-socialshare-cons-section");
add_settings_field("ibeat_key", "ibeat Key", "display_ibeat_key_element", "custom-socialshare-cons-options", "custom-socialshare-cons-section");
add_settings_field("ibeat_domain", "ibeat Domain", "display_ibeat_domain_element", "custom-socialshare-cons-options", "custom-socialshare-cons-section");


register_setting("custom-socialshare-cons-section", "ga");
register_setting("custom-socialshare-cons-section", "fbappid");
register_setting("custom-socialshare-cons-section", "fb_url");
register_setting("custom-socialshare-cons-section", "twitter_url");
register_setting("custom-socialshare-cons-section", "twitter_handle");
register_setting("custom-socialshare-cons-section", "google_url");
register_setting("custom-socialshare-cons-section", "rss_url");
register_setting("custom-socialshare-cons-section", "comscore_tag"); 
register_setting("custom-socialshare-cons-section", "ibeat_channel");
register_setting("custom-socialshare-cons-section", "ibeat_host");
register_setting("custom-socialshare-cons-section", "ibeat_key");
register_setting("custom-socialshare-cons-section", "ibeat_domain");
    
}

function display_theme_ad_panel_fields(){
    add_settings_section("custom-ad-cons-section", "", null, "custom-ad-cons-options");
    
    
    add_settings_field("footer_google_tag", "Footer Google Tag", "display_footer_google_tag_element", "custom-ad-cons-options", "custom-ad-cons-section");
    add_settings_field("google_tag_js_head", "Google Tag js head", "display_google_tag_js_head_element", "custom-ad-cons-options", "custom-ad-cons-section");
    add_settings_field("google_ad_top", "Google ad top", "display_google_ad_top_element", "custom-ad-cons-options", "custom-ad-cons-section");
    add_settings_field("google_ad_right_1", "Google ad right 1", "display_google_ad_right_1_element", "custom-ad-cons-options", "custom-ad-cons-section");
    add_settings_field("google_ad_right_2", "Google ad right 2", "display_google_ad_right_2_element", "custom-ad-cons-options", "custom-ad-cons-section");
    add_settings_field("ctn_homepage", "ctn homepage", "display_ctn_homepage_element", "custom-ad-cons-options", "custom-ad-cons-section");
    add_settings_field("ctn_homepage_rhs", "ctn homepage rhs", "display_ctn_homepage_rhs_element", "custom-ad-cons-options", "custom-ad-cons-section");
    add_settings_field("ctn_article_list", "ctn article list", "display_ctn_article_list_element", "custom-ad-cons-options", "custom-ad-cons-section");
    add_settings_field("ctn_article_list_rhs", "ctn article list rhs", "display_ctn_article_list_rhs_element", "custom-ad-cons-options", "custom-ad-cons-section");
    add_settings_field("ctn_article_show_rhs", "ctn article show rhs", "display_ctn_article_show_rhs_element", "custom-ad-cons-options", "custom-ad-cons-section");
    add_settings_field("ctn_article_show_end_article_1", "ctn article show end article_1", "display_ctn_article_show_end_article_1_element", "custom-ad-cons-options", "custom-ad-cons-section");
    add_settings_field("ctn_article_show_end_article_2", "ctn article show end article_2", "display_ctn_article_show_end_article_2_element", "custom-ad-cons-options", "custom-ad-cons-section");
    add_settings_field("esi_status", "esi status", "display_esi_status_element", "custom-ad-cons-options", "custom-ad-cons-section");
    add_settings_field("esi_header", "esi header", "display_esi_header_element", "custom-ad-cons-options", "custom-ad-cons-section");
    add_settings_field("esi_homepage_ids", "esi_homepage_ids", "display_esi_homepage_ids_element", "custom-ad-cons-options", "custom-ad-cons-section");
    add_settings_field("esi_homepage", "esi_homepage", "display_esi_homepage_element", "custom-ad-cons-options", "custom-ad-cons-section");
    add_settings_field("esi_homepage_rhs", "esi_homepage_rhs", "display_esi_homepage_rhs_element", "custom-ad-cons-options", "custom-ad-cons-section");
    add_settings_field("esi_article_list_ids", "esi_article_list_ids", "display_esi_article_list_ids_element", "custom-ad-cons-options", "custom-ad-cons-section");
    add_settings_field("esi_article_list", "esi_article_list", "display_esi_article_list_element", "custom-ad-cons-options", "custom-ad-cons-section");
    add_settings_field("esi_article_list_rhs", "esi_article_list_rhs", "display_esi_article_list_rhs_element", "custom-ad-cons-options", "custom-ad-cons-section");
    add_settings_field("esi_article_show_ids", "esi_article_show_ids", "display_esi_article_show_ids_element", "custom-ad-cons-options", "custom-ad-cons-section");
    add_settings_field("esi_article_show_rhs", "esi_article_show_rhs", "display_esi_article_show_rhs_element", "custom-ad-cons-options", "custom-ad-cons-section");
    add_settings_field("esi_article_show_end_article_1", "esi_article_show_end_article_1", "display_esi_article_show_end_article_1_element", "custom-ad-cons-options", "custom-ad-cons-section");
    add_settings_field("esi_article_show_end_article_2", "esi_article_show_end_article_2", "display_esi_article_show_end_article_2_element", "custom-ad-cons-options", "custom-ad-cons-section");
    add_settings_field("ctn_article_show_mid_article_video", "ctn_article_show_mid_article_video", "display_ctn_article_show_mid_article_video_element", "custom-ad-cons-options", "custom-ad-cons-section");
    
    add_settings_field("esi_header_m", "esi header_m", "display_esi_header_m_element", "custom-ad-cons-options", "custom-ad-cons-section");
    add_settings_field("esi_homepage_ids_m", "esi_homepage_ids_m", "display_esi_homepage_ids_m_element", "custom-ad-cons-options", "custom-ad-cons-section");
    add_settings_field("esi_homepage_m", "esi_homepage_m", "display_esi_homepage_m_element", "custom-ad-cons-options", "custom-ad-cons-section");
    add_settings_field("esi_homepage_rhs_m", "esi_homepage_rhs_m", "display_esi_homepage_rhs_m_element", "custom-ad-cons-options", "custom-ad-cons-section");
    add_settings_field("esi_article_list_ids_m", "esi_article_list_ids_m", "display_esi_article_list_ids_m_element", "custom-ad-cons-options", "custom-ad-cons-section");
    add_settings_field("esi_article_list_m", "esi_article_list_m", "display_esi_article_list_m_element", "custom-ad-cons-options", "custom-ad-cons-section");
    add_settings_field("esi_article_list_rhs_m", "esi_article_list_rhs_m", "display_esi_article_list_rhs_m_element", "custom-ad-cons-options", "custom-ad-cons-section");
    add_settings_field("esi_article_show_ids_m", "esi_article_show_ids_m", "display_esi_article_show_ids_m_element", "custom-ad-cons-options", "custom-ad-cons-section");
    add_settings_field("esi_article_show_rhs_m", "esi_article_show_rhs_m", "display_esi_article_show_rhs_m_element", "custom-ad-cons-options", "custom-ad-cons-section");
    add_settings_field("esi_article_show_end_article_1_m", "esi_article_show_end_article_1_m", "display_esi_article_show_end_article_1_m_element", "custom-ad-cons-options", "custom-ad-cons-section");
    add_settings_field("esi_article_show_end_article_2_m", "esi_article_show_end_article_2_m", "display_esi_article_show_end_article_2_m_element", "custom-ad-cons-options", "custom-ad-cons-section");
    add_settings_field("ctn_article_show_mid_article_video_m", "ctn_article_show_mid_article_video_m", "display_ctn_article_show_mid_article_video_m_element", "custom-ad-cons-options", "custom-ad-cons-section");
    add_settings_field("ATF_300_DIV_ID", "ATF_300_DIV_ID", "display_ATF_300_DIV_ID_element", "custom-ad-cons-options", "custom-ad-cons-section");
    add_settings_field("ATF_728_DIV_ID", "ATF_728_DIV_ID", "display_ATF_728_DIV_ID_element", "custom-ad-cons-options", "custom-ad-cons-section");
    add_settings_field("BTF_300_DIV_ID", "BTF_300_DIV_ID", "display_BTF_300_DIV_ID_element", "custom-ad-cons-options", "custom-ad-cons-section");
    add_settings_field("ATF_300_AD_CODE", "ATF_300_AD_CODE", "display_ATF_300_AD_CODE_element", "custom-ad-cons-options", "custom-ad-cons-section");
    add_settings_field("ATF_728_AD_CODE", "ATF_728_AD_CODE", "display_ATF_728_AD_CODE_element", "custom-ad-cons-options", "custom-ad-cons-section");
    add_settings_field("BTF_300_AD_CODE", "BTF_300_AD_CODE", "display_BTF_300_AD_CODE_element", "custom-ad-cons-options", "custom-ad-cons-section");

   
   
    
    register_setting("custom-ad-cons-section", "footer_google_tag");
    register_setting("custom-ad-cons-section", "google_tag_js_head");
    register_setting("custom-ad-cons-section", "google_ad_top");
    register_setting("custom-ad-cons-section", "google_ad_right_1");
    register_setting("custom-ad-cons-section", "google_ad_right_2");
    register_setting("custom-ad-cons-section", "ctn_homepage");
    register_setting("custom-ad-cons-section", "ctn_homepage_rhs");
    register_setting("custom-ad-cons-section", "ctn_article_list");
    register_setting("custom-ad-cons-section", "ctn_article_list_rhs");
    register_setting("custom-ad-cons-section", "ctn_article_show_rhs");
    register_setting("custom-ad-cons-section", "ctn_article_show_end_article_1");
    register_setting("custom-ad-cons-section", "ctn_article_show_end_article_2");
    register_setting("custom-ad-cons-section", "esi_status");
    register_setting("custom-ad-cons-section", "esi_header");
    register_setting("custom-ad-cons-section", "esi_homepage_ids");
    register_setting("custom-ad-cons-section", "esi_homepage");
    register_setting("custom-ad-cons-section", "esi_homepage_rhs");
    register_setting("custom-ad-cons-section", "esi_article_list_ids");
    register_setting("custom-ad-cons-section", "esi_article_list");
    register_setting("custom-ad-cons-section", "esi_article_list_rhs");
    register_setting("custom-ad-cons-section", "esi_article_show_ids");
    register_setting("custom-ad-cons-section", "esi_article_show_rhs");
    register_setting("custom-ad-cons-section", "esi_article_show_end_article_1");
    register_setting("custom-ad-cons-section", "esi_article_show_end_article_2");
    register_setting("custom-ad-cons-section", "ctn_article_show_mid_article_video");    
    register_setting("custom-ad-cons-section", "esi_header_m");
    register_setting("custom-ad-cons-section", "esi_homepage_ids_m");
    register_setting("custom-ad-cons-section", "esi_homepage_m");
    register_setting("custom-ad-cons-section", "esi_homepage_rhs_m");
    register_setting("custom-ad-cons-section", "esi_article_list_ids_m");
    register_setting("custom-ad-cons-section", "esi_article_list_m");
    register_setting("custom-ad-cons-section", "esi_article_list_rhs_m");
    register_setting("custom-ad-cons-section", "esi_article_show_ids_m");
    register_setting("custom-ad-cons-section", "esi_article_show_rhs_m");
    register_setting("custom-ad-cons-section", "esi_article_show_end_article_1_m");
    register_setting("custom-ad-cons-section", "esi_article_show_end_article_2_m");
    register_setting("custom-ad-cons-section", "ctn_article_show_mid_article_video_m");
    register_setting("custom-ad-cons-section", "ATF_300_DIV_ID");
    register_setting("custom-ad-cons-section", "ATF_728_DIV_ID");
    register_setting("custom-ad-cons-section", "BTF_300_DIV_ID");
    register_setting("custom-ad-cons-section", "ATF_300_AD_CODE");
    register_setting("custom-ad-cons-section", "ATF_728_AD_CODE");
    register_setting("custom-ad-cons-section", "BTF_300_AD_CODE");
    
}

function display_site_lang_element(){?>
    <input type="text" name="site_lang" id="site_lang" class="large-text code" class="large-text code" value="<?php echo get_custom_option('site_lang'); ?>" /><?php 
    
}
function display_og_locale_element(){?>
    <input type="text" name="og_locale" id="og_locale" class="large-text code" value="<?php echo get_custom_option('og_locale'); ?>" /><?php 
    
}
function display_google_site_verification_element(){?>
    <input type="text" name="google_site_verification" class="large-text code" id="google_site_verification" value="<?php echo get_custom_option('google_site_verification'); ?>" /><?php 
    
}

function display_fblikeurl_element(){?>
    <input type="text" name="fblikeurl" id="fblikeurl" class="large-text code" value="<?php echo get_custom_option('fblikeurl'); ?>" /><?php 
    
}
function display_favicon_element(){?>
    <input type="text" name="favicon" id="favicon" class="large-text code" value="<?php echo get_custom_option('favicon'); ?>" /><?php 
    
}
function display_ga_element(){?>
    <textarea name="ga" rows="10" cols="50" id="ga" class="large-text code" class="large-text code"><?php echo get_custom_option('ga'); ?></textarea><?php 
    
}
function display_fbappid_element(){?>
    <input type="text" name="fbappid" id="fbappid" class="large-text code" value="<?php echo get_custom_option('fbappid'); ?>" /><?php 
    
}
function display_channel_url_part_element(){?>
    <input type="text" name="channel_url_part" id="channel_url_part" class="large-text code" value="<?php echo get_custom_option('channel_url_part'); ?>" /><?php 
    
}
function display_main_site_url_element(){?>
    <input type="text" name="main_site_url" id="main_site_url" class="large-text code" value="<?php echo get_custom_option('main_site_url'); ?>" /><?php 
    
}
function display_main_site_txt_element(){?>
    <input type="text" name="main_site_txt" id="main_site_txt" class="large-text code" value="<?php echo get_custom_option('main_site_txt'); ?>" /><?php 
    
}
function display_fb_url_element(){?>
    <input type="text" name="fb_url" id="fb_url" class="large-text code" value="<?php echo get_custom_option('fb_url'); ?>" /><?php 
    
}
function display_twitter_url_element(){?>
    <input type="text" name="twitter_url" id="twitter_url" class="large-text code" value="<?php echo get_custom_option('twitter_url'); ?>" /><?php 
    
}
function display_twitter_handle_element(){?>
    <input type="text" name="twitter_handle" id="twitter_handle" class="large-text code" value="<?php echo get_custom_option('twitter_handle'); ?>" /><?php 
    
}

function display_google_url_element(){?>
    <input type="text" name="google_url" id="google_url" class="large-text code" value="<?php echo get_custom_option('google_url'); ?>" /><?php 
    
}
function display_rss_url_element(){?>
    <input type="text" name="rss_url" id="rss_url" class="large-text code" value="<?php echo get_custom_option('rss_url'); ?>" /><?php 
    
}
function display_logo_title_element(){?>
    <input type="text" name="logo_title" id="logo_title" class="large-text code" value="<?php echo get_custom_option('logo_title'); ?>" /><?php 
    
}
function display_logo_url_element(){?>
    <input type="text" name="logo_url" id="logo_url" class="large-text code" value="<?php echo get_custom_option('logo_url'); ?>" /><?php 
    
}
function display_footer_logo_txt_element(){?>
    <input type="text" name="footer_logo_txt" id="footer_logo_txt" class="large-text code" value="<?php echo get_custom_option('footer_logo_txt'); ?>" /><?php 
    
}
function display_comscore_tag_element(){?>
   <textarea name="comscore_tag" rows="10" cols="50" id="comscore_tag" class="large-text code"><?php echo get_custom_option('comscore_tag'); ?></textarea><?php 
    
}
function display_ibeat_channel_element(){?>
   <input type="text" name="ibeat_channel" id="ibeat_channel" class="large-text code" value="<?php echo get_custom_option('ibeat_channel'); ?>" /><?php 
    
}
function display_ibeat_host_element(){?>
   <input type="text" name="ibeat_host" id="ibeat_host" class="large-text code" value="<?php echo get_custom_option('ibeat_host'); ?>" /><?php 
    
}
function display_ibeat_key_element(){?>
   <input type="text" name="ibeat_key" id="ibeat_key" class="large-text code" value="<?php echo get_custom_option('ibeat_key'); ?>" /><?php 
    
}
function display_ibeat_domain_element(){?>
   <input type="text" name="ibeat_domain" id="ibeat_domain" class="large-text code" value="<?php echo get_custom_option('ibeat_domain'); ?>" /><?php 
    
}
function display_footer_dmp_tag_element(){?>
   <input type="text" name="footer_dmp_tag" id="footer_dmp_tag" class="large-text code" value="<?php echo get_custom_option('footer_dmp_tag'); ?>" /><?php 
    
}
function display_footer_google_tag_element(){?>
   <textarea name="footer_google_tag" rows="10" cols="50" id="footer_google_tag" class="large-text code"><?php echo get_custom_option('footer_google_tag'); ?></textarea><?php 
}
function display_google_tag_js_head_element(){?>
   <textarea name="google_tag_js_head" rows="10" cols="50" id="google_tag_js_head" class="large-text code"><?php echo get_custom_option('google_tag_js_head'); ?></textarea><?php 
    
}

function display_google_ad_top_element(){?>
   <textarea name="google_ad_top" rows="10" cols="50" id="google_ad_top" class="large-text code"><?php echo get_custom_option('google_ad_top'); ?></textarea><?php 
    
}

function display_google_ad_right_1_element(){?>
   <textarea name="google_ad_right_1" rows="10" cols="50" id="google_ad_right_1" class="large-text code"><?php echo get_custom_option('google_ad_right_1'); ?></textarea><?php 
    
}

function display_google_ad_right_2_element(){?>
   <textarea name="google_ad_right_2" rows="10" cols="50" id="google_ad_right_2" class="large-text code"><?php echo get_custom_option('google_ad_right_2'); ?></textarea><?php 
    
}
function display_visual_revenue_reader_response_tracking_script_element(){?>
   <textarea name="visual_revenue_reader_response_tracking_script" rows="10" cols="50" id="visual_revenue_reader_response_tracking_script" class="large-text code"><?php echo get_custom_option('visual_revenue_reader_response_tracking_script'); ?></textarea><?php 
    
}

function display_visual_revenue_reader_response_tracking_script_for_not_singular_element(){?>
    <textarea name="visual_revenue_reader_response_tracking_script_for_not_singular" rows="10" cols="50" id="visual_revenue_reader_response_tracking_script_for_not_singular" class="large-text code"><?php echo get_custom_option('visual_revenue_reader_response_tracking_script_for_not_singular'); ?></textarea><?php 
    
}
function display_not_found_heading_element(){?>
   <input type="text" name="not_found_heading" id="not_found_heading" class="large-text code" value="<?php echo get_custom_option('not_found_heading'); ?>" /><?php 
    
}
function display_not_found_txt_element(){?>
    <input type="text" name="not_found_txt" id="not_found_txt" class="large-text code" value="<?php echo get_custom_option('not_found_txt'); ?>" /><?php 
    
}

function display_search_placeholder_txt_element(){?>
    <input type="text" name="search_placeholder_txt" id="search_placeholder_txt" class="large-text code" value="<?php echo get_custom_option('search_placeholder_txt'); ?>" /><?php 
    
}

function display_go_to_the_profile_of_txt_element(){?>
    <input type="text" name="go_to_the_profile_of_txt" id="go_to_the_profile_of_txt" class="large-text code" value="<?php echo get_custom_option('go_to_the_profile_of_txt'); ?>" /><?php 
    
}

function display_go_to_txt_element(){?>
    <input type="text" name="go_to_txt" id="go_to_txt" class="large-text code" value="<?php echo get_custom_option('go_to_txt'); ?>" /><?php 
    
}

function display_share_link_fb_txt_element(){?>
    <input type="text" name="share_link_fb_txt" id="share_link_fb_txt" class="large-text code" value="<?php echo get_custom_option('share_link_fb_txt'); ?>" /><?php 
    
}

function display_share_link_twitter_txt_element(){?>
    <input type="text" name="share_link_twitter_txt" id="share_link_twitter_txt" class="large-text code" value="<?php echo get_custom_option('share_link_twitter_txt'); ?>" /><?php 
    
}

function display_share_link_linkedin_txt_element(){?>
    <input type="text" name="share_link_linkedin_txt" id="share_link_linkedin_txt" class="large-text code" value="<?php echo get_custom_option('share_link_linkedin_txt'); ?>" /><?php 
    
}

function display_mail_link_txt_element(){?>
    <input type="text" name="mail_link_txt" id="mail_link_txt" class="large-text code" value="<?php echo get_custom_option('mail_link_txt'); ?>" /><?php 
    
}

function display_read_complete_article_here_txt_element(){?>
    <input type="text" name="read_complete_article_here_txt" id="read_complete_article_here_txt" class="large-text code" value="<?php echo get_custom_option('read_complete_article_here_txt'); ?>" /><?php 
    
}

function display_featured_txt_element(){?>
    <input type="text" name="featured_txt" id="featured_txt" class="large-text code" value="<?php echo get_custom_option('featured_txt'); ?>" /><?php 
    
}

function display_disclaimer_txt_element(){?>
    <input type="text" name="disclaimer_txt" id="disclaimer_txt" class="large-text code" value="<?php echo get_custom_option('disclaimer_txt'); ?>" /><?php 
    
}
function display_disclaimer_on_txt_element(){?>
    <input type="text" name="disclaimer_on_txt" id="disclaimer_on_txt" class="large-text code" value="<?php echo get_custom_option('disclaimer_on_txt'); ?>" /><?php 
    
}
function display_disclaimer_off_txt_element(){?>
    <input type="text" name="disclaimer_of_txt" id="disclaimer_of_txt" class="large-text code" value="<?php echo get_custom_option('disclaimer_of_txt'); ?>" /><?php 
    
}
function display_most_discussed_txt_element(){?>
    <input type="text" name="most_discussed_txt" id="most_discussed_txt" class="large-text code" value="<?php echo get_custom_option('most_discussed_txt'); ?>" /><?php 
    
}

function display_more_txt_element(){?>
    <input type="text" name="more_txt" id="more_txt" class="large-text code" value="<?php echo get_custom_option('more_txt'); ?>" /><?php 
    
}
function display_less_txt_element(){?>
    <input type="text" name="less_txt" id="less_txt" class="large-text code" value="<?php echo get_custom_option('less_txt'); ?>" /><?php 
    
}
function display_login_txt_element(){?>
    <input type="text" name="login_txt" id="login_txt" class="large-text code" value="<?php echo get_custom_option('login_txt'); ?>" /><?php 
    
}
function display_logout_txt_element(){?>
    <input type="text" name="logout_txt" id="logout_txt" class="large-text code" value="<?php echo get_custom_option('logout_txt'); ?>" /><?php 
    
}
function display_view_all_posts_in_txt_element(){?>
    <input type="text" name="view_all_posts_in_txt" id="view_all_posts_in_txt" class="large-text code" value="<?php echo get_custom_option('view_all_posts_in_txt'); ?>" /><?php 
}

function display_home_txt_element(){?>
    <input type="text" name="home_txt" id="home_txt" class="large-text code" value="<?php echo get_custom_option('home_txt'); ?>" /><?php 
}
function display_blogs_txt_element(){?>
    <input type="text" name="blogs_txt" id="blogs_txt" class="large-text code" value="<?php echo get_custom_option('blogs_txt'); ?>" /><?php 
}

function display_search_results_for_txt_element(){?>
    <input type="text" name="search_results_for_txt" id="search_results_for_txt" class="large-text code" value="<?php echo get_custom_option('search_results_for_txt'); ?>" /><?php 
}
function display_most_read_txt_element(){?>
    <input type="text" name="most_read_txt" id="most_read_txt" class="large-text code" value="<?php echo get_custom_option('most_read_txt'); ?>" /><?php 
}
function display_popular_tags_txt_element(){?>
    <input type="text" name="popular_tags_txt" id="popular_tags_txt" class="large-text code" value="<?php echo get_custom_option('popular_tags_txt'); ?>" /><?php 
}
function display_recently_joined_authors_txt_element(){?>
    <input type="text" name="recently_joined_authors_txt" id="recently_joined_authors_txt" class="large-text code" value="<?php echo get_custom_option('recently_joined_authors_txt'); ?>" /><?php 
}
function display_like_us_txt_element(){?>
    <input type="text" name="like_us_txt" id="like_us_txt" class="large-text code" value="<?php echo get_custom_option('like_us_txt'); ?>" /><?php 
}
function display_author_txt_element(){?>
    <input type="text" name="author_txt" id="author_txt" class="large-text code" value="<?php echo get_custom_option('author_txt'); ?>" /><?php 
}
function display_popular_from_author_txt_element(){?>
    <input type="text" name="popular_from_author_txt" id="popular_from_author_txt" class="large-text code" value="<?php echo get_custom_option('popular_from_author_txt'); ?>" /><?php 
}
function display_search_authors_by_name_txt_element(){?>
    <input type="text" name="search_authors_by_name_txt" id="search_authors_by_name_txt" class="large-text code" value="<?php echo get_custom_option('search_authors_by_name_txt'); ?>" /><?php 
}
function display_search_txt_element(){?>
    <input type="text" name="search_txt" id="search_txt" class="large-text code" value="<?php echo get_custom_option('search_txt'); ?>" /><?php 
}
function display_back_to_authors_page_txt_element(){?>
    <input type="text" name="back_to_authors_page_txt" id="back_to_authors_page_txt" class="large-text code" value="<?php echo get_custom_option('back_to_authors_page_txt'); ?>" /><?php 
}
function display_no_authors_found_txt_element(){?>
    <input type="text" name="no_authors_found_txt" id="no_authors_found_txt" class="large-text code" value="<?php echo get_custom_option('no_authors_found_txt'); ?>" /><?php 
}
function display_further_commenting_is_disabled_txt_element(){?>
    <input type="text" name="further_commenting_is_disabled_txt" id="further_commenting_is_disabled_txt" class="large-text code" value="<?php echo get_custom_option('further_commenting_is_disabled_txt'); ?>" /><?php 
}
function display_comments_on_this_post_are_closed_now_txt_element(){?>
    <input type="text" name="comments_on_this_post_are_closed_now_txt" id="comments_on_this_post_are_closed_now_txt" class="large-text code" value="<?php echo get_custom_option('comments_on_this_post_are_closed_now_txt'); ?>" /><?php 
}
function display_add_your_comment_here_txt_element(){?>
    <input type="text" name="add_your_comment_here_txt" id="add_your_comment_here_txt" class="large-text code" value="<?php echo get_custom_option('add_your_comment_here_txt'); ?>" /><?php 
}
function display_characters_remaining_txt_element(){?>
    <input type="text" name="characters_remaining_txt" id="characters_remaining_txt" class="large-text code" value="<?php echo get_custom_option('characters_remaining_txt'); ?>" /><?php 
}
function display_share_on_fb_txt_element(){?>
    <input type="text" name="share_on_fb_txt" id="share_on_fb_txt" class="large-text code" value="<?php echo get_custom_option('share_on_fb_txt'); ?>" /><?php 
}
function display_share_on_twitter_txt_element(){?>
    <input type="text" name="share_on_twitter_txt" id="share_on_twitter_txt" class="large-text code" value="<?php echo get_custom_option('share_on_twitter_txt'); ?>" /><?php 
}
function display_sort_by_txt_element(){?>
    <input type="text" name="sort_by_txt" id="sort_by_txt" class="large-text code" value="<?php echo get_custom_option('sort_by_txt'); ?>" /><?php 
}

function display_newest_txt_element(){?>
    <input type="text" name="newest_txt" id="newest_txt" class="large-text code" value="<?php echo get_custom_option('newest_txt'); ?>" /><?php 
}
function display_oldest_txt_element(){?>
    <input type="text" name="oldest_txt" id="oldest_txt" class="large-text code" value="<?php echo get_custom_option('oldest_txt'); ?>" /><?php 
}
function display_discussed_txt_element(){?>
    <input type="text" name="discussed_txt" id="discussed_txt" class="large-text code" value="<?php echo get_custom_option('discussed_txt'); ?>" /><?php 
}
function display_up_voted_txt_element(){?>
    <input type="text" name="up_voted_txt" id="up_voted_txt" class="large-text code" value="<?php echo get_custom_option('up_voted_txt'); ?>" /><?php 
}
function display_down_voted_txt_element(){?>
    <input type="text" name="down_voted_txt" id="down_voted_txt" class="large-text code" value="<?php echo get_custom_option('down_voted_txt'); ?>" /><?php 
}
function display_be_the_first_one_to_review_txt_element(){?>
    <input type="text" name="be_the_first_one_to_review_txt" id="be_the_first_one_to_review_txt" class="large-text code" value="<?php echo get_custom_option('be_the_first_one_to_review_txt'); ?>" /><?php 
}
function display_more_points_needed_to_reach_next_level_txt_element(){?>
    <input type="text" name="more_points_needed_to_reach_next_level_txt" id="more_points_needed_to_reach_next_level_txt" class="large-text code" value="<?php echo get_custom_option('more_points_needed_to_reach_next_level_txt'); ?>" /><?php 
}
function display_know_more_about_times_points_txt_element(){?>
    <input type="text" name="know_more_about_times_points_txt" id="know_more_about_times_points_txt" class="large-text code" value="<?php echo get_custom_option('know_more_about_times_points_txt'); ?>" /><?php 
}
function display_know_more_about_times_points_link_element(){?>
    <input type="text" name="know_more_about_times_points_link" id="know_more_about_times_points_link" class="large-text code" value="<?php echo get_custom_option('know_more_about_times_points_link'); ?>" /><?php 
}
function display_badges_earned_txt_element(){?>
    <input type="text" name="badges_earned_txt" id="badges_earned_txt" class="large-text code" value="<?php echo get_custom_option('badges_earned_txt'); ?>" /><?php 
}
function display_just_now_txt_element(){?>
    <input type="text" name="just_now_txt" id="just_now_txt" class="large-text code" value="<?php echo get_custom_option('just_now_txt'); ?>" /><?php 
}
function display_follow_txt_element(){?>
    <input type="text" name="follow_txt" id="follow_txt" class="large-text code" value="<?php echo get_custom_option('follow_txt'); ?>" /><?php 
}
function display_reply_txt_element(){?>
    <input type="text" name="reply_txt" id="reply_txt" class="large-text code" value="<?php echo get_custom_option('reply_txt'); ?>" /><?php 
}
function display_flag_txt_element(){?>
    <input type="text" name="flag_txt" id="flag_txt" class="large-text code" value="<?php echo get_custom_option('flag_txt'); ?>" /><?php 
}
function display_up_vote_txt_element(){?>
    <input type="text" name="up_vote_txt" id="up_vote_txt" class="large-text code" value="<?php echo get_custom_option('up_vote_txt'); ?>" /><?php 
}
function display_down_vote_txt_element(){?>
    <input type="text" name="down_vote_txt" id="down_vote_txt" class="large-text code" value="<?php echo get_custom_option('down_vote_txt'); ?>" /><?php 
}
function display_mark_as_offensive_txt_element(){?>
    <input type="text" name="mark_as_offensive_txt" id="mark_as_offensive_txt" class="large-text code" value="<?php echo get_custom_option('mark_as_offensive_txt'); ?>" /><?php 
}
function display_find_this_comment_offensive_txt_element(){?>
    <input type="text" name="find_this_comment_offensive_txt" id="find_this_comment_offensive_txt" class="large-text code" value="<?php echo get_custom_option('find_this_comment_offensive_txt'); ?>" /><?php 
}
function display_reason_submitted_to_admin_txt_element(){?>
    <input type="text" name="reason_submitted_to_admin_txt" id="reason_submitted_to_admin_txt" class="large-text code" value="<?php echo get_custom_option('reason_submitted_to_admin_txt'); ?>" /><?php 
}
function display_choose_reason_txt_element(){?>
    <input type="text" name="choose_reason_txt" id="choose_reason_txt" class="large-text code" value="<?php echo get_custom_option('choose_reason_txt'); ?>" /><?php 
}
function display_reason_for_reporting_txt_element(){?>
    <input type="text" name="reason_for_reporting_txt" id="reason_for_reporting_txt" class="large-text code" value="<?php echo get_custom_option('reason_for_reporting_txt'); ?>" /><?php 
}
function display_foul_language_txt_element(){?>
    <input type="text" name="foul_language_txt" id="foul_language_txt" class="large-text code" value="<?php echo get_custom_option('foul_language_txt'); ?>" /><?php 
}
function display_defamatory_txt_element(){?>
    <input type="text" name="defamatory_txt" id="defamatory_txt" class="large-text code" value="<?php echo get_custom_option('defamatory_txt'); ?>" /><?php 
}
function display_inciting_hatred_against_certain_community_txt_element(){?>
    <input type="text" name="inciting_hatred_against_certain_community_txt" id="inciting_hatred_against_certain_community_txt" class="large-text code" value="<?php echo get_custom_option('inciting_hatred_against_certain_community_txt'); ?>" /><?php 
}
function display_out_of_context_spam_txt_element(){?>
    <input type="text" name="out_of_context_spam_txt" id="out_of_context_spam_txt" class="large-text code" value="<?php echo get_custom_option('out_of_context_spam_txt'); ?>" /><?php 
}
function display_others_txt_element(){?>
    <input type="text" name="others_txt" id="others_txt" class="large-text code" value="<?php echo get_custom_option('others_txt'); ?>" /><?php 
}
function display_report_this_txt_element(){?>
    <input type="text" name="report_this_txt" id="report_this_txt" class="large-text code" value="<?php echo get_custom_option('report_this_txt'); ?>" /><?php 
}
function display_close_txt_element(){?>
    <input type="text" name="close_txt" id="close_txt" class="large-text code" value="<?php echo get_custom_option('close_txt'); ?>" /><?php 
}
function display_already_marked_as_offensive_element(){?>
    <input type="text" name="already_marked_as_offensive" id="already_marked_as_offensive" class="large-text code" value="<?php echo get_custom_option('already_marked_as_offensive'); ?>" /><?php 
}
function display_flagged_txt_element(){?>
    <input type="text" name="flagged_txt" id="flagged_txt" class="large-text code" value="<?php echo get_custom_option('flagged_txt'); ?>" /><?php 
}
function display_blogauthor_link_element(){?>
    <input type="text" name="blogauthor_link" id="blogauthor_link" class="large-text code" value="<?php echo get_custom_option('blogauthor_link'); ?>" /><?php 
}
function display_old_blogauthor_link_element(){?>
    <input type="text" name="old_blogauthor_link" id="old_blogauthor_link" class="large-text code" value="<?php echo get_custom_option('old_blogauthor_link'); ?>" /><?php 
}
function display_blog_link_element(){?>
    <input type="text" name="blog_link" id="blog_link" class="large-text code" value="<?php echo get_custom_option('blog_link'); ?>" /><?php 
}
function display_common_cookie_domain_element(){?>
    <input type="text" name="common_cookie_domain" id="common_cookie_domain" class="large-text code" value="<?php echo get_custom_option('common_cookie_domain'); ?>" /><?php 
}
function display_quill_lang_element(){?>
    <input type="text" name="quill_lang" id="quill_lang" class="large-text code" value="<?php echo get_custom_option('quill_lang'); ?>" /><?php 
}
function display_quill_link_1_element(){?>
    <input type="text" name="quill_link_1" id="quill_link_1" class="large-text code" value="<?php echo get_custom_option('quill_link_1'); ?>" /><?php 
}
function display_quill_link_2_element(){?>
    <input type="text" name="quill_link_2" id="quill_link_2" class="large-text code" value="<?php echo get_custom_option('quill_link_2'); ?>" /><?php 
}

function display_quill_link_3_element(){?>
    <input type="text" name="quill_link_3" id="quill_link_3" class="large-text code" value="<?php echo get_custom_option('quill_link_3'); ?>" /><?php 
}

function display_quill_link_4_element(){?>
    <input type="text" name="quill_link_4" id="quill_link_4" class="large-text code" value="<?php echo get_custom_option('quill_link_4'); ?>" /><?php 
}
function display_offensive_comment_warning_element(){?>
    <input type="text" name="offensive_comment_warning" id="offensive_comment_warning" class="large-text code" value="<?php echo get_custom_option('offensive_comment_warning'); ?>" /><?php 
}
function display_footer_element(){?>
    <textarea name="footer" rows="10" cols="50" id="footer" class="large-text code"><?php echo get_custom_option('footer'); ?></textarea><?php 
}
function display_footer_css_element(){?>
    <textarea name="footer_css" rows="10" cols="50" id="footer_css" class="large-text code"><?php echo get_custom_option('footer_css'); ?></textarea><?php 
}
function display_ctn_homepage_element(){?>
    <textarea name="ctn_homepage" rows="10" cols="50" id="ctn_homepage" class="large-text code"><?php echo get_custom_option('ctn_homepage'); ?></textarea><?php 
}
function display_ctn_homepage_rhs_element(){?>
    <textarea name="ctn_homepage_rhs" rows="10" cols="50" id="ctn_homepage_rhs" class="large-text code"><?php echo get_custom_option('ctn_homepage_rhs'); ?></textarea><?php 
}
function display_ctn_article_list_element(){?>
    <textarea name="ctn_article_list" rows="10" cols="50" id="ctn_article_list" class="large-text code"><?php echo get_custom_option('ctn_article_list'); ?></textarea><?php 
}

function display_ctn_article_list_rhs_element(){?>
    <textarea name="ctn_article_list_rhs" rows="10" cols="50" id="ctn_article_list_rhs" class="large-text code"><?php echo get_custom_option('ctn_article_list_rhs'); ?></textarea><?php 
}
function display_ctn_article_show_rhs_element(){?>
    <textarea name="ctn_article_show_rhs" rows="10" cols="50" id="ctn_article_show_rhs" class="large-text code"><?php echo get_custom_option('ctn_article_show_rhs'); ?></textarea><?php 
}
function display_ctn_article_show_end_article_1_element(){?>
    <textarea name="ctn_article_show_end_article_1" rows="10" cols="50" id="ctn_article_show_end_article_1" class="large-text code"><?php echo get_custom_option('ctn_article_show_end_article_1'); ?></textarea><?php 
}
function display_ctn_article_show_end_article_2_element(){?>
    <textarea name="ctn_article_show_end_article_2" rows="10" cols="50" id="ctn_article_show_end_article_2" class="large-text code"><?php echo get_custom_option('ctn_article_show_end_article_2'); ?></textarea><?php 
}
function display_esi_status_element(){?>
    <!--<input type="text" name="esi_status" id="esi_status" class="large-text code" value="<?php echo get_custom_option('esi_status'); ?>" />-->
        <div class="large-text code"><select style="width: 100%"  name="esi_status" id="esi_status"  >
  <option <?php if(get_custom_option('esi_status')=='TEST'){ echo 'selected';}?> value="TEST">TEST</option>
  <option <?php if(get_custom_option('esi_status')=='LIVE'){ echo 'selected';}?> value="LIVE">LIVE</option>
  <option <?php if(get_custom_option('esi_status')=='NONE'){ echo 'selected';}?> value="NONE">NONE</option>
  
</select></div><?php 
}
function display_esi_header_element(){?>
    <textarea name="esi_header" rows="10" cols="50" id="esi_header" class="large-text code"><?php echo get_custom_option('esi_header'); ?></textarea><?php 
    
}
function display_esi_homepage_ids_element(){?>
    <input type="text" name="esi_homepage_ids" id="esi_homepage_ids" class="large-text code" value="<?php echo get_custom_option('esi_homepage_ids'); ?>" /><?php 
}
function display_esi_homepage_element(){?>
    <textarea name="esi_homepage" rows="10" cols="50" id="esi_homepage" class="large-text code"><?php echo get_custom_option('esi_homepage'); ?></textarea><?php     
}
function display_esi_homepage_rhs_element(){?>
    <textarea name="esi_homepage_rhs" rows="10" cols="50" id="esi_homepage_rhs" class="large-text code"><?php echo get_custom_option('esi_homepage_rhs'); ?></textarea><?php     
}
function display_esi_article_list_ids_element(){?>
    <input type="text" name="esi_article_list_ids" id="esi_article_list_ids" class="large-text code" class="large-text code" value="<?php echo get_custom_option('esi_article_list_ids'); ?>" /><?php     
}
function display_esi_article_list_element(){?>
    <textarea name="esi_article_list" rows="10" cols="50" id="esi_article_list" class="large-text code"><?php echo get_custom_option('esi_article_list'); ?></textarea><?php     
}
function display_esi_article_list_rhs_element(){?>
    <textarea name="esi_article_list_rhs" rows="10" cols="50" id="esi_article_list_rhs" class="large-text code"><?php echo get_custom_option('esi_article_list_rhs'); ?></textarea><?php     
}
function display_esi_article_show_ids_element(){?>
    <input type="text" name="esi_article_show_ids" id="esi_article_show_ids" class="large-text code" class="large-text code" value="<?php echo get_custom_option('esi_article_show_ids'); ?>" /><?php     
}
function display_esi_article_show_rhs_element(){?>
    <textarea name="esi_article_show_rhs" rows="10" cols="50" id="esi_article_show_rhs" class="large-text code"><?php echo get_custom_option('esi_article_show_rhs'); ?></textarea><?php     
}

function display_esi_article_show_end_article_1_element(){?>
    <textarea name="esi_article_show_end_article_1" rows="10" cols="50" id="esi_article_show_end_article_1" class="large-text code"><?php echo get_custom_option('esi_article_show_end_article_1'); ?></textarea><?php     
}

function display_esi_article_show_end_article_2_element(){?>
    <textarea name="esi_article_show_end_article_2" rows="10" cols="50" id="esi_article_show_end_article_2" class="large-text code"><?php echo get_custom_option('esi_article_show_end_article_2'); ?></textarea><?php     
}

function display_ctn_article_show_mid_article_video_element(){?>
    <textarea name="ctn_article_show_mid_article_video" rows="10" cols="50" id="ctn_article_show_mid_article_video" class="large-text code"><?php echo get_custom_option('ctn_article_show_mid_article_video'); ?></textarea>
        <div style="margin: 1em 0;overflow: hidden;width: 100%;background-color: #3A87AD;color: #ffffff">ESI MOBILE FIELDS</div><?php     
}
function display_esi_header_m_element(){?>
    <textarea name="esi_header_m" rows="10" cols="50" id="esi_header_m" class="large-text code"><?php echo get_custom_option('esi_header_m'); ?></textarea><?php 
    
}
function display_esi_homepage_ids_m_element(){?>
    <input type="text" name="esi_homepage_ids_m" id="esi_homepage_ids_m" class="large-text code" value="<?php echo get_custom_option('esi_homepage_ids_m'); ?>" /><?php 
}
function display_esi_homepage_m_element(){?>
    <textarea name="esi_homepage_m" rows="10" cols="50" id="esi_homepage_m" class="large-text code"><?php echo get_custom_option('esi_homepage_m'); ?></textarea><?php     
}
function display_esi_homepage_rhs_m_element(){?>
    <textarea name="esi_homepage_rhs_m" rows="10" cols="50" id="esi_homepage_rhs_m" class="large-text code"><?php echo get_custom_option('esi_homepage_rhs'); ?></textarea><?php     
}
function display_esi_article_list_ids_m_element(){?>
    <input type="text" name="esi_article_list_ids_m" id="esi_article_list_ids_m" class="large-text code" class="large-text code" value="<?php echo get_custom_option('esi_article_list_ids_m'); ?>" /><?php     
}
function display_esi_article_list_m_element(){?>
    <textarea name="esi_article_list_m" rows="10" cols="50" id="esi_article_list_m" class="large-text code"><?php echo get_custom_option('esi_article_list_m'); ?></textarea><?php     
}
function display_esi_article_list_rhs_m_element(){?>
    <textarea name="esi_article_list_rhs_m" rows="10" cols="50" id="esi_article_list_rhs_m" class="large-text code"><?php echo get_custom_option('esi_article_list_rhs_m'); ?></textarea><?php     
}
function display_esi_article_show_ids_m_element(){?>
    <input type="text" name="esi_article_show_ids_m" id="esi_article_show_ids_m" class="large-text code" class="large-text code" value="<?php echo get_custom_option('esi_article_show_ids_m'); ?>" /><?php     
}
function display_esi_article_show_rhs_m_element(){?>
    <textarea name="esi_article_show_rhs_m" rows="10" cols="50" id="esi_article_show_rhs_m" class="large-text code"><?php echo get_custom_option('esi_article_show_rhs_m'); ?></textarea><?php     
}

function display_esi_article_show_end_article_1_m_element(){?>
    <textarea name="esi_article_show_end_article_1_m" rows="10" cols="50" id="esi_article_show_end_article_1_m" class="large-text code"><?php echo get_custom_option('esi_article_show_end_article_1_m'); ?></textarea><?php     
}

function display_esi_article_show_end_article_2_m_element(){?>
    <textarea name="esi_article_show_end_article_2_m" rows="10" cols="50" id="esi_article_show_end_article_2_m" class="large-text code"><?php echo get_custom_option('esi_article_show_end_article_2_m'); ?></textarea><?php     
}

function display_ctn_article_show_mid_article_video_m_element(){?>
    <textarea name="ctn_article_show_mid_article_video_m" rows="10" cols="50" id="ctn_article_show_mid_article_video_m" class="large-text code"><?php echo get_custom_option('ctn_article_show_mid_article_video_m'); ?></textarea><?php     
}
function display_ATF_300_DIV_ID_element(){?>
    <input type="text" name="ATF_300_DIV_ID" id="ATF_300_DIV_ID" class="large-text code" value="<?php echo get_custom_option('ATF_300_DIV_ID'); ?>" /><?php 
}
function display_ATF_728_DIV_ID_element(){?>
    <input type="text" name="ATF_728_DIV_ID" id="ATF_728_DIV_ID" class="large-text code" value="<?php echo get_custom_option('ATF_728_DIV_ID'); ?>" /><?php 
}
function display_BTF_300_DIV_ID_element(){?>
    <input type="text" name="BTF_300_DIV_ID" id="BTF_300_DIV_ID" class="large-text code" value="<?php echo get_custom_option('BTF_300_DIV_ID'); ?>" /><?php 
}
function display_ATF_300_AD_CODE_element(){?>
    <input type="text" name="ATF_300_AD_CODE" id="ATF_300_AD_CODE" class="large-text code" value="<?php echo get_custom_option('ATF_300_AD_CODE'); ?>" /><?php 
}
function display_ATF_728_AD_CODE_element(){?>
    <input type="text" name="ATF_728_AD_CODE" id="ATF_728_AD_CODE" class="large-text code" value="<?php echo get_custom_option('ATF_728_AD_CODE'); ?>" /><?php 
}
function display_BTF_300_AD_CODE_element(){?>
    <input type="text" name="BTF_300_AD_CODE" id="BTF_300_AD_CODE" class="large-text code" value="<?php echo get_custom_option('BTF_300_AD_CODE'); ?>" /><?php 
}
function display_channel_name_element(){?>
    <input type="text" name="channel_name" id="channel_name" class="large-text code" value="<?php echo get_custom_option('channel_name'); ?>" /><?php 
}
function display_site_id_element(){?>
    <input type="text" name="site_id" id="site_id" class="large-text code" value="<?php echo get_custom_option('site_id'); ?>" /><?php 
}
function display_domain_name_element(){?>
    <input type="text" name="domain_name" id="domain_name" class="large-text code" value="<?php echo get_custom_option('domain_name'); ?>" /><?php 
}
function display_ofcommenthostid_element(){?>
    <input type="text" name="ofcommenthostid" id="ofcommenthostid" class="large-text code" value="<?php echo get_custom_option('ofcommenthostid'); ?>" /><?php 
}
function display_ofcommentchannelid_element(){?>
    <input type="text" name="ofcommentchannelid" id="ofcommentchannelid" class="large-text code" value="<?php echo get_custom_option('ofcommentchannelid'); ?>" /><?php 
}
function display_appid_element(){?>
    <input type="text" name="appid" id="appid" class="large-text code" value="<?php echo get_custom_option('appid'); ?>" /><?php 
}
function display_comment_text_element(){?>
    <textarea name="comment_text" rows="10" cols="50" id="comment_text" class="large-text code"><?php echo get_custom_option('comment_text'); ?></textarea><?php  
}
function display_Z_IMAGE_PLACEHOLDER_element(){?>
    <input type="text" name="Z_IMAGE_PLACEHOLDER" id="Z_IMAGE_PLACEHOLDER" class="large-text code" value="<?php echo get_custom_option('Z_IMAGE_PLACEHOLDER'); ?>" /><?php    
}
function display_FB_PLACEHOLDER_element(){?>
    <input type="text" name="FB_PLACEHOLDER" id="FB_PLACEHOLDER" class="large-text code"  value="<?php echo get_custom_option('FB_PLACEHOLDER'); ?>" /><?php    
}
function display_Z_AUTHOR_PLACEHOLDER_element(){?>
    <input type="text" name="Z_AUTHOR_PLACEHOLDER" id="Z_AUTHOR_PLACEHOLDER" class="large-text code"  value="<?php echo get_custom_option('Z_AUTHOR_PLACEHOLDER'); ?>" /><?php    
}
function display_RECO_URL_element(){?>
    <input type="text" name="RECO_URL" id="RECO_URL" class="large-text code" value="<?php echo get_custom_option('RECO_URL'); ?>" /><?php    
}
function display_COMMENTS_URL_element(){?>
    <input type="text" name="COMMENTS_URL" id="COMMENTS_URL" class="large-text code" value="<?php echo get_custom_option('COMMENTS_URL'); ?>" /><?php    
}

function display_RCHID_element(){?>
    <input type="text" name="RCHID" id="RCHID"  class="large-text code" value="<?php echo get_custom_option('RCHID'); ?>" /><?php    
}
function display_MYTIMES_URL_element(){?>
    <input type="text" name="MYTIMES_URL" id="MYTIMES_URL"  class="large-text code" value="<?php echo get_custom_option('MYTIMES_URL'); ?>" /><?php    
}

function display_MYT_COMMENTS_API_KEY_element(){?>
    <input type="text" name="MYT_COMMENTS_API_KEY" id="MYT_COMMENTS_API_KEY"  class="large-text code" value="<?php echo get_custom_option('MYT_COMMENTS_API_KEY'); ?>" /><?php    
}



add_filter('upload_mimes','restrict_mime');

function restrict_mime($mimes) {
	$mimes = array(
					'jpg|jpeg|jpe' => 'image/jpeg',
					//'gif' => 'image/gif',
					'png' => 'image/png',
	);
	return $mimes;
}


function getAppKeyForMyTimes()
{
    $appKeyForMyTimes = MYT_COMMENTS_API_KEY;    
    return $appKeyForMyTimes; 
}

add_action('wp_ajax_mytimeEntityMigration', 'mytimeEntityMigration');
add_action('wp_ajax_nopriv_mytimeEntityMigration', 'mytimeEntityMigration');

function mytimeEntityMigration()
{
    ignore_user_abort(true);
    set_time_limit(0);
    $appKeyForMyTimes = getAppKeyForMyTimes();
    $perPageRecord = 2000;
    if(isset($_GET['page'])&& $_GET['page']>0){
        $offset = $_GET['page'];
    }else{
        $offset = 1;
    }
    $offset = $perPageRecord*($offset-1);
    $args = array('numberposts' => $perPageRecord,
                  'offset' => $offset  
                 );
    $postList = get_posts($args);
    //var_dump($postList);
    //die;
    foreach ($postList as $key => $postDetails) 
    {
        
        echo $post_id = $postDetails->ID;
        echo "<br/>";
        //continue;
        if(has_post_thumbnail($post_id)) 
        { 
                $photo = wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), 'single-post-thumbnail' )[0];
        }
        else
        {
            $post_author = get_post_field( 'post_author', $post_id );
            $photo = get_user_avatar($post_author);
        }

        $title = $postDetails->post_title;
        $subtitle = get_post_meta($post_id, 'wps_subtitle', true );
        $headline = $subtitle ? $subtitle : $title;

        $specialCharToRemove = array('&lsquo;','&rsquo;','&zwj;','&zwnj;','!&nbsp;','&nbsp;','&mdash;','&ndash;','&amp;');
        $synopsys = strip_tags(get_the_excerpt($post_id));
        foreach ($specialCharToRemove as $k=>$value) 
        {
            $synopsys = str_replace($value,'', $synopsys);
        }

        global $my_settings;
        $photo = str_replace($my_settings['blogauthor_link'], $my_settings['blog_link'], $photo);
        $photo = str_replace($my_settings['old_blogauthor_link'], $my_settings['blog_link'], $photo);
        $articleLink = get_permalink( $post_id );
        $articleLink = str_replace($my_settings['blogauthor_link'], $my_settings['blog_link'], $articleLink);
        $articleLink = str_replace($my_settings['old_blogauthor_link'], $my_settings['blog_link'], $articleLink);

        $categories = get_the_category($post_id);
        $catIds = array();
        if($categories){
            foreach($categories as $category) {
                $catIds[] = $category->cat_ID;
            }
        }
        $catIdsList = implode(',', $catIds);
        $catName    = trim($categories[0]->cat_name);

        $xmlPost = array(
                    'name'=>($headline),
                    'id' => $post_id,
                    'synopsys' =>$synopsys, 
                    'url' =>$articleLink,
                    'type' =>'ARTICLE',
                    'appKey'=>$appKeyForMyTimes,
                    'cat'=>$catName,
                    'scat'=>'',
                    'parentmsidlist'=>$catIdsList,
                    'imageUrl'=>$photo
                );
        //$str = 'cmsObjectDetail=<entity><msid>'.$post_id.'</msid><Mstype>2</Mstype><Mssubtype>0</Mssubtype><title><![CDATA['.urlencode(get_the_title( $post_id )).']]></title><uppdt_date>10/18/2016 12:36:06 PM</uppdt_date><appKey>'.$appKeyForMyTimes.'</appKey><synopsys>'.urlencode(get_the_excerpt($post_id)).'</synopsys><url>'.get_permalink( $post_id ).'</url><topicNames></topicNames><parentmsidlist></parentmsidlist></entity>';
        $str = "entityDetail=".json_encode($xmlPost);
        //$url = MYTIMES_URL.'mytimes/cms/add';
        $url = MYTIMES_URL.'mytimes/entity/add';
        //die;
        //$curl_headers = array('Content-Type: application/x-www-form-urlencoded; charset=UTF-8', 'Accept-Encoding: gzip; deflate', 'Accept-Language: en-US');
        $curl_headers = array('Content-Type: application/x-www-form-urlencoded; charset=UTF-8', 'Accept-Language: en-US');
	$ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url); 
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$str);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,300);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $curl_headers);
        $ch_result = curl_exec($ch);
        //var_dump($ch_result);
        curl_close($ch);
    }
    die();   
}

add_action('wp_ajax_mytimeEntityVerification', 'mytimeEntityVerification');
add_action('wp_ajax_nopriv_mytimeEntityVerification', 'mytimeEntityVerification');

function mytimeEntityVerification()
{
    ignore_user_abort(true);
    set_time_limit(0);
    $appKeyForMyTimes = getAppKeyForMyTimes();
    $perPageRecord = 2000;
    if(isset($_GET['page'])&& $_GET['page']>0){
        $offset = $_GET['page'];
    }else{
        $offset = 1;
    }
    $offset = $perPageRecord*($offset-1);
    $args = array('numberposts' => $perPageRecord,
                  'offset' => $offset  
                 );
    $postList = get_posts($args);

    //$curl_headers = array('Content-Type: application/x-www-form-urlencoded; charset=UTF-8', 'Content-Length: ' . (strlen($str)), 'Accept-Encoding: gzip; deflate', 'Accept-Language: en-US');
	$curl_headers = array('Content-Type: application/x-www-form-urlencoded; charset=UTF-8', 'Content-Length: ' . (strlen($str)), 'Accept-Language: en-US');
    foreach ($postList as $key => $postDetails) 
    {
        $url = "http://mytpvt.indiatimes.com/mytimes/entity?msid=".$postDetails->ID."&appKey=".$appKeyForMyTimes;
        //echo "<br/>";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,300);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $curl_headers);
        $ch_result = curl_exec($ch);
        //var_dump($ch_result);
        if($ch_result=="[]"){
            echo "Failed for postId ==".$postDetails->ID." and appkey=".$appKeyForMyTimes."<br/>";
        }else{
            //echo "Success for postId ==".$postDetails->ID." and appkey=".$appKeyForMyTimes."<br/>";
        }
        curl_close($ch);   
    }
    die();
}

function printBottomComments($postId){
    global $my_settings;
    $appKeyForMyTimes = getAppKeyForMyTimes($postId);
    $url = MYTIMES_URL.'mytimes/getFeed/Activity/?msid='
            .$postId.'&curpg=1&commenttype=CreationDate&appkey='.
            $appKeyForMyTimes.'&sortcriteria=CreationDate&order=asc&size=3&pagenum=1';
    //$curl_headers = array('Content-Type: application/x-www-form-urlencoded; charset=UTF-8', 'Accept-Encoding: gzip; deflate', 'Accept-Language: en-US');
    $curl_headers = array('Content-Type: application/x-www-form-urlencoded; charset=UTF-8', 'Accept-Language: en-US');
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url); 
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,300);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $curl_headers);
    $ch_result = curl_exec($ch);
    curl_close($ch);
    $commentsData = json_decode($ch_result, true);
    if(count($commentsData) > 1){
		for($c = 1; $c < count($commentsData); $c++){

            $imgSrc = "https://timesofindia.indiatimes.com/photo/29251859.cms";
            if($commentsData[$c]['PIU']!=''){
               $imgSrc = $commentsData[$c]['PIU']; 
            }

			?>
			<div class="comment-box level1" data-id="<?php echo $commentsData[$c]['_id']; ?>">
				<div class="user-thumbnail"><img class="userimg flL" src="https://timesofindia.indiatimes.com/photo/29251859.cms" data-src="<?php echo str_replace('http://','https://',$imgSrc); ?>"></div>
				<a href="http://mytimes.indiatimes.com/profile/<?php echo $commentsData[$c]['A_ID']; ?>" class="name" target="_blank"><?php echo $commentsData[$c]['A_D_N']; ?></a>
				<p class="short_comment"><?php echo substr(strip_tags($commentsData[$c]['C_T']), 0, 150); ?><?php if(strlen(strip_tags($commentsData[$c]['C_T'])) > 150) { echo '...'; } ?></p>
				<div class="footbar clearfix">
					<span data-action="comment-reply" class="cpointer cp-reply"><?php echo $my_settings['reply_txt'];?></span>
					<span class="up cpointer" data-action="comment-agree" title="Up Vote"><i class="icon-uparrow"></i></span>
					<span class="down cpointer" data-action="comment-disagree" title="Down Vote"><i class="icon-downarrow"></i></span>
					<span class="cpointer flag_comment"> <span data-action="comment-offensive" title="Mark as offensive"></span></span>
				</div>
			</div>
			<?php
		}
	}
}

function getNbtFooterHtml(){
    $langChannel = isLang();
    $domainName ='';
    return '';
    if($langChannel){
        switch ($langChannel) {
            case 'nbt':
                $domainName ='https://navbharattimes.indiatimes.com/';
                break;
            case 'mt':
                $domainName ='https://maharashtratimes.indiatimes.com/';
                break;
            case 'eisamay':
                $domainName ='https://eisamay.indiatimes.com/';
                break;
            case 'vk':
                $domainName ='https://vijaykarnataka.indiatimes.com/';
                break;
            case 'ml':
                $domainName ='https://malayalam.samayam.com/';
                break;
            case 'tm':
                $domainName ='https://tamil.samayam.com/';
                break;
            case 'tl':
                $domainName ='https://telugu.samayam.com/';
                break;

            default:
                $domainName ='https://navbharattimes.indiatimes.com/';
                break;
        }
        
        if($domainName){
            $footerUrl = $domainName.'wdt_footer.cms?showstyle=1';
            $footerHtml = file_get_contents($footerUrl);
            if ($footerHtml) {
                return $footerHtml;
            } else {
                return '';
            }
        }else{
            return '';
        }        
        
    }
    return '';
    

}

function setEnvironment(){
        $blogsDomainArr = ['blogs.malayalam.samayam.com',
                    'local.blogs.navbharattimes.indiatimes.com',
                    'blogs.navbharattimes.indiatimes.com',
                    'blogs.maharashtratimes.indiatimes.com',
                    'blogs.eisamay.indiatimes.com',
                    'blogs.vijaykarnataka.indiatimes.com',
                    'blogs.tamil.samayam.com/',
                    'blogs.telugu.samayam.com/'
                  ];
        
        

        if(in_array($_SERVER['HTTP_HOST'], $blogsDomainArr)){
                putenv("environment=live");
        }else{
                putenv("environment=test");
        }
}
setEnvironment();

