var ad_config = {
	defSlots: [
		[320, 50],
		[468, 60],
		[728, 90]
	]
};

function defineAdSlots(wapads) {
	var _auds = new Array();
	if (typeof(_ccaud) != 'undefined') {
		for (var i = 0; i < _ccaud.Profile.Audiences.Audience.length; i++)
			if (i < 200)
				_auds.push(_ccaud.Profile.Audiences.Audience[i].abbr);
	}

	var _HDL = '';
	var _ARC1 = 'strong';
	var _Hyp1 = '';
	var _article = '';
	
	var _SCN = (typeof _SCN_pg != "undefined") ? _SCN_pg : '';
	var _Tmpl = (typeof _Tmpl_pg != "undefined") ? _Tmpl_pg : '';
	var _Tmpl_SCN = (_Tmpl && _SCN) ? _Tmpl+"_"+_SCN : '';
	
	var _tval = function(v) {
		if (typeof(v) == 'undefined') return '';
		if (v.length > 100) return v.substr(0, 100);
		return v;
	};
       ///console.log(wapads);
        //alert(wapads);
	$.each(wapads, function(i, v) {
           // alert(i);
		if($('div.' + i + ':visible').length > 0){
			if(v['type'].indexOf("OP_Innov1") >= 0  || v['type'].indexOf("OP_Inter") >= 0 || v['type'].indexOf("OP_Pop") >= 0 || v['type'].indexOf("OP_Shosh") >= 0 || v['type'].indexOf("OP_SPTLIT") >= 0){
				var gtag = googletag.defineOutOfPageSlot(v['name'], v['id']).addService(googletag.pubads());
			}else{
				var gtag = googletag.defineSlot(v['name'], (v['size'] ? v['size'] : ad_config.defSlots), v['id']).addService(googletag.pubads());
			}
			var mlb;
			if (v['mlb'] && v['mlb'].length > 0) {
				mlb = googletag.sizeMapping();
				for (var j = 0; j < v['mlb'].length; j++) {
					mlb = mlb.addSize(v['mlb'][j][0], v['mlb'][j][1]);
				}
				gtag.defineSizeMapping(mlb.build());
			}
			gtag.addService(googletag.pubads());
		}
	});

	googletag.pubads().setTargeting('sg', _auds).setTargeting('HDL', _tval(_HDL)).setTargeting('ARC1', _tval(_ARC1)).setTargeting('Hyp1', _tval(_Hyp1)).setTargeting('article', _tval(_article));
                //setTargeting(_Tmpl_SCN_var, _Tmpl_SCN + '');
	googletag.pubads().enableSingleRequest();
	googletag.pubads().collapseEmptyDivs(true);
	TimesGDPR.common.consentModule.gdprCallback(function(data) {
			if( TimesGDPR.common.consentModule.isEUuser() ) {
				googletag.pubads().setRequestNonPersonalizedAds(1);
			}
			googletag.enableServices();
		} );
}

function resizeAdInfo() {
	/*'atf_300': {
		resize: function() {
			$('.ATF_300').css({
				'height': '250px'
			});
		}
	},
	'btf': {
		resize: function() {
			$('[data-node="footer"]').css({
				'margin-top': '10px'
			});
		}
	}*/
}

function createAds(wapads) {
	$.each(wapads, function(i, v) {
		if($('div.' + i + ':visible').length > 0){
			var tmpScript = $('<script>').attr('type', 'text/javascript');
			var div = $('<div>').attr('id', v['id']).append(tmpScript);
			if(v['type'].indexOf("OP_Innov1") >= 0  || v['type'].indexOf("OP_Inter") >= 0 || v['type'].indexOf("OP_Pop") >= 0 || v['type'].indexOf("OP_Shosh") >= 0 || v['type'].indexOf("OP_SPTLIT") >= 0){
				$('body').append(div);
			} else {
				if($('div.' + i).length > 0){
					$('div.' + i).append(div);
				}
			}
			
			googletag.cmd.push(function(){googletag.display(v['id']);});
		}
	});
}

function init_Ads(callback) {
	if(typeof googletag === "undefined"){
		googletag = window.googletag || {};
		googletag.cmd = googletag.cmd || [];
		(function() {
			var gads = document.createElement('script');
			gads.async = true;
			gads.type = 'text/javascript';
			var useSSL = 'https:' == document.location.protocol;
			gads.src = (useSSL ? 'https:' : 'http:') + '//www.googletagservices.com/tag/js/gpt.js';
			googletag.cmd.push(callback);
			var node = document.getElementsByTagName('script')[0];
			node.parentNode.insertBefore(gads, node);
		})();
	}else{
		googletag.cmd.push(callback);
	}
}

function initGoogleAds(wapads) {
	//if(_isPrime){ return; }
	init_Ads(function() {
		googletag.cmd.push(defineAdSlots(wapads));
		googletag.pubads().addEventListener('slotRenderEnded', function(event) {
			if (event && event.slot) {
				var slotName = event.slot.getAdUnitPath().toLowerCase();
				// console.log(slotName);
				for (var i in resizeAdInfo) {
					if (slotName.indexOf(i) != -1) {
						resizeAdInfo[i].resize();
						break;
					}
				}
			}
		});
		createAds(wapads);
	});
}
