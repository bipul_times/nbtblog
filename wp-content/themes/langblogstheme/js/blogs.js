    
        var TimesApps = window.TimesApps || {};
        TimesApps.isIncognito = false;
        
        (function(){
            function retry(isDone, next) {
                var current_trial = 0, max_retry = 50, interval = 10, is_timeout = false;
                var id = window.setInterval(
                    function() {
                        if (isDone()) {
                            window.clearInterval(id);
                            next(is_timeout);
                        }
                        if (current_trial++ > max_retry) {
                            window.clearInterval(id);
                            is_timeout = true;
                            next(is_timeout);
                        }
                    },
                    10
                );
            }
            
            function isIE10OrLater(user_agent) {
                var ua = user_agent.toLowerCase();
                if (ua.indexOf('msie') === 0 && ua.indexOf('trident') === 0) {
                    return false;
                }
                var match = /(?:msie|rv:)\s?([\d\.]+)/.exec(ua);
                if (match && parseInt(match[1], 10) >= 10) {
                    return true;
                }
                return false;
            }
            
            function detectPrivateMode(callback) {
                var is_private;
            
                if (window.webkitRequestFileSystem) {
                    window.webkitRequestFileSystem(
                        window.TEMPORARY, 1,
                        function() {
                            is_private = false;
                        },
                        function(e) {
                           // console.log(e);
                            is_private = true;
                        }
                    );
                } else if (window.indexedDB && /Firefox/.test(window.navigator.userAgent)) {
                    var db;
                    try {
                        db = window.indexedDB.open('test');
                    } catch(e) {
                        is_private = true;
                    }
            
                    if (typeof is_private === 'undefined') {
                        retry(
                            function isDone() {
                                return db.readyState === 'done' ? true : false;
                            },
                            function next(is_timeout) {
                                if (!is_timeout) {
                                    is_private = db.result ? false : true;
                                }
                            }
                        );
                    }
                } else if (isIE10OrLater(window.navigator.userAgent)) {
                    is_private = false;
                    try {
                        if (!window.indexedDB) {
                            is_private = true;
                        }                 
                    } catch (e) {
                        is_private = true;
                    }
                } else if (window.localStorage && /Safari/.test(window.navigator.userAgent)) {
                    try {
                        window.localStorage.setItem('test', 1);
                    } catch(e) {
                        is_private = true;
                    }
            
                    if (typeof is_private === 'undefined') {
                        is_private = false;
                        window.localStorage.removeItem('test');
                    }
                }
            
                retry(
                    function isDone() {
                        return typeof is_private !== 'undefined' ? true : false;
                    },
                    function next(is_timeout) {
                        callback(is_private);
                    }
                );
            }
            
            detectPrivateMode(
                function(is_private) {
                    TimesApps.isIncognito  = typeof is_private === 'undefined' ? false : is_private ? true : false;
                    if(TimesApps.isIncognito && typeof ga!="undefined"){
                        ga('send', 'event', 'Incognito_WEB', window.navigator.userAgent, window.location.href);
                
                    }
                }
            );
        })();
    
    
	    
/*mousewheel*/
(function(a){function d(b){var c=b||window.event,d=[].slice.call(arguments,1),e=0,f=!0,g=0,h=0;return b=a.event.fix(c),b.type="mousewheel",c.wheelDelta&&(e=c.wheelDelta/120),c.detail&&(e=-c.detail/3),h=e,c.axis!==undefined&&c.axis===c.HORIZONTAL_AXIS&&(h=0,g=-1*e),c.wheelDeltaY!==undefined&&(h=c.wheelDeltaY/120),c.wheelDeltaX!==undefined&&(g=-1*c.wheelDeltaX/120),d.unshift(b,e,g,h),(a.event.dispatch||a.event.handle).apply(this,d)}var b=["DOMMouseScroll","mousewheel"];if(a.event.fixHooks)for(var c=b.length;c;)a.event.fixHooks[b[--c]]=a.event.mouseHooks;a.event.special.mousewheel={setup:function(){if(this.addEventListener)for(var a=b.length;a;)this.addEventListener(b[--a],d,!1);else this.onmousewheel=d},teardown:function(){if(this.removeEventListener)for(var a=b.length;a;)this.removeEventListener(b[--a],d,!1);else this.onmousewheel=null}},a.fn.extend({mousewheel:function(a){return a?this.bind("mousewheel",a):this.trigger("mousewheel")},unmousewheel:function(a){return this.unbind("mousewheel",a)}})})(jQuery);
/*custom scrollbar*/
(function(c){var b={init:function(e){var f={set_width:false,set_height:false,horizontalScroll:false,scrollInertia:950,mouseWheel:true,mouseWheelPixels:"auto",autoDraggerLength:true,autoHideScrollbar:false,alwaysShowScrollbar:false,snapAmount:null,snapOffset:0,scrollButtons:{enable:false,scrollType:"continuous",scrollSpeed:"auto",scrollAmount:40},advanced:{updateOnBrowserResize:true,updateOnContentResize:false,autoExpandHorizontalScroll:false,autoScrollOnFocus:true,normalizeMouseWheelDelta:false},contentTouchScroll:true,callbacks:{onScrollStart:function(){},onScroll:function(){},onTotalScroll:function(){},onTotalScrollBack:function(){},onTotalScrollOffset:0,onTotalScrollBackOffset:0,whileScrolling:function(){}},theme:"light"},e=c.extend(true,f,e);return this.each(function(){var m=c(this);if(e.set_width){m.css("width",e.set_width)}if(e.set_height){m.css("height",e.set_height)}if(!c(document).data("mCustomScrollbar-index")){c(document).data("mCustomScrollbar-index","1")}else{var t=parseInt(c(document).data("mCustomScrollbar-index"));c(document).data("mCustomScrollbar-index",t+1)}m.wrapInner("<div class='mCustomScrollBox mCS-"+e.theme+"' id='mCSB_"+c(document).data("mCustomScrollbar-index")+"' style='position:relative; height:100%; overflow:hidden; max-width:100%;' />").addClass("mCustomScrollbar _mCS_"+c(document).data("mCustomScrollbar-index"));var g=m.children(".mCustomScrollBox");if(e.horizontalScroll){g.addClass("mCSB_horizontal").wrapInner("<div class='mCSB_h_wrapper' style='position:relative; left:0; width:999999px;' />");var k=g.children(".mCSB_h_wrapper");k.wrapInner("<div class='mCSB_container' style='position:absolute; left:0;' />").children(".mCSB_container").css({width:k.children().outerWidth(),position:"relative"}).unwrap()}else{g.wrapInner("<div class='mCSB_container' style='position:relative; top:0;' />")}var o=g.children(".mCSB_container");if(c.support.touch){o.addClass("mCS_touch")}o.after("<div class='mCSB_scrollTools' style='position:absolute;'><div class='mCSB_draggerContainer'><div class='mCSB_dragger' style='position:absolute;' oncontextmenu='return false;'><div class='mCSB_dragger_bar' style='position:relative;'></div></div><div class='mCSB_draggerRail'></div></div></div>");var l=g.children(".mCSB_scrollTools"),h=l.children(".mCSB_draggerContainer"),q=h.children(".mCSB_dragger");if(e.horizontalScroll){q.data("minDraggerWidth",q.width())}else{q.data("minDraggerHeight",q.height())}if(e.scrollButtons.enable){if(e.horizontalScroll){l.prepend("<a class='mCSB_buttonLeft' oncontextmenu='return false;'></a>").append("<a class='mCSB_buttonRight' oncontextmenu='return false;'></a>")}else{l.prepend("<a class='mCSB_buttonUp' oncontextmenu='return false;'></a>").append("<a class='mCSB_buttonDown' oncontextmenu='return false;'></a>")}}g.bind("scroll",function(){if(!m.is(".mCS_disabled")){g.scrollTop(0).scrollLeft(0)}});m.data({mCS_Init:true,mCustomScrollbarIndex:c(document).data("mCustomScrollbar-index"),horizontalScroll:e.horizontalScroll,scrollInertia:e.scrollInertia,scrollEasing:"mcsEaseOut",mouseWheel:e.mouseWheel,mouseWheelPixels:e.mouseWheelPixels,autoDraggerLength:e.autoDraggerLength,autoHideScrollbar:e.autoHideScrollbar,alwaysShowScrollbar:e.alwaysShowScrollbar,snapAmount:e.snapAmount,snapOffset:e.snapOffset,scrollButtons_enable:e.scrollButtons.enable,scrollButtons_scrollType:e.scrollButtons.scrollType,scrollButtons_scrollSpeed:e.scrollButtons.scrollSpeed,scrollButtons_scrollAmount:e.scrollButtons.scrollAmount,autoExpandHorizontalScroll:e.advanced.autoExpandHorizontalScroll,autoScrollOnFocus:e.advanced.autoScrollOnFocus,normalizeMouseWheelDelta:e.advanced.normalizeMouseWheelDelta,contentTouchScroll:e.contentTouchScroll,onScrollStart_Callback:e.callbacks.onScrollStart,onScroll_Callback:e.callbacks.onScroll,onTotalScroll_Callback:e.callbacks.onTotalScroll,onTotalScrollBack_Callback:e.callbacks.onTotalScrollBack,onTotalScroll_Offset:e.callbacks.onTotalScrollOffset,onTotalScrollBack_Offset:e.callbacks.onTotalScrollBackOffset,whileScrolling_Callback:e.callbacks.whileScrolling,bindEvent_scrollbar_drag:false,bindEvent_content_touch:false,bindEvent_scrollbar_click:false,bindEvent_mousewheel:false,bindEvent_buttonsContinuous_y:false,bindEvent_buttonsContinuous_x:false,bindEvent_buttonsPixels_y:false,bindEvent_buttonsPixels_x:false,bindEvent_focusin:false,bindEvent_autoHideScrollbar:false,mCSB_buttonScrollRight:false,mCSB_buttonScrollLeft:false,mCSB_buttonScrollDown:false,mCSB_buttonScrollUp:false});if(e.horizontalScroll){if(m.css("max-width")!=="none"){if(!e.advanced.updateOnContentResize){e.advanced.updateOnContentResize=true}}}else{if(m.css("max-height")!=="none"){var s=false,r=parseInt(m.css("max-height"));if(m.css("max-height").indexOf("%")>=0){s=r,r=m.parent().height()*s/100}m.css("overflow","hidden");g.css("max-height",r)}}m.mCustomScrollbar("update");if(e.advanced.updateOnBrowserResize){var i,j=c(window).width(),u=c(window).height();c(window).bind("resize."+m.data("mCustomScrollbarIndex"),function(){if(i){clearTimeout(i)}i=setTimeout(function(){if(!m.is(".mCS_disabled")&&!m.is(".mCS_destroyed")){var w=c(window).width(),v=c(window).height();if(j!==w||u!==v){if(m.css("max-height")!=="none"&&s){g.css("max-height",m.parent().height()*s/100)}m.mCustomScrollbar("update");j=w;u=v}}},150)})}if(e.advanced.updateOnContentResize){var p;if(e.horizontalScroll){var n=o.outerWidth()}else{var n=o.outerHeight()}p=setInterval(function(){if(e.horizontalScroll){if(e.advanced.autoExpandHorizontalScroll){o.css({position:"absolute",width:"auto"}).wrap("<div class='mCSB_h_wrapper' style='position:relative; left:0; width:999999px;' />").css({width:o.outerWidth(),position:"relative"}).unwrap()}var v=o.outerWidth()}else{var v=o.outerHeight()}if(v!=n){m.mCustomScrollbar("update");n=v}},300)}})},update:function(){var n=c(this),k=n.children(".mCustomScrollBox"),q=k.children(".mCSB_container");q.removeClass("mCS_no_scrollbar");n.removeClass("mCS_disabled mCS_destroyed");k.scrollTop(0).scrollLeft(0);var y=k.children(".mCSB_scrollTools"),o=y.children(".mCSB_draggerContainer"),m=o.children(".mCSB_dragger");if(n.data("horizontalScroll")){var A=y.children(".mCSB_buttonLeft"),t=y.children(".mCSB_buttonRight"),f=k.width();if(n.data("autoExpandHorizontalScroll")){q.css({position:"absolute",width:"auto"}).wrap("<div class='mCSB_h_wrapper' style='position:relative; left:0; width:999999px;' />").css({width:q.outerWidth(),position:"relative"}).unwrap()}var z=q.outerWidth()}else{var w=y.children(".mCSB_buttonUp"),g=y.children(".mCSB_buttonDown"),r=k.height(),i=q.outerHeight()}if(i>r&&!n.data("horizontalScroll")){y.css("display","block");var s=o.height();if(n.data("autoDraggerLength")){var u=Math.round(r/i*s),l=m.data("minDraggerHeight");if(u<=l){m.css({height:l})}else{if(u>=s-10){var p=s-10;m.css({height:p})}else{m.css({height:u})}}m.children(".mCSB_dragger_bar").css({"line-height":m.height()+"px"})}var B=m.height(),x=(i-r)/(s-B);n.data("scrollAmount",x).mCustomScrollbar("scrolling",k,q,o,m,w,g,A,t);var D=Math.abs(q.position().top);n.mCustomScrollbar("scrollTo",D,{scrollInertia:0,trigger:"internal"})}else{if(z>f&&n.data("horizontalScroll")){y.css("display","block");var h=o.width();if(n.data("autoDraggerLength")){var j=Math.round(f/z*h),C=m.data("minDraggerWidth");if(j<=C){m.css({width:C})}else{if(j>=h-10){var e=h-10;m.css({width:e})}else{m.css({width:j})}}}var v=m.width(),x=(z-f)/(h-v);n.data("scrollAmount",x).mCustomScrollbar("scrolling",k,q,o,m,w,g,A,t);var D=Math.abs(q.position().left);n.mCustomScrollbar("scrollTo",D,{scrollInertia:0,trigger:"internal"})}else{k.unbind("mousewheel focusin");if(n.data("horizontalScroll")){m.add(q).css("left",0)}else{m.add(q).css("top",0)}if(n.data("alwaysShowScrollbar")){if(!n.data("horizontalScroll")){m.css({height:o.height()})}else{if(n.data("horizontalScroll")){m.css({width:o.width()})}}}else{y.css("display","none");q.addClass("mCS_no_scrollbar")}n.data({bindEvent_mousewheel:false,bindEvent_focusin:false})}}},scrolling:function(i,q,n,k,A,f,D,w){var l=c(this);if(!l.data("bindEvent_scrollbar_drag")){var o,p,C,z,e;if(c.support.pointer){C="pointerdown";z="pointermove";e="pointerup"}else{if(c.support.msPointer){C="MSPointerDown";z="MSPointerMove";e="MSPointerUp"}}if(c.support.pointer||c.support.msPointer){k.bind(C,function(K){K.preventDefault();l.data({on_drag:true});k.addClass("mCSB_dragger_onDrag");var J=c(this),M=J.offset(),I=K.originalEvent.pageX-M.left,L=K.originalEvent.pageY-M.top;if(I<J.width()&&I>0&&L<J.height()&&L>0){o=L;p=I}});c(document).bind(z+"."+l.data("mCustomScrollbarIndex"),function(K){K.preventDefault();if(l.data("on_drag")){var J=k,M=J.offset(),I=K.originalEvent.pageX-M.left,L=K.originalEvent.pageY-M.top;G(o,p,L,I)}}).bind(e+"."+l.data("mCustomScrollbarIndex"),function(x){l.data({on_drag:false});k.removeClass("mCSB_dragger_onDrag")})}else{k.bind("mousedown touchstart",function(K){K.preventDefault();K.stopImmediatePropagation();var J=c(this),N=J.offset(),I,M;if(K.type==="touchstart"){var L=K.originalEvent.touches[0]||K.originalEvent.changedTouches[0];I=L.pageX-N.left;M=L.pageY-N.top}else{l.data({on_drag:true});k.addClass("mCSB_dragger_onDrag");I=K.pageX-N.left;M=K.pageY-N.top}if(I<J.width()&&I>0&&M<J.height()&&M>0){o=M;p=I}}).bind("touchmove",function(K){K.preventDefault();K.stopImmediatePropagation();var N=K.originalEvent.touches[0]||K.originalEvent.changedTouches[0],J=c(this),M=J.offset(),I=N.pageX-M.left,L=N.pageY-M.top;G(o,p,L,I)});c(document).bind("mousemove."+l.data("mCustomScrollbarIndex"),function(K){if(l.data("on_drag")){var J=k,M=J.offset(),I=K.pageX-M.left,L=K.pageY-M.top;G(o,p,L,I)}}).bind("mouseup."+l.data("mCustomScrollbarIndex"),function(x){l.data({on_drag:false});k.removeClass("mCSB_dragger_onDrag")})}l.data({bindEvent_scrollbar_drag:true})}function G(J,K,L,I){if(l.data("horizontalScroll")){l.mCustomScrollbar("scrollTo",(k.position().left-(K))+I,{moveDragger:true,trigger:"internal"})}else{l.mCustomScrollbar("scrollTo",(k.position().top-(J))+L,{moveDragger:true,trigger:"internal"})}}if(c.support.touch&&l.data("contentTouchScroll")){if(!l.data("bindEvent_content_touch")){var m,E,s,t,v,F,H;q.bind("touchstart",function(x){x.stopImmediatePropagation();m=x.originalEvent.touches[0]||x.originalEvent.changedTouches[0];E=c(this);s=E.offset();v=m.pageX-s.left;t=m.pageY-s.top;F=t;H=v});q.bind("touchmove",function(x){x.preventDefault();x.stopImmediatePropagation();m=x.originalEvent.touches[0]||x.originalEvent.changedTouches[0];E=c(this).parent();s=E.offset();v=m.pageX-s.left;t=m.pageY-s.top;if(l.data("horizontalScroll")){l.mCustomScrollbar("scrollTo",H-v,{trigger:"internal"})}else{l.mCustomScrollbar("scrollTo",F-t,{trigger:"internal"})}})}}if(!l.data("bindEvent_scrollbar_click")){n.bind("click",function(I){var x=(I.pageY-n.offset().top)*l.data("scrollAmount"),y=c(I.target);if(l.data("horizontalScroll")){x=(I.pageX-n.offset().left)*l.data("scrollAmount")}if(y.hasClass("mCSB_draggerContainer")||y.hasClass("mCSB_draggerRail")){l.mCustomScrollbar("scrollTo",x,{trigger:"internal",scrollEasing:"draggerRailEase"})}});l.data({bindEvent_scrollbar_click:true})}if(l.data("mouseWheel")){if(!l.data("bindEvent_mousewheel")){i.bind("mousewheel",function(K,M){var J,I=l.data("mouseWheelPixels"),x=Math.abs(q.position().top),L=k.position().top,y=n.height()-k.height();if(l.data("normalizeMouseWheelDelta")){if(M<0){M=-1}else{M=1}}if(I==="auto"){I=100+Math.round(l.data("scrollAmount")/2)}if(l.data("horizontalScroll")){L=k.position().left;y=n.width()-k.width();x=Math.abs(q.position().left)}if((M>0&&L!==0)||(M<0&&L!==y)){K.preventDefault();K.stopImmediatePropagation()}J=x-(M*I);l.mCustomScrollbar("scrollTo",J,{trigger:"internal"})});l.data({bindEvent_mousewheel:true})}}if(l.data("scrollButtons_enable")){if(l.data("scrollButtons_scrollType")==="pixels"){if(l.data("horizontalScroll")){w.add(D).unbind("mousedown touchstart MSPointerDown pointerdown mouseup MSPointerUp pointerup mouseout MSPointerOut pointerout touchend",j,h);l.data({bindEvent_buttonsContinuous_x:false});if(!l.data("bindEvent_buttonsPixels_x")){w.bind("click",function(x){x.preventDefault();r(Math.abs(q.position().left)+l.data("scrollButtons_scrollAmount"))});D.bind("click",function(x){x.preventDefault();r(Math.abs(q.position().left)-l.data("scrollButtons_scrollAmount"))});l.data({bindEvent_buttonsPixels_x:true})}}else{f.add(A).unbind("mousedown touchstart MSPointerDown pointerdown mouseup MSPointerUp pointerup mouseout MSPointerOut pointerout touchend",j,h);l.data({bindEvent_buttonsContinuous_y:false});if(!l.data("bindEvent_buttonsPixels_y")){f.bind("click",function(x){x.preventDefault();r(Math.abs(q.position().top)+l.data("scrollButtons_scrollAmount"))});A.bind("click",function(x){x.preventDefault();r(Math.abs(q.position().top)-l.data("scrollButtons_scrollAmount"))});l.data({bindEvent_buttonsPixels_y:true})}}function r(x){if(!k.data("preventAction")){k.data("preventAction",true);l.mCustomScrollbar("scrollTo",x,{trigger:"internal"})}}}else{if(l.data("horizontalScroll")){w.add(D).unbind("click");l.data({bindEvent_buttonsPixels_x:false});if(!l.data("bindEvent_buttonsContinuous_x")){w.bind("mousedown touchstart MSPointerDown pointerdown",function(y){y.preventDefault();var x=B();l.data({mCSB_buttonScrollRight:setInterval(function(){l.mCustomScrollbar("scrollTo",Math.abs(q.position().left)+x,{trigger:"internal",scrollEasing:"easeOutCirc"})},17)})});var j=function(x){x.preventDefault();clearInterval(l.data("mCSB_buttonScrollRight"))};w.bind("mouseup touchend MSPointerUp pointerup mouseout MSPointerOut pointerout",j);D.bind("mousedown touchstart MSPointerDown pointerdown",function(y){y.preventDefault();var x=B();l.data({mCSB_buttonScrollLeft:setInterval(function(){l.mCustomScrollbar("scrollTo",Math.abs(q.position().left)-x,{trigger:"internal",scrollEasing:"easeOutCirc"})},17)})});var h=function(x){x.preventDefault();clearInterval(l.data("mCSB_buttonScrollLeft"))};D.bind("mouseup touchend MSPointerUp pointerup mouseout MSPointerOut pointerout",h);l.data({bindEvent_buttonsContinuous_x:true})}}else{f.add(A).unbind("click");l.data({bindEvent_buttonsPixels_y:false});if(!l.data("bindEvent_buttonsContinuous_y")){f.bind("mousedown touchstart MSPointerDown pointerdown",function(y){y.preventDefault();var x=B();l.data({mCSB_buttonScrollDown:setInterval(function(){l.mCustomScrollbar("scrollTo",Math.abs(q.position().top)+x,{trigger:"internal",scrollEasing:"easeOutCirc"})},17)})});var u=function(x){x.preventDefault();clearInterval(l.data("mCSB_buttonScrollDown"))};f.bind("mouseup touchend MSPointerUp pointerup mouseout MSPointerOut pointerout",u);A.bind("mousedown touchstart MSPointerDown pointerdown",function(y){y.preventDefault();var x=B();l.data({mCSB_buttonScrollUp:setInterval(function(){l.mCustomScrollbar("scrollTo",Math.abs(q.position().top)-x,{trigger:"internal",scrollEasing:"easeOutCirc"})},17)})});var g=function(x){x.preventDefault();clearInterval(l.data("mCSB_buttonScrollUp"))};A.bind("mouseup touchend MSPointerUp pointerup mouseout MSPointerOut pointerout",g);l.data({bindEvent_buttonsContinuous_y:true})}}function B(){var x=l.data("scrollButtons_scrollSpeed");if(l.data("scrollButtons_scrollSpeed")==="auto"){x=Math.round((l.data("scrollInertia")+100)/40)}return x}}}if(l.data("autoScrollOnFocus")){if(!l.data("bindEvent_focusin")){i.bind("focusin",function(){i.scrollTop(0).scrollLeft(0);var x=c(document.activeElement);if(x.is("input,textarea,select,button,a[tabindex],area,object")){var J=q.position().top,y=x.position().top,I=i.height()-x.outerHeight();if(l.data("horizontalScroll")){J=q.position().left;y=x.position().left;I=i.width()-x.outerWidth()}if(J+y<0||J+y>I){l.mCustomScrollbar("scrollTo",y,{trigger:"internal"})}}});l.data({bindEvent_focusin:true})}}if(l.data("autoHideScrollbar")&&!l.data("alwaysShowScrollbar")){if(!l.data("bindEvent_autoHideScrollbar")){i.bind("mouseenter",function(x){i.addClass("mCS-mouse-over");d.showScrollbar.call(i.children(".mCSB_scrollTools"))}).bind("mouseleave touchend",function(x){i.removeClass("mCS-mouse-over");if(x.type==="mouseleave"){d.hideScrollbar.call(i.children(".mCSB_scrollTools"))}});l.data({bindEvent_autoHideScrollbar:true})}}},scrollTo:function(e,f){var i=c(this),o={moveDragger:false,trigger:"external",callbacks:true,scrollInertia:i.data("scrollInertia"),scrollEasing:i.data("scrollEasing")},f=c.extend(o,f),p,g=i.children(".mCustomScrollBox"),k=g.children(".mCSB_container"),r=g.children(".mCSB_scrollTools"),j=r.children(".mCSB_draggerContainer"),h=j.children(".mCSB_dragger"),t=draggerSpeed=f.scrollInertia,q,s,m,l;if(!k.hasClass("mCS_no_scrollbar")){i.data({mCS_trigger:f.trigger});if(i.data("mCS_Init")){f.callbacks=false}if(e||e===0){if(typeof(e)==="number"){if(f.moveDragger){p=e;if(i.data("horizontalScroll")){e=h.position().left*i.data("scrollAmount")}else{e=h.position().top*i.data("scrollAmount")}draggerSpeed=0}else{p=e/i.data("scrollAmount")}}else{if(typeof(e)==="string"){var v;if(e==="top"){v=0}else{if(e==="bottom"&&!i.data("horizontalScroll")){v=k.outerHeight()-g.height()}else{if(e==="left"){v=0}else{if(e==="right"&&i.data("horizontalScroll")){v=k.outerWidth()-g.width()}else{if(e==="first"){v=i.find(".mCSB_container").find(":first")}else{if(e==="last"){v=i.find(".mCSB_container").find(":last")}else{v=i.find(e)}}}}}}if(v.length===1){if(i.data("horizontalScroll")){e=v.position().left}else{e=v.position().top}p=e/i.data("scrollAmount")}else{p=e=v}}}if(i.data("horizontalScroll")){if(i.data("onTotalScrollBack_Offset")){s=-i.data("onTotalScrollBack_Offset")}if(i.data("onTotalScroll_Offset")){l=g.width()-k.outerWidth()+i.data("onTotalScroll_Offset")}if(p<0){p=e=0;clearInterval(i.data("mCSB_buttonScrollLeft"));if(!s){q=true}}else{if(p>=j.width()-h.width()){p=j.width()-h.width();e=g.width()-k.outerWidth();clearInterval(i.data("mCSB_buttonScrollRight"));if(!l){m=true}}else{e=-e}}var n=i.data("snapAmount");if(n){e=Math.round(e/n)*n-i.data("snapOffset")}d.mTweenAxis.call(this,h[0],"left",Math.round(p),draggerSpeed,f.scrollEasing);d.mTweenAxis.call(this,k[0],"left",Math.round(e),t,f.scrollEasing,{onStart:function(){if(f.callbacks&&!i.data("mCS_tweenRunning")){u("onScrollStart")}if(i.data("autoHideScrollbar")&&!i.data("alwaysShowScrollbar")){d.showScrollbar.call(r)}},onUpdate:function(){if(f.callbacks){u("whileScrolling")}},onComplete:function(){if(f.callbacks){u("onScroll");if(q||(s&&k.position().left>=s)){u("onTotalScrollBack")}if(m||(l&&k.position().left<=l)){u("onTotalScroll")}}h.data("preventAction",false);i.data("mCS_tweenRunning",false);if(i.data("autoHideScrollbar")&&!i.data("alwaysShowScrollbar")){if(!g.hasClass("mCS-mouse-over")){d.hideScrollbar.call(r)}}}})}else{if(i.data("onTotalScrollBack_Offset")){s=-i.data("onTotalScrollBack_Offset")}if(i.data("onTotalScroll_Offset")){l=g.height()-k.outerHeight()+i.data("onTotalScroll_Offset")}if(p<0){p=e=0;clearInterval(i.data("mCSB_buttonScrollUp"));if(!s){q=true}}else{if(p>=j.height()-h.height()){p=j.height()-h.height();e=g.height()-k.outerHeight();clearInterval(i.data("mCSB_buttonScrollDown"));if(!l){m=true}}else{e=-e}}var n=i.data("snapAmount");if(n){e=Math.round(e/n)*n-i.data("snapOffset")}d.mTweenAxis.call(this,h[0],"top",Math.round(p),draggerSpeed,f.scrollEasing);d.mTweenAxis.call(this,k[0],"top",Math.round(e),t,f.scrollEasing,{onStart:function(){if(f.callbacks&&!i.data("mCS_tweenRunning")){u("onScrollStart")}if(i.data("autoHideScrollbar")&&!i.data("alwaysShowScrollbar")){d.showScrollbar.call(r)}},onUpdate:function(){if(f.callbacks){u("whileScrolling")}},onComplete:function(){if(f.callbacks){u("onScroll");if(q||(s&&k.position().top>=s)){u("onTotalScrollBack")}if(m||(l&&k.position().top<=l)){u("onTotalScroll")}}h.data("preventAction",false);i.data("mCS_tweenRunning",false);if(i.data("autoHideScrollbar")&&!i.data("alwaysShowScrollbar")){if(!g.hasClass("mCS-mouse-over")){d.hideScrollbar.call(r)}}}})}if(i.data("mCS_Init")){i.data({mCS_Init:false})}}}function u(w){if(i.data("mCustomScrollbarIndex")){this.mcs={top:k.position().top,left:k.position().left,draggerTop:h.position().top,draggerLeft:h.position().left,topPct:Math.round((100*Math.abs(k.position().top))/Math.abs(k.outerHeight()-g.height())),leftPct:Math.round((100*Math.abs(k.position().left))/Math.abs(k.outerWidth()-g.width()))};switch(w){case"onScrollStart":i.data("mCS_tweenRunning",true).data("onScrollStart_Callback").call(i,this.mcs);break;case"whileScrolling":i.data("whileScrolling_Callback").call(i,this.mcs);break;case"onScroll":i.data("onScroll_Callback").call(i,this.mcs);break;case"onTotalScrollBack":i.data("onTotalScrollBack_Callback").call(i,this.mcs);break;case"onTotalScroll":i.data("onTotalScroll_Callback").call(i,this.mcs);break}}}},stop:function(){var g=c(this),e=g.children().children(".mCSB_container"),f=g.children().children().children().children(".mCSB_dragger");d.mTweenAxisStop.call(this,e[0]);d.mTweenAxisStop.call(this,f[0])},disable:function(e){var j=c(this),f=j.children(".mCustomScrollBox"),h=f.children(".mCSB_container"),g=f.children(".mCSB_scrollTools"),i=g.children().children(".mCSB_dragger");f.unbind("mousewheel focusin mouseenter mouseleave touchend");h.unbind("touchstart touchmove");if(e){if(j.data("horizontalScroll")){i.add(h).css("left",0)}else{i.add(h).css("top",0)}}g.css("display","none");h.addClass("mCS_no_scrollbar");j.data({bindEvent_mousewheel:false,bindEvent_focusin:false,bindEvent_content_touch:false,bindEvent_autoHideScrollbar:false}).addClass("mCS_disabled")},destroy:function(){var e=c(this);e.removeClass("mCustomScrollbar _mCS_"+e.data("mCustomScrollbarIndex")).addClass("mCS_destroyed").children().children(".mCSB_container").unwrap().children().unwrap().siblings(".mCSB_scrollTools").remove();c(document).unbind("mousemove."+e.data("mCustomScrollbarIndex")+" mouseup."+e.data("mCustomScrollbarIndex")+" MSPointerMove."+e.data("mCustomScrollbarIndex")+" MSPointerUp."+e.data("mCustomScrollbarIndex"));c(window).unbind("resize."+e.data("mCustomScrollbarIndex"))}},d={showScrollbar:function(){this.stop().animate({opacity:1},"fast")},hideScrollbar:function(){this.stop().animate({opacity:0},"fast")},mTweenAxis:function(g,i,h,f,o,y){var y=y||{},v=y.onStart||function(){},p=y.onUpdate||function(){},w=y.onComplete||function(){};var n=t(),l,j=0,r=g.offsetTop,s=g.style;if(i==="left"){r=g.offsetLeft}var m=h-r;q();e();function t(){if(window.performance&&window.performance.now){return window.performance.now()}else{if(window.performance&&window.performance.webkitNow){return window.performance.webkitNow()}else{if(Date.now){return Date.now()}else{return new Date().getTime()}}}}function x(){if(!j){v.call()}j=t()-n;u();if(j>=g._time){g._time=(j>g._time)?j+l-(j-g._time):j+l-1;if(g._time<j+1){g._time=j+1}}if(g._time<f){g._id=_request(x)}else{w.call()}}function u(){if(f>0){g.currVal=k(g._time,r,m,f,o);s[i]=Math.round(g.currVal)+"px"}else{s[i]=h+"px"}p.call()}function e(){l=1000/60;g._time=j+l;_request=(!window.requestAnimationFrame)?function(z){u();return setTimeout(z,0.01)}:window.requestAnimationFrame;g._id=_request(x)}function q(){if(g._id==null){return}if(!window.requestAnimationFrame){clearTimeout(g._id)}else{window.cancelAnimationFrame(g._id)}g._id=null}function k(B,A,F,E,C){switch(C){case"linear":return F*B/E+A;break;case"easeOutQuad":B/=E;return -F*B*(B-2)+A;break;case"easeInOutQuad":B/=E/2;if(B<1){return F/2*B*B+A}B--;return -F/2*(B*(B-2)-1)+A;break;case"easeOutCubic":B/=E;B--;return F*(B*B*B+1)+A;break;case"easeOutQuart":B/=E;B--;return -F*(B*B*B*B-1)+A;break;case"easeOutQuint":B/=E;B--;return F*(B*B*B*B*B+1)+A;break;case"easeOutCirc":B/=E;B--;return F*Math.sqrt(1-B*B)+A;break;case"easeOutSine":return F*Math.sin(B/E*(Math.PI/2))+A;break;case"easeOutExpo":return F*(-Math.pow(2,-10*B/E)+1)+A;break;case"mcsEaseOut":var D=(B/=E)*B,z=D*B;return A+F*(0.499999999999997*z*D+-2.5*D*D+5.5*z+-6.5*D+4*B);break;case"draggerRailEase":B/=E/2;if(B<1){return F/2*B*B*B+A}B-=2;return F/2*(B*B*B+2)+A;break}}},mTweenAxisStop:function(e){if(e._id==null){return}if(!window.requestAnimationFrame){clearTimeout(e._id)}else{window.cancelAnimationFrame(e._id)}e._id=null},rafPolyfill:function(){var f=["ms","moz","webkit","o"],e=f.length;while(--e>-1&&!window.requestAnimationFrame){window.requestAnimationFrame=window[f[e]+"RequestAnimationFrame"];window.cancelAnimationFrame=window[f[e]+"CancelAnimationFrame"]||window[f[e]+"CancelRequestAnimationFrame"]}}};d.rafPolyfill.call();c.support.touch=!!("ontouchstart" in window);c.support.pointer=window.navigator.pointerEnabled;c.support.msPointer=window.navigator.msPointerEnabled;var a=("https:"==document.location.protocol)?"https:":"http:";c.event.special.mousewheel||document.write('<script src="'+a+'//cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.0.6/jquery.mousewheel.min.js"><\/script>');c.fn.mCustomScrollbar=function(e){if(b[e]){return b[e].apply(this,Array.prototype.slice.call(arguments,1))}else{if(typeof e==="object"||!e){return b.init.apply(this,arguments)}else{c.error("Method "+e+" does not exist")}}}})(jQuery);

	
		
		    var videos_section_slider = (function() {
            var _slideShowElementSelector = '[data-plugin="videos_section_slider"]';
        
            function rightEndReached($slider, $outerWidthEl, totalInnerWidth) {
                if ((Math.abs(parseFloat($slider.css("margin-left"))) + $outerWidthEl.outerWidth()) < totalInnerWidth) {
                    return false;
                }
                return true;
            }
        
            function leftEndReached($slider) {
                if (Math.abs(parseInt($slider.css("margin-left"))) === 0) {
                    return true;
                }
                return false;
            }
            
            function getPartialSlideWidth($slider, $outerWidthEl, totalInnerWidth, imageWidth){
                var remainingSlidesWidth = totalInnerWidth - (Math.abs(parseFloat($slider.css("margin-left"))) + $outerWidthEl.outerWidth());
                
                return remainingSlidesWidth < imageWidth ? remainingSlidesWidth : 0;
            }
            
            function loadNextData($slider, url, success, failure){
                $.ajax({
                    "url": url,
                    "success": function(response){
                        var $html = $('<div>');
                            
                        $html.html(response);
                        
                        $slider.append($html.find('li'));
                        if(typeof success == 'function'){
                            success();
                        }
                    },
                    "error": function(){
                        if(typeof failure == 'function'){
                            failure();
                        }
                    }
                });
            }
        
            return {
                init: function($slideShowEl, nextButton, prevButton) {
                        var currentSlide = 0,
                            sliding = false,
                            //$slideShowEl = $(this),
                            $prevButton = prevButton ? prevButton: $('<span class="btn prev disabled"><i></i></span>'),
                            $nextButton = nextButton ? nextButton : $('<span class="btn next disabled"><i></i></span>'),
                            $slider = $slideShowEl.find('ul'),
                            $slides = $slideShowEl.find("li"),
                            $outerWidthEl = $slider.closest('.slideshowbox'),
                            totalImages = $slides.length,
                            imageWidth = $slides.eq(0).outerWidth(true) * Math.floor(($slideShowEl.outerWidth() / $slides.eq(0).outerWidth(true))),
                            totalInnerWidth = 0,
                            updatedTotalInnerWidth = 0,
                            partialSlidedValue = 0,
                            slideShowActive = false,
                            nextDataLoaded = false,
                            nextDataLoading = false;
                            
                        function handleResize(e, source){
                            //$slider.width('');
                            if(source === 'charts'){
                                return;
                            }
                            //console.log("resize :: sliding :: " + sliding);
                            if(sliding){
                                return;
                            }
                            //console.log("resize :: slideShowActive :: " + slideShowActive);
                            if(slideShowActive){
                                sliding = true;
                                $prevButton.addClass("disabled");
                                $slider.animate({
                                    "margin-left": "0px"
                                }, 500, function() {
                                    sliding = false;
                                    currentSlide = 0;
                                    partialSlidedValue = 0;
                                    if ($slider.outerWidth(true) >= totalInnerWidth) {
                                        //console.log("resize :: after :: slideShowActive :: " + false);
                                        $nextButton.addClass('disabled');
                                        slideShowActive = false;
                                    } else {
                                        slideShowActive = true;
                                        //$slider.width(totalInnerWidth);
                                        $nextButton.removeClass('disabled');
                                    }
                                });
                            }
                            else{
                                if ($slider.outerWidth(true) >= totalInnerWidth) {
                                    //console.log("resize :: else :: after :: slideShowActive :: " + false);
                                    $nextButton.addClass('disabled');
                                    slideShowActive = false;
                                } else {
                                    slideShowActive = true;
                                    //$slider.width(totalInnerWidth);
                                    $nextButton.removeClass('disabled');
                                }
                            }
                            if( $slideShowEl.outerWidth() > 0 
                                && $slides.eq(0).outerWidth(true) > 0
                            ){
                                imageWidth = $slides.eq(0).outerWidth(true) * Math.floor(($slideShowEl.outerWidth() / $slides.eq(0).outerWidth(true)));
                            }
                            
                        }
                        
                        //slideshow was initialized already , if  totalinnnerwidth is changed, handle like resize, else do nothing  
                        if($slideShowEl.find('.npbtn.next').length){
                            updatedTotalInnerWidth = 0;
                            $slides.each(function() {
                                updatedTotalInnerWidth += $(this).outerWidth(true);
                            });
                            //rounding to 1 decimal place
                            updatedTotalInnerWidth = Math.round(updatedTotalInnerWidth * 10) / 10;
                            if(updatedTotalInnerWidth === totalInnerWidth){
                                return;
                            }
                            else{
                                handleResize();
                                return;
                            }
                        }
                        
                        
                    
                        
                        //on load checks and settings
                        $slides.each(function() {
                            totalInnerWidth += $(this).outerWidth(true);
                            //rounding to 1 decimal place
                            totalInnerWidth = Math.round(totalInnerWidth * 10) / 10;
                        });
                        //console.log("slide width on load :: "+ $slider.outerWidth(true));
                        //console.log("totalInnerWidth on load :: "+ totalInnerWidth);
                        if (totalInnerWidth > $slider.outerWidth(true)) {
                            if( !prevButton ){
                                $slideShowEl.append($prevButton);
                            }
                            if( !nextButton ){
                                $slideShowEl.append($nextButton);
                            }
                            slideShowActive = true;
                            $nextButton.removeClass('disabled');
                        }
                        
                        //$(window).on('resize', handleResize);
                        require(["tiljs/event"], function(eventBus){
                            eventBus.subscribe("window.resize", handleResize);
                        })
        
                        $nextButton.on('click', function() {
                            var partialSlideWidth = getPartialSlideWidth($slider, $outerWidthEl, totalInnerWidth, imageWidth),
                                slideWidth = imageWidth;
                            //console.log("next :: sliding :: " + sliding + " :: rightEndReached ::  " + rightEndReached($slider, $outerWidthEl, totalInnerWidth));
                            if (nextDataLoading || sliding || rightEndReached($slider, $outerWidthEl, totalInnerWidth)) {
                                return;
                            }
        
                            if ($prevButton.hasClass("disabled")) {
                                $prevButton.removeClass("disabled");
                            }
                            //console.log("next :: partialSlideWidth :: " + partialSlideWidth);
                            if(partialSlideWidth > 0){
                                slideWidth = partialSlideWidth;
                                partialSlidedValue = partialSlideWidth;
                            }
                            //console.log("next :: slideWidth :: " + slideWidth);
                            //load next data, if required
                            if(!nextDataLoaded && $slideShowEl.data('next-url')){
                                nextDataLoading = true;
                                $nextButton.addClass('loader');
                                loadNextData($slider, $slideShowEl.data('next-url'), function(){
                                    totalInnerWidth = 0;
                                    $slides = $slider.find('li');
                                    $slides.each(function() {
                                        totalInnerWidth += $(this).outerWidth(true);
                                    });
                                    //rounding to 1 decimal place
                                    totalInnerWidth = Math.round(totalInnerWidth * 10) / 10;
                                    nextDataLoaded = true;
                                    nextDataLoading = false;
                                    $nextButton.removeClass('loader disabled');
                                    require( ['tiljs/plugin/lazy', 'toicommonjs/rodate'], function(lazy, rodate){
                                        lazy.load();
                                        rodate.uptime();
                                        //WatchLaterManger is initialized on window load
                                        if(TimesApps.WatchLaterManager.getInitStatus()){
                                            TimesApps.WatchLaterManager.updateStatus($slideShowEl);
                                        }
                                        else{
                                            $(window).load(function(){
                                        	    TimesApps.WatchLaterManager.updateStatus($slideShowEl);
                                            });
                                        }
                                    });
                                    
                                }, function(){
                                    nextDataLoading = false;
                                });
                            }
                            
                            sliding = true;
                            $slider.animate({
                                "margin-left": parseFloat($slider.css("margin-left")) - (slideWidth) + "px"
                            }, 500, function() {
                                sliding = false;
                                currentSlide += 1;
                                //console.log("next :: after :: rightEndReached :: " + rightEndReached($slider, $outerWidthEl, totalInnerWidth));
                                if (rightEndReached($slider, $outerWidthEl, totalInnerWidth)) {
                                    $nextButton.addClass("disabled");
                                }
                            });
                        });
        
                        $prevButton.on('click', function() {
                            var slideWidth = imageWidth,
                                finalMargin;
                            //console.log("prev :: sliding :: " + sliding + " :: leftEndReached ::  " + leftEndReached($slider));
                            if (nextDataLoading || sliding || leftEndReached($slider)) {
                                //console.log('prev returned');
                                return;
                            }
                            //console.log('prev continued');
                            if ($nextButton.hasClass("disabled")) {
                                $nextButton.removeClass("disabled");
                            }
                            
                            sliding = true;
                            //console.log("prev :: partialSlidedValue :: " + partialSlidedValue);
                            if(partialSlidedValue > 0){
                                slideWidth = partialSlidedValue;
                            }
                            //console.log("prev :: slideWidth :: " + slideWidth);
                            finalMargin = parseFloat($slider.css("margin-left")) + (slideWidth);
                            
                            // finalMargin > 0 -- do not go further than the left limit
                            // Math.abs(finalMargin) < 1 -- set as 0 for 0.x values
                            if(finalMargin > 0 || Math.abs(finalMargin) < 1){
                                finalMargin = 0;
                            }
                            
                            $slider.animate({
                                "margin-left": finalMargin + "px"
                            }, 500, function() {
                                sliding = false;
                                currentSlide -= 1;
                                if(partialSlidedValue > 0){
                                    partialSlidedValue = 0;
                                }
                                //console.log("prev :: after :: leftEndReached :: " + leftEndReached($slider));
                                if (leftEndReached($slider)) {
                                    $prevButton.addClass("disabled");
                                }
                            });
                        });
                }
            }
        })();
	    
	    $(function() {
            $('[data-plugin="videos_section_slider"]').each(function() {
                videos_section_slider.init($(this));
            });
        });
		
	
/**
 * @license almond 0.3.3 Copyright jQuery Foundation and other contributors.
 * Released under MIT license, http://github.com/requirejs/almond/LICENSE
 */
//Going sloppy to avoid 'use strict' string cost, but strict practices should
//be followed.
/*global setTimeout: false */

var requirejs, require, define;
(function (undef) {
    var main, req, makeMap, handlers,
        defined = {},
        waiting = {},
        config = {},
        defining = {},
        hasOwn = Object.prototype.hasOwnProperty,
        aps = [].slice,
        jsSuffixRegExp = /\.js$/;

    function hasProp(obj, prop) {
        return hasOwn.call(obj, prop);
    }

    /**
     * Given a relative module name, like ./something, normalize it to
     * a real name that can be mapped to a path.
     * @param {String} name the relative name
     * @param {String} baseName a real name that the name arg is relative
     * to.
     * @returns {String} normalized name
     */
    function normalize(name, baseName) {
        var nameParts, nameSegment, mapValue, foundMap, lastIndex,
            foundI, foundStarMap, starI, i, j, part, normalizedBaseParts,
            baseParts = baseName && baseName.split("/"),
            map = config.map,
            starMap = (map && map['*']) || {};

        //Adjust any relative paths.
        if (name) {
            name = name.split('/');
            lastIndex = name.length - 1;

            // If wanting node ID compatibility, strip .js from end
            // of IDs. Have to do this here, and not in nameToUrl
            // because node allows either .js or non .js to map
            // to same file.
            if (config.nodeIdCompat && jsSuffixRegExp.test(name[lastIndex])) {
                name[lastIndex] = name[lastIndex].replace(jsSuffixRegExp, '');
            }

            // Starts with a '.' so need the baseName
            if (name[0].charAt(0) === '.' && baseParts) {
                //Convert baseName to array, and lop off the last part,
                //so that . matches that 'directory' and not name of the baseName's
                //module. For instance, baseName of 'one/two/three', maps to
                //'one/two/three.js', but we want the directory, 'one/two' for
                //this normalization.
                normalizedBaseParts = baseParts.slice(0, baseParts.length - 1);
                name = normalizedBaseParts.concat(name);
            }

            //start trimDots
            for (i = 0; i < name.length; i++) {
                part = name[i];
                if (part === '.') {
                    name.splice(i, 1);
                    i -= 1;
                } else if (part === '..') {
                    // If at the start, or previous value is still ..,
                    // keep them so that when converted to a path it may
                    // still work when converted to a path, even though
                    // as an ID it is less than ideal. In larger point
                    // releases, may be better to just kick out an error.
                    if (i === 0 || (i === 1 && name[2] === '..') || name[i - 1] === '..') {
                        continue;
                    } else if (i > 0) {
                        name.splice(i - 1, 2);
                        i -= 2;
                    }
                }
            }
            //end trimDots

            name = name.join('/');
        }

        //Apply map config if available.
        if ((baseParts || starMap) && map) {
            nameParts = name.split('/');

            for (i = nameParts.length; i > 0; i -= 1) {
                nameSegment = nameParts.slice(0, i).join("/");

                if (baseParts) {
                    //Find the longest baseName segment match in the config.
                    //So, do joins on the biggest to smallest lengths of baseParts.
                    for (j = baseParts.length; j > 0; j -= 1) {
                        mapValue = map[baseParts.slice(0, j).join('/')];

                        //baseName segment has  config, find if it has one for
                        //this name.
                        if (mapValue) {
                            mapValue = mapValue[nameSegment];
                            if (mapValue) {
                                //Match, update name to the new value.
                                foundMap = mapValue;
                                foundI = i;
                                break;
                            }
                        }
                    }
                }

                if (foundMap) {
                    break;
                }

                //Check for a star map match, but just hold on to it,
                //if there is a shorter segment match later in a matching
                //config, then favor over this star map.
                if (!foundStarMap && starMap && starMap[nameSegment]) {
                    foundStarMap = starMap[nameSegment];
                    starI = i;
                }
            }

            if (!foundMap && foundStarMap) {
                foundMap = foundStarMap;
                foundI = starI;
            }

            if (foundMap) {
                nameParts.splice(0, foundI, foundMap);
                name = nameParts.join('/');
            }
        }

        return name;
    }

    function makeRequire(relName, forceSync) {
        return function () {
            //A version of a require function that passes a moduleName
            //value for items that may need to
            //look up paths relative to the moduleName
            var args = aps.call(arguments, 0);

            //If first arg is not require('string'), and there is only
            //one arg, it is the array form without a callback. Insert
            //a null so that the following concat is correct.
            if (typeof args[0] !== 'string' && args.length === 1) {
                args.push(null);
            }
            return req.apply(undef, args.concat([relName, forceSync]));
        };
    }

    function makeNormalize(relName) {
        return function (name) {
            return normalize(name, relName);
        };
    }

    function makeLoad(depName) {
        return function (value) {
            defined[depName] = value;
        };
    }

    function callDep(name) {
        if (hasProp(waiting, name)) {
            var args = waiting[name];
            delete waiting[name];
            defining[name] = true;
            main.apply(undef, args);
        }

        if (!hasProp(defined, name) && !hasProp(defining, name)) {
            throw new Error('No ' + name);
        }
        return defined[name];
    }

    //Turns a plugin!resource to [plugin, resource]
    //with the plugin being undefined if the name
    //did not have a plugin prefix.
    function splitPrefix(name) {
        var prefix,
            index = name ? name.indexOf('!') : -1;
        if (index > -1) {
            prefix = name.substring(0, index);
            name = name.substring(index + 1, name.length);
        }
        return [prefix, name];
    }

    //Creates a parts array for a relName where first part is plugin ID,
    //second part is resource ID. Assumes relName has already been normalized.
    function makeRelParts(relName) {
        return relName ? splitPrefix(relName) : [];
    }

    /**
     * Makes a name map, normalizing the name, and using a plugin
     * for normalization if necessary. Grabs a ref to plugin
     * too, as an optimization.
     */
    makeMap = function (name, relParts) {
        var plugin,
            parts = splitPrefix(name),
            prefix = parts[0],
            relResourceName = relParts[1];

        name = parts[1];

        if (prefix) {
            prefix = normalize(prefix, relResourceName);
            plugin = callDep(prefix);
        }

        //Normalize according
        if (prefix) {
            if (plugin && plugin.normalize) {
                name = plugin.normalize(name, makeNormalize(relResourceName));
            } else {
                name = normalize(name, relResourceName);
            }
        } else {
            name = normalize(name, relResourceName);
            parts = splitPrefix(name);
            prefix = parts[0];
            name = parts[1];
            if (prefix) {
                plugin = callDep(prefix);
            }
        }

        //Using ridiculous property names for space reasons
        return {
            f: prefix ? prefix + '!' + name : name, //fullName
            n: name,
            pr: prefix,
            p: plugin
        };
    };

    function makeConfig(name) {
        return function () {
            return (config && config.config && config.config[name]) || {};
        };
    }

    handlers = {
        require: function (name) {
            return makeRequire(name);
        },
        exports: function (name) {
            var e = defined[name];
            if (typeof e !== 'undefined') {
                return e;
            } else {
                return (defined[name] = {});
            }
        },
        module: function (name) {
            return {
                id: name,
                uri: '',
                exports: defined[name],
                config: makeConfig(name)
            };
        }
    };

    main = function (name, deps, callback, relName) {
        var cjsModule, depName, ret, map, i, relParts,
            args = [],
            callbackType = typeof callback,
            usingExports;

        //Use name if no relName
        relName = relName || name;
        relParts = makeRelParts(relName);

        //Call the callback to define the module, if necessary.
        if (callbackType === 'undefined' || callbackType === 'function') {
            //Pull out the defined dependencies and pass the ordered
            //values to the callback.
            //Default to [require, exports, module] if no deps
            deps = !deps.length && callback.length ? ['require', 'exports', 'module'] : deps;
            for (i = 0; i < deps.length; i += 1) {
                map = makeMap(deps[i], relParts);
                depName = map.f;

                //Fast path CommonJS standard dependencies.
                if (depName === "require") {
                    args[i] = handlers.require(name);
                } else if (depName === "exports") {
                    //CommonJS module spec 1.1
                    args[i] = handlers.exports(name);
                    usingExports = true;
                } else if (depName === "module") {
                    //CommonJS module spec 1.1
                    cjsModule = args[i] = handlers.module(name);
                } else if (hasProp(defined, depName) ||
                           hasProp(waiting, depName) ||
                           hasProp(defining, depName)) {
                    args[i] = callDep(depName);
                } else if (map.p) {
                    map.p.load(map.n, makeRequire(relName, true), makeLoad(depName), {});
                    args[i] = defined[depName];
                } else {
                    throw new Error(name + ' missing ' + depName);
                }
            }

            ret = callback ? callback.apply(defined[name], args) : undefined;

            if (name) {
                //If setting exports via "module" is in play,
                //favor that over return value and exports. After that,
                //favor a non-undefined return value over exports use.
                if (cjsModule && cjsModule.exports !== undef &&
                        cjsModule.exports !== defined[name]) {
                    defined[name] = cjsModule.exports;
                } else if (ret !== undef || !usingExports) {
                    //Use the return value from the function.
                    defined[name] = ret;
                }
            }
        } else if (name) {
            //May just be an object definition for the module. Only
            //worry about defining if have a module name.
            defined[name] = callback;
        }
    };

    requirejs = require = req = function (deps, callback, relName, forceSync, alt) {
        if (typeof deps === "string") {
            if (handlers[deps]) {
                //callback in this case is really relName
                return handlers[deps](callback);
            }
            //Just return the module wanted. In this scenario, the
            //deps arg is the module name, and second arg (if passed)
            //is just the relName.
            //Normalize module name, if it contains . or ..
            return callDep(makeMap(deps, makeRelParts(callback)).f);
        } else if (!deps.splice) {
            //deps is a config object, not an array.
            config = deps;
            if (config.deps) {
                req(config.deps, config.callback);
            }
            if (!callback) {
                return;
            }

            if (callback.splice) {
                //callback is an array, which means it is a dependency list.
                //Adjust args if there are dependencies
                deps = callback;
                callback = relName;
                relName = null;
            } else {
                deps = undef;
            }
        }

        //Support require(['a'])
        callback = callback || function () {};

        //If relName is a function, it is an errback handler,
        //so remove it.
        if (typeof relName === 'function') {
            relName = forceSync;
            forceSync = alt;
        }

        //Simulate async callback;
        if (forceSync) {
            main(undef, deps, callback, relName);
        } else {
            //Using a non-zero value because of concern for what old browsers
            //do, and latest browsers "upgrade" to 4 if lower value is used:
            //http://www.whatwg.org/specs/web-apps/current-work/multipage/timers.html#dom-windowtimers-settimeout:
            //If want a value immediately, use require('id') instead -- something
            //that works in almond on the global level, but not guaranteed and
            //unlikely to work in other AMD implementations.
            setTimeout(function () {
                main(undef, deps, callback, relName);
            }, 4);
        }

        return req;
    };

    /**
     * Just drops the config on the floor, but returns req in case
     * the config return value is used.
     */
    req.config = function (cfg) {
        return req(cfg);
    };

    /**
     * Expose module registry for debugging and tooling
     */
    requirejs._defined = defined;

    define = function (name, deps, callback) {
        if (typeof name !== 'string') {
            throw new Error('See almond README: incorrect module build, no module name');
        }

        //This module may not have dependencies
        if (!deps.splice) {
            //deps is not an array, so probably means
            //an object literal or factory function for
            //the value. Adjust args.
            callback = deps;
            deps = [];
        }

        if (!hasProp(defined, name) && !hasProp(waiting, name)) {
            waiting[name] = [name, deps, callback];
        }
    };

    define.amd = {
        jQuery: true
    };
}());

( function () {
	var shim = {};
	var paths = {};
	var deps = [];
	var min = ".min";
	//    var min = "";
	var preload = [
		{
			module: "json",
			variable: "JSON",
			js: "//cdnjs.cloudflare.com/ajax/libs/json2/20121008/json2" + min
		}
		,
		{
			module: "jquery",
			variable: "jQuery",
			js: [ "//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery" + min
				, "//cdnjs.cloudflare.com/ajax/libs/jquery/1.8.2/jquery" + min ]
		}
	];
	for( var i = 0; i < preload.length; i++ ) {
		var pre = preload[ i ];
		if( !window[ pre.variable ] ) {
			paths[ pre.module ] = pre.js;
			shim[ pre.module ] = {
				"exports": pre.variable
			};
			deps.push( pre.module );
		} else {
			define( pre.module, ( function ( pre, min ) {
				return function () {
					return window[ pre.variable ]
				};
			}( pre, min ) ) );
		}
	}
	require( {
		deps: deps,
		shim: shim,
		paths: paths
	} );
}() );
define( 'config',[], function () {return {};});
require.config({
    shim: {
        //        "jquery": {"exports":"jQuery"},
        //        "json": {"exports":"JSON"} ,
        // "jsrender": {
        //     "exports": "jQuery.fn.render",
        //     deps: ['jquery']
        // }
    },
    paths: {
        //        times: 'apps/times',  //used so that app module name looks nice
        //        toi: 'apps/toi',  //used so that app module name looks nice
        //        jquery: [
        //                     "//timesofindia.indiatimes.com/jquery_toi.cms?minify=1",
        //                     "//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js",
        //                     "//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min"
        //                ],
        //        json: "//cdnjs.cloudflare.com/ajax/libs/json2/20121008/json2",
        // jsrender: "/jsrender.cms?"
    },
    config: {
        "tiljs/page": {
            channel: tLoginObj.ssochannel,
            siteId: tLoginObj.siteId,
            domain: tLoginObj.domainName
        },
        "tiljs/plugin/lazy": {
            skew: 0,
            error_image: "https://timesofindia.indiatimes.com/photo/34824568.cms"
        },
        "tiljs/social/facebook": {
            xfbml: true,
            parse: false,
            appid: tLoginObj.fbclient,
            load_js: false,
            init: false
        },
        "tiljs/social/twitter": {
            parse: false,
            load_js: false,
            init: false
        },
        "tiljs/analytics/mytimes": {
            "appKey": tLoginObj.MytCommentApiKey
        },
        "tiljs/apps/times/comments": {
            /*loadCommentFromMytimes: true,*/
            //commentType: "comments_agree",
            commentType: "comments",
			comment_block_count:15,
        },
        "tiljs/apps/times/api": {
            post_comment: {
                url: site_url+"/wp-admin/admin-ajax.php?action=postComment"
            },
            validate_comment: {
                url: site_url+"/wp-admin/admin-ajax.php?action=validatecomment"
            },
            rate_comment: {
                url: site_url+"/wp-admin/admin-ajax.php?action=ratecomment"
            },
            rate_comment_offensive: {
                url: site_url+"/wp-admin/admin-ajax.php?action=offensive"
            },
            comments: {
                url: site_url+"/wp-admin/admin-ajax.php?action=commentsdata",
                type: "json",
                params: {
                    appkey: tLoginObj.MytCommentApiKey,
                    msid: window.msid,
                    sortcriteria: "CreationDate",
                    order: "asc",
                    size: 15,
                    lastdeenid: 123,
                    after: true,
                    withReward: true

                }
            },
            comments_oldest: {
                url: site_url+"/wp-admin/admin-ajax.php?action=commentsdata",
                type: "json",
                params: {
                    appkey: tLoginObj.MytCommentApiKey,
                    msid: window.msid,
                    sortcriteria: "CreationDate",
                    order: "desc",
                    size: 15,
                    lastdeenid: 123,
                    after: true,
                    withReward: true
                }
            },
            comments_agree: {
                url: site_url+"/wp-admin/admin-ajax.php?action=commentsdata",
                type: "json",
                params: {
                    appkey: tLoginObj.MytCommentApiKey,
                    msid: window.msid,
                    sortcriteria: "AgreeCount",
                    order: "desc",
                    size: 15,
                    lastdeenid: 123,
                    after: true,
                    withReward: true,
                    medium: 'WEB'
                }
            },
            comments_disagree: {
                url: site_url+"/wp-admin/admin-ajax.php?action=commentsdata",
                type: "json",
                params: {
                    appkey: tLoginObj.MytCommentApiKey,
                    msid: window.msid,
                    sortcriteria: "DisagreeCount",
                    order: "desc",
                    size: 15,
                    lastdeenid: 123,
                    after: true,
                    withReward: true
                }
            },
            comments_discussed: {
                url: site_url+"/wp-admin/admin-ajax.php?action=commentsdata",
                type: "json",
                params: {
                    appkey: tLoginObj.MytCommentApiKey,
                    msid: window.msid,
                    sortcriteria: "discussed",
                    order: "desc",
                    size: 15,
                    lastdeenid: 123,
                    after: true,
                    withReward: true
                }
            }
        }
    }
});
define('jquery',[],function(){
    return jQuery;
});
define('tiljs/compatibility',["jquery"], function ($) {
    var mod_compatibility = {};
    //This function is not available in IE8 so implementing it.
    if (!Array.prototype.indexOf) {
        Array.prototype.indexOf = mod_compatibility.Array_indexOf = function (searchElement, fromIndex) {
            if (this === undefined || this === null) {
                throw new TypeError('"this" is null or not defined');
            }
            var length = this.length >>> 0; // Hack to convert object.length to a UInt32
            fromIndex = +fromIndex || 0;
            if (Math.abs(fromIndex) === Infinity) {
                fromIndex = 0;
            }
            if (fromIndex < 0) {
                fromIndex += length;
                if (fromIndex < 0) {
                    fromIndex = 0;
                }
            }
            for (; fromIndex < length; fromIndex++) {
                if (this[fromIndex] === searchElement) {
                    return fromIndex;
                }
            }
            return -1;
        };
    }
    //This function is not available in IE8 so implementing it.
    if (!String.prototype.trim) {
        String.prototype.trim = function () {
            return this.replace(/^\s+|\s+$/gm, '');
        };
    }
    //Utility function
    if (!String.prototype.splice) {
        String.prototype.splice = function (idx, rem, s) {
            return ( this.slice(0, idx) + s + this.slice(idx + Math.abs(rem)) );
        };
    }
    //Placeholders do not work in old browsers specially IE
    $(function () {
        //start added by Amit
        $('body').on('focus', '[placeholder]', function () {
            var input = $(this);
            if (input.val() === input.attr('placeholder')) {
                input.val('');
                input.removeClass('placeholder');
            }
        });
        $('body').on('blur', '[placeholder]', function () {
            var input = $(this);
            if (input.val() === '' || input.val() === input.attr('placeholder')) {
                input.addClass('placeholder');
                input.val(input.attr('placeholder'));
            }
        });
//		$( '[placeholder]' ).focus();
        $('[placeholder]').blur();
        //        $('[placeholder]').parents('form').submit(function() {
        //            $(this).find('[placeholder]').each(function() {
        //                var input = $(this);
        //                if (input.val() == input.attr('placeholder')) {
        //                    input.val('');
        //                }
        //            })
        //        });
        //todo move to comments
        //        $('body').on('mouseenter','.comment-box', function() {
        //            var obj = $(this).find("[data-plugin='comment-user-follow_wrapper']");
        //            if(!obj.hasClass('dont_show')){
        //                obj.show();
        //            }
        //        });
        //        $('body').on('mouseleave','.comment-box', function() {
        //            var obj = $(this);
        //            obj.find("[data-plugin='comment-user-follow_wrapper']").hide();
        //        });
        //end added by Amit
    });
    return mod_compatibility;
});

define( 'tiljs/cookie',[], function () {
    var mod_cookie = {};
    //    var default_config = {
    //          localstorage : false //use localstorage if available or else cookie
    //    };
    //
    //    var config = $.extend({}, default_config, module.config());

    /**
     * Get value of a cookie
     *
     * @memberOf module:cookie#
     * @function get
     *
     * @param name {String} name of the cookie for which value is required,
     *                        if name is not provided an object with all cookies is returned
     * @returns value {String | Array} value of the requested cookie / Array of all cookies
     *
     * @example
     *
     *  require(['cookie'],function(cookie){
	 *     var abc_cookie = cookie.get("abc");
	 *  });
     */
    mod_cookie.get = function ( name ) {
        var result = name ? undefined : {};
        var cookies = document.cookie ? document.cookie.split( '; ' ) : [];
        for( var i = 0, l = cookies.length; i < l; i++ ) {
            var parts = cookies[ i ].split( '=' );
            var nameK = decodeURIComponent( parts.shift() );
            var cookie = parts.join( '=' );
            cookie = mod_cookie._parseCookieValue( cookie );
            if( name && name === nameK ) {
                result = cookie;
                break;
            }
            if( !name && cookie !== undefined ) {
                result[ nameK ] = cookie;
            }
        }
        return result;
    };

    /**
     * Cookie Set,Get,Delete
     */
    mod_cookie.getAll = function () {
        return mod_cookie.get();
    };
    /**
     * Remove a cookie
     *
     * @memberOf module:cookie#
     * @function remove
     *
     * @param {String} name name of the cookie to be removed
     * @param {String} [path] path of the cookie
     * @param {String} [domain] domain of the cookie
     *
     * @example
     *
     *  require(['cookie'],function(cookie){
	 *     cookie.remove("abc");
	 *  });
     */
    mod_cookie.remove = function ( name, path, domain ) {
        if( name ) {
            domain = ( domain || document.location.host ).split( ":" )[ 0 ];
            path = path || document.location.pathname;
            mod_cookie.set( name, null, -1, path, domain );
        }
    };
    /**
     * Set a cookie
     *
     * @param {String} name name of the cookie to be set
     * @param {String} value value of the cookie to be set
     * @param {Number} days number of days for which the cookie is to be set
     * @param {String} path path of the cookie to be set
     * @param {String} domain domain of the cookie to be set
     * @param {Boolean} secure true if the cookie is to be set on https only
     */
    mod_cookie.set = function ( name, value, days, path, domain, secure ) {
        var expires = '';
        days = ( days !== undefined ) ? days : 30;
        var date = new Date();
        date.setTime( date.getTime() + ( days * 24 * 60 * 60 * 1000 ) );
        expires = '; expires=' + date.toGMTString();
        domain = ( domain || document.location.host ).split( ":" )[ 0 ]; //removing port
        path = path || document.location.pathname;
        //Removing file name, fix for IE11
        if( /\/.*\..*/.test( path ) ) { //if path contains file name
            path = path.split( "/" );
            path.pop();
            path = path.join( "/" );
        }
        document.cookie = name + '=' +
            value + expires +
            ( ( path ) ? ';path=' + path : '' ) +
            ( ( domain && domain !='localhost' ) ? ';domain=' + domain : '' ) +
            ( ( secure ) ? ';secure' : '' );
    };
    mod_cookie._parseCookieValue = function ( s ) {
        if( s.indexOf( '"' ) === 0 ) {
            // This is a quoted cookie as according to RFC2068, unescape...
            s = s.slice( 1, -1 ).replace( /\\"/g, '"' ).replace( /\\\\/g, '\\' );
        }
        try {
            // If we can't decode the cookie, ignore it, it's unusable.
            // Replace server-side written pluses with spaces.
            return decodeURIComponent( s.replace( /\+/g, ' ' ) );
        } catch( e ) {}
    };
    return mod_cookie;
} );
/**
 * 'event' module.
 *
 * @module event
 */
define('tiljs/event',[], function () {
    var mod_event = {},
        pubsub = {},
        onsubscribe_prefix = "__on_",
        generateUid = (function () {
            var id = 0;
            return function () {
                return id++;
            };
        })();
    /**
     * Publish subscribed events
     *
     * @param name Method name for the event to be published
     * @param data data to be passed to the subscribed event
     * @returns null
     */
    mod_event.publish = function (name, data) {
        if (!name) {
            return null;
        }
        if (pubsub[name]) {
            for (var e in pubsub[name]) {
                if (pubsub[name].hasOwnProperty(e)) {
                    var eventCallback = pubsub[name][e];
                    try { //try catch done to keep publisher running in case of error in event callback
                        eventCallback(data);
                    } catch (err) {
                        mod_event.publish("logger.error", err.stack);
                    }
                }
            }
        }
    };
    mod_event.subscribeAll = function (name, eventCallback, options) {
        if (name instanceof Array) {
            var eventIds = [];
            var responses = [];
            for (var i = 0; i < name.length; i++) {
                eventIds.push(mod_event.subscribe(name[i], function (response) {
                    responses.push(response);
                    if (eventIds.length === responses.length) { // todo fix, call even when first event is called twice
                        if (eventCallback) {
                            eventCallback(responses);
                        }
                        responses = [];
                    }
                }, options));
            }
            return eventIds;
        }
        return mod_event.subscribe(name, eventCallback, options);
    };
    /**
     * Subscribe custom events
     *
     * @param name Method name for the event to be subscribed
     * @param eventCallback(data) Function to be called when the event is published with the data
     * @options options
     * @returns eventId unique id generated for every subscription
     */
    mod_event.subscribe = function (name, eventCallback, options) {
        if (name instanceof Array) {
            var eventIds = [];
            for (var i = 0; i < name.length; i++) {
                eventIds.push(mod_event.subscribe(name[i], eventCallback, options));
            }
            return eventIds;
        }
        if (!name || !eventCallback) {
            return null;
        }
        if (!pubsub[name]) {
            pubsub[name] = {};
        }
        var eventId = name + ":" + generateUid();
        pubsub[name][eventId] = eventCallback;
        //TODO find better way
        //Setting callback in options so that it can be used in onsubscribe event.
        if (options) {
            options.__callback = eventCallback;
        }
        //Calling onsubscribe events
        mod_event.publish(onsubscribe_prefix + name, options);
        return eventId;
    };
    /**
     * Unsubscribe an event
     *
     * @param eventId id of the event to be unsubscribed
     * @returns boolean true if event is successfully unsubscribed,else false
     */
    mod_event.unsubscribe = function (eventId) {
        if (!eventId) {
            return null;
        }
        var eventArr = eventId.split(":");
        if (eventArr.length === 2) {
            var eventName = eventArr[0];
            //            var eventNum = eventArr[1];
            if (pubsub[eventName][eventId]) {
                delete pubsub[eventName][eventId];
                return true;
            }
        }
        return false;
    };
    /**
     * Get all the subscribed events in an object
     *
     *
     * @param name Event name for which all events are required
     * @returns Event object for the provided event name
     */
    mod_event.getSubscriptions = function (name) {
        if (!name) {
            return pubsub; //todo return cloned object istead of original
        }
        return pubsub[name];
    };
    /**
     * Used to subscribe to subscribe events, onsubscribe('method1') is called when subscribe('method1') is called
     * This can be used to setup data / setup publish events
     *
     * @param name
     * @param eventCallback
     * @returns {string}
     */
    mod_event.onsubscribe = function (name, eventCallback) {
        if (!name || !eventCallback) {
            return null;
        }
        name = onsubscribe_prefix + name;
        if (!pubsub[name]) {
            pubsub[name] = {};
        }
        var eventId = name + ":" + generateUid();
        pubsub[name][eventId] = eventCallback;
        return eventId;
    };
    return mod_event;
});

/**
 * 'logger' module.
 *
 * @module logger
 * @requires event
 * @requires logger
 */
define('tiljs/logger',["./cookie", "module", "./event", "jquery"], function (cookie, module, event, $) {
    var mod_logger = {}, logCache = [];
    var types = ["log", "debug", "info", "warn", "error"];

    var default_config = {
        cookieName: "d",
        hashString: "#debugdsfgw456g", //change to url param  / use cookie
        prefix: "[times_log] ",
        //linenum: false,
        time: false, //todo to be implemented
        handleWindowError: true,
        handleJqueryError: true,
        logModuleLoad: true,
        log: false //can be overridden by cookie or hash when it is false
    };
    var config = $.extend({}, default_config, module.config()); //todo remove jquery dependency
    /**
     * Returns the stack details for method that calls log
     * @param stack
     * @returns {string}
     * @private
     */
    function getStackDetails(stack) {
        var regex = new RegExp("\/(.*):([0-9]*)\:([0-9]*)", "g");
        //done thrice to get the main calling method and line number
        var res = regex.exec(stack);
        res = regex.exec(stack);
        res = regex.exec(stack);
        if (res) {
            res.shift();
        }
        return res ? res.join(":").replace(config.hashString, "") : null;
    }

    /**
     * times.debug
     *
     * times.log/info/error
     */
    mod_logger = {};
    mod_logger.disable = function () {
        config.log = false;
        cookie.remove(config.cookieName, "/");

        var i, type;
        for (i in types) {
            if (types.hasOwnProperty(i)) {
                type = types[i];

                mod_logger[type] = (function (type) {
                    return function () {
                        logCache.push.apply(logCache, arguments);
                    };
                })(type);
            }
        }

        return "Logging Disabled";
    };
    mod_logger.enable = function () {
        config.log = true;
        cookie.set(config.cookieName, config.hashString, 30, "/");


        var i, type;
        for (i in types) {
            if (types.hasOwnProperty(i)) {
                type = types[i];

                function fun() {
                    console.log.apply(this, arguments);
                }

                mod_logger[type] = (function (type) {
                    var noop = function () {
                    };
                    var log;
                    var context = config.prefix;
                    if (console.log.bind === 'undefined') { // IE < 10
                        log = Function.prototype.bind.call(console[type], console, context);
                    }
                    else {
                        log = console[type].bind(console, context);
                    }

                    //log = console[type].bind(console);
                    //log = fun.bind(console);

                    //log = (window.console === undefined) ? noop
                    //	: (Function.prototype.bind !== undefined) ? Function.prototype.bind.call(console[type], console)
                    //	: function() {Function.prototype.apply.call(console[type], console, arguments);};

                    return log;

                })(type);

                (function (type) {
                    event.subscribe("logger." + type, function (data) {
                        Function.prototype.apply.call(mod_logger[type], console, arguments);
                    });
                }(type));
            }
        }


        return "Logging Enabled";
    };
    mod_logger.isEnabled = function () {
        return config.log || ( window.location.hash.length > 0 && window.location.hash === ( config.hashString ) ) || ( cookie.get(config.cookieName) === config.hashString );
    };
    mod_logger.handleWindowError = function () {
        window.onerror = function (msg, url, linenumber, colno, error) {
            mod_logger.error.apply(this,
                ['Error message: ' + msg +
                '\n\tURL: ' + url + ( linenumber ? ":" + linenumber + ":" + colno : "" ) +
                '\n\tLine Number: ' + linenumber + ":" + colno +
                '\n\tError: ' + error]);
            return true;
        };
    };
    mod_logger.handleJqueryError = function () {
        $(document).ajaxError(function (event, jqxhr, settings, exception) {
            if (exception === "timeout") {
                mod_logger.error.apply(this, [exception + ": " + settings.url]);
            } else {
                mod_logger.error.apply(this, [exception]);
            }
        });
        $(document).error(function (event) {
            mod_logger.error.apply(this, event);
        });
    };
    /**
     window.require.onResourceLoad = function (context, map, depArray) {
            console.log(map.name);
        };

     */
    function init_log() {

        config.log = mod_logger.isEnabled();
        if (config.log) {
            mod_logger.enable();
        } else {
            mod_logger.disable();
        }

        if (config.handleWindowError === true) {
            mod_logger.handleWindowError();
            //Doing it again on window load
            event.subscribe("window.load", function () {
                mod_logger.handleWindowError();
            });
        }
        if (config.handleJqueryError === true) {
            mod_logger.handleJqueryError();
        }
    }

    /**
     * As of date we are saving only logs which are not being cached.
     *
     * //TODO save all logs to cache
     */
    mod_logger.getLogs = function () {
        return logCache.slice(0);//slice to create a new reference
    };
    mod_logger.getLogsStr = function () {
        return mod_logger.getLogs().join("\n");
    };


    init_log();
    return mod_logger;
});
/** 'is' module.
 * @module is
 * @exports is
 */
define('tiljs/is',[], function () {
    var mod_is = {};
    /**
     * Checks if the param is a number.
     *
     * @memberOf module:is#
     * @function number
     * @param ele {object} Any element which is to be checked
     * @returns {boolean}
     * @example
     *
     * require(['is'],function(is){
	 *     is.number(1);        //returns true
	 *     is.number('abc');    //returns false
	 * });
     */
    mod_is.number = function (ele) {
        return typeof ele === "number";
    };
    /**
     * Checks if the param is a string.
     *
     * @memberOf module:is#
     * @function string
     * @param ele {object} Any element which is to be checked
     * @returns {boolean}
     * @example
     *
     * require(['is'],function(is){
	 *     is.number(1);        //returns false
	 *     is.number('abc');    //returns true
	 * });
     */
    mod_is.string = function (ele) {
        return typeof ele === "string";
    };
    mod_is.funct = mod_is.method = function (ele) {
        return typeof ele === "function";
    };
    mod_is.object = function (ele) {
        return ele !== null && typeof ele === "object"; //&& !(ele instanceof Array);
    };
    mod_is.array = Array && Array.isArray ? Array.isArray : function (ele) {
        return ele instanceof Array;
    };
    mod_is.undefined = function (ele) {
        return typeof ele === "undefined";
    };
    mod_is.defined = function (ele) {
        return typeof ele !== "undefined";
    };
    mod_is.exists = function (ele) {
        return mod_is.defined(ele) || ele === "";
    };
    mod_is.empty = function (ele) {
        if (!mod_is.defined(ele)) {
            return true;
        } else if (mod_is.string(ele) || mod_is.array(ele)) {
            return ele.length === 0;
        } else if (mod_is.object(ele)) {
            var i = 0,
                e;
            for (e in ele) {
                if (ele.hasOwnProperty(e)) {
                    i++;
                }
            }
            return i === 0;
        } else if (mod_is.number(ele)) {
            return false;
        } else {
            return true;
        }
    };
    mod_is.alphaOnly = function (str) {
        return /^[A-z\s]+$/.test(str);
    };
    mod_is.numberOnly = function (str) {
        return /^[0-9]+$/.test(str);
    };
    mod_is.mobile = function () {
        return (function (a) {
            return /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4));
        })(navigator.userAgent || navigator.vendor || window.opera);
    };
    mod_is.tablet = function () {
        return (function (a) {
            return /(?:ipad|tab)/i.test(a);
        })(navigator.userAgent || navigator.vendor || window.opera);
    };
    mod_is.desktop = function () {
        return !mod_is.mobile() && !mod_is.tablet();
    };
    mod_is.touch = function () {
        return (('ontouchstart' in window) || ('DocumentTouch' in window));
    };
    mod_is.IE = function () {
        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");
        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) { // If Internet Explorer, return version number
            return true;
        } else { // If another browser, return 0
            return false;
        }
    };
    mod_is.IE11 = function () {
        return ( !!navigator.userAgent.match(/Trident\/7\./) );
    };
    mod_is.visible = function (ele) {
        return ele && ele.is(":visible");
    };
    mod_is.iframe = function (ele) {
        return ele.tagName === "IFRAME";
    };
    mod_is.dateStr = function (str) {
        try {
            new Date(str);
            return true;
        } catch (e) {
            return false;
        }
    };
    mod_is.email = function (email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    };
    mod_is.url = function (str) {
        var url_pattern = /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[\-;:&=\+\$,\w]+@)?[A-Za-z0-9\.\-]+|(?:www\.|[\-;:&=\+\$,\w]+@)[A-Za-z0-9\.\-]+)((?:\/[\+~%\/\.\w\-]*)?\??(?:[\-\+=&;%@\.\w]*)#?(?:[\.\!\/\\\w]*))?)/g;
        return url_pattern.test(str);
    };
    return mod_is;
});

/**
 * 'string' module.
 *
 * @module string
 */
define('tiljs/string',[], function () {
    var mod_string = {};
    mod_string.camelCase = function (str) {
        return str.replace(/(?:^|\s)\w/g, function (match) {
            return match.toUpperCase();
        }).replace(/(\s|-|_)/g, "");
    };
    mod_string.splice = function (str, idx, rem, s) {
        return ( str.slice(0, idx) + s + str.slice(idx + Math.abs(rem)) );
    };
    mod_string.startsWith = function (haystack, needle) {
        return haystack.indexOf(needle) === 0;
    };
    return mod_string;
});

define('json',[],function(){
    return JSON;
});
/**
 * 'util' module.
 *
 * @module util
 * @requires logger
 * @requires jquery
 * @requires is
 * @requires string
 * @requires json
 */
define('tiljs/util',["./logger", "jquery", "./is", "./string", "json"], function (logger, $, is, string, json) {
    logger.log("util loaded");
    var mod_util = {};
    mod_util.extend = $.extend;
    /*

     function () {
     var arg = [true];
     for(var i=0;arguments[i];i++){
     arg.push(arguments[i]);
     }

     return mod_util.deepextend.apply(this,arg);
     };*/
    mod_util.deepextend = function () {
        //todo remove jquery dependency
        return $.extend.apply(this, arguments);
        //        for (var i = 1; i < arguments.length; i++)
        //            for (var key in arguments[i]){
        //                if (arguments[i].hasOwnProperty(key)){
        //                    if(arguments[i][key] instanceof Array || arguments[i][key] instanceof Object){
        ////                        console.log(arguments[i][key]);
        //                        mod_util.extend(arguments[0][key],arguments[i][key]);
        //                    }else{
        //                        console.log(arguments[i][key]);
        //
        //                        arguments[0][key] = arguments[i][key];
        //                    }
        //                }
        //            }
        //        return arguments[0];
    };
    /**
     * Get values in obj2 which are not there in obj1
     *
     * INPUT
     * obj1 = {a:1,b:2,c:3}
     * obj2 = {a:1,c:3,d:4}
     *
     * OUTPUT
     * obj = {d:4}
     *
     * @param obj1
     * @param obj2
     * @returns {{}}
     */
    mod_util.xor = function (obj1, obj2) {
        var obj = {}, o;
        for (o in obj2) {
            if (!obj1.hasOwnProperty(o)) {
                obj[o] = obj2[o];
            }
        }
        return obj;
    };
    mod_util.convertObj = function (obj, callback) {
        var objResult = {};
        util.each(obj, function (k, v) {
            objResult[k] = callback(k, v);
        });
        return objResult;
    };
    mod_util.each = function (obj, callback) {
        var i, o;
        if (!callback) {
            return null;
        }
        if (obj instanceof Array) {
            for (i = 0; i < obj.length; i++) {
                if (callback(i, obj[i]) === false) {
                    break;
                }
            }
        } else if (obj instanceof Object) {
            for (o in obj) {
                if (obj.hasOwnProperty(o)) {
                    if (callback(o, obj[o]) === false) {
                        break;
                    }
                }
            }
        } else {
            //callback(0, obj);
        }
    };
    mod_util.wrapCallback = function (callback, arga) {
        try {
            if (callback) {
                callback.apply(this, args);
            }
        } catch (e) {
            logger.error(e);
        }
    };
    //    mod_util.update = function (obj) {
    //        config = mod_util.extend(true, config, obj);
    //    };
    mod_util.formatNumber = function (num) {
        if (num >= 1000000000) {
            return ( num / 1000000000 ).toFixed(1) + 'G';
        }
        if (num >= 1000000) {
            return ( num / 1000000 ).toFixed(1) + 'M';
        }
        if (num >= 1000) {
            return ( num / 1000 ).toFixed(1) + 'K';
        }
        return num;
    };
    mod_util.getJsonFromString = function (str, entitySeparator, keyValueSeparator) {
        entitySeparator = entitySeparator || "&";
        keyValueSeparator = keyValueSeparator || "=";
        var json = {};
        var entities = str.split(entitySeparator);
        mod_util.each(entities, function (i, entity) {
            var keyValue = entity.split(keyValueSeparator);
            json[keyValue[0]] = keyValue[1];
        });
        return json;
    };
    mod_util.val = function (val, options) {
        if (is.funct(val)) {
            return val(options);
        }
        return val;
    };
    /**
     * Throttles the number of times a function is called, ignores rest of calls
     * <br/>
     * function called normally        ##############################<br/>
     * function called with throttle   #     #     #     #     #    #<br/>
     * <br/>
     * Assuming one space as one milisecond & delay=5<br/>
     * <br/>
     * Usage : 'scroll'/'move' functions where events are fired on every scroll/move, calling of event can be throttled
     *
     * @function throttle
     * @memberOf module:util#
     * @param delay time after which that method is to be called
     * @param callback method to be called after specified delay
     * @returns {function} throttled instance of function which can be called on throttle
     * @example
     *
     * function run(i){
	 *     console.log("run called : "+i);
	 * }
     *
     * //Normal Call
     * for(var i=0;i<500;i++){
	 *     run(i);  //called 500 times
	 * }
     *
     * //Throttled call
     * require(['util'],function(util){
	 *   var th = util.throttle(5,run);
	 *   for(var i=0;i<500;i++){
	 *     th(i);  //called (500/time taken by function) times
	 *   }
	 * });
     *
     */
    mod_util.throttle = function (delay, callback) {
        var _timeout;
        var _exec = 0;
        return function callable() {
            var elapsed = +new Date() - _exec;
            //                console.log("callable:elapsed:"+_exec);
            var tthis = this;
            var args = arguments;

            function run() {
                _exec = +new Date();
                //                    console.log("run_exec:"+_exec);
                callback.apply(tthis, args);
            }

            _timeout && clearTimeout(_timeout);
            //                console.log("elapsed>run : "+(elapsed+">"+delay));
            if (elapsed > delay) run();
            else _timeout = setTimeout(run, delay - elapsed);
        };
    };
    mod_util.getDate = function (str) {
        try {
            if (is.number(str)) {
                return new Date(str);
            } else {
                str = str.replace(/(\d)(A|P)M/, "$1 $2M"); //Changes "22 Nov 2013, 10:19AM" -> "22 Nov 2013, 10:19 AM"
            }
            return new Date(str);
        } catch (e) {
            logger.error(e);
            return null;
        }
    };
    var __uuidCnt = 0;
    /**
     * Generates uuid.
     * @returns {number} unique id for a system
     */
    mod_util.uuid = function () {
        var d = new Date().getTime();
        var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = ( d + Math.random() * 16 ) % 16 | 0;
            d = Math.floor(d / 16);
            return ( c == 'x' ? r : ( r & 0x7 | 0x8 ) ).toString(16);
        });
        return uuid;
    };
    /**
     * Make getter setters for heirerchy less object supplied as parameter
     * @param objName
     * @param json
     * @param setter
     */
    //    mod_util.jsonToObj = function(objName,json,setter){
    //        this[objName] = function(attr){
    //            var tthis = this;
    //            mod_util.each(attr,function(k,v){
    //                tthis[k] = v;
    //            });
    //        };
    //        var obj = new this[objName](json);
    ////        if(!is.object(obj)){
    ////            throw new Error("Paramater is not an 'Object'. Cannot apply Getter-Setter");
    ////        }
    //
    //        mod_util.each(json,function(k,v){
    //            var kCamelCase = string.camelCase(k);
    //            if(setter === true){
    //                obj['set'+kCamelCase] = function(kk){return this[k] = kk;};
    //            }
    //            obj['get'+kCamelCase] = function(){return this[k]};
    //        });
    //        return obj;
    //    };
    /**
     * Changes the key for an object using the maping.
     *
     * Eg :
     *    obj = {id:123, name:"Joe"};
     *    mapping = {idd:id}
     *
     *    result = {idd:123}
     *
     *
     * @param mapping
     * @param obj
     * @returns {{}}
     */
    mod_util.mapObj = function (mapping, obj) {
        if (!is.object(mapping) && !is.object(obj)) {
            throw new Error('Invalid Parameters'); //todo give proper message
        }
        var newObj = {};
        mod_util.each(mapping, function (k, v) {
            if (is.funct(v)) {
                newObj[k] = v(obj);
            } else {
                newObj[k] = obj[v];
            }
        });
        return newObj;
    };
    mod_util.getDomainOnly = function (url) {
        //        var separate = window.location.host.split('.');
        //        separate.shift();
        //        return "." + separate.join('.');
        return "." + ( url || document.location.host ).split(":")[0].split(".").reverse().slice(0, 2).reverse().join(".");
    };
    mod_util.format = function (text, data) { //todo move to format.js
        var formattedText = text;
        mod_util.each(data, function (i, v) {
            formattedText = formattedText.replace("{{:" + i + "}}", data[i]); //todo use better way
        });
        //Removing text which could not be formatted, todo log it
        formattedText = formattedText.replace(/{{:[a-zA_Z0-9]+}}/, "");
        return formattedText;
    };
    mod_util.stringify = JSON ? JSON.stringify : null;
    mod_util.checkNoScript = function (url) {
        //        $("noscript")
    };
    mod_util.getClick = function () {
        return is.touch() ? "touchstart" : "click";
    };
    mod_util.getParam = function (key) {
        var sanitizedParams = window.location.search.replace("?", "");
        if (is.defined(key)) {
            return mod_util.deparam(sanitizedParams)[key];
        } else {
            return mod_util.deparam(sanitizedParams);
        }
    };
    mod_util.deparam = function (text) {
        var data = {}; //todo mot handled 'a[]' or 'a[b][c]'
        mod_util.each(text.replace(/\+/g, ' ').split("&"), function (i, pair) {
            var kv = pair.split("=");
            data[decodeURIComponent(kv[0])] = decodeURIComponent(kv[1] || ""); //todo all values are string
        });
        return data
    };
    //todo implement setting and getting each attr
    mod_util.data = function (element) {
        var data = {};
        var attrMap = $(element)[0].attributes;
        $.each(attrMap, function (i, e) {
            var prefix = e.nodeName.substr(0, 5);
            if (prefix === "data-") { //It is a data attribute
                data[e.nodeName.replace(prefix, "")] = e.nodeValue; //TODO nodeValue depricated use node
            }
        });
        return data;
    };
    var ManagedError = function (message) {
        Error.prototype.constructor.apply(this, arguments);
        this.message = message;
    };
    ManagedError.prototype = new Error();
    mod_util.wrapCallback = function (fn) {
        return function () {
            try {
                return fn.apply(this, arguments);
            } catch (e) {
                if (e instanceof ManagedError) {
                    // re-throw immediately
                    throw e;
                }
                logger.error(e);
                // re-throw to halt execution
                throw e;
            }
        };
    };
    /**
     * MD5 (Message-Digest Algorithm) by WebToolkit
     * @param s
     * @returns {string}
     */
    mod_util.md5 = function (s) {
        function L(k, d) {
            return ( k << d ) | ( k >>> ( 32 - d ) )
        }

        function K(G, k) {
            var I, d, F, H, x;
            F = ( G & 2147483648 );
            H = ( k & 2147483648 );
            I = ( G & 1073741824 );
            d = ( k & 1073741824 );
            x = ( G & 1073741823 ) + ( k & 1073741823 );
            if (I & d) {
                return ( x ^ 2147483648 ^ F ^ H )
            }
            if (I | d) {
                if (x & 1073741824) {
                    return ( x ^ 3221225472 ^ F ^ H )
                } else {
                    return ( x ^ 1073741824 ^ F ^ H )
                }
            } else {
                return ( x ^ F ^ H )
            }
        }

        function r(d, F, k) {
            return ( d & F ) | ( ( ~d ) & k )
        }

        function q(d, F, k) {
            return ( d & k ) | ( F & ( ~k ) )
        }

        function p(d, F, k) {
            return ( d ^ F ^ k )
        }

        function n(d, F, k) {
            return ( F ^ ( d | ( ~k ) ) )
        }

        function u(G, F, aa, Z, k, H, I) {
            G = K(G, K(K(r(F, aa, Z), k), I));
            return K(L(G, H), F)
        }

        function f(G, F, aa, Z, k, H, I) {
            G = K(G, K(K(q(F, aa, Z), k), I));
            return K(L(G, H), F)
        }

        function D(G, F, aa, Z, k, H, I) {
            G = K(G, K(K(p(F, aa, Z), k), I));
            return K(L(G, H), F)
        }

        function t(G, F, aa, Z, k, H, I) {
            G = K(G, K(K(n(F, aa, Z), k), I));
            return K(L(G, H), F)
        }

        function e(G) {
            var Z;
            var F = G.length;
            var x = F + 8;
            var k = ( x - ( x % 64 ) ) / 64;
            var I = ( k + 1 ) * 16;
            var aa = Array(I - 1);
            var d = 0;
            var H = 0;
            while (H < F) {
                Z = ( H - ( H % 4 ) ) / 4;
                d = ( H % 4 ) * 8;
                aa[Z] = ( aa[Z] | ( G.charCodeAt(H) << d ) );
                H++
            }
            Z = ( H - ( H % 4 ) ) / 4;
            d = ( H % 4 ) * 8;
            aa[Z] = aa[Z] | ( 128 << d );
            aa[I - 2] = F << 3;
            aa[I - 1] = F >>> 29;
            return aa
        }

        function B(x) {
            var k = "",
                F = "",
                G, d;
            for (d = 0; d <= 3; d++) {
                G = ( x >>> ( d * 8 ) ) & 255;
                F = "0" + G.toString(16);
                k = k + F.substr(F.length - 2, 2)
            }
            return k
        }

        function J(k) {
            k = k.replace(/rn/g, "n");
            var d = "";
            for (var F = 0; F < k.length; F++) {
                var x = k.charCodeAt(F);
                if (x < 128) {
                    d += String.fromCharCode(x)
                } else {
                    if (( x > 127 ) && ( x < 2048 )) {
                        d += String.fromCharCode(( x >> 6 ) | 192);
                        d += String.fromCharCode(( x & 63 ) | 128)
                    } else {
                        d += String.fromCharCode(( x >> 12 ) | 224);
                        d += String.fromCharCode(( ( x >> 6 ) & 63 ) | 128);
                        d += String.fromCharCode(( x & 63 ) | 128)
                    }
                }
            }
            return d
        }

        var C = Array();
        var P, h, E, v, g, Y, X, W, V;
        var S = 7,
            Q = 12,
            N = 17,
            M = 22;
        var A = 5,
            z = 9,
            y = 14,
            w = 20;
        var o = 4,
            m = 11,
            l = 16,
            j = 23;
        var U = 6,
            T = 10,
            R = 15,
            O = 21;
        s = J(s);
        C = e(s);
        Y = 1732584193;
        X = 4023233417;
        W = 2562383102;
        V = 271733878;
        for (P = 0; P < C.length; P += 16) {
            h = Y;
            E = X;
            v = W;
            g = V;
            Y = u(Y, X, W, V, C[P + 0], S, 3614090360);
            V = u(V, Y, X, W, C[P + 1], Q, 3905402710);
            W = u(W, V, Y, X, C[P + 2], N, 606105819);
            X = u(X, W, V, Y, C[P + 3], M, 3250441966);
            Y = u(Y, X, W, V, C[P + 4], S, 4118548399);
            V = u(V, Y, X, W, C[P + 5], Q, 1200080426);
            W = u(W, V, Y, X, C[P + 6], N, 2821735955);
            X = u(X, W, V, Y, C[P + 7], M, 4249261313);
            Y = u(Y, X, W, V, C[P + 8], S, 1770035416);
            V = u(V, Y, X, W, C[P + 9], Q, 2336552879);
            W = u(W, V, Y, X, C[P + 10], N, 4294925233);
            X = u(X, W, V, Y, C[P + 11], M, 2304563134);
            Y = u(Y, X, W, V, C[P + 12], S, 1804603682);
            V = u(V, Y, X, W, C[P + 13], Q, 4254626195);
            W = u(W, V, Y, X, C[P + 14], N, 2792965006);
            X = u(X, W, V, Y, C[P + 15], M, 1236535329);
            Y = f(Y, X, W, V, C[P + 1], A, 4129170786);
            V = f(V, Y, X, W, C[P + 6], z, 3225465664);
            W = f(W, V, Y, X, C[P + 11], y, 643717713);
            X = f(X, W, V, Y, C[P + 0], w, 3921069994);
            Y = f(Y, X, W, V, C[P + 5], A, 3593408605);
            V = f(V, Y, X, W, C[P + 10], z, 38016083);
            W = f(W, V, Y, X, C[P + 15], y, 3634488961);
            X = f(X, W, V, Y, C[P + 4], w, 3889429448);
            Y = f(Y, X, W, V, C[P + 9], A, 568446438);
            V = f(V, Y, X, W, C[P + 14], z, 3275163606);
            W = f(W, V, Y, X, C[P + 3], y, 4107603335);
            X = f(X, W, V, Y, C[P + 8], w, 1163531501);
            Y = f(Y, X, W, V, C[P + 13], A, 2850285829);
            V = f(V, Y, X, W, C[P + 2], z, 4243563512);
            W = f(W, V, Y, X, C[P + 7], y, 1735328473);
            X = f(X, W, V, Y, C[P + 12], w, 2368359562);
            Y = D(Y, X, W, V, C[P + 5], o, 4294588738);
            V = D(V, Y, X, W, C[P + 8], m, 2272392833);
            W = D(W, V, Y, X, C[P + 11], l, 1839030562);
            X = D(X, W, V, Y, C[P + 14], j, 4259657740);
            Y = D(Y, X, W, V, C[P + 1], o, 2763975236);
            V = D(V, Y, X, W, C[P + 4], m, 1272893353);
            W = D(W, V, Y, X, C[P + 7], l, 4139469664);
            X = D(X, W, V, Y, C[P + 10], j, 3200236656);
            Y = D(Y, X, W, V, C[P + 13], o, 681279174);
            V = D(V, Y, X, W, C[P + 0], m, 3936430074);
            W = D(W, V, Y, X, C[P + 3], l, 3572445317);
            X = D(X, W, V, Y, C[P + 6], j, 76029189);
            Y = D(Y, X, W, V, C[P + 9], o, 3654602809);
            V = D(V, Y, X, W, C[P + 12], m, 3873151461);
            W = D(W, V, Y, X, C[P + 15], l, 530742520);
            X = D(X, W, V, Y, C[P + 2], j, 3299628645);
            Y = t(Y, X, W, V, C[P + 0], U, 4096336452);
            V = t(V, Y, X, W, C[P + 7], T, 1126891415);
            W = t(W, V, Y, X, C[P + 14], R, 2878612391);
            X = t(X, W, V, Y, C[P + 5], O, 4237533241);
            Y = t(Y, X, W, V, C[P + 12], U, 1700485571);
            V = t(V, Y, X, W, C[P + 3], T, 2399980690);
            W = t(W, V, Y, X, C[P + 10], R, 4293915773);
            X = t(X, W, V, Y, C[P + 1], O, 2240044497);
            Y = t(Y, X, W, V, C[P + 8], U, 1873313359);
            V = t(V, Y, X, W, C[P + 15], T, 4264355552);
            W = t(W, V, Y, X, C[P + 6], R, 2734768916);
            X = t(X, W, V, Y, C[P + 13], O, 1309151649);
            Y = t(Y, X, W, V, C[P + 4], U, 4149444226);
            V = t(V, Y, X, W, C[P + 11], T, 3174756917);
            W = t(W, V, Y, X, C[P + 2], R, 718787259);
            X = t(X, W, V, Y, C[P + 9], O, 3951481745);
            Y = K(Y, h);
            X = K(X, E);
            W = K(W, v);
            V = K(V, g)
        }
        var i = B(Y) + B(X) + B(W) + B(V);
        return i.toLowerCase()
    };
    /**
     * Decode '<' to '&lt;'
     * @param decodedText
     * @returns {String}
     */
    mod_util.encodeHTML = function (text) {
        return $("<div/>").text(text).html();
    }
    /**
     * Decode '&lt;' to '<'
     * @param decodedText
     * @returns {String}
     */
    mod_util.decodeHTML = function (text) {
        return $("<div/>").html(text).text();
    }
    /**
     * reload iframe
     * @param: iframe selector
     * @usage: Used to reload iframe by passing
     *         any selector
     * @example: util.reloadIframe("#id");
     *         util.reloadIframe(".class");
     */
    mod_util.reloadIframe = function (iframe) {
        if ($(iframe).length > 0) {
            $(iframe).attr("src", $(iframe).attr("src"));
            return true;
        }
        return false;
    }
    /**
     *trim text
     *@param: cmt:text, charlen: character length
     *@usage: Used to cut the string to desired length
     *@example:util.trimText("text string",5)
     */
    mod_util.trimText = function (cmt, charlen) {
        var trimmedString = cmt.substr(0, charlen);
        if (cmt.length > charlen) {
            trimmedString = cmt.replace(new RegExp("^(.{" + charlen + "}[^\\s]*).*"), "$1");
        }
        return trimmedString;
    }
    return mod_util;
});

/**
 * 'timer' module.
 *
 * @module timer
 * @requires util
 * @requires is
 */
define('tiljs/timer',["./util", "./is"], function (util, is) {
    var mod_timer = {};
    mod_timer.every = function (time, callback) {
        return setInterval(callback, time);
    };
    mod_timer.after = mod_timer.delay = function (time, callback) {
        return setTimeout(callback, time);
    };
    mod_timer.available = function (varname, callback, time, trytimes) {
        if (trytimes == 0) {
            callback(null);
        }
        if (window[varname]) {
            if (callback) {
                callback(window[varname]);
            }
        } else {
            time = time || 2000;
            setTimeout(function () {
                mod_timer.available(varname, callback, time, --trytimes);
            }, time);
        }
    };
    mod_timer.cancel = function (id) { //todo handle gracefully
        try {
            clearTimeout(id);
        } catch (e) {
        }
        try {
            clearInterval(id);
        } catch (e) {
        }
    };
    mod_timer.elapsedTime = function (ctime, labels_config, last_only) {
        if (!is.number(ctime)) {
            //            if(is.dateStr(ctime)){
            //                return mod_timer.elapsedTime(new Date(ctime).getTime(),labels_config, last_only);
            //            }
            try {
                ctime = parseInt(ctime, 10);
                return mod_timer.elapsedTime(ctime, labels_config, last_only);
            } catch (e) {
                return "";
            }
        }
        var labels_default = {
            year: "year",
            //            month:"month",  //todo implement months
            day: "day",
            hour: "hour",
            minute: "minute",
            second: "second",
            ago: "ago"
        };
        var labels = util.extend(true, {}, labels_default, labels_config);
        var timeparts = [
            {
                name: labels.year,
                div: 31536000000,
                mod: 10000
            },
            //            {name: labels.month, div: 86400000, mod: 365},
            {
                name: labels.day,
                div: 86400000,
                mod: 365
            },
            {
                name: labels.hour,
                div: 3600000,
                mod: 24
            },
            {
                name: labels.minute,
                div: 60000,
                mod: 60
            },
            {
                name: labels.second,
                div: 1000,
                mod: 60
            }
        ];
        var
            i = 0,
            l = timeparts.length,
            calc,
            values = [],
            interval = new Date().getTime() - ctime; //todo use server time
        while (i < l) {
            calc = Math.floor(interval / timeparts[i].div) % timeparts[i].mod;
            if (calc && calc >= 0) {
                values.push(calc + ' ' + timeparts[i].name + ( calc > 1 ? 's' : '' ));
            }
            i += 1;
        }
        if (values.length === 0) {
            values.push('1 ' + labels.second);
        }
        if (last_only === true) {
            return values[0] + ' ' + labels.ago;
        } else {
            return values.join(', ') + ' ' + labels.ago;
        }
    };
    return mod_timer;
});

/**
 * 'ajax' module.
 *
 * @module ajax
 * @requires timer
 * @requires util
 * @requires jquery
 * @requires is
 */
define('tiljs/ajax',["./timer", "./util", "jquery", "./is", "module"], function (timer, util, $, is, module) {
    var default_config = {
            timeout: 60000 // 60 seconds
        },
        config = util.extend(true, {}, default_config, module.config()),
        mod_ajax = {};

    $.ajaxSetup({
        timeout: config.timeout
    });
    /**
     * Checks if the javascript or css is loaded
     *
     * This is to be deprecated, use 'queue' instead.
     *
     * @memberOf module:ajax#
     * @function getAll
     *
     * @param urlArr
     * @param dataArr
     * @param callback
     * @param type
     * @returns {boolean}
     * @example
     *
     *  require(['ajax'],function(ajax){
	 *     ajax.getAll(['a.json','b.json'],[{a_param:'a'},[]],function(a_resp,b_resp){
	 *          console.log(a_resp);
	 *          console.log(b_resp);
	 *     })
	 *  });
     */
    mod_ajax.getAll = function (urlArr, dataArr, callback, type) {
        var tthis = this,
            result = [],
            response = [],
            i, currDataArr;

        function process(data) {
            response.push(data);
            if (response.length === urlArr.length) {
                callback.apply(tthis, response);
            }
        }

        for (i = 0; i < urlArr.length; i++) {
            currDataArr = dataArr && dataArr.length > i ? dataArr[i] : {}; //todo use better method
            result.push($.get(urlArr[i], currDataArr, process, type));
        }
        return result;
    };
    /**
     * Creates a serialized representation of an array or object,
     * suitable for use in a URL query string or Ajax request.
     *
     * @memberOf module:ajax#
     * @function param
     *
     * @param {Array|Object} obj
     * @param {Boolean} [traditional]
     * @returns {String} Serialised array or object
     * @example
     *
     *  require(['ajax'],function(ajax){
	 *
	 *  var myObject = {
	 *    a: {
	 *      one: 1,
	 *      two: 2,
	 *      three: 3
	 *    },
	 *    b: [ 1, 2, 3 ]
	 *  };
	 *  ajax.param(myObject);
	 *  //output : a%5Bone%5D=1&a%5Btwo%5D=2&a%5Bthree%5D=3&b%5B%5D=1&b%5B%5D=2&b%5B%5D=3
	 *  });
     */
    mod_ajax.param = $.param; //todo override
    /**
     * Perform an asynchronous HTTP (Ajax) request<br/>
     * <br/>
     * See {@link http://api.jquery.com/jQuery.ajax/|jQuery.ajax}
     *
     * @memberOf module:ajax#
     * @function ajax
     *
     * @param {String} url A string containing the URL to which the request is sent.
     * @param {Object} [settings] A set of key/value pairs that configure the Ajax request. All settings are optional.
     *                            A default can be set for any option with $.ajaxSetup(). See jQuery.ajax( settings )
     *                            below for a complete list of all settings.
     * @returns {object}
     * @example
     *
     *  require(['ajax'],function(ajax){
	 *     ajax.ajax('http://www.abc.com/ajax.json')
	 *  });
     */
    /**
     * Perform an asynchronous HTTP (Ajax) request<br/>
     * <br/>
     * See {@link http://api.jquery.com/jQuery.ajax/|jQuery.ajax}
     *
     * @memberOf module:ajax#
     * @function ajax
     *
     * @param {Object} [settings] A set of key/value pairs that configure the Ajax request. All settings are optional.
     *                            A default can be set for any option with $.ajaxSetup(). See jQuery.ajax( settings )
     *                            below for a complete list of all settings.
     * @returns {object}
     * @example
     *
     *  require(['ajax'],function(ajax){
	 *     ajax.ajax({
	 *       url: "http://www.abc.com/data.json",
	 *       beforeSend: function( xhr ) {
	 *         xhr.overrideMimeType( "text/plain; charset=x-user-defined" );
	 *       },
	 *       success:function(data){
	 *          console.log(data);   //data returned by ajax request
	 *       }
	 *     })
	 *  });
     */
    mod_ajax.ajax = $.ajax; //todo override
    /**
     * Load data from the server using a HTTP GET request.<br/>
     * <br/>
     * See {@link http://api.jquery.com/jQuery.get/|jQuery.get}
     *
     * This is a shorthand Ajax function, which is equivalent to:
     *
     * <pre><code>
     ajax.ajax({
              url: url,
              data: data,
              success: success,
              dataType: dataType
            });
     * </code></pre>
     *
     * @memberOf module:ajax#
     * @function get
     *
     * @example
     *
     *  require(['ajax'],function(ajax){
     *     ajax.get("http://www.abc.com/data.json",{},function(data){
     *         console.log(data);   //data returned by ajax request
     *     });
     *  });
     * @param {String|Function} url A string containing the URL to which the request is sent.
     * @param {Object|String} [data] A plain object or string that is sent to the server with the request.
     * @param {Function} [callback] A callback function that is executed if the request succeeds.
     * @param {String} [type] The type of data expected from the server. Default: Intelligent Guess (xml, json, script, or html).
     * @returns {object}
     */
    mod_ajax.get = function (url, data, callback, type) {
        if (is.funct(url)) {
            return url(data, callback);
        } else {
            return $.get(url, data, callback, type);
        }
    };
    /**
     * Send/Load data to/from the server using a HTTP POST request.<br/>
     * <br/>
     * See {@link http://api.jquery.com/jQuery.get/|jQuery.get}
     *
     * This is a shorthand Ajax function, which is equivalent to:
     *
     * <pre><code>
     ajax.ajax({
              type: "POST",
              url: url,
              data: data,
              success: success,
              dataType: dataType
            });
     * </code></pre>
     *
     * @memberOf module:ajax#
     * @function post
     *
     * @example
     *
     *  require(['ajax'],function(ajax){
     *     ajax.post("http://www.abc.com/data.json",{},function(data){
     *         console.log(data);   //data returned by ajax request
     *     });
     *  });
     * @param {String} url A string containing the URL to which the request is sent.
     * @param {Object|String} [data] A plain object or string that is sent to the server with the request.
     * @param {Function} [callback] A callback function that is executed if the request succeeds.
     * @param {String} [type] The type of data expected from the server. Default: Intelligent Guess (xml, json, script, or html).
     * @returns {object}
     */
    mod_ajax.post = function (url, data, callback, type) {
        return $.post(url, data, callback, type);
    };
    mod_ajax.postCrossDomain = function (url, data) {
        var randomName = "postFrame" + new Date().getTime() + Math.floor(( Math.random() * 1000 )),
            iframe = $("<iframe></iframe>").attr("name", randomName),
            //Create the form in a jQuery object
            form = $("<form action='" + url + "' method='post' target='" + randomName + "'></form>");
        //Get the value from the asp textbox with the ID txtBox1 and POST it as b1
        util.each(data, function (k, v) {
            form.append($("<input type='hidden' name='" + k + "' />").attr('value', v));
        });
        //Add the iframe to the body
        iframe.appendTo('body');
        //Add the form to the body
        form.appendTo('body')
        //Submit the form which posts the variables into the iframe
            .submit()
            //Remove the form from the DOM (so subsequent requests won't keep expanding the DOM)
            .remove();
        timer.after(10000, function () {
            iframe.remove();
        });
        //        return $.post(url,data,callback,type);
    };
    mod_ajax.getJSONP = function (url, data, callback) {
        //        var req = mod_ajax.ajax({
        //            url : url,
        //            data : data,
        //            dataType : "jsonp",
        //            timeout : config.timeout
        //        });
        //
        //        req.success(callback);
        //        req.error(callback);
        return mod_ajax.get(url, data, callback, "jsonp");
    };
    mod_ajax.getJSON = function (url, data, callback) {
        return mod_ajax.get(url, data, callback, "json");
    };
    return mod_ajax;
});

var TimesApps = window.TimesApps || {};
TimesApps.checkGdprAndCall = function(callback, callbackForEu, checkForUserConsent){
    var geoContCookieName = 'geo_continent';
    
    if ( 
        window.TimesGDPR 
        && TimesGDPR.common.consentModule.gdprCallback
    ){
        TimesGDPR.common.consentModule.gdprCallback(function(data){
            if(data && data.isEUuser && typeof callbackForEu == "function" ){
                callbackForEu();
            }else if( !data.isEUuser && typeof callback == "function" ){
                callback();
            }
        });
    }else if( window.self != window.parent ){
        /*
        * current window is a TOI iframe
        * & doesn't has gdpr_js
        * checking cookie for EU
        */
        var continent = ( toiprops && toiprops.cookie && toiprops.cookie.get(geoContCookieName) ) || "";
        var isEUuser = continent.toUpperCase() === "EU";

        if(isEUuser && typeof callbackForEu == "function"){
            callbackForEu();
        }
    }else if(typeof callbackForEu == "function"){
        callbackForEu();
    }
}


define('tiljs/user',["./util", "./ajax", "./cookie", "./event", "jquery", "./is", "./logger", "./string"], function (util, ajax, cookie, event, $, is, logger, string) {
    var mod_user = {},
        /*__default_user = {
         uuid: util.uuid(),
         firstName: "",
         lastName: ""
         },*/
        User = function (attr, mapping) {
            var prvt = {},
                tthis = this;
            if (mapping && is.object(mapping)) {
                attr = util.mapObj(mapping, attr);
            }
            util.each(attr, function (k, v) {
                prvt[k] = v;
                var kCamelCase = string.camelCase(k);
                //            if(setter === true){
                //                tthis['set'+kCamelCase] = function(kk){return prvt[k] = kk;};
                //            }
                tthis['get' + kCamelCase] = function () {
                    return prvt[k];
                };
            });
            this.getMappedUser = function () {
                return prvt; //todo return clone.
            };
            this.getOriginalUser = function () {
                return attr; //todo return clone.
            };
            this.toString = this.toJson = function () {
                return util.stringify(prvt, true, "\t");
            };
        };
    mod_user.getNewUser = function (default_user, mapping) {
        if (default_user instanceof User) {
            return default_user;
        } else if (is.object(default_user)) {
            return new User(default_user, mapping);
        } else {
            return null;
        }
    };
    mod_user.isUser = function (obj) {
        return obj instanceof User;
    };
    return mod_user;
});

/**
 * 'page' module.
 * //todo find better name - it is not generic file
 * @module page
 * @requires util
 */
define('tiljs/page',["module", "./util", "jquery"], function (module, util, $) {
    var default_config = {
        msid: window.msid,
        channel: "",
        siteId: "",
        domain: ""
    };
    var config = util.extend(true, {}, default_config, module.config());
    var mod_page = {};
    mod_page.getMsid = function (url) {
        try {
            if (!url || url.length === 0 || url === "#") {
                url = location.href;
                //                return /\/(\d*)\.cms/.exec(url)[1];
                return /(\d*)\.cms/.exec(url)[1];
            } else {
                return config.msid;
            }
        } catch (e) {
            return config.msid;
        }
    }
    mod_page.getSiteId = function () {
        return config.siteId;
    }
    mod_page.getDomain = function () {
        return config.domain;
    }
    mod_page.getChannel = function () {
        return config.channel;
    }
    mod_page.getMeta = function (property) {
        if (property) {
            return $("meta[property='" + property + "']").attr("content");
        } else {
            var meta = {};
            $("meta[property]").each(function (k, v) {
                var metaElement = $(v);
                meta[metaElement.attr("property")] = metaElement.attr("content");
            });
            return meta;
        }
    }
    return mod_page;
});

define('tiljs/analytics/mytimes',["module", "../ajax", "../util"], function (module, ajax, util) {
    var mod_mytimes = {};
    var default_config = {
        url: tLoginObj.myTimesUrl+"/mytimes/",
        //        js_url: "http://mytest.indiatimes.com/mytimes/" //test url
        appKey: ""
    };
    var config = util.extend(true, {}, default_config, module.config());
    mod_mytimes.log = function (url, params) {
        var req = new Image();
        req.src = url + "?" + ajax.param(params);
    };
    mod_mytimes.logActivity = function (args) {
        var _url = config.url + "addActivity";
        var params = util.extend(true, {
            //            appKey: config.appKey,
            //            uniqueAppID: 25416336,
            //            activityType: "Shared",
            //            baseEntityType: "ARTICLE",
            //            objectType: "B",
            //            url: url,
            //            via: "facebook"
        }, args);
        mod_mytimes.log(_url, params);
    };
    mod_mytimes.logCommentActivity = function (args) {
        var params = util.extend(true, {
            appKey: config.appKey,
            parentCommentId: 0,
            activityType: "", //Agreed, Disagreed, Reccomended
            baseEntityType: "ARTICLE",
            objectType: "A",
            url: document.location.href
        }, args);
        mod_mytimes.logActivity(params);
    };
    mod_mytimes.agreeComment = function (msid) {
        var params = {
            parentCommentId: msid,
            activityType: "Agreed"
        };
        mod_mytimes.logCommentActivity(params);
    };
    mod_mytimes.disagreeComment = function (msid) {
        var params = {
            parentCommentId: msid,
            activityType: "Disagreed"
        };
        mod_mytimes.logCommentActivity(params);
    };
    mod_mytimes.recommendComment = function (msid) {
        var params = {
            parentCommentId: msid,
            activityType: "Reccomended"
        };
        mod_mytimes.logCommentActivity(params);
    };
    mod_mytimes.logShareCount = function (entityId, via) {
        var url = config.url + "entityCount";
        var params = {
            appKey: config.appKey,
            via: via,
            entityId: entityId
        };
        mod_mytimes.log(url, params);
    };
    mod_mytimes.getShareCount = function (msids, callback) {
        var url = config.url + "sharedEntity";
        var params = {
            appKey: config.appKey,
            msids: msids
        };
        //        $.getJSON(url,params,callback);
        ajax.ajax({
            type: 'GET',
            url: url,
            jsonpCallback: 'getShareCount' + Math.random() * 1000000000000000000,
            dataType: 'jsonp',
            data: params,
            success: function (res) {
                var result = res && res.length > 0 ? res[0] : null;
                if (result && callback) {
                    if (callback) {
                        callback(result);
                    } else {
                        callback(null);
                    }
                }
            },
            error: callback
        });
    };
    mod_mytimes.getNotifications = function (callback) {
        var url = config.url + "notification";
        var params = {
            appKey: config.appKey,
            openNetworkId: "sso",
            size: 100,
            lastSeenId: 0,
            after: true
        };
        //        $.getJSON(url,params,callback);
        ajax.ajax({
            type: 'GET',
            url: url,
            jsonpCallback: 'getNotifications' + Math.random() * 1000000000000000000,
            dataType: 'jsonp',
            data: params,
            success: callback,
            error: callback
        });
    };
    mod_mytimes.followUser = function (userId, callback) {
        ajax.getJSONP(config.url + "followuser", {
            userId: userId,
            fromMyTimes: true
        }, callback);
    };
    mod_mytimes.unfollowUser = function (userId, callback) {
        ajax.getJSONP(config.url + "unfollowuser", {
            userId: userId,
            fromMyTimes: true
        }, callback);
    };
    mod_mytimes.getFollowers = function (callback) {
        ajax.getJSONP(config.url + "activity/myfriends", {
            openNetworkId: "sso",
            /*appKey: "", */ size: -1,
            lastSeenId: 0,
            after: true,
            type: "follower"
        }, callback);
    };
    mod_mytimes.getFollowee = function (callback) {
        ajax.getJSONP(config.url + "activity/myfriends", {
            openNetworkId: "sso",
            /*appKey: "",*/ size: -1,
            lastSeenId: 0,
            after: true,
            type: "followee"
        }, callback);
    };
    mod_mytimes.followUser = function (userId, callback) {
        ajax.getJSONP(config.url + "followuser", {
            userId: userId,
            fromMyTimes: true,
            medium: 'WEB'
        }, callback);
    };
    mod_mytimes.updateUserCity = function (userCity, callback) {
        ajax.getJSONP(config.url + "/profile/update?city=" + userCity, {
            openNetworkId: "sso",
            /*appKey: "",*/ size: -1,
            lastSeenId: 0,
            after: true
        }, callback);
    };
    return mod_mytimes;
});
define('tiljs/apps/times/api',[
    "module",
    "../../util",
    "../../ajax",
    "../../cookie",
    "../../event",
    "../../string",
    "../../page",
    "../../analytics/mytimes"
], function (module, util, ajax, cookie, event, string, page, mytimes) { //todo remove mytimes dependency
    var mod_api = {};
    var default_config = {
        ticket: {
            url: tLoginObj.ssoUrl+"/sso/crossdomain/getTicket"
        },
        usersInfo: {
            //url: "http://myt.indiatimes.com/mytimes/profile/info",
            url: tLoginObj.myTimesUrl+"/mytimes/profile/info/v1/",
            params: {
                ssoid: "" // comma separated ssoids(emailid)
            }
        },
        badges: {
            url: "https://rewards.indiatimes.com/bp/api/urs/mubhtry",
            params: {
                format: "json",
                pcode: page.getChannel(),
                uid: "" // comma separated ssoids(emailid)
            }
        },
        rewards: {
            url: "https://rewards.indiatimes.com/bp/api/urs/ups",
            params: {
                format: "json",
                pcode: page.getChannel(),
                uid: "" // comma separated ssoids(emailid)
            }
        },
        comments: {
            //todo remove
            url: document.location.host == "test.indiatimes.com" || document.location.host == "test.happytrips.com" ? "lib/getComments.php" : "/json/new_cmtofart2_nit_v1.cms",
            type: "json",
            //            url: function(params){return "data/"+params.msid+".comments.json"},
            //            url: function(params){return "/json/test_comments/"+params.msid+".cms"},
            params: {
                msid: "",
                curpg: 1,
                pcode: page.getChannel()
                //                commenttype: "mostrecommended",
                //                sorttype: "bycount",
                //                ordertype: "asc"
                /*
                 &ordertype=asc
                 &commenttype=mostrecommended&sorttype=bycount
                 &commenttype=mostdiscussed
                 &commenttype=agree&sorttype=bycount
                 &commenttype=disagree&sorttype=bycount
                 */
            }
        }
    };
    default_config.comments_newest = default_config.comments;
    default_config.comments_oldest = {
        url: default_config.comments.url,
        type: default_config.comments.type,
        params: {
            msid: "",
            curpg: 1,
            ordertype: "asc",
            pcode: page.getChannel()
        }
    };
    default_config.comments_recommended = {
        url: default_config.comments.url,
        type: default_config.comments.type,
        params: {
            msid: "",
            curpg: 1,
            commenttype: "mostrecommended"
            //            ,sorttype: "bycount"
            ,
            pcode: page.getChannel()
        }
    };
    default_config.comments_discussed = {
        url: default_config.comments.url,
        type: default_config.comments.type,
        params: {
            msid: "",
            curpg: 1,
            commenttype: "mostdiscussed"
            //            ,sorttype: "bycount"
            ,
            pcode: page.getChannel()
        }
    };
    default_config.comments_agree = {
        url: default_config.comments.url,
        type: default_config.comments.type,
        params: {
            msid: "",
            curpg: 1,
            commenttype: "agree"
            //            ,sorttype: "bycount"
            ,
            pcode: page.getChannel()
        }
    };
    default_config.comments_disagree = {
        url: default_config.comments.url,
        type: default_config.comments.type,
        params: {
            msid: "",
            curpg: 1,
            commenttype: "disagree"
            //            ,sorttype: "bycount"
            ,
            pcode: page.getChannel()
        }
    };
    default_config.validate_comment = {
        url: "/validatecomment.cms",
        type: "html",
        params: {
            //hostid:83,//259:travel
            //rchid:-2128958273,//2147477992:travel
            fromname: null,
            fromaddress: null,
            userid: null,
            location: null,
            imageurl: null,
            loggedstatus: null,
            message: null,
            roaltdetails: null,
            ArticleID: null,
            msid: null,
            parentid: null,
            rootid: null
        }
    };
    default_config.post_comment = {
        url: "/postro.cms",
        type: "html",
        params: {
            //hostid:83,//259:travel
            //rchid:-2128958273,//2147477992:travel
            fromname: null,
            fromaddress: null,
            userid: null,
            location: null,
            imageurl: null,
            loggedstatus: null,
            message: null,
            roaltdetails: null,
            ArticleID: null,
            msid: null,
            parentid: null,
            rootid: null
        }
    };
    default_config.post_comment_withoutverification = {
        url: "/postro_nover.cms",
        type: "html",
        params: {
            //hostid:83,//259:travel
            //rchid:-2128958273,//2147477992:travel
            fromname: null,
            fromaddress: null,
            userid: null,
            location: null,
            imageurl: null,
            loggedstatus: null,
            message: null,
            roaltdetails: null,
            ArticleID: null,
            msid: null,
            parentid: null,
            rootid: null
        }
    };

    default_config.post_comment_withverification = {
        url: "/postroemailverification.cms",
        type: "html",
        params: {
            //hostid:83,//259:travel
            //rchid:-2128958273,//2147477992:travel
            fromname: null,
            fromaddress: null,
            userid: null,
            location: null,
            imageurl: null,
            loggedstatus: null,
            message: null,
            roaltdetails: null,
            ArticleID: null,
            msid: null,
            parentid: null,
            rootid: null
        }
    };
    default_config.rate = {
        //http://timesofindia.indiatimes.com/rate_techreview.cms?msid=44823267&getuserrating=1&criticrating=9&vote=8
        url: "/rate_techreview.cms",
        type: "html",
        params: {
            msid: null,
            getuserrating: null,
            vote: null,
            criticrating: null
        }
    };

    default_config.rate_comment = {
        url: "/ratecomment_new.cms",
        type: "html",
        params: {
            opinionid: null,
            typeid: null,
            rateid: null
        }
    };
    default_config.rate_comment_offensive = {
        url: "/offensive/mark",
        type: "html",
        params: {
            ofusername: null,
            ofreason: "NONE",
            ofcommenteroid: null,
            ofcommenthostid: 83,
            ofcommentchannelid: -2128958273,
            ofcommentid: null,
            ofuserisloggedin: null,
            ofuserssoid: null,
            ofuseremail: null
        }
    };
    var config = util.extend(true, {}, default_config, module.config());
    mod_api.updateConfig = function (new_config) {
        config = util.extend(true, config, new_config);
    };
    mod_api.getConfig = function (new_config) {
        return config;
    };
    mod_api.getTicket = function (_params, callback) {
        return mod_api.api("ticket", _params, callback);
    };
    mod_api.getUsersInfo = function (_params, callback) {
        return mod_api.api("usersInfo", {ssoid: _params.ssoids}, callback);
    };
    mod_api.getBadges = function (_params, callback) {
        return mod_api.api("badges", _params, callback);
    };
    mod_api.getRewards = function (_params, callback) {
        return mod_api.api("rewards", _params, callback);
    };
    mod_api.getComments = mod_api.getComments_newest = function (_params, callback) {
        return mod_api.api("comments", _params, callback);
    };
    mod_api.getComments_oldest = function (_params, callback) {
        return mod_api.api("comments_oldest", _params, callback);
    };
    mod_api.getComments_recommended = function (_params, callback) {
        return mod_api.api("comments_recommended", _params, callback);
    };
    mod_api.getComments_discussed = function (_params, callback) {
        return mod_api.api("comments_discussed", _params, callback);
    };
    mod_api.getComments_agree = function (_params, callback) {
        return mod_api.api("comments_agree", _params, callback);
    };
    mod_api.getComments_disagree = function (_params, callback) {
        return mod_api.api("comments_disagree", _params, callback);
    };
    mod_api.validateComment = function (_params, callback) {
        return mod_api.post("validate_comment", _params, callback);
    };
    mod_api.postComment = function (_params, callback) {
        return mod_api.post("post_comment", _params, callback);
    };
    mod_api.postCommentWithoutVerification = function (_params, callback) {
        return mod_api.post("post_comment_withoutverification", _params, callback);
    };
    mod_api.postCommentWithVerification = function (_params, callback) {
        return mod_api.post("post_comment_withverification", _params, callback);
    };

    /**
     *
     *
     *
     * @param rating msid, user_rating, critic_rating, rating
     * @param callback
     * @param user
     * @returns {*}
     */
    mod_api.rate = function (rating, callback, user) {
        var rating_u = {
            msid: rating.msid,
            getuserrating: rating.user_rating,
            vote: rating.rating,
            criticrating: rating.critic_rating
        };
        return mod_api.api("rate", rating_u, callback);
    };

    mod_api.rateComment = function (rating, callback, user) {
        if (user) { //TODO move all calls to backend
            switch (rating.typeid) {
                case 100: //Agree
                    mytimes.agreeComment(rating.opinionid);
                    break;
                case 101: //Disagree
                    mytimes.disagreeComment(rating.opinionid);
                    break;
                case 102: //Recommend
                    mytimes.recommendComment(rating.opinionid);
                    break;
                case 103: //Offensive
                    mod_api.rateCommentOffensive(rating, user); //todo check if mytimes request is to be sent
                    break;
            }
        }
        //http://myt.indiatimes.com/mytimes/addActivity?activityType=Agreed&appKey=TOI&parentCommentId=28368882&baseEntityType=ARTICLE&objectType=A&url=
        //http://myt.indiatimes.com/mytimes/addActivity?activityType=Disagreed&appKey=TOI&parentCommentId=28368547&baseEntityType=ARTICLE&objectType=A&url=
        //http://myt.indiatimes.com/mytimes/addActivity?activityType=Reccomended&appKey=TOI&parentCommentId=28368541&baseEntityType=ARTICLE&objectType=A&url=
        return mod_api.api("rate_comment", rating, callback);
    };
    mod_api.rateCommentOffensive = function (rating, user, callback) {
        //OLD http://timesofindia.indiatimes.com/offensiveService/offence.asmx/getOffencivecomment?ofusername=Del%20Sanic&ofreason=Others:%20testing&ofcommenteroid=28231992&ofcommenthostid=83&ofcommentchannelid=-2128958273&ofcommentid=30937855&ofuserisloggedin=1&ofuserssoid=delsanic@gmail.com&ofuseremail=delsanic@gmail.com
        //http://timesofindia.indiatimes.com/offensive/mark?ofusername=Del%20Sanic&ofreason=Others:%20testing&ofcommenteroid=28232011&ofcommenthostid=83&ofcommentchannelid=-2128958273&ofcommentid=30937855&ofuserisloggedin=1&ofuserssoid=delsanic@gmail.com&ofuseremail=delsanic@gmail.com
        if (user) {
            var params = {};
            params.ofcommenteroid = rating.opinionid;
            params.ofcommentid = window.msid;
            params.ofuserisloggedin = 1;
            params.ofuserssoid = params.ofuseremail = user.getEmail();
            params.ofusername = user.getFullName();
            //            params = util.extend(true,{}, params, rating) ;
            return mod_api.api("rate_comment_offensive", params, callback);
        } else {
            event.publish("logger.error", "Cannot rate comment offensive, user not available.");
        }
    };
    mod_api.api = function (api, _params, callback) {
        var value = config[api];
        return ajax.get(util.val(value.url, value.params), util.extend(true, {}, value.params, _params), function (data) {
            if (callback) {
                try {
                    callback(data);
                } catch (e) {
                    event.publish("logger.error", e.stack);
                }
            }
        }, value.type || "jsonp").error(function () {
            if (callback) {
                try {
                    callback();
                } catch (e) {
                    event.publish("logger.error", e.stack);
                }
            }
        });
    };
    mod_api.post = function (api, _params, callback) {
        var value = config[api];
        return ajax.post(util.val(value.url, value.params), util.extend(true, {}, value.params, _params), function (data) {
            if (callback) {
                try {
                    callback(data);
                } catch (e) {
                    event.publish("logger.error", e.stack);
                }
            }
        }).error(function (event, xhr, e) {
            event.publish("comment.post.error", e);
        });
    };
    mod_api.get = function (key, params) { //todo change method name
        if (params) {
            var api = config[key];
            var _params = util.extend(true, {}, api.params, params);
            return {
                url: util.val(api.url, _params),
                params: _params
            };
        } else {
            return config[key];
        }
    };
    mod_api.init = function () {
        util.each(config, function (key, value) {
            var funName = string.camelCase("get " + key);
            mod_api[funName] = function (_params, callback) {
                ajax.getJSONP(util.val(value.url, value.params), util.extend(true, {}, value.params, _params), function (data) {
                    if (callback) {
                        callback(data);
                    }
                });
            };
        });
    };
    //    mod_api.init();
    return mod_api;
});

/**
 * 'ui' module.
 *
 * @module ui
 * @requires util
 * @requires logger
 * @requires event
 * @requires jquery
 * @requires is
 */
define('tiljs/ui',["./util", "./logger", "./event", "jquery", "./is", "./timer"], function (util, logger, event, $, is, timer) {
    logger.log("ui loaded");
    var mod_ui = {},
        open_window = {},
        mask, maskConfig;

    mod_ui.window = function (url, options) {
        logger.log("ui.window called");
        var default_options, opt, x, y, params, popup;
        default_options = {
            width: 300,
            height: 300,
            name: "Window",
            mask: true,
            resizable: false,
            disableScroll: false,
            closeCallback: function () {
                logger.log("Empty ui.window callback function.");
            }
        };
        opt = util.extend(true, {}, default_options, options);

        x = window.screen.width / 2 - opt.width / 2;
        y = window.screen.height / 2 - opt.height / 2;

        params = ['width=' + opt.width,
            'height=' + opt.height,
            'left=' + (is.defined(opt.left) ? opt.left : x),
            'top=' + (is.defined(opt.top) ? opt.top : (y - 20)),
            'scrollbars=' + (is.defined(opt.scrollbars) && (opt.scrollbars == 1) ? opt.scrollbars : 0),
            'resizable=' + opt.resizable];

        if (opt.mask === true && is.desktop()) {
            mod_ui.mask(null, {}, false);
        }
        if (opt.disableScroll === true) {
            mod_ui.disableScroll();
        }

        popup = window.open(url, opt.name, params.join(","));
        if (popup) {
            popup.focus();
            logger.log("Popup opened: " + url);
            logger.log(popup);
        }

//        popup.onresize = function(){
//            popup.resizeTo(opt.width,opt.height);
//        };


        try {
            //Exception "Permission Denied" in IE, using try catch so that this functionality works in other browsers.
            popup.reposition = function () {
                var x = window.screen.width / 2 - opt.width / 2,
                    y = (window.screen.height / 2 - opt.height / 2);
                popup.moveTo(x, y);
            };
        } catch (e) {
            logger.warn("Handled Exception in IE10.");
            logger.error(e);
        }

        if (!popup) {
            alert("Popups are blocked. Please enable them.");//TODO find better way
            logger.error("Popups are blocked. Please enable them.");
            mod_ui.unmask();
            return popup;
        }
        //TODO use timer module, prevent recursive dependency
        (function (popup, url, opt) {
            var interval = window.setInterval(function () {   //todo use timer
                try {
                    logger.log("Checking if popup is closed:" + url + ":" + popup.closed);
                    if (popup === null || popup.closed !== false) {
                        mod_ui.window.close(opt.name);
                    }
                }
                catch (e) {
                    logger.warn("Handled exception while closing popup.");
                    logger.error(e.stack);
                    window.clearInterval(interval);
                    interval = null;
                }
            }, 500);

            open_window[opt.name] = {opt: opt, popup: popup, interval: interval, url: url};

        }(popup, url, opt));


        return popup;
    };

    mod_ui.window.close = function (name, all) {
        if (!name) {
            logger.error("Window name is required to close it.");
            return;
        }
        var popup = open_window[name];
        if (all === true) {
            util.each(open_window, function (k) {
                mod_ui.window.close(k);
            });
        } else if (popup) {
            logger.log("Closing popup is closed:" + popup.opt.name + ":" + popup.url);

            if (popup.opt.mask === true) {
                mod_ui.unmask();
            }
            if (popup.opt.disableScroll === true) {
                mod_ui.enableScroll();
            }

            window.clearInterval(popup.interval);
            popup.interval = null;
            popup.popup.close();
            open_window[popup.opt.name] = null;
            popup.opt.closeCallback(popup.popup);
        } else {
            logger.warn("Popup '" + name + "' not found.");
        }
    };

    if (window.closeWindow) {
        logger.warn("window.closeWindow is already defined, open windows may not close properly.");
    } else {
        window.closeWindow = mod_ui.window.close;
    }

    mod_ui.iframe = function (url, options) {
        var default_options = {
                width: 300,
                height: 300,
                name: "Window",
                disablePopup: true,
                closeCallback: function () {
                    logger.log("Empty ui.iframe callback function.");
                }
            },

            opt = util.extend(true, {}, default_options, options),
            iframe = $("<iframe class='loginsignupframe'></iframe>"),//todo fix class dependency
            _iframe, popup;

        iframe.attr("src", url);
        iframe.css({
            width: "100%",
            height: "100%",
            border: "4px solid #cccccc",
            backgroundColor: "#FFF"
        });

        _iframe = iframe[0];

        popup = mod_ui.popup(iframe, opt);


        _iframe.close = function () {
            iframe.remove();
            popup.remove();
            mod_ui.unmask();
        };

        _iframe.resizeTo = function (width, height) {
            if (width && height) {
                popup.width(width);
                popup.height(height);
            }
        };

        _iframe.reposition = popup.reposition;

        return _iframe;
    };

    mod_ui.closeButton = function (clickCallback) {
        var closeButton = $("<span></span>");
        closeButton.on("click", clickCallback);
        return closeButton;
    };

    mod_ui.popup = function (html, options) {
        var default_options, closeButton, mask, opt, popup;
        default_options = {
            width: 300,
            height: 300,
            name: "Window",
            closeCallback: function () {
                logger.log("Empty ui.popup callback function.");
            },
            disableScroll: false
        };

        opt = util.extend(true, {}, default_options, options);
        popup = $("<div></div>");
        popup.width(opt.width);
        popup.height(opt.height);
        popup.css({
//            margin:"0 auto",
//            marginTop:"5%",
            position: "absolute",
            zIndex: maskConfig.zIndex + 1
        });
        popup.append(html);

        if (opt.disableScroll === true) {
            mod_ui.disableScroll();
        }

        if (opt.closeCallback) {
            if (window.closePopup) {
                logger.warn("'window.closePopup' is already defined.");
            }
            if (window.disablePopup) {
                logger.warn("'window.disablePopup' is already defined.");
            }

            window.closePopup = window.disablePopup = function () {
                popup.remove();
                mod_ui.unmask();

                if (opt.disableScroll === true) {
                    mod_ui.enableScroll();
                }

                if (opt.closeCallback) {
                    opt.closeCallback();
                }
                window.closePopup = null;
                window.disablePopup = null;
            };

            closeButton = mod_ui.closeButton(window.closePopup);

            closeButton.addClass("popup_close_button");
            //todo remove css from here
            closeButton.css({
                position: "absolute",
                top: "5px",
                right: "5px",
                cursor: "pointer",
                background: "url('https://timesofindia.indiatimes.com/photo/25494620.cms') no-repeat scroll -160px -20px rgba(0, 0, 0, 0)",
                width: "18px",
                height: "18px"
            });

            popup.append(closeButton);
        }

        $("body").append(popup);

        mask = mod_ui.mask(popup, {}, false);

        popup.reposition = function () {
            var $w = $(window),
                windowWidth = $w.width(),
                windowHeight = $w.height(),

                calcWidth = ((windowWidth / 2) - (popup.width() / 2)),
                calcHeight = ((windowHeight / 2) - (popup.height() / 2));

            if (calcWidth < 0) {
                calcWidth = 0;
            }
            if (calcHeight < 0) {
                calcHeight = 0;
            }
            popup.css({
                left: calcWidth,
                top: calcHeight + $(window).scrollTop()
            });
        };

        popup.reposition();

        popup.close = function () {
            popup.remove();
            mod_ui.unmask();
        };

        event.subscribe("window.resize", function () {
            popup.reposition();
        });


        return popup;
    };


    mod_ui.img = function (url, attr) {
        var img = $("<img>");
        img.attr("src", url);
        util.each(attr, function (k, v) {
            img.attr(k, v);
        });
        return img;
    };


    mod_ui.anchor = function (url, text, attr) {
        var anchor = $("<a>");
        anchor.attr("href", url);
        anchor.text(text);
        util.each(attr, function (k, v) {
            anchor.attr(k, v);
        });
        return anchor;
    };

    /**
     * Checks if the provided element is in viewport.
     *
     * //todo create event 'inview' with callback which fires when element comes in view
     *
     * @param {selector/instance} elem element to be checked
     * @param {boolean} partial  when true,returns true when element is partially in view.
     * @param {int} skew Pixels to skew/move view from the top
     * @returns {boolean} true if element is in view
     */
    mod_ui.inView = function (elem, partial, skew) {
        skew = skew || 0;

        var $w = $(window), $e = $(elem),
            docViewTop, docViewBottom, elemTop, elemBottom, in_view;

        if ($e.is(":hidden")) { // Element is hidden so its not in the view
            return false;
        }

        docViewTop = $w.scrollTop() - skew; //todo - skew
        docViewBottom = docViewTop + $w.height() + skew;//todo  + skew

        elemTop = $e.offset() ? $e.offset().top : 0;
        elemBottom = elemTop + $e.height();


        in_view = false;

        if (partial === true) {
            in_view = ((elemBottom > docViewTop ) && (elemTop <= docViewBottom ));
//            if(in_view){
//                logger.log("P:"+elemBottom +">"+docViewTop+","+elemTop+"<="+docViewBottom);
//            }
        } else {
            in_view = ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
//            if(in_view){
//                logger.log(elemBottom +"<="+docViewBottom+","+elemTop+">="+docViewTop);
//            }
        }

        return in_view;
    };


    mod_ui.init = function () {
        var th_scroll = util.throttle(500, function (e) {
                event.publish("window.scroll", e);
            }),

            th_load = function (e) {
                event.publish("window.load", e);
            },

            th_resize = util.throttle(500, function (e) {
                event.publish("window.resize", e);
            });

        if (window.addEventListener) {  // W3C DOM
            window.addEventListener("scroll", th_scroll, false);
            window.addEventListener("load", th_load, false);
            window.addEventListener("resize", th_resize, false);
        } else if (window.attachEvent) { // IE DOM
            window.attachEvent("onscroll", th_scroll);
            window.attachEvent("onload", th_load);
            window.attachEvent("onresize", th_resize);
        }

        $(function (e) {
            event.publish("document.ready", e);
        });

    };


    mod_ui.getGravatar = function (email, size) {
        if (!email || email.length === 0) {
            return null;
        }

        size = size || 80;

        return 'http://www.gravatar.com/avatar/' + util.md5(email.trim().toLowerCase()) + '.jpg?s=' + size;
    };

    mod_ui.maxlength = function (input, max, callback, parent, alertMsg) {

        $(parent || "body").off("keyup change", input);
        $(parent || "body").on("keyup change", input, function () {
            var val = $(this).val(),
                len = val.length;
            max = parseInt(max, 10);

            callback.call(this, max - len, len);
            if (len > max) {
                $(this).val(val.substr(0, max));
                if (alertMsg) {
                    window.alert(alertMsg);
                }
                return false;
            }
        });
    };

    mod_ui.getActionReferences = function (currentEle, mainParent) {
        var obj = {current: $(currentEle)};
        obj.parent = mainParent ? obj.current.parents(mainParent) : obj.current;

        obj.parent.find("[data-plugin]").each(function (i, v) {
            obj[$(v).attr("data-plugin")] = $(this);
        });

        return obj;
    };

    mask = null;
    maskConfig = {
        position: "fixed",
        left: 0,
        top: 0,
        width: "100%",
        height: "100%",
        backgroundColor: "#000",
        opacity: 0.5,
        zIndex: 99999
    };

    /**
     *
     * @param config to be provided once on a page.
     * @returns {null}
     * @param divEle Element to be shown above the mask
     * @param closeOnClick (default = false) If true, mask will be closed when clicked
     */
    mod_ui.mask = function (divEle, config, closeOnClick) {
        if (!mask) {
            mask = $("<div></div>");
            mask.popup = divEle;
            mask.css(util.extend(maskConfig, config));
//            mask.css("height", $(document).height());
            if (mask.popup) {
                mask.popup.show();
            }
            if (typeof closeOnClick === 'undefined' || closeOnClick === true) {
                mask.on("click", function () {
                    if (mask.popup) {
                        mask.popup.hide();
                    }
                    mod_ui.unmask();
                });
            }
            $(document).one("keyup", function (e) {     //todo move to common
                if (e.keyCode === 27) {
                    if (mask) {
                        if (mask.popup) {
                            mask.popup.hide();
                        }
                        mod_ui.unmask();
                    }
                }
            });
            $("body").append(mask);
        } else {
            logger.warn("Already masked. Use ui.unmask() before masking again.");
        }
        return mask;
    };

    mod_ui.isMasked = function () {
        return mask ? true : false;
    };

    mod_ui.unmask = function () {
        if (mask) {
            if (mask.popup) {
                mask.popup.hide();
            }
            mask.remove();
            mask = null;
        } else {
            logger.warn("No mask available. Use ui.mask() before unmasking.");
        }
    };

    mod_ui.tooltip = function (message, hideAfter) {
        hideAfter = hideAfter || 5;

        var tooltip = $("<div></div>");
        if (hideAfter > 0) {
            timer.after(function () {
                tooltip.fadeOut(function () {

                });

            });
        }

        return tooltip;
    };

    mod_ui.dialog = function (divEle) {
        //var default_params = {},
        //prms = util.extend(true,{}, default_params, params),

        var dialog = $("<div></div>");
        dialog.css({
            backgroundColor: "#FFF"
        });

        dialog.html(divEle.html());

        return dialog;
    };

    mod_ui.customScrollbar = function (ele) {
        if (window.$ && window.$ && window.$.fn && window.$.fn.mCustomScrollbar) {
            ele = window.$(ele);
            ele.mCustomScrollbar("destroy");
            ele.mCustomScrollbar({
                scrollButtons: {
                    enable: true
                },
                advanced: {
                    updateOnContentResize: true,
                    updateOnBrowserResize: true
                }
            });
        } else {
            logger.warn("mCustomScrollbar not found.");
        }
    };

    mod_ui.disableScroll = function (mainContainer) {
        $("html,body").css({overflow: "hidden", height: "100%", padding: "0", margin: "0"});
        $(mainContainer || "#container").css({overflowY: "scroll", height: "100%"});
    };

    mod_ui.enableScroll = function (mainContainer) {
        $("html,body").css({overflow: "", height: "", padding: "", margin: ""});
        $(mainContainer || "#container").css({overflowY: "", height: ""});
    };

    mod_ui.init();


    return mod_ui;
});

/**
 * 'load' module.
 *
 * @module load
 * @requires event
 * @requires logger
 */
define('tiljs/load',["./event", "./logger", "./util", "./is"], function (event, logger, util, is) {
    logger.log("load loaded");
    //    var LOADING = 0;
    //    var LOADED = 1;
    var mod_load = {};
    //    mod_load.js_queue = {};
    /**
     * Load javascript file on a page
     *
     * @memberOf module:load#
     * @function js
     * @param {String} url URL of the javascript file
     * @param {Function} [callback] function to be called when js is loaded
     * @param {String} [id] id to be given to the javascript tag
     * @param {Boolean} [async] true by default, set false to load synchronously
     * @returns {HTMLElement} generated script tag element
     * @example
     *  require(['load'],function(load){
	 *     //Load javascript 'abc.js'
	 *     load.js('abc.js',function(){
	 *        console.log('abc.js loaded');
	 *     });
	 *
	 *     //Load javascript 'abc.js' , the script element is assigned provided id
	 *     load.js('abc.js',function(){
	 *        console.log('abc.js loaded');
	 *     },'element_id',true);
	 *  });
     */
    mod_load.js = function (url, callback, id, async) {
        //TODO implement queue - prevent multiple js loading
        //        id = id || url.replace(/[^\w\s]/gi, ''); //todo BAD hack, implement queue
        //        var script =  document.getElementById(id);
        //        if(script && script.loaded){
        //            if(callback){
        //                callback();
        //            }
        //            return script;
        //        }
        var head = document.getElementsByTagName("head") ? document.getElementsByTagName("head")[0] : null;
        if (head) {
            var script = document.createElement("script");
            var done = false; // Handle Script loading
            if (id) {
                script.id = id;
            }
            if (async) {
                script.async = async;
            }
            if (!url) {
                throw new Error("Param 'url' not defined.");
            }
            script.src = url;
            script.onload = script.onreadystatechange = function () { // Attach handlers for all browsers
                if (!script.loaded && ( !this.readyState || this.readyState === "loaded" || this.readyState === "complete" )) {
                    script.loaded = true;
                    var endTime = new Date().getTime();
                    var timeSpent = endTime - script.startTime;
                    event.publish("load.js", ['_trackTiming', 'js', url, timeSpent, url]);
                    //                        track.ga(['_trackTiming', 'js', url , timeSpent, url ]);//todo identify label(last param)
                    if (callback) {
                        try {
                            callback();
                        } catch (e) { //to handle
                            event.publish("logger.error", e.stack);
                        }
                    }
                    script.onload = script.onreadystatechange = null; // Handle memory leak in IE
                }
            };
            script.startTime = new Date().getTime();
            head.appendChild(script);
            return script;
        } else {
            logger.info("Head Element not found. JS '" + url + "' not loaded. ");
            return null;
        }
    };
    /**
     * Load css file on a page
     *
     * @memberOf module:load#
     * @function css
     *
     * @param {String} href URL of the css file
     * @param {Function} [callback] function to be called when
     * @returns {HTMLElement} generated link tag element
     * @example
     *  require(['load'],function(load){
	 *     //Load stylesheet 'abc.css'
	 *     load.css('abc.css',function(){
	 *        console.log('abc.css loaded');
	 *     });
	 *  });
     */
    mod_load.css = function (href, callback) {
        var headEle = document.getElementsByTagName("head") ? document.getElementsByTagName("head")[0] : null;
        if (headEle) {
            var cssLink = document.createElement("link");
            cssLink.setAttribute("rel", "stylesheet");
            cssLink.setAttribute("type", "text/css");
            cssLink.setAttribute("href", href);
            headEle.appendChild(cssLink);
            if (callback) {
                setTimeout(callback, 0); //TODO find better way
            }
        } else {
            logger.info("Head Element not found. CSS '" + href + "' not loaded. ");
        }
        return cssLink;
    };
    /**
     * Load iframe on a page
     *
     * @memberOf module:load#
     * @function iframe
     *
     * @param {String} src URL of the iframe to be loaded
     * @param {Number} width width of the iframe, defaults to 0
     * @param {Number} height height of the iframe, defaults to 0
     * @param {String} containerId id of the container in which the iframe is to be loaded
     * @returns {HTMLElement} generated iframe element
     * @example
     *  require(['load'],function(load){
	 *     load.iframe('http://www.abc.com',100,100,'container_iframe');
	 *  });
     */
    mod_load.iframe = function (src, width, height, containerId) {
        var container = document.getElementById(containerId) || document.body;
        if (container) {
            var i = document.createElement("iframe");
            i.src = src;
            i.scrolling = "no";
            i.frameBorder = "0";
            i.width = width || 0;
            i.height = height || 0;
            container.appendChild(i);
            return i;
        } else {
            logger.info("Container with id '" + containerId + "' not found. iframe '" + src + "' not loaded. ");
        }
    };
    mod_load.image = function (src) {
        if (is.array(src)) {
            util.each(ajaxLogout, function (i, v) {
                mod_load.image(v);
            });
        } else {
            var img = new Image();
            img.src = src;
        }
    };
    /**
     * //TODO, remove private after implementing it.
     *
     * @private
     */
    mod_load._content = function (url, containerId) {
    };
    /**
     * Checks if the javascript or css is loaded
     *
     * @memberOf module:load#
     * @function isLoaded
     *
     * @param path
     * @returns {boolean}
     * @example
     *
     *  require(['load'],function(load){
	 *     load.isLoaded('abc.js');   // returns true is abc.js is loaded
	 *     load.isLoaded('abc.css');  // returns true is abc.css is loaded
	 *  });
     */
    mod_load.isLoaded = function (path) { //todo import jquery dependency
        return $('script[src="' + path + '"]').length > 0 || $('link[href="' + path + '"]').length > 0;
    };
    return mod_load;
});

/**
 * 'localstorage' module.
 *
 * @module localstorage
 * @requires util
 * @requires json
 */
define('tiljs/localstorage',["module", "./util", "json", "./is", "./logger"], function (module, util, JSON, is, logger) {
    var mod_localstorage = {};
    var default_config = {};
    var config = util.extend(true, {}, default_config, module.config());
    var LS = window.localStorage;
    if (!LS) {
        logger.warn("'localStorage' is not available.");
        LS = {
            getItem: function () {
            },
            removeItem: function () {
            },
            setItem: function () {
            }
        };
    }
    /**
     * Get value of a localstorage
     *
     * @param {String} [name] name of the localstorage for which value is required,
     *                        if name is not provided an object with all localstorages is returned
     * @returns {String | Array} value of the requested localstorage / Array of all localstorages
     */
    mod_localstorage.get = function (name) {
        var val = LS.getItem(name);
        if (val != null) {
            var record = JSON.parse(val);
            if (!record) {
                return null;
            }
            return ( new Date().getTime() < record.timestamp ) ? record.value : null;
        } else {
            return null;
        }
    };
    /**
     * localStorage Set,Get,Delete
     */
    mod_localstorage.getAll = function () {
        return mod_localstorage.get();
    };
    /**
     * Remove a localstorage
     *
     * @param {String} name name of the localstorage to be removed
     * @param {String} [path] path of the localstorage
     * @param {String} [domain] domain of the localstorage
     */
    mod_localstorage.remove = function (name, path, domain) {
        if (name) {
            LS.removeItem(name);
        }
    };
    /**
     * Set a localstorage
     *
     * @param {String} name name of the localstorage to be set
     * @param {String} value value of the localstorage to be set
     * @param {Number} [days] number of days for which the localstorage is to be set
     * @param {String} [path] path of the localstorage to be set
     * @param {String} [domain] domain of the localstorage to be set
     * @param {Boolean} [secure] true if the localstorage is to be set on https only (to be implemented)
     */
    mod_localstorage.set = function (name, value, days, path, domain, secure) {
        var date = new Date();
        var expirationMS = date.getTime() + ( ( days || 365 ) * 24 * 60 * 60 * 1000 );
        var record = {
            value: value,
            timestamp: new Date().getTime() + expirationMS,
            path: path,
            domain: domain
        };
        try {
            LS.setItem(name, JSON.stringify(record));
        } catch (e) {
            //            if (e == 'QUOTA_EXCEEDED_ERR') {
            logger.error('Unable to save item in localStorage:' + name);
            //            }
        }
        //        return value;
    };
    return mod_localstorage;
});

define('api',["tiljs/apps/times/api"], function (api) {
    var mod_api = api;
    return mod_api;
});

define('tiljs/plugin/plugin',["../load", "../util", /* "jquery",*/ "../event", "../ui", "../logger", "../is"], function (load, util, /*$,*/ event, ui, logger, is) {
    var default_config = {
        init: true,
        root: "body",
        dependency: function (callback) {
            //this is config here
            this.dep_data = {};
            if (callback) {
                callback();
            }
        }
    };
    var mod_plugin = function (id, config) {
        if (!id) {
            throw new Error("'id' param is required.");
        }
        this.id = id;
        this.updateConfig(config);
        //        console.log("done");
    };
    mod_plugin.prototype.updateConfig = function (config) {
        this.config = util.extend(true, {}, default_config, config);
    };
    mod_plugin.prototype.find = function () {
        return $(this.config.root).find("[data-plugin='" + this.id + "']");
    };
    mod_plugin.prototype.init = function () {
        event.publish(getEventName("beforeinit"));
        //        var plugins = opt.find();
        //        logger.log(opt.id + " init:" + plugins.length);
        //        if (plugins && plugins.length > 0) {
        //            event.publish(getEventName("beforeloadjs"));
        //            if (opt.js && opt.isJSdependent(plugins)) {
        //                load.js(opt.js, function (options, plugins) {
        //                    return function () {
        //                        event.publish(getEventName("afterloadjs"));
        //                        opt.initPlugins(plugins);
        //                    };
        //                }(opt, plugins), opt.js_id);
        //            } else {
        //                opt.initPlugins(plugins);
        //            }
        //        }
        //        console.log("init" );
        if (is.funct(this.config.dependency)) {
            var tthis = this;
            this.config.dependency(function () {
                tthis.render();
            });
        } else {
            this.render();
        }
    };
    mod_plugin.prototype.render = function () {
        event.publish(getEventName("beforerender"));
        var plugin = this;
        this.find().each(function (i, ele) {
            plugin.renderEach(ele);
        });
        event.publish(getEventName("afterrender"));
    };
    mod_plugin.prototype.renderEach = function (ele) {
        //$(ele).html("yoo hoo");
        //Override this
    };
    var getEventName = function (eventName) {
        return "plugin." + this.id + eventName; //todo replace with ===> "plugin." + this.id + "." + eventName;
    };
    return mod_plugin;
});

define('tiljs/plugin/dynamic',["../timer", "../util", "../plugin/plugin"], function (timer, util, plugin) {
    var mod_uptime = new plugin("dynamic-uptime");
    mod_uptime.uptime = function () {
        var plugins = $("[data-plugin='dynamic-uptime']");
        plugins.each(function (i, p) {
            mod_uptime.renderEach(p);
        });
    };
    mod_uptime.renderEach = function (ele) {
        //        var plugins = $("[data-plugin='dynamic-uptime']");
        //        plugins.each(function(i,p){
        var plugin = $(ele);
        var time = plugin.attr("data-time");
        var elapsedTime = timer.elapsedTime(time, {
            minute: "min",
            second: "sec"
        }, true);
        plugin.html(elapsedTime);
        //        });
    };
    timer.every(60000 /*minute*/, function () {
        mod_uptime.render();
    });
    mod_uptime.init();
    return mod_uptime;
});

define('rodate',[], function () {
    var rodate = {};
    rodate.uptime = function () {
        var plugins = $("[rodate]");
        plugins.each(function (i, p) {
            rodate.renderEach(p);
        });
    };
    rodate.renderEach = function (ele) {
        var plugin = $(ele);
        var time = plugin.attr("rodate");
        var match = time.match(/^(\d+)-(\d+)-(\d+) (\d+)\:(\d+)\:(\d+)$/);
        if (!match) {
            time = !isNaN(time) ? parseInt(time) : time;
            var timeObj = new Date(time);
            if (!isNaN(timeObj.getTime())) {
                rod = timeObj;
            } else {
                if (time.indexOf("hrs IST") != -1) {
                    time = time.replace(" hrs IST", "");
                    time = time.splice(-2, 0, ":"); //21 Apr, 2014, 16:31
                }
                time = time.replace("IST", "");
                time = time.replace("hrs", "");
                time = time.replace("AM", " AM");
                time = time.replace("PM", " PM");
                rod = new Date(time);
                if (isNaN(rod.getTime())) {
                    if (time.indexOf(':') > -1) {
                        time = time.split(' ').join('T');
                        time = time.concat('+05:30');
                        rod = new Date(time);
                    }

                }
            }
            match = ['', rod.getYear() + 1900, rod.getMonth() + 1, rod.getDate(), rod.getHours(), rod.getMinutes(), rod.getSeconds()];
        }
        var elapsedTime = rodate.elapsedTime(new Date(match[1], match[2] - 1, match[3], match[4], match[5], match[6]).getTime(), {
            minute: "min",
            second: "sec"
        }, true);
        plugin.html(elapsedTime);
    };
    rodate.elapsedTime = function (ctime, labels_config, last_only) {
        if (typeof ctime !== "number") {
            return "";
        }
        var labels_default = {
            year: "year",
            day: "day",
            hour: "hour",
            minute: "minute",
            second: "second",
            ago: "ago"
        };
        var labels = $.extend({}, labels_default, labels_config);
        var timeparts = [
            {
                name: labels.year,
                div: 31536000000,
                mod: 10000
            },
            {
                name: labels.day,
                div: 86400000,
                mod: 365
            },
            {
                name: labels.hour,
                div: 3600000,
                mod: 24
            },
            {
                name: labels.minute,
                div: 60000,
                mod: 60
            },
            {
                name: labels.second,
                div: 1000,
                mod: 60
            }
        ];
        var
            i = 0,
            l = timeparts.length,
            calc,
            values = [],
            interval = new Date().getTime() - ctime; //todo use server time
        while (i < l) {
            calc = Math.floor(interval / timeparts[i].div) % timeparts[i].mod;
            if (calc && calc >= 0) {
                values.push(calc + ' ' + timeparts[i].name + ( calc > 1 ? 's' : '' ));
            }
            i += 1;
        }
        if (values.length === 0) {
            values.push('1 ' + labels.second);
        }
        if (last_only === true) {
            return values[0] + ' ' + labels.ago;
        } else {
            return values.join(', ') + ' ' + labels.ago;
        }
    };
    return rodate;
});

/**
 * 'login' module.
 *
 * @module login
 * @requires util
 * @requires ajax
 * @requires cookie
 * @requires event
 * @requires jquery
 * @requires module
 * @requires user
 * @requires string
 * @requires logger
 */
define('tiljs/login',["tiljs/apps/times/usermanagement"],
    function (login) {
        mod_login = login;
        
        return mod_login;
    }
);

define('tiljs/plugin/lazy',["../event", "../ui", "module", "../util"], function (event, ui, module, util) {
    var mod_lazy = {};
    var default_config = {
        skew: 1000,
        error_image: null
    };
    var config = util.extend(true, {}, default_config, module.config());
    mod_lazy.init = function () {
        var tthis = this;
        tthis.load();
        event.subscribe("window.scroll", function () { //todo this wont work
            tthis.load();
        })
    };
    mod_lazy.load = function () {
        var tthis = this;
        var imgEle = $("[data-src]");
        imgEle.each(function (i, imgEleC) {
            if (ui.inView(imgEleC, true, config.skew)) {
                tthis.loadEach(imgEleC);
            }
        });
    };
    mod_lazy.loadEach = function (imgEleC) {
        var imgEle$ = $(imgEleC);
        var imgdomain=(typeof toiprops != "undefined" && toiprops.hasOwnProperty("imgdomain"))?toiprops.imgdomain:'';
        var imgsrc = (/^(f|ht)tps?:\/\//i.test(imgEle$.attr("data-src")))?imgEle$.attr("data-src"):imgdomain+imgEle$.attr("data-src");
        if (config.error_image) {
            imgEle$.error(function () {
                $(this).attr('src', imgdomain+config.error_image);
            });
        }
        imgEle$.attr("src", imgEle$.attr("data-src"));
        imgEle$.removeAttr("data-src");
        
        if(imgEle$.siblings(".loader").length>0)
		{
		    setTimeout(function() {
		        if(imgEle$.hasClass("blurimage"))
        		{
        		    imgEle$.removeClass("blurimage");
        		}
    		    imgEle$.siblings(".loader").hide();    
    		}, 500);
		}
    };
    mod_lazy.init();
    return mod_lazy;
});

define('jsrender',["jquery"],function($){
    if(!$.fn.render){
        throw new Error("Missing js: http://timesofindia.indiatimes.com/jsrender.cms" );
    }
    return $.fn.render;
});
define('localstoragec',["tiljs/cookie"], function (cookie) {
    var mod_local = {};
    mod_local.isEnabled = function () {
        return typeof ( Storage ) !== "undefined";
    };
    mod_local.checkExpiry = function (key) {
        var obj = localStorage.getItem(key);
        if (obj && !( obj.hasOwnProperty("timestamp") && obj.hasOwnProperty("expires") )) return key;
        if (obj && (Number(new Date().getTime()) - Number(obj.timestamp) > Number(obj.expires) * 60 * 60 * 24 * 1000 )) {
            localStorage.removeItem(key);
            return;
        }
        return key;
    };
    mod_local.set = function (name, value, expires, path, domain, secure) {
        try {
            var object = {
                "value": value,
                timestamp: new Date().getTime(),
                "expires": expires
            };
            localStorage.setItem(name, JSON.stringify(object));
        } catch (e) {
            cookie.set(name, value, expires, path, domain, secure);
        }
    };
    mod_local.get = function (key) {
        //return (mod_local.checkExpiry(key))?localStorage.getItem(key):'';
        var val = '';
        try {
            val = JSON.parse(localStorage.getItem(key));
        } catch (ex) {
            val = localStorage.getItem(key);
        }
        return ( val && val['value'] ) ? val['value'] : val;
    };
    mod_local.csget = function (key) {
        //return (mod_local.isEnabled() && mod_local.checkExpiry(key))?mod_local.get(key):mod_local.get(key);
        return ( mod_local.isEnabled() && mod_local.get(key) ) ? mod_local.get(key) : cookie.get(key);
    };
    mod_local.remove = function (data) {
        return localStorage.removeItem(data);
    };
    return mod_local;
});
define('tiljs/comments',["./util", "./ui", "jquery", "module", "./is", "./plugin/lazy", "./event"], function (util, ui, $, module, is, lazy, event) {
    var default_config = {
        gravatar: false,
        wrapper: "#comment-section", //data-plugin=['comments'] //All comments inside this
        main: "#comments", //data-plugin=['comments'] //Comment will be added here
        comment: "[data-plugin='comment']", //Each comment
        tmpl: "comment_tmpl",
        count: "[data-plugin='comments-count']",
        post_button: "[data-plugin='comment-post']",
        comment_input: "[data-plugin='comment-input']",
        form: "[data-plugin='comment-form']",
        loadonscroll: true
    };
    var mod_comments = function (config) {
        this.config = util.extend(true, {}, config);
    };
    mod_comments.config = util.extend(true, {}, default_config, module.config());
    var sample_comment = [
        {
            text: "Hello World",
            name: "Del Sanic",
            image: "http://www.gravatar.com/avatar/?d=identicon",
            uid: "123456",
            badge: [
                {
                    name: "Silver"
                },
                {
                    name: "Gold"
                }
            ]
        },
        {
            text: "Test Comment",
            name: "John",
            image: "http://www.gravatar.com/avatar/?d=identicon",
            uid: "123457",
            badge: [
                {
                    name: "Silver"
                },
                {
                    name: "Gold"
                }
            ]
        }
    ];
    mod_comments.getConfig = function () {
        return this.config;
    };
    mod_comments.prototype.getConfig = function () {
        return this.config;
    };
    mod_comments.prototype.initialize = function () {
        var tthis = this;
        //        tthis.isLoading=0;
        this.comments = [];
        this.getData(function (data, commentCount) {
            tthis.renderCommentCount(commentCount || ( data ? data.length : 0 ));
            if (data && data.length > 0) {
                tthis.renderNoComment(false);
                tthis.render(data);
            } else {
                tthis.renderNoComment(true);
            }
        });
        if (tthis.config.loadonscroll === true) {
            (function (tthis) {
                event.subscribe("window.scroll", function () { //todo this wont work
                    tthis.loadNextPg();
                })
            })(tthis);
        }
    };
    mod_comments.prototype.getData = function (callback) {
        //        var comment = util.extend(true,{},sample_comment);
        //        ui.getGravatar();
        callback(sample_comment);
    };
    mod_comments.prototype.renderCommentCount = function (count, type) {
        $("[data-plugin='comment-count']").text(count);
    };
    mod_comments.prototype.renderNoComment = function (showOrHide) {
        if (showOrHide) {
            $("[data-plugin='comment-none']").show();
        } else {
            $("[data-plugin='comment-none']").hide();
        }
    };
    mod_comments.prototype.render = function (data, callback, append) {
        var tthis = this;
        if (data) {
            var root = $(tthis.config.wrapper + " " + tthis.config.main);
            if (append == false || typeof append === "undefined") {
                root.empty();
                ////            root.append(this.renderInput()) ;
            }
            //            if (this.config.tmpl) {
            //                this.renderUsingTemplate(0,data,this.config.tmpl);
            //            } else {
            util.each(data, function (i, v) {
                root.append(tthis.renderEach(i, v));
                if (callback) {
                    callback(data);
                }
            });
            lazy.load();
            //            }
        }
    };
    mod_comments.prototype.renderEach = function (index, dataOne) {
        if (this.config.tmpl && $.fn.render) {
            return this.renderUsingTemplate(index, dataOne, this.config.tmpl);
        } else {
            event.publish("logger.error", "$.fn.render not defined. Render using jquery. this.config.tmpl=" + this.config.tmpl + "&$.fn.render=" + $.fn.render);
            return this.renderUsingjQuery(index, dataOne);
        }
    };
    mod_comments.prototype.renderUsingTemplate = function (index, dataOne, tmpl, prepend) {
        return $("#" + tmpl).render(dataOne);
    };
    mod_comments.prototype.renderUsingjQuery = function (index, dataOne) {
        var li = $("<li></li>");
        li.text(dataOne.name + ": " + dataOne.text);
        return li;
    };
    mod_comments.prototype.loadingDiv = function (text) {
        $("[data-plugin='comment-loading']").text(text);
    };
    //    mod_comments.prototype.isLoading = function () {
    //        return this.isLoading === 1;
    //    };
    mod_comments.prototype.loadNextPg = function () {
        var tthis = this;
        //        var config = tthis.getConfig();
        if (tthis.isLoading === 0 && ui.inView(tthis.config.main + " .comment:last-child", true, 1000)) { //todo remove li from here
            tthis.loadComment(tthis.config.commentType, function (data) {
            }, true, ++tthis.config.curpg);
        }
    };
    mod_comments.prototype.loading = function (progress) {
        this.loadingDiv("Loading..." /*+ " " + (progress?progress + "%":"")*/);
        this.isLoading = 1;
    };
    mod_comments.prototype.loaded = function () {
        this.loadingDiv("");
        this.isLoading = 0;
        event.publish("comments.loaded", this);
        if (this.config.loadonscroll == false) {
            $(this.config.wrapper).find('#' + this.config.commentType).append("<div class='loadmore'>View more comments</div>");
        }
    };
    mod_comments.prototype.loadedAll = function () {
        this.loadingDiv("Loaded all comments");
        $(this.config.wrapper + ' #' + this.config.commentType + ' .loadmore').hide(); //todo change to data plugin
        if (this.comments.length == 0) {
            event.publish("comments.loaded.none", this);
        } else if (this.comments.length >= 0) {
            event.publish("comments.loaded.all", this);
        }
        this.isLoading = 2;
    };
    return mod_comments;
});

define('tiljs/social/social',["../load", "../util", "jquery", "../event", "../ui", "../logger"], function (load, util, $, event, ui, logger) {
    //    function SocialPlugin(id, social_parent) {
    //        var tthis = this;
    //
    //        this.id = id;
    //        this.social_id = social_parent.id;
    //        this.social_parent = social_parent;
    //
    //        var registerEvents = function () {
    //            var pluginName = tthis.getName();
    //            util.each(this.events, function (eventName, eventCallback) {
    //                $("body").on(eventName, "[data-plugin='" + pluginName + "']", function(e){
    //                    if(eventCallback){
    //                        eventCallback.call(this,e);
    //                    }
    //                    e.stopImmediatePropagation();
    //                    return false;
    //                });
    //            });
    //        };
    //
    //        this.readOptions = function (ele) {
    //            return util.data(ele);
    //        };
    //
    //        this.getName = function () {
    //            return this.social_id + "-" + this.id;
    //        };
    //
    //        this.init=function(){
    //
    //        };
    //
    //        this.preAttachPluginEvent = function (plugins) {
    //            var pluginInstance = plugins.filter("[data-plugin='" + this.getName() + "']");
    //            pluginInstance.each(function (kk, vv) {
    //                if (tthis.init) {
    //                    tthis.init(vv, tthis);
    //                }
    //            });
    //        };
    //    }
    var mod_social = function (id) {
        this.id = id;
        this.config = {
            js: null,
            js_id: null
        };
        this.config = function (config) {
            this.config = util.extend(true, {}, this.config, config);
            this.js = config.js;
            this.js_id = config.js_id;
        };
        /**
         * @deprecated use this.config
         * @param js
         * @param js_id
         */
        this.setJS = function (js, js_id) {
            this.js = js;
            this.js_id = js_id;
        };
        this.plugins = {};
        this._getPluginID = function (id) {
            return opt.id + "-" + id;
        };
        this.addPlugin = function (plugin_options) {
            opt.plugins[this._getPluginID(plugin_options.id)] = plugin_options;
            if (plugin_options.alias) {
                for (var i = 0; i < plugin_options.alias.length; i++) {
                    var alias_options = util.extend(true, {}, plugin_options, {
                        id: plugin_options.alias[i]
                    });
                    delete alias_options.alias;
                    this.addPlugin(alias_options);
                }
            }
        };
        this.getPlugin = function (id) {
            return opt.plugins[this._getPluginID(id)];
        };
        var opt = this;
        this.beforeinit = function () {
        };
        this.afterloadjs = function () {
        };
        this.beforeloadjs = function () {
        };
        this.init = function (config) {
            if (opt.beforeinit) {
                opt.beforeinit();
            }
            var plugins = opt.find();
            logger.log(opt.id + " init:" + plugins.length);
            if (plugins && plugins.length > 0 || config.parse === true) {
                if (opt.beforeloadjs) {
                    opt.beforeloadjs();
                }
                //                console.log(opt.js_id + ":" + opt.js_id);
                if (opt.js && ( opt.isJSdependent(plugins) || config.parse === true )) {
                    load.js(opt.js, function (options, plugins) {
                        return function () {
                            if (opt.afterloadjs) {
                                opt.afterloadjs(config);
                            }
                            opt.initPlugins(plugins);
                        };
                    }(opt, plugins), opt.js_id);
                } else {
                    opt.initPlugins(plugins);
                }
            }
        };
        this.find = function () {
            return $("[data-plugin^='" + this.id + "-']");
        };
        this.isJSdependent = function (plugins) {
            for (var i = 0; i < plugins.length; i++) {
                var plugin = plugins[i];
                var pluginName = $(plugin).attr("data-plugin");
                if (opt.plugins[pluginName] && opt.plugins[pluginName].js) {
                    return true;
                }
            }
            return false;
        };
        this.initPlugins = function (plugins) {
            this.registerPluginEvents();
            //            this.initPlugins(plugins);
        };
        this.registerPluginEvents = function () {
            util.each(opt.plugins, function (pluginName, plugin) {
                util.each(plugin.events, function (eventName, eventCallback) {
                    //                        console.log("EVENT:"+pluginName+":"+eventName);
                    $("body").off(eventName, "[data-plugin='" + pluginName + "']");
                    $("body").on(eventName, "[data-plugin='" + pluginName + "']", function (e) {
                        if (eventCallback) {
                            try {
                                event.publish("social.action", {
                                    plugin: pluginName,
                                    event: eventName,
                                    element: this,
                                    dom_event: e
                                });
                                eventCallback.call(this, e);
                            } catch (e) {
                                event.publish("logger.error", e.stack);
                            }
                        }
                        e.stopImmediatePropagation();
                        return false;
                    });
                });
                var pluginInstance = $("body").find("[data-plugin='" + pluginName + "']");
                pluginInstance.each(function (kk, vv) {
                    if (plugin.init) {
                        try {
                            plugin.init(vv, plugin);
                        } catch (e) {
                            event.publish("logger.error", e.stack);
                        }
                    }
                });
            });
        };
        this.preAttachPluginEvent = function (plugins) {
            //            console.log(opt.plugins);
            var plugincount = 0;
            util.each(opt.plugins, function (k, v) {
                var pluginInstance = plugins.filter("[data-plugin='" + k + "']");
                pluginInstance.each(function (kk, vv) {
                    plugincount++;
                    if (v.init) {
                        v.init(vv, v);
                        $(vv).css("border", "2px solid red");
                    }
                });
            });
            logger.log(opt.id + " found plugin : " + plugincount + "/" + plugins.length);
        };
        this.renderPlugin = function (ele, default_params, main_ele) {
            var element = $(ele);
            var data = util.data(ele);
            var params = util.extend(true, {}, default_params, data);
            params.href = data.url || data.href || element.attr("href") || params.href;
            var pluginEle = $(main_ele);
            //Using this because data method in jquery does not append dom element
            $.each(params, function (name, value) {
                pluginEle.attr("data-" + name, value);
            });
            element.empty();
            element.append(pluginEle);
            this.parse(ele);
        };
        this.getAbsoluteUrl = function (url) {
            var resolvedUrl = ( url && url.length > 0 && url !== "#" ) ? url : document.location.href;
            resolvedUrl = resolvedUrl.split("#")[0]; // remove #
            if (resolvedUrl && resolvedUrl.length > 2 && resolvedUrl[0] === "/" && resolvedUrl[1] !== "/") { //URL is relative, make it absolute
                resolvedUrl = location.protocol + "//" + location.host + resolvedUrl;
            }
            return resolvedUrl;
        };
        ///Helper Methods
        this._share = function (url, params, options, element) {
            event.publish("social.onBeforeShare", {
                params: params,
                element: element,
                network: this.id
            });
            var _url = this.getAbsoluteUrl(url);
            var name = "social_share_" + new Date().getTime();
            var _options = $.extend({
                name: name,
                width: 700,
                height: 400
            }, options);
            var popup = ui.window(_url + "?" + $.param(params), _options);
            if (popup) {
                popup.moveTo(275, 275);
            }
        };
    };
    mod_social.onBeforeShare = function (callback) {
        event.subscribe("social.onBeforeShare", callback);
    };
    return mod_social;
});

define('tiljs/social/facebook',["../social/social", "../ui", "../logger", "../event", "../util", "module", "jquery", "../ajax", "../is"], function (social, ui, logger, event, util, module, $, ajax, is) {
    var default_config = {
        parse: false,
        init: true,
        appid: null,
        js: "//connect.facebook.net/en_US/sdk.js",
        //        js: "//connect.facebook.net/en_US/all.js",
        js_id: "facebook-jssdk",
        load_js: false,
        share_url: "http://www.facebook.com/sharer.php",
        oauth: false,
        status: false,
        xfbml: false,
        fb_version: 'v2.12',
        share_params: function (url, title, summary, image) {
            return {
                "url[url]": url
            }
        }
    };
    var config = util.extend(true, {}, default_config, module.config());
    var mod_facebook = new social("facebook", "facebook");
    mod_facebook.setJS(config.js, config.js_id);
    var __isJSdependent = mod_facebook.isJSdependent;
    mod_facebook.isJSdependent = function (plugins) {
        return config.load_js || __isJSdependent(plugins);
    };
    mod_facebook.addPlugin({
        id: "like",
        js: true,
        init: function (ele, plugin) {
            var element = $(ele);
            var data = util.data(ele);
            var default_params = {
                href: location.href,
                layout: "button_count", //standard , box_count, button_count, button
                "show-faces": "false", //false, true
                width: "100", //integer
                action: "like", //like , recommend,
                share: false //true, false
            };
            var params = util.extend(true, {}, default_params, data);
            params.href = data.url || data.href || element.attr("href") || params.href;
            var fb_like = $("<div class='fb-like'></div>");
            $.each(params, function (name, value) {
                //Using this because data method in jquery does not append dom element
                fb_like.attr("data-" + name, value);
            });
            element.empty();
            element.append(fb_like);
            mod_facebook.parse(ele);
        }
    });
    mod_facebook.addPlugin({
        id: "comments",
        js: true,
        init: function (ele, plugin) {
            var element = $(ele);
            var data = util.data(ele);
            var default_params = {
                href: location.href,
                colorscheme: "light", //light, dark
                numposts: 10 //10
                //                width : "100"            //integer
            };
            var params = util.extend(true, {}, default_params, data);
            params.href = data.url || data.href || element.attr("href") || params.href;
            var fb_comments = $("<div class='fb-comments'></div>");
            //Using this because data method in jquery does not append dom element
            $.each(params, function (name, value) {
                fb_comments.attr("data-" + name, value);
            });
            element.empty();
            element.append(fb_comments);
            mod_facebook.parse(element[0]);
            //            FB.xfbml.parse(ele);
        }
    });
    mod_facebook.addPlugin({
        id: "share",
        alias: ["button"],
        events: {
            click: function (e) {
                var data = util.data(this);
                var title = data.title || document.title;
                mod_facebook.share(data.url || $(this).attr("href"), title, data.summary, data.image, this);
            }
        }
    });
    mod_facebook.share = function (url, title, summary, image, element) {
        if (false && window.FB && config.appid) { //disabling this because proper app id not available
            FB.ui({
                method: 'feed',
                display: 'popup',
                link: mod_facebook.getAbsoluteUrl(url),
                picture: mod_facebook.getAbsoluteUrl(image), //check picture should be correct
                name: title,
                //             caption: summary,
                description: summary
            }, function (response) {
                //                alert(response);
            });
        } else {
            var shareParam = {
                u: mod_facebook.getAbsoluteUrl(url),
                display: 'popup',
                sdk: 'joey'
                //                    'p[url]': mod_facebook.getAbsoluteUrl(url),
                //                    'p[title]': title,
                //                    'p[summary]': summary,
                //                    'p[image][0]': mod_facebook.getAbsoluteUrl(image)
            };
            if (!is.empty(config.appid)) {
                shareParam.app_id = config.appid;
            }
            mod_facebook._share(
                config.share_url, shareParam, {
                    name: 'facebook_share_dialog',
                    width: 626,
                    height: (is.IE()) ? 600 : 436
                }, element
            );
        }
    };
    mod_facebook.addPlugin({
        id: "follow",
        events: {
            click: function (e) {
                mod_facebook.follow($(this).attr("data-url") || $(this).attr("href"));
            }
        },
        init: function (ele, plugin) {
        }
    });
    mod_facebook.follow = function (url, options) {
        var _url = mod_facebook.getAbsoluteUrl(url);
        var win = window.open(url, "_blank");
        win.focus();
    };
    mod_facebook.addPlugin({
        id: "login",
        js: true,
        events: {
            click: function (e) {
                mod_facebook.login();
            }
        }
    });
    var __perms = null;
    mod_facebook.parse = function (ele) {
        if (typeof FB !== "undefined") {
            FB.XFBML.parse(ele);
        } else {
            logger.warn("'FB' is required in parse");
        }
    };
    mod_facebook.getGrantedPermissions = function (callback) {
        if (typeof FB !== "undefined") {
            mod_facebook.api("/me/permissions", function (resp) {
                if (callback) {
                    callback(resp);
                }
            });
        } else {
            logger.warn("'FB' is required in login");
        }
    };
    mod_facebook.hasPermissions = function (permissions, callback) {
        if (typeof FB !== "undefined") {
            mod_facebook.api("/me/permissions", function (resp) {
                if (resp && resp.data && resp.data.length > 0) {
                    var perms = typeof permissions === "string" ? permissions.split(",") : ( permissions instanceof Array ? permissions : [] );
                    for (var i = 0; i < perms.length; i++) {
                        var perm = resp.data[0];
                        if (perm.hasOwnProperty(perms[i])) {
                            if (callback) {
                                callback(true);
                                return null;
                            }
                        }
                    }
                }
                if (callback) {
                    callback(false, resp && resp.error ? resp.error : null);
                }
            });
        } else {
            logger.warn("'FB' is required in login");
        }
    };
    mod_facebook.getPermissions = function (permissions, callback) {
        if (typeof FB !== "undefined") {
            var perms = typeof permissions === "string" ? permissions : ( permissions instanceof Array ? permissions.join(",") : "" );
            mod_facebook.hasPermissions(permissions, function (resp) {
                if (resp === true) {
                    if (callback) {
                        callback(resp);
                    }
                } else {
                    mod_facebook.login(permissions, function () {
                        mod_facebook.hasPermissions(permissions, function (resp) {
                            if (callback) {
                                callback(resp);
                            }
                        });
                    });
                }
            });
        } else {
            logger.warn("'FB' is required in login");
        }
    };
    mod_facebook.login = function (permissions, callback) {
        if (typeof FB !== "undefined") {
            logger.log(permissions);
            var perms = typeof permissions === "string" ? permissions : ( permissions instanceof Array ? permissions.join(",") : "" );
            if (__perms != perms) {
                //                __perms = perms;
                FB.login(callback, {
                    scope: perms
                }); //'email,user_likes'
            }
        } else {
            logger.warn("'FB' is required in login");
        }
    };
    mod_facebook.addPlugin({
        id: "logout",
        events: {
            click: function (e) {
                mod_facebook.logout();
            }
        }
    });
    mod_facebook.logout = function (callback) {
        if (typeof FB !== "undefined") {
            FB.logout(callback)
        } else {
            logger.warn("'FB' is required in logout");
        }
    };
    mod_facebook._createFBroot = function () {
        //Create fb-root div.
        var id = "fb-root";
        var div = $("#" + id);
        if (!div || div.length == 0) {
            div = $("<div></div>");
            div.attr("id", id);
            div.css("display", "none");
            $("body").append(div);
        }
        return div;
    };
    mod_facebook.beforeinit = function () {
        if (!config.appid) {
            logger.warn("'config.appid' is Required");
        }
    };
    mod_facebook.beforeloadjs = function () {
        mod_facebook._createFBroot();
    };
    mod_facebook.afterloadjs = function (k) {
        if (typeof FB !== "undefined") {
            logger.log("FB js loaded");
            if (config.init === true || k.init === true) {
                FB.init({
                    appId: config.appid,
                    oauth: config.oauth,
                    status: config.status,
                    cookie: true,
                    xfbml: config.xfbml,
                    version: config.fb_version
                });
            }
            event.publish("FB.onload", FB);

            FB.Event.subscribe('edge.remove', function (response) {
                event.publish("FB.edge.remove", response);
            });

            FB.Event.subscribe('edge.create', function (response) {
                event.publish("FB.edge.create", response);
            });

            FB.Event.subscribe('comment.create', function (response) {
                event.publish("FB.comment.create", response);
            });
            FB.Event.subscribe('auth.authResponseChange', function (response) {
                event.publish("FB.auth.authResponseChange", response);
                if (response.status === 'connected') {
                    // the user is logged in and has authenticated your
                    // app, and response.authResponse supplies
                    // the user's ID, a valid access token, a signed
                    // request, and the time the access token
                    // and signed request each expire
                    var uid = response.authResponse.userID;
                    var accessToken = response.authResponse.accessToken;
                    event.publish("FB.auth.authResponseChange.connected", response);
                    event.publish("FB.connected", FB);
                } else if (response.status === 'not_authorized') {
                    // the user is logged in to Facebook,
                    // but has not authenticated your app
                    event.publish("FB.auth.authResponseChange.not_authorised", response);
                } else {
                    // the user isn't logged in to Facebook.
                    event.publish("FB.auth.authResponseChange.failed", response);
                }
            });
        } else {
            logger.warn("'FB' is required in afterloadjs");
        }
    };
    var currentUser = null;
    mod_facebook.getUser = function (callback) {
        if (typeof FB !== "undefined" && !currentUser) {
            FB.api('/me', function (response) { //todo use mod_facebook.api
                if (callback) {
                    currentUser = response;
                    callback(response); //response is the basic user object
                }
            });
        } else {
            if (callback) {
                callback(currentUser);
            }
        }
    };
    mod_facebook.onlogin = function (callback) {
        event.subscribe("FB.auth.authResponseChange.connected", function (resp) {
            mod_facebook.getUser(function (user) {
                if (callback) {
                    callback(user, resp);
                }
            })
        });
    };
    mod_facebook.oncomment = function (callback) {
        event.subscribe("FB.comment.create", function (resp) {
            mod_facebook.getUser(function (user) {
                if (callback) {
                    callback(user, resp);
                }
            })
        });
    };
    mod_facebook.onlogout = function (callback) {
        event.subscribe(["FB.auth.authResponseChange.not_authorised", "FB.auth.authResponseChange.failed"], function (resp) {
            if (callback) {
                currentUser = null;
                callback(resp);
            }
        });
    };
    mod_facebook.fql = function (query, callback) {
        if (typeof FB !== "undefined") {
            logger.log(query);
            FB.api({ //todo use mod_facebook.api
                method: 'fql.query',
                query: query
            }, callback);
        } else {
            logger.warn("'FB' is required in fql");
        }
    };
    //select name from page where page_id in (SELECT page_id FROM page WHERE page_id IN (SELECT uid, page_id, type FROM page_fan WHERE uid=me()) AND type='city')
    mod_facebook.getLikes = function (callback) {
        if (typeof FB !== "undefined") {
            FB.api('/me/likes', /*{'limit': '5'},*/ function (response) { //todo use mod_facebook.api
                if (callback) {
                    currentUser = response;
                    callback(response); //response is the basic user object
                }
            });
        }
    };
    mod_facebook.getLikesByCategory = function (categories, callback) {
        if (categories.length > 0 && is.string(categories)) {
            categories = categories.split(",");
        }
        var catArr = [];
        if (categories.length > 0) {
            util.each(categories, function (i, v) {
                catArr.push("type='" + v + "'");
            });
        }
        var catStr = catArr.length > 0 ? "AND (" + catArr.join(" OR ") + ")" : "";
        mod_facebook.getUser(function (user) {
            if (user) {
                mod_facebook.fql("select name from page where page_id in (SELECT page_id FROM page WHERE page_id IN (SELECT uid, page_id, type FROM page_fan WHERE uid=me()) " + catStr + ")", function (data) {
                    if (callback) {
                        callback(data);
                    }
                });
            } else {
                logger.warn("User is not logged in. Cannot run facebook.getLikesByCategory.");
            }
        });
    };
    /**
     * Paging Example :

     var facebook = require("social/facebook")
     facebook.getLikes(function(data){
            if(data){
                console.log(data);
                facebook.paging(data,arguments.callee); //This will recursively call facebook
            }
        });

     * @param data
     * @param callback
     * @returns {*}
     */
    mod_facebook.paging = mod_facebook.pageNext = function (data, callback) {
        //{"cursors":{"after":"MjY4ODE3MDI2NDgyMDU5","before":"MTYwMjI3OTE0MDM3NzU0"},
        // "next":"https://graph.facebook.com/100006612690893/likes?access_token=CAABrIHwVZA2GUh3ryguvIzDAHbM1SR0iHYnLHo2hZAp1gZDZD&limit=25&after=MjY4ODE3MDI2NDgyMDU5"}
        if (data && data.paging && data.paging.next) {
            return ajax.getJSONP(data.paging.next, callback);
        } else {
            callback(null);
        }
    };
    mod_facebook.pagePrev = function (data, callback) {
        //{"cursors":{"after":"MjY4ODE3MDI2NDgyMDU5","before":"MTYwMjI3OTE0MDM3NzU0"},
        // "next":"https://graph.facebook.com/100006612690893/likes?access_token=CAABrIHwVZA2GUh3ryguvIzDAHbM1SR0iHYnLHo2hZAp1gZDZD&limit=25&after=MjY4ODE3MDI2NDgyMDU5"}
        if (data && data.paging && data.paging.previous) {
            return ajax.getJSONP(data.paging.previous, callback);
        } else {
            callback(null);
        }
    };
    mod_facebook.getRegisteredUsersTxt = function (callback) {
        mod_facebook.getRegisteredUsers(function (users) {
            if (callback) {
                if (users.length == 1) {
                    callback(users[0].name + " has planned his/her trips on HappyTrips.com.", users);
                } else if (users.length > 1) {
                    callback(users.length + " of your friends have planned their trips on HappyTrips.com", users);
                } else {
                    callback("None of your friends have planned their trips on HappyTrips.com.", users);
                }
            }
        })
    };
    //    mod_facebook.getRegisteredUsersTxtImg = function (callback, count, link) {
    //        mod_facebook.getRegisteredUsersTxt(function (text, users) {
    //            if (users && users.length > 0) {
    //                var imgEle = $("<span></span>");
    //
    //                var maxImg = 10;
    //
    //                imgEle.empty();
    //                for (var i = 0; i < users.length && i < maxImg; i++) {
    //                    var user = users[i];
    //                    if (link === true) {
    //                        imgEle.append($('<a href="https://www.facebook.com/' + user.uid + '" target="_blank"><img src="' + user.pic_square_with_logo + '" alt="' + user.name + '" title="' + user.name + '" /></a>'));
    //                    } else {
    //                        imgEle.append($('<img src="' + user.pic_square_with_logo + '" alt="' + user.name + '" title="' + user.name + '" />'));
    //                    }
    //
    //                }
    //                if(!count || count === true){
    //                    imgEle.append($('<a href="https://www.facebook.com/' + user.uid + '" class="fbImgCount" target="_blank">+' + ((users.length) - i) + '</a>'));
    //                }
    //            }
    //        });
    //    };
    /**
     * N users have registered on APP_ID
     * @param callback
     */
    mod_facebook.getRegisteredUsers = function (callback) {
        mod_facebook.fql('SELECT uid,name,pic_square_with_logo FROM user WHERE is_app_user AND uid IN (SELECT uid2 FROM friend WHERE uid1 = me())', callback);
    };
    mod_facebook.api = function () {
        var query = arguments[0];
        var method = typeof arguments[1] === "string" ? arguments[1] : "GET";
        var params = typeof arguments[2] === "object" ? arguments[2] : {};
        var callback = typeof arguments[1] === "function" ? arguments[1] : ( typeof arguments[2] === "function" ? arguments[2] : ( typeof arguments[3] === "function" ? arguments[3] : null ) );
        /*
         'since':'last week',
         'limit': '10',
         'offset': '20',
         'until': 'yesterday'
         */
        if (typeof FB !== "undefined") {
            logger.debug(query);
            FB.api(query, method, params, function (response) {
                if (callback) {
                    callback(response);
                }
            });
        } else {
            logger.warn("'FB' is required in api");
        }
    };
    mod_facebook.getCheckins = function (callback) {
        mod_facebook.api("/me/locations", callback);
    };
    mod_facebook.post = function (message, callback) {
        var _message = typeof arguments[0] === "string" ? {
            message: arguments[0]
        } : arguments[0];
        mod_facebook.login("publish_stream", function () {
            mod_facebook.api("/me/feed", "POST", _message, callback);
        });
    };
    /**SELECT page_id,name
     FROM place
     WHERE distance(latitude, longitude, "28.6410126613", "77.2408139523") < 50000
     ORDER BY distance(latitude, longitude, "28.6410126613", "77.2408139523")
     LIMIT 10*/
    mod_facebook.checkin = function (message, placeid, callback) {
        mod_facebook.post({
            message: message,
            place: placeid
        }, callback);
    };
    $(document).ready(function(){
      mod_facebook.init(config);  
    });
    return mod_facebook;
});

define('tiljs/ext/date',[], function () {
    /*
     * @version  0.5.0
     * @author   Lauri Rooden - https://github.com/litejs/date-format-lite
     * @license  MIT License  - http://lauri.rooden.ee/mit-license.txt
     */
    !function (Date, proto) {
        var maskRe = /(["'])((?:[^\\]|\\.)*?)\1|YYYY|([MD])\3\3(\3?)|SS|([YMDHhmsW])(\5?)|[uUAZSwo]/g,
            yearFirstRe = /(\d{4})[-.\/](\d\d?)[-.\/](\d\d?)/,
            dateFirstRe = /(\d\d?)[-.\/](\d\d?)[-.\/](\d{4})/,
            timeRe = /(\d\d?):(\d\d):?(\d\d)?\.?(\d{3})?(?:\s*(?:(a)|(p))\.?m\.?)?(\s*(?:Z|GMT|UTC)?(?:([-+]\d\d):?(\d\d)?)?)?/i,
            wordRe = /.[a-z]+/g,
            unescapeRe = /\\(.)/g
        //, isoDateRe = /(\d{4})[-.\/]W(\d\d?)[-.\/](\d)/
        // ISO 8601 specifies numeric representations of date and time.
        //
        // The international standard date notation is
        // YYYY-MM-DD
        //
        // The international standard notation for the time of day is
        // hh:mm:ss
        //
        // Time zone
        //
        // The strings +hh:mm, +hhmm, or +hh (ahead of UTC)
        // -hh:mm, -hhmm, or -hh (time zones west of the zero meridian, which are behind UTC)
        //
        // 12:00Z = 13:00+01:00 = 0700-0500
        Date[proto].format = function (mask) {
            mask = Date.masks[mask] || mask || Date.masks["default"]
            var self = this,
                get = "get" + ( mask.slice(0, 4) == "UTC:" ? ( mask = mask.slice(4), "UTC" ) : "" )
            return mask.replace(maskRe, function (match, quote, text, MD, MD4, single, pad) {
                text = single == "Y" ? self[get + "FullYear"]() % 100 : match == "YYYY" ? self[get + "FullYear"]() : single == "M" ? self[get + "Month"]() + 1 : MD == "M" ? Date.monthNames[self[get + "Month"]() + ( MD4 ? 12 : 0 )] : single == "D" ? self[get + "Date"]() : MD == "D" ? Date.dayNames[self[get + "Day"]() + ( MD4 ? 7 : 0 )] : single == "H" ? self[get + "Hours"]() % 12 || 12 : single == "h" ? self[get + "Hours"]() : single == "m" ? self[get + "Minutes"]() : single == "s" ? self[get + "Seconds"]() : match == "S" ? self[get + "Milliseconds"]() : match == "SS" ? ( quote = self[get + "Milliseconds"](), quote > 99 ? quote : ( quote > 9 ? "0" : "00" ) + quote ) : match == "u" ? ( self / 1000 ) >>> 0 : match == "U" ? +self : match == "A" ? Date[self[get + "Hours"]() > 11 ? "pm" : "am"] : match == "Z" ? "GMT " + ( -self.getTimezoneOffset() / 60 ) : match == "w" ? self[get + "Day"]() || 7 : single == "W" ? ( quote = new Date(+self + ( ( 4 - ( self[get + "Day"]() || 7 ) ) * 86400000 )), Math.ceil(( ( quote.getTime() - quote["s" + get.slice(1) + "Month"](0, 1) ) / 86400000 + 1 ) / 7) ) : match == "o" ? new Date(+self + ( ( 4 - ( self[get + "Day"]() || 7 ) ) * 86400000 ))[get + "FullYear"]() : quote ? text.replace(unescapeRe, "$1") : match
                return pad && text < 10 ? "0" + text : text
            })
        }
        Date.am = "AM"
        Date.pm = "PM"
        Date.masks = {
            "default": "DDD MMM DD YYYY hh:mm:ss",
            "isoUtcDateTime": 'UTC:YYYY-MM-DD"T"hh:mm:ss"Z"'
        }
        Date.monthNames = "JanFebMarAprMayJunJulAugSepOctNovDecJanuaryFebruaryMarchAprilMayJuneJulyAugustSeptemberOctoberNovemberDecember".match(wordRe)
        Date.dayNames = "SunMonTueWedThuFriSatSundayMondayTuesdayWednesdayThursdayFridaySaturday".match(wordRe)
        //*/
        /*
         * // In Chrome Date.parse("01.02.2001") is Jan
         * n = +self || Date.parse(self) || ""+self;
         */
        String[proto].date = Number[proto].date = function (format) {
            var m, temp, d = new Date,
                n = +this || "" + this
            if (isNaN(n)) {
                // Big endian date, starting with the year, eg. 2011-01-31
                if (m = n.match(yearFirstRe)) d.setFullYear(m[1], m[2] - 1, m[3])
                else if (m = n.match(dateFirstRe)) {
                    // Middle endian date, starting with the month, eg. 01/31/2011
                    // Little endian date, starting with the day, eg. 31.01.2011
                    temp = Date.middle_endian ? 1 : 2
                    d.setFullYear(m[3], m[temp] - 1, m[3 - temp])
                }
                // Time
                m = n.match(timeRe) || [0, 0, 0]
                d.setHours(m[6] && m[1] < 12 ? +m[1] + 12 : m[5] && m[1] == 12 ? 0 : m[1], m[2], m[3] | 0, m[4] | 0)
                // Timezone
                if (m[7]) {
                    d.setTime(d - ( ( d.getTimezoneOffset() + ( m[8] | 0 ) * 60 + ( ( m[8] < 0 ? -1 : 1 ) * ( m[9] | 0 ) ) ) * 60000 ))
                }
            } else d.setTime(n < 4294967296 ? n * 1000 : n)
            return format ? d.format(format) : d
        }
    }(Date, "prototype");
    return {};
});

define('tiljs/apps/times/comments',[
        "module",
        "jquery",
        "json",
        "jsrender",
        "localstoragec",
        "../times/api",
        "../../comments",
        "../../ajax",
        "../../plugin/dynamic",
        "../../util",
        "../times/usermanagement",
        "../../event",
        "../../ui",
        "../../is",
        "../../string",
        "../../plugin/lazy",
        "../../cookie",
        "../../logger",
        "../../timer",
        "../../analytics/mytimes",
        "../../compatibility",
        "../../social/facebook",
        "../../page",
        "../../user",
        "../../ext/date"
    ],
    function (module, $, json, jsrender, localstoragec, api, comments, ajax, dynamic, util, login, event, ui, is, string, lazy,
              cookie, logger, timer, mytimes, compatibility, facebook, page, userClass, ext_date) {
        //todo use plugin module for data-plugin

        var CONSTANT = {
            RATE_TYPE: {
                AGREE: 100,
                DISAGREE: 101,
                RECOMMEND: 102,
                OFFENSIVE: 103
            }
        };
        var commentReplyFormHTML = '';

        var default_config = {
//            post_url :function(){
//                var url = "/postro.cms";
//
//                if (document.location.host == "test.indiatimes.com") {
//                    url = "lib/postComment.php";
//                }
//                return url;
//            }(),
//            validate_url : function(){
//                var validate_url = "/validatecomment.cms";
//                if (document.location.host == "test.indiatimes.com" || document.location.host == "test.happytrips.com") {
//                    validate_url = "lib/validateComment.php";
//                }
//                return validate_url;
//            }(),
//            rate_url : "/ratecomment_new.cms",
            validation: {
                minlength: 1
            },
            loadCommentFromMytimes: false,
            commentType: "comments",
            loadonscroll: true,
            sendCommentLiveEmail: true,
            loginRequiredForRating: true,
            nonloggedinComment: true,
            disabledirectcomment: false,
            maxchar: 3000,
            maxCommentWrapLength: 500,
            maxResponseCount: 3,
            hideResponses: false,
            share_url: "/share.cms",
            verify_comment_url: "/json/cmtverified.cms",
            messages: {
                "name_required": "Please enter your name.",
                "location_required": "Please enter your location.",
                "captcha_required": "Please enter captcha value.",
                "name_toolong": "Name cannot be longer than 30 chars.",
                "name_not_string": "Name can only contain alphabets.",
                "location_toolong": "Location cannot be longer than 30 chars.",
                "location_not_string": "Location can only contain alphabets.",
                "captcha_toolong": "Captcha cannot be longer than 4 chars.",
                "captcha_number_only": "Captcha value can only be a number.",
                "email_required": "Please enter your email address.",
                "email_invalid": "Please enter a valid email address.",
                "captcha_invalid": "Please enter a valid captcha value.",
                "minlength": "You can't post this comment as the length it is too short. ",
                "blank": "You can't post this comment as it is blank.",
                "maxlength": "You have entered more than 3000 characters.",
                "popup_blocked": "Popup is blocked.",
                "has_url": "You can't post this comment as it contains URL.",
                "duplicate": "You can't post this comment as it is identical to the previous one.",
                "abusive": "You can't post this comment as it contains inappropriate content.",
                "self_agree": "You can't Agree with your own comment",
                "self_disagree": "You can't Disagree with your own comment",
                "self_recommend": "You can't Recommend your own comment",
                "self_offensive": "You can't mark your own comment as Offensive",
                "already_agree": "You have already Agreed with this comment",
                "already_disagree": "You have already Disagreed with this comment",
                "already_recommended": "You have already Recommended this comment",
                "already_offensive": "You have already marked this comment Offensive",
                "cant_agree_disagree": "You can't Agree and Disagree with the same comment",
                "cant_agree_offensive": "You can't Agree and mark the same comment Offensive",
                "cant_disagree_recommend": "You can't Disagree and Recommend the same comment",
                "cant_recommend_offensive": "You can't Recommend and mark the same comment Offensive",
                "permission_facebook": "You can't post to facebook. Post permission is required.",
                "offensive_reason": "Please select a reason.",
                "offensive_reason_text": "Please enter a reason.",
                "offensive_reason_text_limit": "Please enter less than 200 chars."
            }
        };


        var times_comments = comments;


        comments.config = util.extend(true, {}, comments.config, default_config, module.config());

        times_comments.prototype.updateConfig = function (update_config) {
            //config = util.extend(true, config, update_config);
        };

//    login.init();

//        var isLoading = 0;
        var commentType = comments.config.commentType;
//        var curpg = 1;

        times_comments.prototype.initialize = function () {
            var tthis = this;
            this.config = util.extend(true, {}, comments.config, module.config(), this.config);
//            if (update_config) {
//                tthis.updateConfig(update_config);
//            }

            tthis.comments = [];
            tthis.commentCount = 0;
            tthis.countPresent = false;

            tthis.pageCount = 1;
//            tthis.commentsCacheDate = null;
//            tthis.commentsCacheUpdated = [];

            //todo move all this to config object
            tthis.config.comment_block_count = tthis.config.comment_block_count || 25;
            tthis.config.commentType = tthis.config.commentType || commentType;
//            tthis.config.attachInput = tthis.config.attachInput || true;
            tthis.config.attachReplyAction = tthis.config.attachReplyAction !== false;
            tthis.config.attachOpinionAction = tthis.config.attachOpinionAction !== false;

            if (tthis.config.disabledirectcomment) {
                $(tthis.config.wrapper + ' [data-plugin="comment-form"]').hide();
            }
            if (login.getUser()) {
                $(tthis.config.wrapper + " [data-plugin='user-isloggedin']").show();
                $(tthis.config.wrapper + " [data-plugin='user-notloggedin']").hide();
            }
            else {
                $(tthis.config.wrapper + " [data-plugin='user-isloggedin']").hide();
                $(tthis.config.wrapper + " [data-plugin='user-notloggedin']").show();
            }
            if (!tthis.config.nonloggedinComment && tthis.pageCount === 1) {
                $(tthis.config.wrapper + ' [data-plugin="user-notloggedin"]').remove();
            }

            tthis.config.opinions = [
                {name: "Agree", id: "agree"},
                {name: "Disagree", id: "disagree"},
                {name: "Recommend", id: "recommended"},
                {name: "Offensive", id: "offensive"}
            ];

//            if (tthis.config.attachInput === true) {
//                tthis.attachInput();
//            }

            if (tthis.config.attachOpinionAction === true) {
                tthis.attachOpinionAction();
            }

            if (tthis.config.attachReplyAction === true) {
                tthis.attachReplyAction();
            }

            this.getData(function (data, commentCount) {


                tthis.renderCommentCount(commentCount || (data ? data.length : 0));
                if (data && data.length > 0) {
                    tthis.renderNoComment(false);
                    tthis.render(data);
                } else {
                    tthis.renderNoComment(true);
                }

                tthis.verifyEmailComment();

            });

            (function (tthis) {
                event.subscribe("window.scroll", function () {//todo this wont work
                    var config = tthis.getConfig();
                    if (tthis.config.loadonscroll === true && tthis.isLoading === 0 && ui.inView(tthis.config.wrapper + " #" + tthis.config.commentType + " [data-plugin='comment']:visible:last", true, 200)) {  //todo remove li from here
                        tthis.loadComment(tthis.config.commentType, function (data) {

                        }, true, ++tthis.pageCount);
                    }
                });
            })(tthis);


//            tthis.onLoaded(function (comment_ref) {
//                tthis.loadSavedComment();
//            });


//            login.onStatusChange(function(){
//                tthis.loadSavedComment();
//            });
        };

        times_comments.run = function (config) {
            //Comments
            this.config = util.extend(true, {}, comments.config, module.config());
            var msid = config && config.msid ? config.msid : window.msid;//22655412;//27810093/*,channel = "toi"*/;
            var main = config && config.main ? config.main : "#" + this.config.commentType;//22655412;//27810093/*,channel = "toi"*/;
            var wrapper = config && config.wrapper ? config.wrapper : this.config.wrapper;//22655412;//27810093/*,channel = "toi"*/;
            var toi_comment = {};
            toi_comment[this.config.commentType] = new comments({main: main, msid: msid, wrapper: wrapper});
            toi_comment[this.config.commentType].initialize();

            $(wrapper + ' #comment_sort').change(function () {
                var rootid = $(this).val(),
                    id = "comments" + (rootid && rootid.length > 0 ? '_' + rootid : ""),
                    sortLbl = rootid.length ? rootid : "newest";

                $(wrapper + ' .comments-list').hide();
                $(wrapper + " #" + id).show();

                $(wrapper + ".comment-section .noComment").hide();

                if (toi_comment[id]) {
                    toi_comment[id].updateAfterCommentsLoaded();
                    toi_comment[id].attachReplyAction();
                    toi_comment[id].attachOpinionAction();
                    toi_comment[id].updateCachedRating();
                } else {
                    toi_comment[id] = new comments({
                        main: "#" + id,
                        msid: msid,
                        commentType: id,
                        wrapper: wrapper/*,attachReplyAction:false, attachOpinionAction:false*/
                    });
                    toi_comment[id].initialize();
                }
                event.publish("comment.sort", sortLbl);
                $(wrapper + " [data-plugin='comment-error-outer']").text("");
                $(wrapper + " [data-plugin='comment-error']").text("");
                event.publish("comment_error");
            });


            toi_comment[this.config.commentType].processRating();

            (function (that) {
                event.subscribe("user.status", function () {
                    toi_comment[that.config.commentType].processRating(that);
                });
            }(this))


            event.subscribe("comments.loaded", function (commentObj) {
                logger.log("comments.loaded:" + commentObj.config.commentType);
                commentObj.updateAfterCommentsLoaded();
                if (commentObj.config.hideResponses === true) {
                    commentObj.hideExtraResponses();
                }
            });

            event.subscribe("comments.loaded.none", function (commentObj) {
                logger.log("comments.loaded.none:" + commentObj.config.commentType);
                commentObj.updateAfterCommentsLoaded();
            });

            event.subscribe("login.error", function (err) {
                if (err && err.error) {
                    logger.error(err.error);
                } else {
                    logger.error(err);
                }
            });

            return toi_comment;
        };

        times_comments.prototype.processRating = function () {
            var user = login.getUser();
            if (!user) {
                $(this.config.wrapper + " [data-plugin='comment-error-outer']").text("");
                $(this.config.wrapper + " [data-plugin='comment-error']").text("");
                event.publish("comment_error");
            }
            var commenttype = "comments" + ($(this.config.wrapper + ' #comment_sort').length && $(this.config.wrapper + ' #comment_sort').val() !== '' ? '_' + $(this.config.wrapper + ' #comment_sort').val() : "");
            // callback to get user rating from mytimes
            if (user && this && this.config && this.config.withrating) {
                //var tthis = toicomment[commenttype];
                var savedComment = this.getSavedComment();
                var usr_data = {};
                usr_data.userId = user.getId();
                usr_data.baseEntityId = 0;
                usr_data.uniqueAppKey = this.config.withrating;
                usr_data.appKey = page.getChannel();
                $.getJSON(tLoginObj.myTimesUrl+"/mytimes/alreadyRated?callback=?", usr_data, function (data) {
                    event.publish("comments.user_has_rated", data);
                    if (window.msid == this.config.msid && this.getSavedComment()) {
                        data = (data && data != '') ? data : ' ';
                        savedComment.urs = data;
                        this.saveComment(savedComment);
                        this.loadSavedComment();
                    }
                });
            }

            this.markFollowingAll(); //Added by Amit
        };

        //Todo remove from prototype
        times_comments.prototype.attachUserToComment = function (commentObj, user) {
            logger.log("Attaching user to comment");
            commentObj.user = {
                "id": user.getId(),
                "uid": user.getId() ? user.getUid() : '',
                "name": user.getFullName(),
                "username": user.getUsername(),
                "location": user.getCITY(),
                "image": user.getThumb(),
                "email": user.getEmail()
//                    "followers": user.getFollowersCount(),
//                    ,"badge": [],  //todo
//                    "opinion": [],//todo
//                    "points": 1205,     //todo
//                    "pointslevel": "Silver"   //todo
            };
        };

        times_comments.prototype.hideExtraResponses = function () {
            var ttthis = this;
            $($(this.config.wrapper + " #" + this.config.commentType + " .comment-box.level1").get().reverse()).each(function (i, v) {
                // $(".comment-box.level1").get().reverse().each(function(i,v){
                var tc = $(this);
                var tc_level = tc.data("level");
                var tc_next = tc.next();
                if (is.numberOnly(tc_next.data("level"))) {
                    var response_cnt = 0;
                    while ((tc_next.data("level") != tc_level) && (tc_next.is(":hidden")) && (i < ttthis.config.comment_block_count)) {
                        if (response_cnt == ttthis.config.maxResponseCount) {
                            tc_next.before('<div class="show_all_responses comment-box level' + (tc_level + 1) + '" data-level="' + tc_level + '" data-action="all_responses"> Show all responses </div>');
                            break;
                        } else {
                            tc_next.slideDown();
                            tc_next = tc_next.next();
                            if (!is.numberOnly(tc_next.data("level"))) {
                                break;
                            }
                        }
                        response_cnt++;
                    }
                }
            });
        };

        times_comments.prototype.updateAfterCommentsLoaded = function () {
            var commentObj = this;
            if (this.pageCount === 1) {
                if (commentObj.config.commentType === this.config.commentType) {
                    if (commentObj.comments.length > 0) {
                        logger.log("updateAfterCommentsLoaded : comments.loaded:" + commentObj.config.commentType);
                        $(commentObj.config.wrapper + ".comment-section .sortby").show();
                    } else {
                        logger.log("updateAfterCommentsLoaded : comments.loaded.none:" + commentObj.config.commentType);
                        $(commentObj.config.wrapper + ".comment-section .sortby").hide();
                        $(commentObj.config.wrapper + ".comment-section .noComment").text("Be the first one to comment.").show();
                    }
                }

                if (commentObj.countPresent !== true) {
                    logger.log("updateAfterCommentsLoaded : countPresent:false:" + commentObj.config.commentType);
                    if (commentObj.comments.length === 0) {
                        $(commentObj.config.wrapper + ".comment-section .noComment").text("Be the first one to comment.").show();
                    } else if (commentObj.config.commentType === "comments_discussed") {
                        $(commentObj.config.wrapper + ".comment-section .noComment").text("None of the comments have been discussed.").show();
                    } else if (commentObj.config.commentType === "comments_agree") {
                        //$(commentObj.config.wrapper + ".comment-section .noComment").text("None of the comments have been up voted.").show();
                    } else if (commentObj.config.commentType === "comments_disagree") {
                        $(commentObj.config.wrapper + ".comment-section .noComment").text("None of the comments have been down voted.").show();
                    }
                }
            }
        };

        times_comments.prototype.renderCommentCount = function (count) {
            var tthis = this;

            function displayCount(type, cnt) {
                //$("[data-plugin='comment-count" + (type ? "-" + type : "") + "']").text(cnt ? cnt : "0");
                $(tthis.config.wrapper + " [data-plugin='comment-count" + (type ? "-" + type : "") + "']").text(cnt ? cnt : "0");
                event.publish("comment.count", (cnt ? cnt : "0"));
            }

            if (is.object(count)) {
                displayCount("", count.newest);
                displayCount("oldest", count.oldest);
                displayCount("recommended", count.recommended);
                displayCount("discussed", count.discussed);
                displayCount("agree", count.agree);
                displayCount("disagree", count.disagree);
            } else {
                displayCount("", count);
            }
        };


        /**
         *
         * @param commentType newest(default),oldest,recommended,discussed,agree,disagree
         * @param callback
         * @param append true if comments visible are not to be cleaned
         * @param curpg
         */
        times_comments.prototype.loadComment = function (commentType, callback, append, curpg) {
            var tthis = this;
            if (tthis.config.loadCommentFromMytimes == true) {
                tthis._loadCommentMytimes(commentType, function (data) {
                    tthis.render(data, callback, append);
                }, curpg);
            } else {
                tthis._loadComment(commentType, function (data) {
                    tthis.render(data, callback, append);
                }, curpg);
            }
        };

        times_comments.prototype.getData = function (callback) {
            var tthis = this;
            if (tthis.config.loadCommentFromMytimes == true) {
                this._loadCommentMytimes(this.config.commentType, callback);
            } else {
                this._loadComment(this.config.commentType, callback);
            }
        };

        times_comments.prototype._loadComment = function (commentType, callback, curpg) {
            var tthis = this;
            var config = tthis.config;//getConfig();
            tthis.loading();

//        var _comments_url = comments_url(config.msid);
            logger.log("loading comments: " + config.msid);
            api.api(commentType, {
                msid: config.msid,
                curpg: curpg || 1/*,channel:config.channel*/
            }, function (comments_data) {
//                tthis.loading(50);

                logger.log("loaded comments: " + config.msid);
                var commentsD = comments_data.articlecomment
                    || comments_data.new_cmtofart2_nit
                    || comments_data.new_cmtofart2_nit_v1
                    || comments_data.articleshow_othcmtofart
                    || comments_data.new_cmtofart2_nit_sub_dev || (comments_data.mytuserdata ? comments_data : null);
//                tthis.commentsCacheDate = tthis.commentsCacheDate || (commentsD.tdate&&commentsD.tdate.date?commentsD.tdate.date:logger.warn("Comments do not have tdate."));

                var userData = commentsD.mytuserdata && commentsD.mytuserdata['array'] ? (is.array(commentsD.mytuserdata['array']) ? commentsD.mytuserdata['array'] : [commentsD.mytuserdata['array']]) : null;
//                var commentCount = commentsD.rothrd ? commentsD.rothrd.opctr : null;
                var commentsData = commentsD.rothrd && commentsD.rothrd.op ? (is.array(commentsD.rothrd.op) ? commentsD.rothrd.op : [commentsD.rothrd.op]) : null;
                if (!commentsData || commentsData.length == 0) {
                    tthis.loadedAll();
                    callback([]);
                    return null;
                }

                tthis.flagData = commentsData[0];
                var commentCount = {
                    total: commentsD.rothrd.opctr,
                    newest: commentsD.rothrd.opctr,
                    oldest: commentsD.rothrd.opctr,
                    recommended: commentsD.rothrd.recommendcount,
                    discussed: commentsD.rothrd.opctrtopcnt, // total top level comments
                    disagree: commentsD.rothrd.disagreecount,
                    agree: commentsD.rothrd.agreecount
                };
                var commentPageCount = {
                    comments: Math.ceil(commentsD.rothrd.opctrtopcnt / config.comment_block_count),
                    comments_oldest: Math.ceil(commentsD.rothrd.opctrtopcnt / config.comment_block_count),
                    comments_recommended: Math.ceil(commentsD.rothrd.recommendcount / config.comment_block_count),
                    comments_discussed: Math.ceil(commentsD.rothrd.opctrtopcnt / config.comment_block_count),
                    comments_disagree: Math.ceil(commentsD.rothrd.disagreecount / config.comment_block_count),
                    comments_agree: Math.ceil(commentsD.rothrd.agreecount / config.comment_block_count)
                };

                var users = [];//getUsers(commentsData);
//                console.log("---------------------");
                var i = 0;
                if (userData) {
                    for (i = 0; i < userData.length; i++) {
                        users.push(userData[i].sso);
                    }
                }

                var commentsResult = [];

                for (var c = 0; c < commentsData.length; c++) {
                    var cmt = commentsData[c];
                    var user = userData ? userData[users.indexOf(cmt.roaltdetails.fromaddress)] : null;
                    var optext = cmt.optext;
                    optext = (typeof optext === "object") && optext.hasOwnProperty('content') ? String(optext.content) : optext;
                    var comment = {
                        index: ++tthis.commentCount,
                        id: cmt.messageid,
                        comment: is.string(optext) ? $("<div/>").text(optext).html() : "",
                        level: cmt.level,
                        childcount: cmt.CHILD ? cmt.CHILD.length : 0,
                        parentuid: cmt.parentuid,
                        parentusername: cmt.parentusername,
                        time: cmt.rodate ? util.getDate(cmt.rodate).getTime() : "",//1384934349412,    //cmt.rodate //19 Nov, 2013 01:04 PM
                        abstime: cmt.rodate,
                        opinion: (function (cmt) {
                            var opinions = [];
                            if (tthis.config.opinions) {
                                for (var o = 0; o < tthis.config.opinions.length; o++) {
                                    opinions.push({
                                        name: tthis.config.opinions[o].name,
                                        id: tthis.config.opinions[o].id,
                                        count: cmt[tthis.config.opinions[o].id]
                                    });
                                }
                            }
                            return opinions.length > 0 ? opinions : null;
                        }(cmt)),

                        user: {
                            //TODO fix multiple condition check ,user tthis.attachUserToComment(comment,user);
                            id: user && user._id ? user._id : cmt.roaltdetails.fromname,
                            username: user ? user.D_N_U : null,
                            name: cmt.roaltdetails.fromname || (user && user.FL_N ? user.FL_N : ""),    //name used from comment because name in replies to does not match user name in mytimes
                            location: user && user.CITY && !is.object(user.CITY) && !is.empty(user.CITY) ? user.CITY : (!is.empty(cmt.roaltdetails.location) && !is.object(cmt.roaltdetails.location) ? cmt.roaltdetails.location : null),
                            image: user && user.thumb ? user.thumb : cmt.roaltdetails.imageurl,
                            email: user ? user.sso : "",
                            rate: cmt.roaltdetails.urs,
                            followers: user ? user.F_C : 0,
                            follower_text: (user ? user.F_C : 0) > 1 ? ('(' + user.F_C + ' followers)') : ((user ? user.F_C : '') > 0 ? '(' + user.F_C + ' follower)' : ''),
                            points: user && user.reward && user.reward.user && user.reward.user.statusPoints ? user.reward.user.statusPoints : null,
                            pointslevel: user && user.reward && user.reward.user && user.reward.user.levelName ? user.reward.user.levelName : null,
                            pointsNeeded: (function (user) {
                                var points_needed = '';
                                var user_points = user && user.reward && user.reward.user && user.reward.user.statusPoints ? user.reward.user.statusPoints : null;
                                if (user_points == null || user_points < 250) {
                                    points_needed = 250 - user_points;
                                } else if (user_points > 249 && user_points < 5000) {
                                    points_needed = 5000 - user_points;
                                } else if (user_points > 4999 && user_points < 25000) {
                                    points_needed = 25000 - user_points;
                                } else if (user_points > 24999 && user_points < 250000) {
                                    points_needed = 250000 - user_points;
                                }
                                return points_needed;
                            }(user)),

                            badge: (function (user) {
                                var badges = [];
                                if (user && user.rewardpoint && user.rewardpoint.userbadges && user.rewardpoint.userbadges.activityBadge) {
                                    var activityBadge = user.rewardpoint.userbadges.activityBadge;
                                    if (!is.array(activityBadge)) { // Backend gives object when length = 1, converting ot array
                                        activityBadge = [activityBadge];
                                    }
                                    for (var d = 0; d < activityBadge.length; d++) {  //todo remove loop
                                        var badge = activityBadge[d].currentBadge;
                                        badges.push({
                                            name: badge.bname,
                                            count: badge.level,
                                            image: badge.bimg,
                                            desc: badge.desc,
                                            levelDesc: util.decodeHTML(badge.levelDesc)
                                        });
                                    }
                                }
                                return badges.length > 0 ? badges : null;
                            }(user))

                        }
                    };

                    commentsResult.push(comment);
//                        if( c > 10){
//                            break;
//                        }
                    logger.log("Parsed comment " + c);
                }

                tthis.comments = tthis.comments.concat(commentsResult);

                logger.log("rendering  " + commentsResult.length + " comments");
                callback(commentsResult, commentCount);
                logger.log("rendered " + commentsResult.length + " comments");

                tthis.loaded();
                var curpgn = curpg || 1;
                if (tthis.config.loadonscroll == false && !commentsResult || !(curpgn < commentPageCount[tthis.config.commentType])) {
                    tthis.loadedAll();
                } else if (tthis.config.loadonscroll == true && !commentsResult || commentsResult.length == 0) {
                    tthis.loadedAll();
                }

            });

            return null;
        };


        times_comments.prototype.parseComment = function (cmt, commentsResult, level) {
            var ttthis = this;
            var user = cmt.user_detail || {
                    FL_N: cmt.A_D_N || cmt.F_NAME,
                    CITY: cmt.CITY
                };
            user.reward = {user: cmt.user_reward};
            user.rewardpoint = {userbadges: cmt.user_reward_point_info};
            user.rate = cmt && cmt.U_R ? cmt.U_R : '';
            var tthis = this;

            level = level || 1;

            var comment = {
                index: ++tthis.commentCount,
                id: cmt.A_U_I,
//                comment: cmt.C_T , //is.string(cmt.C_T) ? $("<div/>").text(cmt.C_T).html() : "",
                comment: is.string(cmt.C_T) ? $("<div/>").html(cmt.C_T).text() : "",
                trimcom: (function (cmt) {
                    var t_c = is.string(cmt.C_T) ? $("<div/>").html(cmt.C_T).text() : "";
                    return util.trimText(t_c, ttthis.config.maxCommentWrapLength);
                }(cmt)),
                authrid: (cmt.C_A_ID ? cmt.C_A_ID : ""),
                level: level,
                parentuid: cmt.O_ID,
                childcount: cmt.CHILD ? cmt.CHILD.length : 0,
                parentusername: cmt.O_D_N || cmt.F_NAME,
                time: (cmt.A_DT ? util.getDate(parseInt(cmt.A_DT, 10)).getTime() : ""),//1384934349412,    //cmt.rodate //19 Nov, 2013 01:04 PM
                abstime: cmt.A_DT ? util.getDate(parseInt(cmt.A_DT, 10)).format("DD MMM, YYYY HH:mm A") : "",
                opinion: (function (cmt) {
                    var opinions = [];

                    opinions.push({name: "Agree", id: "AC_A_C", count: (cmt.AC_A_C) ? cmt.AC_A_C : 0});
                    opinions.push({name: "Disagree", id: "AC_D_C", count: (cmt.AC_D_C) ? cmt.AC_D_C : 0});
                    opinions.push({name: "Recommend", id: "AC_R_C", count: (cmt.AC_R_C) ? cmt.AC_R_C : 0});
                    opinions.push({name: "Offensive", id: "AC_O_C", count: (cmt.AC_O_C) ? cmt.AC_O_C : 0});

                    return opinions.length > 0 ? opinions : null;
                }(cmt)),

                user: {
                    //TODO fix multiple condition check ,user tthis.attachUserToComment(comment,user);
                    id: user && user._id ? user._id : null,
                    username: user && user.D_N_U ? user.D_N_U : null,
                    name: user && user.FL_N ? user.FL_N : "",    //name used from comment because name in replies to does not match user name in mytimes
                    location: user && user.CITY && !is.object(user.CITY) && !is.empty(user.CITY) && (user.CITY != "Unknown") ? user.CITY : null,
                    image: user && user.thumb ? user.thumb : "",
                    email: user && user.sso ? user.sso : "",
                    rate: user && user.rate ? user.rate : "",
                    followers: user && user.F_C ? user.F_C : 0,
                    follower_text: (user ? user.F_C : 0) > 1 ? ('(' + user.F_C + ' followers)') : ((user ? user.F_C : '') > 0 ? '(' + user.F_C + ' follower)' : ''),
                    points: user && user.reward && user.reward.user && user.reward.user.statusPoints ? user.reward.user.statusPoints : null,
                    pointslevel: user && user.reward && user.reward.user && user.reward.user.levelName ? user.reward.user.levelName : null,
                    pointsNeeded: (function (user) {
                        var points_needed = '';
                        var user_points = user && user.reward && user.reward.user && user.reward.user.statusPoints ? user.reward.user.statusPoints : null;
                        if (user_points == null || user_points < 250) {
                            points_needed = 250 - user_points;
                        } else if (user_points > 249 && user_points < 5000) {
                            points_needed = 5000 - user_points;
                        } else if (user_points > 4999 && user_points < 25000) {
                            points_needed = 25000 - user_points;
                        } else if (user_points > 24999 && user_points < 250000) {
                            points_needed = 250000 - user_points;
                        }
                        return points_needed;
                    }(user)),

                    badge: (function (user) {
                        var badges = [];
                        if (user && user.rewardpoint && user.rewardpoint.userbadges && user.rewardpoint.userbadges.activityBadge) {
                            var activityBadge = user.rewardpoint.userbadges.activityBadge;
                            if (!is.array(activityBadge)) { // Backend gives object when length = 1, converting ot array
                                activityBadge = [activityBadge];
                            }
                            for (var d = 0; d < activityBadge.length; d++) {  //todo remove loop
                                var badge = activityBadge[d].currentBadge;
                                badges.push({
                                    name: badge.bname,
                                    count: badge.level,
                                    image: badge.bimg,
                                    desc: badge.desc,
                                    levelDesc: util.decodeHTML(badge.levelDesc)
                                });
                            }
                        }
                        return badges.length > 0 ? badges : null;
                    }(user))

                }
            };
            commentsResult.push(comment);

            if (cmt.CHILD) {
                for (var cmtChild in cmt.CHILD) {
                    tthis.parseComment(cmt.CHILD[cmtChild], commentsResult, level + 1);
                }
            }

        };

        times_comments.prototype._loadCommentMytimes = function (commentType, callback, curpg) {
            var tthis = this;
            var config = tthis.config;//getConfig();
            tthis.loading();

//        var _comments_url = comments_url(config.msid);
            logger.log("loading comments: " + config.msid);
            api.api(commentType, {
                msid: config.msid,
                pagenum: curpg || 1/*,channel:config.channel*/
            }, function (comments_data) {
//                tthis.loading(50);

                logger.log("loaded comments: " + config.msid);
                var commentsData = comments_data;
//                tthis.commentsCacheDate = tthis.commentsCacheDate || (commentsData.tdate&&commentsData.tdate.date?commentsD.tdate.date:logger.warn("Comments do not have tdate."));

                if (!commentsData || commentsData.length == 0) {
                    tthis.loadedAll();
                    callback([]);
                    return null;
                }

                tthis.flagData = commentsData[0];
                var commentCount = {
                    total: commentsData[0].totalcount,
                    newest: commentsData[0].totalcount,
                    oldest: commentsData[0].totalcount,
                    recommended: commentsData[0].totalcount,
                    discussed: commentsData[0].totalcount,
                    disagree: commentsData[0].totalcount,
                    agree: commentsData[0].totalcount
                };
//                var commentCount = {
//                    total: commentsD.rothrd.opctr,
//                    newest: commentsD.rothrd.opctr,
//                    oldest: commentsD.rothrd.opctr,
//                    recommended: commentsD.rothrd.recommendcount,
//                    discussed: commentsD.rothrd.opctrtopcnt, // total top level comments
//                    disagree: commentsD.rothrd.disagreecount,
//                    agree: commentsD.rothrd.agreecount
//                };
                var commentPageCount = {
                    comments: Math.ceil(commentCount.newest / config.comment_block_count),
                    comments_oldest: Math.ceil(commentCount.oldest / config.comment_block_count),
                    comments_recommended: Math.ceil(commentCount.recommended / config.comment_block_count),
                    comments_discussed: Math.ceil(commentCount.discussed / config.comment_block_count),
                    comments_disagree: Math.ceil(commentCount.disagree / config.comment_block_count),
                    comments_agree: Math.ceil(commentCount.agree / config.comment_block_count)
                };

//                var users = [];//getUsers(commentsData);
////                console.log("---------------------");
//                var i = 0;
//                if (userData) {
//                    for (i = 0; i < userData.length; i++) {
//                        users.push(userData[i].sso);
//                    }
//                }

                var commentsResult = [], priorityComments=[];
                
                if(commentsData && commentsData[0]['priorityComment'] && commentsData[0]['priorityComment'].length > 0){
                        var cmnts = commentsData[0]['priorityComment'];
                        for (var c = 0; c < cmnts.length; c++) {
                                var cmt = cmnts[c];
                                var reason = null;
            
                                if (cmt.DLT == 1) {
                                    reason = "DLT fail";
                                } else if (is.empty(cmt.C_T)) {
                                    reason = "Blank fail";
                                }
                                if (reason) {
                                    logger.log("Not Parsed " + reason + " comment " + c);
                                } else {
                                    tthis.parseComment(cmt, priorityComments);
                                    logger.log("Parsed comment " + c)
                                }
                            }
                    }

                for (var c = 1; c < commentsData.length; c++) {
                    var cmt = commentsData[c];
                    var reason = null;

                    if (cmt.DLT == 1) {
                        reason = "DLT fail";
                    } else if (is.empty(cmt.C_T)) {
                        reason = "Blank fail";
                    }
                    if (reason) {
                        logger.log("Not Parsed " + reason + " comment " + c);
                    } else {
                        tthis.parseComment(cmt, commentsResult);
                        logger.log("Parsed comment " + c)
                    }
                }

                tthis.countPresent = commentsData[0].countPresent || false;
                tthis.comments = tthis.comments.concat(commentsResult);
                tthis.priorityComments = priorityComments;

                logger.log("rendering  " + commentsResult.length + " comments");
                callback(commentsResult, commentCount);
                logger.log("rendered " + commentsResult.length + " comments");

                tthis.loaded();
                var curpgn = curpg || 1;
                if (tthis.config.loadonscroll == false && !commentsResult || !(curpgn < commentPageCount[tthis.config.commentType])) {
                    tthis.loadedAll();
                } else if (tthis.config.loadonscroll == true && !commentsResult || commentsResult.length == 0) {
                    tthis.loadedAll();
                }

            });

            return null;
        };
        function replaceAll(find, replace, str) {
            return str.replace(new RegExp(find, 'g'), replace);
        }


        var domain = "http://jcmsdev.indiatimes.com";

        var getUsers = function (data) {
            var ssoids = {};
            for (var i = 0; i < data.length; i++) {
                ssoids[(data[i].roaltdetails.fromaddress)] = {};
            }
            return ssoids;
        };


        times_comments.prototype.postToSocial = function (message, sites, closeCallback) {
            var tthis = this;
            var domainOnly = util.getDomainOnly();

            var user = login.getUser();

            window.newShare = true;
            window.newShareData = {
//                site:"facebook",
                sites: sites,
                message: message,
                link: window.facebooklink || page.getMeta('og:url'),
                title: window.facebookktitle || page.getMeta('og:title'),
                domain: window.location.host,
                picture: window.fb_Img || page.getMeta('og:image'),
                desc: window.facebooksyn || page.getMeta('og:description'),
                facebook: user ? user.facebook : null,
                twitter: user ? user.twitter : null
            };

            /******************Comment Share on FB*****************/
            if (-1 != window.newShareData.sites.indexOf('facebook')) {
                if (typeof FB == 'undefined') {
                    require(["tiljs/social/facebook", "tiljs/event"], function (facebook, event) {
                        // Load FB sdk if it's not loaded
                        facebook.init({init: true, parse: true});
                        //Open FB feed dialog to post comment once sdk is loaded
                        event.subscribe("FB.onload", function (data) {
                            tthis.postToFB(window.newShareData);
                        });
                    });
                } else {
                    tthis.postToFB(window.newShareData);
                }
                sites = sites.replace(/facebook,|facebook/g, "");
                window.newShareData.sites = sites;
            }
            if (!sites.length) {
                return true;
            }
            /*****************************************************/

           // var url = tthis.config.share_url + "?sites=" + sites;
            var url = site_url + "/phpshare?sites=" + sites;
            //alert(url);
            var socialPost = ui.window(url, {
                width: 600,
                height: 230,
                name: "socialPost",
                closeCallback: closeCallback
            });
            if (socialPost) {
                try {
                    socialPost.moveTo(250, 240);
                } catch (e) {
                    logger.log("Handled IE Exception.");
                    logger.error(e);
                }
            } else {
//                window.popupblocked = 1;
//                if(is.desktop()){
//                    twitterPost = ui.iframe(url, {width: 575, height: 314, name: "twitterPost", closeCallback: closeCallback});
//                }else{
                logger.error("Popups are blocked. Please enable them.");
//                }
            }
        };

        times_comments.prototype.postToFB = function (data) {
            FB.ui({
                method: 'feed',
                message: data.message,
                name: data.title,
                link: data.link,
                picture: data.picture,
                caption: data.domain,
                description: data.desc
            });
        };

        times_comments.prototype.postToTwitter = function (message, closeCallback) {
            var domainOnly = util.getDomainOnly();

            if (message.length > 100) {
                message = message.substring(0, 99) + "...";
            }

            window.log = window.log || function () {
                };
            window.popupblocked = 0;
            cookie.remove("fbcheck", "/", domainOnly);
            cookie.remove("twtcheck", "/", domainOnly);

            cookie.set("twtcheck", "1", 1, "/", domainOnly);
            cookie.set("usercomt", message, 1, "/", domainOnly);

            var url = "/stgredirectpagetest.cms";
            var twitterPost = ui.window(url, {
                width: 600,
                height: 230,
                name: "twitterPost",
                closeCallback: closeCallback
            });
            if (twitterPost) {
                twitterPost.moveTo(250, 240);
            } else {
                window.popupblocked = 1;
                if (is.desktop()) {
                    twitterPost = ui.iframe(url, {
                        width: 575,
                        height: 314,
                        name: "twitterPost",
                        closeCallback: closeCallback
                    });
                } else {
                    alert("Popups are blocked. Please enable them.");
                }
            }


//            var url = "https://twitter.com/share?text=" + message + "&url=" + document.location.href;
//            var twitterPost = ui.window(url, {width: 750, height: 500, name: "twitterPost", closeCallback: closeCallback});
//            if(twitterPost){
//                try{//Not working in IE10 - TOIPR-5367
//                    twitterPost.moveTo(275, 275);
//                }catch(e){
//                    logger.warn("Exception handled for IE10");
//                    logger.error(e);
//                }
//            }else{
//                if(is.desktop()){
//                    ui.iframe(url, {width: 750, height: 500, name: "twitterPost", closeCallback: closeCallback});
//                }else{
//                    alert("Popups are blocked. Please enable them.");
//                }
//            }
        };

        times_comments.prototype.postToFacebook = function (message, closeCallback) {
            var domainOnly = util.getDomainOnly();
            var tthis = this;

            //todo remove cookie use
            if (window.localStorage) {
                localStorage.setItem("usercomt", message);
            }

            window.log = window.log || function () {
                };
            window.popupblocked = 0;
            cookie.remove("fbcheck", "/", domainOnly);
            cookie.remove("twtcheck", "/", domainOnly);

            cookie.set("fbcheck", "1", 1, "/", domainOnly);
            cookie.set("usercomt", message, 1, "/", domainOnly);

            var url = "/stgredirectpagetest.cms";
//            facebook.getPermissions("publish_stream",function(got_permission){
//                if(got_permission === true){
            var facebookPost = ui.window(url, {
                width: 600,
                height: 230,
                name: "facebookPost",
                closeCallback: closeCallback
            });
            if (facebookPost) {
                facebookPost.moveTo(250, 240);
            } else {
                window.popupblocked = 1;
                if (is.desktop()) {
                    facebookPost = ui.iframe(url, {
                        width: 575,
                        height: 314,
                        name: "facebookPost",
                        closeCallback: closeCallback
                    });
                } else {
                    alert("Popups are blocked. Please enable them.");
                }
            }
//                }   else{
//                    tthis.error(config.messages.permission_facebook);
//                }
//            });


            /*
             if (window.FB) {  //todo use facebook plugin
             FB.ui({
             method: "stream.publish",
             message: message,
             attachment: {media: [
             {type: "image", href: window.facebooklink, src: window.fb_Img}
             ], name: window.facebookktitle,
             caption: "timesofindia.indiatimes.com",
             description: window.facebooksyn,
             href: window.facebooklink},
             user_message_prompt: "What's on your mind?"}, function (d) {
             //                    if (d && d.post_id) {
             //                    } else {
             //                    }
             });
             }else{
             logger.warn("'window.FB' is not defined. Will try again after 2secs.");
             timer.available("FB",function(FB){
             if(FB){
             toi_comments.postToFacebook(message);
             }else{
             logger.error("'window.FB' is not defined. Giving up.");
             }
             },3000, 10);
             }     */
        };

        times_comments.prototype.attachOpinionAction = function () {
            logger.log("Attaching Opinion Action");
            var tthis = this;
            var opinions = this.config.opinions;

            for (var o = 0; o < opinions.length; o++) {
                var opinion = opinions[o];
                (function (opinion) {
                    $(tthis.config.wrapper).off("click", "[data-action='comment-" + opinion.id + "']");
                    $(tthis.config.wrapper).on("click", "[data-action='comment-" + opinion.id + "']", function () {
                        var ref = ui.getActionReferences(this, tthis.config.comment);
                        var parent = ref.parent;

                        var opinionid = parent.attr("data-id");
                        //rateAgree, rateDisagree, rateRecommend, rateOffensive
                        (function (tthis, parent, opinion, ref) {
                            tthis["rate" + opinion.name](opinionid, function (errMsg) {
                                if (errMsg) {
                                    tthis.error(errMsg, ref["comment-error-outer"]);
                                } else {
                                    tthis.error("", ref["comment-error-outer"]);
                                    var count_ele = parent.find("[data-plugin='comment-" + opinion.id + "-count']");
                                    var currentCount = parseInt(count_ele.text(), 10);
                                    $(count_ele).text(++currentCount);
                                }
                            }, ref);
                        }(tthis, parent, opinion, ref));
                        event.publish("comment.opinion", opinion.id);
                        return false;
                    });
                }(opinion))
            }

            $(tthis.config.wrapper).off('focus', '[data-plugin="comment-input"]');
            $(tthis.config.wrapper).on('focus', '[data-plugin="comment-input"]', function () {
                var ref = ui.getActionReferences(this, tthis.config.comment);
                if(!commentReplyFormHTML){
					commentReplyFormHTML = $(tthis.config.wrapper + " [data-plugin='comment-form']")[0].outerHTML;
				}
                if (ref["comment-form"] && !ref["comment-form"].hasClass('full')) {
                    ref["comment-form"].addClass("full");
                    event.publish("comment.form.addclass.full", this);
                } else if (ref["comment-reply"] && !ref["comment-reply"].hasClass('full')) {
                    ref["comment-reply"].addClass("full");
                     event.publish("comment.reply.form.addclass.full", this);
                }
                //setcaptcha(ref);
                console.log("comment.form.input.focus");
                event.publish("comment.form.input.focus", this);
            });


            $(tthis.config.wrapper).off("focus", "[data-plugin='get-user-name']");
            $(tthis.config.wrapper).on("focus", "[data-plugin='get-user-name']", function () {
                var ref = ui.getActionReferences(this, tthis.config.comment);
                if (ref["comment-form-login"]) {
                    ref["comment-form-login"].removeClass("noreg");
                    setcaptcha(ref);
                }
            });
            $(tthis.config.wrapper).on("mouseover", ".badges .more-btn", function (event) {
                var elemTop = $(event.target).closest('.comment-box').position().top + $(event.target).closest('.comment-box').closest('.comment-box').height(),
                    elemH = $(event.target).find('.popup_badge').height(),
                    wH = $(window).height(),
                    commentScrollPos = 0
                // $('[data-plugin="offensive_popup"]').hide();
                $(event.target).find('.popup_badge').removeClass('popup_above');

                if (((elemTop - commentScrollPos) + elemH) > wH) {
                    $(event.target).find('.popup_badge').addClass('popup_above');
                }
                $(event.target).find('.popup_badge').show();
            });

            $(tthis.config.wrapper).on("mouseout", ".badges .more-btn", function (event) {

                $(event.target).find('.popup_badge').hide();
            });


            function setcaptcha(ref) {
                var $set_user_captcha = ref['set-user-captcha'];
                if ($set_user_captcha.text().length == 0) {
                    $set_user_captcha.text(Math.floor((Math.random() * 10)) + "+" + Math.floor((Math.random() * 10)) + "=");
                }
            }

            $(tthis.config.wrapper).off("click", "[data-action='offensive_popup_submit']");
            $(tthis.config.wrapper).on("click", "[data-action='offensive_popup_submit']", function () {
                var ref = ui.getActionReferences(this, tthis.config.comment);
                var opinionid = ref.parent.attr("data-id");
                var reason = null;
                reason = ref.offensive_popup.find("input:checked").val();

                if (!reason) {
                    tthis.error(tthis.config.messages.offensive_reason, ref["comment-offensive-error"]);
                } else if (reason == "Others") {
                    var reasonVal = ref.offensive_popup_reason.val().trim();
                    if (!reasonVal || reasonVal == "") {
                        tthis.error(tthis.config.messages.offensive_reason_text, ref["comment-offensive-error"]);
                        reason = null;
                    } else {
                        if (reasonVal.length > 200) {
                            tthis.error(tthis.config.messages.offensive_reason_text_limit, ref["comment-offensive-error"]);
                            reason = null;
                        } else {
                            reason = "Others" + ": " + reasonVal;
                        }
//                        tthis.rateOffensive(opinionid,function(errMsg){
//                            if (errMsg) {
//                                tthis.error(errMsg, ref["comment-offensive-error"]);
//                            } else {
//                                tthis.error("", ref["comment-offensive-error"]);
//
//                                if(ref.parent){
//                                    ref.parent.find("[data-action='comment-offensive-already']").show();
//                                    ref.parent.find("[data-action='comment-offensive']").hide();
//                                }
//
//                                ref.offensive_popup.hide();
//                            }
//                        },ref, reason);
                    }
                }

                if (reason) {
                    tthis.rate(opinionid, CONSTANT.RATE_TYPE.OFFENSIVE, 0, function (error, resp) {
                        if (error) {
                            tthis.error(error, ref["comment-offensive-error"]);
                        } else if (!error && ref.parent) {
//                            ref.parent.find("[data-action='comment-offensive-already']").show();
//                            ref.parent.find("[data-action='comment-offensive']").hide();
//                            ref.offensive_popup.hide();
                            ref.offensive_popup.addClass("submitted");
                        }


                    }, ref, reason);
                }


//                ref.offensive_popup.find();
            });


            $(tthis.config.wrapper).off("click", "[data-action='offensive_popup_close']");
            $(tthis.config.wrapper).on("click", "[data-action='offensive_popup_close']", function () {
                var ref = ui.getActionReferences(this, tthis.config.comment);

                if (ref.offensive_popup && ref.offensive_popup.hasClass("submitted")) {
                    ref.parent.find("[data-action='comment-offensive-already']").show();
                    ref.parent.find("[data-action='comment-offensive']").hide();
                }
                ref.offensive_popup.hide();

                return false;
            });

        };

        times_comments.prototype.attachReplyAction = function () {
            logger.log("Attaching Reply Action");
            var tthis = this;

            $(tthis.config.wrapper).off("click", "[data-action='all_responses']");
            $(tthis.config.wrapper).on("click", "[data-action='all_responses']", function () {
                var tc_level = $(this).data("level"),
                    tc_next = $(this).next();
                while (tc_next.data("level") != tc_level) {
                    tc_next.slideDown();
                    tc_next = tc_next.next();
                    if (!is.numberOnly(tc_next.data("level"))) {
                        break;
                    }
                }
                $(this).remove();
            });

            $(tthis.config.wrapper).off('click', "[data-action='more-response-length']");
            $(tthis.config.wrapper).on("click", "[data-action='more-response-length']", function () {
                $(this).parent().hide();
                $(this).parent().next().show();
                event.publish("comment.readmore");
            });


            $(tthis.config.wrapper).off("click", "[data-action='comment-reply']");
            $(tthis.config.wrapper).on("click", "[data-action='comment-reply']", function () {
                var ref = ui.getActionReferences(this, tthis.config.comment);

                var parent = ref["parent"];
                if (ref["comment-reply"]) {
                    ref["comment-reply"].show();
                    if (is.defined(tthis.config.rating) && tthis.config.rating) {
                        if (userInfo.userRating != null && userInfo.userRating != undefined && userInfo.userRating != "") {
                            $(tthis.config.wrapper + " [data-plugin='comment-rating']").val($(tthis.config.wrapper + " [data-plugin='rating-list'] [data-plugin='rating-vaue'][val=" + userInfo.userRating + "]").html());
                        }
                    }
                } else {
                    /*var commentForm = $(tthis.config.wrapper + " [data-plugin='comment-form']");
                    var commentFormHTML = $("<div>" + commentForm.html() + "</div>");
                    commentFormHTML.find("[data-clear='true']").remove();
                    // changes for prime comments box
                    if(commentForm.hasClass('prime-comment')) {
                        commentFormHTML.find('[data-plugin="comment-post"]').val('Reply');
                        commentFormHTML.find('[data-action="comment-close"]').remove();
                        $('<input type="bu192.169.34.129tton" value="Cancel" class="button1 close" data-action="comment-close" title="Cancel"/>').insertBefore(commentFormHTML.find('[data-plugin="comment-post"]'));
                    }
                    var tmpl = $.templates("<div class='" + commentForm.attr("class") + " reply' data-plugin='comment-reply'>" + commentFormHTML.html() + "</div>");
                            */
                           if(commentReplyFormHTML){
						var commentForm = $(commentReplyFormHTML);
					} else {
						var commentForm = $(tthis.config.wrapper + " [data-plugin='comment-form']");
					}
                    var commentFormHTML = $("<div>" + commentForm.html()+ "</div>");
					commentFormHTML.find("[data-clear='true']").remove();
                    var tmpl = $.templates( "<div class='" + commentForm.attr("class") + " reply' data-plugin='comment-reply'>" +commentFormHTML.html() + "</div>");
                    parent.append(tmpl.render([
                        {}
                    ]));
                    if (is.defined(tthis.config.rating) && tthis.config.rating) {
                        //$('.readrate').remove();
                        if (userInfo.userRating != null && userInfo.userRating != undefined && userInfo.userRating != "") {
                            $(tthis.config.wrapper + " [data-plugin='comment-rating']").val($(tthis.config.wrapper + " [data-plugin='rating-list'] [data-plugin='rating-vaue'][val=" + userInfo.userRating + "]").html());
                        }
                    }
                }
                //updated references
                ref = ui.getActionReferences(this, tthis.config.comment);
//                ref["comment-input"].keyup();
//                ref["comment-input"].focus();
                tthis.error("", ref["comment-error"]);
                //Trigger placeholders
                $('[placeholder]').blur();
                return false;

            });

            $(tthis.config.wrapper).off("click", "[data-action='toggle_replies']");
            $(tthis.config.wrapper).on("click", "[data-action='toggle_replies']", function () {
                var t = $(this);
                // var tc=t.parent();
                var tc = t.closest('.comment-box');
                var tc_level = tc.data("level");
                var tc_next = tc.next();
                var tstatus = t.attr("data-togglereplies");
                if (is.numberOnly(tc_next.data("level")) && tstatus == "show") {
                    event.publish("comment.togglereplies", "Show");
                    t.attr("data-togglereplies", "hide");
                    var response_cnt = 0;
                    while (tc_next.data("level") != tc_level) {
                        if (response_cnt == 3) {
                            tc_next.before('<div class="show_all_responses comment-box level' + (tc_level + 1) + '" data-level=' + tc_level + ' data-action="all_responses"> Show all responses </div>');
                            break;
                        } else {
                            tc_next.slideDown();
                            tc_next = tc_next.next();
                            if (!is.numberOnly(tc_next.data("level"))) {
                                break;
                            }
                        }
                        response_cnt++;
                    }
                } else {
                    event.publish("comment.togglereplies", "Hide");
                    t.attr("data-togglereplies", "show");
                    $('.show_all_responses').remove();
                    while (tc_next.data("level") != tc_level) {
                        tc_next.slideUp();
                        tc_next = tc_next.next();
                        if (!is.numberOnly(tc_next.data("level"))) {
                            break;
                        }
                    }
                }
            });

            $(tthis.config.wrapper).off('click', '.loadmore');
            $(tthis.config.wrapper).on('click', '.loadmore', function () {
                $(this).remove();
                if (tthis.config.loadonscroll == false) {
                    tthis.loadComment(tthis.config.commentType, function (data) {
                    }, true, ++tthis.pageCount);
                }
            });

            $(tthis.config.wrapper).off("keyup", "[data-plugin='comment-input']");
            $(tthis.config.wrapper).on("keyup", "[data-plugin='comment-input']", function (e) {
                if (e.keyCode === 27) {//Esc Key
                    var ref = ui.getActionReferences(this, tthis.config.comment);
                    ref["comment-input"].val("");
                    ref["comment-reply"].hide();
                }
            });

            ui.maxlength("[data-plugin='comment-input']", tthis.config.maxchar, function (remainingChar, messageLength) {
                var ref = ui.getActionReferences(this, tthis.config.comment);
                var parent = ref["parent"];
                var charsrem = ref["comment-input-remaining"];
                charsrem.text(remainingChar >= 0 ? remainingChar : 0);

                if (tthis.config.messages.maxlength && messageLength > tthis.config.maxchar) {
                    tthis.error(tthis.config.messages.maxlength, ref["comment-error"]);
                } else {
                    tthis.error("", ref["comment-error"]);
                }

            }, tthis.config.wrapper);


            $(tthis.config.wrapper).off("click", "[data-action='comment-close']");
            $(tthis.config.wrapper).on("click", "[data-action='comment-close']", function () {
                var ref = ui.getActionReferences(this, tthis.config.comment);

                var reply = ref["comment-reply"];
                var input = ref["comment-input"];
                ref["comment-input-remaining"].text(tthis.config.maxchar)
                ref["comment-post"].removeClass("active");
                input.val("");
                reply.hide();
                tthis.error("", ref["comment-error"]);

                return false;//todo remove this
            });

//            var bbb = $("#badge_tmpl").render([{}]);
//            $(tthis.config.wrapper).on("mouseenter mouseleave","[data-plugin='comment-user-badge']",function(evt){
//                var badge = $(this);
//                var name = badge.attr("data-name");
//                 if(evt.type == "mouseenter"){
//                     logger.log("enter:"+name);
//                     badge.append(bbb);
//                 }else if(evt.type == "mouseleave"){
//                     logger.log("leave:"+name);
//                     $(badge).find(".badge_new").remove();
//                 }
//            });

            tthis.onLoaded(function (tthis) {
//                tthis =  _tthis;   //changing reference of comments input when another set of comments is loaded
//                if(tthis.commentsCacheDate){
                tthis.updateCachedRating();
//                }
                var user = login.getUser();
                if (tthis.followee) {
                    tthis.markFollowingAll();
                } else {
                    
                    if(user){
                            mytimes.getFollowee(function (data) {
                                tthis.followee = data;
                                tthis.markFollowingAll();
                            });
                    }
                }

                lazy.load();
                dynamic.uptime();
            });

            $(tthis.config.wrapper).off("click", "[data-plugin='comment-user-follow']");
            $(tthis.config.wrapper).on("click", "[data-plugin='comment-user-follow']", function () {
                var currentEle = $(this);
                var currentUser = login.getUser();
                var ref = ui.getActionReferences(this, tthis.config.comment);
                var userId = ref.parent.attr("data-userid");
                var userName = ref.parent.attr("data-user");
                if (currentUser && currentUser.getId() != userId) {
                    (function (userId, userName) {
                        mytimes.followUser(userId, function (dataa) {
                            tthis.followee = tthis.followee || [];
                            var currFollowee = {_id: userId, F_N: userName};
                            tthis.followee.push(currFollowee);
                            tthis.markFollowingId(currFollowee);
                        });
                    }(userId, userName))
                } else {
                    logger.warn("Cannot follow self.");
                }
            });


            $(tthis.config.wrapper).off("click",
                "[data-plugin='comment-post'],[data-plugin='comment-facebook-post'],[data-plugin='comment-twitter-post'],[data-plugin='comment-google-post'],[data-plugin='comment-email-post']");
            $(tthis.config.wrapper).on("click",
                "[data-plugin='comment-post'],[data-plugin='comment-facebook-post'],[data-plugin='comment-twitter-post'],[data-plugin='comment-google-post'],[data-plugin='comment-email-post']", function () {
                    event.publish("comment.post.start");
                    var ref = ui.getActionReferences(this, tthis.config.comment);

                    var parent = ref["parent"];
                    var reply = ref["comment-reply"];
                    var errorElement = ref["comment-error"];
                    var reply_input = ref["comment-input"];


                    var parentid = parent.attr("data-id");
                    var parentlevel = parent.attr("data-level");

                    var rootid = parent.attr("data-id");
                    var rootlevel = parent.attr("data-level");

                    var preventInfinite = 10;
                    var prevParent = parent;
                    while (rootlevel > 1 && --preventInfinite > 0) {
                        prevParent = prevParent.prev();
                        if (prevParent.attr("data-plugin") === "comment") {
                            rootid = prevParent.attr("data-id");
                            rootlevel = prevParent.attr("data-level");
                        } else {
                            break;
                        }
                    }

                    tthis.error("", errorElement);

                    var val = (reply_input.val() === reply_input.attr("placeholder")) ? "" : reply_input.val().trim();

//                    val = $("<div/>").text(val).html(); // encoded html content
                    if (val) {
                        tthis.add(val, parentid, rootid, rootlevel, function (commentObj, errMsg) {
                            tthis.renderComment(commentObj, errMsg);
                            event.publish("comment.post.end");
                        }, false, this);
                    }
                    else {
                        //tthis.renderComment(null, tthis.config.messages.blank);
                        event.publish("comment_error", tthis.config.messages.blank);
                        if($(tthis.config.wrapper + " [data-plugin='comment-error']").length>1){
                            $(this).parent().find("[data-plugin='comment-error']").text(tthis.config.messages.blank);
                            errorElement.text(tthis.config.messages.blank);
                        }
                        else{
                            $(tthis.config.wrapper + " [data-plugin='comment-error']").text(tthis.config.messages.blank);
                        }
                    }
//                reply_input.val("");
                    return false; //todo remove
                });

        };

        times_comments.prototype.markFollowingAll = function () {
            var tthis = this;
            var followees = tthis.followee;
            var comments = $(tthis.config.wrapper);
            var user = login.getUser();
            if (user) {
                logger.info("Showing follow links");
                comments.find("[data-plugin='comment-user-follow_wrapper']").show();

                //Hide current user follow
                //Start Added by Amit
                //comments.find("[data-plugin='comment'][data-userid!='" + user.getId() + "'] [data-plugin='comment-user-follow_wrapper']").show();
                comments.find("[data-plugin='comment'][data-userid ='" + user.getId() + "'] [data-plugin='comment-user-follow_wrapper']").hide();
//                comments.find("[data-plugin='comment'][data-userid !='" + user.getId() + "'] [data-plugin='comment-user-follow_wrapper']").removeClass('dont_show');
                // End Added by Amit
                //Mark following
                util.each(followees, function (i, followee) {
                    tthis.markFollowingId(followee);
                });
            } else {
                //Added by Amit
                comments.find("[data-plugin='comment-user-follow_wrapper']").hide();
                logger.info("Follow links : Not logged in.");
            }
        };

        times_comments.prototype.markFollowingId = function (followee) {
            var tthis = this;
            var user = login.getUser();
            var comments = $(tthis.config.wrapper);
            if (user) {
                var foloweeEle = comments.find("[data-userid='" + followee._id + "'] .follow");

                if (foloweeEle && foloweeEle.length > 0) {
                    logger.info("Mark Following : " + user.getFullName() + "(" + user.getId() + ") >> " + followee.F_N + "(" + followee._id + ")");
                    tthis.markFollowing(foloweeEle);
                } else {
                    logger.info("Mark Following : " + user.getFullName() + "(" + user.getId() + ") >> " + followee.F_N + "(" + followee._id + ") " + " - NA");
                }
            } else {
                logger.info("Follow link : Not logged in.");

            }
        };

        times_comments.prototype.markFollowing = function (currentEle) {
            if (currentEle && currentEle.length > 0) {
                currentEle.html("<span class='divider'></span><i class='icon-follow'></i>Following");
                currentEle.removeClass("follow");
                currentEle.addClass("following");
                currentEle.removeAttr("data-plugin");
                currentEle.attr("title", currentEle.attr("title").replace("Follow", "Following"));
            }
        };

        times_comments.prototype.onLoaded = function (callback) {
            event.subscribe("comments.loaded", callback);
        };

        var savedComment = null;
        times_comments.prototype.saveComment = function (comment) {
            savedComment = comment;
            cookie.set("comment" + window.msid, json.stringify(comment), 1, document.location.pathname);
        };

        times_comments.prototype.getSavedComment = function () {
            var commentCookie = cookie.get("comment" + window.msid);
            if (is.defined(commentCookie)) {
                return json.parse(commentCookie);
            } else if (savedComment) {
                return savedComment;
            } else {
                return null;
            }
        };

        times_comments.prototype.removeSavedComment = function () {
            savedComment = null;
            return cookie.remove("comment" + window.msid, document.location.pathname);
        };

        times_comments.prototype.loadSavedComment = function () {
            var tthis = this;
            var user = login.getUser();
            var savedComment = tthis.getSavedComment();
            if (savedComment) {
                if (user) {
                    if (!tthis.config.rating || savedComment.urs != '') {
                        savedComment.urs = tthis.config.rating ? $.trim(savedComment.urs) : '';
                        logger.log("Has saved comment and user is logged in.");
                        logger.log(savedComment);
                        tthis.attachUserToComment(savedComment, user);
                        //                    if(savedComment.type != "comment-email-post") {
                        tthis.removeSavedComment();
                        tthis.post(savedComment, function (savedComment, errMsg) {
                            tthis.renderComment(savedComment, errMsg);
                        });
                        //                    }
                    }
                } else {
                    logger.log("Saved comment but user not logged in");
                }
            } else {
                logger.log("No Saved comment.");
            }
        };

        times_comments.prototype.verifyEmailComment = function () {
            var url_params = util.getParam();

//            if(!is.empty(url_params.messageid) && !is.empty(url_params.r)){

            var tthis = this;
            tthis.verify(function (commentObj, errMsg) {
                if (is.object(commentObj)) {
                    tthis.renderComment(commentObj, errMsg, true);
                    $(tthis.config.wrapper + " [data-plugin='comment-verified-msg']").show();
                    tthis.removeSavedComment();
                }
            });
//            }

        };

        times_comments.prototype.saveRatingValidation = function (opinionid, typeid, rateid, val, ref) {
            logger.log("Save Rating: " + this.config.commentType + ":" + opinionid);

            var user = login.getUser();
            var userId = user ? user.getId() : 0;

            var rating = ["rateV", opinionid, typeid, rateid];
//            var ratingC = ["rateC", opinionid, typeid, rateid];
            localstoragec.set(rating.join(":"), val || "1", 1, '/');
//            cookie.set(rating.join(":"), val || "1", 1, document.location.pathname);
//            cookie.set(ratingC.join(":"), val || "1", (1/24/60)*7, document.location.pathname);//7 minutes
//            this.commentsCacheUpdated.push(ratingC.join(":"));
        };

        times_comments.prototype.getSavedRatingValidation = function (opinionid, typeid, rateid, ref) {
            var user = login.getUser();
            var userId = user ? user.getId() : 0;

            var rating = ["rateV", opinionid, typeid, rateid];
            var commentCookie = localstoragec.csget(rating.join(":")); //cookie.get(rating.join(":"));
            if (is.defined(commentCookie)) {
                return commentCookie;
            } else {
                return null;
            }
        };

        times_comments.prototype.updateCachedRating = function () {
            var tthis = this;
            var cookies = cookie.getAll();
            var comments = $(tthis.config.wrapper + " #" + this.config.commentType);
            util.each(cookies, function (key, value) {
                if (key.indexOf("rateV") === 0/* && tthis.commentsCacheUpdated.indexOf(key) < 0 */) {
//                    if(value == tthis.commentsCacheDate){
                    logger.log("Updating Rating from cookie: " + key);
//                        tthis.commentsCacheUpdated.push(key);
                    var keyArr = key.split(":");
                    var rating = {
                        opinionid: parseInt(keyArr[1], 10),
                        typeid: parseInt(keyArr[2], 10),
                        rateid: parseInt(keyArr[3], 10),
                        commentType: tthis.config.commentType
                    };
                    //todo merge two if cases
                    var eleType = "";
                    switch (rating.typeid) {
                        case CONSTANT.RATE_TYPE.AGREE:
                            eleType = "comment-agree-count";
                            break;
                        case CONSTANT.RATE_TYPE.DISAGREE:
                            eleType = "comment-disagree-count";
                            break;
                    }
                    if (eleType != "") {
                        var commentRateCount = comments.find("[data-id='" + rating.opinionid + "']  [data-plugin='" + eleType + "']");
                        var val = parseInt(commentRateCount.text(), 10);
                        var cookieVal = parseInt(cookie.get(key), 10);
                        if (cookieVal > val) {
                            commentRateCount.text(cookieVal);
                            logger.log("Updated Rating from cookie: " + key + " : " + val + " updated to " + cookieVal);
                        } else {
                            //It is already updated
                            logger.log(key + " Already Updated");
                        }
                    }
//                    }else{//Remove cookie because comments have refreshed
                    //cookie.remove(key);
//                    }
                }
            });
        };


//        toi_comments.prototype.saveRating = function (opinionid, typeid, rateid, val, ref) {
//            var rating = ["rate", opinionid, typeid, rateid];
//            cookie.set(rating.join(":"), val || "1", 1, document.location.pathname);
//        };
//
//        toi_comments.prototype.getSavedRating = function (opinionid, typeid, rateid, ref) {
//            var rating = ["rate", opinionid, typeid, rateid];
//            var commentCookie = cookie.get(rating.join(":"));
//            if (is.defined(commentCookie)) {
//                return commentCookie;
//            } else {
//                return null;
//            }
//        };

//        toi_comments.prototype.removeSavedRating = function (opinionid, typeid, rateid) {
//            var rating = ["rate", opinionid, typeid, rateid];
//            return cookie.remove(rating.join(":"), document.location.pathname);
//        };
//
//        toi_comments.prototype.loadSavedRating = function (opinionid, typeid, rateid) {
//            var tthis = this;
//            var user = login.getUser();
//            var savedRating = tthis.getSavedRating(opinionid, typeid, rateid);
//            if (savedRating && user) {
//                logger.log("Has saved rating and user is logged in.");
//                logger.log(savedRating);
//
//                tthis.rate(savedRating.opinionid, savedRating.typeid, savedRating.rateid, function (savedComment) {
//                });
//            }
//        };

        times_comments.prototype.validateRating = function (opinionid, typeid, rateid, ref, callback) {
            logger.log("Validate Rating: " + opinionid);
            var config = this.config;
            var user = login.getUser();
            var savedRating = this.getSavedRatingValidation(opinionid, typeid, rateid, ref);
            if (ref) {
                if (user && ref.parent && ref.parent.attr("data-userid") == user.getId()) { //Own comment
                    switch (typeid) {
                        case CONSTANT.RATE_TYPE.AGREE:
                            callback(config.messages.self_agree);
                            break;
                        case CONSTANT.RATE_TYPE.DISAGREE:
                            callback(config.messages.self_disagree);
                            break;
                        case CONSTANT.RATE_TYPE.RECOMMEND:
                            callback(config.messages.self_recommend);
                            break;
                        case CONSTANT.RATE_TYPE.OFFENSIVE:
                            callback(config.messages.self_offensive);
                            break;
                    }
                } else if (savedRating && savedRating > 0) {
                    switch (typeid) {
                        case CONSTANT.RATE_TYPE.AGREE:
                            callback(config.messages.already_agree);
                            break;
                        case CONSTANT.RATE_TYPE.DISAGREE:
                            callback(config.messages.already_disagree);
                            break;
                        case CONSTANT.RATE_TYPE.RECOMMEND:
                            callback(config.messages.already_recommended);
                            break;
                        case CONSTANT.RATE_TYPE.OFFENSIVE:
                            callback(config.messages.already_offensive);
                            break;
                    }
                } else if (this.getSavedRatingValidation(opinionid, CONSTANT.RATE_TYPE.AGREE, rateid, ref) && typeid == CONSTANT.RATE_TYPE.DISAGREE) {
                    callback(config.messages.cant_agree_disagree);
                } else if (this.getSavedRatingValidation(opinionid, CONSTANT.RATE_TYPE.DISAGREE, rateid, ref) && typeid == CONSTANT.RATE_TYPE.AGREE) {
                    callback(config.messages.cant_agree_disagree);
                } else if (this.getSavedRatingValidation(opinionid, CONSTANT.RATE_TYPE.AGREE, rateid, ref) && typeid == CONSTANT.RATE_TYPE.OFFENSIVE) {
                    callback(config.messages.cant_agree_offensive);
                } else if (this.getSavedRatingValidation(opinionid, CONSTANT.RATE_TYPE.OFFENSIVE, rateid, ref) && typeid == CONSTANT.RATE_TYPE.AGREE) {
                    callback(config.messages.cant_agree_offensive);
                } else if (this.getSavedRatingValidation(opinionid, CONSTANT.RATE_TYPE.DISAGREE, rateid, ref) && typeid == CONSTANT.RATE_TYPE.RECOMMEND) {
                    callback(config.messages.cant_disagree_recommend);
                } else if (this.getSavedRatingValidation(opinionid, CONSTANT.RATE_TYPE.RECOMMEND, rateid, ref) && typeid == CONSTANT.RATE_TYPE.DISAGREE) {
                    callback(config.messages.cant_disagree_recommend);
                } else if (this.getSavedRatingValidation(opinionid, CONSTANT.RATE_TYPE.OFFENSIVE, rateid, ref) && typeid == CONSTANT.RATE_TYPE.RECOMMEND) {
                    callback(config.messages.cant_recommend_offensive);
                } else if (this.getSavedRatingValidation(opinionid, CONSTANT.RATE_TYPE.RECOMMEND, rateid, ref) && typeid == CONSTANT.RATE_TYPE.OFFENSIVE) {
                    callback(config.messages.cant_recommend_offensive);
                } else {
                    callback();
                }
            } else {
                logger.error("Cannot validate rating. 'ref' is undefined.");
            }
        };

        times_comments.prototype.validateComment = function (commentObj, callback, ref) {
            event.publish("comment.post.validating");
            var comment = commentObj.comment;
            var config = this.config;

            logger.info("Validating Comment: " + comment);
            commentObj.urs = (commentObj.urs === '') ? undefined : commentObj.urs;
            if (is.empty(comment)) {
                callback(config.messages.blank);
            } else if (is.url(comment)) {
                callback(config.messages.has_url);
            } else if (comment.length < config.validation.minlength) {
                callback(config.messages.minlength);
            } else if (is.defined(this.config.rating) && this.config.rating && (!is.defined(commentObj.urs))) {
                callback(config.messages.ratemendate);
            } else {
                var msgObj = this.commentObjToMsgObj(commentObj);
//                var validate_url = config.validate_url;
                msgObj.message = msgObj.message.toLowerCase();
                api.validateComment(msgObj, function (response) {
                    if (response === "duplicatecontent") {
                        event.publish("comment.post.validation.failed", response);
                        callback(config.messages.duplicate);
                    } else if (response === "abusivecontent") {
                        event.publish("comment.post.validation.failed", response);
                        callback(config.messages.abusive);
                    } else {
                        event.publish("comment.post.validated");
                        callback();
                    }
                });

//                ajax.post(validate_url, msgObj, function (response) {
//                    if (response === "duplicatecontent") {
//                        event.publish("comment.post.validation.failed",response);
//                        callback(config.messages.duplicate);
//                    } else if (response === "abusivecontent") {
//                        event.publish("comment.post.validation.failed",response);
//                        callback(config.messages.abusive);
//                    } else {
//                        event.publish("comment.post.validated");
//                        callback();
//                    }
//                }).error(function(event,xhr, e){
//                    event.publish("comment.post.error",e);
//                });
            }
        };

        times_comments.prototype.add = function (msg, parentId, rootId, level, callback, cnsl, currentEle) {
            logger.info("Adding Comment: " + msg);
            var tthis = this;
            $(tthis.config.wrapper + " [data-plugin='comment-verified-msg']").hide(); //todo move to reset function

            var config = this.getConfig();
            var user = login.getUser();

            var ref = ui.getActionReferences(currentEle, tthis.config.comment);//todo optimize

            /*if($("[data-plugin='comment-rating']").length > 0){
             var rating_input = $("input[data-plugin='comment-rating']").val().trim();
             if ($("input[data-plugin='comment-rating']:first").val().trim() == "Rate this movie") {
             this.error("Please rate before comment", ref["comment-error"]);
             return;
             }
             }*/

            var commentObj = {
                index: -1,
                id: -1,
                comment: msg.trim(),
                level: level || 1,
                parentId: parentId || 0,
                rootId: rootId || 0,
                parentuid: ref.parent ? ref.parent.attr("data-id") : null,
                parentusername: ref.parent ? ref.parent.attr("data-user") : null,
                type: currentEle ? $(currentEle).attr("data-plugin") : "comment-post",
                time: new Date().getTime(), //todo user server time,
                configid: "41083278",
                url: window.location.href.split("?")[0].split("#")[0],
                rotype: typeof tthis.config.rotype == 'undefined' ? 0 : tthis.config.rotype, // for article by default
                urs: typeof userInfo === 'object' ? userInfo.userRating : ''

            };
            /*if(typeof userInfo === 'object'){
             commentObj.urs = userInfo.userRating;
             }*/
            var facebookCheckbox = ref["comment-facebook"];
            var twitterCheckbox = ref["comment-twitter"];

            commentObj.social = [];
            if (facebookCheckbox && facebookCheckbox.is(':checked')) {
                commentObj.social.push("facebook");
            }

            if (twitterCheckbox && twitterCheckbox.is(':checked')) {
                commentObj.social.push("twitter");
            }


            if (!user) {
                var cmtUser = (commentObj.type == "comment-post" ? this.getCommentUser(ref) : null);
                if (is.object(cmtUser)) {
                    user = cmtUser;
                } else if (cmtUser) {
                    return;
                }
            }


            if (user) {


                tthis.attachUserToComment(commentObj, user);

                if (cnsl === true) {
//                    console.log(msgObj);
                    if (callback) {
                        callback(commentObj);
                    }
                } else {
                    tthis.post(commentObj, callback);
                }
                //todo add class .highlight
            } else {
//                this.validateComment(commentObj, function (error) {
//                    if (!is.empty(error)) {
//                        if (callback) {
//                            callback(commentObj, error);
//                        }
//                        return;
//                    }

                switch (commentObj.type) {
                    case "comment-facebook-post":
                        tthis.commentOnFacebook(commentObj, ref);
                        break;
                    case "comment-google-post":
                        tthis.commentOnGoogle(commentObj, ref);
                        break;
                    case "comment-twitter-post":
                        tthis.commentOnTwitter(commentObj, ref);
                        break;
                    case "comment-email-post":
                        tthis.commentUsingEmail(commentObj, ref);
                        break;
                    default:
                        tthis.commentWithoutLogin(commentObj, ref);
                        break;
                }

//                },ref);
            }


            /*
             //http://timesofindia.indiatimes.com/fbpostcomment.cms?article=http://timesofindia.indiatimes.com/city/delhi/Thieves-who-stole-judges-car-held/articleshow/26115345.cms&access_token=CAABrIHwVZA2UBACX0gAvHb8VBrOCmK6F3fmNZCSY8UL86A9wjUBwZAZChjmV3Np4VaBV41DYJNhagZA83pipQaacl66yYxNSzVsoZCi2CqWZCgfT22WfYHf9f3JzgYD11LxptjZCxZCjDdxdZA6K5BcLijMNDCv8JScHqmoMrOq9F6l5C2G5VDP3v2q699gbfwPtYo3YSGx8CLrwZDZ
             */
        };

        times_comments.prototype.getCommentUser = function (ref) {
            var config = this.config;

            var $get_user_name = ref['get-user-name'];
            var $get_user_email = ref['get-user-email'];
            var $get_user_location = ref['get-user-location'];
            var $set_user_captcha = ref['set-user-captcha'];
            var $get_user_captcha = ref['get-user-captcha'];

            if ($get_user_name && $get_user_email && $get_user_name.length > 0 && $get_user_email.length > 0) {
                var user = {};

                user.name = $get_user_name.val().trim();
                user.email = $get_user_email.val().trim();
                if ($get_user_location && $get_user_location.length > 0) {
                    user.location = $get_user_location.val().trim();
                }

                if (user.name.length == 0 || user.name == $get_user_name.attr("placeholder")) {
                    this.error(config.messages.name_required, ref["comment-error"]);
                    return true;
                }

                if (user.name.length > 30) {
                    this.error(config.messages.name_toolong, ref["comment-error"]);
                    return true;
                }

                if (!is.alphaOnly(user.name)) {
                    this.error(config.messages.name_not_string, ref["comment-error"]);
                    return true;
                }


                if (user.email.trim().length == 0 || user.email == $get_user_email.attr("placeholder")) {
                    this.error(config.messages.email_required, ref["comment-error"]);
                    return true;
                }

                if (!is.email(user.email.trim())) {
                    this.error(config.messages.email_invalid, ref["comment-error"]);
                    return true;
                }

                if ($get_user_location && $get_user_location.length > 0) {
                    if (user.location.length == 0 || user.location == $get_user_location.attr("placeholder")) {
                        this.error(config.messages.location_required, ref["comment-error"]);
                        return true;
                    }

                    if (user.location.length > 30) {
                        this.error(config.messages.location_toolong, ref["comment-error"]);
                        return true;
                    }

                    if (!is.alphaOnly(user.location)) {
                        this.error(config.messages.location_not_string, ref["comment-error"]);
                        return true;
                    }
                }

                if ($get_user_captcha && $get_user_captcha.val().trim().length == 0) {
                    this.error(config.messages.captcha_required, ref["comment-error"]);
                    return true;
                }

                if ($get_user_captcha && $get_user_captcha.val().trim().length > 4) {
                    this.error(config.messages.captcha_toolong, ref["comment-error"]);
                    return true;
                }

                if ($get_user_captcha && !is.numberOnly($get_user_captcha.val().trim())) {
                    this.error(config.messages.captcha_number_only, ref["comment-error"]);
                    return true;
                }

                if ($set_user_captcha && $get_user_captcha && !eval($set_user_captcha.text() + "=" + $get_user_captcha.val())) {
                    this.error(config.messages.captcha_invalid, ref["comment-error"]);
                    return true;
                }


                return userClass.getNewUser(user, {
                    "id": "id",
                    "username": "username",
                    "thumb": "thumb",
                    "email": "email",
                    "name": "name",
                    "fullName": "name",
                    "CITY": "location"
                });

//                app: "toiipad",
//                    useripaddress: "202.134.162.148",
//                    location: "India",
//                    fromname: "Lawrance",
//                    fromaddress: "darrylthebest@gmail.com",
//                    rotype: 0

            }
        };
        times_comments.prototype.commentOnFacebook = function (commentObj, ref) {
            var tthis = this;
            var domainOnly = util.getDomainOnly();
//            commentObj.social.push("facebook");
            tthis.saveComment(commentObj);
            cookie.set("clickkepfbtart", "1", 1, "/", domainOnly);
            cookie.set("clickkepfbtart" + window.msid, window.msid, 1, "/", domainOnly);
            login.loginWithFacebook(function () {
                tthis.loadSavedComment();
            });
        };

        times_comments.prototype.commentOnGoogle = function (commentObj, ref) {
            var tthis = this;
//            commentObj.social.push("google");
            tthis.saveComment(commentObj);
            login.loginWithGoogle(function () {
                tthis.loadSavedComment();
            });
        };

        times_comments.prototype.commentOnTwitter = function (commentObj, ref) {
            var tthis = this;
            var domainOnly = util.getDomainOnly();
//            commentObj.social.push("twitter");
            tthis.saveComment(commentObj);
            cookie.set("clickkeptwtart", "1", 1, "/", domainOnly);
            cookie.set("clickkeptwtart" + window.msid, window.msid, 1, "/", domainOnly);
            login.loginWithTwitter(function () {
                tthis.loadSavedComment();
            });
        };

        times_comments.prototype.commentUsingEmail = function (commentObj, ref) {
            var tthis = this;
            var domainOnly = util.getDomainOnly();
            tthis.saveComment(commentObj);
            cookie.set("clickkepssoart", "1", 1, "/", domainOnly);
            cookie.set("clickkepssoart" + window.msid, window.msid, 1, "/", domainOnly);
            login.login(function () {
                tthis.loadSavedComment();
            });
        };

//        toi_comments.prototype.commentWithoutLogin = function (commentObj, ref) {
//            if (window.resetRegisterForm1 && window.putMathQ && window.lightbox2n) {    //todo, this is toi specific and is bad code, couldn't help it
//                window.resetRegisterForm1();
//                $("#registerForm1 #comments").val(commentObj.comment);
//                $("#registerForm1 #parentid").val(commentObj.parentId);
//                $("#registerForm1 #rootid").val(commentObj.rootId);
//
//                window.putMathQ(4);
//                window.lightbox2n();
//                window.scrollTo(0, 0);
//
//                var input = ref["comment-input"];
//                if (input) {
//                    input.val("");
//                    input.keyup();
////                    input.focus();
//                }
//            } else {
//                logger.error("'window.resetRegisterForm1 or window.putMathQ or window.lightbox2n' is not defined.");
//            }
//            this.loadSavedComment();
//        };


        times_comments.prototype.commentWithoutLogin = function (commentObj, ref) {
            var tthis = this;
            var domainOnly = util.getDomainOnly();

            tthis.saveComment(commentObj);
            cookie.set("clickkepssoart", "1", 1, "/", domainOnly);
            cookie.set("clickkepssoart" + window.msid, window.msid, 1, "/", domainOnly);
            login.login(function () {
                if (!(tthis.config.rating)) {
                    tthis.loadSavedComment();
                }
            });
        };
        /**
         * Verify comment posted using email
         *
         * @param commentObj
         * @param callback
         */
        times_comments.prototype.verify = function (callback, ref) {
            //http://timesofindia.indiatimes.com/cmtverified/29255491.cms?cmtid=23834874&r=1393492740078
            var tthis = this;
            var url = tthis.config.verify_comment_url;

            var msgObj = {
                msid: window.msid,
                cmtid: null,//ActionParams > messageid
                r: null//ActionParams > r
            };
            var url_params = util.getParam();
            if (window.messageid && window.param_r) {
                msgObj.cmtid = window.messageid;
                msgObj.r = window.param_r;
            }
            else {
                msgObj.cmtid = url_params.messageid;
                msgObj.r = url_params.r;
            }

            if (msgObj.msid && msgObj.cmtid && msgObj.r) {
                logger.info("Verifying Comment.");

                ajax.get(url, msgObj, function (response) {
                    if (response.cmtverified.commentbyid && response.cmtverified.commentbyid.roaltdetails) {
                        var cmt = response.cmtverified.commentbyid;
                        cmt.roaltdetails = cmt.roaltdetails.roaltdetails;

                        var commentObj = {
                            index: -1,
                            id: -1,
                            comment: is.string(cmt.message) ? $("<div/>").text(cmt.message).html() : "",
                            level: 1,
                            parentuid: cmt.parentuid,
                            parentusername: cmt.parentusername,
                            abstime: msgObj.r ? util.getDate(msgObj.r).getTime() : "",//1384934349412,    //cmt.rodate //19 Nov, 2013 01:04 PM
                            time: msgObj.r,
                            type: "comment-post",
                            user: {
                                name: cmt.roaltdetails.fromname,
                                location: cmt.roaltdetails.location
                            }
                        };


                        if (callback) {
                            callback(commentObj);
                        }
                    } else {
                        if (callback) {
                            callback(null, {});
                        }
                    }
                });
            }

            if (url_params.register === "1") {
                logger.info("Display register window.");

                if (!login.getUser()) {
                    login.login();
                }
            }
        };
        times_comments.prototype.post = function (commentObj, callback, ref) {
            var tthis = this;
//            var url = this.config.post_url;


//            if (!is.empty(commentObj.social)) {
//                if (commentObj.social.indexOf("facebook") > -1) {//todo use common function
//                    tthis.postToFacebook(commentObj.comment, function () {
//                        if (commentObj.social.indexOf("twitter") > -1) {//open twitter dialog after fb dialog closes
//                            tthis.postToTwitter(commentObj.comment);
//                        }
//                    });
//                } else if (commentObj.social.indexOf("twitter") > -1) {
//                    tthis.postToTwitter(commentObj.comment);
//                } else if (commentObj.social.indexOf("google") > -1) {
//                    tthis.postToGoogle(commentObj.comment);
//                }
//            }


            /*if (!is.empty(commentObj.social)) {
             if(is.defined(tthis.config.rating) && tthis.config.rating){
             if( is.defined(tthis.config.moviename) && tthis.config.moviename){
             // for movie review
             var socialtext = "Movie: "+tthis.config.moviename+", My Rating "+(commentObj.urs/2)+"/5. "+commentObj.comment;
             tthis.postToSocial(socialtext,commentObj.social.join(","));
             }
             }
             else{
             tthis.postToSocial(commentObj.comment,commentObj.social.join(","));
             }
             }*/

            if (!is.empty(commentObj.social)) {
                if (is.defined(tthis.config.rating) && tthis.config.rating) {
                    if (is.defined(tthis.config.moviename) && tthis.config.moviename) {
                        // for movie review
                        var socialtext = "Movie: " + tthis.config.moviename + ", My Rating " + (commentObj.urs / 2) + "/5. " + commentObj.comment;
                        tthis.postToSocial(socialtext, commentObj.social.join(","));
                    }
                    else if (is.defined(tthis.config.techprdtname) && tthis.config.techprdtname) {
                        // for tech prdt review
                        var socialtext = tthis.config.techprdtname + " : " + "My Rating " + (commentObj.urs / 2) + "/5, " + commentObj.comment;
                        tthis.postToSocial(socialtext, commentObj.social.join(","));
                    }
                }
                else {
                    tthis.postToSocial(commentObj.comment, commentObj.social.join(","));
                }
            }


            //todo move to api
            tthis.validateComment(commentObj, function (error) {
                if (!is.empty(error)) {
                    if (callback) {
                        callback(commentObj, error);
                    }
                    return;
                }
                event.publish("comment.post.posting");
                logger.info("Posting Comment:" + commentObj.comment);


                var msgObj = tthis.commentObjToMsgObj(commentObj);
                msgObj.pcode = page.getChannel();

                //todo implemente and check commented code.
                if (tthis.config.sendCommentLiveEmail === false) {
                    msgObj.verifyuser = 1; //comment is posted, email is not sent
                    api.postCommentWithoutVerification(msgObj, function (response) {
                        event.publish("comment.post.posted");
                        if (callback) {
                            callback(commentObj);
                        }
                    });
                } else {
                    if (tthis.config.enableparam && msgObj.rotype != 0) {
                        msgObj.url = msgObj.url + '?' + window.location.search.replace("?", "").split("&")[0] + '&';
                        msgObj.configid = '46516605'; // to handle param include
                    }
                    else if (msgObj.rotype != 0) {
                        msgObj.url = msgObj.url + '?';
                    }
                    api.postComment(msgObj, function (response) {
                        event.publish("comment.post.posted");
                        if (callback) {
                            callback(commentObj);
                        }
                    });
                }

//                ajax.post(url, msgObj, function (response) {
//                        event.publish("comment.post.posted");
//                        if (callback) {
//                            callback(commentObj);
//                        }
//                    })
//                    .error(function(event,xhr,e){
//                        event.publish("comment.post.error",e);
//                    });

            }, ref);
        };

        times_comments.prototype.commentObjToMsgObj = function (commentObj) {
            var config = this.config;//getConfig();

            var msgObj = {
                //                    hostid:83,//259:travel
                //                    rchid:-2128958273,//2147477992:travel
                fromname: commentObj.user ? commentObj.user.name : null,
                fromaddress: commentObj.user ? commentObj.user.email : null,
                userid: commentObj.user ? (commentObj.user.id ? commentObj.user.uid : "qrst") : "qrst", // todo use SSO
//                fbemailid: commentObj.user ? commentObj.user.email : null,//todo use fb email
                location: commentObj.user ? commentObj.user.location : null,
                imageurl: commentObj.user ? commentObj.user.image : null,
                loggedstatus: commentObj.user && commentObj.user.id ? 1 : 0,

                message: commentObj.comment,
                roaltdetails: 1,
                ArticleID: config.msid,
                msid: config.msid,
                parentid: commentObj.parentId,
                rootid: commentObj.rootId,
                url: commentObj.url,
                configid: commentObj.configid,
                urs: commentObj.urs ? commentObj.urs : null,
                rotype: commentObj.rotype
            };

            if (!commentObj.user.id) {
                msgObj.verifyuser = 1; //To prevent sending "You comment is live mail" in case of non-logged in user.
            }
            return msgObj;
        };

        times_comments.prototype.error = function (errMsg, wrapper) {
            if (is.visible(wrapper)) {
                wrapper.text(errMsg);
            } else if (!is.empty(errMsg)) {
                alert(errMsg);
            }
            event.publish("comment_error", errMsg);
        };

        times_comments.prototype.rateAgree = times_comments.prototype.rateUpVote = function (opinionid, callback, ref) {
//            login.loginType(15);
            logger.log("Agree Comment: " + opinionid);
            this.rate(opinionid, CONSTANT.RATE_TYPE.AGREE, 0, callback, ref);
//            login.loginType();
            //http://timesofindia.indiatimes.com/ratecomment_new.cms?opinionid=22554141&typeid=100&rateid=1
            //http://myt.indiatimes.com/mytimes/addActivity?activityType=Agreed&appKey=TOI&parentCommentId=22554141&baseEntityType=ARTICLE&objectType=A&url=
        };

        times_comments.prototype.rateDisagree = times_comments.prototype.rateDownVote = function (opinionid, callback, ref) {
//            login.loginType(16);
            logger.log("Disagree Comment: " + opinionid);
            this.rate(opinionid, CONSTANT.RATE_TYPE.DISAGREE, 0, callback, ref);
//            login.loginType();
            //http://timesofindia.indiatimes.com/ratecomment_new.cms?opinionid=22587427&typeid=101&rateid=1
            //http://myt.indiatimes.com/mytimes/addActivity?activityType=Disagreed&appKey=TOI&parentCommentId=22587427&baseEntityType=ARTICLE&objectType=A&url=
        };

        times_comments.prototype.rateRecommend = function (opinionid, callback, ref) {
//            login.loginType(17);
            logger.log("Recommend Comment: " + opinionid);
            this.rate(opinionid, CONSTANT.RATE_TYPE.RECOMMEND, 0, callback, ref);
//            login.loginType();
            //http://timesofindia.indiatimes.com/ratecomment_new.cms?opinionid=22587134&typeid=102&rateid=1
            //http://myt.indiatimes.com/mytimes/addActivity?activityType=Reccomended&appKey=TOI&parentCommentId=22587134&baseEntityType=ARTICLE&objectType=A&url=
        };

        times_comments.prototype.flag = times_comments.prototype.rateOffensive = function (opinionid, callback, ref, reason) {
            var tthis = this;
            logger.log("Flag/Offensive Comment: " + opinionid);

            $(ref.offensive_popup).off("click");
            $(ref.offensive_popup).on("click", function () {
                var ref = ui.getActionReferences(this, tthis.config.comment);
                var valReason = ref.offensive_popup.find("input:checked").val();
                if (valReason && valReason.length > 0 && valReason == "Others") {
                    ref.offensive_popup_reason.show();
                } else {
                    ref.offensive_popup_reason.hide();
                }

                tthis.error("", ref["comment-offensive-error"]);

//                return false;
            });
            event.publish("comment.flag.show", ref.offensive_popup);

            var elemTop = $(ref.offensive_popup).closest('.comment-box').position().top + $(ref.offensive_popup).closest('.comment-box').closest('.comment-box').height(),
                elemH = $(ref.offensive_popup).height(),
                wH = $(window).height(),
                commentScrollPos = 0
            // $('[data-plugin="offensive_popup"]').hide();
            $(ref.offensive_popup).removeClass('popup_above');

            if (((elemTop - commentScrollPos) + elemH) > wH) {
                $(ref.offensive_popup).addClass('popup_above');
            }
            ref.offensive_popup.show();
            return;
            /*
             Find this comment offensive?
             Choose your reason below and click on the Submit button. This will alert our moderators to take action
             Reason for reporting:
             Foul language
             Defamatory
             Inciting hatred against a certain community
             Out of context/Spam
             Others

             */

//            //todo remove dependency on articleshow, use common rating.
//            var tthis = this;
//            //todo code repeated in rating also
//            var rating = {opinionid: opinionid, typeid: CONSTANT.RATE_TYPE.OFFENSIVE, rateid: 0};
//
//            var user = login.getUser();
//
//            if (user) {
//                this.validateRating(opinionid, rating.typeid, rating.rateid, ref, function (error) {
//                    if (!is.empty(error)) {
//                        if (callback) {
//                            callback(error);
//                        }
//                        return;
//                    }
//                    tthis.rate(opinionid, CONSTANT.RATE_TYPE.OFFENSIVE, rating.rateid, callback, ref);
//                });
//            } else {
//                (function (rating, callback, ref) {
//                    login.login(function () {
//                        tthis.rateOffensive(rating.opinionid, callback, ref);
//                    }, true);
//                }(rating, callback, ref));
//            }

            //http://timesofindia.indiatimes.com/offensiveService/offence.asmx/getOffencivecomment?ofusername=delsanic&ofreason=Out%20of%20context/Spam&ofcommenteroid=22586038&ofcommenthostid=83&ofcommentchannelid=-2128958273&ofcommentid=29127171&ofuserisloggedin=1&ofuserssoid=delsanic@gmail.com&ofuseremail=delsanic@gmail.com
            //http://timesofindia.indiatimes.com/ratecomment_new.cms?opinionid=22586038&typeid=103&rateid=1
        };

        times_comments.prototype.showRateOffensiveBox = function (opinionid, callback, ref) {
//            ref.current.find('[data-plugin="offensive_popup"]').show();
//            ref.current.find('[data-plugin="offensive_popup_submit"]').show();
//            ref.current.find('[data-plugin="offensive_popup_close"]').show();

        };

        times_comments.prototype.rate = function (opinionid, typeid, rateid, callback, ref, reason) {
            logger.log("Rate Comment: " + opinionid + ":" + typeid);
            var tthis = this;

            var rating = {opinionid: opinionid, typeid: typeid, rateid: rateid || 0};

            if (reason) {
                rating.ofreason = reason;
            }

            var user = login.getUser();

            if (user) {
                logger.log("Rate Comment user is logged in");
                this.validateRating(opinionid, typeid, rateid, ref, function (error) {
                    if (!is.empty(error)) {
                        if (callback) {
                            callback(error);
                        }
                        return;
                    }
                    var val = parseInt(ref["comment-agree-count"].text(), 10);
                    tthis.saveRatingValidation(opinionid, typeid, rateid, val + 1, ref);
                    api.rateComment(rating, function (resp) {
                        if (callback) {
                            callback(null, resp);
                        }
                    }, user);
//                    ajax.get(tthis.config.rate_url, rating, function (resp) {
//                        if (callback) {
//                            callback(null, resp);
//                        }
//                    });
                });
            } else {
                logger.log("Rate Comment user is NOT logged in, login prompted.");
                (function (rating, callback, ref) {
                    if (tthis.config.loginRequiredForRating === true) {
                        login.login(function () {
                            logger.log("Rate Comment user logged in, continue rating.");
                            tthis.rate(rating.opinionid, rating.typeid, rating.rateid, callback, ref, rating.reason);
                        }, true);
                    } else {
                        logger.log("Rate Comment user NOT logged in, continue rating.");
                        tthis.validateRating(rating.opinionid, rating.typeid, rating.rateid, ref, function (error) {
                            if (!is.empty(error)) {
                                if (callback) {
                                    callback(error);
                                }
                                return;
                            }
                            var val = parseInt(ref["comment-agree-count"].text(), 10);
                            tthis.saveRatingValidation(rating.opinionid, rating.typeid, rating.rateid, val + 1, ref);
                            api.rateComment(rating, function (resp) {
                                if (callback) {
                                    callback(null, resp);
                                }
                            });

//                            ajax.get(tthis.config.rate_url, rating, function (resp) {
//                                if (callback) {
//                                    callback(null, resp);
//                                }
//                            });
                        });
                    }
                }(rating, callback, ref));
            }

        };

        times_comments.prototype.renderComment = function (commentObj, errMsg, verified) {
            var tthis = this;
            if (commentObj) {
                var ref = ui.getActionReferences(tthis.config.wrapper + " [data-id='" + commentObj.parentId + "']");

                var parent = ref["parent"];
                var input = ref["comment-input"];
                var facebookCheckbox = ref["comment-facebook"];
                var twitterCheckbox = ref["comment-twitter"];

                if (errMsg) {
                    event.publish("comment.post.error", errMsg);
                    this.error(errMsg, ref["comment-error"]);
                } else {
                    event.publish("comment.post.rendering");
                    commentObj.comment = $("<div/>").text(commentObj.comment).html();
                    commentObj.user.rate = commentObj.urs;
                    if ($(tthis.config.wrapper + " .comment-verification-msg").length) {
                        $(tthis.config.wrapper + " .comment-verification-msg").remove();
                    }
                    if (this.isReply(commentObj)) {
                        var reply = ref["comment-reply"];
                        //Convert to number if level is not a number
                        if (!is.number(commentObj.level)) {
                            commentObj.level = parseInt(commentObj.level, 10);
                        }
                        ++commentObj.level; //TODO check if level needs to be increased.

                        if ((commentObj && commentObj.user && commentObj.user.id) || !ref['set-user-captcha']) {
                            parent.after(this.renderEach(-1, commentObj, true));
                            reply.hide();
                        } else {
                            parent.after("<div class='comment-box highlight comment-verification-msg'>" + $(tthis.config.wrapper + " [data-plugin='comment-verification-msg']").html() + "</div>");
                            reply.hide();
                        }
                    } else {
                        if ((commentObj && commentObj.user && commentObj.user.id) || verified == true || !ref['set-user-captcha']) {
                            $(tthis.config.wrapper + ' ' + this.config.main).prepend(this.renderEach(-1, commentObj, true));
                        } else {
                            $(tthis.config.wrapper + ' ' + this.config.main).prepend("<div class='comment-box highlight comment-verification-msg'>" + $(tthis.config.wrapper + " [data-plugin='comment-verification-msg']").html() + "</div>");
                        }
                    }

                    if (input) {
                        input.val("");
                        input.keyup();
                        input.focus();
                        if (facebookCheckbox) {
                            facebookCheckbox.prop('checked', false);
                        }
                        if (twitterCheckbox) {
                            twitterCheckbox.prop('checked', false);
                        }

                        var $get_user_captcha = ref['get-user-captcha'];
                        var $set_user_captcha = ref['set-user-captcha'];
                        if ($get_user_captcha) {
                            $get_user_captcha.val("");
                        }
                        if ($set_user_captcha) {
                            $set_user_captcha.text(Math.floor((Math.random() * 10)) + "+" + Math.floor((Math.random() * 10)) + "=");
                        }
                    }
                    this.error("", ref["comment-error"]);
                }
                //
                lazy.load();
            } else {
                this.error(errMsg);
            }

        };

        times_comments.prototype.renderUsingTemplate = function (index, dataOne, tmpl, prepend) {
            return $("#" + tmpl).render(dataOne, {
                formatNumber: function (val) {
                    return util.formatNumber(val);
                },
                timeToDate: function (time) {
                    return new Date(time);
                }, parseDate: function (time) {
                    if (jsonDate != null) {
                        var date = new Date(time);
                        var newDate = $.fullCalendar.formatDate(date, "MMM dd, yyyy");
                        return newDate;
                    }
                }
            });
        };

        times_comments.prototype.isReply = function (cmt) {
            return cmt.parentuid && cmt.parentusername;
        };

//        toi_comments.prototype.parseTmpl = function (data) {
//            return $("#tmpl1").render(data, { formatNumber: function (val) {
//                return util.formatNumber(val);
//            }});
//        };

        times_comments.prototype.loadingDiv = function (text) {
            var tthis = this;
            if (text === "Loading...") {
                $(tthis.config.wrapper + " [data-plugin='comment-loading']").show();
            } else {
                $(tthis.config.wrapper + " [data-plugin='comment-loading']").hide();
            }
        };

        return times_comments;
    });

define('comments',[
        "tiljs/apps/times/comments",
        "tiljs/apps/times/usermanagement",
        "tiljs/logger",
        "tiljs/event",
        "jquery",
        "tiljs/util",
        "module",
        "tiljs/cookie",
        "tiljs/ui",
        "jsrender"
    ],
    function (comments, login, logger, event, $, util, module, cookie, ui, jsrender) {
        var mod_comments = comments;
        var default_config = {
            validation: {
                minlength: 1 //140
            },
            messages: {
                "name_required": "Please enter your name.",
                "name_toolong": "Name cannot be longer than 30 chars.",
                "email_required": "Please enter your email address.",
                "email_invalid": "Please enter a valid email address.",
                "minlength": "Whoops! Your comment is too short. Please write a comment which is at least 140 characters long so that it is helpful for others as well :)",
                "maxlength": "You have entered more than 3000 characters.",
                "blank": "You can't post this comment as it is blank.",
                "popup_blocked": "Popup is blocked.",
                "has_url": "You can't post this comment as it contains URL.",
                "duplicate": "You can't post this comment as it is identical to the previous one.",
                "abusive": "You can't post this comment as it contains inappropriate content.",
                "self_agree": "You can't Agree with your own comment",
                "self_disagree": "You can't Disagree with your own comment",
                "self_recommend": "You can't Recommend your own comment",
                "self_offensive": "You can't mark your own comment as Offensive",
                "already_agree": "You have already Agreed with this comment",
                "already_disagree": "You have already Disagreed with this comment",
                "already_recommended": "You have already Recommended this comment",
                "already_offensive": "You have already marked this comment Offensive",
                "cant_agree_disagree": "You can't Agree and Disagree with the same comment",
                "cant_agree_offensive": "You can't Agree and mark the same comment Offensive",
                "cant_disagree_recommend": "You can't Disagree and Recommend the same comment",
                "cant_recommend_offensive": "You can't Recommend and mark the same comment Offensive",
                "permission_facebook": "You can't post to facebook. Post permission is required."
            }
        };
        mod_comments.config = util.extend(true, {}, comments.config, default_config, module.config());
        return mod_comments;
    });

define( 'homepage',[ "tiljs/cookie", "tiljs/load" ], function ( cookie, load ) {
	var mod_homepage = {};
	mod_homepage.init = function () {
		//--GA code--
		//        var _gaq = _gaq || [];
		//        _gaq.push(['_setAccount', 'UA-198011-4']);
		//        _gaq.push(['_setDomainName', 'none']);
		//        _gaq.push(['_setAllowLinker', true]);
		//        _gaq.push(['_addIgnoredOrganic', 'times of india']);
		//        _gaq.push(['_addIgnoredOrganic', 'toi']);
		//        _gaq.push(['_addIgnoredOrganic', 'the times of india']);
		//        _gaq.push(['_addIgnoredOrganic', 'timesofindia']);
		//        _gaq.push(['_addIgnoredOrganic', 'www.timesofindia.com']);
		//        _gaq.push(['_addIgnoredOrganic', 'timesofindia.com']);
		//        _gaq.push(['_addIgnoredOrganic', 'thetimesofindia']);
		//        _gaq.push(['_addIgnoredOrganic', 'time of india']);
		//        _gaq.push(['_addIgnoredOrganic', 'times of india headlines']);
		//        _gaq.push(['_addIgnoredOrganic', 'timesofindia.indiatimes.com']);
		//
		//        _gaq.push(['_trackPageview']);
		//        _gaq.push(['_trackPageLoadTime']);
		//        (function () {
		//            var ga = document.createElement('script');
		//            ga.type = 'text/javascript';
		//            ga.async = true;
		//            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		//            var s = document.getElementsByTagName('script')[0];
		//            s.parentNode.insertBefore(ga, s);
		//        })();
		//        //--GA code end--
		//
		//        //-- Begin comScore Tag --
		//        var _comscore = _comscore || [];
		//        _comscore.push({ c1: "2", c2: "6036484" });
		//        (function () {
		//            var s = document.createElement("script"), el = document.getElementsByTagName("script")[0];
		//            s.async = true;
		//            s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";
		//            el.parentNode.insertBefore(s, el);
		//        })();
		//        //-- End comScore Tag --
		//
		//        //--Visual Revenue Reader Response Tracking Script (v6) --
		//        var page = (typeof homeObj != "undefined") ? true : false;
		//        var _vrq = _vrq || [];
		//        _vrq.push(['id', 77]);
		//        _vrq.push(['automate', page]);
		//        _vrq.push(['track', function () {
		//        }]);
		//        (function (d, a) {
		//            var s = d.createElement(a),
		//                x = d.getElementsByTagName(a)[0];
		//            s.async = true;
		//            s.src = 'http://a.visualrevenue.com/vrs.js';
		//            x.parentNode.insertBefore(s, x);
		//        })(document, 'script');
		//
		//        //-- End of VR RR Tracking Script - All rights reserved --
		//
		//        //-- BEGIN EFFECTIVE MEASURE CODE --
		//        (function () {
		//            var em = document.createElement('script');
		//            em.type = 'text/javascript';
		//            em.async = true;
		//            em.src = ('https:' == document.location.protocol ? 'https://in-ssl' : 'http://in-cdn') + '.effectivemeasure.net/em.js';
		//            var s = document.getElementsByTagName('script')[0];
		//            s.parentNode.insertBefore(em, s);
		//        })();
		//        //--END EFFECTIVE MEASURE CODE --
		//
		//        //********************************************
		//        //************** SET GeoLocation *************
		//        //********************************************
		//        //var gloc = times.cookie.get('geolocation')||"";
		//        if (!(cookie.get('geolocation')) || cookie.get('geolocation') == "undefined") {
		//            cookie.remove('geolocation', '/', location.host);
		//            cookie.remove('geolocation', '/', '.indiatimes.com');
		//            var ord = window.ord || Math.floor(Math.random() * 1e16);
		//            load.js("http://ad.doubleclick.net/N7176/adj/TOI_Test/TOI_Test_1x1;sz=1x1;ord=" + ord + "?");
		//            setTimeout(function () {
		//                var gloc = cookie.get('geolocation');
		//                if (gloc) {
		//                    cookie.remove('geolocation', '/', '.indiatimes.com');
		//                    cookie.set("geolocation", gloc, 365, "/", location.host, "");
		//                }
		//            }, 5000);
		//        }
		//        //************* END GeoLocation *************
	};
	return mod_homepage;
} );

define('crwdcnctrl',['tiljs/cookie', 'tiljs/event'], function (cookie, event) {
    var mod_lotame = {};

    var getAgeFromDOB = function (dob) {
        var today = new Date(), birthDate = new Date(dob), age = today.getFullYear() - birthDate.getFullYear();
        var m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }
        return age;
    };

    var userDetailsFromCookie = function () {
        var userdetails = cookie.get("MSCSAuthDetail");
        var dtls = {};
        dtls.act = (typeof userdetails != 'undefined') ? "TOI" : "";
        if (typeof userdetails != 'undefined') {
            dtls.dob = (userdetails.match("DOB=(.*) ") != null) ? userdetails.match("DOB=(.*) ")[1] : "";
            dtls.gender = (userdetails.match("Gender=(.*)~Country") != null) ? userdetails.match("Gender=(.*)~Country")[1] : "";
        }
        dtls.age = (dtls.dob != "") ? getAgeFromDOB(dtls.dob) : "";
        return dtls;
    };

    mod_lotame.init = function () {
        var userDetails = userDetailsFromCookie();

        if (userDetails.gender != "" || userDetails.age != "") {
            if (typeof _cc2801 != 'undefined') {
                _cc2801.add("act", userDetails.act);
                _cc2801.add("dem", "toi.web:gender=" + userDetails.gender);
                _cc2801.add("dem", "toi.web:age=" + userDetails.age);
                if (typeof _cc2801.bcp != 'undefined') {
                    _cc2801.bcp();
                }
            }
        }
    };

    mod_lotame.init();

    event.subscribe(["user.login"], function (user) {
        if (typeof mod_lotame.init == 'function') {
            mod_lotame.init();
        }
    });
});
define('tiljs/social/twitter',["../social/social", "../ui", "../logger", "../event", "../util", "jquery", "module"], function (social, ui, logger, event, util, $, module) {
    var mod_twitter = new social("twitter", "twitter");
    var default_config = {
        parse: false,
        js: "//platform.twitter.com/widgets.js",
        js_id: "twitter-wjs",
        share_url: "https://twitter.com/share",
        via: null,
        format: "{{:title}}",
        sharePageTitle: true
    };
    var config = util.extend(true, {}, default_config, module.config());
    mod_twitter.setJS(config.js, config.js_id);
    mod_twitter.addPlugin({
        id: "button",
        js: true,
        init: function (ele, plugin) {
            var element = $(ele);
            var data = util.data(ele);
            var default_params = {
                //                size:"large", //
                count: "vertical", // horizontal, vertical, none
                url: location.href
            };
            var params = util.extend(true, {}, default_params, data);
            params.url = data.url || data.href || element.attr("href") || params.href;
            var tweet_button = $("<a class='twitter-share-button'></a>");
            //            tweet_button.attr("href",params.url);
            //Using this because data method in jquery does not append dom element
            $.each(params, function (name, value) {
                tweet_button.attr("data-" + name, value);
            });
            element.empty();
            element.append(tweet_button);
            mod_twitter.parse(ele);
        }
    });

    mod_twitter.addPlugin({
        id: "frame",
        js: true,
        init: function (ele, plugin) {

            var element = $(ele);
            var data = util.data(ele);
            var default_params = {
                twtid: "",
                theme: 'light'
            };
            var params = util.extend(true, {}, default_params, data);

            var divid = "twt-" + params.twtid;
            if (window.twttr) {
                twttr.widgets.createTweet(
                    params.twtid,
                    document.getElementById(divid),
                    {
                        theme: params.theme
                    }
                );
            }
        }
    });

    mod_twitter.addPlugin({
        id: "share",
        alias: ["tweet"],
        events: {
            click: function (e) {
                var data = util.data(this);
                mod_twitter.tweet(data.url || $(this).attr("href"), util.format(data.format || config.format, data), data.via || config.via, this);
            }
        },
        init: function (ele, plugin) {
            //            console.log(plugin.id + " init called");
            //            $(ele).click(function (e) {
            //                mod_twitter.tweet($(this).attr("data-url") || $(this).attr("href"));
            //                e.preventDefault();
            //            })
        }
    });
    mod_twitter.tweet = mod_twitter.share = function (url, text, via, element) {
        console.log(url+"||"+text,'IKHWAN');
        text = ( typeof text == "string" ) ? text.trim() : text;
        var tweetObj = {
            url: mod_twitter.getAbsoluteUrl(url),
            text: text || ( config.sharePageTitle === true ? document.title.split("|")[0] : "" )
        };
        if (via) {
            tweetObj.via = via;
        }
        mod_twitter._share(config.share_url, tweetObj, {
            name: 'twitter_tweet_dialog',
            width: 500,
            height: 400
        }, element);
    };
    mod_twitter.addPlugin({
        id: "follow",
        events: {
            click: function (e) {
                mod_twitter.follow($(this).attr("data-url") || $(this).attr("href"));
            }
        },
        init: function (ele, plugin) {
        }
    });
    mod_twitter.follow = function (url, options) {
        var _url = mod_twitter.getAbsoluteUrl(url);
        var win = window.open(url, "_blank");
        win.focus();
    };
    mod_twitter.addPlugin({
        id: "login",
        //        js: true,
        init: function (ele, plugin) {
            //            console.log(plugin.id + " init called");
            //            $(ele).click(function (e) {
            //                mod_twitter.login();
            //                e.preventDefault();
            //            })
        }
    });
    var __perms = null;
    mod_twitter.login = function (permissions, callback) {
    };
    mod_twitter.addPlugin({
        id: "logout",
        init: function (ele, plugin) {
            //            console.log(plugin.id + " init called");
            //            $(ele).click(function (e) {
            //                mod_twitter.logout();
            //                e.preventDefault();
            //            })
        }
    });
    mod_twitter.parse = function (ele) {
        if (typeof twttr !== "undefined") {
            twttr.widgets.load(ele);
        } else {
            logger.warn("'twttr' is required in parse");
        }
    };
    mod_twitter.logout = function (callback) {
    };
    mod_twitter.beforeinit = function () {
    };
    mod_twitter.beforeloadjs = function () {
    };
    mod_twitter.afterloadjs = function () {
        if (typeof twttr !== "undefined") {
            //                twttr.ready(function (twttr) {
            //                     attachtwitterevent();
            //                });
            logger.log("twttr js loaded");
            var event_names = ["click", "tweet", "retweet", "follow", "favorite"];
            util.each(event_names, function (k, event_name) {
                if (event_names.hasOwnProperty(event_name)) {
                    twttr.events.bind(event_name, function (event_name) {
                        return function (intent_event) {
                            event.publish("twttr." + event_name, {
                                intent_event: intent_event
                            });
                        };
                    }(event_name));
                }
            });
        } else {
            logger.warn("'twttr' is required in afterloadjs");
        }
    };
    var currentUser = null;
    mod_twitter.getUser = function (callback) {
        //        if (typeof FB !== "undefined" && !currentUser) {
        //            FB.api('/me', function (response) {
        //                if (callback) {
        //                    currentUser = response;
        //                    callback(response);  //response is the basic user object
        //                }
        //            });
        //        } else {
        //            if (callback) {
        //                callback(currentUser);
        //            }
        //        }
    };
    mod_twitter.onlogin = function (callback) {
        //        event.subscribe("FB.auth.authResponseChange.connected", function (resp) {
        //            mod_twitter.getUser(function (user) {
        ////                $("[data-plugin='" + opt.plugins.fb_login + "']").hide();
        ////                $("[data-plugin='" + opt.plugins.fb_logout + "']").show();
        //
        //                if (callback) {
        //                    callback(user, resp);
        //                }
        //            })
        //        });
    };
    mod_twitter.onlogout = function (callback) {
        //        event.subscribe(["FB.auth.authResponseChange.not_authorised", "FB.auth.authResponseChange.failed"], function (resp) {
        ////            $("[data-plugin='" + opt.plugins.fb_logout + "']").hide();
        ////            $("[data-plugin='" + opt.plugins.fb_login + "']").show();
        //
        //            if (callback) {
        //                currentUser = null;
        //                callback(resp);
        //            }
        //        });
    };
    
    $(document).ready(function(){
      mod_twitter.init(config);
    });
    return mod_twitter;
});

define('tiljs/social/pinterest',["../social/social", "../ui", "../logger", "../event", "../util", "../load", "jquery", "module"], function (social, ui, logger, event, util, load, $, module) {
    var mod_pinterest = new social("pinterest", "pinterest");
    var default_config = {
        parse: false,
        js: "//assets.pinterest.com/js/pinit.js",
        js_id: "pinterest-js",
        share_url: "//www.pinterest.com/pin/create/button/"
    };
    var config = util.extend(true, {}, default_config, module.config());
    mod_pinterest.setJS(config.js, config.js_id);
    mod_pinterest.addPlugin({
        id: "button",
        //        js:true,
        init: function (ele, plugin) {
            var element = $(ele);
            var data = util.data(ele);
            var default_params = {
                //                size:"large", //
                "pin-do": "buttonPin",
                "pin-config": "above" // horizontal, vertical, none
            };
            var params = util.extend(true, {}, default_params, data);
            params.href = data.url || data.href || element.attr("href") || params.href;
            var fb_like = $("<a></a>");
            var prm = {
                url: mod_pinterest.getAbsoluteUrl(params.href),
                media: mod_pinterest.getAbsoluteUrl(params.image),
                description: params.summary
            };
            fb_like.attr("href", "http://www.pinterest.com/pin/create/button/?" + $.param(prm));
            //Using this because data method in jquery does not append dom element
            $.each(params, function (name, value) {
                fb_like.attr("data-" + name, value);
            });
            element.empty();
            element.append(fb_like);
            mod_pinterest.parse(ele);
        }
    });
    mod_pinterest.addPlugin({
        id: "share",
        alias: ["pin"],
        events: {
            click: function (e) {
                var data = util.data(this);
                mod_pinterest.share(data.url || $(this).attr("href"), data.title, data.summary, data.image, this);
            }
        },
        //        js:true,
        init: function (ele, plugin) {
        }
    });
    mod_pinterest.pin = mod_pinterest.share = function (url, title, summary, image, element) {
        mod_pinterest._share(config.share_url, {
            url: mod_pinterest.getAbsoluteUrl(url),
            description: title,
            summary: summary,
            media: mod_pinterest.getAbsoluteUrl(image)
        }, {
            name: 'pinterest_pin_dialog',
            width: 700,
            height: 400
        }, element);
    };
    mod_pinterest.addPlugin({
        id: "follow",
        events: {
            click: function (e) {
                mod_pinterest.follow($(this).attr("data-url") || $(this).attr("href"));
            }
        },
        init: function (ele, plugin) {
        }
    });
    mod_pinterest.follow = function (url, options) {
        var _url = mod_pinterest.getAbsoluteUrl(url);
        var win = window.open(url, "_blank");
        win.focus();
    };
    mod_pinterest.parse = function (ele) {
        var pinJs = $('#' + config.js_id);
        pinJs.remove();
        load.js(config.js, null, config.js_id);
    };
    $(document).ready(function(){
      mod_pinterest.init(config);
    });
    return mod_pinterest;
});

define('tiljs/social/linkedin',["../social/social", "../ui", "../logger", "../event", "../util", "jquery", "module"], function (social, ui, logger, event, util, $, module) {
    var mod_linkedin = new social("linkedin", "linkedin");
    var default_config = {
        parse: false,
        js: "//platform.linkedin.com/in.js",
        js_id: "linkedin-js",
        //                share_url:"https://www.linkedin.com/cws/share"
        share_url: "http://www.linkedin.com/shareArticle"
    };
    var config = util.extend(true, {}, default_config, module.config());
    mod_linkedin.setJS(config.js, config.js_id);
    mod_linkedin.addPlugin({
        id: "button",
        js: true,
        init: function (ele, plugin) {
            //<script type="IN/Share" data-url="http://www.google.com" data-counter="top"></script>
            var element = $(ele);
            var data = util.data(ele);
            var default_params = {
                url: location.href,
                counter: "top" //top
            };
            var params = util.extend(true, {}, default_params, data);
            params.url = data.url || data.href || element.attr("href") || params.href;
            var fb_like = $("<script type='IN/Share'></script>");
            //Using this because data method in jquery does not append dom element
            $.each(params, function (name, value) {
                fb_like.attr("data-" + name, value);
            });
            element.empty();
            element.append(fb_like);
            mod_linkedin.parse(ele);
        }
    });
    mod_linkedin.addPlugin({
        id: "share",
        events: {
            click: function (e) {
                var data = util.data(this);
                mod_linkedin.share(data.url || $(this).attr("href"), data.title, data.summary, data.image, this);
            }
        },
        init: function (ele, plugin) {
        }
    });
    mod_linkedin.share = function (url, title, summary, image, element) {
        mod_linkedin._share(config.share_url, {
            url: mod_linkedin.getAbsoluteUrl(url),
            title: title,
            summary: summary
        }, {
            name: 'linkedin_share_dialog',
            width: 500,
            height: 570
        }, element);
        //        window.open("//linkedin.com/submit?url=" + _url + "&title=" + $("title").text(), "_blank");
    };
    mod_linkedin.parse = function (ele) {
        if (typeof IN !== "undefined") {
            IN.parse(ele);
        } else {
            logger.warn("'IN' is required in parse");
        }
    };
    $(document).ready(function(){
      mod_linkedin.init(config);
    });
    return mod_linkedin;
});

define('tiljs/social/googleplus',["../social/social", "../ui", "../logger", "module", "../event", "../util", "jquery"], function (social, ui, logger, module, event, util, $) {
    /*
     <body itemscope itemtype="http://schema.org/Product">
     <h1 itemprop="name">Shiny Trinket</h1>
     <img itemprop="image" src="{image-url}" />
     <p itemprop="description">Shiny trinkets are shiny.</p>
     </body>
     */
    var default_config = {
        parse: false,
        js: "https://apis.google.com/js/plusone.js?onload=attachgoogleevent",
        js_id: "googleplus-js",
        share_url: "https://plus.google.com/share"
    };
    var config = util.extend(true, {}, default_config, module.config());
    var mod_googleplus = new social("googleplus", "googleplus");
    mod_googleplus.setJS(config.js, config.js_id);
    mod_googleplus.addPlugin({
        id: "button",
        js: true,
        init: function (ele, plugin) {
            var element = $(ele);
            var data = util.data(ele);
            var default_params = {
                href: location.href,
                size: "tall", //standard , small, medium, tall
                annotation: "bubble", //bubble, inline, none
                width: null //integer
            };
            var params = util.extend(true, {}, default_params, data);
            params.href = data.url || data.href || element.attr("href") || params.href;
            var fb_like = $("<div class='g-plusone'></div>");
            //Using this because data method in jquery does not append dom element
            $.each(params, function (name, value) {
                fb_like.attr("data-" + name, value);
            });
            element.empty();
            element.append(fb_like);
            mod_googleplus.parse(element[0]);
        }
    });
    mod_googleplus.addPlugin({
        id: "share",
        events: {
            click: function (e) {
                var data = util.data(this);
                mod_googleplus.share(data.url || $(this).attr("href"), data.title, data.summary, data.image, this);
            }
        },
        config: {
            shareUrl: ""
        },
        init: function (ele, plugin) {
        }
    });
    mod_googleplus.share = function (url, title, summary, image, element) {
        mod_googleplus._share(config.share_url, {
            url: mod_googleplus.getAbsoluteUrl(url)
        }, {
            name: 'googleplus_share_dialog',
            width: 500,
            height: 400,
            scrollbars: 1
        }, element);
        //        ui.window("https://plus.google.com/share?url=" + encodeURIComponent(_url)
        //            , {name: 'googleplus_pin_dialog', width: 700, height: 440});
    };
    mod_googleplus.addPlugin({
        id: "follow",
        events: {
            click: function (e) {
                mod_googleplus.follow($(this).attr("data-url") || $(this).attr("href"));
            }
        },
        init: function (ele, plugin) {
        }
    });
    mod_googleplus.follow = function (url, options) {
        var _url = mod_googleplus.getAbsoluteUrl(url);
        var win = window.open(url, "_blank");
        win.focus();
    };
    mod_googleplus.parse = function (ele) {
        if (typeof gapi !== "undefined") {
            gapi.plusone.go();
            //            gapi.plusone.render();
        } else {
            logger.warn("'gapi' is required in parse");
        }
    };
    $(document).ready(function(){
      mod_googleplus.init(config);
    });
    return mod_googleplus;
});

/**
 var __times = __times || [];
 __times.push(['event','subscribe','times.load',function(){
	///this is called when toicommonjs is loaded

 }]);

 __times.push(function(){
	///this is called when toicommonjs is loaded

 });

 */
define('tiljs/plugin/pre_event',["../event", "module", "../util", "../is"], function (event, module, util, is) {
    var mod_pre_event = {};
    var default_config = {};
    var config = util.extend(true, {}, default_config, module.config());

    mod_pre_event.init = function () {
        if (window.__times) {
            mod_pre_event.process(window.__times);
        } else {
            window.__times = [];
        }

        window.__times.push = function (v) {
            mod_pre_event.processOne(v);
            return Array.prototype.push.apply(this, arguments);
        };
    };

    mod_pre_event.process = function (__times) {
        util.each(__times, function (k, v) {
            mod_pre_event.processOne(v);
        });
    };

    mod_pre_event.processOne = function (v) {
        try {
            if (is.funct(v)) {
                v();
            } else if (is.array(v)) {
                var mod = v[0],
                    method = v[1],
                    params = v.splice(2);

                require(mod)[method].apply(this, params);
            } else {
                event.publish("logger.warn", "Invali params in __times");
            }
        } catch (e) {
            event.publish("logger.error", e);
        }
    };

    mod_pre_event.init();


    return mod_pre_event;
});


var TimesApps = window.TimesApps || {};
TimesApps.getGACategory = function(){
    category = 'WEB-';
    
    if (typeof toiprops==='object' && toiprops._SCN_pg) {
        category +=  (toiprops._Tmpl_pg=='articlelist')?'CL':toiprops._Tmpl_pg;
        category += (toiprops.seoLocation && toiprops._Tmpl_pg && toiprops._Tmpl_pg.indexOf('show')===-1)?'/'+toiprops.seoLocation:'';
        category += (category.match(/CL$/))? document.location.pathname:'';
    }else{
        category += document.location.pathname;
    }
    
    return category;
}

define('pgtrack',[], function () {
    var mod_pgtrack = {};
    mod_pgtrack.evtCap = function (ee) {
        try {
            if (window.event && window.event.srcElement) {
                var level = 3, target = window.event.srcElement;
                while (level > 0 && target != document.querySelector('body')) {
                    if (target.getAttribute("pg") != null) {
                        mod_pgtrack.track(mod_pgtrack.addUrl(target));
                        break;
                    }
                    level--;
                    target = target.parentNode;
                }
            } else if (ee && ee.stopPropagation && !window.opera) {
                if (ee.target.getAttribute("pg") != null) {
                    mod_pgtrack.track(mod_pgtrack.addUrl(ee.target));
                }
            }
        } catch (ex) {
        }
    };

    mod_pgtrack.addUrl = function (target) {

        var pg = target.getAttribute("pg");

        if (pg && pg.indexOf('geturl') > -1) {

            if (target.nodeName == 'IMG' || target.nodeName == 'SPAN') {
                target = target.parentElement;
            }
            pg = pg.replace('geturl', target.getAttribute("href"));
        }
        if (pg && pg.indexOf('getmsid') > -1) {
            if (target.nodeName == 'IMG' || target.nodeName == 'SPAN') {
                target = target.parentElement;
            }
            pg = pg.replace('getmsid', target.getAttribute("href").match(/\d+(?=\D*$)/)[0]);
        }

        return pg;

    };

    mod_pgtrack.track = function (pgatt) {
        if (pgatt != null) {
            var pgat = pgatt.split('#');
            var trcode = null;
            if (pgatt != null) {
                var pgat = pgatt.split('#'),action,label;
                //************ ACTION ************//
                var si = pgatt.indexOf('~'), action;
                if (si > -1) {
                    action = pgatt.substr(si + 1);
                    pgatt = pgatt.substr(0, si);
                    pgat = pgatt.split('#');
                }else{
                    action = pgat[0]
                }
                
                //************ Label ************//
                if (pgat[1] ) label=pgat[1];
                var trcode = null;
                trcode = ga('send', 'event', TimesApps.getGACategory(), action, label);
            }
        }
    };
    $(document).click(mod_pgtrack.evtCap);
    return mod_pgtrack;
});
define('global',[], function () {
    var global = {};
    var mod_global = {};
    mod_global.global = global;
    mod_global.set = function (key, value) {
        global[key] = value;
    };
    mod_global.get = function (key) {
        return global[key];
    };
    return mod_global;
});

define("ajax", ["tiljs/ajax"], function (m) {
    return m;
});
define("event", ["tiljs/event"], function (m) {
    return m;
});
define("localstorage", ["tiljs/localstorage"], function (m) {
    return m;
});
define("preload", [],function(){});

define("util", ["tiljs/util"], function (m) {
    return m;
});
define("is", ["tiljs/is"], function (m) {
    return m;
});
define("ui", ["tiljs/ui"], function (m) {
    return m;
});
define("cookie", ["tiljs/cookie"], function (m) {
    return m;
});
define("event", ["tiljs/event"], function (m) {
    return m;
});
define("plugin/lazy", ["tiljs/plugin/lazy"], function (m) {
    return m;
});
define("load", ["tiljs/load"], function (m) {
    return m;
});
define("toicommonjs/rodate", ["rodate"], function (m) {
    return m;
});
define("logger", ["tiljs/logger"], function (m) {
    return m;
});
define("page", ["tiljs/page"], function (m) {
    return m;
});
define("user", ["tiljs/user"], function (m) {
    return m;
});
define("times/comments", ["comments"], function (m) {
    return m;
});
define("authorcomments", ["tiljs/apps/times/authorcomments"], function (m) {
    return m;
});
define('tiljs/apps/times/authorcomments',['jquery', 'event', 'plugin/lazy'], function ($, event, lazy) {
    var authorcomments = {};
    //authrcmnt.checkFlag = true;
    //authrcmnt._authorCommentIds = [];
    authorcomments.run = function () {
        event.subscribe("comments.loaded", function (c) {
            var root = $(c.config.main);
            if (!$('#' + $(root).attr('id') + ' [id="authComment"]').length) {
                //authrcmnt.checkFlag=false;
                var cmt = c.flagData ? c.flagData.authorsComment : null;
                if (cmt) {
                    var commentsResult = [];
                    for (var i = 0; i < cmt.length; i++) {
                        //authrcmnt._authorCommentIds.push(cmt[i].A_ID);
                        c.parseComment(cmt[i], commentsResult);
                    }
                    //authrcmnt._authorCommentIds = authrcmnt._authorCommentIds.filter(function(item,pos){ return authrcmnt._authorCommentIds.indexOf(item)==pos;});
                    //var root = $( c.config.main );
                    var str;
                    str = "<div id='authComment' style='border:1px solid #ccc;margin-bottom:10px;'><div class='authorTxt'>Author's Comment</div>";
                    for (var i = 0; i < commentsResult.length; i++) {
                        str = str + c.renderEach(i, commentsResult[i]);
                    }
                    str = str + '</div>';
                    root.prepend(str);
                    lazy.load();
                    $("#authComment .authorTxt:not(:first-child)").remove();
                }
            }
        });
    };
    return authorcomments;
});

define('nlwidget',[], function () {
    var nlwidget = {};
    nlwidget.run = function () {
        if ($('[data-id="newsletterWidgetDisplay"]').length) {

            $.ajax({
                url: '/newnlwidget.cms?ver=4',
                cache: true,
                dataType: "html",
                success: function (data) {
                    $('[data-id="newsletterWidgetDisplay"]').html(data);
                    var fileref = document.createElement('script');
                    fileref.setAttribute("type", "text/javascript");
                    fileref.setAttribute("src", "/newsletterwidget_js/version-25,minify-1.cms");
                    document.getElementsByTagName("head")[0].appendChild(fileref);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (window.console && console.log) {
                        console.log(errorThrown);
                    }
                }
            });
        }
    }
    return nlwidget;
});
define('tpwidget',["event", "tiljs/cookie"], function (event, cookie) {
    var tpwidget = {};
    // initialise timespoint-widget
    tpwidget.init = function () {

        var initCallback = function(){
            tpwidget.subscribeLoggedInStatus();
            tpwidget.bindEvents();
        }
        TimesApps.checkGdprAndCall(initCallback);
    },
        tpwidget.data = {


            'widgettypes': ['widget-head', 'widget-two'],
            'widget-head': 'tpwidget-one',
            'widget-two': 'tpwidget-two',
            'widgetsArray': [],
            'isUserLoggedIn': false,
            'isInitialised': false,
            'userInfo': [],
            'uemail': '',
            'url': escape(document.location.href),
            'pcode': 'TOI',
            'scode': 'News',
            'oid': '',
            'uid': '',
            'aname': '',
            'txnId': '',
            'firsttime': true

        },
        tpwidget.handlePostLoginActivity = function (activity, txnId) {


            var request = "?uid=" + tpwidget.data.uid;
            request += "&pcode=" + tpwidget.data.pcode;
            request += "&scode=" + tpwidget.data.scode;
            request += "&aname=" + activity;
            request += "&pfm=WEB";
            request += "&txnId=" + txnId;


            tpwidget.sendActivityRequest(request);
        },
        tpwidget.sendActivityRequest = function (request) {
            //replacing server call with client call

            $.get("https://tpapi.timespoints.com/v1/activity/logact" + request, function (data, status) {
                if (data.status == '"FAILURE"') {
                    console.log(data.message);
                } else {
                    console.log("TPActivity Post Login Activity URL -https://tpapi.timespoints.com/v1/activity/logact" + request)
                }


            });
        },
        tpwidget.bindEvents = function () {

            $(document).on('VOD_EVENTS', function (event, data, eventType) {
                var msid = data.id ? data.id : Math.round((new Date()).getTime())
                if (typeof eventType != "undefined" && eventType.toUpperCase() == "VIDEOVIEW") {
                    tpwidget.fireActivity('watch_video', msid)
                }
            });

            // Article Read
            $(document).on('article_read', function (event, data, eventType) {

                tpwidget.fireActivity('read', data.msid);

            });
            
            //View Photos 
            $(document).on('view_photo', function (event, data, eventType) {

                tpwidget.fireActivity('view_photo', data.msid);

            });
            
            $(document).on('social.onBeforeShare', function (e) {
                if (!tpwidget.data.isUserLoggedIn) {
                    TPWidget.addPreLoginActivity('sh_tw')
                }

            });
        },
        tpwidget.fireActivity = function (aname, txnId) {
            if (!tpwidget.data.isUserLoggedIn) {
                console.log('TPActivity Activity- ' + aname + ', Transcation ID-' + txnId)
                TPWidget.addPreLoginActivity(aname)
            } else {

                tpwidget.handlePostLoginActivity(aname, txnId);
            }
        },
        tpwidget.initialisewidget = function (userloginInfo) {

            if (typeof TPWidget != 'undefined' && typeof TPWidget.init == 'function') {
                var widgettypes = [];
                $.each(tpwidget.data.widgettypes, function (i, val) {
                    if ($("#" + val).length) {

                        widgettypes.push({
                            'ele': val,
                            'widgetType': tpwidget.data[val]
                        })

                    }


                });


                if (userloginInfo != null && typeof(userloginInfo) != 'undefined') {

                    TPWidget.init({

                        widgets: widgettypes,

                        userLoginInfo: {

                            "host": tpwidget.data.pcode,

                            "channel": tpwidget.data.scode,

                            "URL": escape(document.location.href),

                            "userId": tpwidget.data.uid,

                            "oid": "",

                            "email": tpwidget.data.email

                        }

                    });
                } else {

                    TPWidget.init({
                        widgets: widgettypes
                    });
                }

            }

            event.publish('tpinitated')


        },
        tpwidget.PostLoginActions = function () {

            TPWidget.PostLoginActions({
                "host": "TOI",
                "channel": "News",
                "URL": escape(document.location.href),
                "userId": tpwidget.data.uid,
                "oid": "",
                "email": tpwidget.data.email
            });

        },
        /**
         * { subscribeLoggedInStatus - to subscribe user logged in ,logged out status and behave accordingly }
         */
        tpwidget.subscribeLoggedInStatus = function () {


            event.subscribe("user.status", function (user) {
                tpwidget.data.isUserLoggedIn = typeof user !== "undefined" && user !== null ? true : false;
                // For First time Activity
                if (!tpwidget.data.isInitialised) {
                    tpwidget.updateUserDetails();
                    tpwidget.initialisewidget(user)
                    tpwidget.data.isInitialised = true;
                    // Fire Visit activity for Home page only
                    if (toiprops && toiprops["_Tmpl_pg"] && toiprops["_Tmpl_pg"].toLowerCase() == 'default')
                         {   tpwidget.fireActivity('visit', Math.round((new Date()).getTime()))
                            tpwidget.fireActivity('register', Math.round((new Date()).getTime()))
                        }
                }


                tpwidget.data.firsttime = false;

            });
            //Logged in User
            event.subscribe("user.login", function (user) {

                if (user) {

                    tpwidget.data.isUserLoggedIn = true;

                    tpwidget.updateUserDetails(user.getEmail());
                    tpwidget.PostLoginActions();
                    typeof ga == "function" && ga('send', 'event', 'TPRedeem', 'login_success', window.location.href);

                }

            });
            event.subscribe("user.logout", function (user) {
                tpwidget.data.isUserLoggedIn = false;
                if (!tpwidget.data.firsttime) {

                    // Logged-in User logged out
                    if (typeof TPWidget != 'undefined' && typeof TPWidget.logoutAction == 'function') {

                        TPWidget.logoutAction();
                    }

                }

                tpwidget.data.isUserLoggedIn = false;

            });

        },
        tpwidget.updateUserDetails = function (email) {
            tpwidget.data.email = email ? email : cookie && cookie.get('MSCSAuthDetails') && cookie.get('MSCSAuthDetails').split('=')[1];
            tpwidget.data.uid = cookie && cookie.get('ssoid')
        }
    return tpwidget;
});


document.domain = tLoginObj.domainName;    
var TimesApps = window.TimesApps || {};
TimesApps.VideoCore = (function(){
    var config, fn, api;
    
    config = {
        iframeTemplateName : "vod_player.cms",
        iframeHtml: '<iframe allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>'
    }
    
    fn = {
        _generateIframeSrc : function(dataParams){
            var src =  "/" + config.iframeTemplateName + "?";
            var params = [];
            
            for(key in dataParams){
                if( !dataParams.hasOwnProperty(key) ){
                    return;
                }
                
                params.push(key + "=" + dataParams[key]);
            }
            
            src += params.join('&');
            
            return src;
        },
        
        _createVideoIframe : function(dataParams){
            var src = fn._generateIframeSrc(dataParams);

            var container = document.createElement("div");
            $(container).html(config.iframeHtml);
            $(container).find("iframe").attr("src", src);
            if(dataParams.classNames){
                $(container).find("iframe").addClass(dataParams.classNames);
            }
            if(dataParams.domId){
                $(container).find("iframe").attr('id', dataParams.domId);
            }
            return container.innerHTML;
        }
    }
    
    api = {
        generateIframeSrc : function(dataParams){ return fn._generateIframeSrc(msid, dataParams); },
        createVideoIframe : function(dataParams){ return fn._createVideoIframe(dataParams) }
    }
    
    return api;
}(window.jQuery));


TimesApps.overlayModule = (function($){
        var fn, api, data, defaults, templates, bindEvents;
        
        templates = {
            defaultHtml : '<div id="vcw" class="jOverlay video-overlay-wrapper"><div class="inner"><div class="buttonContainer"><span class="jToggleOverlay expand"></span><button type="button" class="closebtn"></button></div><div class="video-content"></div></div></div>'
        }
        
        defaults = {
                    html : "",
                    videosection: "toi",
                    source: "toi",
                    pg : "toi",
                    isFullscreen : false,
                    allowDock : false
                };
                
        data = {
            isOverlayOpen : false
        }
                
        bindEvents = function(){
            $('#vcw .closebtn').off('click').on('click', fn._close);
            $('#vcw .jToggleOverlay').off('click').on('click', fn._toggleOverlayAndDock);
        }
        
        fn = {
            _init : function(){
                $(document).on('keyup', function(e){
                    if(e.keyCode == 27){ // on Esc key press
                       fn._close();
                    } 
                });
            },
            _create : function(options){
                data = $.extend( {},defaults,options);
                fn._render(data.html);
                bindEvents();
            },
            _render : function(html){
                if( !$("#vcw").length ){
                    var sidebar = ( $(".sidebar").length ? $(".sidebar") : $(".sidebar2") ).eq(0);
                    var wrapper = sidebar.closest('.wrapper').length ? sidebar.closest('.wrapper') : $(".wrapper");
                    wrapper.eq(0).append(templates.defaultHtml);
                }
                if( data.isFullscreen ){
                    $(".jOverlay")
                            .addClass("fullscreen");
                    $("body")
                            .addClass("overflowHidden");
                }else{
                    $(".jOverlay")
                            .removeClass("fullscreen");
                }
                $("#vcw .video-content").html(html);
                $("#vcw").fadeIn(500);
                data.isOverlayOpen = true;

                if( !data.allowDock ){
                    $('.jToggleOverlay').addClass('hidden');
                }
            },
            _close : function(){
                $("body")
                    .removeClass("overflowHidden");
                $('#vcw').fadeOut(500,function(){$('#vcw').remove();});
                // $('body').css('overflow','auto');
                data.isOverlayOpen = false;
                require(["tiljs/event"], function(eventBus){
                    eventBus.publish("overlayClosed", {id : data.id});
                });
            },
            _isOverlayOpen : function(){
                return data.isOverlayOpen;
            },
            _getSidebarDimenions : function(){
                var dimensions = {
                    leftOffset : 0,
                    width : 0
                };

                var sidebar = ( $(".sidebar").length ? $(".sidebar") : $(".sidebar2") ).eq(0);
                if( !sidebar.length ){
                    return dimensions;
                }
                
                dimensions.width = sidebar.width() + "px";
                
                var wrappperOffset = 0;
                var wrapper = sidebar.closest('.wrapper').length ? sidebar.closest('.wrapper') : $(".wrapper");
                if( wrapper.length ){
                    wrappperOffset = wrapper.offset().left + parseInt( wrapper.css("padding-left") );
                    wrappperOffset = Math.abs(wrappperOffset);
                }
                
                dimensions.leftOffset = ( sidebar.offset().left - wrappperOffset )  + "px";
                return dimensions;
            },
            _positionDock : function(){
                var jOverlay = $(".jOverlay");
                var sidebarDimensions = fn._getSidebarDimenions();

                jOverlay
                    .css({'margin-left': sidebarDimensions.leftOffset, 'width': sidebarDimensions.width })
                    .addClass("belowHeader");
            },
            _toggleOverlayAndDock : function(e){
                if( !data.allowDock ){
                    return;
                }
                var jOverlay = $(".jOverlay");
                jOverlay
                    .toggleClass('video-overlay-wrapper fullscreen dockedVideo fixed animateDock dockedVideoSize')
                    .removeClass("belowHeader")
                    .css({'margin-left':'', 'width':''});
                $('body')
                    .toggleClass('overflowHidden');

                if( data.isOverlayOpen ){
                    data.isOverlayOpen = false;
                    fn._positionDock();
                    require(["tiljs/event"], function(eventBus){
                        eventBus.publish("overlayClosed", {id : data.id});
                        eventBus.publish("onOverlayDock", {id : data.id});
                    });
                }else{
                    data.isOverlayOpen = true;
                    require(["tiljs/event"], function(eventBus){
                        eventBus.publish("overlayOpen", {id : data.id});
                    });
                }
            }
        }
        
        api = {
            init : function(){ fn._init(); },
            create: function(options){ fn._create(options); },
            close : function(){ fn._close(); },
            isOverlayOpen : function(){ return fn._isOverlayOpen(); },
            getSidebarPosition : function(){ return fn._getSidebarDimenions(); }
        }
        return api;
    }(jQuery));
TimesApps.overlayModule.init();

	
 
    define('onetapsignin',[
        "tiljs/cookie",
        "tiljs/ui",
        "tiljs/login",
        "tiljs/event" ],
    function (cookie, ui, login, event) {
        var constants = {
            clientId : "103703403489-b4t4lt8mr05brqpcdrmsu0di54cmjv4f.apps.googleusercontent.com",
            //get your domain whitelisted in authorised javascript origin domains
            scriptURL : "https://smartlock.google.com/client",
            jssoAPIru : "bchjqhklhblchbhjglh1wbfbv",
            jssoAPIGetURL : tLoginObj.ssoUrl+"/sso/services/gponetaplogin/verify",
            nextShowTimeInDays : 15,
            nextShowCookieName : "oneTapNextShow",
            sessionDuration : 10000,
            doNotInitiateOneTapByDefault: typeof __initializeOneTapForTOI !== 'undefined' && __initializeOneTapForTOI === false
        }
        
        var mod_onetapsignin = {};
        
        mod_onetapsignin.jssoCallback = function () {
            var callback = function (user) {
                if(user) {
                    event.publish("user.login", user);
                    mod_onetapsignin.attachPostLoginHTML(user);
                }
                ga('send', 'event', 'Login', 'One_Tap', 'Success_google');
            };
            
            login.isLoggedIn(callback);
        };
        
        mod_onetapsignin.initiateAPI = function(callback) {
            var ssoid = cookie.get("ssoid") || cookie.get("ssoId");
            var sessionDate, currentDate = new Date();
            if(ssoid){
                return;
            }
            
            if(typeof callback === 'undefined') {
                callback = function() {};
            }
            
            if(typeof window.googleyolo !== 'object') {
                callback();
                return;
            }
            
            
            //  ga('send', 'event', 'Login', 'One_Tap', 'Initiated');
            var checkForIframe = function(){
                $('iframe').each(function(index,item) {
                	var iframeSrc=$(item).attr('src');
                	if(iframeSrc && iframeSrc.indexOf('smartlock')>0){
            		    console.log('view');
            			ga('send', 'event', 'Login', 'One_Tap', 'View');
            			clearInterval(window.iframeTimer);
                	}
                });
            }
            window.iframeTimer=setInterval(checkForIframe,2000);
            
            //console.log(googleyolo);
            //Hint method/promise is used to initiate google login
            // supportedAuthMethods is list of types of logins
            //clientID  is where all analytics data will be sent (Should be created by TOI google account) done!!!
            if(window.googleyolo){
                googleyolo=window.googleyolo;
                if(!constants.doNotInitiateOneTapByDefault) {
                    try {
                        if(typeof window.localStorage !== 'undefined') {
                            // set session for 12 hours
                            window.localStorage.setItem('onetap_session', currentDate.getTime() + (12*60*60*1000));
                        }
                    } catch(e) {}   
                }
            }
            googleyolo.setTimeouts(5000);
            var hintPromise = googleyolo.hint({
              supportedAuthMethods: [
                "https://accounts.google.com"
              ],
              supportedIdTokenProviders: [
                {
                  uri: "https://accounts.google.com",
                  clientId: constants.clientId
                }
              ]
            });
            // get your domain whitelisted by SSO team 
            //The Hint promise resolves here
            hintPromise.then(function(credential) {
              if (credential.idToken) {
                 constants.credential=credential;
                // Send the token to your auth backend.
                //console.log(credential.idToken);
                var googleLongToken=credential.idToken;
                var channel="toi";
                var ru=constants.jssoAPIru;
                var request = $.ajax({
                  url:constants.jssoAPIGetURL,
                  method: "GET",
                  data: { token: googleLongToken,channel:channel,ru:ru},
                  dataType: "jsonp",
                  success:mod_onetapsignin.jssoCallback
                });
              }
            }, function(error) {
              switch (error.type) {
                //  most common errors are when there are accounts available , google's native API does not  throw error,User closes the popup
                case "userCanceled":
                    ga('send', 'event', 'Login', 'One_Tap', 'Close');
                  // The user closed the hint selector. Depending on the desired UX,
                  // request manual sign up or do nothing.
                    cookie.set(constants.nextShowCookieName, '123', constants.nextShowTimeInDays);
                  break;
                case "noCredentialsAvailable":
                    callback();
                    ga('send', 'event', 'Login', 'One_Tap', 'ErrorNoCredentialsAvailable');
                  // No hint available for the session. Depending on the desired UX,
                  break;
                case "requestFailed":
                    callback();
                    ga('send', 'event', 'Login', 'One_Tap', 'ErrorTimesout');
                    //retry once more
                    /*if(! window.oneTapRetry){
                        window.oneTapRetry=setTimeout(mod_onetapsignin.initiateAPI(), 6000);
                        console.log('One Tap retry')
                    }*/
                     
                  // The request failed, most likely because of a timeout.
                    break;
                default:
                    callback();
                    ga('send', 'event', 'Login', 'One_Tap', error.type);
              }
            });
            
        };
        
        mod_onetapsignin.setGoogleYoloLoadObj = function(googleyolo) {
            window.googleyoloObj = googleyolo;
            if(!constants.doNotInitiateOneTapByDefault) {
                mod_onetapsignin.initiateAPI();
            }
        };
        
        mod_onetapsignin.attachPostLoginHTML = function(user) {
            var loginHTML = 
                '<div class="ontap-background id_postLogin hide">'+
                    '<div class="tap-message">'+
                        '<span class="success"></span>'+
                        '<span class="text">Successfully signed in as '+user.getEmail()+'</span>'+
                        '<span class="close id_onetapclose"></span>'+
                    '</div>'+
                '</div>';
            var $topArea = $('.top-area .row');
            if($topArea.length === 1) {
                $topArea.append(loginHTML);
            } else {
                $('body').append(loginHTML);   
            }
            
            $('.id_postLogin').slideToggle();
            $('.id_onetapclose').on('click',function(){
                $('.id_postLogin').slideToggle();
            });
            
            // Auto hide success login pop up after 10 seconds
            setTimeout(function() {
                var $postLogin = $('.id_postLogin');
                if($postLogin.is(':visible')) {
                    $postLogin.slideToggle();
                }
            }, 10000);
        };
        
        mod_onetapsignin.bindFunction = function() {
            window.onGoogleYoloLoad = mod_onetapsignin.setGoogleYoloLoadObj;
        };
        
        mod_onetapsignin.loadjs = function() {
            //append the google oneTap API script to dom
            (function(d, s, id){
                 var js, fjs = d.getElementsByTagName(s)[0];
                 if (d.getElementById(id)) {return;}
                 js = d.createElement(s); js.id = id;
                 js.src = "https://smartlock.google.com/client";
                 fjs.parentNode.insertBefore(js, fjs);
               }(document, 'script', 'onetap-jssdk'));
        };
        
        mod_onetapsignin.init = function() {
            var onetapInitCallback = function() {
                var nextShowCookieName = cookie.get(constants.nextShowCookieName);
                var isOnetapSessionShown = false;
                var ssoid = cookie.get("ssoid") || cookie.get("ssoId");
                var currentDate = new Date();
                var sessionDate;
                if(!ssoid){
                    mod_onetapsignin.bindFunction();
                    
                    // do not initiate if on newsletter page
                    if(constants.doNotInitiateOneTapByDefault) {
                        return;
                    }
                    
                    try {
                        if(typeof window.localStorage !== 'undefined') {
                            sessionDate = window.localStorage.getItem('onetap_session');
                            if(sessionDate && sessionDate > currentDate) {
                                isOnetapSessionShown = true;
                                // window.localStorage.setItem('onetap_session', currentDate.setHours(23,59,59,999));
                            }
                        }
                    } catch(e) {}
                    
                    // load js after non logged in user is on page for specified seconds and onetapcookie is expired
                    if(!nextShowCookieName && !isOnetapSessionShown) {
                        setTimeout(mod_onetapsignin.loadjs, constants.sessionDuration);
                    }
                    
                }
            }
            
            TimesApps.checkGdprAndCall(onetapInitCallback);
        };
        
        return mod_onetapsignin;
    });



    /* To integrate login please do the following:
     * 1. Fork toiusermanagement_js and usermanagementcss and include it in your project
     * 2. Add below JS files in your project. These files should be added globally
     *  a) <script type="text/javascript" src="https://jssocdn.indiatimes.com/crosswalk/jsso_crosswalk_legacy_0.2.4.min.js"></script>
     *  b) <script src="https://www.google.com/recaptcha/api.js" async="true" defer="true"></script>
     * 3. Update GA events category in the toiusermanagement_js as per your project needs
     * 4. Update usermanagementcss file to change skin of Login modal
    */
    define('tiljs/apps/times/usermanagement',[
        "../../util",
        "module",
        "../../page",
        "../../ajax",
        "../times/api",
        "../../is",
        "../../cookie",
        "../../ui",
        "../../logger",
        "../../event",
        "../../load",
        "../../localstorage",
        "../../user"
    ],
    function (util, module, page, ajax, api, is, cookie, ui, logger, event, load, localstorage, user) {
        window.newLogin = true;// This enables twitter share dialog in /share.cms

        var default_config = {
            autoinit: false,
            multiuser: false, //allows multiple users to login at a time
            login: "",
            logout: "",
            check_user_status: "",
            mapping: null,
            renderer: true //todo to be implemented
        };
        var config = util.extend(true, {}, default_config, module.config());
        
        var errorConfig = {
            fpNoEmailOrMobile: 'Email/Mobile No. cannot be left blank.',
            fpInvalidEmail: 'Enter a valid Email/Mobile No.',
            fpInvalidEmailOnly: 'Enter a valid email.',
            fpInvalidMobileOnly: 'Enter a valid mobile no.',
            accountUnregistered: 'This account is not registered with us.',
            emptyPassword: 'Password cannot be left blank.',
            emptyName: 'Enter your full name.',
            wrongName: 'Enter your name without special character or numbers.',
            wrongPassword: 'Invalid Credentials',
            wrongMobile: 'Invalid Mobile number.',
            wrongEmail: 'Invalid Email Id.',
            expiredOTP: 'The OTP you entered has expired.',
            limitExceeded: 'Maximum number of unsuccessful attempts exceeded. Please try again later.',
            wrongOtp: 'Enter valid OTP sent to your mobile no.',
            wrongOtpEmail: 'Enter valid OTP sent to your email.',
            matchLastThree: 'Password cannot match your last three passwords.',
            passwordMismatch: 'Your passwords do not match.',
            captchaUnselected: 'Select the checkbox to proceed.',
            tncNotSelected: 'Accept Terms & Conditions to proceed.',
            userAlreadyRegistered: 'This user is already registered with us.',
            serverError: 'An error occurred while processing your request. Please try again later.',
            connectionError: 'There was an error processing your request. Please check your internet connection and try again.',
            email_failure:"Username or password is incorrect. Please try again.",
            facebook_failure:"Username or password is incorrect. Please try again.",
            facebook_failure_user_denied:"Kindly authenticate facebook request. Please try again.",
            facebook_failure_no_email:"Could not login using this facebook account. Please try again with another account.",
            twitter_failure:"Kindly authenticate twitter request.",
            twitter_link_username_failure:"Username or password is incorrect. Please try again.",
            twitter_link_email_failure:"Email or password is incorrect. Please try again.",
            twitter_failure_unknown:"Kindly authenticate twitter request.",
            twitter_failure_user_denied:"Username or password is incorrect. Please try again.",
            twitter_failure_server_error:"Server did not respond. Please try again.",
            twitter_failure_already_exist:"It seems you have already registered with the Indiatimes network using this Email ID. Try linking your Twitter account with your existing Indiatimes account or use an alternate Email ID.",
            twitter_failure_invalid_email:"It seems you have entered and invalid Email ID. Try linking your Twitter account with your existing Indiatimes account or use an alternate Email ID.",
            unknown_error:"Unknown error has occurred. Please try again."
        };
        
        var constants = {
            oauthUrl: tLoginObj.ssoUrl+"/sso/identity/login/socialLogin?channel={$channel}&oauthsiteid={$siteid}&ru=" + encodeURIComponent(site_url+"/phpsso"),
            oauthUrlFacebook: 'https://www.facebook.com/v3.1/dialog/oauth?display=popup&scope=email%2Cuser_birthday%2Cuser_hometown&client_id='+tLoginObj.fbclient+'&state={"oauthsiteid":"facebook"}&redirect_uri='+encodeURIComponent(site_url+"/t-login-sso"),
            oauthUrlGooglePlus: 'https://accounts.google.com/o/oauth2/auth?response_type=code&scope=email%20https://www.googleapis.com/auth/plus.login&access_type=online&client_id='+tLoginObj.gPlusclient+'&state={"oauthsiteid":"googleplus"}&redirect_uri='+encodeURIComponent(site_url+"/t-login-sso"),
            logoutUrl : tLoginObj.ssoUrl+"/sso/identity/profile/logout/external?channel={$channel}&redirect_uri="+encodeURIComponent(site_url+"/t-login-sso?channel="+tLoginObj.ssochannel+"&status=logout"),
        };
        
        var cachedElements = {
            loginPopup: $('#login-popup'),
            closeBtn: $('#login-popup .close-btn')
        }
        
        var ssoLoginType = '', 
            jssoCrosswalkObj, 
            recaptchaCode = '', 
            recaptchaWidgetId = '', 
            pageName='', 
            screenName='Login_Screen',
            registerFormSubmitted = false;
        
        var userList = {};
        
        var single_user_id = "SINGLE_USER";
        
        var call_sso = function () {
            if (localStorage.getItem('_ssodata') != null) {
                __sso(JSON.parse(localStorage.getItem('_ssodata')));
                localStorage.removeItem('_ssodata');
            }
        }
        // to handle ios chrome issue - where parent is undefined
        var sso = function (url, callback) {
            loginWindow = ui.window(url, {
                width: 850,
                height: 780,
                scrollbars: 0
            }, function (result) {
                if (result && result.code === "200") {
                    mod_login.isLoggedIn();
                } else {
                    if (callback) {
                        callback(null, result);
                    }
                }
            });
            loginWindow.moveTo(315, 250);
        };
        var mod_login = {};
        var loginCallback = null;
        var loginData = null;
        var setLoginError = function (loginErrorMsg) {
            setLoginData({
                error: {
                    code: loginErrorMsg,
                    message: config.messages[loginErrorMsg]
                }
            });
        };
        var setLoginData = function (data) {
            loginData = data;
        };
        var reset = function () {
            return;
            if (loginWindow) {
                loginWindow.close();
                loginWindow = null;
            }
            loginData = null;
        };
        
        var setLocation = function (href) {
            if (is.iframe(loginWindow)) {
                loginWindow.src = href;
            } else {
                loginWindow.document.location.href = href;
            }
        };
        var loginResponse = function (site, error) {
            if(site === 'facebook') {
                $("#sso-fblogin-error").html(errorConfig[error]).show();
            } else {
                $("#sso-gplus-error").html(errorConfig[error]).show();
            }         
        };
        
        var loginResponseTwitter = function (url, error, data) {
            setLocation(url);
            if (error) {
                setLoginError(error);
            } else if (data) {
                setLoginData(data);
            }
        };
        
        var loginWindow = null;
        
        var setCriOS;
        
        if (config.autoinit === true) {
            mod_login.init();
        }
        
        try {
            document.domain = page.getDomain();
        } catch (e) {
            logger.info("Domain cannot be set:" + page.getDomain());
        }
        
        // to handle ios chrome issue - where parent is undefined
        if (navigator.userAgent.match('CriOS')) {
            localStorage.removeItem('_ssodata');
            setCriOS = setCriOS || setInterval(function () {
                    call_sso();
                }, 1000);
        }

        window.getLoginCallback = function () {
            return loginCallback;
        };
        window.getLoginData = function () {
            return loginData;
        };
        window.getDomain = function () {
            return page.getDomain();
        };
        
        window.__sso = function (data, url) {
            var $loginPopup = cachedElements.loginPopup;
            logger.log("__sso called");
            logger.log(data);
            logger.log(loginWindow);
            if (data && loginWindow) {
                logger.log(url);
                var currLoginData = loginData ? loginData.data : null;
                var closeWindow = data.closeWindow,
                    isLogout = data.status === "logout",
                    signInSuccess = data.status === "signinsuccess" || data.status === "ssosigninsuccess",
                    fbOrGplusMappingUpdated = (data.site === "facebook" || data.site === "googleplus" ) && data.status === "MappingUpdated";
                data.site = data.site || "";
                if (closeWindow || isLogout || signInSuccess || fbOrGplusMappingUpdated) {
                    logger.log("Close pressed, closing window / popup");
                    loginWindow.close();
                    $("#sso-fblogin-error, #sso-gplus-error").html('').hide();
                    if($loginPopup.is(':visible')) {
                        mod_login.closeBtnHandler();
                    }
                    
                    if(data.site && (signInSuccess || fbOrGplusMappingUpdated)) {
                        mod_login.fireGAEvent(data.site === 'googleplus'? 'Login_Success_Google': 'Login_Success_FB');
                    }
                } else if (data.status == "ssosigninfailure" && data.ssoerror == "E119") { //E119-username with password incorrect
                    loginResponse(data.site, "email_failure");
                } else if (data.status == "ssosigninfailure" && data.ssoerror == "E104") { //E104-email with password incorrect
                    loginResponse(data.site, "email_failure");
                } else if (data.err == "E104" && data.facebooktoken) { //E104-facebook not sending email
                    loginResponse(data.site, "facebook_failure_no_email");
                } else if (data.status == "signinfailure" && data.error == "F101") {
                    loginResponse(data.site, data.site + "_failure");
                } else if (data.status == "signinfailure" && data.error == "user_denied" && data.site == "facebook") { //facebook user denied access to the account
                    loginResponse(data.site, "facebook_failure_user_denied");
                } /* else if (data.status == "signinfailure" && data.error == "twitter" && data.site == "twitter") { // twitter user denied access to account
                    loginResponseTwitter(config.base_url + "/loginview.cms?x=error&site=twitter", "twitter_failure_unknown");
                } else if (data.error == "E104" && data.site == "twitter") { //when log in with twitter and it is not linked
                    loginResponseTwitter(config.base_url + "/socialconnect.cms?site=twitter", null, {
                        data: data,
                        twitter_connect: true
                    });
                } else if (data.error == "E119" && data.site == "twitter") { //when log in for linking twitter, username/email with wrong pasword
                    loginResponseTwitter(config.base_url + "/socialconnect.cms?site=twitter", "twitter_link_username_failure");
                    loginData.data = currLoginData;
                } else if (data.status == "signinfailure" && data.error == "T102" && data.site == "twitter") { //when twitter server is not responding
                    loginResponseTwitter(config.base_url + "/socialconnect.cms?site=" + data.site, "twitter_failure_server_error");
                } else if (data.error == "E103" && data.site == "twitter") { //when trying to create a new account using already existing id after twitter connect
                    loginResponseTwitter(config.base_url + "/socialconnect.cms?site=" + data.site, "twitter_failure_already_exist", loginData ? loginData.data : null);
                    loginData.data = currLoginData;
                } else if (data.error == "SSO_INVALID_RES_CHK_MAIL_AV" && data.site == "twitter") { //when trying to create a new account a@b.com after twitter connect
                    loginResponseTwitter(config.base_url + "/socialconnect.cms?site=" + data.site, "twitter_failure_invalid_email");
                    loginData.data = currLoginData;
                }*/ else {
                    logger.warn("Login case not handled");
                    loginResponse(data.site, "unknown_error");
                }
            }
            logger.log("Checking user status in __sso");

            mod_login.isLoggedIn(loginCallback);
        };
        mod_login.closeWindow = window.closeLoginWindow = function () {
            if (loginWindow) {
                loginWindow.close();
            }
        };
        
        mod_login.fireGAEvent = function (label) {
            label += ('-' + window.location.pathname);
            ga('send', 'event', 'WEB_Login', mod_login.getScreenName(), label);
        };
        
        /**
         * Returns false if browser is Opera
         *
         * @param 
         * @param 
         */
        mod_login.showCaptcha = function () {
            return (navigator.userAgent.indexOf("Opera") === -1);
        };
        
        mod_login.registerFormSubmitted = function (isSubmitted) {
            registerFormSubmitted = !!isSubmitted;
        };
        
        mod_login.setLoginWindowDimension = window.setLoginWindowDimension = function (width, height) {
            if (loginWindow) {
                loginWindow.resizeTo(width, height);
                if (loginWindow.reposition) { //TODO not working for window.open - popup
                    loginWindow.reposition();
                }
                loginWindow.focus();
            }
        };
        mod_login.login = function (callback, action, identifier) {
            TimesApps.checkGdprAndCall(function() {
                mod_login.showLoginScreen(callback);
                cachedElements.loginPopup.addClass('active');
            }, mod_login.logout);
        };
        mod_login.loginWithTwitter = function (callback) {
            loginCallback = function (user) {
                event.publish("user.login", user);
                if (callback) {
                    callback(user);
                }
                mod_login.closeBtnHandler();
            };   
            reset();
            
            TimesApps.checkGdprAndCall(function(){
                var login_url = constants.oauthUrl.replace('{$channel}', page.getChannel().toLowerCase()).replace('{$siteid}', 'twitter');
                sso(login_url, callback);
            }, mod_login.logout);
        };
        
        mod_login.loginWithFacebook = function (callback) {
            loginCallback = function (user) {
                event.publish("user.login", user);
                if (callback) {
                    callback(user);
                }
                mod_login.closeBtnHandler();
            };    

            reset();
            
            mod_login.initiateFbLogin(callback);
            
        };
        
        // Initiate FB Login
        mod_login.initiateFbLogin = function (callback) {
            TimesApps.checkGdprAndCall(function(){
                mod_login.setScreenName('Login_Screen');
                mod_login.fireGAEvent('Click_FB');
                var login_url = constants.oauthUrl.replace('{$channel}', page.getChannel().toLowerCase()).replace('{$siteid}', 'facebook');
                //var login_url = constants.oauthUrlFacebook.replace('{$channel}', page.getChannel().toLowerCase());
                console.log(login_url,'IKHWAN');
                sso(login_url, callback);
            }, mod_login.logout);
        };
        
        mod_login.loginWithGoogle = function (callback) {
            loginCallback = function (user) {
                event.publish("user.login", user);
                if (callback) {
                    callback(user);
                }
                mod_login.closeBtnHandler();
            };   
            reset();
            mod_login.initiateGplusLogin(callback);
        };
        
        // Initiates Google login 
        mod_login.initiateGplusLogin = function ( callback ) {
            TimesApps.checkGdprAndCall(function(){
                mod_login.setScreenName('Login_Screen');
                mod_login.fireGAEvent('Click_Google');
                var login_url = constants.oauthUrl.replace('{$channel}', page.getChannel().toLowerCase()).replace('{$siteid}', 'googleplus');
                //var login_url = constants.oauthUrlGooglePlus.replace('{$channel}', page.getChannel().toLowerCase());
                sso(login_url, callback);
            }, mod_login.logout);
        };
        
        /**
         * Creates Login Screen UI and inserts in page
         *
         * @param 
         * @param 
         */
        mod_login.showLoginScreen = function (callback) {
            loginCallback = function (user) {
                event.publish("user.login", user);
                // Trigger GA Event for All Login
                if (window.ga) {

                    ga('send', 'event', 'Login', 'Mast Head', document.location.href, {'nonInteraction': 1});
                }
                if (callback) {
                    callback(user);
                }
            };
            var loginScreen = '';
            loginScreen +=  '<div id="toi-login">'
                        +       '<div class="signin-section">'
                        +           '<figure class="user-icon"><img src="https://timesofindia.indiatimes.com/photo/63379366.cms" src="user-icon" /></figure>'
                        +           '<div id="socialConnectImgDiv">'
                        +               '<button type="button" id="sso-fb-login" class="fb">Sign in with Facebook</button>'
                        +               '<span id="sso-fblogin-error" class="errorMsg"></span>'
                        +               '<button type="button" id="sso-gplus-login" class="gplus">Sign in with Google</button>'
                        +               '<span id="sso-gplus-error" class="errorMsg"></span>'
                        +           '</div>'
                        +           '<h4 class="heading small">'
                        +               '<span>or go the traditional way</span>'
                        +           '</h4>'
                        +           '<form class="form" autocomplete="off">'
                        +               '<ul>'
                        +                   '<li class="input-field email">'
                        +                       '<p>'
                        +                           '<input autocomplete="off" type="text" name="emailId" placeholder="Sign In/Sign Up with Email or Mobile No." maxlength="100" />'
                        +                       '</p>'
                        +                       '<div class="errorMsg"></div>'
                        +                       '<a href="javascript:void(0)" id="changeEmailIdDiv" class="secondary-link" style="display: none">Change Email Or Mobile No.</a>'
                        +                   '</li>'
                        +                   '<li class="input-field password" id="sso-pwdDiv">'
                        +                       '<p>'
                        +                           '<input autocomplete="off" type="password" name="password" placeholder="Password" maxlength="14" />'
                        +                           '<span class="view-password"></span>'
                        +                       '</p>'
                        +                       '<div class="errorMsg"></div>'
                        +                       '<a id="sso-generate-otp" href="javascript:void(0)" class="secondary-link">Generate OTP to Login</a>'
                        +                   '</li>'
                        +                   '<li id="sso-login-otp-msg" class="text-field">'
                        +                       '<p></p>'
                        +                   '</li>'
                        +                   '<li class="input-field password" id="sso-otpLoginDiv">'
                        +                       '<p>'
                        +                           '<input type="password" name="otplogin" maxlength="6" placeholder="Enter the verification code"/>'
                        +                       '</p>'
                        +                       '<div class="errorMsg"></div>'
                        +                       '<div class="successMsg"></div>'
                        +                       '<span class="regenerate-otp">Didn\'t receive OTP?</span>'
                        +                       '<a id="sso-regenerate-otp" href="javascript:void(0)" class="secondary-link">Re-Generate OTP</a>'
                        +                   '</li>'
                        +                   '<li id="sso-signInButtonDiv" class="submit">'
                        +                       '<input type="submit" class="submit-btn disabled" value="Continue" disabled="disabled" />'
                        +                   '</li>'
                        +               '</ul>'
                        +               '<a href="javascript:void(0)" id="sso-forgot-pass" class="forget-password">Forgot Password?</a>'
                        +           '</form>'
                        +       '</div>'
                        +       '<div class="powered-by">'
                        +           '<img src="https://static.toiimg.com/photo/65257504.cms" />'
                        +       '</div>'
                        +       '<div class="teams-logo">'
                        +          '<span>One Network. One Account</span>'
                        +           '<a href="http://timesofindia.indiatimes.com/" target="_blank" class="toi"/><a href="http://economictimes.indiatimes.com/" target="_blank" class="et"/><a href="http://navbharattimes.indiatimes.com/" class="'+tLoginObj.ssochannel+'" target="_blank"/><a href="http://maharashtratimes.indiatimes.com/" class="sm" target="_blank"/><a href="http://www.speakingtree.in/" class="st" target="_blank"/><a href="http://gaana.com/" class="gaana" target="_blank"/><a href="http://itimes.com/" class="itimes" target="_blank"/><a href="http://www.timespoints.com" class="tp" target="_blank"/>'
                        +       '</div>'
                        +   '</div>';
            $('#user-sign-in').html(loginScreen);
        };
        
        /**
         * Creates Register Screen UI and inserts in page
         *
         * @param 
         * @param 
         */
        mod_login.showRegisterScreen = function (callback) {
            var inputVal = $('#toi-login input[name="emailId"]').val();
            var loginType = mod_login.getLoginType();
            var registerScreen = '';
            registerScreen  +=      '<div id="toi-register">'
                            +           '<div class="signup-section">'
                            +               '<h4 class="heading">'
                            +                   '<span>Complete Your Profile</span>'
                            +               '</h4>'
                            +               '<form class="form" action="#" autocomplete="off">'
                            +                   '<input type="hidden" id="register-inputVal" value="' + inputVal + '"/>'
                            +                   '<ul>'
                            +                       '<li class="input-field ' + (loginType !== 'email'? 'mobile-no': 'email') + '">'
                            +                           '<p>'
                            +                               (loginType !== 'email'? '<span class="country-code">+91 - </span>': '')
                            +                               '<input autocomplete="off" type="text" name="' + (loginType === 'email'? 'emailId': 'mobile') + '" maxlength="100" disabled="disabled" value="' + inputVal  + '" />'
                            +                           '</p>'
                            +                           '<a href="javascript:void(0)" id="changeRegisterEmailId" class="secondary-link">Change Email Or Mobile No.</a>'
                            +                       '</li>'
                            +                       '<li class="input-field user-name">'
                            +                           '<p>'
                            +                               '<input autocomplete="off" type="text" name="fullname" placeholder="Full Name" maxlength="30" />'
                            +                           '</p>'
                            +                           '<div class="errorMsg"></div>'
                            +                       '</li>'
                            +                       '<li class="input-field password">'
                            +                           '<p>'
                            +                               '<input autocomplete="off" type="password" name="registerPwd" placeholder="Password" maxlength="14" />'
                            +                               '<span class="view-password"></span>'
                            +                           '</p>'
                            +                           '<div class="password-conditions">'
                            +                               '<p>Password must have:</p>'
                            +                               '<ul>'
                            +                                   '<li id="charCnt" class="error">6-14 characters</li>'
                            +                                   '<li id="lwCnt" class="error">1 Lower case character (a-z)</li>'
                            +                                   '<li id="numCnt" class="error">1 Numeric character (0-9)</li>'
                            +                                   '<li id="spclCharCnt" class="error">1 special character (Such as #, $, %, &, !)</li>'
                            +                               '</ul>'
                            +                           '</div>'
                            +                       '</li>'
                            +                       '<li class="input-field password">'
                            +                           '<p>'
                            +                               '<input autocomplete="off" type="password" name="registerCnfrmPwd" placeholder="Confirm Password" maxlength="14" />'
                            +                               '<span class="view-password"></span>'
                            +                           '</p>'
                            +                           '<div class="errorMsg"></div>'
                            +                       '</li>'
                            +                       '<li class="input-field ' + (loginType === 'email'? 'mobile-no': 'email') + '">'
                            +                           '<p>'
                            +                               (loginType === 'email'? '<span class="country-code">+91 - </span>': '')
                            +                               '<input autocomplete="off" type="text" name="' + (loginType === 'email'? 'mobile': 'emailId') + '" maxlength="' + (loginType === 'email'? '10': '100') + '" placeholder="' + (loginType === 'email'? 'Mobile Number': 'Email') + ' (Optional)" />'
                            +                           '</p>'
                            +                           '<div class="errorMsg"></div>'
                            +                       '</li>';
                            if(mod_login.showCaptcha()) {
                                registerScreen  +=  '<li class="recaptcha-wrapper">'
                                            +           '<div id="recaptcha-container"></div>'
                                            +           '<div class="errorMsg"></div>'
                                            +       '</li>';    
                            }
                            
                            registerScreen +=       '<li class="checkbox">'
                            +                           '<p>'
                            +                               '<input type="checkbox" id="agree" name="agree" checked="checked">'
                            +                               '<label for="agree">I am at least 16 years old and agree with the'
                            +                               '<a href="https://www.indiatimes.com/termsandcondition/" target="_blank" rel="noopener noreferrer"> Terms & Conditions </a>and'
                            +                               '<a href="https://www.indiatimes.com/privacypolicy/" target="_blank" rel="noopener noreferrer"> Privacy Policy</a> of Times of India</label>'
                            +                           '</p>'
                            +                           '<div class="errorMsg"></div>'
                            +                       '</li>'
                            +                       '<li class="checkbox">'
                            +                           '<p>'
                            +                               '<input type="checkbox" id="promotions" name="promotions" checked="checked">'
                            +                               '<label for="promotions">Send me offers and promotions</label>'
                            +                           '</p>'
                            +                           '<div class="errorMsg"></div>'
                            +                       '</li>'
                            +                       '<li class="checkbox">'
                            +                           '<p>'
                            +                               '<input type="checkbox" id="sharedDataAllowed" name="sharedDataAllowed" checked="checked">'
                            +                               '<label for="sharedDataAllowed">Please show me personalized content and advertisements as per the '
                            +                               '<a href="https://www.indiatimes.com/privacypolicy/" target="_blank" rel="noopener noreferrer"> Privacy Policy</a></label>'
                            +                           '</p>'
                            +                           '<div class="errorMsg"></div>'
                            +                       '</li>'
                            +                       '<li class="submit">'
                            +                           '<input type="submit" id="sso-registerBtn" class="submit-btn" value="Update" />'
                            +                       '</li>'
                            +                   '</ul>'
                            +               '</form>'
                            +           '</div>'
                            +       '</div>';
            $('#user-sign-in').html(registerScreen);
            mod_login.setRecaptchaCode('');
            // Render racaptcha widget.
            if(typeof grecaptcha === 'object' && mod_login.showCaptcha()) {
                recaptchaWidgetId = grecaptcha.render(
                    'recaptcha-container',
                    {
                        "sitekey": "6LcXeh0TAAAAAO1DsEX1iEF8n8-E_hQB67bIpxIw", 
                        "theme": "light",
                        "callback": mod_login.recaptchaResponse,
                        "error-callback": mod_login.recaptchaErrorCallback,
                        "expired-callback": mod_login.recaptchaExpiredCallback
                    }
                )
            }
        };
        
        /**
         * Creates Forgot Password Screen UI and inserts in page
         *
         * @param 
         * @param 
         */
        mod_login.showForgotPasswordScreen = function (callback) {
            var inputVal = $('#toi-login input[name="emailId"]').val();
            var loginType = mod_login.getLoginType();
            var fpScreen = '';
            fpScreen +=  '<div id="toi-forgot-password">'
                        +       '<div class="signin-section">'
                        +           '<h4 class="heading">'
                        +               '<span>Forgot Password</span>'
                        +           '</h4>'
                        +           '<p id="forgot-password-sent">'
                        +               'We have sent a 6 digit verification code ' + (loginType !== 'email'? 'on <strong>+91-': 'to <strong>') + inputVal + '</strong>'
                        +           '</p>'
                        +           '<form class="form" autocomplete="off">'
                        +               '<input type="hidden" id="fp-inputVal" value="' + inputVal + '"/>'
                        +               '<ul>'
                        +                   '<li class="input-field password">'
                        +                       '<p>'
                        +                           '<input type="password" name="otpfp" maxlength="6" placeholder="Enter the verification code"/>'
                        +                       '</p>'
                        +                       '<div class="errorMsg"></div>'
                        +                       '<div class="successMsg"></div>'
                        +                       '<span class="regenerate-otp">Didn\'t receive OTP?</span>'
                        +                       '<a id="sso-fp-regenerate-otp" href="javascript:void(0)" class="secondary-link">Re-Generate OTP</a>'
                        +                   '</li>'
                        +                   '<li class="input-field password">'
                        +                       '<p>'
                        +                           '<input autocomplete="off" type="password" name="registerPwd" placeholder="Enter new password" maxlength="14" />'
                        +                           '<span class="view-password"></span>'
                        +                       '</p>'
                        +                           '<span class="subtext">Should not match last 3 passwords.</span>'
                        +                           '<div class="errorMsg"></div>'
                        +                           '<div class="password-conditions">'
                        +                               '<p>Password must have:</p>'
                        +                               '<ul>'
                        +                                   '<li id="charCnt" class="error">6-14 characters</li>'
                        +                                   '<li id="lwCnt" class="error">1 Lower case character (a-z)</li>'
                        +                                   '<li id="numCnt" class="error">1 Numeric character (0-9)</li>'
                        +                                   '<li id="spclCharCnt" class="error">1 special character (Such as #, $, %, &, !)</li>'
                        +                               '</ul>'
                        +                           '</div>'
                        +                   '</li>'
                        +                   '<li class="submit">'
                        +                       '<input type="submit" id="sso-fp-btn" class="submit-btn disabled" value="Verify & Login" disabled="disabled" />'
                        +                   '</li>'
                        +               '</ul>'
                        +           '</form>'
                        +       '</div>'
                        +   '</div>';
            $('#user-sign-in').html(fpScreen);
        };
        
        /**
         * Creates Verify OTP Screen UI to be shown after Register page and inserts in page
         *
         * @param Mobile will be set when user tries to Register with mobile and email
         * @param emailId will be set once user verifies Mobile. Format email#mobile
         */
        mod_login.showSignUpOtpScreen = function (ssoid, mobile, emailId, callback) {
            var emailIdArr = emailId && emailId.length > 0? emailId.split('#'): [];
            var inputVal = emailIdArr[0] || $('#register-inputVal').val();
            var email = '';
            var pageName = '';
            var loginType = mod_login.getLoginType();
            if(loginType === 'email' && mobile && mobile.length) {
                pageName = 'mobile';
            } else if(emailIdArr && emailIdArr.length > 0) {
                pageName = 'email';
            } else if(loginType === 'email') {
                pageName = 'email';
            } else {
                pageName = 'mobile';
            }
            
            mod_login.setPageName(pageName);
            
            if(mobile && mobile.length) {
                loginType = 'mobile';
                email = inputVal;
                inputVal = mobile;
            }
            var fpScreen = '';
            fpScreen +=  '<div id="toi-verifyotp-password">'
                        +       '<div class="signin-section">'
                        +           '<h4 class="heading">'
                        +               '<span>Complete Your Profile</span>'
                        +           '</h4>';
            if(emailIdArr.length > 0) {
                fpScreen    +=      '<p class="mn-verified">Mobile number verified: <strong>+91-' + emailIdArr[1] + '</strong><i class="tick"></i>'
                            +       '<p>Verify your email id</p>';
            } else {
                fpScreen    +=      '<p>We have sent a 6 digit verification code on your ' + (loginType !== 'email'? 'Mobile Number': 'Email Id') + '</p>';
            }
                        
            fpScreen    +=          '<form class="form" autocomplete="off">'
                        +               '<input type="hidden" id="verify-inputVal" value="' + inputVal + '"/>'
                        +               '<input type="hidden" id="verify-email" value="' + email + '"/>'
                        +               '<input type="hidden" id="verify-ssoid" value="' + ssoid + '"/>'
                        +               '<input type="hidden" id="verify-logintype" value="' + loginType + '"/>'
                        +               '<ul>'
                        +                   '<li class="input-field ' + (loginType !== 'email'? 'mobile-no': 'email') + '">'
                        +                       '<p>'
                        +                           (loginType !== 'email'? '<span class="country-code">+91 - </span>': '')
                        +                           '<input autocomplete="off" type="text" name="verify-emailid" maxlength="100" disabled="disabled" value="' + inputVal  + '" />'
                        +                       '</p>'
                        +                       '<a href="javascript:void(0)" id="changeRegisterEmailId" class="secondary-link">Change Email/Mobile No.</a>'
                        +                   '</li>'
                        +                   '<li class="input-field password">'
                        +                       '<p>'
                        +                           '<input type="password" name="otpverify" maxlength="6" placeholder="Enter the verification code"/>'
                        +                       '</p>'
                        +                       '<div class="errorMsg"></div>'
                        +                       '<div class="successMsg"></div>'
                        +                       '<span class="regenerate-otp">Didn\'t receive OTP?</span>'
                        +                       '<a id="sso-verify-regenerate-otp" href="javascript:void(0)" class="secondary-link">Re-Generate OTP</a>'
                        +                   '</li>'
                        +                   '<li class="submit">'
                        +                       '<input type="submit" id="' + (emailId? 'sso-verify-email-btn': 'sso-verify-btn') + '" class="submit-btn disabled" value="' + (emailId? 'Verify': 'Verify and Login') + '" disabled="disabled" />'
                        +                   '</li>'
                        +               '</ul>'
                        +           '</form>'
                        +           '<div class="mandatory-box"><p>*Email or mobile no. verification is mandatory to complete the registration process.</p></div>'
                        +       '</div>'
                        +   '</div>';
            $('#user-sign-in').html(fpScreen);
        };
        
        mod_login.showSuccessMsgScreen = function (isForgotPassword, data) {
            var successScreen = '';
            successScreen   +=  '<div id="toi-success-screen">'
                            +       '<div class="signin-section">'
                            +           '<h4 class="heading">'
                            +               '<span>' + (isForgotPassword? 'Forgot Password': 'Complete Your Profile') + '</span>'
                            +           '</h4>';
            if(isForgotPassword) {
                successScreen   +=      '<div class="fp-success">'
                                +           '<i class=""></i>'
                                +           '<div class="fp-success-msg">Password changed successfully.</div>'
                                +       '</div>';
            } else {
                successScreen   +=      '<div class="register-success">'
                                +           '<div class="verified">'
                                +               '<div>' + (data.email? 'Email Id': 'Mobile Number') + ' verified:</div>'
                                +               '<div><strong>' + (data.email || data.mobile) + '</strong><i class="tick"></i></div>'
                                +           '</div>'
                                +           '<div class="success-wrapper">'
                                +               '<i class="success-user"></i>'
                                +               '<div class="fp-success-msg">Thank you for registering.</div>'
                                +           '</div>'
                                +       '</div>';
            }
            
            successScreen   +=      '</div>'
                            +   '</div>';
                        
            $('#user-sign-in').html(successScreen);
            
            setTimeout(function(){
                var $loginPopUp = $("#login-popup");
                if($loginPopUp.hasClass('active')) {
                    mod_login.closeBtnHandler();
                }
            }, 5000);
        };
        
        /**
         * Sets recaptcha code once it is validated
         *
         * @param data - Recaptcha string returned in callback
         * @param 
         */
        mod_login.recaptchaResponse = function (data) {
            var $errorElement = $('li.recaptcha-wrapper');
            mod_login.handleError($errorElement);
            mod_login.setRecaptchaCode(data);
        };
        mod_login.recaptchaErrorCallback = function (err) {
            var $errorElement = $('li.recaptcha-wrapper');
            mod_login.handleError($errorElement, errorConfig.serverError);
        };
        mod_login.recaptchaExpiredCallback = function (data) {
            
        };
        /**
         * Sets recaptcha code
         *
         * @param data - Recaptcha string
         * @param 
         */
        mod_login.setRecaptchaCode = function (data) {
            recaptchaCode = data;
        };
        /**
         * returns recaptcha code
         *
         * @param
         * @param 
         */
        mod_login.getRecaptchaCode = function () {
            return recaptchaCode;
        };
        mod_login.logout = function (callback) {
            loginCallback = function () {
                event.publish("user.logout");
                if (callback) {
                    callback();
                }
            };
            reset();
            var logout_url = tLoginObj.ssoUrl+"/sso/identity/profile/logout/external?channel=" + page.getChannel().toLowerCase() + "&ru=" + encodeURIComponent(site_url+"/phpsso/?channel="+tLoginObj.ssochannel+"&status=logout");
            //var logout_url = constants.logoutUrl.replace('{$channel}', page.getChannel().toLowerCase()); 
            var ifr = load.iframe(logout_url);
            $(ifr).load(function () {
                $(ifr).remove();
                mod_login.removeUser();
                if (window.__sso) {
                    window.__sso();
                }
            });
            localstorage.remove("sso_user");
            var domain = page.getDomain();
            var cookieList = [
                {name: 'ssoid', path: '/', domain: domain},
                {name: 'Fbsecuritykey', path: '/', domain: domain},
                {name: 'fbookname', path: '/', domain: domain},
                {name: 'CommLogP', path: '/', domain: domain},
                {name: 'CommLogU', path: '/', domain: domain},
                {name: 'FaceBookEmail', path: '/', domain: domain},
                {name: 'Fbimage', path: '/', domain: domain},
                {name: 'fbooklocation', path: '/', domain: domain},
                {name: 'Fboauthid', path: '/', domain: domain},
                {name: 'fbname', path: '/', domain: domain},
                {name: 'fbLocation', path: '/', domain: domain},
                {name: 'fbimage', path: '/', domain: domain},
                {name: 'fbOAuthId', path: '/', domain: domain},
                {name: 'MSCSAuth', path: '/', domain: domain},
                {name: 'MSCSAuthDetail', path: '/', domain: domain},
                {name: 'MSCSAuthDetails', path: '/', domain: domain},
                {name: 'Twimage', path: '/', domain: domain},
                {name: 'TwitterUserName', path: '/', domain: domain},
                {name: 'Twoauthid', path: '/', domain: domain},
                {name: 'Twsecuritykey', path: '/', domain: domain},
                {name: 'ssosigninsuccess', path: '/', domain: domain},
                {name: 'prc', path: '/', domain: domain},
                {name: 'ssoid'},
                {name: 'MSCSAuthDetail'},
                {name: 'articleid'},
                {name: 'txtmsg'},
                {name: 'tflocation'},
                {name: 'tfemail'},
                {name: 'setfocus'},
                {name: 'fbookname'},
                {name: 'CommLogP'},
                {name: 'CommLogU'},
                {name: 'FaceBookEmail'},
                {name: 'Fbimage'},
                {name: 'fbooklocation'},
                {name: 'Fboauthid'},
                {name: 'Fbsecuritykey'},
                {name: 'fbname'},
                {name: 'fbLocation'},
                {name: 'fbimage'},
                {name: 'fbOAuthId'},
                {name: 'MSCSAuth'},
                {name: 'MSCSAuthDetails'},
                {name: 'ssosigninsuccess'},
                {name: 'Twimage'},
                {name: 'TwitterUserName'},
                {name: 'Twoauthid'},
                {name: 'Twsecuritykey'},
                {name: 'prc'}
            ];
            var counter = 0;

            for(; counter < cookieList.length; counter++) {
                if(cookieList[counter].path) {
                    cookie.remove(cookieList[counter].name, cookieList[counter].path, cookieList[counter].domain);
                } else {
                    cookie.remove(cookieList[counter].name);
                }
            };
        };
        var mod_login_config = {
            check_user_status: function (params, callback) {
                var ssoid = cookie.get("ssoid") || cookie.get("ssoId");
                var MSCSAuthDetails = cookie.get("MSCSAuthDetails");
                if (!ssoid && MSCSAuthDetails) {
                    ssoid = MSCSAuthDetails.split("=")[1];
                }
                if (ssoid && ssoid.length > 0) {
                    var isLoggedInUser = mod_login.getUser();
                    if (isLoggedInUser) {
                        if (callback) {
                            callback(isLoggedInUser);
                        }
                    } else {
                        if (MSCSAuthDetails) {//old cookie is set
                            api.getUsersInfo({ssoids: ssoid}, function (user) {
                                if (callback) {
                                    callback(user[0] || user);
                                }
                            });
                        } else {
                            // set older cookie
                            ajax.getJSONP(tLoginObj.ssoUrl+'/sso/crossdomain/getTicket?version=v1', function (data) {
                                if (data.ticketId != null && data.ticketId != undefined && data.ticketId.length > 0) {
                                    var socialappurl = tLoginObj.socialappsintegrator+'/socialsite/v1validateTicket?ticketId=' + data.ticketId + '&channel=' + page.getChannel().toLowerCase();
                                    // set old cookies
                                    ajax.getJSONP(socialappurl, function (data1) {
                                        if (location.hostname.indexOf(".indiatimes.com") > -1) {
                                            api.getUsersInfo({ssoids: ssoid}, function (user) {
                                                if (callback) {
                                                    callback(user[0] || user);
                                                }
                                            });
                                        } else {
                                            var socialappcdurl = 'http://' + location.hostname + '/v1validateTicket?ticketId=' + data.ticketId + '&channel=' + page.getChannel().toLowerCase();
                                            ajax.getJSONP(socialappcdurl, function (data) {
                                                // get user info from mytimes
                                                api.getUsersInfo({ssoids: ssoid}, function (user) {
                                                    if (callback) {
                                                        callback(user[0] || user);
                                                    }
                                                });
                                            });
                                        }
                                    });
                                } else {
                                    if (callback) {
                                        callback(null);
                                    }
                                }
                            });
                        }
                    }
                } else {
                    if (callback) {
                        callback(null);
                    }
                }
            },
            mapping: {
                "uid": "uid",
                "email": "EMAIL", // map email
                "id": "_id",
                "name": "FL_N",
                "username": "D_N_U",
                "fullName": "FL_N",
                "firstName": "F_N",
                "lastName": "L_N",
                "icon": "tiny",
                "link": "profile",
                "CITY": "CITY",
                "thumb": "thumb",
                "followersCount": "F_C",
                "FE_C": "FE_C",
                "I_U_A": "I_U_A",
                "I_I_L": "I_I_L",
                "badges": "badges",
                "rewards": "rewards",
                "whatsonid": "W_ID",
                "ps": "SUB_U_J"
            }
        };
        mod_login.renderPlugins = function (user) {
            user = user || mod_login.getUser();
            $(function () {
                if (user) {
                    $("[data-plugin='user-isloggedin']").show();
                    $("[data-plugin='user-notloggedin']").hide();
                    $("[data-plugin='user-name']").text(user.getFirstName());
                    $("[data-plugin='user-icon']").attr("src", user.getIcon()); //todo debug data-src, was not working, fix in html also
                    $("[data-plugin='user-thumb']").attr("src", user.getThumb());
                    api.getRewards({
                        uid: user.getUid()
                    }, function (rewards) {
                        if (rewards && rewards.output && rewards.output.user && rewards.output.user.levelName) {
                            $("[data-plugin='user-points']").text(( rewards.output.user.statusPoints ));
                            $("[data-plugin='user-level']").text(( rewards.output.user.levelName ));
                            $("[data-plugin='user-points-wrapper']")
                                .show()
                                .addClass("points_" + rewards.output.user.levelName.toLowerCase());
                        } else {
                            $("[data-plugin='user-points-wrapper']").hide();
                        }
                    });
                } else {
                    $("[data-plugin='user-icon']").attr("src", config.default_user_icon); //todo debug data-src, was not working, fix in html also
                    $("[data-plugin='user-thumb']").attr("src", config.default_user_icon);
                    $("[data-plugin='user-isloggedin']").hide();
                    $("[data-plugin='user-notloggedin']").show();
                }

                $("body").toggleClass("loggedin", !!user);
                $("body").toggleClass("notloggedin", !user);
            });
        };
        
        mod_login.register = function () {
            logger.info("Register event called.");
        };
        mod_login.isLoggedIn = function (callback) {
            ajax.get(config.check_user_status, {}, function (result) {
                var _user = user.getNewUser(result, config.mapping);
                if (_user) {
                    _user.loginType = cookie.get("LoginType");
                    _user.facebook = {
                        name: cookie.get("fbookname"),
                        location: cookie.get("fbooklocation"),
                        image: cookie.get("Fbimage"),
                        email: cookie.get("FaceBookEmail"),
                        oauth: cookie.get("Fboauthid")
                    };
                    _user.twitter = {
                        name: cookie.get("TwitterUserName"),
                        image: cookie.get("Twimage"),
                        oauth: cookie.get("Twoauthid")
                    };
                    mod_login.setUser(_user);
                } else {
                    mod_login.removeUser();
                }
                if (callback) {
                    callback(_user);
                }
            });
        };
        mod_login.removeUser = function (userId) {
            if (config.multiuser) {
                if (userId) {
                    delete userList[userId];
                } else {
                    throw new Error("'userId' is required to remove a user.");
                }
            } else {
                delete userList[single_user_id];
            }
            mod_login.statusChange(null);
        };
        mod_login.setUser = mod_login.addUser = function (_user) {
            if (typeof _user !== 'undefined' && !user.isUser(_user)) {
                throw new Error("Object is not an instance of User, use 'user.getNewUser()' to get a User object.");
            }
            if (config.multiuser) {
                userList[_user.id](_user);
            } else {
                userList[single_user_id] = _user;
            }
            mod_login.statusChange(_user);
        };
        mod_login.getUser = function (userId) {
            if (config.multiuser) {
                return util.extend(true, {}, userList[userId]);
            } else {
                return userList[single_user_id];
            }
        };
        mod_login.statusChange = function (user) {
            logger.info("User Status:" + ( user ? user.toString() : null ));
            event.publish("user.status", user);
            // Refresh Iframes with data-refreshState attr
            mod_login.refreshIframes();
        };

        mod_login.refreshIframes = function () {


            $(window.parent.document).find('iframe[data-refreshstate]').each(function (i, ele) {

                $(ele).attr('src', $(ele).attr('src'))
            })
        };
        mod_login.onStatusChange = function (callback) {
            event.subscribe("user.status", callback);
        };
        mod_login.updateConfig = function (init_config) {
            if (init_config) {
                config = util.extend(true, {}, config, init_config);
            }
        };
        
        /**
         * Callback that calls forgot password API
         *
         * @param
         * @param 
         */
        mod_login.forgotPasswordBtnHandler = function (e) {
            e.preventDefault();
            var jssoObj = mod_login.setAndGetJssoCrosswalkObj(),
                loginType = mod_login.getLoginType(),
                inputVal = $('#fp-inputVal').val(),
                $fpScreen = $('#toi-forgot-password'),
                otp = $fpScreen.find('input[name="otpfp"]').val(),
                password = $fpScreen.find('input[name="registerPwd"]').val(),
                fnCall;
            
            fnCall = (loginType === 'email'? jssoObj.loginEmailForgotPassword: jssoObj.loginMobileForgotPassword);
            if(typeof fnCall === 'function') {
                mod_login.showLoader();
                fnCall.call(jssoObj, inputVal, otp, password, password, mod_login.handleForgotPasswordVerifyCallback);
                mod_login.fireGAEvent(mod_login.getPageName() + '_PW_Verify');  
            }
        };
        
        /**
         * Callback returned by Forgot password API with response
         *
         * @param response - Object
         * @param 
         */
        mod_login.handleForgotPasswordVerifyCallback = function (response) {
            mod_login.hideLoader();
            var $errorElementOtp = $('#toi-forgot-password input[name="otpfp"]').closest('li');
            var $errorElementPass = $('#toi-forgot-password input[name="registerPwd"]').closest('li');
            var loginType = mod_login.getLoginType();
            if(response.code === 200) {
                // $('#user-sign-in').html('').hide();
                mod_login.fireGAEvent( 'Login_Success_' + mod_login.getPageName());
                // $('#login-popup .close-btn').click();
                mod_login.showSuccessMsgScreen(true);
                mod_login.isLoggedIn(loginCallback);
            } else {
                // Reset error and success messages
                mod_login.handleError($errorElementOtp);
                mod_login.handleError($errorElementPass);
                $('.successMsg').hide();
                switch(response.code) {
                    case 414: 
                        mod_login.handleError($errorElementOtp, (loginType === 'email'? errorConfig.wrongOtpEmail: errorConfig.wrongOtp));
                        break;
                    case 415:
                        mod_login.handleError($errorElementOtp, errorConfig.expiredOTP);
                        break;
                    case 416:
                        mod_login.handleError($errorElementOtp, errorConfig.limitExceeded);
                        break;
                    case 418:
                        mod_login.handleError($errorElementPass, errorConfig.matchLastThree);
                        break;
                    case 503:
                        mod_login.handleError($errorElementPass, errorConfig.connectionError);
                        break;
                    default:
                        mod_login.handleError($errorElementPass, errorConfig.serverError);
                        
                }
                mod_login.fireGAEvent('API_Error_' + response.code);
            } 
        };
        
        /**
         * Click handler of Forgot password link on Login Screen.
         *
         * @param
         * @param 
         */
        mod_login.forgotPasswordHandler = function (e) {
            if($('#sso-forgot-pass').hasClass('disabled')) {
                return;
            }
            var $emailId = $('#toi-login input[name="emailId"]');
            var jssoObj = mod_login.setAndGetJssoCrosswalkObj(),
                loginType = mod_login.getLoginType(),
                inputVal = $('#toi-login input[name="emailId"]').val(),
                $errorElement = $('#toi-login li.email'),
                fnCall;
                
            
            if(inputVal.length === 0) {
                mod_login.handleError($errorElement, errorConfig.fpNoEmailOrMobile);
                return;
            } else if(!loginType) {
                mod_login.handleError($errorElement, errorConfig.fpInvalidEmail);
                return;
            }
            
            mod_login.handleError($errorElement);
            inputVal = (loginType === 'email'? mod_login.getValidEmailId(inputVal): mod_login.getValidMobileNumber(inputVal));
            fnCall = (loginType === 'email'? jssoObj.getEmailForgotPasswordOtp: jssoObj.getMobileForgotPasswordOtp);
            if(typeof fnCall === 'function') {
                mod_login.showLoader();
                fnCall.call(jssoObj, inputVal, mod_login.handleForgotPasswordOTPCallback);
            }
            
            mod_login.setPageName(mod_login.getLoginType());
            mod_login.fireGAEvent(mod_login.getPageName() + '_Forgot_PW');
        };
        
        /**
         * Sets error messages on screens
         *
         * @param $errorElement - Parent element within which error messages have to be set
         * @param msg - Error message to be set
         */
        mod_login.handleError = function ($errorElement, msg) {
            if(msg) {
                $errorElement.find('p').addClass('error');
                $errorElement.find('.errorMsg').html(msg).show();
            } else {
                $errorElement.find('p').removeClass('error');
                $errorElement.find('.errorMsg').html('').hide();
            }
        };
        
        /**
         * Callback after sending OTP for Forgot password
         *
         * @param response - Object
         * @param 
         */     
        mod_login.handleForgotPasswordOTPCallback = function (response) {
            mod_login.hideLoader();
            var $errorElement = $('#toi-login li.email');
            if(response && response.code === 200) {
                mod_login.showForgotPasswordScreen();
                var loginType = mod_login.getLoginType();
                mod_login.setScreenName('Forgot_PW');
            } else {
                if([405, 406, 407, 408].indexOf(response.code) !== -1) {
                    mod_login.handleError($errorElement, errorConfig.accountUnregistered);
                } else if(response.code === 503) {
                    mod_login.handleError($errorElement, errorConfig.connectionError);
                } else if (response.code === 416) {
                    mod_login.handleError($errorElement, errorConfig.limitExceeded);
                    $('#sso-regenerate-otp, #sso-generate-otp, #sso-forgot-pass').addClass('disabled');
                } else {
                    mod_login.handleError($errorElement, errorConfig.serverError);
                }
                
                mod_login.fireGAEvent('API_Error_' + response.code);
            }
        };
        
        /**
         * Keyup event handler for Forgot Password page
         *
         * @param response - Object
         * @param 
         */
        mod_login.fpInputKeyupHandler = function (e) {
            var $this = $(this);
            // setTimeout required for paste events.
            setTimeout(function () {
                var $fpScreen = $('#toi-forgot-password');
                var otp = $fpScreen.find('input[name="otpfp"]').val();
                var password = $fpScreen.find('input[name="registerPwd"]').val();
                var $fbBtn = $('#sso-fp-btn');
                var enableFpBtn = true;
                // check if OTP is number and length is 6 and password is valid
                if(!(!isNaN(otp) && otp.length === 6) || !mod_login.isPasswordValid(password)) {
                    enableFpBtn = false;
                }
                
                $fbBtn.prop('disabled', !enableFpBtn);
            	if(enableFpBtn) {
            	    $fbBtn.removeClass('disabled');
            	} else {
            	    $fbBtn.addClass('disabled');
            	}
            	
            	// If keyup is for password field call password error function to handle its errors
            	if($this.attr('name') === 'registerPwd') {
            	    mod_login.passwordErrors.call($this, e);
            	}
            }, 0);
        };
        
        /**
         * Handles Change Email/Mobile link click
         *
         * @param
         * @param 
         */
        mod_login.changeEmailIdHandler = function (e) {
            $('#sso-pwdDiv, #changeEmailIdDiv, #sso-otpLoginDiv, #sso-login-otp-msg').hide();
            $('#toi-login input[name="emailId"]').prop('disabled', false).val('');
            $('#sso-signInButtonDiv input[type="submit"]').prop('disabled', true).addClass('disabled');
            $('.errorMsg, .successMsg').hide();
            $('.error').removeClass('error');
            $('#sso-signInButtonDiv > input').val('Continue');
            $('#sso-pwdDiv input[name="password"]').val('');
            $('#sso-otpLoginDiv input[type="password"]').val('');
            $('#sso-regenerate-otp, #sso-fp-regenerate-otp, #sso-verify-regenerate-otp, #sso-generate-otp, #sso-forgot-pass').removeClass('disabled');
            mod_login.fireGAEvent(mod_login.getPageName() + '_Change');
            mod_login.setScreenName('Login_Screen');
        };
        
        /**
         * Shows login screen when user clicks change email on Register page
         *
         * @param
         * @param 
         */
        mod_login.changeRegisterEmailIdHandler = function (e) {
            mod_login.showLoginScreen();
            mod_login.fireGAEvent( mod_login.getPageName() + '_Change');
        };
        
        /**
         * Handles Email Id/ Mobile input field on Login page
         *
         * @param
         * @param 
         */
        mod_login.handleEmailIdKeyUp = function (e) {
        	var $this = $(this);
        	setTimeout(function (e) {
        	    var val = $this.val(),
            	    checkIsEmail = val.indexOf('@'),
            	    checkIsMobile = !isNaN(val) && val.length >= 10,
            	    enableSignIn = false,
            	    $errorElement = $('#toi-login li.email'),
            	    $ssoSignInInputBtn = $('#sso-signInButtonDiv > input[type="submit"]');
            	    
            	if(checkIsEmail && mod_login.getValidEmailId(val).length > 0) {
            	    mod_login.setLoginType('email');
            	    enableSignIn = true;
            	} else if(checkIsMobile && mod_login.getValidMobileNumber(val).length > 0) {
            	    mod_login.setLoginType('mobile');
            	    enableSignIn = true;
            	} else {
            	    mod_login.setLoginType('');
            	}
            	
            	$ssoSignInInputBtn.prop('disabled', !enableSignIn);
            	mod_login.handleError($errorElement);
            	if(enableSignIn) {
            	    $ssoSignInInputBtn.removeClass('disabled');
            	} else {
            	    $ssoSignInInputBtn.addClass('disabled');
            	}
        	}, 0);
        };
        
        /**
         * API callback of checkUserExists
         *
         * @param response - Response object
         * @param 
         */
        mod_login.checkUserExists = function(response) {
            mod_login.hideLoader();
            var $errorElement = $('#toi-login li.email');
            var $emailId = $('#toi-login input[name="emailId"]');
            var errorMsg = '';
            var loginType = mod_login.getLoginType();
            mod_login.handleError($errorElement);
            if(response && response.code === 200 && response.data) {
                if(response.data.statusCode === 212 || response.data.statusCode === 213) {
                    $('#sso-pwdDiv, #changeEmailIdDiv').show();
                    $('#sso-signInButtonDiv > input').val('Sign In');
                } else if(response.data.statusCode === 205 || response.data.statusCode === 206 || response.data.statusCode === 214 || response.data.statusCode === 215) {
                    mod_login.registerUser();
                    mod_login.setScreenName('Register_New_User');
                } else {
                    $emailId.prop('disabled', false);
                    errorMsg = response.data.statusCode === 216 ? errorConfig.fpInvalidEmailOnly: errorConfig.fpInvalidEmail;
                    mod_login.handleError($errorElement, errorMsg);
                }
            } else {
                $emailId.prop('disabled', false);
                if(response.code === 410) {
                    mod_login.handleError($errorElement, (loginType === 'email'? errorConfig.fpInvalidEmailOnly: errorConfig.fpInvalidMobileOnly));
                } else if(response.code === 503) {
                    mod_login.handleError($errorElement, errorConfig.connectionError);
                } else {
                    mod_login.handleError($errorElement, errorConfig.serverError);
                }
                
                mod_login.fireGAEvent('API_Error_' + response.code);
            }
        };
        
        /**
         * Shows register screen
         *
         * @param
         * @param 
         */
        mod_login.registerUser = function() {
            mod_login.showRegisterScreen();
        };
        
        /**
         * Handles Register button click and validates Register form
         *
         * @param
         * @param 
         */
        mod_login.registerButtonHandler = function (e) {
            e.preventDefault();
            mod_login.registerFormSubmitted(true);
            var $register = $('#toi-register');
            var $email = $register.find('input[name="emailId"]');
            var $fullname = $register.find('input[name="fullname"]');
            var $password = $register.find('input[name="registerPwd"]');
            var $cnfrmPassword = $register.find('input[name="registerCnfrmPwd"]');
            var $mobile = $register.find('input[name="mobile"]');
            var recaptcha = mod_login.getRecaptchaCode();
            var $agree = $register.find('input[name="agree"]');
            var agree = $agree.is(':checked');
            var isSendOffer = $register.find('input[name="promotions"]').is(':checked');
            var sharedDataAllowed = $register.find('input[name="sharedDataAllowed"]').is(':checked')? '1': '0';
            var email = $email.val();
            var fullname = $fullname.val();
            var password = $password.val().trim();
            var cnfrmPassword = $cnfrmPassword.val().trim();
            var mobile = $mobile.val() || '';
            var username = {};
            var validForm = true;
            var loginType = mod_login.getLoginType();
            var jssoObj = mod_login.setAndGetJssoCrosswalkObj(), fnCall;
            var $fullNameParent = $fullname.closest('li');
            var $passwordParent = $password.closest('li');
            var $cnfrmPasswordParent = $cnfrmPassword.closest('li');
            var $mobileParent = $mobile.closest('li');
            var $emailParent = $email.closest('li');
            var $agreeParent = $agree.closest('li');
            var $recaptchaParent = $('#recaptcha-container').closest('li');
            
            var isValidName = mod_login.checkAndSetFullNameError($fullname, $fullNameParent);
            var isValidCnfrmPassword = mod_login.checkAndSetConfirmPasswordError($cnfrmPassword, $cnfrmPasswordParent);
            var isValidEmailOrMobile = true;
            var isTnCAgreed = mod_login.checkAndSetAgreeTnCError($agree, $agreeParent);
            
            if(loginType === 'email') {
                isValidEmailOrMobile = mod_login.checkAndSetEmailOrMobileToRegisterError($mobile, $mobileParent, 'mobile');
            } else {
                isValidEmailOrMobile = mod_login.checkAndSetEmailOrMobileToRegisterError($email, $emailParent, 'email');
            }
            
            if(!isValidName || !isValidCnfrmPassword || !isValidEmailOrMobile || !isTnCAgreed) {
                validForm = false;
            }
            
            if(!mod_login.isPasswordValid(password)) {
                validForm = false;
            }
            
            if(mod_login.showCaptcha()) {
                if(!recaptcha) {
                    validForm = false;
                    mod_login.handleError($recaptchaParent, errorConfig.captchaUnselected);
                } else {
                     mod_login.handleError($recaptchaParent);
                } 
            }
            
            $('.password-conditions').show();
            
            if(validForm) {
                username = mod_login.getFirstAndLastName(fullname);
                // Call registerUser API in case of Opera browser
                fnCall = mod_login.showCaptcha()? jssoObj.registerUserRecaptcha: jssoObj.registerUser;
                if(typeof fnCall === 'function') {
                    mod_login.showLoader();
                    if(mod_login.showCaptcha()) {
                        fnCall.call(jssoObj, username.firstName, username.lastName, '', '', email, mobile, password, isSendOffer, recaptcha, '1', sharedDataAllowed, mod_login.registerUserCallback);
                    } else {
                        fnCall.call(jssoObj, username.firstName, username.lastName, '', '', email, mobile, password, isSendOffer, '1', sharedDataAllowed, mod_login.registerUserCallback);
                    }
                }
            }
            
            mod_login.fireGAEvent( mod_login.getPageName() + '_Verify' );
        };
        
        /**
         * Handles keyup events on register button
         *
         * @param
         * @param 
         */
        mod_login.registerFormErrorHandler = function (e) {
            if(!registerFormSubmitted) {
                return;
            }
            
            var $inputElem = $(e.target);
            var inputFieldName = $inputElem.attr('name');
            var $elemParent = $inputElem.closest('li');
            
            if(inputFieldName === 'fullname') {
                mod_login.checkAndSetFullNameError($inputElem, $elemParent);
            } else if (inputFieldName === 'registerCnfrmPwd') {
                mod_login.checkAndSetConfirmPasswordError($inputElem, $elemParent);
            } else if (inputFieldName === 'emailId') {
                mod_login.checkAndSetEmailOrMobileToRegisterError($inputElem, $elemParent, 'email');
            } else if (inputFieldName === 'mobile') {
                mod_login.checkAndSetEmailOrMobileToRegisterError($inputElem, $elemParent, 'mobile');
            } else if (inputFieldName === 'agree') {
                mod_login.checkAndSetAgreeTnCError($inputElem, $elemParent);
            }
        };
        
        mod_login.checkAndSetFullNameError = function (inputElem, elemParent) {
            var nameRegex = /^[a-zA-Z\s]*$/;
            var fullname = inputElem.val();
            var validField = true;
            
            if(!(fullname && fullname.length > 0 && nameRegex.test(fullname))) {
                validField = false;
                if(fullname.length === 0) {
                    mod_login.handleError(elemParent, errorConfig.emptyName);
                } else {
                    mod_login.handleError(elemParent, errorConfig.wrongName);
                }
            } else {
                mod_login.handleError(elemParent);
            }
            
            return validField;
        };
        
        mod_login.checkAndSetConfirmPasswordError = function (inputElem, elemParent) {
            var password = $('#toi-register input[name="registerPwd"]').val().trim();
            var confirmPassword = inputElem.val().trim();
            var validField = true;
            if(password !== confirmPassword) {
                validField = false;
                mod_login.handleError(elemParent, errorConfig.passwordMismatch);
            } else {
                mod_login.handleError(elemParent);
            }
            
            return validField;
        };
        
        mod_login.checkAndSetEmailOrMobileToRegisterError = function (inputElem, elemParent, loginType) {
            var inputFieldVal = inputElem.val();
            var validField = true;
            
            if(inputFieldVal.length === 0 ) {
                mod_login.handleError(elemParent);
            } else {
                inputFieldVal = loginType === 'email'? mod_login.getValidEmailId(inputFieldVal): mod_login.getValidMobileNumber(inputFieldVal, true);
                if(inputFieldVal.length === 0) {
                    validField = false;
                    mod_login.handleError(elemParent, (loginType === 'email'? errorConfig.wrongEmail: errorConfig.wrongMobile));
                } else {
                    mod_login.handleError(elemParent);
                }
            }
            
            return validField;
        };
        
        mod_login.checkAndSetAgreeTnCError = function (inputElem, elemParent) {
            var tncAgreed = inputElem.is(':checked');
            var validField = true;
            
            if(!tncAgreed) {
                validField = false;
                mod_login.handleError(elemParent, errorConfig.tncNotSelected);
            } else {
                mod_login.handleError(elemParent);
            }
            
            return validField;
        };
        
        /**
         * Handles Verify button click on Verify OTP page
         *
         * @param
         * @param 
         */
        mod_login.verifyButtonHandler = function (e) {
            e.preventDefault();
            var $verifyParent = $('#toi-verifyotp-password');
            var intent = $("#verify-inputVal").val();
            var ssoId = $("#verify-ssoid").val();
            var otp = $verifyParent.find('input[name="otpverify"]').val();
            var actualLoginType = mod_login.getLoginType();
            var loginType = $('#verify-logintype').val();
            var emailId = $('#verify-email').val() || '';
            var jssoObj = mod_login.setAndGetJssoCrosswalkObj();
            var fnCall = (loginType === 'email'? jssoObj.verifyEmailSignUp: jssoObj.verifyMobileSignUp);
            if(typeof fnCall === 'function') {
                mod_login.showLoader();
                fnCall.call(jssoObj, intent, ssoId, otp, mod_login.handleSignUpVerifyCallback((actualLoginType !== loginType && emailId? emailId: ''), ssoId));
                mod_login.fireGAEvent(mod_login.getPageName() + '_Verify');
            }
        };
        
        /**
         * Handles Verify Email button click on Verify OTP page
         *
         * @param
         * @param 
         */
        mod_login.verifyEmailButtonHandler = function (e) {
            e.preventDefault();
            // Duplicate method to handle any different functionality if any
            mod_login.verifyButtonHandler(e);
        };
        
        /**
         * Enable Verify button of valid OTP is entered on Verify OTP page
         *
         * @param
         * @param 
         */
        mod_login.enableVerifyButton = function (e) {
            var $this = $(this);
            setTimeout(function(){
                var otp = $this.val();
                var $submitBtn = $('#sso-verify-btn');
                if(!$submitBtn.is(':visible')) {
                    $submitBtn = $('#sso-verify-email-btn');
                }
                if(!isNaN(otp) && otp.length ===6 ) {
                    $submitBtn.prop('disabled', false).removeClass('disabled');
                } else {
                    $submitBtn.prop('disabled', true).addClass('disabled');
                }
            }, 0);
        };
        
        /**
         * Returns callback for Register User API
         *
         * @param emailId, sso - Sets in case user tries to register with both email and mobile
         * @param response - API response
         */
        mod_login.handleSignUpVerifyCallback = function (emailId, sso, response) {
            return function (response) {
                mod_login.hideLoader();
                var $errorElementOtp = $('#toi-verifyotp-password input[name="otpverify"]').closest('li');
                var loginType = $('#verify-logintype').val();
                var mobile = '';
                var $inputVal = $("#verify-inputVal");
                var verifiedData = {};
                if(response && response.code === 200) {
                    mod_login.fireGAEvent( 'Login_Success_' + mod_login.getPageName());
                    if(!emailId || !sso) {
                        if(loginType === 'email') {
                            verifiedData.email = $inputVal.val();
                        } else {
                            verifiedData.mobile = $inputVal.val();
                        }
                        mod_login.showSuccessMsgScreen(false, verifiedData);
                    } else {
                        mobile = $inputVal.val();
                        mod_login.showSignUpOtpScreen(sso, '', emailId + '#' + mobile);
                    }
                    
                    mod_login.isLoggedIn(loginCallback);
                } else {
                    $('.successMsg').hide();
                    switch(response.code) {
                        case 414: 
                            mod_login.handleError($errorElementOtp, (loginType === 'email'? errorConfig.wrongOtpEmail: errorConfig.wrongOtp));
                            break;
                        case 415:
                            mod_login.handleError($errorElementOtp, errorConfig.expiredOTP);
                            break;
                        case 416:
                            mod_login.handleError($errorElementOtp, errorConfig.limitExceeded);
                            break;
                        case 503:
                            mod_login.handleError($errorElementOtp, errorConfig.connectionError);
                            break;
                        default:
                            mod_login.handleError($errorElementOtp, (errorConfig.serverError));
                            
                    }
                    
                    mod_login.fireGAEvent('API_Error_' + response.code);
                }   
            }
        };
        
        
        mod_login.registerUserCallback = function (response) {
            mod_login.hideLoader();
            var $errorElement = $('#sharedDataAllowed').closest('li');
            var mobile = $('#toi-register input[name="mobile"]').val();
            if(response && response.code === 200) {
                mod_login.showSignUpOtpScreen(response.data.ssoid, mobile);
                mod_login.setScreenName('Complete_Profile');
            } else {
                if(response.code === 429) {
                    mod_login.handleError($errorElement, errorConfig.userAlreadyRegistered);
                } else if (response.code === 416) {
                    mod_login.handleError($errorElement, errorConfig.limitExceeded);
                } else if(response.code === 503) {
                    mod_login.handleError($errorElement, errorConfig.connectionError);
                } else {
                    mod_login.handleError($errorElement, errorConfig.serverError);
                }
                
                if(typeof grecaptcha === 'object' && mod_login.showCaptcha()) {
                    grecaptcha.reset(recaptchaWidgetId);
                }
                
                mod_login.setRecaptchaCode('');
                
                mod_login.fireGAEvent('API_Error_' + response.code);
            }
        };
        
        mod_login.getFirstAndLastName = function (name) {
            var nameArr = [];
            var nameObj = {firstName: '', lastName: ''};
            if(name && name.length > 0) {
                name = name.replace(/  +/g, ' ');
                nameArr = name.split(' ');
                nameObj.firstName = nameArr[0] || '';
                if(nameArr.length > 1) {
                    nameArr.splice(0,1);
                    nameObj.lastName = nameArr.join(' ');
                }
            }
            
            return nameObj;
        };
        
        mod_login.loginWithOTP = function(e, isRegenerate) {
            // Do not perform any action if generate otp is disabled
            if($('#sso-generate-otp').hasClass('disabled')) {
                return;
            }
            
            var $emailId = $('#toi-login input[name="emailId"]');
            var jssoObj = mod_login.setAndGetJssoCrosswalkObj(),
                loginType = mod_login.getLoginType(),
                inputVal = $('#toi-login input[name="emailId"]').val(),
                fnCall;
            
            inputVal = (loginType === 'email'? mod_login.getValidEmailId(inputVal): mod_login.getValidMobileNumber(inputVal));
            fnCall = (loginType === 'email'? jssoObj.getEmailLoginOtp: jssoObj.getMobileLoginOtp);
            if(typeof fnCall === 'function') {
                mod_login.showLoader();
                fnCall.call(jssoObj, inputVal, mod_login.handleLoginOTPCallback(isRegenerate));
                if(isRegenerate) {
                    mod_login.fireGAEvent( mod_login.getPageName() + '_Re_OTP');
                } else {
                    mod_login.fireGAEvent(mod_login.getPageName() + '_OTP_Submit');   
                }
            }
        };
        
        // Duplicate method in case any message needed for regenerate OTP logic        
        mod_login.regenerateLoginOTP = function() {
            // Do not perform any action if regenerate otp is disabled
            if($('#sso-regenerate-otp').hasClass('disabled')) {
                return;
            }
            
            mod_login.loginWithOTP({}, true);
        };
        
        mod_login.fpRegenerateOTP = function () {
            // Do not perform any action if regenerate OTP button is disabled
            if($(this).hasClass('disabled')) {
                return;
            }
            var jssoObj = mod_login.setAndGetJssoCrosswalkObj(),
                loginType = mod_login.getLoginType(),
                inputVal = $('#fp-inputVal').val(),
                fnCall;
  
            fnCall = (loginType === 'email'? jssoObj.getEmailForgotPasswordOtp: jssoObj.getMobileForgotPasswordOtp);
            if(typeof fnCall === 'function') {
                mod_login.showLoader();
                fnCall.call(jssoObj, inputVal, mod_login.handleForgotPasswordRegenerateOTPCallback);
                mod_login.fireGAEvent(mod_login.getPageName() + '_Re_OTP');
            }
        };
        
        mod_login.handleForgotPasswordRegenerateOTPCallback = function (response) {
            mod_login.hideLoader();
            var $errorElement = $('input[name="otpfp"]').parent().parent();
            mod_login.handleError($errorElement);
            if(response && response.code === 200) {
                $('#toi-forgot-password input[name="otpfp"]').val('');
                $('.successMsg').text('OTP has been successfully sent.').show();
            } else {
                $('.successMsg').hide();
                switch(response.code) {
                    case 416:
                        mod_login.handleError($errorElement, errorConfig.limitExceeded);
                        $('#sso-fp-regenerate-otp').addClass('disabled');
                        $('#forgot-password-sent').hide();
                        break;
                    case 503:
                        mod_login.handleError($errorElement, errorConfig.connectionError);
                        break;
                    default:
                        mod_login.handleError($errorElement, errorConfig.serverError);
                    
                }
                
                mod_login.fireGAEvent('API_Error_' + response.code);
            }
        };
        
        mod_login.verifyPageRegenerateOTP = function () {
            if($('#sso-verify-regenerate-otp').hasClass('disabled')) {
                return;
            }
            var jssoObj = mod_login.setAndGetJssoCrosswalkObj(),
                loginType = $('#verify-logintype').val(),
                inputVal = $('#verify-inputVal').val(),
                ssoId = $('#verify-ssoid').val(),
                fnCall;
  
            fnCall = (loginType === 'email'? jssoObj.resendEmailSignUpOtp: jssoObj.resendMobileSignUpOtp);
            if(typeof fnCall === 'function') {
                mod_login.showLoader();
                fnCall.call(jssoObj, inputVal, ssoId, mod_login.handleSignUpVerifyRegenerateOTPCallback);
                mod_login.fireGAEvent(mod_login.getPageName() + '_Re_OTP');
            }
        };
        
        mod_login.handleSignUpVerifyRegenerateOTPCallback = function (response) {
            mod_login.hideLoader();
            var $errorElement = $('#toi-verifyotp-password li.password:visible');
            mod_login.handleError($errorElement);
            if(response && response.code === 200) {
                $('#toi-verifyotp-password input[name="otpverify"]').val('');
                $('.successMsg').text('OTP has been successfully sent.').show();
            } else {
                $('.successMsg').hide();
                switch(response.code) {
                    case 416:
                        mod_login.handleError($errorElement, errorConfig.limitExceeded);
                        $('#sso-verify-regenerate-otp').addClass('disabled');
                        break;
                    case 503:
                        mod_login.handleError($errorElement, errorConfig.connectionError);
                        break;
                    default:
                        mod_login.handleError($errorElement, errorConfig.serverError);
                    
                }
                
                mod_login.fireGAEvent('API_Error_' + response.code);
            }
        };
        
        mod_login.handleLoginOTPCallback = function(isRegenerate) {
            return function (response) {
                mod_login.hideLoader();
                var $errorElement = $('#toi-login li.password:visible');
                mod_login.handleError($errorElement);
                if(response && response.code === 200) {
                    var loginType = mod_login.getLoginType();
                    var inputVal = $('#toi-login input[name="emailId"]').val();
                    inputVal = (loginType === 'email'? mod_login.getValidEmailId(inputVal): mod_login.getValidMobileNumber(inputVal));
                    $('#sso-pwdDiv').hide();
                    $('#sso-otpLoginDiv, #sso-login-otp-msg').show();
                    $('#sso-login-otp-msg > p').text('We have sent a 6 digit verification code ' + (loginType === 'email'? 'to ': 'on +91-') + inputVal);
                    if(isRegenerate) {
                        $('#toi-login input[name="otplogin"]').val('');
                        $('#sso-otpLoginDiv .successMsg').text('OTP has been successfully sent.').show();
                    }
                } else {
                    $('#sso-otpLoginDiv .successMsg').hide();
                    switch(response.code) {
                        case 416:
                            mod_login.handleError($errorElement, errorConfig.limitExceeded);
                            // Disable Regenerate OTP button and remove text message specifying OTP has been sent
                            $('#sso-regenerate-otp, #sso-generate-otp, #sso-forgot-pass').addClass('disabled');
                            $('#sso-login-otp-msg > p').text('');
                            break;
                        case 503:
                            mod_login.handleError($errorElement, errorConfig.connectionError);
                            break;
                        default:
                            mod_login.handleError($errorElement, errorConfig.serverError);
                        
                    }
                    
                    mod_login.fireGAEvent('API_Error_' + response.code);
                }
            }
        };
        
        mod_login.fbLoginHandler = function (e) {
            var callback = function () {
                mod_login.closeBtnHandler();
            };
            
            mod_login.initiateFbLogin(callback);
        };
        
        mod_login.gplusLoginHandler = function () {
            var callback = function () {
                mod_login.closeBtnHandler();
            };
            
            mod_login.initiateGplusLogin(callback);
        };
        
        mod_login.handleLoginCallback = function (response) {
            mod_login.hideLoader();
            var isOtpDivVisible = $('#sso-otpLoginDiv').is(':visible');
            var $errorElement = $('#toi-login li.password:visible');
            var loginType = mod_login.getLoginType();
            if(response && response.code === 200) {
                mod_login.closeBtnHandler();
                mod_login.isLoggedIn(loginCallback);
                mod_login.fireGAEvent('Login_Success_' + mod_login.getPageName());
            } else {
                $('.successMsg').hide();
                switch(response.code) {
                    case 415:
                        mod_login.handleError($errorElement, (!isOtpDivVisible? errorConfig.wrongPassword: errorConfig.expiredOTP));
                        break;
                    case 416:
                        mod_login.handleError($errorElement, errorConfig.limitExceeded);
                        break;
                    case 503:
                        mod_login.handleError($errorElement, errorConfig.connectionError);
                        break;
                    default:
                        mod_login.handleError($errorElement, (!isOtpDivVisible? errorConfig.wrongPassword: (loginType === 'email'? errorConfig.wrongOtpEmail: errorConfig.wrongOtp )));
                        
                }
                
                mod_login.fireGAEvent('API_Error_' + response.code);
            }
        };
        
        mod_login.handleEmailIdClick = function (e) {
            e.preventDefault();
            var $password = $('#sso-pwdDiv input[name="password"]');
            var $otp = $('#sso-otpLoginDiv input[type="password"]');
            var $emailId = $('#toi-login input[name="emailId"]');
            var password = '';
            var $errorMsgElem = $('#toi-login li.password:visible .errorMsg');
            $emailId.prop('disabled', true);
            var jssoObj = mod_login.setAndGetJssoCrosswalkObj(),
                loginType = mod_login.getLoginType(),
                inputVal = $('#toi-login input[name="emailId"]').val(),
                fnCall;
            
            inputVal = (loginType === 'email'? mod_login.getValidEmailId(inputVal): mod_login.getValidMobileNumber(inputVal));
            
            if($password.is(':visible') || $otp.is(':visible')) {
                fnCall = (loginType === 'email'? jssoObj.verifyEmailLogin: jssoObj.verifyMobileLogin);
                password = $password.is(':visible')? $password.val(): $otp.val();
                mod_login.fireGAEvent(mod_login.getPageName() + ($password.is(':visible')? '_PW': '_OTP') + '_Entry');
                if(password.length === 0) {
                    $errorMsgElem.html(errorConfig.emptyPassword).show();
                    return;
                } else if(typeof fnCall === 'function') {
                    $('.errorMsg').html('').hide();
                    mod_login.showLoader();
                    fnCall.call(jssoObj, inputVal, password, mod_login.handleLoginCallback);
                }
            } else {
                if(typeof jssoObj.checkUserExists === 'function') {
                    mod_login.showLoader();
                    jssoObj.checkUserExists(inputVal, mod_login.checkUserExists);
                    mod_login.setPageName(loginType);
                    mod_login.fireGAEvent(mod_login.getPageName() + '_Continue');
                } else {
                    $emailId.prop('disabled', false);
                }
            }
        };
        
        mod_login.getValidEmailId = function (email) {
            var regEmail = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,6})$/;
            var emailId = '';
            if(regEmail.test(email)) {
                emailId = email;
            }
            
            return emailId;
        };
        
        // withoutPrefix parameter is needed to check for valid number without +91 or 0 appended
        mod_login.getValidMobileNumber = function (mobile, withoutPrefix) {
            var regMobile = (withoutPrefix? /^[789]\d{9}$/ : /^(\+91)?[0]?[789]\d{9}$/);
            var notAllowedNumbers = ['7777777777', '8888888888', '9999999999'];
            var mobileNo = '';
            var length = mobile.length;
            if(regMobile.test(mobile)) {
                mobileNo = mobile.substring(mobile.length - 10, mobile.length);
            }
            
            if(notAllowedNumbers.indexOf(mobileNo) !== -1) {
                mobileNo = '';
            }
            
            return mobileNo;
        };
        
        mod_login.closeModalOnEscapeKeyPress = function (e) {
            if(!cachedElements.loginPopup.hasClass('active')) {
                return;
            }
        	var keyCode = e.keyCode || e.which;
        	if(keyCode === 27) {
        		cachedElements.closeBtn.click();
        	}
        };
        
        mod_login.isPasswordValid = function (password){
        	return password && password.length >= 6 && password.length <= 14 && mod_login.hasNumber(password) && mod_login.hasSpecialCharacters(password) && mod_login.hasLowerCase(password);
        };
        
        mod_login.hasLowerCase = function (str) {
            return (/[a-z]/.test(str));
        };
        
        mod_login.hasNumber = function (str) {
            return (/[0-9]/.test(str));
        };
        
        mod_login.hasSpecialCharacters = function (str) {
            return (/[!@#$%^&*()]/.test(str));
        };
        
        mod_login.passwordErrors = function (e) {
            var $this = $(this);
            setTimeout(function(){
                var password= $this.val();
            	if(password.length < 6 || password.length > 14){
            		$("#charCnt").removeClass('success').addClass('error');
            	}else{
            		$("#charCnt").removeClass('error').addClass('success');
            	}
            	if(mod_login.hasLowerCase(password)){
            		$("#lwCnt").removeClass('error').addClass('success');
            	}else{
            		$("#lwCnt").removeClass('success').addClass('error');
            	}
            	if(mod_login.hasNumber(password)){
            		$("#numCnt").removeClass('error').addClass('success');
            	}else{
            		$("#numCnt").removeClass('success').addClass('error');
            	}
            	if(mod_login.hasSpecialCharacters(password)){
            		$("#spclCharCnt").removeClass('error').addClass('success');
            	}else{
            		$("#spclCharCnt").removeClass('success').addClass('error');
            	}
            }, 0);
        	
        	// return validPassword;
        };
        
        mod_login.showPassword = function (e) {
            var $this = $(this);
            $this.prev().attr('type', 'text');
            $this.removeClass('view-password').addClass('hide-password');
        };
        
        mod_login.hidePassword = function (e) {
            var $this = $(this);
            $this.prev().attr('type', 'password');
            $this.removeClass('hide-password').addClass('view-password');
        };
        
        mod_login.showPasswordCondition = function (e) {
            e.stopPropagation();
            $('.password-conditions').show();
        };
        
        mod_login.stopEventProp = function (e) {
            e.stopPropagation();  
        };
        
        mod_login.setLoginType = function (type) {
            ssoLoginType = type;
        }
        
        mod_login.getLoginType = function () {
            return ssoLoginType;
        }
        
        mod_login.setPageName = function(loginType) {
            pageName = (loginType === 'email'? 'Email': 'MobNo');
        }
        
        mod_login.getPageName = function() {
            return pageName;
        }
        
        mod_login.setScreenName = function (name) {
            screenName = name;
        };
        
        mod_login.getScreenName = function (name) {
            return screenName;
        };
        
        mod_login.setAndGetJssoCrosswalkObj = function () {
            var jssoObj = {};
            if(typeof jssoCrosswalkObj === 'object') {
                jssoObj = jssoCrosswalkObj;
            } else if (typeof JssoCrosswalk === 'function') {
                jssoCrosswalkObj = new JssoCrosswalk(tLoginObj.ssochannel, 'web');
                jssoObj = jssoCrosswalkObj;
            }
            
            return jssoObj;
        }
        
        mod_login.showLoader = function() {
            $('#user-sign-in').addClass('loader');
        }
        
        mod_login.hideLoader = function() {
            $('#user-sign-in').removeClass('loader');
        }
        
        mod_login.closeBtnHandler = function() {
            cachedElements.loginPopup.removeClass('active');
            $('body').removeClass('disable-scroll');
            if(typeof grecaptcha === 'object' && $('#toi-register').is(':visible') && mod_login.showCaptcha()) {
                grecaptcha.reset(recaptchaWidgetId);   
            }
        };
        
        mod_login.init = function (init_config) {
            var initCallback = function() {
                cachedElements.loginPopup.show();
                mod_login.updateConfig(init_config);
                if (config.renderer === true) {
                    mod_login.onStatusChange(function (_user) {
                        mod_login.renderPlugins(_user);
                    });
                }
                mod_login.isLoggedIn(function () {
                });
                mod_login.initActions();
            }
            
            TimesApps.checkGdprAndCall(initCallback, mod_login.logout);
        };
        
        mod_login.initActions = function () {
            cachedElements.closeBtn
                .on("click", function() {
                    mod_login.closeBtnHandler();
                    mod_login.fireGAEvent('Close');
                });
            
            $("[data-plugin='user-isloggedin']")
                .on("click", "[data-plugin='user-logout']", function () {
                    mod_login.logout();
                });
            $("[data-plugin='user-notloggedin']")
                .on("click", "[data-plugin='user-login']", function () {
                    
                    $('body').addClass('disable-scroll');
                    cachedElements.loginPopup.addClass('active');
                    mod_login.showLoginScreen();
                    mod_login.setScreenName('Login_Screen');
                    mod_login.fireGAEvent('Load');
                })
                .on("click", "[data-plugin='user-register']", function () {
                    mod_login.register();
                })
                .on("click", "[data-plugin='user-login-facebook']", function () {
                    mod_login.loginWithFacebook();
                })
                .on("click", "[data-plugin='user-login-twitter']", function () {
                    mod_login.loginWithTwitter();
                })
                .on("click", "[data-plugin='user-login-google']", function () {
                    mod_login.loginWithGoogle();
                });
                
            $(document).off('keyup').on('keyup', mod_login.closeModalOnEscapeKeyPress);
            
            $("#user-sign-in")
                .off('keyup paste', '#toi-login input[name="emailId"]').on('keyup paste', '#toi-login input[name="emailId"]', mod_login.handleEmailIdKeyUp)
                .off('click', '#sso-signInButtonDiv input[type="submit"]').on('click', '#sso-signInButtonDiv input[type="submit"]', mod_login.handleEmailIdClick)
                .off('submit', '#toi-login form').on('submit', '#toi-login form', mod_login.handleEmailIdClick)
                .off('click', '#changeEmailIdDiv').on('click', '#changeEmailIdDiv', mod_login.changeEmailIdHandler)
                .off('click', '#changeRegisterEmailId').on('click', '#changeRegisterEmailId', mod_login.changeRegisterEmailIdHandler)
                .off('click', '#sso-forgot-pass').on('click', '#sso-forgot-pass', mod_login.forgotPasswordHandler)
                .off('click', '#sso-fb-login').on('click', '#sso-fb-login', mod_login.fbLoginHandler)
                .off('click', '#sso-gplus-login').on('click', '#sso-gplus-login', mod_login.gplusLoginHandler)
                .off('click', '#sso-generate-otp').on('click', '#sso-generate-otp', mod_login.loginWithOTP)
                .off('click', '#sso-regenerate-otp').on('click', '#sso-regenerate-otp', mod_login.regenerateLoginOTP)
                .off('click', '#sso-fp-regenerate-otp').on('click', '#sso-fp-regenerate-otp', mod_login.fpRegenerateOTP)
                .off('click', '#sso-verify-regenerate-otp').on('click', '#sso-verify-regenerate-otp', mod_login.verifyPageRegenerateOTP)
                .off('click', '#sso-registerBtn').on('click', '#sso-registerBtn', mod_login.registerButtonHandler)
                .off('submit', '#toi-register form').on('submit', '#toi-register form', mod_login.registerButtonHandler)
                .off('click', '#sso-verify-btn').on('click', '#sso-verify-btn', mod_login.verifyButtonHandler)
                .off('click', '#sso-verify-email-btn').on('click', '#sso-verify-email-btn', mod_login.verifyEmailButtonHandler)
                .off('submit', '#toi-verifyotp-password form').on('submit', '#toi-verifyotp-password form', mod_login.verifyButtonHandler)
                .off('click', '#sso-fp-btn').on('click', '#sso-fp-btn', mod_login.forgotPasswordBtnHandler)
                .off('submit', '#toi-forgot-password form').on('submit', '#toi-forgot-password form', mod_login.forgotPasswordBtnHandler)
                .off('focus', 'input[name="registerPwd"]').on('focus', 'input[name="registerPwd"]', mod_login.showPasswordCondition)
                .off('keyup paste', '#toi-register input[name="registerPwd"]').on('keyup paste', '#toi-register input[name="registerPwd"]', mod_login.passwordErrors)
                .off('keyup paste', '#toi-register input[type="text"]').on('keyup paste', '#toi-register input[type="text"]', mod_login.registerFormErrorHandler)
                .off('keyup paste', '#toi-register input[name!="registerPwd"][type="password"]').on('keyup paste', '#toi-register input[name!="registerPwd"][type="password"]', mod_login.registerFormErrorHandler)
                .off('change', '#toi-register input[name="agree"]').on('change', '#toi-register input[name="agree"]', mod_login.registerFormErrorHandler)
                // .off('focus blur', '[placeholder]:not(input[name="registerPwd"])').on('focus blur', '[placeholder]:not(input[name="registerPwd"])', mod_login.stopEventProp)
                .off('focus blur', '[placeholder]').on('focus blur', '[placeholder]', mod_login.stopEventProp)
                .off('keyup paste', '#toi-forgot-password input').on('keyup paste', '#toi-forgot-password input', mod_login.fpInputKeyupHandler)
                .off('keyup paste', '#toi-verifyotp-password input[name="otpverify"]').on('keyup paste', '#toi-verifyotp-password input[name="otpverify"]', mod_login.enableVerifyButton)
                .off('click', '.view-password').on('click', '.view-password', mod_login.showPassword)
                .off('click', '.hide-password').on('click', '.hide-password', mod_login.hidePassword)
        };
        
        cachedElements.loginPopup.off('click').on('click', function(e){
           if(e.srcElement && e.srcElement.id === 'login-popup') {
               cachedElements.closeBtn.click();
           }
        });
        mod_login.updateConfig(mod_login_config);
        return mod_login;
    });
    
    define( 'login',[ "tiljs/apps/times/usermanagement" ],
	function ( login) {

		return login;
	} );


define('index',["./preload",
        "./config",
        "tiljs/compatibility",
        "login",
        "./api",
        "tiljs/plugin/dynamic",
        "./rodate",
        "tiljs/plugin/lazy",
        "tiljs/event",
        "tiljs/util",
        "./comments",
        "./homepage",
        "./crwdcnctrl",
        "tiljs/social/facebook",
        "tiljs/social/twitter",
        "tiljs/social/pinterest",
        "tiljs/social/linkedin",
        "tiljs/social/googleplus",
        "tiljs/plugin/pre_event",
        //"personalisation" ,
        "pgtrack",
        "localstoragec",
        "global",
        "tiljs/load",
        "jquery",
        "tiljs/apps/times/authorcomments",
        "./nlwidget",
        "./tpwidget",
        "onetapsignin"
    ],
    function (
        preload,
        config,
        compatibility,
        login,
        api,
        dynamic,
        rodate,
        lazy,
        event,
        util,
        comments,
        homepage,
        crwdcnctrl,
        facebook,
        twitter,
        pinterest,
        linkedin,
        googleplus,
        pevent,
        //personalisation,
        pgtrack,
        localstoragec,
        global,
        load,
        $,
        authorcomments,
        nlwidget,
        tpwidget,
        onetapsignin) {



        setTimeout( function () {
            login.init();
        }, 1000 );
        window.toicommonjs = true;
        

        /*Login through mytimes widget*/
        try{
            $(document).on('click','[data-action="login"]',function(){
                login.login();
                return false;
            });
        }catch(e){}
        
        onetapsignin.init();
        
        
        /********************************************/
        //pointsEarned.run();

        /************************Disable img right click******************************/
        $(document).off('contextmenu').on('contextmenu','img', function(e) {
            return false;
        });
        /*****************************************************************************/

// Global Sign In function
        TimesApps.SignIn = function(){
            require(['tiljs/apps/times/usermanagement'],function(login){
                login.login();
            });

        }
        
    } );

require(["index"]);
