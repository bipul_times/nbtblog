if(typeof colombia == 'undefined')
{
	var colombia = colombia || {};
	colombia.fns = colombia.fns || [];
	(function() {
		  var cads = document.createElement("script");
		  cads.async = true;
		  cads.type = "text/javascript";
		  cads.src = "https://static.clmbtech.com/ad/commons/js/colombia_v2.js";
		  var node = document.getElementsByTagName("script")[0];
		  node.parentNode.insertBefore(cads, node);
	})();
}


	
function adwidget(data,container)
	{
	 if (data != null)
	 {
	  var conObj = document.getElementById(container);
	  var conText= "";
	  var callToAction = "";
	   if(data.hasOwnProperty('paidAds')){
		 if(document.getElementById(container).getAttribute("data-separate-container") != null) { conText += '<div class="paidAds">'; }
		 if(document.getElementById(container).getAttribute("data-paidad-head") != null) { conText += '<div class="ctnad_head">'+document.getElementById(container).getAttribute("data-paidad-head")+'</div>'; }
		  for(var i=0; i<data.paidAds.length; i++) 
		   {
			if(data.paidAds[i].itemType==2 && data.paidAds[i].ctatext!="") {
				 callToAction = "<input class='callactbutton' type='button' value='"+data.paidAds[i].ctatext+"'onclick=window.open('"+data.paidAds[i].clk[0]+"')>";
				}
			 conText+='<div class="colombiaAds '+((i==0)?'first':'')+'"><a target="_blank" href="' + data.paidAds[i].clk[0] + '" title="' + data.paidAds[i].title + '"><img class="lazy" src="' + data.paidAds[i].mainimage + '" alt="' + data.paidAds[i].title + '"></a><a target="_blank" href="' + data.paidAds[i].clk[0] + '">' + data.paidAds[i].title + '</a><p class="sponsored">' + data.paidAds[i].brandtext + '</p><p class="adsDesc">' + data.paidAds[i].text + '</p></div>';
			 conText+=callToAction;	
		  }
		 if(document.getElementById(container).getAttribute("data-separate-container") != null) { conText += '</div>'; }
	  }
	  if(data.hasOwnProperty('organicAds')){
		if(document.getElementById(container).getAttribute("data-separate-container") != null) { conText += '<div class="organicAds">'; } else if(document.getElementById(container).getAttribute("data-clear-float") != null) { conText += '<div style="clear:both;"></div>'; }
		if(document.getElementById(container).getAttribute("data-organicad-head") != null) { conText += '<div class="ctnad_head organicads">'+document.getElementById(container).getAttribute("data-organicad-head")+'</div>'; }
		  for(var j=0; j<data.organicAds.length; j++) 
		   {
			 conText+='<div class="colombiaAds '+((j==0)?'first':'')+'"><a target="_blank" href="' + data.organicAds[j].clk[0] + '" title="' + data.organicAds[j].title + '"><img class="lazy" src="' + data.organicAds[j].mainimage + '" alt="' + data.organicAds[j].title + '"></a><a target="_blank" href="' + data.organicAds[j].clk[0] + '">' + data.organicAds[j].title + '</a><p class="sponsored">' + data.organicAds[j].brandtext + '</p><p class="adsDesc">' + data.organicAds[j].text + '</p></div>';
		  }
		if(document.getElementById(container).getAttribute("data-separate-container") != null) { conText += '</div>'; }
	  }
	conObj.innerHTML = conText;
	conObj.style.display = '';
   }
}
