$.fn.blogsInfiniteScroll = function(options) {
	/**
	 * Settings
	*/
	var isMobile = window.matchMedia("only screen and (max-width: 760px)");
	var windowHeight = (typeof window.outerHeight !== "undefined") ? Math.max(window.outerHeight, $(window).height()) : $(window).height(),
	defaults = {
		contentsWrapperSelector: "#contentsWrapper",
		contentSelector: ".content",
		msidSelectorAttr: "data-id",
		nextSelector: "#next",
		processFunction: "",
		articleDivider: "<hr />",
		contentLimit: 10,
		duplicateElements: [],
		loadImage: "ajax-loader.gif",
		offset: windowHeight,
	}, settings = $.extend(defaults, options);

	/**
	 * Private methods
	*/
	var generateHiddenSpans = function(_msid, _title, _path) {
		return "<span class='hidden-article-data' style='display:none' data-msid='" + _msid + "' data-title='" + _title + "' data-url='" + _path + "'></span>";
	},
	setTitleAndHistory = function(_title, _path) {
		// Set history
		history.pushState(null, _title, _path);
		// Set title
		$("title").html(_title);
	},
	changeTitleAndURL = function(_value) {
		$(".bis-load-img").remove();
		// value is an element of a content user is seeing
		// Get title and path of the article page from hidden span elements
		var title = $(".hidden-article-data[data-msid='"+_value+"']").attr('data-title'),
			path = $(".hidden-article-data[data-msid='"+_value+"']").attr('data-url');
		if($("title").text() !== title) {
			// If it has changed
			$(settings.contentSelector).removeClass("currentArticle");
			$(settings.contentSelector + '['+settings.msidSelectorAttr+'="'+_value+'"]').addClass("currentArticle");
			setTitleAndHistory(title, path);
			if(settings.processFunction !== "") {
				var fn = window[settings.processFunction];
				if( typeof fn  === "function"){
					fn(_value, title, path);
				}
			}
		}

		if(window._euuser){
			enablegdpr();
		}
	};

	/**
	 * Initialize
	*/
	// Get current page's title and URL.
	var msid = $(settings.contentSelector + ":first").attr(settings.msidSelectorAttr),
		title = $("title").text(),
		path = $(location).attr("href"),
		documentHeight = $(document).height(),
		threshold = settings.offset,
		$contents = $(settings.contentSelector);
	// Set hidden span elements and history
	$(settings.contentSelector + ":last").append(generateHiddenSpans(msid, title, path));
	setTitleAndHistory(title, path);
	$(settings.contentSelector + ":first").addClass("currentArticle");

	/**
	 * scroll
	*/
	$(window).scroll(function() {
		require(['tiljs/ui'], function(ui) {
			var newId;
			$contents.each(function(key, value) {
				if (ui.inView($(value), true)) {
					newId = $(value).attr(settings.msidSelectorAttr);
					if (ui.inView($(value).find("[data-articlepage-end]"), true)) {
						$(value).find('.commentwrapperv').addClass("maxbottom");
						$(value).find('.commentwrapperv').removeClass("sticky");
					} else if (ui.inView($('.currentArticle').find("[data-articlepage-start]"), true)) {
						$(value).find('.commentwrapperv').removeClass("maxbottom");
						$(value).find('.commentwrapperv').removeClass("sticky");
					} else {
						$(value).find('.commentwrapperv').removeClass("maxbottom");
						$(value).find('.commentwrapperv').addClass("sticky");
					}
				} else {
					$(value).find('.commentwrapperv').addClass("maxbottom");
					$(value).find('.commentwrapperv').removeClass("sticky");
				}
			});
			// Change title and URL
			changeTitleAndURL(newId);
		});

		if($(window).scrollTop() + windowHeight + threshold >= documentHeight) {
			// If scrolling close to the bottom

			// Getting URL from settings.nextSelector
			var $url = [$(settings.nextSelector).attr("href")];
			$(settings.nextSelector).remove();
			if($url[0] !== undefined && $(settings.contentSelector).length < settings.contentLimit) {
				// If the page has link, call ajax
				if(settings.loadImage !== "") {
					$(settings.contentsWrapperSelector).append("<img src='" + settings.loadImage + "' class='bis-load-img'>");
				}
				$.ajax({
					url: $url[0],
					dataType: "html",
					success: function(res) {
						// Get msid, title and URL
						msid = $(res).find(settings.contentSelector).attr(settings.msidSelectorAttr);
						title = $(res).filter("title").text();
						path = $url[0];
						$res = $(res).find(settings.contentSelector);
						$.each(settings.duplicateElements, function( index, value ) {
							$res.find(value).remove();
						});
						if(($(settings.contentSelector).length + 1) == settings.contentLimit){
							$res.find(".keepscrolling").remove();
						}
						if (isMobile.matches) {
							$res.find("[data-articlepage-start]").after(settings.articleDivider);
						} else {
							$res.find("[data-articlepage-start]").after('<div class="ad728 ATF_728_'+msid+'"></div>' + settings.articleDivider);
						}
						// Set hidden span elements and history
						$(settings.contentsWrapperSelector).append($res.append(generateHiddenSpans(msid, title, path)));
						if($(res).find(settings.contentSelector).find(settings.nextSelector).length === 0){
							//If there is no nextSelector in the contentSelector, get next Slecter from response and append it.
							$(settings.contentsWrapperSelector).append($(res).find(settings.nextSelector));
						}
						documentHeight = $(document).height();
						$contents = $(settings.contentSelector);
						$(".bis-load-img").remove();
					}
				});
			}
		}
	}); //scroll

	return (this);
};

var facebookktitle, facebooksyn, facebooklink, fb_Img;

$(document).ready(function() {

	require(["tiljs/event"],function(event){
		event.subscribe("comment.post.end",function(){
			$(".comment-section .noComment").hide();
		});
		event.subscribe("user.logout",function(){
			$( "[data-plugin='user-thumb']" ).attr( "src", "https://timesofindia.indiatimes.com/photo/29251859.cms" );
		});
	})
	
	$("body").on("click", ".keepscrolling", function() {
		var a = $(this).attr("data-nxtmsid");
		$(window).scrollTop($("[data-articlemsid='" + a + "']").offset().top)
	})
	
	$("body").on("focus", '[data-plugin="comment-input"]', function() {
		var currentMsid = $(this).parents('.comment-section').attr('id').replace('comment-section-', '')
		facebookktitle = eval("facebookktitle_" + currentMsid);
		facebooksyn = eval("facebooksyn_" + currentMsid);
		facebooklink = eval("facebooklink_" + currentMsid);
		fb_Img = eval("fb_Img_" + currentMsid);
	})
	
	if(getUrlParameter('source') != 'app' && 1==2){
		$('#contentsWrapper').blogsInfiniteScroll({
			contentsWrapperSelector: '#contentsWrapper',
			contentSelector: '.aShow',
			msidSelectorAttr: 'data-articlemsid',
			nextSelector: 'a[rel="prev"]',
			processFunction: 'processArticle',
			articleDivider: '<div class="article_divider">Next Post</div>',
			contentLimit: 10,
			duplicateElements: ['#comment_tmpl', '#badge_tmpl', '#ctn_rhs_vid', '#ctn_rhs_paid', '#ctn_rhs_organic', '#ctn_belowarticle_2_paid', '#ctn_belowarticle_2_organic', '#ctn_belowarticle_1_paid', '#ctn_belowarticle_1_organic'],
			loadImage: 'https://timesofindia.indiatimes.com/photo/29439462.cms'
		});
	}else{
		$('a[rel="prev"]').remove();
	}
	//bindCommentSectionSlider($('.aShow:eq(0)').attr('data-articlemsid'));
	processComments($('.aShow:eq(0)').attr('data-articlemsid'));
	photoSwipeGallery($('.aShow:eq(0)').attr('data-articlemsid'));

});

function processArticle(msid, title, url){
	var el = $(".aShow[data-articlemsid='"+msid+"']");
	
	var isSocialLoaded = typeof el.attr('data-social-loaded') !='undefined' ?  el.attr('data-social-loaded') : '';
	if(!isSocialLoaded){
		el.find('.social-likes').socialLikes();
		el.attr('data-social-loaded',true);
	}
	$(".bis-load-img").remove();
	processComments(msid);
	//photoSwipeGallery(msid);
	
	// Load Google ads
	initGoogleAds(eval("wapads_" + msid));
	
	// Load mitr & DMP js
	require(["tiljs/load"],function(load){
		if($("#mitr-js").length){
			var mitrJs = $("#mitr-js").attr('src');
			$("#mitr-js").remove();
			load.js(mitrJs, function () {},'mitr-js');
		}
		if($("#dmp-js").length){
			var dmpJs = $("#dmp-js").attr('src');
			$("#dmp-js").remove();
			load.js(dmpJs, function () {},'dmp-js');
		}
	});
	
	// GA Call
	if (typeof(ga) == "function") {
		ga('send', {
			'hitType': 'pageview',
			'page': url.replace(window.location.protocol + "//" + window.location.hostname, ''),
			'title': title,
			'location': url
		});
	}
	
	// Comscore Call
	if (typeof(fireComscore) == "function") {
		fireComscore();
	}
	
	// ChartBeat Call
	if (typeof(pSUPERFLY) !="undefined" && typeof(pSUPERFLY.virtualPage) == "function") {
		_sf_async_config.sections = $('[data-articlemsid="'+msid+'"]').find('.media-meta span.cat:eq(0)').text();
		_sf_async_config.authors = $('[data-articlemsid="'+msid+'"]').find('.media-meta a.author').text();
		pSUPERFLY.virtualPage(url, title);
	}
	
	// iBeat Call
	/*(function() {
		setTimeout(function() {
			if (typeof setPageConfig == "function") {
				setPageConfig(_ibeat_config[msid]);
			}
			if (typeof iBeatPgTrend != "undefined") {
				iBeatPgTrend.init();
			}
		}, 1000);
	})();*/
}

function moreAuthor(elemId, isMore, isMobile){
	if(isMobile == 0){
		var elemPrefix = '';
	} else {
		var elemPrefix = 'mb-';
	}
	if(isMore == 'more'){
		$("#" + elemPrefix + "showmore-" + elemId).show();
		$("#" + elemPrefix + "showless-" + elemId).hide();	
		$("#" + elemPrefix + "less-button-" + elemId).show();
		$("#" + elemPrefix + "more-button-" + elemId).hide();		
	} else {
		$("#" + elemPrefix + "showmore-" + elemId).hide();
		$("#" + elemPrefix + "showless-" + elemId).show();
		$("#" + elemPrefix + "less-button-" + elemId).hide();
		$("#" + elemPrefix + "more-button-" + elemId).show();
	}
	return false;	
}

function getParameterByName(name) {
	var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
	return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
}
function stripslashes(str) {
  return (str + '')
	.replace(/\\(.?)/g, function(s, n1) {
	  switch (n1) {
	  case '\\':
		return '\\';
	  case '0':
		return '\u0000';
	  case '':
		return '';
	  default:
		return n1;
	  }
	});
}

function processComments(id){
	window.msid = id;
	window.toicommonjs = true;
	window.cmsid = '';
	
	var el = $(".aShow[data-articlemsid='"+id+"']");
	var isCommentsLoaded = typeof el.attr('data-comments-loaded') !='undefined' ?  el.attr('data-comments-loaded') : '';
	if(isCommentsLoaded){
		return false;
	}
	el.attr('data-comments-loaded',true);
	
	var isMobile = window.matchMedia("only screen and (max-width: 760px)");
	if (isMobile.matches && window.getParameterByName('comments') != 'show') {
		if(window.location.hostname.indexOf('readerblog') >= 0){
			$.getJSON(site_url + '/wp-admin/admin-ajax.php?action=commentsdatamobile&msid='+id+'&curpg=1&pcode=TOI', function(jd) {
				if(typeof(jd[0]) == "undefined" || typeof(jd[1]) == "undefined" || jd[0].totalcount == "0"){
					$('#mobilecomment-' + id).remove();
					$('#writeacomment-' + id).show();
					$( "[data-plugin='comment-count-"+id+"']" ).text('0');
				} else {
					$( "[data-plugin='comment-count-"+id+"']" ).text(jd[0].totalcount);
					$( "[data-plugin-count='"+id+"']" ).text(jd[0].totalcount);
					$( "[data-plugin='upvalue-comment-"+id+"']" ).html(stripslashes(jd[1].C_T));
					$( "[data-plugin='upvalue-user-"+id+"']" ).text(jd[1].A_D_N);
				}
			});
		} else {
			$.getJSON(site_url + '/wp-admin/admin-ajax.php?action=commentsdatamobile&msid='+id+'&curpg=1&pcode=TOI', function(jd) {
				if(typeof(jd.rothrd) == "undefined" || typeof(jd.rothrd.opctr) == "undefined" || jd.rothrd.opctr == "0"){
					$('#mobilecomment-' + id).remove();
					$('#writeacomment-' + id).show();
					$( "[data-plugin='comment-count-"+id+"']" ).text('0');
				} else {
					$( "[data-plugin='comment-count-"+id+"']" ).text(jd.rothrd.opctr);
					$( "[data-plugin-count='"+id+"']" ).text(jd.rothrd.opctr);
					var cData = jd.rothrd.op;
					if($.isArray(cData)) {
						var cData = cData[0];
					}
					$( "[data-plugin='upvalue-comment-"+id+"']" ).html(stripslashes(cData.optext));
					$( "[data-plugin='upvalue-user-"+id+"']" ).text(cData.roaltdetails.fromname);
				}
			});
		}
	} else {
		require(["comments"],function(comments){
			comments.run({msid:id, wrapper:'#comment-section-'+id});
		});
		require(["tiljs/event"],function(event){
			event.subscribe("comments.loaded",function(){
				if (window.getParameterByName('comments') == 'show') {
					$("[data-plugin='comment-count-"+id	+"']").text($('#mainCommentCount').text());
				}
				$("[data-plugin='comment"+id+"']").show();
				setTimeout(function(){
					var cnt = parseInt($("#cs"+id).find("[data-plugin='comment-count']").eq(0).text());
                                       // console.log('START');
                                       // console.log(cnt);
                                       // console.log('END');
					if(isNaN(cnt) || cnt=="0")
					{
						cnt='';
					}else{
                                            cnt = '('+cnt+')';
                                        }
                                        //alert(cnt);
                                        //alert(id);
					$("[data-plugin='commentCount"+id+"']").text(cnt);
				},300);
				imgLazyLoad();
			});

			event.subscribe("comment.post.posting",function(){
				setTimeout(function(){
					$("#cs"+id).find('.highlight').animate({
    												opacity: 1});
					$("#cs"+id).find('.highlight').removeClass('highlight');
				},1000);
			});

			event.subscribe("comment.form.input.focus",function(secObj){
					$(secObj).parent().find('.comment-footer').show();
			});
		});
	}
	bindCommentSectionSlider(id);
	bindBottomCommentToSlider(id);
}

function bindCommentSectionSlider(id){
	$( "#cs"+id+" [data-plugin='show-comments']").on('click',function(){
		showCommentOverlay(id);
		enablegdpr();
	});

	$("#cs"+id+" .comments-overlay").on('click',function(event){
		if(!$(event.target).closest(".article").length && $(event.target).hasClass('loadmore')==false)
		{
			closeCommentOverlay(id);
		}
		event.stopPropagation();
	});
	$("#cs"+id).find('#overlayClose').on('click',function(){
		closeCommentOverlay(id);
	});
}

function showCommentOverlay(id){
	$("#cs"+id+" [data-comment-type='slider']").show();
	var secObj = $("#comment-section-"+id);

	$(secObj).find("article h2").show();
	$(secObj).animate({ "right": "0px" }, "fast",function() {
					    $('.sortby').show('');
					  	});
	$(secObj).find("#comments").show();
	//$(secObj).find("#comment_sort").trigger('change');
	$('body').css('overflow',"hidden");
	var attr = $(this).attr('data-showtype');
	if (typeof attr !== typeof undefined && attr == "autofocus") 
	{
		setTimeout(function(){
			$('.comment-form2').find('textarea').eq(0).focus();
		},1500);
	}
}

function closeCommentOverlay(id)
{
	$('.comments-overlay').hide();
	$('.sortby').hide();
	$(".article").css('right','-426px');
	$('body').css('overflow',"auto");
	$('.hghlgt').removeClass('hghlgt');
}

function imgLazyLoad(){
	$('.userimg').each(function()
	{
		var attr = $(this).attr('data-src');
		if (typeof attr !== typeof undefined && attr !== false) {
		   $(this).attr( "src", $(this).attr( "data-src").replace('http://','https://') );
		   $(this).removeAttr( "data-src" );
		}
	});
}

function bindBottomCommentToSlider(msid){
	$('[data-msid="'+msid+'"]').eq(0).find('.comment-box').on('click',function(event){

		var tag = event.target;
		if($(tag).hasClass('name')==false)
		{
			showCommentOverlay(msid);
			var commentId = $(tag).closest('.comment-box').attr('data-id');
			var targetBox = $('.comments-list:visible').find('[data-id="'+commentId+'"]');
			$(targetBox).addClass('hghlgt');
			setTimeout(function(){
				//$(targetBox).removeClass('hghlgt');
			},6000);

			setTimeout(function(){
				if($(tag).hasClass('icon-uparrow')){
					$(targetBox).find('.icon-uparrow').trigger('click');
				}
				if($(tag).hasClass('icon-downarrow')){
					$(targetBox).find('.icon-downarrow').trigger('click');
				}
				if($(tag).hasClass('cp-reply')){
					$(targetBox).find('[data-action="comment-reply"]').trigger('click');
				}
			},1000);	
		}

	});
}

function getUrlParameter(name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(location.search);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
};

var photoSwipeGallery = function(id){
	jQuery(".aShow[data-articlemsid='"+id+"']").find( 'div.gallery' ).each(function() {
		var el = jQuery(this);
		el.find( '.gallery-item' ).each(function( index ) {    
			jQuery(this).find('a').click(function(e){
				e.preventDefault();
				openPhotoSwipe(index, el);
			});
		});
		
	});
};


var parseThumbnailElements = function(el) {
	items = [];
	el.find( '.gallery-item' ).each(function( index ) {    
		items.push({src:jQuery(this).find('a').attr('href'), w:jQuery(this).find('img').attr('width'), h:jQuery(this).find('img').attr('height'), title:jQuery.trim(jQuery(this).find('.gallery-caption').text())});
	});

	return items;
};


var openPhotoSwipe = function(ind, galleryElement) {
	var pswpElement = document.querySelectorAll('.pswp')[0];
	// define options (if needed)
	var options = {
		// optionName: 'option value'
		// for example:
		history: true,
		index: ind // start at first slide
	};
	
	//options.mainClass = 'pswp--minimal--dark';
	//options.barsSize = {top:0,bottom:0};
	options.captionEl = false;
	options.fullscreenEl = false;
	options.shareEl = false;
	//options.bgOpacity = 0.85;
	//options.tapToClose = true;
	//options.tapToToggleControls = false;
	
	items = parseThumbnailElements(galleryElement);
	
	var gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
	gallery.init();
	//gaTrack('Details', 'imageswipe', 'open', 0, 0);
	gallery.listen('close', function() {
		//gaTrack('Details', 'imageswipe', 'close', 0, 0);
	});
	gallery.listen('afterChange', function() {
		//gaTrack('Details', 'imageswipe', 'slide', 0, 0);
	});
};
