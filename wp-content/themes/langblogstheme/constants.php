<?php 
global $customOptionData;
$customOptionGeneralData = get_option('custom_general_constant_option_fields');
$customOptionssData = get_option('custom_ss_cons_option');
$customOptionAdData = get_option('custom_ad_constant_option_fields');
if($customOptionssData){
    $customOptionGeneralData = @array_merge($customOptionGeneralData,$customOptionssData);
}
if($customOptionAdData){
    $customOptionGeneralData = @array_merge($customOptionGeneralData,$customOptionAdData);
}
$customOptionData = $customOptionGeneralData;

$randomNumber = rand();
define('Z_IMAGE_PLACEHOLDER', get_template_directory_uri().get_custom_option('Z_AUTHOR_PLACEHOLDER'));
define('FB_PLACEHOLDER', get_template_directory_uri().get_custom_option('FB_PLACEHOLDER'));
define('Z_AUTHOR_PLACEHOLDER', get_template_directory_uri().get_custom_option('Z_AUTHOR_PLACEHOLDER'));
define('RECO_URL',get_custom_option('RECO_URL'));
define('COMMENTS_URL',get_custom_option('COMMENTS_URL'));
define('RCHID', get_custom_option('RCHID'));
define('MYTIMES_URL',get_custom_option('MYTIMES_URL'));
define('MYT_COMMENTS_API_KEY', get_custom_option('MYT_COMMENTS_API_KEY'));

//echo '<pre>';print_r($customOptionData);die;

global $my_settings;
$my_settings['site_lang'] = get_custom_option('site_lang');
$my_settings['og_locale'] = get_custom_option('og_locale');
$my_settings['google_site_verification'] = get_custom_option('google_site_verification');
$my_settings['fblikeurl'] = get_custom_option('fblikeurl');
$my_settings['favicon'] = get_custom_option('favicon');
$my_settings['ga'] = get_custom_option('ga');
$my_settings['fbappid'] = get_custom_option('fbappid');
$my_settings['channel_url_part'] = get_custom_option('channel_url_part');
$my_settings['main_site_url'] = get_custom_option('main_site_url');
$my_settings['main_site_txt'] = get_custom_option('main_site_txt');
$my_settings['fb_url'] = get_custom_option('fb_url');
$my_settings['twitter_url'] = get_custom_option('twitter_url');
$my_settings['twitter_handle'] = get_custom_option('twitter_handle');
$my_settings['google_url'] = get_custom_option('google_url');
$my_settings['rss_url'] = get_custom_option('rss_url');
$my_settings['logo_title'] = get_custom_option('logo_title');
$my_settings['logo_url'] = get_custom_option('logo_url');
$my_settings['footer_logo_txt'] = get_custom_option('footer_logo_txt');
$my_settings['comscore_tag'] = get_custom_option('comscore_tag');
$my_settings['ibeat_channel'] = get_custom_option('ibeat_channel');
$my_settings['ibeat_host'] = get_custom_option('ibeat_host');
$my_settings['ibeat_key'] = get_custom_option('ibeat_key');
$my_settings['ibeat_domain'] = get_custom_option('ibeat_domain');
$my_settings['footer_dmp_tag'] = get_custom_option('footer_dmp_tag');
$my_settings['footer_google_tag'] = get_custom_option('footer_google_tag');
$my_settings['google_tag_js_head'] = get_custom_option('google_tag_js_head');
$my_settings['google_ad_top'] = get_custom_option('google_ad_top');
$my_settings['google_ad_right_1'] = get_custom_option('google_ad_right_1');
$my_settings['google_ad_right_2'] = get_custom_option('google_ad_right_2');
$my_settings['visual_revenue_reader_response_tracking_script'] = get_custom_option('visual_revenue_reader_response_tracking_script');
$my_settings['visual_revenue_reader_response_tracking_script_for_not_singular'] = get_custom_option('visual_revenue_reader_response_tracking_script_for_not_singular');
$my_settings['not_found_heading'] = get_custom_option('not_found_heading');
$my_settings['not_found_txt'] = get_custom_option('not_found_txt');
$my_settings['search_placeholder_txt'] = get_custom_option('search_placeholder_txt');
$my_settings['go_to_the_profile_of_txt'] = get_custom_option('go_to_the_profile_of_txt');
$my_settings['go_to_txt'] = get_custom_option('go_to_txt');
$my_settings['share_link_fb_txt'] = get_custom_option('share_link_fb_txt');
$my_settings['share_link_twitter_txt'] = get_custom_option('share_link_twitter_txt');
$my_settings['share_link_linkedin_txt'] = get_custom_option('share_link_linkedin_txt');
$my_settings['mail_link_txt'] = get_custom_option('mail_link_txt');
$my_settings['read_complete_article_here_txt'] = get_custom_option('read_complete_article_here_txt');
$my_settings['featured_txt'] = get_custom_option('featured_txt');
$my_settings['disclaimer_txt'] = get_custom_option('disclaimer_txt');
$my_settings['disclaimer_on_txt'] = get_custom_option('disclaimer_on_txt');
$my_settings['disclaimer_off_txt'] = get_custom_option('disclaimer_off_txt');
$my_settings['most_discussed_txt'] = get_custom_option('most_discussed_txt');
$my_settings['more_txt'] = get_custom_option('more_txt');
$my_settings['less_txt'] = get_custom_option('less_txt');
//$my_settings['login_txt'] = '';
//$my_settings['logout_txt'] = '';
$my_settings['login_txt'] = get_custom_option('login_txt');
$my_settings['logout_txt'] = get_custom_option('logout_txt');
$my_settings['view_all_posts_in_txt'] = get_custom_option('view_all_posts_in_txt');
$my_settings['home_txt'] = get_custom_option('home_txt');
$my_settings['blogs_txt'] = get_custom_option('blogs_txt');
$my_settings['search_results_for_txt'] = get_custom_option('search_results_for_txt');
$my_settings['most_read_txt'] = get_custom_option('most_read_txt');
$my_settings['popular_tags_txt'] = get_custom_option('popular_tags_txt');
$my_settings['recently_joined_authors_txt'] = get_custom_option('recently_joined_authors_txt');
$my_settings['like_us_txt'] = get_custom_option('like_us_txt');
$my_settings['author_txt'] = get_custom_option('author_txt');
$my_settings['popular_from_author_txt'] = get_custom_option('popular_from_author_txt');
$my_settings['search_authors_by_name_txt'] = get_custom_option('search_authors_by_name_txt');
$my_settings['search_txt'] = get_custom_option('search_txt');
$my_settings['back_to_authors_page_txt'] = get_custom_option('back_to_authors_page_txt');
$my_settings['no_authors_found_txt'] = get_custom_option('no_authors_found_txt');
$my_settings['further_commenting_is_disabled_txt'] = get_custom_option('further_commenting_is_disabled_txt');
$my_settings['comments_on_this_post_are_closed_now_txt'] = get_custom_option('comments_on_this_post_are_closed_now_txt');
$my_settings['add_your_comment_here_txt'] = get_custom_option('add_your_comment_here_txt');
$my_settings['characters_remaining_txt'] = get_custom_option('characters_remaining_txt');
$my_settings['share_on_fb_txt'] = get_custom_option('share_on_fb_txt');
$my_settings['share_on_twitter_txt'] = get_custom_option('share_on_twitter_txt');
$my_settings['sort_by_txt'] = get_custom_option('sort_by_txt');
$my_settings['newest_txt'] = get_custom_option('newest_txt');
$my_settings['oldest_txt'] = get_custom_option('oldest_txt');
$my_settings['discussed_txt'] = get_custom_option('discussed_txt');
$my_settings['up_voted_txt'] = get_custom_option('up_voted_txt');
$my_settings['down_voted_txt'] = get_custom_option('down_voted_txt');
$my_settings['be_the_first_one_to_review_txt'] = get_custom_option('be_the_first_one_to_review_txt');
$my_settings['more_points_needed_to_reach_next_level_txt'] = get_custom_option('more_points_needed_to_reach_next_level_txt');
$my_settings['know_more_about_times_points_txt'] = get_custom_option('know_more_about_times_points_txt');
$my_settings['know_more_about_times_points_link'] = get_custom_option('know_more_about_times_points_link');
$my_settings['badges_earned_txt'] = get_custom_option('badges_earned_txt');
$my_settings['just_now_txt'] = get_custom_option('just_now_txt');
$my_settings['follow_txt'] = get_custom_option('follow_txt');
$my_settings['reply_txt'] = get_custom_option('reply_txt');
$my_settings['flag_txt'] = get_custom_option('flag_txt');
$my_settings['up_vote_txt'] = get_custom_option('up_vote_txt');
$my_settings['down_vote_txt'] = get_custom_option('down_vote_txt');
$my_settings['mark_as_offensive_txt'] = get_custom_option('mark_as_offensive_txt');
$my_settings['find_this_comment_offensive_txt'] = get_custom_option('find_this_comment_offensive_txt');
$my_settings['reason_submitted_to_admin_txt'] = get_custom_option('reason_submitted_to_admin_txt');
$my_settings['choose_reason_txt'] = get_custom_option('choose_reason_txt');
$my_settings['reason_for_reporting_txt'] = get_custom_option('reason_for_reporting_txt');
$my_settings['foul_language_txt'] = get_custom_option('foul_language_txt');
$my_settings['defamatory_txt'] = get_custom_option('defamatory_txt');
$my_settings['inciting_hatred_against_certain_community_txt'] = get_custom_option('inciting_hatred_against_certain_community_txt');
$my_settings['out_of_context_spam_txt'] = get_custom_option('out_of_context_spam_txt');
$my_settings['others_txt'] = get_custom_option('others_txt');
$my_settings['report_this_txt'] = get_custom_option('report_this_txt');
$my_settings['close_txt'] = get_custom_option('close_txt');
$my_settings['already_marked_as_offensive'] = get_custom_option('already_marked_as_offensive');
$my_settings['flagged_txt'] = get_custom_option('flagged_txt');
$my_settings['blogauthor_link'] = get_custom_option('blogauthor_link');
$my_settings['old_blogauthor_link'] = get_custom_option('old_blogauthor_link');
$my_settings['blog_link'] = get_custom_option('blog_link');
$my_settings['common_cookie_domain'] = get_custom_option('common_cookie_domain');
$my_settings['quill_lang'] = get_custom_option('quill_lang');
$my_settings['quill_link_1'] = get_custom_option('quill_link_1');
$my_settings['quill_link_2'] = get_custom_option('quill_link_2');
$my_settings['quill_link_3'] = get_custom_option('quill_link_3');
$my_settings['quill_link_4'] = get_custom_option('quill_link_4');
$my_settings['offensive_comment_warning'] = get_custom_option('offensive_comment_warning');
$my_settings['footer'] = str_replace('currentYearText',date('Y') ,get_custom_option('footer'));;
$my_settings['footer_css'] = get_custom_option('footer_css');
$random = rand();
$my_settings['ctn_homepage'] = get_custom_option('ctn_homepage');
$my_settings['ctn_homepage_rhs'] = get_custom_option('ctn_homepage_rhs');
$my_settings['ctn_article_list'] = get_custom_option('ctn_article_list');
$my_settings['ctn_article_list_rhs'] = get_custom_option('ctn_article_list_rhs');
$my_settings['ctn_article_show_rhs'] = @str_replace('randomNumberText',$random,get_custom_option('ctn_article_show_rhs'));
$my_settings['ctn_article_show_end_article_1'] = @str_replace('randomNumberText',$random,get_custom_option('ctn_article_show_end_article_1'));
$my_settings['ctn_article_show_end_article_2'] = @str_replace('randomNumberText',$random,get_custom_option('ctn_article_show_end_article_2'));

// esi ads setting
//echo get_custom_option('esi_status');die;
$my_settings['esi_status'] = get_custom_option('esi_status');// TEST, LIVE, NONE

if(isMobile()){
	
	$esiHeaderStr = '<!--esi 
           <esi:eval src="/fetch_native_content/?fpc=$url_encode($(HTTP_COOKIE{\'_col_uuid\'}))&ab=$(HTTP_COOKIE{\'ce_nbapm\'})&id=$url_encode(\'##ESIIDS##\')&_t=4&_u=$url_encode($(HTTP_HOST)+$(REQUEST_PATH))&ua=$url_encode($(HTTP_USER_AGENT))&ip=$(REMOTE_ADDR)&_v=0&dpv=1&r=$rand()" dca="esi"/> 
           -->';

        $esiHeaderStr .= <<<'ESISTR'
<!--esi           
                        <esi:choose>
                            <esi:when test="$exists($(native_content{'_col_uuid'}))">
                		        $add_header('Set-Cookie', $(native_content{'_col_uuid'}))
                            </esi:when>
                        </esi:choose>
                    -->
                
                    <!--esi
                        <esi:choose>
                            <esi:when test="$exists($(native_content{'_col_script'}))">
                		        $(native_content{'_col_script'})
                            </esi:when>
                        </esi:choose>
                    -->
                
                    <!--esi
                        <esi:choose>
                            <esi:when test="$exists($(native_content{'_col_ab_call'}))">
                		        $(native_content{'_col_ab_call'})
                            </esi:when>
                        </esi:choose>
                    -->
ESISTR;
$my_settings['esi_header'] = get_custom_option('esi_header_m');;

	$my_settings['esi_homepage_ids'] = get_custom_option('esi_homepage_ids_m');
	
	/*HP_CTN_NAT*/
	$my_settings['esi_homepage'] = get_custom_option('esi_homepage_m');
					
	/*HP_RHS_RCMW_CTN_NAT*/
	$my_settings['esi_homepage_rhs'] = get_custom_option('esi_homepage_rhs_m');
	
	$my_settings['esi_article_list_ids'] =get_custom_option('esi_article_list_ids_m');
	
	/*ROS_AL_CTN_NAT*/
	$my_settings['esi_article_list'] = get_custom_option('esi_article_list_m');
	
	/*ROS_AL_RHS_RCMW_CTN_NAT*/
	$my_settings['esi_article_list_rhs'] = get_custom_option('esi_article_list_rhs_m');
	
	$my_settings['esi_article_show_ids'] = get_custom_option('esi_article_show_ids_m');
	
	/*ROS_AS_RHS_RCMW_CTN_NAT*/
	$my_settings['esi_article_show_rhs'] = get_custom_option('esi_article_show_rhs_m');
	
	/*ROS_AS_EOA_RCMW_CTN_NAT*/
	$my_settings['esi_article_show_end_article_1'] = get_custom_option('esi_article_show_end_article_1_m');
	
	/*ROS_AS_EOA1_RCMW_CTN_NAT*/
	$my_settings['esi_article_show_end_article_2'] = get_custom_option('esi_article_show_end_article_2_m');
	
	/*Mweb_AS_BLY_VDO_CTN_NAT*/                               
	$my_settings['ctn_article_show_mid_article_video'] = get_custom_option('ctn_article_show_mid_article_video_m');

}else{

	$esiHeaderStr = '<!--esi 
           <esi:eval src="/fetch_native_content/?fpc=$url_encode($(HTTP_COOKIE{\'_col_uuid\'}))&ab=$(HTTP_COOKIE{\'ce_nbapd\'})&id=$url_encode(\'##ESIIDS##\')&_t=4&_u=$url_encode($(HTTP_HOST)+$(REQUEST_PATH))&ua=$url_encode($(HTTP_USER_AGENT))&ip=$(REMOTE_ADDR)&_v=0&dpv=1&r=$rand()" dca="esi"/> 
           -->';

        $esiHeaderStr .= <<<'ESISTR'
<!--esi           
                        <esi:choose>
                            <esi:when test="$exists($(native_content{'_col_uuid'}))">
                		        $add_header('Set-Cookie', $(native_content{'_col_uuid'}))
                            </esi:when>
                        </esi:choose>
                    -->
                
                    <!--esi
                        <esi:choose>
                            <esi:when test="$exists($(native_content{'_col_script'}))">
                		        $(native_content{'_col_script'})
                            </esi:when>
                        </esi:choose>
                    -->
                
                    <!--esi
                        <esi:choose>
                            <esi:when test="$exists($(native_content{'_col_ab_call'}))">
                		        $(native_content{'_col_ab_call'})
                            </esi:when>
                        </esi:choose>
                    -->
ESISTR;
$my_settings['esi_header'] = get_custom_option('esi_header');;


	$my_settings['esi_homepage_ids'] = get_custom_option('esi_homepage_ids');
	
	/*HP_CTN_NAT*/
	$my_settings['esi_homepage'] = get_custom_option('esi_homepage');
	
	/*HP_RHS_RCMW_CTN_NAT*/
	$my_settings['esi_homepage_rhs'] = get_custom_option('esi_homepage_rhs');
					
	$my_settings['esi_article_list_ids'] = get_custom_option('esi_article_list_ids');
	
	/*ROS_AL_CTN_NAT*/
	$my_settings['esi_article_list'] = get_custom_option('esi_article_list');
	
	/*ROS_AL_RHS_RCMW_CTN_NAT*/
	$my_settings['esi_article_list_rhs'] = get_custom_option('esi_article_list_rhs');
					
	$my_settings['esi_article_show_ids'] = get_custom_option('esi_article_show_ids');
	
	/*ROS_AS_RHS_RCMW_CTN_NAT*/
	$my_settings['esi_article_show_rhs'] = get_custom_option('esi_article_show_rhs');
					
	/*ROS_AS_EOA_RCMW_CTN_NAT*/
	$my_settings['esi_article_show_end_article_1'] = get_custom_option('esi_article_show_end_article_1');
					
	/*ROS_AS_EOA1_RCMW_CTN_NAT*/
	$my_settings['esi_article_show_end_article_2'] = get_custom_option('esi_article_show_end_article_2');
	
	/*AS_BLY_VDO_CTN_NAT*/  
	$my_settings['ctn_article_show_mid_article_video'] = get_custom_option('ctn_article_show_mid_article_video');
}

$my_settings['ATF_300_DIV_ID'] =get_custom_option('ATF_300_DIV_ID');
$my_settings['ATF_728_DIV_ID'] =get_custom_option('ATF_728_DIV_ID');
$my_settings['BTF_300_DIV_ID'] =get_custom_option('BTF_300_DIV_ID');
$my_settings['ATF_300_AD_CODE'] =get_custom_option('ATF_300_AD_CODE');
$my_settings['ATF_728_AD_CODE'] =get_custom_option('ATF_728_AD_CODE');
$my_settings['BTF_300_AD_CODE'] =get_custom_option('BTF_300_AD_CODE');
$my_settings['channel'] =get_custom_option('channel_name');
$my_settings['site_id'] =get_custom_option('site_id');
$my_settings['domain'] =get_custom_option('domain_name');
$my_settings['ofcommenthostid'] =get_custom_option('ofcommenthostid');
$my_settings['ofcommentchannelid'] =get_custom_option('ofcommentchannelid');
$my_settings['appid'] =get_custom_option('appid');
$my_settings['appid'] =get_custom_option('gPlusClient');
$commnetText = ["name_required" =>"Please enter your name.",
			"location_required" =>"Please enter your location.",
			"captcha_required" =>"Please enter captcha value.",
			"name_toolong" =>"Name cannot be longer than 30 chars.",
			"name_not_string" =>"Name can only contain alphabets.",
			"location_toolong" =>"Location cannot be longer than 30 chars.",
			"location_not_string" =>"Location can only contain alphabets.",
			"captcha_toolong" =>"Captcha cannot be longer than 4 chars.",
			"captcha_number_only" =>"Captcha value can only be a number.",
			"email_required" =>"Please enter your email address.",
			"email_invalid" =>"Please enter a valid email address.",
			"captcha_invalid" =>"Please enter a valid captcha value.",
			"minlength"=> "You can t post this comment as the length it is too short. ",
			"blank"=> "You can t post this comment as it is blank.",
			"maxlength"=> "You have entered more than 3000 characters.",
			"popup_blocked"=> "Popup is blocked.",
			"has_url"=> "You can t post this comment as it contains URL.",
			"duplicate"=> "You can t post this comment as it is identical to the previous one.",
			"abusive"=> "You can't post this comment as it contains inappropriate content.",
			"self_agree"=> "You can't Agree with your own comment",
			"self_disagree"=> "You can't Disagree with your own comment",
			"self_recommend"=> "You can't Recommend your own comment",
			"self_offensive"=> "You can't mark your own comment as Offensive",
			"already_agree"=> "You have already Agreed with this comment",
			"already_disagree"=> "You have already Disagreed with this comment",
			"already_recommended"=> "You have already Recommended this comment",
			"already_offensive"=> "You have already marked this comment Offensive",
			"cant_agree_disagree"=> "You can't Agree and Disagree with the same comment",
			"cant_agree_offensive"=> "You can't Agree and mark the same comment Offensive",
			"cant_disagree_recommend"=> "You can't Disagree and Recommend the same comment",
			"cant_recommend_offensive"=> "You can't Recommend and mark the same comment Offensive",
			"permission_facebook"=> "You can't post to facebook. Post permission is required.",
			"offensive_reason"=> "Please select a reason.",
			"offensive_reason_text"=> "Please enter a reason." ,
			"offensive_reason_text_limit"=> "Please enter less than 200 chars.",
			"be_the_first_text"=> "Be the first one to review.",
			"no_comments_discussed_text"=> "None of the comments have been discussed.",
			"no_comments_up_voted_text"=> "None of the comments have been up voted.",
			"no_comments_down_voted_text"=> "None of the comments have been down voted."
		 ];
$my_settings['comment_text'] = get_custom_option('comment_text');