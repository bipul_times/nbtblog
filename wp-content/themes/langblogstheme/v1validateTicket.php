<?php
	global $my_settings;
	$channel = isLang();

	function download_page($path){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$path);
		curl_setopt($ch, CURLOPT_FAILONERROR,1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 15);
		$retValue = curl_exec($ch);			 
		curl_close($ch);
		return $retValue;
	}

	$ticketId = $_GET['ticketId'];

	if(empty($ticketId)){
		$ticketId = $_POST['ticketId'];
	}

	if(empty($ticketId )) {
		// send to error page
		$URL = $my_settings['ssoUrl'].'/sso/identity/login/socialLoginErrorHandler?channel=' . $channel . '&code=500&oauthsiteid='. $oauthsiteid . '&type=' . $type . '&msg=invalid_parameters_to_file';
		header('Location: '. $URL);
	} else {
		$URL = $my_settings['ssoUrl'].'/sso/crossdomain/v1validateTicket?channel=' . $channel . '&ticketId=' . $ticketId . '&getOAuthData=true&type=JSON'; 
		$data = download_page($URL);
		$expires=time()+86400*30;
		$domainName='.' . $my_settings['common_cookie_domain'];
		
		if($data) {
			$jsonData = json_decode($data);
			setcookie("ssoid",$jsonData->{'userId'},$expires,'/',$domainName);
		}
		echo $_GET['callback'] . '({"msg":"success","code":200});';exit;
	}
?>
