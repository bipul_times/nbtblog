<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd" >
<html>

<head>
    <META http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>MT Newsletter</title>
    <style>
        body,
        div,
        img,
        p {
            margin: 0;
            padding: 0;
            font-family: ARIAL UNICODE MS, mangal, raghu8;
            font-size: 17px;
        }
        
        a:hover {
            text-decoration: underline;
        }
        
        a.hover {
            text-decoration: underline;
        }
    </style>
</head>

<body style="background:#f2f2f2;margin:0;padding:0;font-family: ARIAL UNICODE MS,mangal,raghu8;font-size:17px;">
    <table width="700" cellspacing="0" cellpadding="0" align="center">
        <tr>
            <td>
                <div style="width:700px;margin:0 auto;" class="newsltr_cont">
                    <div style="text-align:center;width:650px;font-size:11px;margin:0 auto;padding:10px 0;" class="newsltr_message">To ensure delivery directly to you inbox, please add <a href="mailto:mtnewsletterservices@indiatimes.com">mtnewsletterservices@indiatimes.com</a> to your address book today. If you are having trouble viewing this newsletter, please <a href="">click here</a></div>
                    <div style="width:700px;height:48px;" class="logo"></div>
                    <div style="width: 700px;float: left;background-color:#fff;" id="topnav_outer">
                        <div style="width:700px; margin:0 auto; " id="topNav">
                            <ul style="padding:0; margin:0; margin:0;border-bottom:1px solid #DADADA; padding:0; list-style:none; float:left; width:700px;" class="tabs">
                                <li style="float:left; display:block; padding-top:10px; margin:0; ">
                                    <a target="_blank" href="https://blogs.maharashtratimes.indiatimes.com" style="float:left; padding:0 14px; text-decoration:none !important; color:#fff; height:33px; line-height:33px; font-size:17px;"><img title="Maharashtra Times" alt="Maharashtra Times" border="0" src="https://maharashtratimes.indiatimes.com/photo/2461378.cms"></a>
                                </li>
                            </ul>
                        </div>
                        <div style="clear:both;" class="clear"></div>
                    </div>
                    <div style="background:#fff; float:left;width:668px;padding:20px 15px 15px 15px;border-left:1px solid #dadada; border-right:1px solid #dadada" class="newsltr_body">
                        <div style="float:left;width:655px;margin-right:13px;" class="lftpart">
                            <p style="margin:0;padding:0;font-family: ARIAL UNICODE MS,mangal,raghu8;font-size:17px;"><font style="font-weight:bold">
                           Dear Reader,
                          </font>
                                <br>
                                <br>
                                <br>You just wrote a comment on <a href="https://blogs.maharashtratimes.indiatimes.com" style="text-decoration:none;">blogs.maharashtratimes.indiatimes.com</a>. Please <a href="https://blogs.maharashtratimes.indiatimes.com/?p=[#article_id]&comments=show&amp;commentid=[#usrcommentid]&amp;type=[#commenttype]">click here</a> to view it.
                                <br>
                                <br> Do keep writing in. Thank you!
                                <br>
                                <br>
                                <br>
                                <br><font style="font-weight:bold">
                           Regards,
                          </font>
                                <br> Editor
                                <br>
                            </p>
                        </div>
                        <div style="float:right; width:300px" class="rightpart">
                            <div style="margin:0;padding:0;font-family: ARIAL UNICODE MS,mangal,raghu8;font-size:17px;">
                                <script type="text/javascript">
                                    var ord = window.ord || Math.floor(Math.random() * 1e16);
                                    document.write('<script type="text/javascript" src="http://ad.doubleclick.net/N7176/adj/Navbharattimes/NBT_ROS_ATF_BDW;sz=300x1050,300x250;ord=' + ord + '?"><\/script>');
                                </script>
                                <noscript>
                                    <a href="http://ad.doubleclick.net/N7176/jump/Navbharattimes/NBT_ROS_ATF_BDW;sz=300x1050,300x250;ord=[timestamp]?"><img src="http://ad.doubleclick.net/N7176/ad/Navbharattimes/NBT_ROS_ATF_BDW;sz=300x1050,300x250;ord=[timestamp]?" style="border:0px;margin:0;padding:0;font-family: ARIAL UNICODE MS,mangal,raghu8;font-size:17px;"></a>
                                </noscript>
                            </div>
                            <div style="clear:both;height:20px;background:#ffffff" class="clr"></div>
                        </div>
                    </div>
                    <div style="float:left"><img alt="newsletter MT" src="https://maharashtratimes.indiatimes.com/photo/19527085.cms" style="border:0px;margin:0;padding:0;font-family: ARIAL UNICODE MS,mangal,raghu8;font-size:17px;"></div>
                    <div class="footer" style="font-size:11px; height:50px; padding-bottom:10px;clear:both;" align="center"><a href="http://www.indiatimes.com/aboutus" target="_blank" style="text-decoration:none;">About Us</a>| <a href="http://advertise.indiatimes.com/" target="_blank">Advertise with Us</a> | <a href="http://www.indiatimes.com/termsandcondition" target="_blank">Terms of Use</a> | <a href="http://www.indiatimes.com/privacypolicy" target="_blank">Privacy 
                        Policy</a> | <a href="https://maharashtratimes.indiatimes.com/feedback.cms" target="_blank">Feedback</a> | <a href="https://maharashtratimes.indiatimes.com/sitemap.cms" target="_blank">Sitemap</a>
                        <br><span style="font-size:11px; font-family:ARIAL UNICODE MS, mangal, raghu8" class="copyright">Copyright &copy; <?php echo date('Y');?> Bennett Coleman &amp; Co. Ltd. All rights reserved. For reprint rights: <a href="http://timescontent.com/" target="_blank">Times Syndication Service</a></span>
                        <br><span style="padding-top:5px;">If you want to unsubscribe this service, please <a href="#" target="_blank">click here</a></span></div>
                </div>
            </td>
        </tr>
    </table>
</body>

</html>