<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd" >
<html>

<head>
    <META http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Ei Samay Newsletter</title>
    <style>
        body,
        div,
        img,
        p {
            margin: 0;
            padding: 0;
            font-family: ARIAL UNICODE MS, mangal, raghu8;
            font-size: 17px;
        }
        
        body {
            background: #f2f2f2;
        }
        
        .newsltr_cont {
            width: 700px;
            margin: 0 auto;
        }
        
        .newsltr_message {
            text-align: center;
            width: 650px;
            font-size: 11px;
            margin: 0 auto;
            padding: 10px 0;
        }
        
        .logo {
            width: 700px;
            height: 48px;
        }
        
        #topbar_outer {
            background: #fff url(http://navbharattimes.indiatimes.com/photo/18792467.cms) repeat-x center 30px;
            position: fixed;
            top: 0;
            z-index: 10000;
        }
        
        #topnav_outer {
            width: 700px;
            float: left;
            background-image: url('http://navbharattimes.indiatimes.com/photo/19526379.cms');
            background-repeat: repeat-x;
            height: 33px
        }
        
        #topNav {
            width: 700px;
            margin: 0 auto;
        }
        
        .tabs {
            margin: 0;
            padding: 0;
            list-style: none;
            float: left;
            width: 700px;
        }
        
        .tabs ul {
            padding: 0;
            margin: 0;
        }
        
        .tabs li {
            float: left;
            display: block;
            padding: 0;
            margin: 0;
        }
        
        .tabs a {
            float: left;
            padding: 0 14px;
            text-decoration: none !important;
            color: #fff;
            height: 33px;
            line-height: 33px;
            font-size: 17px;
        }
        
        .clear {
            clear: both;
        }
        
        .newsltr_body {
            background: #fff;
            float: left;
            width: 668px;
            padding: 20px 15px 15px 15px;
            border-left: 1px solid #dadada;
            border-right: 1px solid #dadada
        }
        
        .lftpart {
            float: left;
            width: 655px;
            margin-right: 13px;
        }
        
        .txt_style {
            color: #a1a1a1;
        }
        
        .txt_deco {
            color: #4e80ba;
            font-size: 15px;
        }
        
        .txt_deco a {
            padding-bottom: 5px;
            color: #4e80ba;
            font-size: 14px;
            text-decoration: none;
            line-height: 23px;
        }
        
        .clr {
            clear: both;
            height: 20px;
            background: #ffffff
        }
        
        .footer {
            font-size: 11px;
            height: 50px;
            padding-bottom: 10px;
            clear: both;
        }
        
        .footer span.copyright {
            font-size: 11px;
            font-family: ARIAL UNICODE MS, mangal, raghu8
        }
        
        .footer a {
            text-decoration: none;
        }
        
        img {
            border: 0px;
        }
    </style>
    <link type="image/x-icon" href="//eisamay.indiatimes.com/icons/ei-samay_favicon.ico" rel="shortcut icon">
    <script type="text/javascript">
        var _sf_startpt = (new Date()).getTime()
    </script>
    <script type="text/javascript">
        document.domain = 'indiatimes.com';
        var domainname = document.location.host;
        var hrefpath = document.location.href;
        var domainpath = window.location.origin;
        var __activity = [];
    </script>
    <script language="javascript">
        var qstr = 'cms-sec0=&cms-sec1=&cms-sec2=&cms-sec3=&cms-sec4=&cms-sec5=&cms-sec6=';
        var qmsid = '48893934';
        var cmsur_type = 'viewed';

        function blockError() {
            return true;
        }
        window.onerror = blockError;
        var dtTT_startofpage = new Date();
        var rndtno = Math.random();
        var timeslog_channel_url = 'eisamay.indiatimes.com';
    </script>
    <script>
        function Get_Ckie(name) {
            var value = "; " + document.cookie;
            var parts = value.split("; " + name + "=");
            if (parts.length == 2) return parts.pop().split(";").shift();
        }

        (function(i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function() {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-29031733-3', 'auto', {
            'allowLinker': true
        });
        if (typeof Get_Ckie == "function" && Get_Ckie('ssoid')) {
            ga('set', 'userId', Get_Ckie('ssoid'));
        }
        ga('require', 'displayfeatures');
        ga('require', 'linker');
        ga('linker:autoLink', ['superpacer.in', 'atmadeep.com', 'womenbikerally.com', 'pujoeisamay.com', 'eisamay.com', 'esyoungscholars.com']);
        ga('send', 'pageview');
    </script>
    <script>
        var _comscore = _comscore || [];
        _comscore.push({
            c1: "2",
            c2: "6036484"
        });
        (function() {
            var s = document.createElement("script"),
                el = document.getElementsByTagName("script")[0];
            s.async = true;
            s.src = (document.location.protocol == "https:" ? "https://sb" : "http://b") + ".scorecardresearch.com/beacon.js";
            el.parentNode.insertBefore(s, el);
        })();
    </script>
    <noscript><img src="https://sb.scorecardresearch.com/p?c1=2&amp;c2=6036484&amp;cv=2.0&amp;cj=1"></noscript>
    <script>
        ! function(f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function() {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window,
            document, 'script', '//connect.facebook.net/en_US/fbevents.js');

        fbq('init', '1645030205734815');
        fbq('track', 'PageView');
    </script>
    <noscript><img src="https://www.facebook.com/tr?id=1645030205734815&amp;ev=PageView&amp;noscript=1" style="display:none" width="1" height="1"></noscript>
    <script language="javascript">
        document.domain = 'indiatimes.com';
    </script>
</head>

<body>
    <table width="700" cellspacing="0" cellpadding="0" align="center">
        <tr>
            <td>
                <div class="newsltr_cont">
                    <div class="newsltr_body">
                        <div class="lftpart">
                            <p><font style="font-weight:bold">
									&#2474;&#2509;&#2480;&#2495;&#2527; &#2474;&#2494;&#2464;&#2453;,</font>
                                <br>
                                <br>&#2447;&#2439; &#2488;&#2478;&#2527;-&#2447;&#2480; &#2488;&#2457;&#2509;&#2455;&#2503; &#2488;&#2434;&#2479;&#2497;&#2453;&#2509;&#2468; &#2489;&#2451;&#2527;&#2494;&#2480; &#2460;&#2472;&#2509;&#2479; &#2438;&#2474;&#2472;&#2494;&#2453;&#2503; &#2437;&#2472;&#2503;&#2453; &#2471;&#2472;&#2509;&#2479;&#2476;&#2494;&#2470;&#2404;
                                <br>
                                <br>&#2438;&#2474;&#2472;&#2494;&#2480; &#2478;&#2472;&#2509;&#2468;&#2476;&#2509;&#2479; <a href="https://blogs.eisamay.indiatimes.com" style="text-decoration:none;">blogs.eisamay.indiatimes.com</a> -&#2447; &#2482;&#2494;&#2439;&#2477; &#2438;&#2459;&#2503;&#2404; &#2478;&#2472;&#2509;&#2468;&#2476;&#2509;&#2479;&#2463;&#2495; &#2470;&#2503;&#2454;&#2494;&#2480; &#2460;&#2472;&#2509;&#2479; <a style="text-decoration:none;" href="https://blogs.eisamay.indiatimes.com/?p=[#article_id]&comments=show&amp;commentid=[#usrcommentid]&amp;type=[#commenttype]">&#2453;&#2509;&#2482;&#2495;&#2453;</a> &#2453;&#2480;&#2497;&#2472;&#2404;
                                <br>
                                <br>&#2447;&#2439; &#2488;&#2478;&#2527;-&#2447;&#2480; &#2488;&#2457;&#2509;&#2455;&#2503; &#2472;&#2495;&#2460;&#2503;&#2451; &#2488;&#2434;&#2479;&#2497;&#2453;&#2509;&#2468; &#2469;&#2494;&#2453;&#2497;&#2472;&#2404; &#2488;&#2497;&#2474;&#2494;&#2480;&#2495;&#2486; &#2453;&#2480;&#2497;&#2472; &#2476;&#2472;&#2509;&#2471;&#2497;&#2470;&#2503;&#2480;&#2451;&#2404;
                                <br>
                                <br><font style="font-weight:bold">&#2486;&#2497;&#2477;&#2503;&#2458;&#2509;&#2459;&#2494;</font>
                                <br>&#2488;&#2478;&#2509;&#2474;&#2494;&#2470;&#2453;
                                <br>
                            </p>
                        </div>
                    </div>
                    <div class="footer" align="center"><a href="https://www.indiatimes.com/aboutus" target="_blank">About Us</a>| <a href="http://advertise.indiatimes.com/" target="_blank">Advertise with Us</a> | <a href="http://www.indiatimes.com/termsandcondition" target="_blank">Terms of Use</a> | <a href="http://www.indiatimes.com/privacypolicy" target="_blank">Privacy 
								    Policy</a> | <a href="https://eisamay.indiatimes.com/feedback.cms" target="_blank">Feedback</a> | <a href="http://eisamay.indiatimes.com/sitemap.cms" target="_blank">Sitemap</a>
                        <br><span class="copyright">Copyright &copy; <?php echo date('Y');?> Bennett Coleman &amp; Co. Ltd. All rights reserved. For reprint rights: <a href="http://timescontent.com/" target="_blank">Times Syndication Service</a></span>
                        <br><span style="padding-top:5px;">If you want to unsubscribe this service, please <a href="#" target="_blank">click here</a></span></div>
                </div>
            </td>
        </tr>
    </table>
    <div id="fb-root"></div>
    <script src="//connect.facebook.net/en_US/all.js"></script>
    <script>
        function invokeLogin() {
            try {
                isLoggedSso = getCookievaluetwitt('MSCSAuth');
                if (Get_Ckie('autologin') != null) {
                    Delete_Ckie("autologin", "/", ".indiatimes.com");
                }
                tpbar1();
            } catch (ex) {}
        }
    </script>
    <script>
        var usersessionkey = "";
        var loginFbSts = "";
        //FB.init({appId: '6b01f688a268fc70a489a8b444b7d021', status: true, cookie: true, xfbml: true});

        FB.init({
            appId: '117787264903013',
            status: true,
            cookie: true,
            xfbml: true
        });
        if (Get_Ckie('autologin') != null) {
            Delete_Ckie("autologin", "/", ".indiatimes.com");
        }

        function getFbActiveSession() {
            try {
                if (usersessionkey != '' && Get_Ckie('autologin') == null && Get_Ckie('MSCSAuth') == null) {
                    Set_Ckie("autologin", "1", 0, "/", ".indiatimes.com", "");
                    document.getElementById("signupsso").src = "/autologin.cms";
                } else {
                    if (loginFbSts == "notConnected") {
                        //Received the FB Active Session but app not authenticated
                        if (Get_Ckie('disableautolg') == null) {
                            Set_Ckie("autologin", "1", 0, "/", ".indiatimes.com", "");
                            loadPopup('13');
                        }
                    }
                    if (loginFbSts == "unknown") {
                        // FB not connected 
                    }
                }
            } catch (ex) {}
        }

        function getSessionFB() {
            FB.getLoginStatus(function(response) {
                loginFbSts = response.status;

                if (response.session) {
                    session = response.session;
                    sessionkey = session.session_key;
                    usersessionkey = sessionkey;
                } else {}
                getFbActiveSession();
            });
        }
        try {
            setTimeout("getSessionFB()", 2000);
        } catch (ex) {}
    </script>
    <script>
        var istrack1 = getCookievaluetwitt('MSCSAuth');
        if (istrack1 != null) {

            var rewardemail = getCookievaluetwitt('MSCSAuthDetails').split('=')[1];
            if (rewardemail.indexOf('@') == -1) {
                rewardemail = rewardemail + '@indiatimes.com';
            }
            var locurl = document.location.href;
            locurl = locurl.replace('cmsportaldev', 'timesofindia');
            lbsubmit(locurl, "toi", "b39b9b02b0ef4bc6a0f38eef3e4618fe", "", "visit", rewardemail);
        }
    </script>
    <script>
        getbvdetails();
    </script>
</body>

</html>