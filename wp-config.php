<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', getenv('dbName') );

/** MySQL database username */
define( 'DB_USER', getenv('dbUser') );

/** MySQL database password */
define( 'DB_PASSWORD', getenv('dbPassword') );

/** MySQL hostname */
define( 'DB_HOST', getenv('dbHost') );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'put your unique phrase here' );
define( 'SECURE_AUTH_KEY',  'put your unique phrase here' );
define( 'LOGGED_IN_KEY',    'put your unique phrase here' );
define( 'NONCE_KEY',        'put your unique phrase here' );
define( 'AUTH_SALT',        'put your unique phrase here' );
define( 'SECURE_AUTH_SALT', 'put your unique phrase here' );
define( 'LOGGED_IN_SALT',   'put your unique phrase here' );
define( 'NONCE_SALT',       'put your unique phrase here' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

$localDomains = array();
$localDomains[0] = "local.blogs.navbharattimes.indiatimes.com";
$localDomains[1] = "local.blogs.maharashtratimes.indiatimes.com"; 
$localDomains[2] = "local.blogs.eisamay.indiatimes.com"; 
$localDomains[3] = "local.readerblogs.navbharattimes.indiatimes.com";
$localDomains[4] = "local.blogs.tamil.samayam.com"; 
$localDomains[5] = "local.blogs.vijaykarnataka.indiatimes.com"; 
$localDomains[6] = "local.blogs.malayalam.samayam.com";
$localDomains[7] = "local.blogs.telugu.samayam.com";
if((isset($_SERVER['HTTP_FRONT_END_HTTPS']) && $_SERVER['HTTP_FRONT_END_HTTPS'] == "on") || in_array($_SERVER['HTTP_HOST'], $localDomains)) {
    $scheme = 'https://';
    $_SERVER['HTTPS'] = "on";
} else if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") {
    $scheme = 'https://';
} else {
    $scheme = 'http://';
} 

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );
define('WP_HOME',$scheme.$_SERVER['HTTP_HOST'] );
define('WP_SITEURL',$scheme.$_SERVER['HTTP_HOST'] );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
